** RULE 620. DOMESTIC ANIMAL **;
%macro da();
	data r620fac;
	set fac.'620# Domestic Animal$'n;
	if f1="" then delete;
	fac1='Liability - Bodily injury (Perso'n*1;
	fac2='Liability - Bodily injury (Medic'n*1;
	run;
	data DomesticAnimal; 
	set in.Policy_Info (keep=casenumber policynumber PolicyFormNumber DogsInHouseholdCount);
	if DogsInHouseholdCount <= 0 or PolicyFormNumber = 3 then DomesticAnimal_flag = 0;
	else DomesticAnimal_flag = 1;
	run;
	proc sql;
	    create table out.lia_DomesticAnimal_fac as
		select p.*
			, f.fac1 * DomesticAnimal_flag as liabipi_DomesticAnimal
			, f.fac2 * DomesticAnimal_flag as liabimp_DomesticAnimal
		from r620fac f, DomesticAnimal p;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 620. DOMESTIC ANIMAL';
	proc freq data=out.lia_DomesticAnimal_fac;
	tables DogsInHouseholdCount
		*PolicyFormNumber
		*liabipi_DomesticAnimal
		*liabimp_DomesticAnimal  / list missing;
	run;
%end;
%mend;
%da();
