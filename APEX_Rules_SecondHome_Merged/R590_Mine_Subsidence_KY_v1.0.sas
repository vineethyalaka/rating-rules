
data r590;
set fac.'590# Mine Subsidence$'n;
format f1 f3 12.2;
if f1 = . and f3 = . then delete;
run;
proc sql;
	create table out.Mine_Sub_Rate_A as
	select p.CaseNumber, p.PolicyNumber, e.HO2388_FLAG, p.CoverageA
		, case when e.HO2388_FLAG = '1' then f4 else 0 end as Mine_Sub_Rate_A
	from in.policy_info p
		inner join in.Endorsement_Info e on p.casenumber = e.casenumber
		left join r590 f on p.CoverageA >= f.f1 and (p.CoverageA <= f.f3 or f.f3 = .);
quit;


proc sql;
	create table out.Mine_Sub_Rate_B as
	select p.casenumber, p.policynumber, 
	case when e.HO2388_FLAG = '1'  and  e.HO_0448_flag="1" then 
	ceil(HO_0448_ADDL_LIMIT_01/10000)*2 else 0 end as Mine_Sub_Rate_B
	from in.policy_info p inner join in.Endorsement_Info e on p.casenumber = e.casenumber;
	quit;

proc sql;
	create table out.Mine_Sub_Rate as
	select a.casenumber, a.policynumber, a.Mine_Sub_Rate_A, b.Mine_Sub_Rate_B, a.Mine_Sub_Rate_A+b.Mine_Sub_Rate_B as Mine_Sub_Rate
	from out.Mine_Sub_Rate_A as a inner join out.Mine_Sub_Rate_B as b
	on a.casenumber=b.casenumber;
	quit;
