%macro coastal();
proc sql;
	create table work.Coastal_Group as 
	select p.casenumber, p.policynumber, p.distancetoshore, CoverageA
		, case when 0 < p.distancetoshore <= 250 then "0-250"
			   when 250 < p.distancetoshore <= 500 then "251-500"
			   when 500 < p.distancetoshore <= 1000 then "501-1000"
			   when 1000 < p.distancetoshore <= 1500 then "1,001-1,500"
			   when 1500 < p.distancetoshore <= 2000 then "1,501-2,000"
			   when 2000 < p.distancetoshore <= 2500 then "2,001-2,500"
			   else "Non Coastal" end as Coastal_Group
from in.policy_info as p 
;quit;

proc sql;
	create table out.Coastal_Modifier as 
	select 
		  p.casenumber
		, p.policynumber
		, p.distancetoshore
		, p.Coastal_Group
		, case when f.Hurricane =. then 0 else f.Hurricane end as Coastal_Rate
		, calculated coastal_rate * CoverageA / 1000 as Coastal_Premium
	from work.Coastal_Group p left join fac.'301#A Coastal Modifier$'n f
	on p.Coastal_Group = f.'Rate Per $1,000 of Coverage A'n
;quit;
%mend;
%coastal();
