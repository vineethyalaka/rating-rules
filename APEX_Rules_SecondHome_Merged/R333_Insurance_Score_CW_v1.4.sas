/*2018/11/01 TM change from hard code to read in table
Assumption: hit = 1, no hit = 0, no score = -1; model 2 is TU model, model 3 is TR model*/
/*Wanwan: change no hit = 0, no score = -1*/
/*Two score models*/

data work.R333;
	set fac.'333# Insurance Risk Score$'n;
	if fire =. then delete;

	/* Parse score range to 2 columns, one score model take in F1, 2 score models take in F1, F3*/
	array parsed_tu(2) tu1-tu2;
	i=1;
	do while(scan(F1,i)ne"");
	parsed_tu(i) = scan(F1,i);
	i+1;
	end;

	array parsed_tr(2) tr1-tr2;
	i=1;
	do while(scan(F3,i)ne"");
	parsed_tr(i) = scan(F3,i);
	i+1;
	end;

	drop i;
	rename tu1=TU_low tu2=TU_high tr1=TR_low tr2=TR_high;
run;

/*change max to 9999, min to -100, no hit = 0, no score = -1  */
proc sql;
	update R333
	     set TU_high = 9999
		 where F1 like '%Above';

	update R333
	     set TR_high = 9999
		 where F3 like '%Above';

	select min(TU_low), min(TR_low)
 		into :mintu, :mintr
        from R333;

	update R333
	     set TU_high = &mintu, TU_low = -100
		 where F1 like '%Below';
		 
	update R333
	     set TR_high = &mintr, TR_low = -100
		 where F3 like '%Below';

	update R333
	     set TU_high = 0, TU_low = 0, TR_high =0, TR_low = 0
		 where F1 = 'No Score';

	update R333
	     set TU_high =-1, TU_low = -1, TR_high =-1, TR_low = -1
		 where F1 = 'No Hit';
quit;

%macro insscore();
proc sql;
create table out.ins_score_fac as
   select p.casenumber, p.policynumber, p.insscore, 
	      p.policytermyears, p.policyformnumber, p.seqno, p.scoremodel,p.matchcode, 
		  f.F5 as ins_tier, f.F5 as ins_tier_n
		  , f.fire as fire_ins_score
		  , f.theft as theft_ins_score
		  , f.'Water Non-Weather'n as waternw_ins_score
		  , f.'All Other Non-Weather'n as othernw_ins_score
		  , f.lightning as lightning_ins_score
		  , f.'Water Weather'n as waterw_ins_score
		  , f.wind as wind_ins_score
		  , %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_ins_score
		  , f.'Cov C - Fire'n as Cov_C_Fire_ins_score
	 	  , f.'Cov C - EC'n as Cov_C_EC_ins_score
		  , f.'Property Damage due to Burglary'n as PD_Burg_ins_score
		  , f.'Liability - Bodily injury (Perso'n as liabipi_ins_score
		  , f.'Liability - Bodily injury (Medic'n as liabimp_ins_score
		  , f.'Liability - Property Damage to O'n as liapd_ins_score		
		  , f.'Water Back-up'n as waterbu_ins_score
		  , f.F21 as Lim_Theft_ins_score
   from  in.drc_info p, r333 f
   where (p.matchcode = f.TU_high)
   or (p.insscore between f.TU_low and f.TU_high and p.scoremodel = 2 and p.matchcode not in(0,-1))
   or (p.insscore between f.TR_low and f.TR_high and p.scoremodel = 3 and p.matchcode not in(0,-1));

   	create table ins_tier as  /**also used in rules 353, 354, step 7*/
	      select i.casenumber, i.policynumber, i.insscore, 
	        	 i.policytermyears, i.policyformnumber, i.seqno, i.scoremodel,
				 i.matchcode, i.ins_tier_n, i.ins_tier
	      from out.ins_score_fac i;*/;
   quit;

%if &Process = 0 %then %do;
	title 'RULE 333. INSURANCE RISK SCORE';
	proc print data=out.ins_score_fac;
	where fire_ins_score*theft_ins_score*waternw_ins_score*othernw_ins_score*lightning_ins_score*waterw_ins_score
	*wind_ins_score*hurr_ins_score*Cov_C_Fire_ins_score*Cov_C_EC_ins_score*PD_Burg_ins_score
	*liabipi_ins_score*liabimp_ins_score*liapd_ins_score*waterbu_ins_score*Lim_Theft_ins_score = .;
	run;
	proc gplot data=out.ins_score_fac;
		plot (fire_ins_score theft_ins_score waternw_ins_score othernw_ins_score lightning_ins_score waterw_ins_score
	 		wind_ins_score hurr_ins_score Cov_C_Fire_ins_score Cov_C_EC_ins_score PD_Burg_ins_score
			liabipi_ins_score liabimp_ins_score liapd_ins_score waterbu_ins_score Lim_Theft_ins_score)*ins_tier_n 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.ins_score_fac;
		histogram ins_tier_n;
	run;
%end;
%mend;
%insscore();
 

