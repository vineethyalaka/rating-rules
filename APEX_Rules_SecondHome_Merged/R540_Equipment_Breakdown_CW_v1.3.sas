/*ITR 15050 VR - This version has been created to increase the processing speed of the program.
				Rate sheet has many empty rows and columns. It has been cleaned before using.*/

data r540_sheet_manipulation;
	set fac.'540# Equipment Breakdown$'n(drop=f2 f5--f9);
	where f4 is not missing;
run;

data equip_break_fac;
	set r540_sheet_manipulation;
	f5 = lag(f3);
	if f1=. and f4 ne . then do;
		f1=f5+1;
		f3=9999999;
	end;
	rename f1 = CovA_L f3 = CovA_H f4 = Rate;
	drop f5;
run;

proc sql;
	create table out.equipment_breakdown_fac as
		select c.casenumber, c.policynumber, c.coveragea, c.HO_0456_FLAG, c.HO_04_56_pct, c.CoverageA_adj, p.Equipment_Breakdown_Flag
		  , case when p.Equipment_Breakdown_Flag = 1 then f.Rate else 0 end as Equip_Break_Rate
		from Coveragea c
		left join in.policy_info p on c.casenumber=p.casenumber
		left join equip_break_fac f on c.CoverageA_adj <= f.CovA_H and c.CoverageA_adj >= f.CovA_L
	;
quit;