** RULE 338. Underwriting Tier **;
**=====================================================================================**
History: 
		2018 01 17 NAG  Initial draft
v1.3	2019 08 01 MTL  DNU added OH specific date filter and HS account codes
**=====================================================================================**;
%macro uwtier();
	data uw_tier;
	set in.policy_info;
		If PolicyIssueDate < '27APR2018'd then UW_Tier = 0;
		Else if PolicyAccountNumber = 14000 then UW_Tier = 2;   
		Else if PolicyAccountNumber = 14200 or PolicyAccountNumber = 14300 then UW_Tier = 6;
		Else if PolicyAccountNumber = 14310 or PolicyAccountNumber = 14400 then UW_Tier = 10;
		Else if PolicyAccountNumber = 4009 then UW_Tier = 1;
		Else if PolicyAccountNumber in (4000,4001,4002,4003,4004,4005,4006,4007,4008) then UW_Tier = 2;
		Else if PolicyAccountNumber in (1020,13200) then UW_Tier = 3;
		Else if PolicyAccountNumber in (1131,12200,12700,14900,14901,15100,30001,50000,51000) then UW_Tier = 4;
		Else if PolicyAccountNumber in (4109,4115) then UW_Tier = 5;
		Else if PolicyAccountNumber in (4100,4101,4102,4103,4104,4105,4110,4111,4112,4113) then UW_Tier = 6;
		Else if PolicyAccountNumber in (1021,13220,13221,13222,13223,14700) then UW_Tier = 7;
		Else if PolicyAccountNumber in (1130,1132,1220,10001,13224,13225,14902,14903,14904,14905,50002,50003,50006,51001) then UW_Tier = 8;
		Else if PolicyAccountNumber in (4100,4101,4102,4103,4104,4105,4110,4111,4112,4113) then UW_Tier = 9;
		Else if PolicyAccountNumber in (13240,16000) then UW_Tier = 11;
		Else if PolicyAccountNumber in (12240,50001,50004,50005) then UW_Tier = 16;
		Else UW_Tier = 0;
	run;

	data uw_tier_fac;
	set fac.'338# Underwriting Tier$'n;
	if f1=. then delete;
	run;

	proc sql;
		create table out.uw_tier_fac as
		select p.casenumber, p.policynumber, p.PolicyIssueDate, p.PolicyEffectiveDate
			,p.UW_Tier
			,f.fire as fire_uw_tier
			,f.theft as theft_uw_tier
			,f.'Water Non-Weather'n as waternw_uw_tier 
			,f.'All Other Non-Weather'n as othernw_uw_tier
			,f.lightning as lightning_uw_tier 
			,f.'Water Weather'n as Waterw_uw_tier 
			,f.Wind as wind_uw_tier
			,f.hail as hail_uw_tier 
			%if &Hurricane = 1 %then %do;  
			,f.hurricane as hurr_uw_tier
			%end;
			%else %do;
			, 1 as hurr_uw_tier
			%end;
			,f.'Cov C - Fire'n as Cov_C_Fire_uw_tier
			,f.'Cov C - EC'n as Cov_C_EC_uw_tier
			,f.'Property Damage due to Burglary'n as PD_Burg_uw_tier
			,f.'Liability - Bodily Injury (Perso'n as Liab_BI_uw_tier
			,f.'Liability - Bodily Injury (Medic'n as Liab_MedPay_uw_tier
			,f.'Liability - Property Damage to O'n as Liab_PD_uw_tier
			,f.'Water Back-up'n as WBU_uw_tier
			,f.'Limited Theft Coverage'n as Lim_Theft_uw_tier
			/*,f.'Expense Fee'n as Exp_Fee_uw_tier*/
		from uw_tier p left join uw_tier_fac f
		on p.UW_Tier = f.F1;
	quit;
%mend;
%uwtier();