** RULE 528. HOME BUSINESS PROPERTY **;

**=====================================================================================**
History: 2015 04 13 JT  1.0 Initial draft
		 2015 06 03 SY  1.1 Added Hurricane peril indicator
		 2020 02 12 VR	ITR 15050 CO Mosaic - Added wildfire disc base premium
**=====================================================================================**;

%macro HBP();

data r528property;
set fac.'528# Business Property$'n;
if strip (f2) = . then delete;
rename 'Home Business Property Factor'n = Business_Type;
rename 'Fire'n = Fire_Home_Bus;
rename 'Theft'n = Theft_Home_Bus;
rename 'Water Non-Weather'n = WaterNW_Home_Bus;
rename 'All Other Non-Weather'n = OtherNW_Home_Bus;
rename 'Lightning'n = Lighting_Home_Bus;
rename 'Water Weather'n = WaterW_Home_Bus;
rename 'Wind'n = Wind_Home_Bus;
rename 'Hail'n = Hail_Home_Bus;
if &Hurricane = 1 then Hurricane = Hurricane; else Hurricane = 0;
rename Hurricane = Hurr_Home_Bus;
rename Wildfire = WF_Home_Bus;
run;

data r528endorsement;
set fac.'528# Business End Property$'n;
if strip (f2) = . then delete;
rename 'Home Business Endorsement Factor'n = Business_Type_End;
rename f6 = Home_Bus_512_Fac;
rename f7 = Home_Bus_514_Fac;
rename f8 = Home_Bus_515A_Fac;
run;

%if &Existing = 0 %then %do;

proc sql;
	create table out.Home_Business_Property as
	select e.CaseNumber, e.PolicyNumber, e.Home_Bus_Flag, e.Home_Bus_Type, e.Home_Bus_Receipt, e.Home_Bus_Visits
		, a.fire_disc_base_premium
		, a.theft_disc_base_premium
		, a.waternw_disc_base_premium
		, a.othernw_disc_base_premium
		, a.lightning_disc_base_premium
		, a.waterw_disc_base_premium
		, a.wind_disc_base_premium
		, a.hail_disc_base_premium
		, a.WF_disc_base_premium
/*		, a.hurr_disc_base_premium*/
		, b.*
		, c.Other_Struc_base_premium_nr
		, d.incr_covc_base_premium_nr
		, f.loss_of_use_rate_nr
		, g.*
		, case when e.Home_Bus_Flag = 0  or e.PolicyFormNumber = 9 then 0
	      	 else 	round(a.fire_disc_base_premium*b.Fire_Home_Bus
			 	+	a.theft_disc_base_premium*b.Theft_Home_Bus
				+	a.waternw_disc_base_premium*b.WaterNW_Home_Bus
				+	a.othernw_disc_base_premium*b.OtherNW_Home_Bus
				+	a.lightning_disc_base_premium*b.Lighting_Home_Bus
				+	a.waterw_disc_base_premium*b.WaterW_Home_Bus
				+	a.wind_disc_base_premium*b.Wind_Home_Bus
				+	a.hail_disc_base_premium*b.Hail_Home_Bus
/*				+	a.hurr_disc_base_premium*b.Hurr_Home_Bus*/
				+	a.WF_disc_base_premium*b.WF_Home_Bus
				+	c.Other_Struc_base_premium_nr*g.Home_Bus_514_Fac
				+	d.incr_covc_base_premium_nr*g.Home_Bus_515A_Fac
				+	f.loss_of_use_rate_nr*g.Home_Bus_512_Fac,1.0
) end as Home_Business_Prop_Rate

	from in.Endorsement_info e
	left join out.step2_base_premium a 			on e.casenumber = a.casenumber
	left join r528property b 					on (e.Home_Bus_Type = b.Business_Type 
												and e.Home_Bus_Receipt >= b.f2 
												and e.Home_Bus_Receipt <= b.f3
												and e.Home_Bus_Visits >=  b.f4
												and e.Home_Bus_Visits <= b.f5)
	left join out.Other_Struc_base_premium c 	on e.casenumber = c.casenumber
	left join out.incr_covc_base_premium d		on e.casenumber = d.casenumber
	left join out.loss_of_use_rate f			on e.casenumber = f.casenumber
	left join r528endorsement g					on (e.Home_Bus_Type = g.Business_Type_End 
												and e.Home_Bus_Receipt >= g.f2 
												and e.Home_Bus_Receipt <= g.f3
												and e.Home_Bus_Visits >=  g.f4
												and e.Home_Bus_Visits <= g.f5)
;
quit;
run;

%end;

%if &Existing = 1 %then %do;


proc sql;
	create table out.Home_Business_Property as
	select e.CaseNumber, e.PolicyNumber
/*		, e.Home_Bus_Flag*/
/*		, e.Home_Bus_Type*/
/*		, e.Home_Bus_Receipt*/
/*		, e.Home_Bus_Visits*/
		, a.fire_disc_base_premium
		, a.theft_disc_base_premium
		, a.waternw_disc_base_premium
		, a.othernw_disc_base_premium
		, a.lightning_disc_base_premium
		, a.waterw_disc_base_premium
		, a.wind_disc_base_premium
		, a.hail_disc_base_premium
		, a.WF_disc_base_premium
/*		, b.**/
		, c.Other_Struc_base_premium_nr
		, d.incr_covc_base_premium_nr
		, f.loss_of_use_rate_nr
/*		, g.**/
/*		, case when e.Home_Bus_Flag = 0  then 0*/
/*	      	 else 	round(a.fire_disc_base_premium*b.Fire_Home_Bus*/
/*			 	+	a.theft_disc_base_premium*b.Theft_Home_Bus*/
/*				+	a.waternw_disc_base_premium*b.WaterNW_Home_Bus*/
/*				+	a.othernw_disc_base_premium*b.OtherNW_Home_Bus*/
/*				+	a.lightning_disc_base_premium*b.Lighting_Home_Bus*/
/*				+	a.waterw_disc_base_premium*b.WaterW_Home_Bus*/
/*				+	a.wind_disc_base_premium*b.Wind_Home_Bus*/
/*				+	a.hail_disc_base_premium*b.Hail_Home_Bus*/
/*				+	c.Other_Struc_base_premium_nr*g.Home_Bus_514_Fac*/
/*				+	d.incr_covc_base_premium_nr*g.Home_Bus_515A_Fac*/
/*				+	f.loss_of_use_rate_nr*g.Home_Bus_512_Fac,1.0) end as Home_Business_Prop_Rate*/
		, case when &Existing.=1 or e.PolicyFormNumber = 9 then 0 end as Home_Business_Prop_Rate

	from in.Endorsement_info e
	left join out.step2_base_premium a 			on e.casenumber = a.casenumber
/*	left join r528property b 					on (e.Home_Bus_Type = b.Business_Type */
/*												and e.Home_Bus_Receipt >= b.f2 */
/*												and e.Home_Bus_Receipt <= b.f3*/
/*												and e.Home_Bus_Visits >=  b.f4*/
/*												and e.Home_Bus_Visits <= b.f5)*/
	left join out.Other_Struc_base_premium c 	on e.casenumber = c.casenumber
	left join out.incr_covc_base_premium d		on e.casenumber = d.casenumber
	left join out.loss_of_use_rate f			on e.casenumber = f.casenumber
/*	left join r528endorsement g					on (e.Home_Bus_Type = g.Business_Type_End */
/*												and e.Home_Bus_Receipt >= g.f2 */
/*												and e.Home_Bus_Receipt <= g.f3*/
/*												and e.Home_Bus_Visits >=  g.f4*/
/*												and e.Home_Bus_Visits <= g.f5)*/
;
quit;
run;

%end;




%if &Process = 0 %then %do;
	title 'RULE 528. HOME BUSINESS PROPERTY';
	proc freq data=out.Home_Business_Property;
	tables Home_Business_Prop_Rate / list missing;
	run;
%end;
%mend;
%HBP();
