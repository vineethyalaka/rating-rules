** RULE 529. LIMITED FUNGI COVERAGE **;
** 2017 09 15 AWH Allow rates to vary by form, dwelling type;
** 2017 09 22 AWH Introduce base rates for HF9;
** Limited Fungi Function changed by Di to accomodate NJ special endorseents;


*create two limit fungi temp datasets for holding factors;
data r529_I r529_II;
length Note $10;
set fac.'529# Limited Fungi$'n;
select (upcase(coverage)); 
when ("Basic Limits") do; Section_I_Limit=10000;Note="Basic";keep Section_I_Limit HO3 'HF9-A'n 'HF9-B'n 'HF9-C'n Note; output r529_I; end;
when ("INCREASED LIMITS SECTION I - PROPERTY") do; Section_I_Limit=Limit; Note="Increased"; keep Section_I_Limit HO3 'HF9-A'n 'HF9-B'n 'HF9-C'n Note; output r529_I; end;
when ("INCREASED LIMITS SECTION II - LIABILITY") do; Section_II_Limit=Limit; Note="Increased"; keep Section_II_Limit HO3 'HF9-A'n 'HF9-B'n 'HF9-C'n; output r529_II; end;
otherwise;
end;
run;

/*proc sql; */
/*create table out.Limited_Fungi_Rate as*/
/*select p.policynumber*/
/*, p.casenumber*/
/*, p.HO_04_32*/
/*, p.Property_Amount*/
/*, p.Liability_Amount*/
/*, case */
/*		when p.HO_04_32="1" then case when p.PolicyFormNumber=3 then sum(round(f1.'HO3'n,1.0),round(f2.'HO3'n,1.0))*/
/*									  else case when i.Secondary_Home_Flag = 0 then sum(round(f1.'HF9-A'n,1.0),round(f2.'HF9-A'n,1.0))*/
/*									  	        when i.Secondary_Home_Flag = 1 then sum(round(f1.'HF9-B'n,1.0),round(f2.'HF9-B'n,1.0))	*/
/*												when i.Secondary_Home_Flag = 2 then sum(round(f1.'HF9-C'n,1.0),round(f2.'HF9-C'n,1.0))*/
/*												else 0 */
/*											end*/
/*									   end*/
/*		 else 0*/
/*  end as Limited_Fungi_Rate*/
/*from in.PRODUCTION_INFO as p*/
/*inner join in.policy_info as i on p.casenumber=i.casenumber*/
/*left join r529_I as f1 on p.Property_Amount=f1.Section_I_Limit*/
/*left join r529_II as f2 on p.Property_Amount=f1.Section_II_Limit;*/
/*quit;*/

proc sql; 
create table out.Limited_Fungi_Rate as
select p.policynumber
, p.casenumber
, p.HO_04_32
, p.Property_Amount
, p.Liability_Amount
, case when p.HO_04_32="1" then case when p.PolicyFormNumber=3 then round(f3.'HO3'n,1.0)
									 else case when i.Secondary_Home_Flag = 0 then round(f3.'HF9-A'n,1.0)
									  	       when i.Secondary_Home_Flag = 1 then round(f3.'HF9-B'n,1.0)	
											   when i.Secondary_Home_Flag = 2 then round(f3.'HF9-C'n,1.0)
											   else .
										  end
									  end
  else 0
 end as Basic_Fungi_Rate

, case 
		when p.HO_04_32="1" and f1.Note ne "Basic" then case when p.PolicyFormNumber=3 then sum(round(f1.'HO3'n,1.0),round(f2.'HO3'n,1.0))
									  						 else case when i.Secondary_Home_Flag = 0 then sum(round(f1.'HF9-A'n,1.0),round(f2.'HF9-A'n,1.0))
									  	        				  when i.Secondary_Home_Flag = 1 then sum(round(f1.'HF9-B'n,1.0),round(f2.'HF9-B'n,1.0))	
																  when i.Secondary_Home_Flag = 2 then sum(round(f1.'HF9-C'n,1.0),round(f2.'HF9-C'n,1.0))
																  else 0 
															 end
									                    end

		 when p.HO_04_32="1" and f1.Note eq "Basic" then case when p.PolicyFormNumber=3 then round(f2.'HO3'n,1.0)
									  						  else case when i.Secondary_Home_Flag = 0 then round(f2.'HF9-A'n,1.0)
									  	        				        when i.Secondary_Home_Flag = 1 then round(f2.'HF9-B'n,1.0)	
																        when i.Secondary_Home_Flag = 2 then round(f2.'HF9-C'n,1.0)
																        else 0 
															       end
									                          end		 												      
		 else 0
  end as Increased_Fungi_Rate

, sum(calculated Basic_Fungi_Rate, calculated Increased_Fungi_Rate) as Limited_Fungi_Rate

from in.PRODUCTION_INFO as p   
cross join (select * from r529_I(obs=1)) as f3
inner join in.policy_info as i on p.casenumber=i.casenumber
left join r529_I as f1 on p.Property_Amount=f1.Section_I_Limit
left join r529_II as f2 on p.Liability_Amount=f2.Section_II_Limit;
quit;






