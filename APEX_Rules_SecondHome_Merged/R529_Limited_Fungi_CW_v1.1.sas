** RULE 529. LIMITED FUNGI COVERAGE **;

**Version History:
 	12 29 2016 	XX	Initial Draft
	09 26 2017	JL	Corrected code to replace Special Computer Coverage flag with Limited Fungi flag and to include rates for HF9-A and HF9-B;

%macro sc();
	data r529;
	set fac.'529# Limited Fungi$'n;
	if strip(f1) = '' then delete;
	run;
	proc sql;
		create table out.Limited_Fungi_Rate as
		select 
		  p1.CaseNumber
		, p1.PolicyNumber
		, p1.PolicyFormNumber
		, p1.Secondary_Home_Flag
		, input(p2.HO_04_32,1.) as HO_04_32
		, case when p2.HO_04_32 = '1' and p1.PolicyFormNumber = 9 then
			(case when p1.Secondary_Home_Flag = 1 then round(f.'HF9-A'n,1.0)
		     	when p1.Secondary_Home_Flag = 2 then round(f.'HF9-B'n,1.0)
			 	when p1.Secondary_Home_Flag = 3 then round(f.'HF9-C'n,1.0)
			 	else 0 end)
			 else 0 end as Limited_Fungi_Rate
		from in.Policy_Info p1 
			join in.Production_Info p2 on p1.CaseNumber = p2.CaseNumber
			, r529 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 529. LIMITED FUNGI COVERAGE';
	proc freq data=out.Limited_Fungi_Rate;
	tables HO_04_32*Limited_Fungi_Rate / list missing;
	run;
%end;
%mend;
%sc();