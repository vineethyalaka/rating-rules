** RULE 406. Mitigation Features **;
**=====================================================================================**
History: 2016 07 15 KK Initial Draft
**=====================================================================================**;
%macro MF();

    data mitigation_fac;
		set fac.'406#F Mitigation Features$'n;
		if wind = . then delete;
	run;
	

	proc sql;
	create table out.mitigation_info as
	select p1.casenumber, p1.policynumber, 
			case p1.construction_completed when 100 then 'Reinforced Concrete Roof Deck'
										   when 200 then 'Wood'
										   when 300 then 'All Other'
				end as RoofCovering, 
			case p1.comp_window_door_protection when 1 then '2.5" nail / 12" in field of plywood' 
												when 2 then '2.5" nail / 6" in field of plywood' 
												when 3 then '2" nail / 12" in field of plywood' 
				end as RoofAttachment, 
		    case p1.comp_engineered_status when 1 then 'Clips'
										   when 2 then 'Single Wraps'
										   when 3 then 'Double Wraps'
										   when 4 then 'Toe Nails'
				end as RoofToWallConnection, 
			case p1.comp_roof_equipment when 1 then 'Hurricane Protection'
										when 2 then 'Intermediate'
										else 'None'
				end as Shutters, 
			case p1.comp_roof_wall_anchor when 1 then 'SWR'
										  else 'None'
				end as SecWaterResistance, 
			case p1.question_code_20 when 1 then 'Hip'
									 else 'Other'
				end as RoofShape, 
			case p1.ml_70_number_of_families_01 when 1 then 'Wall-to-Foundation Restraint'
											 else 'No Wall-to-Foundation Restraint'
				end as WallToFoundationRestraint
	from in.production_info p1;
	quit;

	proc sql;
		create table out.mitigation_fac as
		select p.*
		, case when f1.wind = . then 1
			   else f1.wind 
		  end as wind_roof_covering
		, case when f1.hurricane = . then 1
			   else f1.hurricane 
		  end as hurricane_roof_covering
		, case when f2.wind = . then 1
			   else f2.wind 
		  end as wind_wall_restraint
		, case when f2.hurricane = . then 1
			   else f2.hurricane 
		  end as hurricane_wall_restraint
		, case when f3.wind = . then 1
			   else f3.wind 
		  end as wind_roof_attachment
		, case when f3.hurricane = . then 1
			   else f3.hurricane 
		  end as hurricane_roof_attachment
		, case when f4.wind = . then 1
			   else f4.wind 
		  end as wind_roof_connection
		, case when f4.hurricane = . then 1
			   else f4.hurricane 
		  end as hurricane_roof_connection
		, case when f5.wind = . then 1
		       else f5.wind 
		  end as wind_shutters
		, case when f5.hurricane = . then 1
			   else f5.hurricane 
		  end as hurricane_shutters
		, case when f6.wind = . then 1
		 	   else f6.wind 
		  end as wind_roof_shape
		, case when f6.hurricane = . then 1
			   else f6.hurricane 
		  end as hurricane_roof_shape
		, case when f7.wind = . then 1
			   else f7.wind 
		  end as wind_sec_water_resistance
		, case when f7.hurricane = . then 1
			   else f7.hurricane 
		  end as hurricane_sec_water_resistance
		, round(calculated wind_roof_covering * calculated wind_wall_restraint * calculated wind_roof_attachment * calculated wind_roof_connection * calculated wind_shutters * calculated wind_roof_shape * calculated wind_sec_water_resistance, 0.001) as wind_mitigation
		, round(calculated hurricane_roof_covering * calculated hurricane_wall_restraint * calculated hurricane_roof_attachment * calculated hurricane_roof_connection * calculated hurricane_shutters * calculated hurricane_roof_shape * calculated hurricane_sec_water_resistance, 0.001) as hurr_mitigation
		from out.mitigation_info p
			left join mitigation_fac f1 on f1.F1 = 'Roof Covering'
									   and p.RoofCovering = f1.F2
			left join mitigation_fac f2 on f2.F1 = 'Wall-to-Foundation Restraint'
									   and p.WallToFoundationRestraint = f2.F2
			left join mitigation_fac f3 on f3.F1 = 'Roof Attachment'
									   and p.RoofAttachment = f3.F2
			left join mitigation_fac f4 on f4.F1 = 'Roof-to-Wall Connection'
									   and p.RoofToWallConnection = f4.F2
			left join mitigation_fac f5 on f5.F1 = 'Shutters'
									   and p.Shutters = f5.F2
			left join mitigation_fac f6 on f6.F1 = 'Roof Shape'
									   and p.RoofShape = f6.F2
			left join mitigation_fac f7 on f7.F1 = 'Secondary Water Resistance'
									   and p.SecWaterResistance = f7.F2
		;
	quit;

%mend;
%MF();
