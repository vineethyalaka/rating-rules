** RULE 301.A zipcode **;
/*Add P4 All Other Non-Weather, P6 Water Weather*/

/* ITR 14809 VR - Change f1 column from rate sheet to char data type to match it with column from policy_info*/
/* ITR 14827 DG - Remove P16 Water Back-up*/

%macro zipcode();
	proc sql;
		create table out.zipcode_fac as
		select p.casenumber, p.policynumber, p.PropertyZip5
            , f.Fire as fire_zipcode
			, f.Theft as theft_zipcode
			, f.'Water Non-Weather'n as waternw_zipcode
			, f.'All Other Non-Weather'n as othernw_zipcode  
			, f.Lightning as lightning_zipcode
			, f.'Water Weather'n as waterw_zipcode
			, f.Wind as wind_zipcode
			, f.hail as hail_zipcode
			, f.'Cov C - Fire'n as Cov_C_Fire_zipcode
			, f.'Cov C - EC'n as Cov_C_EC_zipcode
			, f.'Property Damage due to Burglary'n as PD_Burglary_zipcode
			, f.'Liability - Bodily injury (Perso'n as liabipi_zipcode
			, f.'Liability - Bodily injury (Medic'n as liabimp_zipcode
			, f.'Liability - Property Damage to O'n as liapd_zipcode
/*			, f.'Water Back-up'n as waterbu_zipcode*/
			, f.'Limited Theft Coverage'n as Limited_Theft_zipcode
		from in.policy_info p left join fac.'301#A Zip Code$'n f 
			on p.PropertyZip5 = put(f.F1, 5.);
	quit;

;
%mend;
%zipcode();
