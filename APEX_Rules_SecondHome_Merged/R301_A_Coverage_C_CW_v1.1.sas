%macro CovC();
	data coveragec; *also used in rule 515;
	set in.policy_info (keep=casenumber policynumber PolicyFormNumber CoverageA CoverageC PartnerFlag PropertyUnitsInBuilding);
	CovC_pctA = CoverageC/CoverageA; 
	if PolicyFormNumber = 3 and PropertyUnitsInBuilding in (1,2) then Base_CovC = 0.5*CoverageA;
	else if PolicyFormNumber = 3 and PropertyUnitsInBuilding = 3 then Base_CovC = 0.3*CoverageA;
	else if PolicyFormNumber = 3 and PropertyUnitsInBuilding = 4 then Base_CovC = 0.25*CoverageA;
	else if PolicyFormNumber = 9 then Base_CovC = input(substr(PartnerFlag,46,3),3.)/100*CoverageA;
	if CoverageC > Base_CovC then Incr_CovC = CoverageC - Base_CovC;
	else Incr_CovC = 0;
	if CoverageC < Base_CovC then Decr_CovC = Base_CovC - CoverageC;
	else Decr_CovC = 0;
	format CovC_pctA percent8.1;
	run;
	data r301CovC;
	set fac.'301#A Coverage C$'n;
	array num_fac{*} _numeric_;
	if f1=. and _n_=1 then 
	do i=1 to dim(num_fac);
	num_fac{i}=0;
	end;
	drop i;
	run;

	data coveragec_aoi; 
	set r301CovC;
	if f1 = . and f2 = . then delete;
	aoi_c_L = lag(f1); rename f1 = aoi_c_H;
	array peril (6) Theft 'Cov C - Fire'n 'Cov C - EC'n 'Limited Theft Coverage - A'n 'Limited Theft Coverage - B'n 'Limited Theft Coverage - C'n;
	array fac_l (6) Theft_l Cov_C_Fire_l Cov_C_EC_l Lim_Theft_A_l Lim_Theft_B_l Lim_Theft_C_l;
	array fac_h (6) Theft_h Cov_C_Fire_h Cov_C_EC_h Lim_Theft_A_h Lim_Theft_B_h Lim_Theft_C_h;
	do i = 1 to 6;
		fac_l(i) = lag(peril(i));
		fac_h(i) = peril(i);
	end;
	if aoi_c_L = . then delete;
	drop i;
	run;
	proc sql;
		create table coverage_c as
		select p.*, f.aoi_c_L, f.aoi_c_H, 
			f.Theft_l, Theft_h,
			f.Cov_C_Fire_l, f.Cov_C_Fire_h,
			f.Cov_C_EC_l, f.Cov_C_EC_h,
			f.Lim_Theft_A_l, f.Lim_Theft_A_h,
			f.Lim_Theft_B_l, f.Lim_Theft_B_h,
			f.Lim_Theft_C_l, f.Lim_Theft_C_h

		from coveragec p 
			left join coveragec_aoi f on (p.CoverageC <= f.aoi_c_H or f.aoi_c_H = .) and p.CoverageC > f.aoi_c_L
		;
	quit;

		proc sql;
		create table coverage_c_2 as
		select p.*, f.aoi_c_L, f.aoi_c_H, 
			f.Theft_l, Theft_h,
			f.Cov_C_Fire_l, f.Cov_C_Fire_h,
			f.Cov_C_EC_l, f.Cov_C_EC_h,
			f.Lim_Theft_A_l, f.Lim_Theft_A_h,
			f.Lim_Theft_B_l, f.Lim_Theft_B_h,
			f.Lim_Theft_C_l, f.Lim_Theft_C_h

		from coveragec p 
			left join coveragec_aoi f on (p.Base_CovC <= f.aoi_c_H or f.aoi_c_H = .) and p.Base_CovC > f.aoi_c_L
		;
	quit;

	data out.coverage_c_fac; set coverage_c;
	array fac_l (6) Theft_l Cov_C_Fire_l Cov_C_EC_l Lim_Theft_A_l Lim_Theft_B_l Lim_Theft_C_l;
	array fac_h (6) Theft_h Cov_C_Fire_h Cov_C_EC_h Lim_Theft_A_h Lim_Theft_B_h Lim_Theft_C_h;
	array fac   (6) Theft_covc Cov_C_Fire_covc Cov_C_EC_covc Lim_Theft_A_covc Lim_Theft_B_covc Lim_Theft_C_covc;
	do i = 1 to 6;
		if aoi_c_H = . then fac(i) = round(fac_h(i)*(CoverageC - aoi_c_L)/100000 + fac_l(i),.0001);
		else fac(i) = round((fac_h(i) - fac_l(i))*(CoverageC - aoi_c_L)/(aoi_c_H - aoi_c_L) + fac_l(i),.0001);
	end;
	drop i;
	run;


	data out.coverage_c_base_fac; set coverage_c_2;
	array fac_l (6) Theft_l Cov_C_Fire_l Cov_C_EC_l Lim_Theft_A_l Lim_Theft_B_l Lim_Theft_C_l;
	array fac_h (6) Theft_h Cov_C_Fire_h Cov_C_EC_h Lim_Theft_A_h Lim_Theft_B_h Lim_Theft_C_h;
	array facbase   (6) Theft_covcbase Cov_C_Fire_covcbase Cov_C_EC_covcbase Lim_Theft_A_covcbase Lim_Theft_B_covcbase Lim_Theft_C_covcbase;
	do i = 1 to 6;
		if aoi_c_H = . then facbase(i) = round(fac_h(i)*(Base_CovC - aoi_c_L)/100000 + fac_l(i),.0001);
		else facbase(i) = round((fac_h(i) - fac_l(i))*(Base_CovC - aoi_c_L)/(aoi_c_H - aoi_c_L) + fac_l(i),.0001);
	end;
	drop i;
	run;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 301.A COVERAGE C';
	proc print data=out.coverage_c_fac;
	where theft_covc = .;
	run;
	proc gplot data=out.coverage_c_fac;
		plot theft_covc*CoverageC;
	run;
	quit; 
	proc print data=out.coverage_c_base_fac;
	where theft_covcbase = .;
	run;
	proc gplot data=out.coverage_c_base_fac;
		plot theft_covcbase*Base_CovC;
	run;
	quit; 
%end;

%mend;
%CovC();
