/*
   ----------------------------------------------------------------------------------------------------------------------
1. ITR 14678 CT Mosaic VR Initial file

   ----------------------------------------------------------------------------------------------------------------------
*/

data r386_sheet_manipulation;
	retain min_dist max_dist;
	set fac.'386# fire department distance$'n;
	where f4 is not missing;
	keep min_dist max_dist f4;
	min_dist = input(f1, 13.2);
	length max_dist 8;
	if Lightning = "up" then max_dist = 1000000000;
	else max_dist = input(Lightning, 13.2);
	rename f4=p5;
run;


proc sql;
	select min_dist 
		into :neutral_fac_dist 
	from r386_sheet_manipulation 
	where p5 = 1;

	select max(min_dist) 
		into :last_row_dist 
	from r386_sheet_manipulation;

quit;

data r386_policy_info_manipulation;
	set in.policy_info;
	length FS1DriveDistance_Updated 8;
	if FS1DriveDistance = . then FS1DriveDistance_Updated = &neutral_fac_dist; *this gives factor of 1;
	else if FS1DriveDistance = -999999 then FS1DriveDistance_Updated = &last_row_dist; *this gives factor from last row;
	else FS1DriveDistance_Updated = FS1DriveDistance;
	FS1DriveDistance_Updated = round(FS1DriveDistance_Updated, 0.01);
run;

proc sql;
	create table out.r386_fac as
		select 
		p.*,
		s.p5 as lightning_fs1_drivedistance
		from r386_policy_info_manipulation p
		left join r386_sheet_manipulation s
			on p.FS1DriveDistance_Updated >= s.min_dist and p.FS1DriveDistance_Updated <= s.max_dist;
quit;
