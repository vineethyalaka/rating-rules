** RULE 603. RESIDENCE EMPLOYEES **;
%macro re();
data r603fac;
set fac.'603# Residence Employees$'n;
if f1="" then delete;
fac1='Liability - Bodily injury (Perso'n*1;
fac2='Liability - Bodily injury (Medic'n*1;
run;
proc sql;
    create table out.lia_ResEmployee_fac as
	select p.casenumber, p.policynumber, p.DomesticEmployeesCount
		, case when p.DomesticEmployeesCount <= 2 then 0 else p.DomesticEmployeesCount - 2 end as ResEmployee_Charge_count
		, f.fac1 * calculated ResEmployee_Charge_count as liabipi_ResEmployee
		, f.fac2 * calculated ResEmployee_Charge_count as liabimp_ResEmployee
	from r603fac f, /*in.policy_info p;*/ in.ENDORSEMENT_INFO p;
quit;

%if &Process = 0 %then %do;
title 'RULE 603. RESIDENCE EMPLOYEES';
proc freq data=out.lia_ResEmployee_fac;
tables DomesticEmployeesCount
	*liabipi_ResEmployee
	*liabimp_ResEmployee / list missing;
run;
%end;
%mend;
%re();
