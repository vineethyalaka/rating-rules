/* ----------------------------- History --------------------------------------
	1. 11-04-2019 VamsiRamineedi ITR#14678 Rate adj fac by territory.


   ----------------------------------------------------------------------------
*/

data r339_manipulation;
	set fac.'''339# rate adjustment$'''n;
	where f1 is not missing;
	keep f1 f2;
	rename f1 = territory f2 = fac;
run;

proc sql;
	create table out.rate_adj_fac as
		select 
			cf.casenumber, 
			cf.territory, 
			rm.fac
		from out.census_fac cf
			left join r339_manipulation rm
				on cf.territory = rm.territory;
quit;