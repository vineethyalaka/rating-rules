** RULE 301.A COVERAGE F **;
%macro CovF();
	proc sql;
		create table out.coverage_f_fac as
		select distinct p.casenumber, p.policynumber, p.coveragef
			, f.'Liability - Bodily injury (Medic'n as liabimp_covf
		from in.policy_info p left join fac.'301#A Coverage F$'n f
			on p.coveragef=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 301.A COVERAGE F';
	proc freq data=out.coverage_f_fac;
	tables coveragef
		*liabimp_covf / list missing;
	run;
%end;

%mend;
%CovF();