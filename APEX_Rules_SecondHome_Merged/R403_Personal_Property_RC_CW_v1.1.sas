** RULE 403. PERSONAL PROPERTY REPLACEMENT COST **;

*Second Home:
	*2017 08 31 1.1 Some rates manuals imported to SAS with column names (rather than assigned F1, F2, etc.)
					Data step added so code will work for both cases;

%macro RC(chk);
	data r403;
	set fac.'403# Personal Prop Repl Cost$'n;
	if missing('HO3'n) then 'HO3'n=input(F2,8.3);
  	if missing('HF9-P10'n) then 'HF9-P10'n=input(F3,8.3);
	if F1=' ' then delete;
	keep F1 'HO3'n 'HF9-P10'n;
	run;

	proc sql;
		create table out.r403_CovC_Repl_Cost as
		select p.CaseNumber, p.PolicyNumber, p.PolicyFormNumber, p.HO_0490_FLAG
			 , case when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ and p.PolicyFormNumber = 3 then f.'HO3'n
			when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ and p.PolicyFormNumber = 9 then f.'HF9-P10'n
			else 1 end as r403_HO_04_90_fac
		from in.Endorsement_Info p, r403 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 403. PERSONAL PROPERTY REPLACEMENT COST';
	proc freq data=out.r403_CovC_Repl_Cost_&chk;
	tables HO_0490_FLAG*r403_HO_04_90_fac*PolicyFormNumber / list missing;
	run;
%end;
%mend;
%RC();


/*compare CAP25 to CAX25*/
/*%libname (apex, HF9C_CovAdj_PPRC, 	inforce_APEX_HF9C_CovAdj_PPRC_25_50); 	%RC(CAP25);*/
/*%libname (apex, HF9C_CovAdj_xPPRC, 	inforce_APEX_HF9C_CovAdj_xPPRC_25_50); 	%RC(CAX25);*/