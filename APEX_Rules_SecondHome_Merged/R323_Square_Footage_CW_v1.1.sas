** RULE 323. SQUARE FOOTAGE **;
/*2019/06/25 TM Expand perils to all but P20 */
%macro SF();
	data sqft; set in.policy_info;
	if propertysquarefootage = 0 then propertysquarefootage = 1850;
	run;
	data sqft_fac;
	set fac.'323# Square Footage$'n;
	if f3=. and theft ne . then f3=99999;
	if f1=. and f3=. then delete;
	run;
	proc sql;
		create table out.sqft_fac as
		select p.casenumber, p.policynumber, p.propertysquarefootage
			, f.fire as fire_sqft
			, f.theft as theft_sqft
			, f.'Water Non-Weather'n as waternw_sqft
			, f.'All Other Non-Weather'n as othernw_sqft
			, f.lightning as lightning_sqft
			, f.'Water Weather'n as waterw_sqft
			, f.Wind as Wind_sqft
			, f.Hail as Hail_sqft
			, f.Hurricane as Hurr_sqft
			, f.'Cov C - Fire'n as Cov_C_Fire_sqft
			, f.'Cov C - EC'n as Cov_C_EC_sqft
			, f.'Property Damage due to Burglary'n as PD_Burg_sqft
			, f.'Liability - Bodily injury (Perso'n as liabipi_sqft
			, f.'Liability - Bodily Injury (Medic'n as liabimp_sqft
			, f.'Liability - Property Damage to O'n as liapd_sqft
			, f.'Water Back-up'n as waterbu_sqft
			, f.'Limited Theft Coverage'n as lim_theft_sqft
	
		from sqft p left join sqft_fac f
			on p.propertysquarefootage>=f.f1
			and p.propertysquarefootage<=f.f3;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 323. SQUARE FOOTAGE';
	proc print data=out.sqft_fac;
	where theft_sqft= .;
	run;
	proc freq data=out.sqft_fac;
		tables theft_sqft*propertysquarefootage*PD_Burg_sqft*Lim_Theft_sqft
			/ list missing;
	run;
	quit; 
%end;
%mend;
%SF();
