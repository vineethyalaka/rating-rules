
** RULE 406. DEDUCTIBLES **;

**=====================================================================================**
History: 2015 05 11 SY  Initial draft
		 2015 08 25 SY  Removed roof deductible inland
		 2016 03 21 DG	Do not enforce minimum deductible rules for Term 1 Bookroll policies
		 2016 11 02 TA	Removed the condition that WHDeductible = AOPDeductible always
**=====================================================================================**;


%macro Ded();
	data deductible_fac;
	set fac.'406# Deductibles$'n;
	format f1 f2 12.2;
	if f1 < 1 then f1 = f1*100;
	if f1 = . then delete;
	cova_L = lag(f2); if cova_L > f2 or cova_L = . then cova_L = 0; rename f2 = cova_H; 
	Fire_n = Fire*1; Theft_n = Theft*1; waternw_n = 'Water Non-Weather'n*1; 
	othernw_n = 'All Other Non-Weather'n*1; Lightning_n = Lightning*1; waterw_n = 'Water Weather'n*1; 
	Wind_n = Wind*1; Hail_n = Hail*1; Hurricane_n = Hurricane*1; Cov_C_Fire_n = 'Cov C - Fire'n*1;
	Cov_C_EC_n = 'Cov C - EC'n*1; PD_Burg_n = 'Property Damage due to Burglary'n*1; Lim_Theft_n = 'Limited Theft Coverage'n*1;
	array peril (13) Fire_n Theft_n waternw_n othernw_n Lightning_n waterw_n Wind_n Hail_n Hurricane_n Cov_C_Fire_n Cov_C_EC_n PD_Burg_n Lim_Theft_n;
	array fac_l (13) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l Cov_C_Fire_l Cov_C_EC_l PD_Burg_l Lim_Theft_l;
	array fac_h (13) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h Cov_C_Fire_h Cov_C_EC_h PD_Burg_h Lim_Theft_h;
	do i = 1 to 13; 
		fac_l(i) = lag(peril(i));
		fac_h(i) = peril(i);
		if cova_L = 0 then fac_l(i) = fac_h(i);
	end;
	drop i;
	run;

	proc sql noprint;
		select max(cova_H)
		into: maxdedcova
		from deductible_fac
		;
	quit;
	%put &maxdedcova;

	proc sql;
		create table work.deductible_info as
		select a.casenumber, a.policynumber, a.coveragea
			 , a.CoverageDeductible, a.WindHailDeductible, a.HurricaneDeductible
			 , b.deductible_group
			 , case	when a.policyaccountnumber in (14300,14310) and a.policytermyears = 1
					then "N"
					else "Y"
			   end length=1 as EnforceMinDeductible
		from in.policy_info a
			inner join out.grid_fac b
			on a.casenumber = b.casenumber
		;
	quit;

	data deductible;
	set work.deductible_info;
	AOPDeductible = input(CoverageDeductible,dollar8.);
	WHDeductible =  input(WindHailDeductible,8.);
	HurrDeductible =  input(HurricaneDeductible,8.);

	if AOPDeductible < 1000 and EnforceMinDeductible = "Y" then AOPDeductible = 1000; 

	if WHDeductible <= 10 then WHDeductible_amt_org = WHDeductible*coveragea/100;
	else WHDeductible_amt_org = WHDeductible;

	if EnforceMinDeductible = "Y" then do;

		if deductible_group = 1 and WHDeductible not in (5,10) then WHDeductible = 5;
		else if deductible_group = 2 and WHDeductible not in (3,4,5,10) then do;
			if coveragea < 250000 then WHDeductible = 5000;
			else WHDeductible = 2;
		end;
		else if deductible_group = 3 and WHDeductible not in (2,3,4,5,10,5000) then do;
			if coveragea < 250000 then WHDeductible = 2500;
			else WHDeductible = 1;
		end;
		else if deductible_group = 3 and WHDeductible =2 and coveragea < 125000 then WHDeductible = 2500;
		else if deductible_group = 4 and WHDeductible < AOPDeductible then WHDeductible = AOPDeductible; *Group 4, $2500 min roof deductible was removed;
		HurrDeductible = WHDeductible;

	end;
	run;




	proc sql;
		create table deductible1 as
		select p.*, f1.cova_L, f1.cova_H
			, f1.fire_l, f1.fire_h
			, f1.theft_l, f1.theft_h
			, f1.waternw_l, f1.waternw_h
			, f1.othernw_l, f1.othernw_h
			, f1.lightning_l, f1.lightning_h
			, f1.waterw_l, f1.waterw_h
			, f2.wind_l, f2.wind_h
			, f2.hail_l, f2.hail_h
			, Case when &Hurricane = 1 then f3.hurr_l else 0 end as hurr_l , Case when &Hurricane = 1 then f3.hurr_h else 0 end as hurr_h
			, f1.Cov_C_Fire_l, f1.Cov_C_Fire_h
			, f1.Cov_C_EC_l, f1.Cov_C_EC_h
			, f1.PD_Burg_l, f1.PD_Burg_h
			, f1.Lim_Theft_l, f1.Lim_Theft_h
	
		from deductible p 
			left join deductible_fac f1 on (p.Coveragea <= f1.cova_H or f1.cova_H = &maxdedcova) 
				and p.Coveragea > f1.cova_L and p.AOPDeductible = f1.F1
			left join deductible_fac f2 on (p.Coveragea <= f2.cova_H or f2.cova_H = &maxdedcova) 
				and p.Coveragea > f2.cova_L and p.WHDeductible = f2.F1
			left join deductible_fac f3 on (p.Coveragea <= f3.cova_H or f3.cova_H = &maxdedcova) 
				and p.Coveragea > f3.cova_L and p.HurrDeductible = f3.F1
		;
	quit;
	data out.ded_fac; set deductible1;
	array fac_l (13) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l Cov_C_Fire_l Cov_C_EC_l PD_Burg_l Lim_Theft_l;
	array fac_h (13) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h Cov_C_Fire_h Cov_C_EC_h PD_Burg_h Lim_Theft_h;
	array fac   (13) fire_ded theft_ded waternw_ded othernw_ded lightning_ded waterw_ded wind_ded hail_ded hurr_ded Cov_C_Fire_ded Cov_C_EC_ded PD_Burg_ded Lim_Theft_ded;
	do i = 1 to 13;
		if EnforceMinDeductible = "N" then do;
			if fac_h(i) = . then fac_h(i) = 1;
			if fac_l(i) = . then fac_l(i) = 1;	
		end;
		if coveragea > &maxdedcova or coveragea = cova_H then fac(i) = fac_h(i);
		else fac(i) = round((fac_h(i) - fac_l(i))*(coveragea - cova_L)/(cova_H - cova_L) + fac_l(i),.0001);
	end;
	drop i;
	run;

%if &Process = 0 %then %do;
	title 'RULE 406. DEDUCTIBLES';
	proc print data=out.ded_fac;
	where fire_ded*theft_ded*waternw_ded*othernw_ded*lightning_ded*waterw_ded*wind_ded*hail_ded*hurr_ded = .;
	run;
	proc sort data=out.ded_fac; by AOPDeductible; run;
	proc gplot data=out.ded_fac;
		by AOPDeductible;
		plot (fire_ded theft_ded waternw_ded othernw_ded lightning_ded waterw_ded)*coveragea 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sort data=out.ded_fac; by WHDeductible; run;
	proc gplot data=out.ded_fac;
		by WHDeductible;
		plot (wind_ded hail_ded)*coveragea 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
/*	proc sort data=out.ded_fac; by HurrDeductible; run;*/
/*	proc gplot data=out.ded_fac;*/
/*		by HurrDeductible;*/
/*		plot hurr_ded*coveragea */
/*			/ overlay legend vaxis=axis1;*/
/*	run;*/
/*	quit; */
%end;

%mend();
%ded();