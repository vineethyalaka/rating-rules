*10/16/2018: Mo added quick rate variable, KY municipality tax, KY county tax, and surcharge.  ;
	proc sql;
	create table out.step8_final_premium as
	select  p1.adjusted_subtotal_premium
			, p2.Property_Premium
			, p3.Total_Liability_Premium
			, p4.policy_expense_fee,
			surcharge,city_tax,county_tax,
            *
			, round((((round(p1.adjusted_subtotal_premium, 1)
			+ p2.Property_Premium
			+ p3.Total_Liability_Premium)
			+ p4.policy_expense_fee))
			* p9.rate_adjustment,1)
			as final_calc_prem
			, round(calculated final_calc_prem*p6.affinity,1) as Affnity_Discount
			, round(calculated final_calc_prem*p5.policy_conversion_fac,1) as Policy_Conversion_Disc
			, (calculated final_calc_prem - calculated Affnity_Discount - calculated Policy_Conversion_Disc) as discounted_final_calc_prem
			, case 
                   when calculated discounted_final_calc_prem <= p7.min_premium 
                     then p7.min_premium
				   else calculated discounted_final_calc_prem 
              end as before_tax_final_premium
		   	, calculated before_tax_final_premium + surcharge+city_tax+county_tax as final_premium
	      from out.step4_Adjusted_subtotal_premium p1
	inner join out.step5_Property_Premium	 p2 on p1.casenumber = p2.casenumber
	inner join out.step6_Liability_Premium	 p3 on p1.casenumber = p3.casenumber
	inner join out.step7_policy_expense_fee	 p4 on p1.casenumber = p4.casenumber
	inner join out.policy_conversion_fac	 p5 on p1.casenumber = p5.casenumber
	inner join out.affinity_fac				 p6 on p1.casenumber = p6.casenumber
    inner join out.surcharge_City_County_Tax p8 on p1.casenumber = p8.casenumber
	inner join out.rate_adjustment			 p9 on p1.casenumber = p9.casenumber
    cross join out.min_premium p7;
	quit;


