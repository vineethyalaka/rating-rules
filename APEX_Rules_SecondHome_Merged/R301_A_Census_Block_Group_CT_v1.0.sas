/*Joing using numeric CBG*/
/*Adding Coastal Modifiers*/
** RULE 301.A CENSUS BLOCK **;
proc sql;
	create table out.census_fac1 as
		select p.casenumber, p.policynumber, p.census_block_group_ID 
			, 
		case 
			when 0<p.distancetoshore<=2600 then "G1"
			when 2600< p.distancetoshore <=5280 then "G2"
			else "G3" 
		end 
	as CoastalGroup
		, f.f2 as Territory
		, f.Fire as fire_census
		, f.Theft as theft_census
		, f.'Water Non-Weather'n as waternw_census
		, f.Lightning as lightning_census
		, f.'Water Weather'n as waterw_census /*add to CT*/

	, f.Wind as wind_census
	, f.hail as hail_census
	, f.hurricane as hurr_census1
	, f.'Liability - Bodily injury (Perso'n as liabipi_census
	, f.'Liability - Bodily injury (Medic'n as liabimp_census
	, f.'Liability - Property Damage to O'n as liapd_census
	, f.'Water Back-up'n as waterbu_census
	from territory p left join fac.'301#A Census Block ID$'n f 
		on p.census_block_group_ID_num = f.F1;
quit;

proc sql;
	create table out.census_fac as
		select c.casenumber, c.policynumber, c.census_block_group_ID, c.Territory, c.CoastalGroup
			, c.fire_census
			, c.theft_census
			, c.waternw_census
			, c.lightning_census
			, c.waterw_census /*Add to CT*/

	, c.wind_census
	, c.hail_census
	, 
	case 
		when c.CoastalGroup = "G1" then /*round(c.hurr_census1*f.G1,0.001)*/
	c.hurr_census1*f.G1
	when c.CoastalGroup = "G2" then /*round(c.hurr_census1*f.G2,0.001)*/
	c.hurr_census1*f.G2
	else  c.hurr_census1 end as hurr_census /*round is not necessary here*/

	, c.liabipi_census
	, c.liabimp_census
	, c.liapd_census
	, c.waterbu_census
	from out.census_fac1 c left join fac.'301#A Coastal Modifier$'n f 
		on c.territory = f.F1;
quit;