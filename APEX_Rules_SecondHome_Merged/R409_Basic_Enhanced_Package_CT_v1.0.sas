
** RULE 409. BASIC AND ENHANCED PACKAGE OPTIONAL ENDORSEMENTS **;
************************NEED TO UPDATE SpecialPersonalProp********************************************************;
	proc sql;
		create table work.Step3Supplement as
		select casenumber
		, fire_base_rate * fire_census * fire_cova * fire_home_age * fire_num_res * fire_heat_sys * fire_ins_score * fire_rowhouse * fire_sec_res * fire_naogclaimxPeril * fire_claim * fire_ded * (fire_sp_personal_prop-1) as SpecialPersonalProp_Fire
		, Theft_base_rate* Theft_census * theft_covcbase * theft_cust_age * Theft_ins_score * theft_length_res * theft_num_res * theft_num_units * theft_pool * theft_sqft * theft_sec_res * theft_naogclaimxPeril * theft_claim * theft_ded * (theft_sp_personal_prop-1) as SpecialPersonalProp_Theft
		, waternw_base_rate* waternw_census * waternw_cova * waternw_fin_bsmnt * waternw_home_age * waternw_ins_score * waternw_length_res * waternw_num_res * waternw_bsmnt * waternw_pool * waternw_rcsqft * waternw_bathroom *waternw_rowhouse * waternw_sec_res * waternw_claimxPeril * waternw_claim * waternw_ded * (waternw_sp_personal_prop-1) as SpecialPersonalProp_WaterNW
		, othernw_base_rate * othernw_cova * othernw_cust_age * othernw_home_age * othernw_ins_score * othernw_pool * othernw_sec_res * othernw_naogclaim * othernw_claim * othernw_ded * (othernw_sp_personal_prop-1) as SpecialPersonalProp_OtherNW
		, lightning_base_rate * lightning_census * lightning_cova * lightning_ins_score * lightning_pool * lightning_rcsqft * (lightning_sp_personal_prop-1) as SpecialPersonalProp_Lightning
		, round(sum(calculated SpecialPersonalProp_Fire, calculated SpecialPersonalProp_Theft, calculated SpecialPersonalProp_WaterNW, calculated SpecialPersonalProp_OtherNW, calculated SpecialPersonalProp_Lightning),1.0) as SpecialPersonalProp
		from out.step2_base_premium
		;
	quit;



				proc sql;
				create table r409 as
				select p.casenumber, p.policynumber, pd.Advfactor
				,  case when pd.Advfactor = 1 then 0.1
						when pd.Advfactor = 2 then 0.15
						else 0 end as Package_Fac
				from in.policy_info p
				left join in.production_info pd on p.casenumber = pd.casenumber
				;
				quit;

			proc sql;
				create table out.package_discount as
				select p.casenumber, p.policynumber, fa.AdvFactor, fa.Package_Fac, b.SpecialPersonalProp
				, case when AdvFactor = 1 then 
							round((-(a.r401_HO_04_20_Prem
							+a.r403_HO_04_90_Prem
							+b.SpecialPersonalProp
							+c.incr_covc_base_premium
							+d.Jewelry
							+e.Refrigerated_Prop_rate
							+f.waterbu_base_premium))
							*fa.Package_Fac,1.0)
						when AdvFactor=2	then
							round((-(a.r401_HO_04_20_Prem
							+a.r403_HO_04_90_Prem
							+a.r404_Ordinance_or_Law_prem
							+b.SpecialPersonalProp
							+c.incr_covc_base_premium
							+d.Jewelry
							+e.Refrigerated_Prop_rate
							+f.waterbu_base_premium))
							*fa.Package_Fac,1.0)
						else 0 end as Package_Discount
				from in.policy_info p
				left join r409 fa 								on p.casenumber = fa.casenumber
				left join out.step4_Adjusted_Subtotal_premium a on p.casenumber = a.casenumber
				left join work.step3supplement b					on p.casenumber = b.casenumber
				left join out.incr_covc_base_premium c			on p.casenumber = c.casenumber
				left join out.USPP_rate d						on p.casenumber = d.casenumber
				left join out.Refrigerated_Property e			on p.casenumber = e.casenumber
				left join out.waterbu_base_premium f			on p.casenumber = f.casenumber
				;
			quit;