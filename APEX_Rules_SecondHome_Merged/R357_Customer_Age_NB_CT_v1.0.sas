** RULE 357. CUSTOMER AGE AT NB **;
/*Revised per Noelle's 11/24/2014 email to use age of oldest policyholder*/
				proc sort data = in.hf_info; by casenumber; run;
				data cust_age_nb;
				merge out.customer_age_fac (keep=casenumber policynumber policytermyears policyissuedate DOB1 DOB2) 
					  in.hf_info (keep=casenumber firstterm); 
				by casenumber;
				length cust_age_nb $9.;
				if &effst=0 then cust_age_nb=max(floor((policyissuedate - dob1)/365),floor((policyissuedate - dob2)/365));
				else cust_age_nb=max(floor((firstterm - dob1)/365),floor((firstterm - dob2)/365));
				if cust_age_nb>=110 then cust_age_nb='110+';
				else if cust_age_nb<18 then cust_age_nb=18;
				run;
				proc sql;
					create table out.customer_age_nb_fac as
					select distinct p.*
						, f.'Expense Fee'n as expense_customer_age_nb
					from cust_age_nb p left join fac.'357# Customer Age at NB$'n f
						on strip(p.cust_age_nb)=strip(f.f1)
						order by casenumber;
				quit;