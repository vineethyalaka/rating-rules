** RULE 530. IDENTITY FRAUD EXPENSE COVERAGE **;
				data r530;
				set fac.'530# Identity Fraud Coverage$'n;
				if strip(f1) = '' then delete;
				run;
				proc sql;
					create table out.ID_Theft_Rate as
					select p.CaseNumber, p.PolicyNumber, p.IDTheft
						, round(p.IDTheft*f.charge,1.0) as ID_Theft_Rate
					from in.Endorsement_Info p, r530 f;
				quit;