** RULE 505.D EARTHQUAKE **;
/*10190315 TM add Year Built Group, change column name to F1,F2,.. */
%macro eq();
	data r505d;
	set fac.'505#D Earthquake$'n;
	if f3 =. then delete;
	retain ConstructionType;
	if f2 ^= "" then ConstructionType = f2;
	if f1 ^= "" then YearBuiltGroup =f1;
	run;
	data coverage_d (keep=CaseNumber PolicyNumber PolicyFormNumber CoverageA CoverageD CovD_pctA Incr_CovD); set in.policy_Info;
		CovD_pctA = CoverageD/CoverageA;
		if CoverageD > CoverageA*0.3 then Incr_CovD = CoverageD - CoverageA*0.3;
		else Incr_CovD = 0;
	run;

	data YearBuiltGroup_Temp(keep=CaseNumber PolicyNumber PolicyFormNumber PropertyYearBuilt Improvement_Year YearBuiltGroup);
	set in.policy_Info;
	if Improvement_Year = . then Improvement_Year = PropertyYearBuilt;
	run;

	data YearBuiltGroup;
	set YearBuiltGroup_Temp;
	if Improvement_Year < 1981 then YearBuiltGroup ='< 1981';
	else if Improvement_Year >= 1981 then YearBuiltGroup='1981 +';
	run;


	proc sql;
		create table out.Earthquake_rate as
		select p1.casenumber, p1.policynumber, p1.PolicyFormNumber, p1.PropertyQuakeZone, p1.PropertyYearBuilt, p1.Improvement_Year, P6.YearBuiltGroup, p2.propertyconstructionclass, p2.ConstructionType
			, p3.CoverageA, Incr_CovC, Incr_CovD
			, HO_0454_FLAG, ORD_LAW_FLAG, HO_0448_FLAG, HO_0448_ADDL_LIMIT_01
			, f4, f7, f9
			, round(f4, .01)*p3.CoverageA*1.1/1000 as EQ_CovA
			, (case when ORD_LAW_FLAG = '1' then round(f4,.01) else 0 end)*p3.CoverageA*0.15/1000 as EQ_ORD_LAW
			, case when p1.PolicyFormNumber = 3 then round(f7,.01)*Incr_CovC/1000 else 0 end as EQ_IncrCovC
			, case when p1.PolicyFormNumber = 3 then round(f9,.01)*Incr_CovD/1000 else 0 end as EQ_IncrCovD
			, (case when HO_0448_FLAG = '1' and p1.PolicyFormNumber = 3 then HO_0448_ADDL_LIMIT_01 else 0 end)*round(f9,.01)/1000 as EQ_HO_0448
			, case when HO_0454_FLAG = '1' 
				then round(calculated EQ_CovA + calculated EQ_ORD_LAW + calculated EQ_IncrCovC + calculated EQ_IncrCovD + calculated EQ_HO_0448,1.0)
				else 0 end
			as Earthquake_rate
		from in.Policy_Info p1
			inner join construction			p2 on p1.casenumber = p2.casenumber
			inner join coverage_c			p3 on p1.casenumber = p3.casenumber
			inner join coverage_d			p4 on p1.casenumber = p4.casenumber
			inner join in.Endorsement_Info	p5 on p1.casenumber = p5.casenumber
			inner join YearBuiltGroup       P6 on p1.casenumber = p6.casenumber
			left join r505d f on input(p1.PropertyQuakeZone,2.) = f.f3 and p2.ConstructionType = f.ConstructionType and p6.YearBuiltGroup = f.YearBuiltGroup;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 505.D EARTHQUAKE';
	proc freq data=out.Earthquake_rate;
	tables HO_0454_FLAG*ConstructionType*PropertyQuakeZone*Earthquake_rate / list missing;
	run;
%end;
%mend;
%eq();
