/*Step 3 Supplement*/

**=====================================================================================**
History: 2015 04 20 JT  1.0 Initial draft
		 2015 05 27 SY  1.1 Added Hurricane peril back and revise the calculations for special personal property
**=====================================================================================**;

%macro sup();
	proc sql;
		create table out.Step3Supplement as
		select casenumber
		, fire_base_premium / fire_sp_personal_prop * (fire_sp_personal_prop-1) as SpecialPersonalProp_Fire
		, theft_base_premium / theft_sp_personal_prop * (theft_sp_personal_prop-1) as SpecialPersonalProp_Theft
		, waternw_base_premium / waternw_sp_personal_prop * (waternw_sp_personal_prop-1) as SpecialPersonalProp_WaterNW
		, othernw_base_premium / othernw_sp_personal_prop * (othernw_sp_personal_prop-1) as SpecialPersonalProp_OtherNW
		, lightning_base_premium / (lightning_sp_personal_prop) * (lightning_sp_personal_prop-1) as SpecialPersonalProp_Lightning
		, round(sum(calculated SpecialPersonalProp_Fire, calculated SpecialPersonalProp_Theft, calculated SpecialPersonalProp_WaterNW, calculated SpecialPersonalProp_OtherNW, calculated SpecialPersonalProp_Lightning),1.0) as SpecialPersonalProp
		from out.step2_base_premium
		;
	quit;

	proc sql;
		create table out.step3_subtotal_premium as
		select a.*
		, b.SpecialPersonalProp
		, round(a.fire_disc_base_premium
						+ a.theft_disc_base_premium
						+ a.waternw_disc_base_premium
						+ a.othernw_disc_base_premium
						+ a.lightning_disc_base_premium
						+ a.waterw_disc_base_premium
						+ a.wind_disc_base_premium
						+ a.hail_disc_base_premium
						+ a.hurr_disc_base_premium
						,.0001) as subtotal_premium
		
		from out.step2_base_premium as a
		inner join out.Step3Supplement as b
		on a.CaseNumber = b.CaseNumber
		;
	quit;

%if &Process = 0 %then %do;
	title 'STEP 3: SUBTOTAL PREMIUM (RULE 301.A.3)';
	proc print data=out.step3_subtotal_premium;
	where subtotal_premium = .;
	run;
	proc sql;
		select mean(fire_disc_base_premium) as fire_disc_base_prem_avg
			, mean(theft_disc_base_premium) as theft_disc_base_prem_avg
			, mean(waternw_disc_base_premium) as waternw_disc_base_prem_avg
			, mean(othernw_disc_base_premium) as othernw_disc_base_prem_avg
			, mean(lightning_disc_base_premium) as lightning_disc_base_prem_avg
			, mean(waterw_disc_base_premium) as waterw_disc_base_prem_avg
			, mean(wind_disc_base_premium) as wind_disc_base_prem_avg
			, mean(hail_disc_base_premium) as hail_disc_base_prem_avg
			, mean(hurr_disc_base_premium) as hurr_disc_base_prem_avg
			, mean(subtotal_premium) as subtotal_premium_avg
		from out.step3_subtotal_premium;
	quit;
%end;
%mend;
%sup();