** RULE 355. AGE OF HOME BY INSURANCE SCORE **;
				data exp_home_age_x_ins_score_fac (keep=home_age ins_tier factor);
				set fac.'355# Age of Home by Ins Score$'n (rename=('Expense Fee'n=f2));
				if f3=. or f1 = 'Home Age' then delete;
				home_age=input(f1,3.);
				array f (21) f2 - f22;
				do i = 1 to 21;
					ins_tier = strip(put(i,2.)); factor = f(i);
					output;
				end;
				run;
				proc sql;
					create table out.exp_home_age_x_ins_score_fac as
					select p1.casenumber, p1.policynumber, p1.PolicyIssueDate, p1.PolicyEffectiveDate, p1.PropertyYearBuilt, p1.home_age
						, p2.matchcode, p2.ScoreModel, p2.insscore, p2.ins_tier, f.factor as exp_home_age_x_ins_score_fac
					from home_age p1 
						inner join ins_tier p2 on p1.casenumber = p2.casenumber
						left join exp_home_age_x_ins_score_fac f on f.home_age = p1.home_age and f.ins_tier = p2.ins_tier;
				quit;
