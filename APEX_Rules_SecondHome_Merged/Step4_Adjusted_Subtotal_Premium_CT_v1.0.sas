** ADJUSTED SUBTOTAL PREMIUM **;
			proc sql;
				create table out.step4_Adjusted_subtotal_premium as
				select *
					, round(subtotal_premium * r401_HO_04_20_fac,.0001) as r401_AdjustedPrem
					, round(calculated r401_AdjustedPrem - subtotal_premium,1.0) as r401_HO_04_20_Prem
					, round(calculated r401_AdjustedPrem * r402c_HO_04_56_fac, .0001) as r402c_AdjustedPrem
					, round(calculated r402c_AdjustedPrem - calculated r401_AdjustedPrem) as r402c_HO_04_56_Prem
					, round(calculated r402c_AdjustedPrem * r403_HO_04_90_fac,.0001) as r403_AdjustedPrem
/*					, round(calculated r403_AdjustedPrem - calculated r401_AdjustedPrem,1.0) as r403_HO_04_90_Prem change in CT, SAS code error*/
					, round(calculated r403_AdjustedPrem - calculated r402c_AdjustedPrem,1.0) as r403_HO_04_90_Prem
					, round(calculated r403_AdjustedPrem * r404_Ordinance_or_Law_fac,.0001) as r404_AdjustedPrem
					, round(calculated r404_AdjustedPrem - calculated r403_AdjustedPrem,1.0) as r404_Ordinance_or_Law_Prem
/*					, round(calculated r404_AdjustedPrem * r411_Seasonal_Secondary_Fac,.0001) as r411_AdjustedPrem*/
/*					, round(calculated r411_AdjustedPrem - calculated r404_AdjustedPrem,1.0) as r411_Seasonal_Secondary_Prem*/
					, calculated r404_AdjustedPrem/*r411_AdjustedPrem*/ as adjusted_subtotal_premium
					as adjusted_subtotal_premium
				from out.step3_subtotal_premium f1
					inner join out.r401_add_amt_ins 				f2 on f1.casenumber=f2.casenumber		
					inner join out.r402_C_Special_Loss_Settlement	f6 on f1.casenumber=f6.casenumber
					inner join out.r403_CovC_Repl_Cost				f4 on f1.casenumber=f4.casenumber
					inner join out.r404_Ordinance_or_Law			f7 on f1.casenumber=f7.casenumber;
/*					inner join out.r411_Seasonal_Secondary_Fac      f8 on f1.casenumber=f8.casenumber;*/
			quit;