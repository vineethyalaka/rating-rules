** RULE 515.B REDUCED COVERAGE C **;
%macro RC();
	data r515b;
	set fac.'515#B Reduced Coverage C$'n;
	if strip(f1) = '' then delete;
	rename 'Coverage Limit Increment (i#e# c'n = Increment;
	run;
	proc sql;
		create table out.Decr_CovC_rate as
		select p.*
			, case when PolicyFormNumber = 3 then round(p.Decr_CovC*f.credit/Increment,1.0) else 0 end as Decr_CovC_rate
		from coverage_c p, r515b f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 515.B REDUCED COVERAGE C';
	proc gplot data=out.Decr_CovC_rate;
		plot Decr_CovC_rate*Decr_CovC
			/ overlay legend vaxis=axis2;
	run;
	quit; 
%end;
%mend;
%RC();
