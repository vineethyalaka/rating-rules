/*7/13/2018: add length of residence of form selection. customer age at new business.*/
** POLICY EXPENSE FEE **;
*Waiting for clarification;
* revise by Di on 03/23/2018: PA expense fee should not exceed 130 starting from 4th term;
* 02/13/2020: PA change exp_uw to exp_uw_tier, change exp_uw_tier to exp_uw_business_tier, same for the exp_home_age_x and Exp_Fee;
%macro fee();
proc sql;
	create table out.step7_policy_expense_fee as
	select f2.casenumber, f2.policynumber, f2.expense_base_rate, f1.expense_base_fac
		, f3.lengthofresidence, f3.expense_length_res
		, f4.new_home_flag, f4.new_home_purch, f4.policytermyears, f4.term, expense_new_home_purch
		, f5.policyissuedate, f5.firstterm, f5.DOB1, f5.cust_age_nb, expense_customer_age_nb
		, f6.BillPlan, f6.MortgageEBillFlag, f6.BillPlanType, f6.lapse, exp_bill_plan_x_lapse_fac
		, f7.home_age, exp_home_age_x_bill_plan_fac
		, f8.matchcode, f8.ScoreModel, f8.insscore, f8.ins_tier, f8.drc_class, f8.uw_tier, f8.uw_tier_n, exp_uw_tier_x_bill_plan_fac
		, exp_uw_tier_x_lapse_fac, exp_home_age_x_uw_tier_fac, exp_home_age_x_lapse_fac, Exp_Fee_uw_business_tier
		, expense_base_fac_HF9
		, 
		case when f13.policytermyears>=4 then min(
		round(
            expense_base_rate		
		/ (
/*expense_base_fac*/
		case when f2.PolicyFormNumber = 3 then expense_base_fac else expense_base_fac_HF9 end
/*		+ expense_length_res*/
	    + case when f2.PolicyFormNumber = 3 then expense_length_res else 0 end 
		+ expense_new_home_purch
/*		+ expense_customer_age_nb*/
		+ case when f2.PolicyFormNumber = 3 then expense_customer_age_nb else 0 end
		+ exp_bill_plan_x_lapse_fac
		+ exp_home_age_x_bill_plan_fac
		+ exp_uw_tier_x_bill_plan_fac
		+ exp_uw_tier_x_lapse_fac
		+ exp_home_age_x_uw_tier_fac
		+ exp_home_age_x_lapse_fac
        + Exp_Fee_sec_res)
		* Exp_Fee_uw_business_tier,1.0),130) 


		else round(
            expense_base_rate		
		/ (
/*expense_base_fac*/
		case when f2.PolicyFormNumber = 3 then expense_base_fac else expense_base_fac_HF9 end
/*		+ expense_length_res*/
	    + case when f2.PolicyFormNumber = 3 then expense_length_res else 0 end 
		+ expense_new_home_purch
/*		+ expense_customer_age_nb*/
		+ case when f2.PolicyFormNumber = 3 then expense_customer_age_nb else 0 end
		+ exp_bill_plan_x_lapse_fac
		+ exp_home_age_x_bill_plan_fac
		+ exp_uw_tier_x_bill_plan_fac
		+ exp_uw_tier_x_lapse_fac
		+ exp_home_age_x_uw_tier_fac
		+ exp_home_age_x_lapse_fac
        + Exp_Fee_sec_res)
		* Exp_Fee_uw_business_tier,1.0)
		end 
		as policy_expense_fee

	from out.base_exp_fac f1, out.base_rate f2
		inner join out.length_res_fac 	  			 f3 on f2.casenumber=f3.casenumber
		inner join out.new_home_purch_fac 			 f4 on f2.casenumber=f4.casenumber
		inner join out.customer_age_nb_fac			 f5 on f2.casenumber=f5.casenumber
		inner join out.exp_bill_plan_x_lapse_fac     f6 on f2.casenumber=f6.casenumber
		inner join out.exp_home_age_x_bill_plan_fac  f7 on f2.casenumber=f7.casenumber
		inner join out.exp_uw_tier_x_bill_plan_fac 	 f8 on f2.casenumber=f8.casenumber
		inner join out.exp_uw_tier_x_lapse_fac	 	 f9 on f2.casenumber=f9.casenumber
		inner join out.exp_home_age_x_uw_tier_fac 	f10 on f2.casenumber=f10.casenumber
		inner join out.exp_home_age_x_lapse_fac	  	f11 on f2.casenumber=f11.casenumber
		inner join out.uw_business_tier_fac         f12 on f2.casenumber=f12.casenumber
		inner join in.policy_info 					f13 on f2.casenumber=f13.casenumber
        inner join out.Seasonal_Secondary_Fac       f14 on f2.casenumber=f14.casenumber;
quit;

%if &Process = 0 %then %do;
title 'STEP 7: POLICY EXPENSE FEE (RULE 301.A.7)';
proc print data=out.step7_policy_expense_fee;
where policy_expense_fee = .;
run;
proc sql;
	create table out.average_policy_expense_fee as
	select mean(policy_expense_fee) as policy_expense_fee_avg
	from out.step7_policy_expense_fee;
quit;
%end;
%mend;
%fee();