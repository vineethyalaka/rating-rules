** UNCAPPED PREMIUM **;
%macro final();
	proc sql;
		create table out.Step8_uncapped_calc_premium as
		select *
			, round(round((p1.adjusted_subtotal_premium
							+ p2.Property_Premium
							+ p3.Total_Liability_Premium
							+ p4.policy_expense_fee),1)* p8.rate_adjustment, 1) as uncapped_calc_prem_before_disc
			, round(calculated uncapped_calc_prem_before_disc*p6.affinity,1) as Affinity_Discount
			, round(calculated uncapped_calc_prem_before_disc*p5.policy_conversion_fac,1) as Policy_Conversion_Disc
			, (calculated uncapped_calc_prem_before_disc - calculated Affinity_Discount - calculated Policy_Conversion_Disc) as uncapped_calc_premium

		from out.step4_Adjusted_subtotal_premium p1
			inner join out.step5_Property_Premium	p2 on p1.casenumber = p2.casenumber
			inner join out.step6_Liability_Premium	p3 on p1.casenumber = p3.casenumber
			inner join out.step7_policy_expense_fee	p4 on p1.casenumber = p4.casenumber
			inner join out.policy_conversion_fac	p5 on p1.casenumber = p5.casenumber
			inner join out.affinity_fac				p6 on p1.casenumber = p6.casenumber
			inner join out.rate_adjustment			p8 on p1.casenumber = p8.casenumber;
/*			, out.min_premium p7;*/
	quit;

/*%if &Process = 0 %then %do;*/
/*	title 'STEP 8: FINAL PREMIUM (RULE 301.A.8)';*/
/*	proc print data=out.step8_final_premium;*/
/*	where final_calc_prem = .;*/
/*	run;*/
/*	proc sql;*/
/*		create table out.average_final_premium as*/
/*		select mean(adjusted_subtotal_premium) as adjusted_subtotal_premium_avg*/
/*			, mean(policy_conversion_fac) as policy_conversion_fac_avg*/
/*			, mean(affinity) as affinity_avg*/
/*			, mean(final_premium) as final_premium_avg*/
/*		from out.step8_final_premium;*/
/*	quit;*/
/*	proc sgplot data=out.step8_final_premium;*/
/*		histogram final_premium;*/
/*	run;*/
/**/
/*%end;*/
%mend;
%final();
