** RULE 403. PERSONAL PROPERTY REPLACEMENT COST **;
				data r403;
				set fac.'403# Personal Prop Repl Cost$'n;
				if factor = . then delete;
				run;
				proc sql;
					create table out.r403_CovC_Repl_Cost as
					select p.CaseNumber, p.PolicyNumber, p.HO_0490_FLAG
						 , case when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ then f.Factor else 1 end as r403_HO_04_90_fac
					from in.Endorsement_Info p, r403 f;
				quit;