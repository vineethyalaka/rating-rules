** RULE 301.A COUNTY **;
* Additional factors for adjustments at the county level **;
* 10/11/2018: Mo added p10,p11,p12, and p17;

%macro county();
	proc sql;
		create table out.county_fac as
		select p.casenumber, p.policynumber, p.countyiD
			, f.Fire as fire_county
			, f.Theft as theft_county
			, f.'Water Non-Weather'n as waternw_county
			, f.'Water Weather'n as waterw_county
			, f.Lightning as lightning_county
			, f.Wind as wind_county
			, f.Hail as hail_county
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_county
			, f.'Cov C - Fire'n as Cov_C_Fire_county
			, f.'Cov C - EC'n as Cov_C_EC_county
			, f.'Property Damage due to Burglary'n as PD_Burglary_county 
			, f.'Limited Theft Coverage'n as Limited_Theft_county
		from out.territory p left join fac.'301#A County (Territory)$'n f 
			on p.countyiD = f.F2;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 301.A COUNTY';
	proc freq data=out.county_fac;
	tables countyid
		*theft_county / list missing;
	run;
%end;
%mend;
%county();