	data lengthresfac;
	set fac.'331# Length of Residence$'n;
	lengthmin = f1+0;
	lengthmax = f3+0;
	if f1=0 and f3=. then lengthmax=0;
	if f1="Unavailable" and f3=. then do; lengthmin=0; lengthmax=0;end;
	if f1=15.01 and f3=. then lengthmax=9999;
	run;

	data r331;
	set in.production_info;
	format lengthofresidence 10.2;
	lengthofresidence = lengthofresidence/100;
	if lengthofresidence=0 then lengthofresidence=5;
	run;


	proc sql;
		create table out.length_res_fac as
		select p.casenumber, p.policynumber, p.lengthofresidence label='length of residence', p.lengthofresidenceind 
			, f.theft as theft_length_res
			, f.'Water Non-Weather'n as waternw_length_res
			, f.'Expense Fee'n as expense_length_res
		from r331 p left join lengthresfac f
			on p.lengthofresidence >= f.lengthmin
			and p.lengthofresidence <= f.lengthmax;
	quit;