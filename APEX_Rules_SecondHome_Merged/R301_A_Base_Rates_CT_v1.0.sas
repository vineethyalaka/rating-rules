** RULE 301.A BASE RATES **;
data base_rate;
	set fac.'301#A Base Rates$'n;

	if Fire in ('','P1') then
		delete;
	fire_base_rate		= input(Fire,dollar10.);
	theft_base_rate		= input(Theft,dollar10.);
	waternw_base_rate	= input('Water Non-Weather'n,dollar10.);
	othernw_base_rate	= input('All Other Non-Weather'n,dollar10.);
	lightning_base_rate	= input(Lightning,dollar10.);
	waterw_base_rate	= input('Water Weather'n,dollar10.);
	wind_base_rate		= input(Wind,dollar10.);
	hail_base_rate		= input(Hail,dollar10.);
	hurr_base_rate		= input(Hurricane,dollar10.);
	liabipi_base_rate	= input('Liability - Bodily injury (Perso'n,dollar10.);
	liabimp_base_rate	= input('Liability - Bodily injury (Medic'n,dollar10.);
	liapd_base_rate		= input('Liability - Property Damage to O'n,dollar10.);
	waterbu_base_rate	= input('Water Back-up'n,dollar10.);
	expense_base_rate	= input('Expense Fee'n,dollar10.);
run;

proc sql;
	create table out.base_rate as
		select p.casenumber, p.policynumber
			, f.fire_base_rate
			, f.theft_base_rate
			, f.waternw_base_rate
			, f.othernw_base_rate
			, f.lightning_base_rate
			, f.waterw_base_rate
			, f.wind_base_rate
			, f.hail_base_rate
			, f.hurr_base_rate
			, f.liabipi_base_rate
			, f.liabimp_base_rate
			, f.liapd_base_rate
			, f.waterbu_base_rate
			, f.expense_base_rate
		from in.policy_info p, base_rate f;
quit;