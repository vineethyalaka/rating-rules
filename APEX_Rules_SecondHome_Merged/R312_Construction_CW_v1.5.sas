** RULE 312. CONSTRUCTION **;
/*2019/03/14 TM change Masonry veneer catergory for AZ*/
/*2019/05/28 WZ change All Other to Masonry for IN. Allow factor table to have different construction type phrasing.*/

%macro construct();
	data construction; *also used in rule 505 earthquake;
	set in.policy_info;
	length ConstructionType $20.;
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Aluminium','Aluminum');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Masonry non-combustible','Non-combustible');
	if strip(propertyconstructionclass) = "" then propertyconstructionclass = 'All Other';
	if upcase(propertyconstructionclass) 
		in ('ALUMINUM SIDING','CLAPBOARD','LOG','LOGS','VINYL SIDING','WOODEN SHAKES','WOOD SHAKES','WOOD SIDING') 
		then ConstructionType = "Frame";
	else if upcase(propertyconstructionclass) 
		in ('ADOBE','BLOCK (PAINTED)','BRICK & BLOCK','FIRE RESISTIVE','NON-COMBUSTIBLE','POURED CONCRETE'
			,'SOLID BRICK','SOLID STONE','STONE & BLOCK','STUCCO ON BLOCK','ALL OTHER') 
		then ConstructionType = "Masonry";
	else if upcase(propertyconstructionclass) 
		in ('ASBESTOS','BRICK VENEER','EIFS/ SYNTHETIC STUCCO','EIFS/SYNTHETIC','HARDY PLANK','HARDIPLANK','HARDY PLANK/CEMENT FIBER','HARDIPLANK/CEMENT FIBER','STONE VENEER','STUCCO ON FRAME') 
		then ConstructionType = "Masonry Veneer";

	run;

	data r312;
	format f1 $30.;
	set fac.'312# Construction$'n;
	f1=tranwrd(f1,'EIFS/ Synthetic','EIFS/Synthetic');
	f1=tranwrd(f1,'HardiPlank','Hardy Plank');
	f1=tranwrd(f1,'Wooden shakes','Wood Shakes');
	f1=tranwrd(f1,'Wooden Shakes','Wood Shakes');
	if strip(f1) = 'Log' then f1=tranwrd(f1,'Log','Logs');
	run;

	proc sql;
		create table out.construction_class_fac as
		select distinct p.casenumber, p.policynumber, p.propertyconstructionclass
			, f.wind as wind_const_class
		from construction p left join r312 f
			on compress(upcase(p.propertyconstructionclass))=compress(upcase(f.f1));
	quit;

%if &Process = 0 %then %do;
	title 'RULE 312. CONSTRUCTION';
	proc freq data=out.construction_class_fac;
	tables 	propertyconstructionclass
		*wind_const_class/ list missing;
	run;
%end;

%mend;
%construct();
