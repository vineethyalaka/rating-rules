%macro Score_Rate_Factor();
data Score_Rate_Factor;
set fac.'339# Rate Adjustment$'n;
if f2=. then delete;
if f1='TUIRS' then ScoreModel =2;
if f1 = 'TrueRisk' then ScoreModel =3;
run;

proc sql;
/*create table out.Score_Rate_Adjustment_Factor as*/
/*select f1, ScoreModel, f2 from Score_Rate_Factor*/
create table out.Score_Rate_Adj_Fac as 
select d.CaseNumber, d.PolicyNumber, f.ScoreModel, f.f2 as Rate_Adj_Fac
from in.drc_info d
inner join Score_Rate_Factor f
on d.ScoreModel = f.ScoreModel;
quit;
%mend;
%Score_Rate_Factor();
/**/
/*data out.Score_Rate_Adjustment_Factor*/
/*set Score_Rate_Factor*/

