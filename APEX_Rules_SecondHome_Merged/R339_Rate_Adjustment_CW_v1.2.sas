**=====================================================================================**
Wanwan Zhang 2019 01 22 craeted for NJ R339 Rate adjustment		 
**=====================================================================================**;
	data r339fac;
	set fac.'339# Rate Adjustment$'n (firstobs=2 obs=2);
	format f1 4.;
	keep f1;
	call symput('quick_rate',f1);
	run;

	%put &quick_rate;

	%global quk_rte;
    %let quk_rte= round(round((p1.adjusted_subtotal_premium
			+ p2.Property_Premium
			+ p3.Total_Liability_Premium
			+ p4.policy_expense_fee),1)*&quick_rate.,1)
			as final_calc_prem;
