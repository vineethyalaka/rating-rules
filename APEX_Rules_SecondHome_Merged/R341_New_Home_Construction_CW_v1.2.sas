** RULE 341. NEW HOME CONSTRUCTION **;
**=====================================================================================**
History: 
		2015 03 17 JT  Initial draft
		2015 05 15 SY  Added Hurricane peril indicator, Revise rule number
Second Home History:
		2016 07 29 MM  Initial draft
		2020 01 30 VR  Added code in home_disc data step that neglects null value rows.
**=====================================================================================**;
%macro NHC();
	data home_age2;
		set home_age;
		if home_age>10 then home_age=10;
	run;

	data home_disc;
		set fac.'341# New Home Construction$'n (firstobs=2 obs=12);
		if f1=. and fire ne . then f1=10;
		drop f9;
	run;

	proc sql;
		create table out.new_home_fac as
		select p.casenumber, p.policynumber, p.PolicyIssueDate, p.PolicyEffectiveDate
			, p.PropertyYearBuilt, p.home_age
			, f.fire as fire_new_home
			, f.'Water Non-Weather'n as waternw_new_home
			, f.'All Other Non-Weather'n as othernw_new_home
			, f.'Water Weather'n as waterw_new_home
			, f.wind as wind_new_home
			, f.hail as hail_new_home
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_new_home
			, f.'Cov C - Fire'n as Cov_C_Fire_new_home
			, f.Wildfire as WF_new_home
		from home_age2 p left join home_disc f
			on p.home_age=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 341. NEW HOME CONSTRUCTION';
	proc freq data=out.new_home_fac;
	tables home_age
		*fire_new_home
		*waternw_new_home
		*othernw_new_home
		*waterw_new_home
		*wind_new_home
		*hail_new_home
		*hurr_new_home
		*Cov_C_Fire_new_home  / list missing;
	run;
%end;
%mend;
%NHC();
