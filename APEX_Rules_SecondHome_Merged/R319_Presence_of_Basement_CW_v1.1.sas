** RULE 319. PRESENCE OF BASEMENT **;
%macro POB();
	data basement;
	set in.policy_info;
	length basement $3.;
	basement='No';
	if propertyfinishedbasementsqft>0 then basement='Yes';
	if propertyfoundationtype in(1,2) then basement='Yes';
	run;
	proc sql;
		create table out.basement_fac as
		select p.casenumber, p.policynumber, p.propertyfinishedbasementsqft, p.propertyfoundationtype, p.basement
			, f.Fire as fire_bamnt
			, f.'Water Non-Weather'n as waternw_bsmnt
			, f.'All Other Non-Weather'n as all_other_non_weather_bsmnt
			, f.'Water Weather'n as water_weather_bsmnt
			, f.Wind as wind_bsmnt
			, f.hail as hail_bsmnt
			, f.'Water Back-up'n as waterbu_bsmnt
		from basement p left join fac.'319# Presence of Basement$'n f
			on p.basement=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 319. PRESENCE OF BASEMENT';
	proc freq data=out.basement_fac;
	tables basement
		*waternw_bsmnt
		*waterbu_bsmnt / list missing;
	run;
%end;
%mend;
%POB();
