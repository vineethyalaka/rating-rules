** RULE 505.D EARTHQUAKE **;
				data r505d;
				set fac.'505#D Earthquake$'n;
				if 'Zone/Column'n =. then delete;
				retain ConstructionType;
				if 'Construction Type'n ^= "" then ConstructionType = 'Construction Type'n;
				run;
				data coverage_d (keep=CaseNumber PolicyNumber CoverageA CoverageD CovD_pctA Incr_CovD); set in.policy_Info;
					CovD_pctA = CoverageD/CoverageA;
					if CoverageD > CoverageA*0.3 then Incr_CovD = CoverageD - CoverageA*0.3;
					else Incr_CovD = 0;
				run;
				proc sql;
					create table out.Earthquake_rate as
					select p1.casenumber, p1.policynumber, p1.PropertyQuakeZone, p2.propertyconstructionclass, p2.ConstructionType
						, p3.CoverageA, Incr_CovC, Incr_CovD
						, HO_0454_FLAG, ORD_LAW_FLAG, HO_0448_FLAG, HO_0448_ADDL_LIMIT_01
						, A, D, F
						, A*p3.CoverageA*1.1/1000 as EQ_CovA
						, (case when ORD_LAW_FLAG = '1' then A else 0 end)*p3.CoverageA*0.15/1000 as EQ_ORD_LAW
						, D*Incr_CovC/1000 as EQ_IncrCovC
						, F*Incr_CovD/1000 as EQ_IncrCovD
						, (case when HO_0448_FLAG = '1' then HO_0448_ADDL_LIMIT_01 else 0 end)*F/1000 as EQ_HO_0448
						, case when HO_0454_FLAG = '1' 
							then round(calculated EQ_CovA + calculated EQ_ORD_LAW + calculated EQ_IncrCovC + calculated EQ_IncrCovD + calculated EQ_HO_0448,1.0)
							else 0 end
						as Earthquake_rate
					from in.Policy_Info p1
						inner join construction			p2 on p1.casenumber = p2.casenumber
						inner join coverage_c			p3 on p1.casenumber = p3.casenumber
						inner join coverage_d			p4 on p1.casenumber = p4.casenumber
						inner join in.Endorsement_Info	p5 on p1.casenumber = p5.casenumber
						left join r505d f on input(p1.PropertyQuakeZone,2.) = f.'Zone/Column'n and p2.ConstructionType = f.ConstructionType;
				quit;