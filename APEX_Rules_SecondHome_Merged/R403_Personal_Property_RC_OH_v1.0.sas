** RULE 403. PERSONAL PROPERTY REPLACEMENT COST **;

**=====================================================================================**
History: 2017 10 03 WZ      Version Control: Extract directly from RatingQA OH Apex Rating Engine
**=====================================================================================**;


%macro RC();
	data r403;
	set fac.'403# Personal Prop Repl Cost$'n;
	if 'HO3'n = . then delete;
	run;
	proc sql;
		create table out.r403_CovC_Repl_Cost as
		select p.CaseNumber, p.PolicyNumber, p.PolicyFormNumber, p.HO_0490_FLAG
			 , case when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ and p.PolicyFormNumber = 3 or p.HO_0490_FLAG = '1' and p.HO_04_95_IND="Y" then f.'HO3'n 
			 else 1 end as r403_HO_04_90_fac,
			 case when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ and p.PolicyFormNumber = 9 then f.'HF9-P10'n
			else 1 end as Cov_C_r403_HO_04_90_fac,
			case when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ and p.PolicyFormNumber = 9 then f.'HF9-P11'n
			else 1 end as Cov_C_EC_r403_HO_04_90_fac,
			case when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ and p.PolicyFormNumber = 9 then f.'P16'n
			else 1 end as waterbu_r403_HO_04_90_fac,
			case when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ and p.PolicyFormNumber = 9 then f.'HF9-P11'n
			else 1 end as Lim_Theft_r403_HO_04_90_fac
		from in.Endorsement_Info p, r403 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 403. PERSONAL PROPERTY REPLACEMENT COST';
	proc freq data=out.r403_CovC_Repl_Cost;
	tables HO_0490_FLAG*r403_HO_04_90_fac*PolicyFormNumber / list missing;
	run;
%end;
%mend;
%RC();


