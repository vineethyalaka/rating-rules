** RULE 344. PARTNER DISCOUNTS **;

**=====================================================================================**
History: 2017 10 06 WZ      Version Control: Extract directly from RatingQA IL Apex Rating Engine
**=====================================================================================**;

data affinity; 
set in.policy_info (keep=casenumber policynumber policyaccountnumber autopolicynumber);
if policyaccountnumber in (1020:1026
                          , 1051, 1130:1132, 1550:1553, 1610, 1920:1921
							 , 2000:2199, 2300:2699, 3300:3899, 4000:4499
							 , 7010, 7030, 8000:8999, 11000, 11510:11520
							 , 13220:13299
                             , 13800:13899
							 , 14000:14500, 13200:13299, 14900:14999)
	then partner = 1;
else partner = 0;
if autopolicynumber = '' then auto = 0;
else auto = 1;
run;
/*proc sql;*/
/*	create table out.affinity_fac as*/
/*	select p.*,  auto*partner*0.1 as affinity*/
/*	from affinity p;*/
/*quit;*/

	proc sql;
		create table out.affinity_fac as
		select p.*, a.partnerflag, 
case 
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

        when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/
 	
 		
		else auto*partner*0.1
/*'Affinity Discount'n*/ end as affinity
		from affinity p inner join in.policy_info a on p.casenumber=a.casenumber/*, affinity_fac f*/;
	quit;

title 'RULE 344. AFFINITY MARKETING';
proc freq data=out.affinity_fac;
tables partner*auto*affinity / list missing;
run;