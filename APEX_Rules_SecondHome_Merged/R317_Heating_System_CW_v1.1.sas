** RULE 317. HEATING SYSTEM **;
%macro heat();

	data heat;
		set in.policy_info;
		length heat $7.;
		if index(upcase(propertyheatingsystem),'OIL')>0 then heat='Oil';
		else heat='Non-Oil';
	run;

	data heatfac;
		set fac.'317# Heating System$'n;
		where fire is not missing;
		keep F1 fire F3;
	run;

	proc sql;
		create table out.heat_system_fac as
		select p.casenumber, p.policynumber, p.propertyheatingsystem, p.heat
			, f.fire as fire_heat_sys, f.F3 as Cov_C_Fire_heat_sys
		from heat p left join heatfac f
			on p.heat=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 317. HEATING SYSTEM';
	proc freq data=out.heat_system_fac;
	tables propertyheatingsystem
		*fire_heat_sys*Cov_C_Fire_heat_sys / list missing;
	run;
%end;

%mend;
%heat();
