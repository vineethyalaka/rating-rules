/*Wanwan Zhang : 10/18/2019 this is to generate SAS table with all Affinity and Minidi discounts and apply the discount to policy*/
/*DG: Update for some step8 SAS colculation to (1 - factor)  (e.g. 0.9 -> 0.1   0.95 -> 0.05    1 -> 0)*/
/* VR - 2-20-2020 - Removed the duplicate rows from affinity_fac*/

%macro discount_from_DB; 
/*Get the latest version of this state with Apex flag for intial SAS discount table generating*/
/*use @ratingversion as version number instead of &ratingversion in following code*/
/*DECLARE  @ratingversion INT */
/*SELECT top 1 @ratingversion = version */
/*FROM dbo.V3RatingVersion WHERE StateCode = 'il' AND ApexFlag = 1 ORDER BY version DESC; */

/*Get the Affinity and Mindi discounts from database with version set up in compile code*/
/*This requires discount table validation between business requirement and database*/

IF OBJECT_ID(N%str(%')tempdb..##AllDiscounts&time.%str(%')) IS NOT NULL
	BEGIN
		DROP TABLE ##AllDiscounts&time.
	END

SELECT * INTO ##AllDiscounts&time. FROM (
SELECT Version,
       MinMarketGroup,
       MaxMarketGroup,
       'Y' AS AutoPolicyFlag,
       'Affinity' AS PartnerPolicyFlag,
       1 - AffinityDiscount AS factor
FROM dbo.HS_APRating_AffinityMarketing
WHERE Version = &ratingversion
UNION
SELECT Version,
       MinMarketGroup,
       MaxMarketGroup,
       'N' AS AutoPolicyFlag,
       'Affinity' AS PartnerPolicyFlag,
       1 AS factor
FROM dbo.HS_APRating_AffinityMarketing
WHERE Version = &ratingversion
UNION
SELECT Version,
       MinMarketGroup,
       MaxMarketGroup,
       LoanFlag AS AutoPolicyFlag,
       'Affinity' AS PartnerPolicyFlag,
       Factor
FROM dbo.HS_V4Rating_AffinityDiscount
WHERE Version = &ratingversion
UNION
SELECT Version,
       MinMarketGroup,
       MaxMarketGroup,
       AutoCustomer,
       PartnerCustomer,
       1 - (MindiDiscount + AdditiveDiscountFactor) AS factor
FROM dbo.HS_APRating_MindiMarketing
WHERE Version = &ratingversion
UNION
SELECT Version,
       MinMarketGroup,
       MaxMarketGroup,
       AutoCustomer,
       PartnerCustomer,
       (1 - (1 - Factor) - (1 - AdditiveDiscountFactor)) as Factor
FROM dbo.HS_V4Rating_MindiDiscount
WHERE Version = &ratingversion) AS temp;

%mend; 

/*Build connections with database and create discount table*/
proc sql; 
   &odbc_string;
   &odbc_string2(%discount_from_DB);    
	 create table out.alldiscounts as
      select * FROM &server.."##AllDiscounts&time."n;

quit; 


/*Find discount fatcors by joining discount table*/
proc sql;
      create table out.alldiscounts_fac as
      select distinct a.CaseNumber, a.PolicyNumber, a.PolicyAccountNumber, a.AutoPolicyNumber as P_AutoFlag,b.AutoPolicyFlag as D_AutoFlag ,substr(a.partnerflag,1,1) as P_PartnerFlag, b.PartnerPolicyFlag as D_PartnerFlag
	  ,b.factor as factor
      from in.policy_info as a
      left join out.alldiscounts as b
      on a.PolicyAccountNumber <= b.MaxMarketGroup
      and a.PolicyAccountNumber >= b.MinMarketGroup
      and ((a.AutoPolicyNumber is null and b.AutoPolicyFlag="N"  and b.PartnerPolicyFlag = "Affinity")
        or (a.AutoPolicyNumber is not null and b.AutoPolicyFlag="Y"  and b.PartnerPolicyFlag = "Affinity")
		or (a.AutoPolicyNumber is null and b.AutoPolicyFlag="N" and substr(a.partnerflag,1,1)="0" and b.PartnerPolicyFlag = "N")
		or (a.AutoPolicyNumber is null and b.AutoPolicyFlag="N" and substr(a.partnerflag,1,1)="1" and b.PartnerPolicyFlag = "Y")
		or (a.AutoPolicyNumber is not null and b.AutoPolicyFlag="Y" and substr(a.partnerflag,1,1)="0" and b.PartnerPolicyFlag = "N")
		or (a.AutoPolicyNumber is not null and b.AutoPolicyFlag="Y" and substr(a.partnerflag,1,1)="1" and b.PartnerPolicyFlag = "Y")
)
      ;
quit;
/*Set polices with no discount as 0*/
proc sql;
	create table out.affinity_fac_temp as 
		select CaseNumber, PolicyNumber, PolicyAccountNumber, P_AutoFlag, D_AutoFlag,
			case 
				when D_PartnerFlag = 'Affinity' then 'Affinity'
				when D_PartnerFlag = '' then 'Not Affinity or Mindi'
				else P_PartnerFlag 
			end 
		as P_PartnerFlag, D_PartnerFlag, 
			case 
				when factor = . then 0 
				else (1-factor) 
			end 
		as affinity 
		from out.alldiscounts_fac;
quit;

proc sort data=out.affinity_fac_temp 
		  out=out.affinity_fac 
		  nodupkey 
		  dupout=out.aff_fac_duplicates_removed;
	by casenumber;
run;
