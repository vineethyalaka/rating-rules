** RULE 372. PRESENCE OF WOODSTOVE **;
/*Make a CW version*/

%macro PoW();
	
	data r372_sheet_manipulation;
		set fac.'372# Presence of Woodstove$'n;
		where Fire is not missing;
		keep F1 Fire;
	run;

	proc sql;
		create table out.Woodstove_Presence_Fac as
		select p.CaseNumber, p.PolicyNumber, p.PropertyWoodStove
			, f.fire as fire_woodstove_pre

		from in.policy_info as p left join r372_sheet_manipulation f
		on p.PropertyWoodStove=substr(f1,1,1);
	quit;

%if &Process = 0 %then %do;
	title 'RULE 372. PRESENCE OF WOODSTOVE';
	proc freq data=out.Woodstove_Presence_Fac;
	tables woodstove_pre					
		*fire_woodstove_pre/list missing;
	run;
%end;

%mend;
%PoW();