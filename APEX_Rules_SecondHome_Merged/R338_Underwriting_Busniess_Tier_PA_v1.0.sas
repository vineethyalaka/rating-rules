/*7/13/2018:  temporarilty change the sheet name for rule 338. Pricing team will fix the rule name issue later.*/
%macro UNDW();
/*	data Underwriting_Tier;*/
/*	set in.policy_info;*/
/*	if policyaccountnumber in (14000,14200,14300,14310,14400) then undwtier = 2;*/
/*	else undwtier = 1;*/
/*	run;*/
/**/
/*	data Underwriting_Tier;*/
/*	set in.policy_info;*/
/*	if policyaccountnumber in (14000,14200,14300,14310,14400) then undwtier = 2;*/
/*	else undwtier = 1;*/
/*	run;*/
/**/
/*	*/
/*	data R338;*/
/*	set fac.'338# Underwriting Tier$'n;*/
/*	if F1=. then delete;*/
/*	rename F2 = Factor;*/
/*	run;*/
/**/
/**/
/*	proc sql;*/
/*		create table out.undwtier as*/
/*		select    p.casenumber*/
/*                , p.policynumber*/
/*				, Fire as Fire_undwtier*/
/*				, Theft as Theft_undwtier*/
/*				, 'Water Non-Weather'n as waternw_undwtier*/
/*				, 'All Other Non-Weather'n as allothernw_undwtier*/
/*				, Lightning as Lightning_undwtier*/
/*				, 'Water Weather' as waterw_undwtier*/
/*				, Wind as wind_undwtier*/
/*				, Hail as Hail_undwtier*/
/*				, Hurricane as hurr_undwtier*/
/*				, 'Cov C - Fire'n as CovCFire_undwtier*/
/*				, 'Cov C - EC'n as CovCEC_undwtier*/
/*				, 'Property Damage due to Burglary'n as PD_Blry_undwtier*/
/*				, 'Liability - Bodily Injury (Perso'n as lia_BI_PI_undwtier*/
/*				, 'Liability - Bodily Injury (Medic'n as lia_BI_MP_undwtier*/
/*				, 'Liability - Property Damage to O'n as lia_PD_undwtier*/
/*				, 'Water Back-up'n as Water_bkup_Fire_undwtier*/
/*				, 'Limited Theft Coverage'n as Limited_Theft_undwtier*/
/*				, 'Expense Fee'n as Expense_undwtier*/
/*		from Underwriting_Tier p*/
/*		left join R338 f on p.undwtier=f.f1;*/
/*	quit;*/

	data uw_tier_fac;
	set fac.'338# Underwriting tier$'n;
	if f1=. then delete;
	run;

	proc sql;
		create table out.uw_tier_fac as
		select p.casenumber, p.policynumber, p.PolicyIssueDate, p.PolicyEffectiveDate
			,p.undwtier
			,f.fire as fire_uw_tier
			,f.theft as theft_uw_tier
			,f.'Water Non-Weather'n as waternw_uw_tier 
			,f.'All Other Non-Weather'n as othernw_uw_tier
			,f.lightning as lightning_uw_tier 
			,f.'Water Weather'n as Waterw_uw_tier 
			,f.Wind as wind_uw_tier
			,f.hail as hail_uw_tier
			%if &Hurricane = 1 %then %do;  
			,f.hurricane as hurr_uw_tier
			%end;
			,f.'Cov C - Fire'n as Cov_C_Fire_uw_tier
			,f.'Cov C - EC'n as Cov_C_EC_uw_tier
			,f.'Property Damage due to Burglary'n as PD_Burg_uw_tier
			,f.'Liability - Bodily Injury (Perso'n as Liab_BI_uw_tier
			,f.'Liability - Bodily Injury (Medic'n as Liab_MedPay_uw_tier
			,f.'Liability - Property Damage to O'n as Liab_PD_uw_tier
			,f.'Water Back-up'n as WBU_uw_tier
			,f.'Limited Theft Coverage'n as Lim_Theft_uw_tier
			,f.'Expense Fee'n as Exp_Fee_uw_tier
		from in.policy_info p left join uw_tier_fac f
		on p.undwtier = f.F1;
	quit;

%mend;
%UNDW();


