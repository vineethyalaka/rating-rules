** RULE 322. PRESENCE OF DOG(S) **;
%macro POD();
	data dogs;
	set in.policy_info;
	if dogsinhouseholdcount>=1 then dogs='Yes';
	else dogs='No';
	run;
	data dogsfac;
	set fac.'322# Presence of Dog(s)$'n;
	liabipi = 'Liability - Bodily injury (Perso'n*1;
	liabimp = 'Liability - Bodily injury (Medic'n*1;
	run;
	proc sql;
		create table out.num_dogs_fac as
		select p.casenumber, p.policynumber, p.dogsinhouseholdcount, p.dogs
			, CASE WHEN p.PolicyFormNumber = 9 then 1 else f.liabipi end as liabipi_num_dogs
			, CASE WHEN p.PolicyFormNumber = 9 then 1 else f.liabimp end as liabimp_num_dogs
		from dogs p left join dogsfac f
			on p.dogs=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 322. PRESENCE OF DOG(S)';
	proc freq data=out.num_dogs_fac;
	tables dogsinhouseholdcount
		*liabipi_num_dogs
		*liabimp_num_dogs / list missing;
	run;
%end;
%mend;
%POD();
