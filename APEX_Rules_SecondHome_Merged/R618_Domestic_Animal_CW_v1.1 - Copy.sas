** RULE 618. DOMESTIC ANIMAL **;
%macro da();
	data r618fac;
	set fac.'618# Domestic Animal$'n;
	if f1="" then delete;
	fac1='Liability - Bodily injury (Perso'n*1;
	fac2='Liability - Bodily injury (Medic'n*1;
	run;
	data DomesticAnimal; 
	set in.Policy_Info (keep=casenumber policynumber PolicyFormNumber /*DogsInHouseholdCount*/ AnimalLiaCov);
/*	if DogsInHouseholdCount <= 0 or PolicyFormNumber = 3 then DomesticAnimal_flag = 0;*/
/*	else DomesticAnimal_flag = 1;*/
	run;
	proc sql;
	    create table out.lia_DomesticAnimal_fac as
		select p.*
			, case when p.PolicyFormNumber = 3 or p.AnimalLiaCov = 0 or p.AnimalLiaCov = . then 0 else f.fac1 end as liabipi_DomesticAnimal
			, case when p.PolicyFormNumber = 3 or p.AnimalLiaCov = 0 or p.AnimalLiaCov = . then 0 else f.fac2 end as liabimp_DomesticAnimal
		from r618fac f, DomesticAnimal p;
	quit;
	

%if &Process = 0 %then %do;
	title 'RULE 618. DOMESTIC ANIMAL';
	proc freq data=out.lia_DomesticAnimal_fac;
	tables DogsInHouseholdCount
		*PolicyFormNumber
		*liabipi_DomesticAnimal
		*liabimp_DomesticAnimal  / list missing;
	run;
%end;
%mend;
%da();
