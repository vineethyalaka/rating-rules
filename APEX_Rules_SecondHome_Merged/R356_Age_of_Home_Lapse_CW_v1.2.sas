** RULE 356. AGE OF HOME BY LAPSE **;
/*11/8/2018 ITR13402 Change max age from hard code to macro variable.
                      Add a temp table home_age_adj_r356 to deal the case when home age > max age by TM*/
%macro agelapse();
	data r356;
	set fac.'356# Age of Home by Lapse$'n;
	run;

    proc sql;
		 select max(f1)+1
		 		into :max_age_lapse
	     from r356;

		 update r356
		        set f1 = &max_age_lapse
				where f1=. and f3 ne .;
	quit;
    
	data exp_home_age_x_lapse_fac (keep=home_age lapse factor);
	set r356;
	if f3=. then delete;
	home_age = f1; 
	lapse=0; factor = 'Expense Fee'n; output;
	lapse=1; factor = f3; output;
	run;

	proc sql;
	    create table home_age_adj_r356 as
		select * from home_age;
		update home_age_adj_r356
		set home_age = &max_age_lapse
		where home_age > &max_age_lapse;

	 
		create table out.exp_home_age_x_lapse_fac as
		select p1.casenumber, p1.policynumber, p1.PolicyIssueDate, p1.PolicyEffectiveDate, p1.PropertyYearBuilt, p1.home_age
			, p2.lapse, f.factor as exp_home_age_x_lapse_fac
		from home_age p1 
			inner join lapse p2	on p1.casenumber = p2.casenumber
			inner join home_age_adj_r356 p3 on p1.casenumber = p3.casenumber
			left join exp_home_age_x_lapse_fac f on f.home_age = p3.home_age and f.lapse = p2.lapse;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 356. AGE OF HOME BY LAPSE';
	proc print data=out.exp_home_age_x_lapse_fac;
	where exp_home_age_x_lapse_fac = .;
	run;
	proc sort data=out.exp_home_age_x_lapse_fac; by lapse; run;
	proc gplot data=out.exp_home_age_x_lapse_fac; 
		plot exp_home_age_x_lapse_fac*home_age 
			/ overlay legend vaxis=axis1;
	by lapse;
	run;
	quit;
%end;
%mend;
%agelapse();