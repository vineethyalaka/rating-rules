** RULE 314. ROOF AGE **;
**=====================================================================================**
History: 2015 03 17 JT  Initial draft
		 2015 05 27 SY  Added Hurricane peril indicator
**=====================================================================================**;
%macro RA();
	data roof_age;
	set in.policy_info;
	if PolicyTermYears=1 and &effst=0 then do;
		if PropertyYearRoofInstalled<=0 then roof_age=floor((PolicyIssueDate-date())/365);
		else roof_age=floor((PolicyIssueDate-mdy(1,1,PropertyYearRoofInstalled))/365);
		end;
	else do;
		if PropertyYearRoofInstalled<=0 then roof_age=floor((PolicyEffectiveDate-date())/365);
		else roof_age=floor((PolicyEffectiveDate-mdy(1,1,PropertyYearRoofInstalled))/365);
		end;
	if roof_age=-1 then roof_age=0;
	if roof_age>30 then roof_age=31;
	run;

	data roof_age_fac;
	set fac.'314# Roof Age$'n;
	if f1=. and wind ne . then f1=31;
	if &Hurricane = 1 then hurricane = hurricane;	else hurricane = 0;
	run;

	proc sql;
		create table out.roof_age_fac as
		select p.casenumber, p.policynumber, p.PropertyYearRoofInstalled, p.roof_age
			, f.'Water Weather'n as waterw_roof_age
			, f.wind as wind_roof_age
			, f.hurricane as hurr_roof_age
		from roof_age p left join roof_age_fac f
		 	on p.roof_age=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 314. ROOF AGE';
	proc print data=out.roof_age_fac;
	where waterw_roof_age*wind_roof_age*hurr_roof_age = .;
	run;
	proc gplot data=out.roof_age_fac;
		plot (waterw_roof_age wind_roof_age hurr_roof_age)*roof_age 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.roof_age_fac;
		histogram roof_age;
	run;
%end;
%mend;
%RA();