** RULE 324. SQUARE FOOTAGE PER STORY **;
%macro SFPS();
	data sqft_story;
	set in.policy_info;
	if propertystories = 0 or propertysquarefootage = 0 then sqft_story = 1000;
	else sqft_story=round(propertysquarefootage/propertystories,1);
	run;
	data sqft_story_fac;
	set fac.'324# Sq# Ft# per Story$'n;
	if f3=. and wind ne . then f3=99999;
	if f1=. and f3=. then delete;
	run;
	proc sql;
		create table out.sqft_story_fac as
		select p.casenumber, p.policynumber, p.propertysquarefootage, p.propertystories, p.sqft_story
			, f.wind as wind_sqft_story
			, f.hail as hail_sqft_story
		from sqft_story p left join sqft_story_fac f
			on p.sqft_story>=f.f1
			and p.sqft_story<=f.f3;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 324. SQUARE FOOTAGE PER STORY';
	proc print data=out.sqft_story_fac;
	where wind_sqft_story*hail_sqft_story= .;
	run;
	proc gplot data=out.sqft_story_fac;
		plot (wind_sqft_story hail_sqft_story)*sqft_story 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.sqft_story_fac;
		histogram sqft_story;
	run;
%end;
%mend;
%SFPS();