
** RULE 301.A COVERAGE E **;
%macro covE();
	proc sql;
		create table out.coverage_e_fac as
		select distinct p.casenumber, p.policynumber, p.coveragee
			, f.'Liability - Bodily injury (Perso'n as liabipi_cove
			, f.'Liability - Property Damage to O'n as liapd_cove
		from in.policy_info p left join fac.'301#A Coverage E$'n f
			on p.coveragee=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 301.A COVERAGE E';
	proc freq data=out.coverage_e_fac;
	tables coveragee
		*liabipi_cove
		*liapd_cove / list missing;
	run;
%end;

%mend;
%covE;