** RULE 301.A COUNTY **;
*2017 09 14 AH Updating to include multiple perils, same as HO3;
%macro county();
	proc sql;
		create table out.county_fac as
		select p.casenumber, p.policynumber, p.countyiD
			, f.Theft as theft_county
			, f.fire as fire_county
			, f.'Water Non-Weather'n as waternw_county
			, f.'All other non-weather'n as othernw_county
			, f.lightning as lightning_county
			, f.'Water Weather'n as waterw_county
			, f.'Cov C - Fire'n as Cov_C_Fire_county
			, f.'Cov C - EC'n as Cov_C_EC_county
			, f.'Liability - Bodily injury (Perso'n as liabipi_county
			, f.'Liability - Bodily injury (Medic'n as liabimp_county
			, f.'Liability - Property Damage to O'n as liapd_county
/*HF9 Perils*/
			, f.'Property Damage due to Burglary'n as PD_Burglary_county 
			, f.'Limited Theft Coverage'n as Limited_Theft_county
		from out.territory p left join fac.'301#A County (Territory)$'n f 
			on p.countyiD = f.F2;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 301.A COUNTY';
	proc freq data=out.county_fac;
	tables countyid*theft_county countyid*PD_Burglary_county countyid*Limited_Theft_county/ 
	list missing;
	run;
%end;
%mend;
%county();
