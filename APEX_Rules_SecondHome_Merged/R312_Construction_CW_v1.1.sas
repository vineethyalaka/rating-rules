** RULE 312. CONSTRUCTION **;
%macro construct();
	data construction; *also used in rule 505 earthquake;
	set in.policy_info;
	length ConstructionType $20.;
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Aluminium','Aluminum');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'EIFS/Synthetic','EIFS/ Synthetic');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Hardy Plank','HardiPlank');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Logs','Log');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Masonry non-combustible','Non-combustible');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Wood shakes','Wooden Shakes');
	if strip(propertyconstructionclass) = "" then propertyconstructionclass = "All Other";
	if upcase(propertyconstructionclass) 
		in ('ALUMINUM SIDING','ASBESTOS'
/*             ,'BRICK VENEER'*/
              ,'CLAPBOARD','EIFS/ SYNTHETIC STUCCO'
			,'HARDIPLANK','HARDIPLANK/CEMENT FIBER','LOG'
/*            ,'STONE VENEER'*/
/*              ,'STUCCO ON FRAME'*/
			,'VINYL SIDING','WOODEN SHAKES','WOOD SIDING') 
		then ConstructionType = "Frame";
	else if upcase(propertyconstructionclass) 
		in ('ADOBE','BLOCK (PAINTED)','BRICK & BLOCK','POURED CONCRETE'
			,'SOLID BRICK','SOLID STONE','STONE & BLOCK','STUCCO ON BLOCK') 
		then ConstructionType = "Masonry";
	else if upcase(propertyconstructionclass) 
		in ('BRICK VENEER','STONE VENEER','STUCCO ON FRAME') 
		then ConstructionType = "Masonry Veneer";
	else if upcase(propertyconstructionclass) in ('FIRE RESISTIVE','NON-COMBUSTIBLE') then ConstructionType = "Superior";
	run;
	proc sql;
		create table out.construction_class_fac as
		select distinct p.casenumber, p.policynumber, p.propertyconstructionclass
			, f.wind as wind_const_class
		from construction p left join fac.'312# Construction$'n f
			on compress(upcase(p.propertyconstructionclass))=compress(upcase(f.f1));
	quit;

%if &Process = 0 %then %do;
	title 'RULE 312. CONSTRUCTION';
	proc freq data=out.construction_class_fac;
	tables 	propertyconstructionclass
		*wind_const_class/ list missing;
	run;
%end;

%mend;
%construct();
