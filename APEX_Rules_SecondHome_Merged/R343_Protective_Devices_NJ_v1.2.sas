** RULE 343. PROTECTIVE DEVICES **;
**=====================================================================================**
History: 2015 03 17 JT  Initial draft
		 2015 05 15 SY  Revise rule number
		 2017 02 20 BF  Adding Smoke Detectors
		 2017 09 14 AH  Second Home changes, incl Smoke Detectors
**=====================================================================================**;
%macro PD();
	data protective_dev_fac;
	set fac.'343# Protective Devices$'n;
	if fire=. then delete;
	run;
	data protective_dev;
	set in.policy_info (keep= casenumber policynumber ProtDevBurglarAlarm ProtDevFireAlarm ProtDevSprinklers ProtDevSmokeDetector);
	length Sprinkler $50. BurglarAlarm $50. FireAlarm $50. SmokeDetector $50.;
	if 		ProtDevSprinklers = 'Y' 		then Sprinkler = 'Sprinkler';
	if 		ProtDevBurglarAlarm = 'Central' then BurglarAlarm = 'Central Burglary Alarm';
	else if ProtDevBurglarAlarm = 'Direct' 	then BurglarAlarm = 'Direct Line to Police Station';
	if 		ProtDevFireAlarm = 'Central' 	then FireAlarm = 'Central Fire Alarm';
	else if ProtDevFireAlarm = 'Direct'	 	then FireAlarm = 'Direct Line to Fire Station';
	if ProtDevSmokeDetector >= 1            then SmokeDetector = 'Smoke detectors';
	run;
	proc sql;
		create table out.protective_dev_fac as
		select p.*
			, case when ProtDevSprinklers = 'N' then 1 else f1.fire end as fire_sprinklers
			, case when FireAlarm = '' then 1 else f2.fire end as fire_alarm
			/*NJ HF9 changes (smoke detector)*/
			, case when SmokeDetector = '' then 1 else f4.fire end as fire_prot_dev_smoke
			, calculated fire_sprinklers * calculated fire_alarm * calculated fire_prot_dev_smoke as fire_prot_dev
			, case when BurglarAlarm = '' then 1 else f3.theft end as theft_prot_dev
			, case when ProtDevSprinklers = 'N' then 1 else f1.'Cov C - Fire'n end as Cov_C_fire_sprinklers


/*CW HF9 changes*/
, case when ProtDevSprinklers = 'N' then 1 else f1.'Cov C - Fire'n end as Cov_C_fire_sprinklers
, case when FireAlarm = '' then 1 else f2.'Cov C - Fire'n end as Cov_C_fire_alarm
/*NJ HF9 changes (smoke detector)*/
, case when SmokeDetector = '' then 1 else f4.fire end as Cov_C_fire_prot_dev_smoke
, calculated Cov_C_fire_sprinklers * calculated Cov_C_fire_alarm * calculated Cov_C_fire_prot_dev_smoke as Cov_C_fire_prot_dev
, case when BurglarAlarm = '' then 1 else f3.'Property Damage due to Burglary'n end as PD_Burg_prot_dev
, case when BurglarAlarm = '' then 1 else f3.'Limited Theft Coverage'n end as Lim_Theft_prot_dev



		from protective_dev p
			left join protective_dev_fac f1 on p.Sprinkler = f1.F1
			left join protective_dev_fac f2 on p.FireAlarm = f2.F1
			left join protective_dev_fac f3 on p.BurglarAlarm = f3.F1
			left join protective_dev_fac f4 on p.SmokeDetector = f4.F1
		;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 343. PROTECTIVE DEVICES';
	proc freq data=out.protective_dev_fac;
	tables  ProtDevSprinklers*ProtDevFireAlarm*fire_sprinklers*fire_alarm*fire_prot_dev
			ProtDevBurglarAlarm*theft_prot_dev*Cov_C_fire_sprinklers*Cov_C_fire_alarm
			Cov_C_fire_prot_dev*PD_Burg_prot_dev*Lim_Theft_prot_dev / list missing;
	run;
%end;

%mend;
%PD();