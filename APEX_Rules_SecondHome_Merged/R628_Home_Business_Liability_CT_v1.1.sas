** RULE 628. HOME BUSINESS PROPERTY - LIABILITY **;

data R628;
set fac.'628# Business Liability$'n;
rename 'Home Business Liability Factor'n = Business_Type;
if strip(f2)= . then delete;
rename f2 = Min_Receipts;
rename f3 = Max_Receipts;
rename f4 = Min_Visits;
rename f5 = Max_Visits;
rename 'Liability - Bodily Injury (Perso'n = Home_Bus_liabipi;
rename 'Liability - Bodily Injury (Medic'n = Home_Bus_liabimp;
rename 'Liability - Property Damage to O'n = Home_Bus_liabipd;
run;

proc sql;
	create table out.Home_Business_Liability_Fac
	as select e.CaseNumber, e.PolicyNumber, e.Home_Bus_Flag, e.Home_Bus_Type, e.Home_Bus_Receipt, e.Home_Bus_Visits
		, a.Business_Type
		, a.Min_Receipts
		, a.Max_Receipts
		, a.Min_Visits
		, a.Max_Visits
		, case when Home_Bus_Flag = 0 then 0 else a.Home_Bus_liabipi end as Home_Bus_liabipi_fac
		, case when Home_Bus_Flag = 0 then 0 else a.Home_Bus_liabimp end as Home_Bus_liabimp_fac
		, case when Home_Bus_Flag = 0 then 0 else a.Home_Bus_liabipd end as Home_Bus_liabipd_fac
	from in.endorsement_info e
	left join R628 a on (e.Home_Bus_Type = a.Business_Type
					 and e.Home_Bus_Receipt >= a.Min_Receipts
					 and e.Home_Bus_Receipt <= a.Max_Receipts
					 and e.Home_Bus_Visits >= a.Min_Visits
					 and e.Home_Bus_Visits <= a.Max_Visits)
;
quit;