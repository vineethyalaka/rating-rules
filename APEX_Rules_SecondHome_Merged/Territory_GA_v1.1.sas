/*Territory_GA*/

proc sql;
	create table out.territory as
	select p.policynumber
      , p.CaseNumber
      , p.policytermyears
      , p.policyeffectivedate
      , p.countyiD as countyiD
      , p.Census2010 as census_block_group_ID
      , p.LocationID
	from in.policy_info as p;
quit;