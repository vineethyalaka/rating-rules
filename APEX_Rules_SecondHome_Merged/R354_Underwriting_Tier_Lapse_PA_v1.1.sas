** RULE 354. UNDERWRITING TIER BY LAPSE **;
** Add ScoreModel = 3**;
%macro inslapse();
	*PA Specific: UW Tier = Insurance Score x DRC;
	data exp_uw_tier_x_lapse_fac_1 (keep=lapse uw_tier factor);
	set fac.'354#1 UW Tier by Lapse$'n;
	if f3=. then delete;
	lapse=0; factor = 'Expense Fee'n; output;
	lapse=1; factor = f3; output;
	rename f1 = uw_tier;
	run;

	data exp_uw_tier_x_lapse_fac_2 (keep=lapse uw_tier factor);
	set fac.'354#2 UW Tier by Lapse$'n;
	if f3=. then delete;
	lapse=0; factor = 'Expense Fee'n; output;
	lapse=1; factor = f3; output;
	rename f1 = uw_tier;
	run;

	proc sql;
		create table out.exp_uw_tier_x_lapse_fac as
		select p1.*, p2.matchcode, p2.ScoreModel, p2.insscore, p2.ins_tier, p2.ins_tier_n, p2.drc_class, p2.uw_tier, p2.uw_tier_n
					, coalesce(fa.factor,fb.factor) as exp_uw_tier_x_lapse_fac
		from lapse p1 
			inner join uw_tier p2 on p1.casenumber = p2.casenumber
			left join exp_uw_tier_x_lapse_fac_1 fa 
				on p2.ScoreModel = 2 and fa.lapse = p1.lapse and fa.uw_tier = p2.uw_tier
			left join exp_uw_tier_x_lapse_fac_2 fb 
				on p2.ScoreModel = 3 and fb.lapse = p1.lapse and fb.uw_tier = p2.uw_tier;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 354. UNDERWRITING TIER BY LAPSE';
	proc freq data=out.exp_uw_tier_x_lapse_fac;
	tables lapse*uw_tier_n*exp_uw_tier_x_lapse_fac / list missing;
	run;
%end;
%mend;
%inslapse();