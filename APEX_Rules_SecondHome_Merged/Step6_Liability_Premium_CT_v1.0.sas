** LIABILITY PREMIUM **;
			proc sql;
				create table out.step6_Liability_Premium as
				select *
					, round(liabipi_base_rate
					* liabipi_census
					* liabipi_cove
					* liabipi_ins_score
/*					* liabipi_num_dogs*/
					* liabipi_num_units
					* liabipi_pool
					* liabipi_naogclaim,0.0001)
					as liabipi_base_premium

					, round(liabimp_base_rate
					* liabimp_census
					* liabimp_covf
					* liabimp_ins_score
/*					* liabimp_num_dogs*/
					* liabimp_num_units
					* liabimp_pool
					* liabimp_naogclaim,0.0001)
					as liabimp_base_premium

					, round(liapd_base_rate
					* liapd_census
					* liapd_cove
					* liapd_ins_score
					* liapd_num_res
					* liapd_claimxPeril
					* liapd_claim,0.0001)
					as liapd_base_premium

					, calculated liabipi_base_premium * 
					( 
					+ liabipi_busipursuit
					+ liabipi_personalinjury
					+ liabipi_watercraft
					)
					as liabipi_base_premium_opt

					, calculated liabimp_base_premium * 
					( 
					+ liabimp_busipursuit
					+ liabimp_watercraft
					)
					as liabimp_base_premium_opt

					, calculated liapd_base_premium * 
					( 
					 liapd_watercraft
					)
					as liapd_base_premium_opt

					, round(calculated liabipi_base_premium + calculated liapd_base_premium,1.0) as Liab_Personal
					, round(calculated liabimp_base_premium, 1.0) as Liab_Medical

					, round(round(calculated liabipi_base_premium * liabipi_busipursuit,.0001)
					+ round(calculated liabimp_base_premium * liabimp_busipursuit,.0001),1.0) as BusiPursuit

					, round(round(calculated liabipi_base_premium * liabipi_personalinjury,.0001),1.0) as PersonalInjury

					, round(round(calculated liabipi_base_premium * liabipi_watercraft,.0001)
					+ round(calculated liabimp_base_premium * liabimp_watercraft,.0001)
					+ round(calculated liapd_base_premium * liapd_watercraft,.0001),1.0) as Watercraft

					, ( calculated liabipi_base_premium * Home_Bus_liabipi_fac / liabipi_census) as Home_Bus_Liability_bipi
			, ( calculated liabimp_base_premium * Home_Bus_liabimp_fac / liabimp_census) as Home_Bus_Liability_bimp
			, ( calculated liapd_base_premium   * Home_Bus_liabipd_fac / liapd_census) as Home_Bus_Liability_pd
			, round(calculated Home_Bus_Liability_bipi + calculated Home_Bus_Liability_bimp + calculated Home_Bus_Liability_pd,1.0) as Home_Bus_Liability

					, calculated Liab_Personal + calculated Liab_Medical 
					+ calculated BusiPursuit + calculated PersonalInjury + calculated Watercraft +calculated Home_Bus_Liability
					as Total_Liability_Premium

				from out.base_rate f1
					inner join out.census_fac				 f3 on f1.casenumber=f3.casenumber
					inner join out.coverage_e_fac			 f4 on f1.casenumber=f4.casenumber
					inner join out.coverage_f_fac			 f5 on f1.casenumber=f5.casenumber
					inner join out.ins_score_fac			 f7 on f1.casenumber=f7.casenumber
/*					inner join out.num_dogs_fac				 f8 on f1.casenumber=f8.casenumber*/
					inner join out.num_residents_fac		 f9 on f1.casenumber=f9.casenumber
					inner join out.num_units_fac		    f10 on f1.casenumber=f10.casenumber
					inner join out.priorclaim_NAOG_fac		f11 on f1.casenumber=f11.casenumber
					inner join out.priorclaim_xPeril_fac	f12 on f1.casenumber=f12.casenumber
					inner join out.priorclaim_Peril_fac		f13 on f1.casenumber=f13.casenumber
					inner join out.lia_busipursuit_fac		f16 on f1.casenumber=f16.casenumber
					inner join out.lia_personalinjury_fac	f17 on f1.casenumber=f17.casenumber
					inner join out.lia_watercraft_fac		f18 on f1.casenumber=f18.casenumber
					inner join out.Home_Business_Liability_Fac f19 on f1.casenumber=f19.casenumber
					inner join out.pool_fac f20 on f1.casenumber=f20.casenumber
					;
			quit;
