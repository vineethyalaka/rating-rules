** RULE 313. AGE OF HOME **;
data home_age;
	*also used in rule 352;
	set in.policy_info;

	if PolicyTermYears=1 and &effst=0 then
		do;
			if PropertyYearBuilt=. then
				home_age=year(PolicyIssueDate)-year(date());
			else home_age=year(PolicyIssueDate)-PropertyYearBuilt;
		end;
	else
		do;
			if PropertyYearBuilt=. then
				home_age=year(PolicyEffectiveDate)-year(date());
			else home_age=year(PolicyEffectiveDate)-PropertyYearBuilt;
		end;

	if home_age=-1 then
		home_age=0;

	if home_age>100 then
		home_age=101;
run;

data home_age_fac;
	set fac.'313# Age of Home$'n;

	if f1=. and fire ne . then
		f1=101;
run;

proc sql;
	create table out.home_age_fac as
		select p.casenumber, p.policynumber, p.PolicyIssueDate, p.PolicyEffectiveDate
			, p.PropertyYearBuilt, p.home_age
			, f.fire as fire_home_age
			, f.'Water Non-Weather'n as waternw_home_age
			, f.'All Other Non-Weather'n as othernw_home_age
			, f.'Water Weather'n as waterw_home_age
			, f.wind as wind_home_age
			, f.hail as hail_home_age
			, f.hurricane as hurr_home_age
		from home_age p left join home_age_fac f
			on p.home_age=f.f1;
quit;