** Rule 541 Service Line **;

/*ITR 14094/13846 MD/IN Replacement Cost in Base and Equipment Breakdown Addition */

	data r541;
	set fac.'541# Service Line$'n;
	if strip(f1) = '' then delete;
	Rate = input(f2, comma9.);
	run;
	proc sql;
		create table out.Service_Line_Rate as
		select p.CaseNumber, p.PolicyNumber, p.PolicyFormNumber, p.Service_Line_Flag
			  , case 
					when p.Service_Line_Flag = 1 then f.Rate
					else 0 
				end as Service_Line_Rate 
		from in.policy_info p, r541 f;
	quit;