** RULE 205. MINIMUM PREMIUM **;

**=====================================================================================**
History: 2017 10 06 WZ      Version Control: Extract directly from RatingQA IL Apex Rating Engine
**=====================================================================================**;

%macro minprem();
proc sql;
create table out.min_premium as
select F2 as min_premium from fac.'205# Minimum Premium$'n where F2 is not null;
quit;
%mend;
%minprem();