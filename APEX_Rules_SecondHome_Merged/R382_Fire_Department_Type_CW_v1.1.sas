/*
1. ITR 14866 NY Mosaic - Wanwan - Initial file
2. ITR 15050 CO Mosaic - Vamsi - clean up the sheet, change proc sql to data step, 
								 change policy info department types to All Other when they are not equal to 'Career'
*/

data r382_sheet_manipulation;
	set fac.'382# Fire Department Type$'n(keep= f1 Fire);
	where Fire is not missing;
run;

data r382_policy_info_manipulation;
	set in.policy_info;
	length FS1DepartmentType_Updated $ 20.;
	if FS1DepartmentType ne 'Career' then FS1DepartmentType_Updated='All Other';
	else FS1DepartmentType_Updated=FS1DepartmentType;
run;

/*proc sql fails when column is not available, but data step can process it*/

/*proc sql;*/
/*	create table r382_temp as*/
/*		select *,*/
/*			case when FS1DepartmentType ^= 'Volunteer' then 'All Other' else FS1DepartmentType end as FS1DepartmentType_updated*/
/*		from in.policy_info;*/
/*quit;*/

proc sql;
	create table out.r382_fac as
		select p.* 
			, s.fire as fire_dep_type 
		from r382_policy_info_manipulation p
		left join r382_sheet_manipulation s
			on s.f1=p.FS1DepartmentType_updated;
quit;

