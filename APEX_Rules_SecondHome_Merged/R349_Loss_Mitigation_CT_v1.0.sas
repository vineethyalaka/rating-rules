** RULE 349. OPENING PROTECTION **;
/*CT - Add Loss Mitigation Discount 12/12/2014*/
/*add to CT: mitigation field CONSTRUCTION_COMPLETED*/
data Open_Prot;
	set in.PRODUCTION_INFO;
	length Mitigation $24.;

	/*			if ml_70_number_of_families_01=200 then Mitigation='Impact Resistant Glass';*/
	/*				else if ml_70_number_of_families_01=300 then Mitigation='Basic Storm Shutters';*/
	/*				else if ml_70_number_of_families_01=400 then Mitigation='Hurricane Storm Shutters';*/
	/*				else Mitigation='None'; */
	/*			run;*/
	if CONSTRUCTION_COMPLETED=200 then
		Mitigation='Impact Resistant Glass';
	else if CONSTRUCTION_COMPLETED=300 then
		Mitigation='Basic Storm Shutters';
	else if CONSTRUCTION_COMPLETED=400 then
		Mitigation='Hurricane Storm Shutters';
	else Mitigation='None';
run;

proc sql;
	create table out.mitigation_fac as
		select p.casenumber, p.policynumber, p.mitigation
			, f.wind as wind_mitigation
			, f.hurricane as hurr_mitigation
		from Open_Prot p left join fac.'349# Opening Protection$'n f
			on p.mitigation=f.f1
	;
quit;