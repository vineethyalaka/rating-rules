%macro add();
	data r401;
	set fac.'401# Addl Amount of Insurance$'n;
	pct='Additional Amount of Insurance'n*100;
	run;
proc sql;
		create table out.r401_add_amt_ins as
		select p.casenumber, p.policynumber, p.PolicyFormNumber, p.HO_0420_flag, p.HO_04_20_pct
			, case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 3 then f.'HO3'n  else 1 end as r401_HO_04_20_fac,
			case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 9 then input(f.'HF9-P1'n,8.)
			else 1 end as fire_r401_HO_04_20_fac,
			case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 9 then input(f.'HF9-P3'n,8.)
			else 1 end as waternw_r401_HO_04_20_fac,
			case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 9 then input(f.'HF9-P4'n,8.)
			else 1 end as othernw_r401_HO_04_20_fac,
			case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 9 then input(f.'HF9-P5'n,8.)
			else 1 end as lightning_r401_HO_04_20_fac,
			case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 9 then input(f.'HF9-P6'n,8.)
			else 1 end as waterw_r401_HO_04_20_fac,
			case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 9 then input(f.'HF9-P7'n,8.)
			else 1 end as wind_r401_HO_04_20_fac,
			case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 9 then input(f.'HF9-P8'n,8.)
			else 1 end as hail_r401_HO_04_20_fac,
			case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 9 then input(f.'HF9-P12'n,8.)
			else 1 end as PD_Burg_r401_HO_04_20_fac
		from in.Endorsement_Info p left join r401 f
			on p.HO_04_20_pct=f.pct;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 401. ADDITIONAL AMOUNT OF INSURANCE';
	proc freq data=out.r401_add_amt_ins;
	tables HO_04_20_pct*r401_HO_04_20_fac*PolicyFormNumber / list missing;
	run;
%end;
%mend;
%add();
