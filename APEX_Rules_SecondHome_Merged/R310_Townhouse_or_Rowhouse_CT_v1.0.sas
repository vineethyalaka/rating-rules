** RULE 310. TOWNHOUSE OR ROWHOUSE **;
data rowhouse;
	set in.policy_info;

	if propertyunitsinbuilding >=2 then
		multifam='2+';
	else multifam='1';
run;

proc sql;
	create table out.rowhouse_fac as
		select p.CaseNumber, p.PolicyNumber, p.PropertyTypeofDwelling, p.propertyunitsinbuilding, p.multifam
				, 
			case 
				when p.PropertyTypeofDwelling in ('Rowhouse','Townhouse') then f.fire 
				else 1 
			end as fire_rowhouse
				, 
			case 
				when p.PropertyTypeofDwelling in ('Rowhouse','Townhouse') then f.'Water Non-Weather'n 
				else 1 
			end as waternw_rowhouse
		from rowhouse p left join fac.'310# Townhouse or Rowhouse$'n f
			on p.multifam=f.f1;
quit;