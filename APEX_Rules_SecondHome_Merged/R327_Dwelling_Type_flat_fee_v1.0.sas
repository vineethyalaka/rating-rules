** RULE 327. DWELLING TYPE **;

**=====================================================================================**
Notes:
9/6/2017 NAG Updated fields used to define sec_residence and added HO3-Seasonal/Secondary as an option  
10/27/2017: 1. Extend the length of the variable of sec_residence to 25 digits.
            2. Put 'HO3 - Seasonal/Secondary' to sec_residence for HO3 Secondary home condition.
**=====================================================================================**;
*Di: add compress function to remove blanks when joining table through sec_residence type;

%macro sshome();
	data sec_residence;
	set in.policy_info;
	length sec_residence $25.;
	if Secondary_Home_flag = 0 and PolicyFormNumber = 9 then sec_residence='HF9-A';
	else if Secondary_Home_flag = 1 and PolicyFormNumber = 3 then sec_residence='HO3 - Seasonal/Secondary';
	else if Secondary_Home_flag = 1 and PolicyFormNumber = 9 then sec_residence='HF9-B';
	else if Secondary_Home_flag = 2 and PolicyFormNumber = 9 then sec_residence='HF9-C';
	else sec_residence='HO3  ';
	run;

	proc sql;
		create table out.Seasonal_Secondary_Fac as
		select p.CaseNumber, p.PolicyNumber
			, p.Secondary_Home_flag
			, p.sec_residence
			, f.fire as fire_sec_res
			, f.Theft as theft_sec_res
			, f.'Water Non-Weather'n as waternw_sec_res
			, f.'All Other Non-Weather'n as othernw_sec_res
			, f.Lightning as lightning_sec_res
			, f.'Water Weather'n as waterw_sec_res
			, f.Wind as wind_sec_res
			, f.Hail as hail_sec_res
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_sec_res
			, f.'Cov C - Fire'n as Cov_C_Fire_sec_res
			, f.'Cov C - EC'n as Cov_C_EC_sec_res
			, f.'Property Damage due to Burglary'n as PD_Burg_sec_res
			, f.'Liability - Bodily Injury (Perso'n as Liab_BI_sec_res
			, f.'Liability - Bodily Injury (Medic'n as Liab_MedPay_sec_res
			, f.'Liability - Property Damage to O'n as Liab_PD_sec_res
			, f.'Water Back-up'n as WBU_sec_res
			, f.'Limited Theft Coverage'n as Lim_Theft_sec_res
/*			, f.'Expense Fee'n as Exp_Fee_sec_res*/

		from sec_residence p left join fac.'327# Dwelling Type$'n f
		on compress(p.sec_residence,' ')=compress(substr(f1,1,24),' ');
	quit;

%if &Process = 0 %then %do;
	title 'RULE 327. DWELLING TYPE';
	proc freq data=out.Seasonal_Secondary_Fac;
	tables sec_residence
			*fire_sec_res
			*theft_sec_res
			*waternw_sec_res*othernw_sec_res
			*lightning_sec_res
			*waterw_sec_res
			*wind_sec_res
			*hail_sec_res					
			*hurr_sec_res
			*Cov_C_Fire_sec_res
			*Cov_C_EC_sec_res
			*PD_Burg_sec_res
			*Liab_BI_sec_res
			*Liab_MedPay_sec_res
			*Liab_PD_sec_res
			*WBU_sec_res
			*Lim_Theft_sec_res
			*Exp_Fee_sec_res/list missing;
	run;
%end;

%mend;
%SShome();
