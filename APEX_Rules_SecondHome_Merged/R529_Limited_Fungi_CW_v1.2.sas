** RULE 529. LIMITED FUNGI COVERAGE **;

**Version History:
 	12 29 2016 	XX	Initial Draft
	09 26 2017	JL	Corrected code to replace Special Computer Coverage flag with Limited Fungi flag and to include rates for HF9-A and HF9-B;

%macro sc();
	data r529;
	set fac.'529# Limited Fungi$'n;
	if strip(f1) = '' then delete;
	run;
	proc sql;
		create table out.Limited_Fungi_Rate as
		select unique 
			p.CaseNumber, p.PolicyNumber, 
			p.HO_04_32,  /*Need to get a field for limited fungi*/
			p.PolicyFormNumber , Secondary_Home_Flag,
			case when p.HO_04_32="1" and p.PolicyFormNumber = 9 and Secondary_Home_Flag = 0 then round(f.'HF9-A'n,1.0) 
				 when p.HO_04_32="1" and p.PolicyFormNumber = 9 and Secondary_Home_Flag = 1 then round(f.'HF9-B'n,1.0) 
				 when p.HO_04_32="1" and p.PolicyFormNumber = 9 and Secondary_Home_Flag = 2 then round(f.'HF9-C'n,1.0) 
			else 0 end as Limited_Fungi_Rate
		from r529 f,  in.PRODUCTION_INFO p
		join in.Policy_Info i on p.casenumber= i.casenumber;
	quit;

/**/
/*	data r529;*/
/*	set fac.'529# Limited Fungi$'n;*/
/*	if strip(f1) = '' then delete;*/
/*	run;*/
/*	proc sql;*/
/*		create table out.Limited_Fungi_Rate as*/
/*		select p.CaseNumber, p.PolicyNumber, */
/*		case when  p.PolicyFormNumber = 9 and Secondary_Home_Flag = 0 then round(f.'HF9-A'n,1.0) */
/*		when p.PolicyFormNumber = 9 and Secondary_Home_Flag = 1 then round(f.'HF9-B'n,1.0) */
/*		when p.PolicyFormNumber = 9 and Secondary_Home_Flag = 2 then round(f.'HF9-C'n,1.0) */
/*		else 0 end as Limited_Fungi_Rate*/
/*		from r529 f,  in.Endorsement_Info p*/
/*			join in.Policy_Info i on p.casenumber= i.casenumber;*/
/*	quit;*/
%if &Process = 0 %then %do;/
	title 'RULE 529. LIMITED FUNGI COVERAGE';
	proc freq data=out.Limited_Fungi_Rate;
	tables HO_0414_FLAG*Limited_Fungi_Rate / list missing;
	run;
%end;
%mend;
%sc();
