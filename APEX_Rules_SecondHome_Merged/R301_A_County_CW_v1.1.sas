** RULE 301.A COUNTY **;
%macro county();
	proc sql;
		create table out.county_fac as
		select p.casenumber, p.policynumber, p.countyiD
			, f.Theft as theft_county, f.Hail as hail_county, f.'Property Damage due to Burglary'n as PD_Burglary_county,
			f.'Limited Theft Coverage'n as Limited_Theft_county
		from out.territory p left join fac.'301#A County (Territory)$'n f 
			on p.countyiD = f.F2;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 301.A COUNTY';
	proc freq data=out.county_fac;
	tables countyid*theft_county countyid*PD_Burglary_county countyid*Limited_Theft_county/ 
	list missing;
	run;
%end;
%mend;
%county();
