%macro cap_non_broll_renewal();
libname CapAcc "\\cambosvnxcifs01\ITRatingTeam\Capping";
	
proc sql;
	create table out.step9_ratio as
		select p1.*,  c.Current_Rate_Stab_Capping_Factor
			/* Find out if a policy is non-broll or not */
			, case when c.PolicyAccountNumber not in (select AccountNumber from CapAcc.BROLL_ACCOUNT_NUMBER) then 1 
				   else 0 
			  end as Non_Broll_Policy

			/* Find out if a policy is Renewal or New */
			, case when calculated Non_Broll_Policy = 1 and c.PolicyTermYears <> 1 then 1 
				   when calculated Non_Broll_Policy = 1 and c.PolicyTermYears = 1 then 0
				   else . 
			  end as Renewal_Business

			/* Find out if a policy is Amend. Need to apply renewal cap factors when not an amend*/
			, case when calculated Renewal_Business = 1 and p.POLICY_TRANS_TYPE = 1 then 1
			  	   when calculated Renewal_Business = 1 and p.POLICY_TRANS_TYPE <> 1 then 0
				   else .
			  end as Amended_Policy

			/* If amended(which also means its renewed), then term cannot be 1. So grab temp premium*/
			, case when calculated Amended_Policy = 0 then t.temp_premium
				   else .
			  end as temp_premium

			/* Premium change ratio = Uncapped prem divided by temp prem*/
			, case when calculated Amended_Policy = 0 and round(p1.uncapped_calc_premium/calculated temp_premium,.01) > 10 then 11
				   when calculated Amended_Policy = 0 then round(p1.uncapped_calc_premium/calculated temp_premium,.01)
				   else . 
			  end as change_ratio

		from out.step8_uncapped_calc_premium p1
			inner join in.capping_info c on p1.casenumber = c.casenumber
			inner join in.policy_info p on p1.casenumber = p.casenumber
			inner join out.temp_premium t on p1.casenumber = t.casenumber
quit;

proc sql;
		create table out.step9_capping_factor as
		select p1.*, rc.ratio as rc_ratio, rc.factor as rc_factor
			/*get the capping factor*/
			, case when  p1.Non_Broll_Policy = 0 then 1
				   when  p1.Renewal_Business = 0 then 1 
				   when  p1.Amended_Policy = 1 then p1.Current_Rate_Stab_Capping_Factor  
				   when  p1.Renewal_Business = 1 and p1.Amended_Policy = 0 and  then rc.factor
				   else .
				   end as capping_factor
			/*get the capped premium*/
			
		from out.step9_ratio p1
			left join work.renewal_capping_factors rc 
				on p1.change_ratio = rc.ratio and p1.PolicyEffectiveDate between rc.after and rc.before
			;
quit;

%mend;