** RULE 344. AFFINITY MARKETING **;
*account numbers for partners are placeholders;
/*The General Account Code: 1910, 1911*/
				data affinity; 
				set in.policy_info (keep=casenumber policynumber policyaccountnumber autopolicynumber);
				if policyaccountnumber in (1020:1026, 1051, 1550:1553, 1610, 1920:1921
										 , 2000:2199, 2300:2699, 3300:3899, 4000:4499
										 , 7010, 7030, 8000:8999, 11000, 13800:13899
										 , 14000:14500
										 , 16000
										 , 13200:13299
										 , 14900:14999
										 , 60500:60599
)
					then partner = 1;
				else partner = 0;
				if autopolicynumber = '' then auto = 0;
				else auto = 1;
				run;
				data affinity_fac;
				*set fac.'344# Affinity Marketing$'n;
				set fac.'''344# partner discounts$'''n;
				if f1 = '' then delete;
				run;

proc sql;
		create table out.affinity_fac as
		select p.*, a.partnerflag, 
case 
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/
		
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.1 /*Auto Partner*/
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.1 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.1 /*Auto Partner*/
		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.1 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.1 /*Auto Partner*/
		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.1 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.1 /*Auto Partner*/
		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.1 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.1 /*Auto Partner*/
		when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.1 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=52400 and a.PolicyAccountNumber<=52499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=52400 and a.PolicyAccountNumber<=52499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=52400 and a.PolicyAccountNumber<=52499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.1 /*Auto Partner*/
		when a.PolicyAccountNumber>=52400 and a.PolicyAccountNumber<=52499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.1 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=52600 and a.PolicyAccountNumber<=52699 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=52600 and a.PolicyAccountNumber<=52699 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=52600 and a.PolicyAccountNumber<=52699 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.1 /*Auto Partner*/
		when a.PolicyAccountNumber>=52600 and a.PolicyAccountNumber<=52699 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.1 /*Affiliate Auto Discounts*/



else auto*partner*0.1
end as affinity
		from affinity p inner join in.policy_info a on p.casenumber=a.casenumber;
quit;