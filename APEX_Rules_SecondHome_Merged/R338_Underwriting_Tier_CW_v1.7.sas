/* ITR 14678 CT Mosaic - R338 sheet doesn't show up in Fac folder. Hence referenced it using xlsx engine(with rating engine file) 
						 into fac_338 library. All the columns show up as character columns. This code modifies them to numeric.	*/

%macro UNDW();
	data uw_tier_fac;
	retain f1;
	set fac_r338.'338. UNDERWRITING BUSINESS TIER'n (firstobs=2);
	f1 = input(A, 3.);
	drop A;
	run;

	proc sql;
		create table out.uw_tier_fac as
		select p.casenumber, p.policynumber, p.PolicyIssueDate, p.PolicyEffectiveDate
			,p.undwtier
			,input(f.fire, 1.0) as fire_uw_tier
			,input(f.theft, 1.0) as theft_uw_tier
			,input(f.'Water Non-Weather'n, 1.0) as waternw_uw_tier 
			,input(f.'All Other Non-Weather'n, 1.0) as othernw_uw_tier
			,input(f.lightning, 1.0) as lightning_uw_tier 
			,input(f.'Water Weather'n, 1.0) as Waterw_uw_tier 
			,input(f.Wind, 1.0) as wind_uw_tier
			,input(f.hail, 1.0) as hail_uw_tier
			%if &Hurricane = 1 %then %do;  
			,input(f.hurricane, 1.0) as hurr_uw_tier
			%end;
/*			,input(f.'Cov C - Fire'n, 1.0) as Cov_C_Fire_uw_tier*/
/*			,input(f.'Cov C - EC'n, 1.0) as Cov_C_EC_uw_tier*/
/*			,input(f.'Property Damage due to Burglary'n, 1.0) as PD_Burg_uw_tier*/
			,input(f.'Liability - Bodily Injury (Perso'n, 1.0) as Liab_BI_uw_tier
			,input(f.'Liability - Bodily Injury (Medic'n, 1.0) as Liab_MedPay_uw_tier
			,input(f.'Liability - Property Damage to O'n, 1.0) as Liab_PD_uw_tier
			,input(f.'Water Back-up'n, 1.0) as WBU_uw_tier
/*			,input(f.'Limited Theft Coverage'n, 1.0) as Lim_Theft_uw_tier*/
			,input(f.'Expense Fee'n, 1.0) as Exp_Fee_uw_tier
		from in.policy_info p left join uw_tier_fac f
		on p.undwtier = f.f1;
	quit;

%mend;
%UNDW();


