** RULE 612. WATERCRAFT **;
%macro wc();
	data r612fac;
	set fac.'612# Watercraft$'n;
	if f1=. then delete;
	if f1=0 then length='<=15';
	else length = '>15';
	run;
	data watercraft; 
	set in.Endorsement_Info 
	(keep= casenumber policynumber PolicyFormNumber HO_2475:);
	length length_1 - length_5 $4.;
	array in_use_to 	(5) HO_2475_IN_USE_TO_MONTH_01	 - HO_2475_IN_USE_TO_MONTH_05;
	array in_use_from 	(5) HO_2475_IN_USE_FROM_MONTH_01 - HO_2475_IN_USE_FROM_MONTH_05;
	array time_in_use 	(5) time_in_use_1				 - time_in_use_5;
	array hp1		 	(5) HO_2475_HORSEPOWER_SPEED_01	 - HO_2475_HORSEPOWER_SPEED_05;
	array hp2		 	(5) HO_2475_HORSE_SPEED_02_01	 - HO_2475_HORSE_SPEED_02_05;
	array hp		 	(5) hp_1						 - hp_5;
	array boattype	 	(5) HO_2475_BOATTYPE_01			 - HO_2475_BOATTYPE_05;
	array length1		(5) HO_2475_LENGTH_01			 - HO_2475_LENGTH_05;
	array len			(5) length_1					 - length_5;
	do i = 1 to 5;
		if in_use_from(i) in (0, .) or PolicyFormNumber = 9 then time_in_use(i) = 0;
		else if in_use_to(i) = 12 and in_use_from(i) = 12 then time_in_use(i) = 1;
		else if in_use_to(i) > in_use_from(i) then time_in_use(i) = (in_use_to(i)-in_use_from(i)+1)/12;
		else time_in_use(i) = (in_use_to(i)+12-in_use_from(i)+1)/12;
		hp(i) = hp1(i) + hp2(i);
		if 		0  <= length1(i) <= 15 then len(i) = '<=15';
		else if 16 <= length1(i) <= 26 then len(i) = '>15';
		else len(i) = 'N/A';
		*covered under base policy;
		if hp(i)<25 or boattype(i)='SAIL' then do;
			hp(i)=25;
			time_in_use(i)=0;
			end;
	end;
	drop i;
	run;
	proc sql;
		create table out.lia_watercraft_fac as
		select p.*
			, f1.'Liability - Bodily injury (Perso'n * time_in_use_1 as liabipi_watercraft_1
			, f2.'Liability - Bodily injury (Perso'n * time_in_use_2 as liabipi_watercraft_2
			, f3.'Liability - Bodily injury (Perso'n * time_in_use_3 as liabipi_watercraft_3
			, f4.'Liability - Bodily injury (Perso'n * time_in_use_4 as liabipi_watercraft_4
			, f5.'Liability - Bodily injury (Perso'n * time_in_use_5 as liabipi_watercraft_5

			, f1.'Liability - Bodily injury (Medic'n * time_in_use_1 as liabimp_watercraft_1
			, f2.'Liability - Bodily injury (Medic'n * time_in_use_2 as liabimp_watercraft_2
			, f3.'Liability - Bodily injury (Medic'n * time_in_use_3 as liabimp_watercraft_3
			, f4.'Liability - Bodily injury (Medic'n * time_in_use_4 as liabimp_watercraft_4
			, f5.'Liability - Bodily injury (Medic'n * time_in_use_5 as liabimp_watercraft_5

			, f1.'Liability - Property Damage to O'n * time_in_use_1 as liapd_watercraft_1
			, f2.'Liability - Property Damage to O'n * time_in_use_2 as liapd_watercraft_2
			, f3.'Liability - Property Damage to O'n * time_in_use_3 as liapd_watercraft_3
			, f4.'Liability - Property Damage to O'n * time_in_use_4 as liapd_watercraft_4
			, f5.'Liability - Property Damage to O'n * time_in_use_5 as liapd_watercraft_5

		from watercraft p
		left join r612fac f1
		on length_1 = f1.length
		and p.hp_1 >= f1.f4
		and p.hp_1 <= f1.f6
		left join r612fac f2
		on length_2 = f2.length
		and p.hp_2 >= f2.f4
		and p.hp_2 <= f2.f6
		left join r612fac f3
		on length_3 = f3.length
		and p.hp_3 >= f3.f4
		and p.hp_3 <= f3.f6
		left join r612fac f4
		on length_4 = f4.length
		and p.hp_4 >= f4.f4
		and p.hp_4 <= f4.f6
		left join r612fac f5
		on length_5 = f5.length
		and p.hp_5 >= f5.f4
		and p.hp_5 <= f5.f6;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 612. WATERCRAFT';
	proc print data=out.lia_watercraft_fac;
	where HO_2475_BOATTYPE_01^='';
	run;
%end;
%mend;
%wc();
