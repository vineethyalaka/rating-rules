** RULE 316. ROOF AGE BY ROOF TYPE **;
%macro RART();
	data roof_age_type_fac;
	set fac.'316# Roof Age by Roof Type$'n;
	if f1=. and f2 ne ' ' then f1=31;
	run;
	proc sql;
		create table out.roof_age_type_fac as
		select p1.casenumber, p1.policynumber, p1.roof_age, p2.propertyrooftypecode
			, f.hail as hail_roof_age_type
		from roof_age p1 
			left join roof_type			p2 on p1.casenumber=p2.casenumber
			left join roof_age_type_fac f on p1.roof_age=f.f1 and lowcase(p2.propertyrooftypecode)=lowcase(f.f2);
	quit;

%if &Process = 0 %then %do;
	title 'RULE 316. ROOF AGE BY ROOF TYPE';
	proc print data=out.roof_age_type_fac;
	where hail_roof_age_type= .;
	run;
	proc sort data=out.roof_age_type_fac; by propertyrooftypecode; run;
	proc gplot data=out.roof_age_type_fac;
		by propertyrooftypecode;
		plot hail_roof_age_type*roof_age 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
%end;
%mend;
%RART();