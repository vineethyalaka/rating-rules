/*2018/11/01 TM change from hard code to read in table
Assumption: hit = 1, no hit = 0, no score = -1; model 2 is TU model, model 3 is TR model*/
/*Wanwan: change no hit = 0, no score = -1*/
/*Two score models*/



/*Xi created to map insurance score class for rate stabilization. 
TR -> TU  should use the mapped class
TU -> TU  no mapping, use the original rate sheet*/

data work.R333;
	set fac.'333# Insurance Risk Score$'n;
	if fire =. then delete;

	/* Parse score range to 2 columns, one score model take in F1, 2 score models take in F1, F3*/
	array parsed_tu(2) tu1-tu2;
	i=1;
	do while(scan(F1,i)ne"");
	parsed_tu(i) = scan(F1,i);
	i+1;
	end;

	drop i;
	rename tu1=TU_low tu2=TU_high;
run;


PROC SQL;
	update R333
	     set TU_high = 9999
		 where F1 like '%Above';

	select min(TU_low)
 		into :mintu
        from R333;
		 
	update R333
	     set TU_high = &mintu, TU_low = -100
		 where F1 like '%Below';

	update R333
	     set TU_high =0, TU_low = 0
		 where F1 = 'No Score';

	update R333
	     set TU_high =-1, TU_low = -1
		 where F1 = 'No Hit';

QUIT;


/*for non-mosaic versions, use mapping to get the insurance score class for TR, use the rate sheet for TU*/


%macro insscore();

proc sql;
create table out.ins_score_fac as
   select p.casenumber, p.policynumber, p.insscore, f.TU_low,f.TU_high,
	      p.policytermyears, p.policyformnumber, p.seqno, p.scoremodel,p.matchcode 
		  , m.TU_TIER as ins_tier
		  , m.TU_TIER as ins_tier_n

		  , f.fire as fire_ins_score
		  , f.theft as theft_ins_score
		  , f.'Water Non-Weather'n as waternw_ins_score
		  , f.'All Other Non-Weather'n as othernw_ins_score
		  , f.lightning as lightning_ins_score
		  , f.'Water Weather'n as waterw_ins_score
		  , f.wind as wind_ins_score
		  , %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_ins_score
		  , f.'Cov C - Fire'n as Cov_C_Fire_ins_score
	 	  , f.'Cov C - EC'n as Cov_C_EC_ins_score
		  , f.'Property Damage due to Burglary'n as PD_Burg_ins_score
		  , f.'Liability - Bodily injury (Perso'n as liabipi_ins_score
		  , f.'Liability - Bodily injury (Medic'n as liabimp_ins_score
		  , f.'Liability - Property Damage to O'n as liapd_ins_score		
		  , f.'Water Back-up'n as waterbu_ins_score
		  , f.'Limited Theft Coverage'n as Lim_Theft_ins_score
/*   from  in.drc_info p, r333 f ,ins_mapping2 m*/
/*   where */
/*	((p.matchcode = m.TU_high)*/
/*    or (p.insscore between m.TU_low and m.TU_high and p.scoremodel = 2 and p.matchcode not in(0,-1))*/
/*    or (p.insscore between m.TR_low and m.TR_high and p.scoremodel = 3 and p.matchcode not in(0,-1)))*/
/**/
/*	and */
/**/
/*	m.TU_Tier = f.F3;*/

	from in.drc_info p, r333 f ,ins_mapping2 m
	where 	((p.matchcode = m.TU_high)
	or (p.insscore between m.TR_low and m.TR_high and p.scoremodel = 3 and p.matchcode not in(0,-1)))
	and m.TU_Tier = f.F3;





	insert into out.ins_score_fac
	select p.casenumber, p.policynumber, p.insscore, f.TU_low,f.TU_high,
	      p.policytermyears, p.policyformnumber, p.seqno, p.scoremodel,p.matchcode 
		  , f.F3 as ins_tier
		  , f.F3 as ins_tier_n

		  , f.fire as fire_ins_score
		  , f.theft as theft_ins_score
		  , f.'Water Non-Weather'n as waternw_ins_score
		  , f.'All Other Non-Weather'n as othernw_ins_score
		  , f.lightning as lightning_ins_score
		  , f.'Water Weather'n as waterw_ins_score
		  , f.wind as wind_ins_score
		  , %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_ins_score
		  , f.'Cov C - Fire'n as Cov_C_Fire_ins_score
	 	  , f.'Cov C - EC'n as Cov_C_EC_ins_score
		  , f.'Property Damage due to Burglary'n as PD_Burg_ins_score
		  , f.'Liability - Bodily injury (Perso'n as liabipi_ins_score
		  , f.'Liability - Bodily injury (Medic'n as liabimp_ins_score
		  , f.'Liability - Property Damage to O'n as liapd_ins_score		
		  , f.'Water Back-up'n as waterbu_ins_score
		  , f.'Limited Theft Coverage'n as Lim_Theft_ins_score

	from in.drc_info p, r333 f
	where (p.insscore between f.TU_low and f.TU_high and p.scoremodel = 2 and p.matchcode not in(0,-1));
	 



   	create table ins_tier as  /**also used in rules 353, 354, step 7*/
	      select i.casenumber, i.policynumber, i.insscore, 
	        	 i.policytermyears, i.policyformnumber, i.seqno, i.scoremodel,
				 i.matchcode, i.ins_tier_n, i.ins_tier
	      from out.ins_score_fac i;*/;
   quit;

%if &Process = 0 %then %do;
	title 'RULE 333. INSURANCE RISK SCORE';
	proc print data=out.ins_score_fac;
	where fire_ins_score*theft_ins_score*waternw_ins_score*othernw_ins_score*lightning_ins_score*waterw_ins_score
	*wind_ins_score*hurr_ins_score*Cov_C_Fire_ins_score*Cov_C_EC_ins_score*PD_Burg_ins_score
	*liabipi_ins_score*liabimp_ins_score*liapd_ins_score*waterbu_ins_score*Lim_Theft_ins_score = .;
	run;
	proc gplot data=out.ins_score_fac;
		plot (fire_ins_score theft_ins_score waternw_ins_score othernw_ins_score lightning_ins_score waterw_ins_score
	 		wind_ins_score hurr_ins_score Cov_C_Fire_ins_score Cov_C_EC_ins_score PD_Burg_ins_score
			liabipi_ins_score liabimp_ins_score liapd_ins_score waterbu_ins_score Lim_Theft_ins_score)*ins_tier_n 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.ins_score_fac;
		histogram ins_tier_n;
	run;
%end;
%mend;
%insscore();
 





