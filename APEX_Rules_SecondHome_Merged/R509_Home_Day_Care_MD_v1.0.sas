*Rule 509 Home Day Care Coverage MD;


%macro daycare();
	data r509_rate;
	set fac.'509# Home Day Care$'n;
	if strip(f1) = '' then delete;
	rename 'Coverage Limit Increment (i#e# r'n = Increment;
	run;
	proc sql;
		create table out.Home_Day_Care_rate as
		select p.CaseNumber, p.PolicyNumber, p.DayCare_Flag, p.Day_Care_AMT
			, case when p.DayCare_Flag="1" then round(p.Day_Care_AMT*f.Rate/Increment,1) else 0 end as Day_Care_rate
		from in.Endorsement_Info p, r509_rate f	;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 509. Home Day Care';
	proc print data=out.Home_Day_Care_rate;
	where Day_Care_rate = .;
	run;
%end;
%mend;
%daycare();


