** RULE 320. PRESENCE OF POOL **;
*update by Di: consider variable type first;
%macro POP();

proc sql;
select type into : type from dictionary.columns where libname="FAC" and memname="'320# Presence of Pool$'" and upcase(name)="THEFT";
quit;

%let type=&type;

%put &type;

%if &type ne num %then %do;
	proc sql;
		create table out.pool_fac as
		select p.casenumber, p.policynumber, p.poolpresent
			, input(f.theft, best8.) as theft_pool
			, input(f.'Water Non-Weather'n, best8.) as waternw_pool
			, input(f.'All Other Non-Weather'n, best8.) as othernw_pool
			, input(f.lightning, best8.) as lightning_pool
			, input(f.'Property Damage due to Burglary'n, best8.) as PD_Burg_pool
			, Case when p.PolicyFormNumber = 9 then 1 else input(f.'Liability - Bodily injury (Perso'n, best8.) end as liabipi_pool
			, Case when p.PolicyFormNumber = 9 then 1 else input(f.'Liability - Bodily injury (Medic'n, best8.) end as liabimp_pool
			, input(f.'Limited Theft Coverage'n, best8.) as Lim_Theft_pool
		from in.policy_info p left join fac.'320# Presence of Pool$'n f
			on p.poolpresent=substr(f1,1,1);
	quit;
%end;

%else %do;
	proc sql;
		create table out.pool_fac as
		select p.casenumber, p.policynumber, p.poolpresent
			, f.theft as theft_pool
			, f.'Water Non-Weather'n as waternw_pool
			, f.'All Other Non-Weather'n as othernw_pool
			, f.lightning as lightning_pool
			, f.'Property Damage due to Burglary'n as PD_Burg_pool
			, Case when p.PolicyFormNumber = 9 then 1 else f.'Liability - Bodily injury (Perso'n end as liabipi_pool
			, Case when p.PolicyFormNumber = 9 then 1 else f.'Liability - Bodily injury (Medic'n end as liabimp_pool
			, f.'Limited Theft Coverage'n as Lim_Theft_pool
		from in.policy_info p left join fac.'320# Presence of Pool$'n f
			on p.poolpresent=substr(f1,1,1);
	quit;
%end;

%mend;
%POP();