** RULE 401. ADDITIONAL AMOUNT OF INSURANCE **;
%macro add();
run;
proc sql;
		create table out.r401_add_amt_ins as
		select p.casenumber, p.policynumber, p.PolicyFormNumber, p.HO_0420_flag, p.HO_04_20_pct
			, case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 3 then f.F2
			when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ and p.PolicyFormNumber = 9 then f.F3
			else 1 end as r401_HO_04_20_fac
		from in.Endorsement_Info p 
		left join (select * from fac.'401# Addl Amount of Insurance$'n where F1 ne .) f on p.HO_04_20_pct=f.F1*100;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 401. ADDITIONAL AMOUNT OF INSURANCE';
	proc freq data=out.r401_add_amt_ins;
	tables HO_04_20_pct*r401_HO_04_20_fac*PolicyFormNumber / list missing;
	run;
%end;
%mend;
%add();
