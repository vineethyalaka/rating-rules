** RULE 512. LOSS OF USE � INCREASED LIMIT **;

	data r512;
	set fac.'512# Loss of Use Inc Lim$'n;
	if f1 = "" then delete;
	rename 'Coverage Limit Increment (i#e# r'n = Increment;
	run;
	proc sql;
		create table out.loss_of_use_rate as
		select p.*
			, round(p.Incr_CovD*f.Rate/Increment,1.0) as loss_of_use_rate
			, p.Incr_CovD*f.Rate/Increment as loss_of_use_rate_nr
		from coverage_d p, r512 f;
	quit; /*modified to latest rule in CT, add loss_of_use_rate_nr*/