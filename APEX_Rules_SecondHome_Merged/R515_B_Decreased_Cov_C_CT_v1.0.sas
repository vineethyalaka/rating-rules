** RULE 515.B REDUCED COVERAGE C **;
				data r515b;
				set fac.'515#B Reduced Coverage C$'n;
				if strip(f1) = '' then delete;
				rename 'Coverage Limit Increment (i#e# C'n = Increment;
				run;
				proc sql;
					create table out.Decr_CovC_rate as
					select p.*
						, round(max(p.Decr_CovC*f.credit/Increment,0),1.0) as Decr_CovC_rate
					from coverage_c p, r515b f;
				quit;