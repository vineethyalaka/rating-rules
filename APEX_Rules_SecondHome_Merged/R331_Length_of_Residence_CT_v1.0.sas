** RULE 331. LENGTH OF RESIDENCE **;
************** ST REVISED 12/4/2014 - REVIEW + COMPARE WITH OTHERS********************;
*added code to account for "Unavailable" rate;
*Change along with pricing rule for SAS;
data lengthresfac;
	set fac.'331# Length of Residence$'n;
	lengthmin = f1+0;
	lengthmax = f3+0;

	if f1="Unavailable" then
		do;
			lengthmin = 0;
			lengthmax = 0;
		end;

	if f1=15.01 and f3=. then
		lengthmax=9999;
run;

data r331;
	set in.production_info;
	format lengthofresidence 10.2;
	lengthofresidence1 = round(lengthofresidence,1.0);
	lengthofresidence1 = lengthofresidence1/100;

	if lengthofresidence1=0 then
		lengthofresidence1=5;
	drop lengthofresidence;
	rename lengthofresidence1=lengthofresidence;
run;

/*			proc sql;*/
/*				create table out.length_res_fac as*/
/*				select p.casenumber, p.policynumber, p.lengthofresidence, p.lengthofresidenceind */
/*					, f.theft as theft_length_res*/
/*					, f.'Water Non-Weather'n as waternw_length_res*/
/*					, f.'Expense Fee'n as expense_length_res*/
/*				from in.production_info p left join lengthresfac f*/
/*					on p.lengthofresidence >= f.lengthmin*/
/*					and p.lengthofresidence <= f.lengthmax;*/
/*			quit;*/
proc sql;
	create table out.length_res_fac as
		select p.casenumber, p.policynumber, p.lengthofresidence label='length of residence', p.lengthofresidenceind 
			, f.theft as theft_length_res
			, f.'Water Non-Weather'n as waternw_length_res
			, f.'Expense Fee'n as expense_length_res
		from r331 p left join lengthresfac f
			on p.lengthofresidence >= f.lengthmin
			and p.lengthofresidence <= f.lengthmax;
quit;