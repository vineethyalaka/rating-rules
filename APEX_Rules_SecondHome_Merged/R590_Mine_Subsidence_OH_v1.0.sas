/*Ohio specific logic*/
/*Output premium field name: Mine_Sub_Rate*/
%macro ms();
** RULE 590. MINE SUBSIDENCE INSURANCE **;
data r590;
set fac.'590# Mine Subsidence$'n;
if f1 = . and f3 = . then delete;
run;
proc sql;
	create table out.Mine_Sub_Rate as
	select p.CaseNumber, p.PolicyNumber, e.HO2388_FLAG, c.countyiD
		, case when e.HO2388_FLAG = '1' or c.countyID in 
			("39009","39013","39019","39029","39031","39053","39059","39067","39073","39075","39079","39081","39087","39099","39105"
			,"39111","39115","39119","39121","39127","39145","39151","39155","39157","39163","39167")
		then f3 else 0 end as Mine_Sub_Rate
	from in.policy_info p
		inner join in.Endorsement_Info e on p.casenumber = e.casenumber
		inner join out.county_fac c on p.casenumber = c.casenumber
		left join r590 f on c.countyiD = f.f2;
quit;

%if &Process = 0 %then %do;
title 'RULE 590. MINE SUBSIDENCE INSURANCE';
proc print data=out.Mine_Sub_Rate;
where Mine_Sub_Rate = .;
run;
quit;
%end;
%mend;
%ms();