** RULE 313. AGE OF HOME **;
**=====================================================================================**
History: 
		2015 03 17 JT  Initial draft
		2015 05 26 SY  Added Hurricane peril indicator
        2018 11 07 TM  Change max home age from hard code to macro variable

Second Home History:
		2016 07 29 MM  Intial draft
**=====================================================================================**;
%macro homeage();
    
    data home_age_fac;
	set fac.'313# Age of Home$'n;
	run;
	
	proc sql;
         select max(f1)+1
		 		into :max_age
	     from home_age_fac;

		 update home_age_fac
		        set f1 = &max_age
				where f1=. and fire ne .;
	quit;

	data home_age; *also used in rule 352, 355, 356; 
	set in.policy_info;
	if PolicyTermYears=1 and &effst=0 then do;
		if PropertyYearBuilt=. then home_age=year(PolicyIssueDate)-year(date());
		else home_age=year(PolicyIssueDate)-PropertyYearBuilt;
		end;
	else do;
		if PropertyYearBuilt=. then home_age=year(PolicyEffectiveDate)-year(date());
		else home_age=year(PolicyEffectiveDate)-PropertyYearBuilt;
		end;
	if home_age=-1 then home_age=0;
	if home_age>&max_age then home_age=&max_age;
	run;

	proc sql;
		create table out.home_age_fac as
		select p.casenumber, p.policynumber, p.PolicyIssueDate, p.PolicyEffectiveDate
			, p.PropertyYearBuilt, p.home_age
			, f.fire as fire_home_age
			, f.'Water Non-Weather'n as waternw_home_age
			, f.'All Other Non-Weather'n as othernw_home_age
			, f.'Water Weather'n as waterw_home_age
			, f.wind as wind_home_age
			, f.hail as hail_home_age
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_home_age
			, f.'Cov C - Fire'n as Cov_C_Fire_home_age

			from home_age p left join home_age_fac f
			on p.home_age=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 313. AGE OF HOME';
	proc print data=out.home_age_fac;
	where fire_home_age*waternw_home_age*othernw_home_age*waterw_home_age*wind_home_age*hail_home_age*hurr_home_age*Cov_C_Fire_home_age = .;
	run;
	proc gplot data=out.home_age_fac;
		plot (fire_home_age waternw_home_age othernw_home_age waterw_home_age wind_home_age hail_home_age hurr_home_age Cov_C_Fire_home_age)*home_age 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.home_age_fac;
		histogram home_age;
	run;
%end;
%mend;
%homeage();
