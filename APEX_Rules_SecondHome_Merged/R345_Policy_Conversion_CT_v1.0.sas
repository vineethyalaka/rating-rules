** RULE 345. POLICY CONVERSION **;
				data policy_conversion; 
				set in.policy_info (keep=casenumber policynumber RolloverCode PolicyTermYears);
				if find(RolloverCode,"homer",'I') > 0
					then conversion = 1;
				else conversion = 0;
				if PolicyTermYears > 6 then term = 6;
				else term = PolicyTermYears;
				run;
				data policy_conversion_fac;
				set fac.'345# Policy Conversion$'n;
				if 'Policy Conversion Discount'n ^= . and term = . then term = 6;
				run;
				proc sql;
				     create table out.policy_conversion_fac as
				     select p.*
				           ,  case when conversion = 1 then 'Policy Conversion Discount'n else 0 end as policy_conversion_fac
				     from policy_conversion p
				           left join policy_conversion_fac f on p.term=f.term;
				quit;