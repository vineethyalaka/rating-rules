/*2019 03 29	Wanwan	Create totally new CW version that apply Rule 370 for year built*/
/*2019 12 27 TM change 'prior to before*/

data R370;
set fac.'370# Year Built$'n;
if F1=. then delete;
Grid_group = f1;
year_built_numeric= input(F2,8.);
/*year_built = F2;*/
hurr_year_built = Hurricane;
run;

proc sql;
   select min(year_built_numeric)
      into :min_year_built
      from R370;
/*%put &min_year_built;*/
proc sql;
   select max(year_built_numeric)
      into :max_year_built
      from R370;
/*%put &max_year_built;*/

proc sql; 
create table R370_Fac as  (
select Grid_group, F2, 
case 
when F2 like '%Before%' then &min_year_built-1
when F2 like '%or Later%' then &max_year_built+1
else input(F2, 8.)
end as year_built,hurr_year_built
from R370 ); 
quit; 
 
proc sql;
create table out.Year_built_Fac as ( 
select temp.casenumber, temp.policynumber,temp.PropertyYearBuilt,temp.YearBuilt,temp.GridGroup,f.hurr_year_built from (
select p.casenumber, p.policynumber,p.PropertyYearBuilt, t.GridGroup,
	case 
        when p.PropertyYearBuilt< &min_year_built then &min_year_built-1
		when p.PropertyYearBuilt> &max_year_built then &max_year_built
		else p.PropertyYearBuilt
	end as YearBuilt
	from in.policy_info p 
    left join out.territory t 
    on t.casenumber = p.casenumber) as temp 
	left join R370_fac f
    on f.year_built = temp.YearBuilt and temp.GridGroup = f.Grid_group);
quit;

