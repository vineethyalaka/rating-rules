** RULE 301.A zipcode **;
%macro zipcode();
	proc sql;
		create table out.zipcode_fac as
		select p.casenumber, p.policynumber, p.ZIP_CODE1
            , f.Fire as fire_zipcode
			, f.Theft as theft_zipcode
			, f.'Water Non-Weather'n as waternw_zipcode
			, f.Lightning as lightning_zipcode
			, f.Wind as wind_zipcode
			, f.hail as hail_zipcode
			, f.'Cov C - Fire'n as Cov_C_Fire_zipcode
			, f.'Cov C - EC'n as Cov_C_EC_zipcode
			, f.'Property Damage due to Burglary'n as PD_Burglary_zipcode
			, f.'Liability - Bodily injury (Perso'n as liabipi_zipcode
			, f.'Liability - Bodily injury (Medic'n as liabimp_zipcode
			, f.'Liability - Property Damage to O'n as liapd_zipcode
			, f.'Water Back-up'n as waterbu_zipcode
			, f.'Limited Theft Coverage'n as Limited_Theft_zipcode
		from in.policy_info p left join fac.'301#A Zip Code$'n f 
			on p.ZIP_CODE1 = f.F1;
	quit;

;
%mend;
%zipcode();
