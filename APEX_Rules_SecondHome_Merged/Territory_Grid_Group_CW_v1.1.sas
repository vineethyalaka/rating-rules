/*2019/06/27 TM Territory with grid group. Getting GRIDLOOPUP data from database */
/*2020/1/3 TM change countyid and census_block_group_id to numeric*/


proc sql;
create table gridlookup as
select *,
GridGroup,
ExpirationDate
from &server..HS_GEOUW_GRID_LOOKUP 
where ExpirationDate = '01JAN2050'd
and Formcode =3/* only need one form, assume all forms have the same GridID*/
and statecode = %tslit(&state); /*add '' to state*/
quit;

proc sql;
create table out.territory as
select 
a.policynumber,
a.casenumber,
a.FIPSID as countyid,
input (a.CensusBlockID, 12.) as census_block_group_id,
a.GridID,
m.locationid,
g.GridGroup,
g.ExpirationDate
from &server..v3ratingrequest as a
inner join &server..v3ratingresult as m on a.casenumber=m.casenumber
left join work.gridlookup g on a.GridID = g.GridID
where  a.casenumber in (select casenumber from in.policy_info);
quit;
