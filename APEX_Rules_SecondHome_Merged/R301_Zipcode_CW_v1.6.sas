** RULE 301.A zipcode **;
/*Add P4 All Other Non-Weather, P6 Water Weather*/

/* ITR 14809 VR - Change f1 column from rate sheet to char data type to match it with column from policy_info 
		   and change limited theft column name to F17*/
/* ITR 14827 DG - Remove P16 Water Back-up*/
/* ITR 14789 YU - Added Water-Back */
data work.R301Zipcode;
	set fac.'301#A Zip Code$'n(firstobs=2);
	f1_char = put(F1, 5.);
	if F1 =. then f1_char='00000';
	drop f1;
	rename f1_char=f1;	
run;

/* temp_zip finds out the cases from policy_info for which zipcode doesn't exist in rate sheet*/
/* policy_info_manipulated creates a new column PropertyZip5_updated which sets 00000 for the cases in temp_zip*/
proc sql;
	create table temp_zip as
		select PropertyZip5 as zip_not_found from in.policy_info
		where PropertyZip5 not in 
								(select f1 from work.R301Zipcode);

	create table policy_info_manipulated as
	select *
			, case when PropertyZip5 in (select zip_not_found from temp_zip) then '00000'
			  	   else PropertyZip5
			  end as PropertyZip5_updated 
	from in.policy_info;

quit;

	

%macro zipcode();
	proc sql;
		create table out.zipcode_fac as
		select p.casenumber, p.policynumber, p.PropertyZip5, p.PropertyZip5_updated
            , f.Fire as fire_zipcode
			, f.Theft as theft_zipcode
			, f.'Water Non-Weather'n as waternw_zipcode
			, f.'All Other Non-Weather'n as othernw_zipcode  
			, f.Lightning as lightning_zipcode
			, f.'Water Weather'n as waterw_zipcode
			, f.Wind as wind_zipcode
			, f.hail as hail_zipcode
			, f.'Cov C - Fire'n as Cov_C_Fire_zipcode
			, f.'Cov C - EC'n as Cov_C_EC_zipcode
			, f.'Property Damage due to Burglary'n as PD_Burglary_zipcode
			, f.'Liability - Bodily injury (Perso'n as liabipi_zipcode
			, f.'Liability - Bodily injury (Medic'n as liabimp_zipcode
			, f.'Liability - Property Damage to O'n as liapd_zipcode
			, f.'Water Back-up'n as waterbu_zipcode
			, f.F17 as Limited_Theft_zipcode
		from policy_info_manipulated p left join R301Zipcode f 
			on p.PropertyZip5_updated = f.f1;
	quit;

;
%mend;
%zipcode();
