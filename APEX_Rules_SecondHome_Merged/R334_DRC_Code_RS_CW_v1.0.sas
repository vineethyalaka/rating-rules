
** RULE 334. DEROGATORY REASON CODE **;
%macro DRC();
	proc sql;

	create table drc_temp as
	select d.*, dm1.f2 as score_12_2, dm2.f2 as score_13_2, dm3.f2 as score_14_2, dm4.f2 as score_15_2
	from in.DRC_INFO d
	left join work.drc_mapping dm1
	on d.score_12_1 = dm1.f1 
	left join work.drc_mapping dm2
	on d.score_13_1 = dm2.f1 
	left join work.drc_mapping dm3
	on d.score_14_1 = dm3.f1 
	left join work.drc_mapping dm4
	on d.score_15_1 = dm4.f1;



	create table drc_temp2 as
	select d.*, 
	case when d.scoremodel = 3 then d.score_12_2 
		 else d.score_12_1 end as score_12, 
	case when d.scoremodel = 3 then d.score_13_2 
		 else d.score_13_1 end as score_13, 
	case when d.scoremodel = 3 then d.score_14_2 
		 else d.score_14_1 end as score_14, 
	case when d.scoremodel = 3 then d.score_15_2 
		 else d.score_15_1 end as score_15
	from drc_temp d;
	
	


	create table drc as 
	select *,
	case when (score_12 not like '%D02%' or score_12 is null) and (score_13 not like '%D02%' or score_13 is null) and 
	           (score_14 not like '%D02%' or score_14 is null) and (score_15 not like '%D02%' or score_15 is null) and
	         ((case when score_12 like '%D%' then 1 else 0 end) + (case when score_13 like '%D%' then 1 else 0 end) +
	          (case when score_14 like '%D%' then 1 else 0 end) + (case when score_15 like '%D%' then 1 else 0 end)) >= 2
	     then 5
	     when (score_12 not like '%D02%' or score_12 is null) and (score_13 not like '%D02%' or score_13 is null) and 
	          (score_14 not like '%D02%' or score_14 is null) and (score_15 not like '%D02%' or score_15 is null) and
	         ((case when score_12 like '%D%' then 1 else 0 end) + (case when score_13 like '%D%' then 1 else 0 end) +
	          (case when score_14 like '%D%' then 1 else 0 end) + (case when score_15 like '%D%' then 1 else 0 end)) = 1
	     then 4
	     when (score_12 like '%D02%' or score_13 like '%D02%' or score_14 like '%D02%' or score_15 like '%D02%') and
	         ((case when score_12 like '%D%' then 1 else 0 end) + (case when score_13 like '%D%' then 1 else 0 end) +
	          (case when score_14 like '%D%' then 1 else 0 end) + (case when score_15 like '%D%' then 1 else 0 end)) >= 2
	     then 3
	     when (score_12 like '%D02%' or score_13 like '%D02%' or score_14 like '%D02%' or score_15 like '%D02%') and
	         ((case when score_12 like '%D%' then 1 else 0 end) + (case when score_13 like '%D%' then 1 else 0 end) +
	          (case when score_14 like '%D%' then 1 else 0 end) + (case when score_15 like '%D%' then 1 else 0 end)) = 1
	     then 2
	     else 1
	end as drc_class
	from drc_temp2;
	quit;
	proc sql;
		create table out.drc_fac as
		select p.casenumber, p.policynumber, p.drc_class
			, f.theft as theft_drc
			, f.'Property Damage due to Burglary'n as PD_Burg_drc
			, f.'Liability - Bodily injury (Perso'n as liabipi_drc
			, f.'Liability - Bodily injury (Medic'n as liabimp_drc
			, f.'Limited Theft Coverage'n as Lim_Theft_drc
		from drc p left join fac.'334# DRC$'n f
			on p.drc_class=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 334. DEROGATORY REASON CODE';
	proc freq data=out.drc_fac;
	tables drc_class
		*theft_drc
		*PD_Burg_drc
		*liabipi_drc
		*liabimp_drc
		*Lim_Theft_drc / list missing;
	run;
%end;

%mend;
%DRC();
