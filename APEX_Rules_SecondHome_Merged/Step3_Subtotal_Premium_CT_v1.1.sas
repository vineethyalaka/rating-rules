/*Remove ACV Roof from formula*/ 
/*Updated rounding 11/20/2014*/ 
/*Add mitigation discount 12/12/2014*/ 
/*Add census (territory) for Hurr 1/2/2015*/
			proc sql;
				create table out.step2_base_premium as
				select *
				 	, fire_base_rate
					* fire_census
					* fire_cova
					* fire_home_age
					* fire_num_res
					* fire_heat_sys
					* fire_ins_score
					* fire_rowhouse
					* fire_sec_res
					* fire_naogclaimxPeril
					* fire_claim
					* fire_ded
					* fire_sp_personal_prop
					* fire_uw_tier
					as fire_base_premium

					, round(calculated fire_base_premium
					* fire_new_home
					* fire_new_home_purch
					* fire_prot_dev,.0001)
					as fire_disc_base_premium


					, theft_base_rate
					* theft_census
					* theft_covcbase
					* theft_cust_age
					* theft_ins_score
					* theft_length_res
					* theft_num_res
					* theft_num_units
					* theft_pool
					* theft_sqft
					* theft_sec_res
					* theft_naogclaimxPeril
					* theft_claim
					* theft_ded
					* theft_sp_personal_prop
					* theft_uw_tier
					as theft_base_premium
				
				
					, round(calculated theft_base_premium
					* theft_prot_dev,.0001)
					as theft_disc_base_premium


					, waternw_base_rate
					* waternw_census
					* waternw_cova	
					* waternw_fin_bsmnt
					* waternw_home_age
					* waternw_ins_score
					* waternw_length_res
					* waternw_num_res
					* waternw_bsmnt
					* waternw_pool
					* waternw_rcsqft
					* waternw_rowhouse
					* waternw_bathroom
					* waternw_sec_res
					* waternw_claimxPeril
					* waternw_claim
					* waternw_found_type_yr_built
					* waternw_num_stories
					* waternw_ded
					* waternw_sp_personal_prop
					* waternw_uw_tier
					as waternw_base_premium
				
					, round(calculated waternw_base_premium
					* waternw_new_home,.0001)
					as waternw_disc_base_premium


					, othernw_base_rate
					* othernw_cova
					* othernw_cust_age
					* othernw_home_age
					* othernw_ins_score
					* othernw_pool
					* othernw_sec_res
					* othernw_naogclaim
					* othernw_claim
					* othernw_ded
					* othernw_sp_personal_prop
					* othernw_uw_tier
					as othernw_base_premium
				
					, round(calculated othernw_base_premium
					* othernw_new_home,.0001)
					as othernw_disc_base_premium


					, lightning_base_rate
					* lightning_census
					* lightning_cova
					* lightning_ins_score
					* lightning_pool
					* lightning_rcsqft
					* lightning_sec_res
					* lightning_ded
					* lightning_sp_personal_prop
					* lightning_naogclaim
					* lightning_claim
					* lightning_fs1_drivedistance
/*					* 0.892 								*/
					* lightning_uw_tier
					as lightning_base_premium
				
					, round(calculated lightning_base_premium
					* lightning_new_home_purch,.0001)
					as lightning_disc_base_premium


					, waterw_base_rate
					* waterw_census /*Add to CT*/
					* waterw_cova		
					* waterw_home_age
					* waterw_ins_score
					* waterw_rcsqft
					* waterw_roof_age
					* waterw_sec_res
					* waterw_claimxPeril
					* waterw_claim
					* waterw_ded
					* waterw_nearest_water_body
/*					* 1.077 								*/
					* Waterw_uw_tier
					as waterw_base_premium
				
					, round(calculated waterw_base_premium
					* waterw_new_roof
					* waterw_new_home,.0001)
					as waterw_disc_base_premium 


					, wind_base_rate
					* wind_census
					* wind_const_class
					* wind_cova
					* wind_home_age
					* wind_ins_score
					* wind_fence
					* wind_rcsqft
					* wind_roof_age
					* wind_roof_type
					* wind_sqft_story
					* wind_sec_res
					* wind_naogclaim
					* wind_claim
					* wind_ded
					* wind_uw_tier
					as wind_base_premium
				
				
					, round(calculated wind_base_premium
					* wind_mitigation
					* wind_new_home
					* wind_new_roof
					* wind_new_home_purch,.0001)
					as wind_disc_base_premium
				
					, hail_base_rate
					* hail_census
					* hail_cova
					* hail_home_age
					* hail_roof_type
					* hail_roof_age_type
					* hail_sqft_story
					* hail_sec_res
					* hail_reason
					* hail_ded
					* hail_uw_tier
					as hail_base_premium
				
					, round(calculated hail_base_premium
					* hail_new_home
					* hail_new_roof,.0001)
					as hail_disc_base_premium


					, hurr_base_rate
					* hurr_census
					* hurr_const_class
					* hurr_cova
					* hurr_ded
					* hurr_ins_score
					* hurr_home_age
					* hurr_roof_age
					* hurr_sec_res
					* hurr_claim
					* hurr_naogclaim
					* hurr_uw_tier
					as hurr_base_premium
				
					, round(calculated hurr_base_premium
					* wind_mitigation
					* hurr_new_roof
					* hurr_new_home,.0001)
					as hurr_disc_base_premium

				from out.base_rate f1
					inner join out.census_fac   		 	   f3 on f1.casenumber=f3.casenumber
					inner join out.construction_class_fac	   f6 on f1.casenumber=f6.casenumber
					inner join out.coverage_a_fac 		 	   f7 on f1.casenumber=f7.casenumber
					inner join out.coverage_c_base_fac	 	   f8 on f1.casenumber=f8.casenumber
					inner join out.customer_age_fac 	 	   f9 on f1.casenumber=f9.casenumber
					inner join out.ded_fac	 				  f10 on f1.casenumber=f10.casenumber
					inner join out.finished_bsmnt_fac 		  f13 on f1.casenumber=f13.casenumber
					inner join out.heat_system_fac 			  f14 on f1.casenumber=f14.casenumber
					inner join out.home_age_fac 			  f15 on f1.casenumber=f15.casenumber
					inner join out.ins_score_fac 			  f16 on f1.casenumber=f16.casenumber
					inner join out.length_res_fac 			  f17 on f1.casenumber=f17.casenumber
/*					inner join out.num_dogs_fac 			  f18 on f1.casenumber=f18.casenumber*/
					inner join out.num_residents_fac 		  f19 on f1.casenumber=f19.casenumber
					inner join out.num_units_fac 			  f20 on f1.casenumber=f20.casenumber
					inner join out.basement_fac				  f21 on f1.casenumber=f21.casenumber
					inner join out.fence_fac 				  f22 on f1.casenumber=f22.casenumber
					inner join out.pool_fac					  f23 on f1.casenumber=f23.casenumber
					inner join out.rcsqft_fac				  f24 on f1.casenumber=f24.casenumber
					inner join out.roof_age_fac				  f25 on f1.casenumber=f25.casenumber
					inner join out.roof_type_fac			  f26 on f1.casenumber=f26.casenumber
					inner join out.roof_age_type_fac		  f27 on f1.casenumber=f27.casenumber
					inner join out.sqft_fac				 	  f28 on f1.casenumber=f28.casenumber
					inner join out.sqft_story_fac		 	  f29 on f1.casenumber=f29.casenumber
					inner join out.special_personal_prop_fac  f30 on f1.casenumber=f30.casenumber
					inner join out.bathroom_fac			 	  f31 on f1.casenumber=f31.casenumber
					inner join out.priorclaim_NAOG_fac		  f32 on f1.casenumber=f32.casenumber
					inner join out.priorclaim_NAOG_xPeril_fac f33 on f1.casenumber=f33.casenumber
					inner join out.priorclaim_xPeril_fac	  f34 on f1.casenumber=f34.casenumber
					inner join out.priorclaim_Peril_fac		  f35 on f1.casenumber=f35.casenumber
					inner join out.priorclaim_xWater_fac	  f36 on f1.casenumber=f36.casenumber
					inner join out.priorclaim_Water_fac		  f37 on f1.casenumber=f37.casenumber
					inner join out.new_home_fac				  f39 on f1.casenumber=f39.casenumber
					inner join out.new_home_purch_fac 		  f40 on f1.casenumber=f40.casenumber
					inner join out.new_roof_fac				  f41 on f1.casenumber=f41.casenumber
					inner join out.protective_dev_fac		  f42 on f1.casenumber=f42.casenumber
					inner join out.rowhouse_fac				  f43 on f1.casenumber=f43.casenumber
					inner join out.mitigation_fac			  f44 on f1.casenumber=f44.casenumber
					inner join out.Seasonal_Secondary_Fac	  f45 on f1.casenumber=f45.casenumber
					inner join out.foundation_type_year_fac   f46 on f1.casenumber=f46.casenumber
					inner join out.num_stories_fac            f47 on f1.casenumber=f47.casenumber
					inner join out.Reason_Code_Fac			  f48 on f1.casenumber=f48.casenumber
					inner join out.r381_fac					  f49 on f1.casenumber=f49.casenumber
					inner join out.r386_fac					  f50 on f1.casenumber=f50.casenumber
					inner join out.uw_tier_fac				  f51 on f1.casenumber=f51.casenumber
;
			quit;

			data out.step3_subtotal_premium; set out.step2_base_premium;
			subtotal_premium = 	round(fire_disc_base_premium
							+ theft_disc_base_premium
							+ waternw_disc_base_premium
							+ othernw_disc_base_premium
							+ lightning_disc_base_premium
							+ waterw_disc_base_premium
							+ wind_disc_base_premium
							+ hail_disc_base_premium
							+ hurr_disc_base_premium
							,.0001);
			run;