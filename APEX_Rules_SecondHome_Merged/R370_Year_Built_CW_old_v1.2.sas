/* 
-------------------- History -----------------------------
1) 10-15-2019 Vamsi.R ITR#14621 Converts yearbuilt from rate sheet into 3 categories and join the policy info using these categories.
----------------------------------------------------------
*/


/*Drop null columns and rows*/
data r370_remove_nulls;
	set fac.'''370# year built$'''n;
	where f1 is not missing;
	GridGroup = f1;
	drop f1 f10 f11 f12 f13 f14;
run;


proc sql;
	/* Create new column called year_built by extracting year value from f2, and converting it into numeric*/
	create table r370_processed as 
		select *,
			case 
				when f2 like "%+%" then input(substr(f2, 1, 4), 4.)
				when f2 like "%<=%" then input(substr(f2, 4, 4), 4.)
				else . 
			end 
		as year_built
			from r370_remove_nulls;

	select max(year_built) into :max_year_built from r370_processed;
	select min(year_built) into :min_year_built from r370_processed;

	create table r370_final as 
		select *,
			case 
				when year_built = &max_year_built then 'A'
				when year_built = &min_year_built then 'C'
				else 'B'
			end 
		as year_category
			from r370_processed;
quit;


proc sql;
	create table out.year_built_fac as ( 
		select temp.casenumber 
			, temp.policynumber
			, temp.PropertyYearBuilt
			, temp.year_category
			, temp.GridGroup
			, f.Fire as fire_year_built
			, f.'Water Non-Weather'n as waternw_year_built
			, f.'All Other Non-Weather'n as othernw_year_built
			, f.'Water Weather'n as waterw_year_built
			, f.Wind as wind_year_built
			, f.Hail as hail_year_built
			, f.'Cov C - Fire'n as cov_c_fire_year_built
		from (
			select p.casenumber, p.policynumber,p.PropertyYearBuilt, t.GridGroup,
				case 
					when p.PropertyYearBuilt<= &min_year_built then "C"
					when p.PropertyYearBuilt>= &max_year_built then 'A'
					else 'B'
				end 
			as year_category
				from in.policy_info p
				left join out.territory t 
					on t.casenumber = p.casenumber) as temp 
		left join r370_final f
			on f.year_category = temp.year_category and f.GridGroup=temp.GridGroup);
quit;
	
