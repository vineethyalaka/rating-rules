** FINAL PREMIUM **;
%macro final();
	proc sql;
		create table out.Step8_uncapped_calc_premium as
		select *
			, p1.adjusted_subtotal_premium as Step_4
			, p2.Property_Premium as Step_5
			, p3.Total_Liability_Premium as Step_6
			, p4.policy_expense_fee as Step_7
			, p8.rate_adjustment as Quick_rate_fac
			, round(round((p1.adjusted_subtotal_premium
							+ p2.Property_Premium
							+ p3.Total_Liability_Premium
							+ p4.policy_expense_fee),1)* p8.rate_adjustment, 1) as uncapped_calc_prem_before_disc
			, round(calculated uncapped_calc_prem_before_disc*p6.affinity,1) as Affinity_Discount
			, round(calculated uncapped_calc_prem_before_disc*p5.policy_conversion_fac,1) as Policy_Conversion_Disc
			, (calculated uncapped_calc_prem_before_disc - calculated Affinity_Discount - calculated Policy_Conversion_Disc) as uncapped_calc_premium

		from out.step4_Adjusted_subtotal_premium p1
			inner join out.step5_Property_Premium	p2 on p1.casenumber = p2.casenumber
			inner join out.step6_Liability_Premium	p3 on p1.casenumber = p3.casenumber
			inner join out.step7_policy_expense_fee	p4 on p1.casenumber = p4.casenumber
			inner join out.policy_conversion_fac	p5 on p1.casenumber = p5.casenumber
			inner join out.affinity_fac				p6 on p1.casenumber = p6.casenumber
			inner join out.rate_adjustment			p8 on p1.casenumber = p8.casenumber;
	quit;

%mend;
%final();
