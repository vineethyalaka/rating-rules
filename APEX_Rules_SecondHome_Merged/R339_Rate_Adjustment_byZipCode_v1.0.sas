** RULE 338. Underwriting Tier**;
**=====================================================================================**
History: 2017 07 26 BF v1.0 Initial draft for APEX 2.0
		 2018 05 16 JL v1.1 Correcting logic
		 2019 06 24 TM Quick rate factor by zipcode
**=====================================================================================**;
%macro adj();

	data R339;
	set fac.'339# Rate Adjustment$'n;
	if F1='Rate Adjustment Factor' then delete;
	if f2 = . then delete;
	zipcode=put(F1,5.);
	run;


	proc sql;
		create table rate_adjustment as
		select p.casenumber, p.policynumber, p.propertyzip5 as zipcode, f.f2 as rate_adjustment
		from in.policy_info p
        left join R339 f
        on p.propertyzip5 = f.zipcode; /* f1 is zip code column in rate sheet*/
/*p.county = substr(f.f1,1,length(f.f1)-7) and */
	quit;

	data out.rate_adjustment;
	set work.rate_adjustment;
	if rate_adjustment = . then rate_adjustment=1; /* if zipcode is not assigned factor, then belong to 'All Other', factor is 1 */
	run;

%mend;
%adj();
