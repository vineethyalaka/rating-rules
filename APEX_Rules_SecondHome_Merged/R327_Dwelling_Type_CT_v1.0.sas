** RULE 327. SEASONAL OR SECONDARY RESIDENCES **;
/*Note: The flag is called "Secondary_Home_Flag" but the flag is for both seasonal and secondary homes*/
**NEED UPDATE after including Secondary_Home_flag********************************************************;
data sec_residence;
	set in.policy_info;

	if Secondary_Home_flag = 1 then
		sec_residence='Y';
	else sec_residence='N';
run;

proc sql;
	create table out.Seasonal_Secondary_Fac as
		select p.CaseNumber, p.PolicyNumber
			, p.Secondary_Home_flag
			, p.sec_residence
			, f.fire as fire_sec_res
			, f.Theft as theft_sec_res
			, f.'Water Non-Weather'n as waternw_sec_res
			, f.'All Other Non-Weather'n as othernw_sec_res
			, f.Lightning as lightning_sec_res
			, f.'Water Weather'n as waterw_sec_res
			, f.Wind as wind_sec_res
			, f.Hail as hail_sec_res
			, f.Hurricane as hurr_sec_res
		from sec_residence p left join fac.'327# Seasonal or Secondary$'n f
			on p.sec_residence=substr(f1,1,1);
quit;