** RULE 590. MINE SUBSIDENCE INSURANCE **;

/* 2019 04 06 AK Updated to calculate the Non-Dwelling and Additional Living Expenses.*/
/* 2019 06 04 WZ Added NonDwelling and ALE rates*/
%macro ms();
data r590;
set fac.'590# Mine Subsidence-Dwelling$'n;
if f1 = . and f3 = . then delete;
run;

/*Set maximum nondwelling limit as macro variable to be used below*/
proc sql;
     select max(f3)
	 		into :max_dwell_limit
     from r590;
quit;

data r590_ale;
set fac.'590# Mine Subsidence ALE$'n;
if f3 = . then delete;
run;

data r590_nondwell;
set fac.'590# Mine Subsidence-NonDwell$'n;
if f1 = . and f3 = . then delete;
run;

/*Set maximum nondwelling limit as macro variable to be used below*/
proc sql;
     select max(f3)
	 		into :max_nondwell_limit
     from r590_nondwell;
quit;


proc sql;
	create table out.Mine_Sub_Rate as
	select p.CaseNumber, p.PolicyNumber, e.HO2388_FLAG, e.HO_0448_FLAG, p.CoverageA, p.ALE_Flag
		, p.HO_0448_Addl_limit_01_pi
		, p.HO_0448_Addl_limit_02_pi
		, p.HO_0448_Addl_limit_03_pi
		, p.HO_0448_Addl_limit_04_pi
		, p.HO_0448_Addl_limit_05_pi
		, p.HO_0448_Addl_limit_06_pi
		, p.HO_0448_Addl_limit_07_pi
		, p.HO_0448_Addl_limit_08_pi
		, p.HO_0448_Addl_limit_09_pi
		, p.HO_0448_Addl_limit_10_pi 
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_01_pi > 0 then f1.f4 else 0 end as Mine_Sub_Rate_nonDwell_1
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_02_pi > 0 then f2.f4 else 0 end as Mine_Sub_Rate_nonDwell_2
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_03_pi > 0 then f3.f4 else 0 end as Mine_Sub_Rate_nonDwell_3
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_04_pi > 0 then f4.f4 else 0 end as Mine_Sub_Rate_nonDwell_4
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_05_pi > 0 then f5.f4 else 0 end as Mine_Sub_Rate_nonDwell_5
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_06_pi > 0 then f6.f4 else 0 end as Mine_Sub_Rate_nonDwell_6
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_07_pi > 0 then f7.f4 else 0 end as Mine_Sub_Rate_nonDwell_7
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_08_pi > 0 then f8.f4 else 0 end as Mine_Sub_Rate_nonDwell_8
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_09_pi > 0 then f9.f4 else 0 end as Mine_Sub_Rate_nonDwell_9
		, case when e.HO2388_FLAG = '1' and e.HO_0448_FLAG = '1' and p.HO_0448_Addl_limit_10_pi > 0 then f10.f4 else 0 end as Mine_Sub_Rate_nonDwell_10
		, case when e.HO2388_FLAG = '1' then f.f4 else 0 end as Mine_Sub_Rate_Dwell	/*Add rates for all other structures separately*/
		, case when p.ALE_Flag = 1 then input(a.f4,dollar21.) else 0 end as Mine_Sub_Rate_Ale   /*ALE rate*/
		, calculated Mine_Sub_Rate_nonDwell_1
		+ calculated Mine_Sub_Rate_nonDwell_2
		+ calculated Mine_Sub_Rate_nonDwell_3
		+ calculated Mine_Sub_Rate_nonDwell_4
		+ calculated Mine_Sub_Rate_nonDwell_5
		+ calculated Mine_Sub_Rate_nonDwell_6
		+ calculated Mine_Sub_Rate_nonDwell_7
		+ calculated Mine_Sub_Rate_nonDwell_8
		+ calculated Mine_Sub_Rate_nonDwell_9
		+ calculated Mine_Sub_Rate_nonDwell_10 as Mine_Sub_Rate_nonDwell
		, case when e.HO2388_FLAG = '1' then calculated Mine_Sub_Rate_Dwell + calculated Mine_Sub_Rate_Ale + calculated Mine_Sub_Rate_nonDwell else 0 end as Mine_Sub_Rate
	from in.policy_info p
		inner join in.Endorsement_Info e on p.casenumber = e.casenumber
		left join r590 f on p.CoverageA >= f.f1 and (p.CoverageA <= f.f3 or f.f3 = . or f.f3 = &max_dwell_limit)
		left join r590_nondwell f1  on p.HO_0448_Addl_limit_01_pi >= f1.f1 and  (p.HO_0448_Addl_limit_01_pi <= f1.f3  or f1.f3 = .  or f1.f3 = &max_nondwell_limit)
		left join r590_nondwell f2  on p.HO_0448_Addl_limit_02_pi >= f2.f1 and  (p.HO_0448_Addl_limit_02_pi <= f2.f3  or f2.f3 = .  or f2.f3 = &max_nondwell_limit)
		left join r590_nondwell f3 	on p.HO_0448_Addl_limit_03_pi >= f3.f1 and  (p.HO_0448_Addl_limit_03_pi <= f3.f3  or f3.f3 = .  or f3.f3 = &max_nondwell_limit)
		left join r590_nondwell f4	on p.HO_0448_Addl_limit_04_pi >= f4.f1 and  (p.HO_0448_Addl_limit_04_pi <= f4.f3  or f4.f3 = .  or f4.f3 = &max_nondwell_limit)
		left join r590_nondwell f5	on p.HO_0448_Addl_limit_05_pi >= f5.f1 and  (p.HO_0448_Addl_limit_05_pi <= f5.f3  or f5.f3 = .  or f5.f3 = &max_nondwell_limit)
		left join r590_nondwell f6	on p.HO_0448_Addl_limit_06_pi >= f6.f1 and  (p.HO_0448_Addl_limit_06_pi <= f6.f3  or f6.f3 = .  or f6.f3 = &max_nondwell_limit)
		left join r590_nondwell f7	on p.HO_0448_Addl_limit_07_pi >= f7.f1 and  (p.HO_0448_Addl_limit_07_pi <= f7.f3  or f7.f3 = .  or f7.f3 = &max_nondwell_limit)
		left join r590_nondwell f8	on p.HO_0448_Addl_limit_08_pi >= f8.f1 and  (p.HO_0448_Addl_limit_08_pi <= f8.f3  or f8.f3 = .  or f8.f3 = &max_nondwell_limit)
		left join r590_nondwell f9	on p.HO_0448_Addl_limit_09_pi >= f9.f1 and  (p.HO_0448_Addl_limit_09_pi <= f9.f3  or f9.f3 = .  or f9.f3 = &max_nondwell_limit)
		left join r590_nondwell f10	on p.HO_0448_Addl_limit_10_pi >= f10.f1 and (p.HO_0448_Addl_limit_10_pi <= f10.f3 or f10.f3 = . or f10.f3 = &max_nondwell_limit)
, r590_ale a
;
quit;

title 'RULE 590. MINE SUBSIDENCE INSURANCE';

%if &Process = 0 %then %do;
proc print data=out.Mine_Sub_Rate;
where Mine_Sub_Rate = .;
run;
proc sort data=out.Mine_Sub_Rate; by HO2388_FLAG; run;
proc gplot data=out.Mine_Sub_Rate; by HO2388_FLAG;
	plot Mine_Sub_Rate*CoverageA 
		/ overlay legend vaxis=axis2;
run;
quit;

%end;
%mend;
%ms();