

libname tax_list '\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\KY\v11550\03_Data\04062018_Tax_schedule';
libname tax "\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\KY\v11550\02_Programs\KY_Tax_Code_Issues_12182017";

*Bulletin for County Credit;

data BulletinCredit;
input Bulli_County_City_name $ 40.;
datalines;
Knott     Hindman
Floyd     Wheelwright
Bullitt   Mount Washington
Meade     Ekron
Hopkins   Dawson Springs
Menifee   Frenchburg
Pulaski   Science Hill
Wayne     Monticello
Trimble   Milton
Henderson Robards
;
run;

proc sql;
select  "'"||compress(upcase(Bulli_County_City_name))||"'"  into : Bulli_County_City_name separated by ',' 
from BulletinCredit;
quit;

%put &Bulli_County_City_name.;



*Get step8 final premium;
proc sql;
	create table out.premium_before_tax as
	select p1.casenumber
	    ,p2.policynumber
		, round((p1.adjusted_subtotal_premium
		+ p2.Property_Premium
		+ p3.Total_Liability_Premium
		+ p4.policy_expense_fee),1)
		as final_calc_prem

		, round(calculated final_calc_prem*p6.affinity,1) as Affnity_Discount
		, round(calculated final_calc_prem*p5.policy_conversion_fac,1) as Policy_Conversion_Disc
		, (calculated final_calc_prem - calculated Affnity_Discount - calculated Policy_Conversion_Disc) as discounted_final_calc_prem

		, case when calculated discounted_final_calc_prem <= p7.min_premium then p7.min_premium
			else calculated discounted_final_calc_prem end 
		as final_premium
	from out.step4_Adjusted_subtotal_premium p1
		inner join out.step5_Property_Premium	p2 on p1.casenumber = p2.casenumber
		inner join out.step6_Liability_Premium	p3 on p1.casenumber = p3.casenumber
		inner join out.step7_policy_expense_fee	p4 on p1.casenumber = p4.casenumber
		inner join out.policy_conversion_fac	p5 on p1.casenumber = p5.casenumber
		inner join out.affinity_fac				p6 on p1.casenumber = p6.casenumber
		, out.min_premium p7;
quit;


data  out.premium_surcharged;
 set  out.premium_before_tax;
surcharge=int(round(final_premium*0.018,0.01));
/*final_premium=final_premium+surcharge; do not use surcharge as base premium to calculate city county tax.*/ 
run;

*Get county and city name;

proc sql noprint;
select casenumber into : casenumber separated by ',' 
from out.premium_before_tax;
quit;


     
proc sql;
create table county_city_name as 
select 
 casenumber 
,County	
,Municipality
,policy_current_status
,PolicyEffectiveDate
/*,KyTaxVersion*/
,PolicyIssueDate
from in.policy_info p
/*left join DH0200P b*/
/*on  p.policynumber = b.policy_number*/
;
quit;

*find county's city and county tax factors for each casenumber;
proc sql;
create table city_county_tax_fac as
select  f.policynumber,
        f.casenumber,
		c.Municipality, 
		c.County,
/*		c.KyTaxVersion,*/
		c.policy_current_status,
		c.PolicyEffectiveDate,
		c.PolicyIssueDate,
		f.surcharge,
		t.'Fire and Allied Perils'n as City_Fire_rate,
		t.'Casualty Liability Only'n as City_Liablity_rate,
		ct.'Fire and Allied Perils'n as County_Fire_rate,
		ct.'Casualty Liability Only'n as County_Liablity_rate,
		f.final_premium
from out.premium_surcharged f
left join county_city_name c
on f.casenumber = c.casenumber
left join tax_list.KY_County_Tax ct
on upcase(left(trim(c.County))) = upcase(left(trim(ct.'Municipality Name'n)))
left join tax_list.KY_City_Tax t
on upcase(left(trim(c.Municipality))) = upcase(left(trim(t.'Municipality Name'n)))
;
quit;


proc sql;
create table out.city_county_tax_fac as 
select 
		f.PolicyNumber	
		,f.CaseNumber	
		,f.Municipality	
		,f.County	
		,f.surcharge	

		,f.City_Fire_rate as City_Fire_rate_regular	
		,f.City_Liablity_rate as City_Liablity_rate_regular	
		,f.County_Fire_rate as County_Fire_rate_regular
		,f.County_Liablity_rate as County_Liablity_rate_regular

		,i.City_Fire_rate_i	
		,i.City_Liablity_rate_i	
		,i.County_Fire_rate_i	
		,i.County_Liablity_rate_i
		,i.'NB TaxYearEffDate'n 
        ,f.PolicyEffectiveDate
		,f.PolicyIssueDate
		,f.policy_current_status
/*		,f.KyTaxVersion*/

       ,case  
              when i.City_Fire_rate_i  is not null 
	              and f.policy_current_status=1
	              and i.'NB TaxYearEffDate'n <= f.PolicyEffectiveDate
			  then i.City_Fire_rate_i
              when i.City_Fire_rate_i  is not null 
	              and f.policy_current_status=0
	              and i.'NB TaxYearEffDate'n <= f.PolicyIssueDate
			  then i.City_Fire_rate_i
		else  f.City_Fire_rate
        end as City_Fire_rate,

		case 
             when i.City_Liablity_rate_i is not null 
				 and f.policy_current_status=1
				 and i.'NB TaxYearEffDate'n <= f.PolicyEffectiveDate
			 then i.City_Liablity_rate_i
             when i.City_Liablity_rate_i is not null 
				 and f.policy_current_status=0
				 and i.'NB TaxYearEffDate'n <= f.PolicyIssueDate
			 then i.City_Liablity_rate_i
	    else  f.City_Liablity_rate
        end as City_Liablity_rate,

		case
             when i.County_Fire_rate_i is not null 
		         and f.policy_current_status=1
                 and i.'NB TaxYearEffDate'n <= f.PolicyEffectiveDate
			 then i.County_Fire_rate_i
             when i.County_Fire_rate_i is not null 
		         and f.policy_current_status=0
                 and i.'NB TaxYearEffDate'n <= f.PolicyIssueDate
             then i.County_Fire_rate_i
	    else  f.County_Fire_rate
        end as County_Fire_rate,

		case 
             when i.County_Liablity_rate_i is not null 
			     and f.policy_current_status=1
	             and i.'NB TaxYearEffDate'n <= f.PolicyEffectiveDate
             then i.County_Liablity_rate_i 
             when i.County_Liablity_rate_i is not null 
			     and f.policy_current_status=0
	             and i.'NB TaxYearEffDate'n <= f.PolicyIssueDate
             then i.County_Liablity_rate_i 
		else  f.County_Liablity_rate
        end as County_Liablity_rate,

		f.final_premium
from city_county_tax_fac f
left join tax.KY_Tax_Code_Issues_add_date i
on compress(upcase(f.Municipality)) = compress(upcase(i.Municipality))
 and compress(upcase(f.County))=compress(upcase(i.County))
 ;quit;



/**/
/*Divide the total premium into two sections: 2/3 for Fire and 1/3 for Liability:*/
/**/
/*       Liability Premium:   one third amount of total premium = round(total premium / 3, 0)*/
/*       Fire Premium:        two thirds amount of total premium = total premium = round(total premium / 3, 0)*/
/**/

%let calc_lia_Fire_prem= %str(Liability_Premium= round(final_premium/3);
Fire_Premium= round(final_premium*(2/3)));

%let  calc_city_county_tax= %str( 
City_Total_Tax_Amount=round(Fire_Premium*City_Fire_rate+Liability_Premium*City_Liablity_rate);
County_Total_Tax_Amount=round(Fire_Premium*County_Fire_rate+Liability_Premium*County_Liablity_rate));

%put &Bulli_County_City_name.;


data City_County_Tax;
 set out.city_county_tax_fac;
&calc_lia_Fire_prem.;
&calc_city_county_tax.;
if (City_Fire_rate ='' and City_Liablity_rate ='') and (County_Fire_rate='' and County_Liablity_rate='')
then do;
       City_Tax=0;
	   County_Tax=0;
end;
else if ( City_Fire_rate ^='' and City_Liablity_rate ^='') and (County_Fire_rate ='' and County_Liablity_rate ='')
then do;        
		City_Tax=City_Total_Tax_Amount;
		County_Tax=0;
end;
else if ( City_Fire_rate ='' and City_Liablity_rate ='') and ( County_Fire_rate ^='' and County_Liablity_rate ^='' )
then do;
		City_Tax=0;
		County_Tax=County_Total_Tax_Amount;
end;
else if ( City_Fire_rate ^='' and City_Liablity_rate ^='') and ( County_Fire_rate ^='' and County_Liablity_rate ^='' )
then do;
      County_municipal_city= upcase(compress(County||Municipality));
     if County_municipal_city in (&Bulli_County_City_name.) then 
	 do;
		if City_Total_Tax_Amount>County_Total_Tax_Amount then 
		do;
		City_Tax=City_Total_Tax_Amount;
		County_Tax=0;
		end;
		if City_Total_Tax_Amount=County_Total_Tax_Amount then 
		do;
		City_Tax=City_Total_Tax_Amount;
		County_Tax=0;
		end;
		if City_Total_Tax_Amount<County_Total_Tax_Amount then 
		do;
		City_Tax=City_Total_Tax_Amount;
		County_Tax=County_Total_Tax_Amount-City_Total_Tax_Amount;
		end;
	 end;
	 else if County_municipal_city not in (&Bulli_County_City_name.) then 
	 do;
		City_Tax=City_Total_Tax_Amount;
		County_Tax=0;
	 end;
end;
array number(*) City_Total_Tax_Amount County_Total_Tax_Amount City_Tax County_Tax;
do i = 1 to dim(number);
 if number(i)='' then 
 number(i)=0;    
end;
keep PolicyNumber CaseNumber Municipality County Fire_Premium Liability_Premium City_Total_Tax_Amount 
County_Total_Tax_Amount City_Tax County_Tax surcharge final_premium;
run;

data out.surcharge_City_County_Tax;
 set City_County_Tax;
 	City_Tax=round(City_Tax,1);
	County_Tax=round(County_Tax,1);
keep PolicyNumber CaseNumber City_Tax County_Tax surcharge;
run;

