
%macro step9();

	proc sql;
		create table out.step9_final as
		select p1.*
			/*get the capped premium*/
			, case when  Old_Broll_Policy = 1 then p1.capped_premium
				   else round(p1.uncapped_calc_premium * p1.capping_factor ,1) end as capped_premium
	
			/*get the final premium by compare to minimum and add the endorsement strategy*/
			, case when calculated capped_premium <= p10.min_premium then p10.min_premium
				else calculated capped_premium end as min_adjusted_prem

			, calculated min_adjusted_prem + p8.Equip_Break_Rate + p9.Service_Line_Rate 
			as final_premium

		from out.step9_capping_factor p1
			inner join out.equipment_breakdown_fac	p8 on p1.casenumber = p8.casenumber
			inner join out.Service_Line_Rate 		p9 on p1.casenumber = p9.casenumber
			, out.min_premium p10;		

	quit;


%mend;

%step9();
