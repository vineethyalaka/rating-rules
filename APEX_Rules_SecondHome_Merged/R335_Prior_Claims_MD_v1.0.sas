** RULE 335. PRIOR CLAIMS **;

**=====================================================================================**
History: 2015 05 07 JT  Initial draft
		 2015 05 27 SY  Added Hurricane peril indicator
**=====================================================================================**;


%macro claim();
%if &Existing = 1 %then %do;

	proc sql;
		create table claim_buckets as
		select distinct l.casenumber, l.policynumber, p.PolicyTermYears, p.PolicyEffectiveDate, l.lossdate, 
			l.LossCode, l.LossAmount
			/* treat cat and non-cat the same */
			, case when l.LossCode > 1000 then l.LossCode-1000 else l.LossCode end as code
			/* bucket loss codes into perils */
			, case when calculated code in(7,17,33)        then 1 else 0 end as fireclaim
			, case when calculated code in(24)             then 1 else 0 end as windclaim
			, case when calculated code in(18,19,20,53,54) then 1 else 0 end as theftclaim
			, case when calculated code in(12) or lightning =1 then 1 else 0 end as lightningclaim /**/
			, case when calculated code in(29) 			   then 1 else 0 end as liabilityclaim /**/
			, case when calculated code in(9,59)           then 1 else 0 end as waterclaim
			, case when calculated code in(23)             then 1 else 0 end as backupclaim
			, case when calculated code in(4,5,6,21,22,25,26,27,32,37,52,58,60,61)
													 	   then 1 else 0 end as otherclaim

			, case when calculated liabilityclaim=0  	   then 1 else 0 end as claimexliability
			, case when calculated waterclaim=0 	 	   then 1 else 0 end as claimexwater
			, case when calculated backupclaim=0 	 	   then 1 else 0 end as claimexbackup
			
			, case when calculated code not in(5,8,11,12,24,41) then 1 else 0 end as naogclaim

			, case when calculated naogclaim=1 and calculated fireclaim=0
													 	   then 1 else 0 end as naogexfire
			, case when calculated naogclaim=1 and calculated theftclaim=0 
													 	   then 1 else 0 end as naogextheft
			, case when calculated naogclaim=1 and calculated otherclaim=0 	 	   
														   then 1 else 0 end as naogexother
		from in.loss_info_all l 
			inner join in.policy_info p on l.casenumber=p.casenumber
		where l.LossAmount >0
			and (p.PolicyTermYears=1 and &effst = 0 and (p.PolicyIssueDate - l.LossDate)/365 <3
			or  p.PolicyTermYears=1 and &effst ^= 0 and (p.PolicyEffectiveDate - l.LossDate)/365 <3
			or  p.PolicyTermYears^=1 and (p.PolicyEffectiveDate - l.LossDate - 60)/365 <3);
	quit;
	proc summary data=claim_buckets;
	class casenumber;
	var fireclaim windclaim theftclaim lightningclaim liabilityclaim waterclaim backupclaim otherclaim
		claimexliability claimexwater claimexbackup naogclaim naogexfire naogextheft naogexother;
	output out=claim_sums sum()=;
	run;

	proc sort data=in.policy_info out=policy_sort; by casenumber; run;
	proc sort data=claim_sums; by casenumber; run;
	data out.claim_buckets;
	merge policy_sort (in=a) claim_sums (in=b drop=_type_ _freq_);
	by casenumber;
	if a and not b then do;
		fireclaim=0; windclaim=0; theftclaim=0; lightningclaim=0; liabilityclaim=0; 
		waterclaim=0; backupclaim=0; otherclaim=0; claimexliability=0; claimexwater=0; 
		claimexbackup=0; naogclaim=0; naogexfire=0; naogextheft=0; naogexother=0;
		end;
	if 	fireclaim > 5 		then fireclaim = 5; 
	if 	windclaim > 5 		then windclaim = 5; 
	if 	theftclaim > 5 		then theftclaim = 5; 
	if 	lightningclaim > 5 	then lightningclaim = 5; 
	if 	liabilityclaim > 5 	then liabilityclaim = 5; 
	if 	waterclaim > 5 		then waterclaim = 5; 
	if 	backupclaim > 5 	then backupclaim = 5; 
	if 	otherclaim > 5 		then otherclaim = 5; 
	if 	claimexliability>10 then claimexliability = 10; 
	if 	claimexwater > 10 	then claimexwater = 10; 
	if 	claimexbackup > 10 	then claimexbackup = 10; 
	if 	naogclaim > 10 		then naogclaim = 10; 
	if 	naogexfire > 10 	then naogexfire = 10; 
	if 	naogextheft > 10 	then naogextheft = 10; 
	if 	naogexother > 10 	then naogexother = 10; 
	if not a then delete;
	run;


	** RULE 335.A NAOG CLAIMS **;
	data priorclaimNAOGfac;
	set fac.'335#A NAOG Claims$'n;
	if f1=. and lightning ne . then naogclaim=10;
	else naogclaim = f1;
	run;
	proc sql;
		create table out.priorclaim_NAOG_fac as
		select p.casenumber, p.policynumber, p.naogclaim
			, f.lightning as lightning_naogclaim
			, f.wind as wind_naogclaim
			, f.'Cov C - EC'n as Cov_C_EC_noagclaim
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_naogclaim
			, f.'Liability - Bodily injury (Perso'n as liabipi_naogclaim
			, f.'Liability - Bodily injury (Medic'n as liabimp_naogclaim		
		from out.claim_buckets p left join priorclaimNAOGfac f
			on p.naogclaim=f.naogclaim;
	quit;





** RULE 335.B NAOG CLAIMS XPERIL **;
	data priorclaimNAOGxPerilfac;
	set fac.'335#B NAOG Claims xPeril$'n;
	if f1=. and fire ne . then naogclaimxPeril=10;
	else naogclaimxPeril = f1;
	run;
	proc sql;
		create table out.priorclaim_NAOG_xPeril_fac as
		select p.casenumber, p.policynumber, p.naogexfire, p.naogextheft, p.naogexother
			, f1.fire as fire_naogclaimxPeril
			, f2.theft as theft_naogclaimxPeril
			, f3.'All Other Non-Weather'n as othernw_naogclaim
			, f1.'Cov C - Fire'n as Cov_C_Fire_naogclaim
			, f2.'Property Damage due to Burglary'n as PD_Burg_naogclaim
			, f2.'Limited Theft Coverage'n as Lim_Theft_naogclaim
		from out.claim_buckets p left join priorclaimNAOGxPerilfac f1
			on p.naogexfire=f1.naogclaimxPeril
			left join priorclaimNAOGxPerilfac f2
			on p.naogextheft=f2.naogclaimxPeril
			left join priorclaimNAOGxPerilfac f3
			on p.naogexother=f3.naogclaimxPeril;
	quit;



** RULE 335.C CLAIMS XLIABILITY **;
	data priorclaimxPerilfac;
	set fac.'335#C Claims xLiability$'n;
	if f1=. and 'Liability - Property Damage to O'n ne . then claimxPeril=10;
	else claimxPeril = f1;
	run;
	proc sql;
		create table out.priorclaim_xPeril_fac as
		select p.casenumber, p.policynumber, p.claimexliability, p.claimexbackup
			, f1.'Liability - Property Damage to O'n as liapd_claimxPeril
		from out.claim_buckets p left join priorclaimxPerilfac f1
			on p.claimexliability=f1.claimxPeril
		;
	quit;


** RULE 335.D PERIL CLAIMS **;
	data priorclaimPerilfac;
	set fac.'335#D Peril Claims$'n;
	if f1=. and fire ne . then Perilclaim=5;
	else Perilclaim = f1;
	run;
	proc sql;
		create table out.priorclaim_Peril_fac as
		select p.casenumber, p.policynumber, p.fireclaim, p.liabilityclaim, p.theftclaim, p.otherclaim, p.windclaim, p.lightningclaim
			, f1.fire as fire_claim		
			, f3.theft as theft_claim
			, f4.'All Other Non-Weather'n as othernw_claim
			, f6.lightning as lightning_claim
			, f5.wind as wind_claim
			, f1.'Cov C - Fire'n as Cov_C_Fire_claim
			, f3.'Property Damage due to Burglary'n as PD_Burg_claim
			, %if &Hurricane = 1 %then f5.Hurricane; %else 0; as hurr_claim
			, f2.'Liability - Property Damage to O'n as liapd_claim
			, f3.'Limited Theft Coverage'n as Lim_Theft_claim
		from out.claim_buckets p left join priorclaimPerilfac f1
			on p.fireclaim=f1.Perilclaim
			left join priorclaimPerilfac f2
			on p.liabilityclaim=f2.Perilclaim
			left join priorclaimPerilfac f3
			on p.theftclaim=f3.Perilclaim
			left join priorclaimPerilfac f4
			on p.otherclaim=f4.Perilclaim
			left join priorclaimPerilfac f5
			on p.windclaim=f5.Perilclaim
			left join priorclaimPerilfac f6
			on p.lightningclaim=f6.Perilclaim;
	quit;


** RULE 335.E CLAIMS XWATER **;
	data priorclaimxWaterfac;
	set fac.'335#E Claims xWater$'n;
	if f1=. and 'Water Non-Weather'n ne . then ClaimsxWater=10;
	else ClaimsxWater = f1;
	run;
	proc sql;
		create table out.priorclaim_xWater_fac as
		select p.casenumber, p.policynumber, p.claimexwater
			, f.'Water Non-Weather'n as waternw_claimxPeril
			, f.'Water Weather'n as waterw_claimxPeril
			, f.'Water Back-up'n as waterbu_claimxPeril
		from out.claim_buckets p left join priorclaimxWaterfac f
			on p.claimexwater=f.ClaimsxWater;
	quit;



** RULE 335.F WATER CLAIMS **;
	data priorclaimWaterfac;
	set fac.'335#F Water Claims$'n;
	if f1=. and 'Water Non-Weather'n ne . then WaterClaims=5;
	else WaterClaims = f1;
	run;
	proc sql;
		create table out.priorclaim_Water_fac as
		select p.casenumber, p.policynumber, p.waterclaim
			, f.'Water Non-Weather'n as waternw_claim
			, f.'Water Weather'n as waterw_claim
			, f.'Water Back-up'n as waterbu_claim
		from out.claim_buckets p left join priorclaimWaterfac f
			on p.waterclaim=f.WaterClaims;
	quit;

%end;

%if &Existing = 0 %then %do;

	proc sql;
		create table claim_buckets as
		select distinct l.casenumber, l.policynumber, p.PolicyTermYears, p.PolicyEffectiveDate, l.lossdate, 
			l.LossCode, l.LossAmount
			/* treat cat and non-cat the same */
			, case when l.LossCode > 1000 then l.LossCode-1000 else l.LossCode end as code
			/* bucket loss codes into perils */
			, case when calculated code in(7,17,33)        then 1 else 0 end as fireclaim
			, case when calculated code in(24)             then 1 else 0 end as windclaim
			, case when calculated code in(18,19,20,53,54) then 1 else 0 end as theftclaim
			, case when calculated code in(12) /*or lightning =1*/ then 1 else 0 end as lightningclaim /**/
			, case when calculated code in(29) 			   then 1 else 0 end as liabilityclaim /**/
			, case when calculated code in(9,59)           then 1 else 0 end as waterclaim
			, case when calculated code in(23)             then 1 else 0 end as backupclaim
			, case when calculated code in(4,5,6,21,22,25,26,27,32,37,52,58,60,61)
													 	   then 1 else 0 end as otherclaim

			, case when calculated liabilityclaim=0  	   then 1 else 0 end as claimexliability
			, case when calculated waterclaim=0 	 	   then 1 else 0 end as claimexwater
			, case when calculated backupclaim=0 	 	   then 1 else 0 end as claimexbackup
			
			, case when calculated code not in(5,8,11,12,24,41) then 1 else 0 end as naogclaim

			, case when calculated naogclaim=1 and calculated fireclaim=0
													 	   then 1 else 0 end as naogexfire
			, case when calculated naogclaim=1 and calculated theftclaim=0 
													 	   then 1 else 0 end as naogextheft
			, case when calculated naogclaim=1 and calculated otherclaim=0 	 	   
														   then 1 else 0 end as naogexother
		from in.loss_info l 
			inner join in.policy_info p on l.casenumber=p.casenumber
		where l.LossAmount >0
			and (p.PolicyTermYears=1 and &effst = 0 and (p.PolicyIssueDate - l.LossDate)/365 <3
			or  p.PolicyTermYears=1 and &effst ^= 0 and (p.PolicyEffectiveDate - l.LossDate)/365 <3
			or  p.PolicyTermYears^=1 and (p.PolicyEffectiveDate - l.LossDate - 60)/365 <3);
	quit;
	proc summary data=claim_buckets;
	class casenumber;
	var fireclaim windclaim theftclaim lightningclaim liabilityclaim waterclaim backupclaim otherclaim
		claimexliability claimexwater claimexbackup naogclaim naogexfire naogextheft naogexother;
	output out=claim_sums sum()=;
	run;

	proc sort data=in.policy_info out=policy_sort; by casenumber; run;
	proc sort data=claim_sums; by casenumber; run;
	data out.claim_buckets;
	merge policy_sort (in=a) claim_sums (in=b drop=_type_ _freq_);
	by casenumber;
	if a and not b then do;
		fireclaim=0; windclaim=0; theftclaim=0; lightningclaim=0; liabilityclaim=0; 
		waterclaim=0; backupclaim=0; otherclaim=0; claimexliability=0; claimexwater=0; 
		claimexbackup=0; naogclaim=0; naogexfire=0; naogextheft=0; naogexother=0;
		end;
	if 	fireclaim > 5 		then fireclaim = 5; 
	if 	windclaim > 5 		then windclaim = 5; 
	if 	theftclaim > 5 		then theftclaim = 5; 
	if 	lightningclaim > 5 	then lightningclaim = 5; 
	if 	liabilityclaim > 5 	then liabilityclaim = 5; 
	if 	waterclaim > 5 		then waterclaim = 5; 
	if 	backupclaim > 5 	then backupclaim = 5; 
	if 	otherclaim > 5 		then otherclaim = 5; 
	if 	claimexliability>10 then claimexliability = 10;
	if 	claimexwater > 10 	then claimexwater = 10; 
	if 	claimexbackup > 10 	then claimexbackup = 10; 
	if 	naogclaim > 10 		then naogclaim = 10; 
	if 	naogexfire > 10 	then naogexfire = 10; 
	if 	naogextheft > 10 	then naogextheft = 10; 
	if 	naogexother > 10 	then naogexother = 10; 
	if not a then delete;
	run;


	** RULE 335.A NAOG CLAIMS **;
	data priorclaimNAOGfac;
	set fac.'335#A NAOG Claims$'n;
	if f1=. and lightning ne . then naogclaim=10;
	else naogclaim = f1;
	run;
	proc sql;
		create table out.priorclaim_NAOG_fac as
		select p.casenumber, p.policynumber, p.naogclaim
			, f.lightning as lightning_naogclaim
			, f.wind as wind_naogclaim
			, f.'Cov C - EC'n as Cov_C_EC_naogclaim
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_naogclaim
			, f.'Liability - Bodily injury (Perso'n as liabipi_naogclaim
			, f.'Liability - Bodily injury (Medic'n as liabimp_naogclaim		
		from out.claim_buckets p left join priorclaimNAOGfac f
			on p.naogclaim=f.naogclaim;
	quit;





** RULE 335.B NAOG CLAIMS XPERIL **;
	data priorclaimNAOGxPerilfac;
	set fac.'335#B NAOG Claims xPeril$'n;
	if f1=. and fire ne . then naogclaimxPeril=10;
	else naogclaimxPeril = f1;
	run;
	proc sql;
		create table out.priorclaim_NAOG_xPeril_fac as
		select p.casenumber, p.policynumber, p.naogexfire, p.naogextheft, p.naogexother
			, f1.fire as fire_naogclaimxPeril
			, f2.theft as theft_naogclaimxPeril
			, f3.'All Other Non-Weather'n as othernw_naogclaim
			, f1.'Cov C - Fire'n as Cov_C_Fire_naogclaimxPeril
			, f2.'Property Damage due to Burglary'n as PD_Burg_naogclaimxPeril
			, f2.'Limited Theft Coverage'n as Lim_Theft_naogclaimxPeril
		from out.claim_buckets p left join priorclaimNAOGxPerilfac f1
			on p.naogexfire=f1.naogclaimxPeril
			left join priorclaimNAOGxPerilfac f2
			on p.naogextheft=f2.naogclaimxPeril
			left join priorclaimNAOGxPerilfac f3
			on p.naogexother=f3.naogclaimxPeril;
	quit;



** RULE 335.C CLAIMS XLIABILITY **;
	data priorclaimxPerilfac;
	set fac.'335#C Claims xLiability$'n;
	if f1=. and 'Liability - Property Damage to O'n ne . then claimxPeril=10;
	else claimxPeril = f1;
	run;
	proc sql;
		create table out.priorclaim_xPeril_fac as
		select p.casenumber, p.policynumber, p.claimexliability, p.claimexbackup
			, f1.'Liability - Property Damage to O'n as liapd_claimxPeril
		from out.claim_buckets p left join priorclaimxPerilfac f1
			on p.claimexliability=f1.claimxPeril
		;
	quit;


** RULE 335.D PERIL CLAIMS **;
	data priorclaimPerilfac;
	set fac.'335#D Peril Claims$'n;
	if f1=. and fire ne . then Perilclaim=5;
	else Perilclaim = f1;
	run;
	proc sql;
		create table out.priorclaim_Peril_fac as
		select p.casenumber, p.policynumber, p.fireclaim, p.liabilityclaim, p.theftclaim, p.otherclaim, p.windclaim, p.lightningclaim
			, f1.fire as fire_claim		
			, f3.theft as theft_claim
			, f4.'All Other Non-Weather'n as othernw_claim
			, f6.lightning as lightning_claim
			, f5.wind as wind_claim
			, %if &Hurricane = 1 %then f5.Hurricane; %else 0; as hurr_claim
			, f1.'Cov C - Fire'n as Cov_C_Fire_claim
			, f3.'Property Damage due to Burglary'n as PD_Burg_claim
			, f2.'Liability - Property Damage to O'n as liapd_claim
			, f3.'Limited Theft Coverage'n as Lim_Theft_claim
		from out.claim_buckets p left join priorclaimPerilfac f1
			on p.fireclaim=f1.Perilclaim
			left join priorclaimPerilfac f2
			on p.liabilityclaim=f2.Perilclaim
			left join priorclaimPerilfac f3
			on p.theftclaim=f3.Perilclaim
			left join priorclaimPerilfac f4
			on p.otherclaim=f4.Perilclaim
			left join priorclaimPerilfac f5
			on p.windclaim=f5.Perilclaim
			left join priorclaimPerilfac f6
			on p.lightningclaim=f6.Perilclaim;
	quit;


** RULE 335.E CLAIMS XWATER **;
	data priorclaimxWaterfac;
	set fac.'335#E Claims xWater$'n;
	if f1=. and 'Water Non-Weather'n ne . then ClaimsxWater=10;
	else ClaimsxWater = f1;
	run;
	proc sql;
		create table out.priorclaim_xWater_fac as
		select p.casenumber, p.policynumber, p.claimexwater
			, f.'Water Non-Weather'n as waternw_claimxPeril
			, f.'Water Weather'n as waterw_claimxPeril
			, f.'Water Back-up'n as waterbu_claimxPeril
		from out.claim_buckets p left join priorclaimxWaterfac f
			on p.claimexwater=f.ClaimsxWater;
	quit;



** RULE 335.F WATER CLAIMS **;
	data priorclaimWaterfac;
	set fac.'335#F Water Claims$'n;
	if f1=. and 'Water Non-Weather'n ne . then WaterClaims=5;
	else WaterClaims = f1;
	run;
	proc sql;
		create table out.priorclaim_Water_fac as
		select p.casenumber, p.policynumber, p.waterclaim
			, f.'Water Non-Weather'n as waternw_claim
			, f.'Water Weather'n as waterw_claim
			, f.'Water Back-up'n as waterbu_claim
		from out.claim_buckets p left join priorclaimWaterfac f
			on p.waterclaim=f.WaterClaims;
	quit;

%end;



%if &Process = 0 %then %do;
	title 'RULE 335.A NAOG CLAIMS';
	proc freq data=out.priorclaim_NAOG_fac;
	tables naogclaim
		*lightning_naogclaim
		*wind_naogclaim
		*Cov_C_EC_naogclaim
		*liabipi_naogclaim
		*liabimp_naogclaim
		*hurr_naogclaim
		 / list missing;
	run;
%end;

%if &Process = 0 %then %do;
	title 'RULE 335.B NAOG CLAIMS XPERIL';
	proc freq data=out.priorclaim_NAOG_xPeril_fac;
	tables naogexfire*fire_naogclaimxPeril*
		   naogextheft*theft_naogclaimxPeril*
		   naogexother*othernw_naogclaim
			*Cov_C_Fire_naogclaimxPeril*PD_Burg_naogclaimxPeril
			*Lim_Theft_naogclaimxPeril / list missing;
	run;
%end;

%if &Process = 0 %then %do;
	title 'RULE 335.C CLAIMS XLIABILITY';
	proc freq data=out.priorclaim_xPeril_fac;
	tables claimexliability*liapd_claimxPeril / list missing;
	run;
%end;

%if &Process = 0 %then %do;
	title 'RULE 335.D PERIL CLAIMS';
	proc freq data=out.priorclaim_Peril_fac;
	tables fireclaim*fire_claim*
		theftclaim*theft_claim*
		otherclaim*othernw_claim*
		lightningclaim*lightning_claim*
		windclaim*wind_claim*
		windclaim*hurr_claim*
		Cov_C_Fire_claim*PD_Burg_claim
		liabilityclaim*liapd_claim*Lim_Theft_claim / list missing;
	run;
%end;

%if &Process = 0 %then %do;
	title 'RULE 335.E CLAIMS XWATER';
	proc freq data=out.priorclaim_xWater_fac;
	tables claimexwater
		*waternw_claimxPeril
		*waterw_claimxPeril
		*waterbu_claimxPeril / list missing;
	run;
%end;

%if &Process = 0 %then %do;
	title 'RULE 335.F WATER CLAIMS';
	proc freq data=out.priorclaim_Water_fac;
	tables waterclaim
		*waternw_claim
		*waterw_claim
		*waterbu_claim / list missing;
	run;
%end;

%mend;
%claim();
