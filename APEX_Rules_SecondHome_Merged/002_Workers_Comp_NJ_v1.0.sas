%macro PD();

data workcomp; set fac.'002# Workers Compensation$'n; 

rate_per_policy = 'Occasional Employee_(Rate per Po'n;
rate_per_employee = 'Full Time Employee_(Rate per Emp'n;

run;

proc sql;
    create table out.WorkersComp_Domestic_Servants as
	select distinct p.CaseNumber, p.PolicyNumber, p.HO_2492_FLAG, p.HO_2494_FLAG,
		case when p.HO_2492_FLAG = "0" and p.HO_2494_FLAG = "0" then 0
			else f.rate_per_policy + f.rate_per_employee*(p.HO_2492_0912_Servant_Count+p.HO_2492_0913_Servant_Count+p.HO_2492_0915_Servant_Count) 
			end as Domestic_Servants
	from in.endorsement_Info as p
	left join workcomp f
	on (case when p.HO_2492_FLAG = "1" then "HO 24 92" when p.HO_2494_FLAG = "1" then "HO 24 94" end) = f.Endorsement
	;
quit;


%mend;
%pd()