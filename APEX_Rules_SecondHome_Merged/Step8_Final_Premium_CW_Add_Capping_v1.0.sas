/*Final Premium Add Capping*/
%macro final();
	data r339fac;
	set fac.'339# Rate Adjustment$'n (firstobs=2 obs=2);
	format f1 4.;
	keep f1;
	call symput('quick_rate',f1);
	run;
    %let quk_rte= round(round((p1.adjusted_subtotal_premium
			+ p2.Property_Premium
			+ p3.Total_Liability_Premium
			+ p4.policy_expense_fee),1)*&quick_rate.,1)
			as final_calc_prem;

	proc sql;
		create table out.step8_final_premium as
		select *
			, &quk_rte.

			, round(calculated final_calc_prem*p6.affinity,1) as Affnity_Discount
			, round(calculated final_calc_prem*p5.policy_conversion_fac,1) as Policy_Conversion_Disc
			, (calculated final_calc_prem - calculated Affnity_Discount - calculated Policy_Conversion_Disc) as discounted_final_calc_prem

			/*Add below logic for capping*/
			, case when p.PolicyAccountNumber in (select AccountNumber from CapAcc.BROLL_ACCOUNT_NUMBER) then  1 else 0 end as Broll_Policy
			, c.cap as DB_Cap_Amount  /*database cap amount*/
			, case when calculated Broll_Policy = 1 then f.Capping_Floor else . end as Capping_Floor
			, case when calculated Broll_Policy = 1 then f.Capping_Ceiling else . end as Capping_Ceiling
			, case when calculated Broll_Policy = 1 then c.prior_premium*(1+f.Capping_Floor) else . end as Premium_Floor
			, case when calculated Broll_Policy = 1 then c.prior_premium*(1+f.Capping_Ceiling) else . end as Premium_Ceiling
			, c.HomesitePreviousCasePremium - c.CapDiscount as HomesiteManualPremium 
				/*If broll policy is amended, calculated with percentage*/
			, case when calculated Broll_Policy = 1 and p.POLICY_TRANS_TYPE = 1 and c.prior_premium = 0 then 
			  (calculated discounted_final_calc_prem - calculated HomesiteManualPremium)/calculated HomesiteManualPremium else . end as Cap_Amend_Factor
				/*If broll policy is amended, calculated with percentage*/
			, case when calculated Broll_Policy = 1 and p.POLICY_TRANS_TYPE = 1 and c.prior_premium = 0 then 
				calculated discounted_final_calc_prem + c.CapDiscount+
				round(c.HomesitePreviousCasePremium * calculated Cap_Amend_Factor - (calculated discounted_final_calc_prem - calculated HomesiteManualPremium),1)
				/*If broll policy is not amended, cap regularly*/
				when calculated Broll_Policy = 1 then 
					case when calculated discounted_final_calc_prem between calculated Premium_Floor and calculated Premium_Ceiling then calculated discounted_final_calc_prem
				/*round the Cap Amount to match with Database*/
					when calculated discounted_final_calc_prem < calculated Premium_Floor then round((calculated Premium_Floor - calculated discounted_final_calc_prem),1) + calculated discounted_final_calc_prem
					when calculated discounted_final_calc_prem > calculated Premium_Ceiling then round((calculated Premium_Ceiling - calculated discounted_final_calc_prem),1) + calculated discounted_final_calc_prem end
				else calculated discounted_final_calc_prem 
				end	as capped_premium
			, case when calculated Broll_Policy = 1 then calculated capped_premium - calculated discounted_final_calc_prem else 0 end as SAS_Cap_Amount
			, case when calculated capped_premium <= p7.min_premium then p7.min_premium
				else calculated capped_premium end 
				as final_premium
		from out.step4_Adjusted_subtotal_premium p1
			inner join in.capping_info c on p1.casenumber = c.casenumber
			inner join in.policy_info p on p1.casenumber = p.casenumber
			inner join cap.'capping$'n as f	on p.PolicyTermYears between f.Term_Lower and f.Term_Upper
			inner join out.step5_Property_Premium	p2 on p1.casenumber = p2.casenumber
			inner join out.step6_Liability_Premium	p3 on p1.casenumber = p3.casenumber
			inner join out.step7_policy_expense_fee	p4 on p1.casenumber = p4.casenumber
			inner join out.policy_conversion_fac	p5 on p1.casenumber = p5.casenumber
			inner join out.affinity_fac				p6 on p1.casenumber = p6.casenumber
			, out.min_premium p7;


quit;

%if &Process = 0 %then %do;
	title 'STEP 8: FINAL PREMIUM (RULE 301.A.8)';
	proc print data=out.step8_final_premium;
	where final_calc_prem = .;
	run;
	proc sql;
		create table out.average_final_premium as
		select mean(adjusted_subtotal_premium) as adjusted_subtotal_premium_avg
			, mean(policy_conversion_fac) as policy_conversion_fac_avg
			, mean(affinity) as affinity_avg
			, mean(final_premium) as final_premium_avg
		from out.step8_final_premium;
	quit;
	proc sgplot data=out.step8_final_premium;
		histogram final_premium;
	run;

%end;
%mend;
%final();