** RULE 320. PRESENCE OF POOL **;
proc sql;
	create table out.pool_fac as
		select p.casenumber, p.policynumber, p.poolpresent
			, input(f.theft, 8.3) as theft_pool
			, input(f.'Water Non-Weather'n, 8.3) as waternw_pool
			, input(f.'All Other Non-Weather'n, 8.3) as othernw_pool
			, input(f.lightning, 8.3) as lightning_pool
			, input(f.'Liability - Bodily injury (Perso'n, 8.3) as liabipi_pool
			, input(f.'Liability - Bodily injury (Medic'n, 8.3) as liabimp_pool
		from in.policy_info p left join fac.'320# Presence of Pool$'n f
			on p.poolpresent=substr(f1,1,1);
quit;