** RULE 621. POOL LIABILITY **;
%macro PL();
	data r621fac;
	set fac.'621# Pool Liability$'n;
	if f1="" then delete;
	fac1='Liability - Bodily injury (Perso'n*1;
	fac2='Liability - Bodily injury (Medic'n*1;
	run;
	data PoolLiab; 
	set in.Policy_Info (keep=casenumber policynumber PolicyFormNumber /*PoolPresent*/ PoolLiaCov);
/*	if PoolPresent = 'N' or PolicyFormNumber = 3 then Pool_Liab_flag = 0;*/
/*	else Pool_Liab_flag = 1;*/
	run;
	proc sql;
	    create table out.lia_Pool_Liab_fac as
		select p.*
			, case when p.PolicyFormNumber = 3 or p.PoolLiaCov = 0 or p.PoolLiaCov = . then 0 else f.fac1 end as liabipi_Pool_Liab
			, case when p.PolicyFormNumber = 3 or p.PoolLiaCov = 0 or p.PoolLiaCov = . then 0 else f.fac2 end as liabimp_Pool_Liab
		from r621fac f, PoolLiab p;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 621. POOL LIABILITY';
	proc freq data=out.lia_Pool_Liab_fac;
	tables PoolPresent
		*PolicyFormNumber
		*liabipi_Pool_Liab
		*liabimp_Pool_Liab / list missing;
	run;
%end;
%mend;
%PL();
