** RULE 610. PERSONAL INJURY **;
%macro PI();
	data r610fac;
	set fac.'610# Personal Injury$'n;
	if f1="" then delete;
	fac='Liability - Bodily injury (Perso'n*1;
	run;
	proc sql;
	    create table out.lia_personalinjury_fac as
		select p.casenumber, p.policynumber, p.HO_2482_FLAG
			, f.fac * input(p.HO_2482_FLAG,1.) as liabipi_personalinjury
		from r610fac f, in.Endorsement_Info p;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 610. PERSONAL INJURY';
	proc freq data=out.lia_personalinjury_fac;
	tables HO_2482_FLAG*liabipi_personalinjury/ list missing;
	run;
%end;

%mend;
%PI();
