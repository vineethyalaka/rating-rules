/*2018 12 03	WZ	read two rate sheets by score model type*/
** RULE 354. INSURANCE SCORE BY LAPSE **;
%macro inslapse();
	data exp_ins_score_x_lapse_fac_1 (keep=lapse ins_tier factor);
	set fac.'354#1 Ins Score by Lapse$'n;
	if f3=. then delete;
	ins_tier = input(f1, 8.);
	lapse=0; factor = 'Expense Fee'n; output;
	lapse=1; factor = f3; output;
/*	rename f1 = ins_tier;*/
	run;

	data exp_ins_score_x_lapse_fac_2 (keep=lapse ins_tier factor);
	set fac.'354#2 Ins Score by Lapse$'n;
	if f3=. then delete;
	ins_tier = input(f1, 8.);
	lapse=0; factor = 'Expense Fee'n; output;
	lapse=1; factor = f3; output;
	run;

	proc sql;
		create table out.exp_ins_score_x_lapse_fac as
		select p1.*, p2.matchcode, p2.ScoreModel, p2.insscore, p2.ins_tier, p2.ins_tier_n,
		coalesce(fa.factor,fb.factor) as exp_ins_score_x_lapse_fac
		from lapse p1 
			inner join ins_tier p2 on p1.casenumber = p2.casenumber
			left join exp_ins_score_x_lapse_fac_1 fa /* These are the TUIRS (APEX) factors */
				on p2.ScoreModel = 2 and 
					fa.lapse = p1.lapse and fa.ins_tier = p2.ins_tier_n
			left join exp_ins_score_x_lapse_fac_2 fb /* These are the TRC (Mosaic) factors */
				on p2.ScoreModel = 3 and
					fb.lapse = p1.lapse and fb.ins_tier = p2.ins_tier_n;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 354. INSURANCE SCORE BY LAPSE';
	proc freq data=out.exp_ins_score_x_lapse_fac;
	tables lapse*ins_tier_n*exp_ins_score_x_lapse_fac / list missing;
	run;
%end;
%mend;
%inslapse();