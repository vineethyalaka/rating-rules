proc sql;
		create table out.step2_base_premium as
		select *
		 	, fire_base_rate
			* fire_zipcode
			* fire_census
			* fire_cova
			* fire_home_age
			* fire_num_res
			* fire_heat_sys
			* fire_ins_score
			* fire_rowhouse
			* fire_sec_res
			* fire_uw_tier
			* fire_woodstove_pre
			* fire_dep_type
			* fire_naogclaimxPeril
			* fire_claim
			* fire_ded
			* case when f1.PolicyFormNumber = 9 then 1 else fire_sp_personal_prop end
			* case when f1.PolicyFormNumber = 3 then 1 else fire_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else fire_r404_OrdinanceorLaw_fac end
			as fire_base_premium

			, round(calculated fire_base_premium
			* fire_new_home
			* fire_new_home_purch
			* fire_prot_dev
			* fire_resmotor,.0001)
			as fire_disc_base_premium


			, theft_base_rate
			* theft_county
			* theft_zipcode
			* theft_census
			* theft_covcbase
			* theft_cust_age
			* theft_ins_score
			* theft_length_res
			* theft_num_res
			* theft_num_units
			* theft_pool
			* theft_sqft
			* theft_sec_res
			* theft_uw_tier
			* theft_naogclaimxPeril
			* theft_claim
			* theft_ded
			* theft_sp_personal_prop
			as theft_base_premium
		
			, round(calculated theft_base_premium
			* theft_prot_dev
			* theft_resmotor,.0001)
			as theft_disc_base_premium


			, waternw_base_rate
			* waternw_zipcode
			* waternw_grid
			* waternw_census
			* waternw_cova	
			* waternw_fin_bsmnt
			* waternw_home_age
			* waternw_ins_score
			* case when f1.PolicyFormNumber = 9 then 1 else waternw_length_res end
			* waternw_num_res
			* waternw_bsmnt
			* waternw_pool
			* waternw_rcsqft
			* waternw_rowhouse
			* waternw_bathroom
			* waternw_uw_tier
			* waternw_sec_res
			* waternw_claimxPeril
			* waternw_claim
			* waternw_found_type_yr_built
			* waternw_num_stories
			* case when f1.PolicyFormNumber = 3 then 1 else waternw_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else waternw_r404_OrdinanceorLaw_fac end
			* waternw_ded
			* case when f1.PolicyFormNumber = 9 then 1 else waternw_sp_personal_prop end
			as waternw_base_premium
		
			, round(calculated waternw_base_premium
			* waternw_new_home
			* waternw_resmotor,.0001)
			as waternw_disc_base_premium


			, othernw_base_rate
			* othernw_zipcode
			* othernw_cova
			* case when f1.PolicyFormNumber = 9 then 1 else othernw_cust_age end
			* othernw_home_age
			* othernw_ins_score
			* othernw_pool
			* othernw_sec_res
			* othernw_uw_tier
			* othernw_naogclaim
			* othernw_claim
			* othernw_ded
			* case when f1.PolicyFormNumber = 9 then 1 else othernw_sp_personal_prop end
			* case when f1.PolicyFormNumber = 3 then 1 else othernw_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else othernw_r404_OrdinanceorLaw_fac end
			as othernw_base_premium
		
			, round(calculated othernw_base_premium
			* othernw_new_home
			* othernw_resmotor,.0001)
			as othernw_disc_base_premium


			, lightning_base_rate
			* lightning_zipcode
			* lightning_census
			* lightning_cova
			* lightning_ins_score
			* lightning_pool
			* lightning_rcsqft
			* lightning_sec_res
			* lightning_ded
			* case when f1.PolicyFormNumber = 9 then 1 else lightning_sp_personal_prop end
			* lightning_uw_tier
			* lightning_naogclaim
			* lightning_claim
			* lightning_fs1_drivedistance
			* case when f1.PolicyFormNumber = 3 then 1 else lightning_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else lgting_r404_OrdinanceorLaw_fac end
			as lightning_base_premium
		
			, round(calculated lightning_base_premium
			* lightning_new_home_purch
			* lightning_resmotor,.0001)
			as lightning_disc_base_premium


			, waterw_base_rate
			* waterw_zipcode
			* waterw_grid
			* waterw_cova		
			* waterw_home_age
			* waterw_ins_score
			* waterw_rcsqft
			* waterw_roof_age
			* waterw_sec_res
			* Waterw_uw_tier
			* waterw_roof_score_fac
			* waterw_nearest_water_body
			* waterw_claimxPeril
			* waterw_claim
			* waterw_ded
			* case when f1.PolicyFormNumber = 3 then 1 else waterw_r401_HO_04_20_fac end 
			* case when f1.PolicyFormNumber = 3 then 1 else waterw_r404_OrdinanceorLaw_fac end
			as waterw_base_premium
		
			, round(calculated waterw_base_premium
			* waterw_new_roof
			* waterw_new_home
			* waterw_resmotor,.0001)
			as waterw_disc_base_premium


			, wind_base_rate
			* wind_zipcode
			* wind_grid
			* wind_census
			* wind_const_class
			* wind_cova
			* wind_home_age
			* wind_ins_score
			* wind_fence
			* wind_rcsqft
			* wind_ACV_roof
			* wind_roof_age
			* wind_roof_type
			* wind_sqft_story
			* wind_sec_res
			* wind_naogclaim
			* wind_claim
			* wind_uw_tier
			* wind_roof_score_fac
			* wind_ded
			* case when f1.PolicyFormNumber = 3 then 1 else wind_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else wind_r404_OrdinanceorLaw_fac end
			as wind_base_premium
		
			, round(calculated wind_base_premium
			* wind_new_home
			* wind_new_roof
			* wind_new_home_purch,.0001)
			as wind_disc_base_premium
		

			, hail_base_rate
			* hail_zipcode
			* hail_grid
			* hail_census
			* hail_cova
			* hail_home_age
			* hail_ACV_roof
			* hail_roof_type
			* hail_roof_age_type
			* hail_sqft_story
			* hail_sec_res
			* hail_reason
			* hail_uw_tier
			* hail_roof_score_fac
			* hail_ded
			* case when f1.PolicyFormNumber = 3 then 1 else hail_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else hail_r404_OrdinanceorLaw_fac end
			as hail_base_premium
		
			, round(calculated hail_base_premium
			* hail_new_home
			* hail_resmotor
			* hail_new_roof,.0001)
			as hail_disc_base_premium
			
			
			, Cov_C_Fire_base_rate
			* Cov_C_Fire_zipcode
			* Cov_C_Fire_census
			* Cov_C_Fire_covcbase
			* Cov_C_Fire_home_age
			* Cov_C_Fire_num_res
			* Cov_C_Fire_heat_sys
			* Cov_C_Fire_ins_score
			* Cov_C_Fire_rowhouse
			* Cov_C_Fire_sec_res
			* Cov_C_Fire_uw_tier
			* Cov_C_Fire_naogclaimxPeril
			* Cov_C_Fire_claim
			* Cov_C_Fire_ded
			* case when f1.PolicyFormNumber = 3 then 1 else Cov_C_r403_HO_04_90_fac end
			as Cov_C_Fire_base_premium

			, round(calculated Cov_C_Fire_base_premium
			* Cov_C_Fire_new_home
			* Cov_C_Fire_new_home_purch
			* Cov_C_Fire_prot_dev
			* Cov_C_Fire_resmotor,.0001)
			as Cov_C_Fire_disc_base_premium


			, Cov_C_EC_base_rate
			* Cov_C_EC_zipcode
			* Cov_C_EC_covcbase
			* Cov_C_EC_grid
			* Cov_C_EC_census
			* Cov_C_EC_ins_score
			* Cov_C_EC_naogclaim
			* Cov_C_EC_ded
			* case when f1.PolicyFormNumber = 3 then 1 else Cov_C_EC_r403_HO_04_90_fac end
			* Cov_C_EC_sec_res
			* Cov_C_EC_uw_tier
			as Cov_C_EC_base_premium

			, round(calculated Cov_C_EC_base_premium
			* Cov_C_EC_resmotor, .0001)
			as Cov_C_EC_disc_base_premium


			, Prop_Dam_Burglary_base_rate
			* PD_Burglary_zipcode
			* PD_Burglary_county
			* PD_Burglary_census
			* PD_Burg_cova
			* PD_Burg_ins_score
			* PD_Burg_num_res
			* PD_Burg_num_units
			* PD_Burg_pool
			* PD_Burg_sqft
			* PD_Burg_naogclaimxperil
			* PD_Burg_claim
			* case when f1.PolicyFormNumber = 3 then 1 else PD_Burg_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else PD_r404_OrdinanceorLaw_fac end
			* PD_Burg_ded
			* PD_Burg_sec_res
			* PD_Burg_uw_tier
			as PD_Burg_base_premium

			, round(calculated PD_Burg_base_premium
			* PD_Burg_prot_dev
			* Prop_Dam_Burglary_resmotor,.0001)
			as PD_Burg_disc_base_premium
			
			, wildfire_base_rate
			* WF_wfrg_fac
			* WF_const_class 
			* WF_cova
			* WF_home_age
			* WF_rowhouse
			* WF_roof_type
			* WF_sec_res
			* wildfire_naogclaimxPeril
			* wildfire_claim
			* WF_uw_tier
			* WF_ded
			* WF_sp_personal_prop
			as WF_base_premium

			, round(calculated WF_base_premium
			* WF_new_home, .0001) as WF_disc_base_premium


		from out.base_rate f1
			inner join out.grid_fac 			 	   f2 on f1.casenumber=f2.casenumber
			inner join out.census_fac   		 	   f3 on f1.casenumber=f3.casenumber
			inner join out.county_fac 				   f4 on f1.casenumber=f4.casenumber
			inner join out.ACV_Roof_fac 		 	   f5 on f1.casenumber=f5.casenumber
			inner join out.construction_class_fac	   f6 on f1.casenumber=f6.casenumber
			inner join out.coverage_a_fac 		 	   f7 on f1.casenumber=f7.casenumber
			inner join out.coverage_c_base_fac	 	   f8 on f1.casenumber=f8.casenumber
			inner join out.customer_age_fac 	 	   f9 on f1.casenumber=f9.casenumber
			inner join out.ded_fac	 				  f10 on f1.casenumber=f10.casenumber
			inner join out.finished_bsmnt_fac 		  f13 on f1.casenumber=f13.casenumber
			inner join out.heat_system_fac 			  f14 on f1.casenumber=f14.casenumber
			inner join out.home_age_fac 			  f15 on f1.casenumber=f15.casenumber
			inner join out.ins_score_fac 			  f16 on f1.casenumber=f16.casenumber
			inner join out.length_res_fac 			  f17 on f1.casenumber=f17.casenumber
/*			inner join out.num_dogs_fac 			  f18 on f1.casenumber=f18.casenumber*/
			inner join out.num_residents_fac 		  f19 on f1.casenumber=f19.casenumber
			inner join out.num_units_fac 			  f20 on f1.casenumber=f20.casenumber
			inner join out.basement_fac				  f21 on f1.casenumber=f21.casenumber
			inner join out.fence_fac 				  f22 on f1.casenumber=f22.casenumber
			inner join out.pool_fac					  f23 on f1.casenumber=f23.casenumber
			inner join out.rcsqft_fac				  f24 on f1.casenumber=f24.casenumber
			inner join out.roof_age_fac				  f25 on f1.casenumber=f25.casenumber
			inner join out.roof_type_fac			  f26 on f1.casenumber=f26.casenumber
			inner join out.roof_age_type_fac		  f27 on f1.casenumber=f27.casenumber
			inner join out.sqft_fac				 	  f28 on f1.casenumber=f28.casenumber
			inner join out.sqft_story_fac		 	  f29 on f1.casenumber=f29.casenumber
			inner join out.special_personal_prop_fac  f30 on f1.casenumber=f30.casenumber
			inner join out.bathroom_fac			 	  f31 on f1.casenumber=f31.casenumber
			inner join out.priorclaim_NAOG_fac		  f32 on f1.casenumber=f32.casenumber
			inner join out.priorclaim_NAOG_xPeril_fac f33 on f1.casenumber=f33.casenumber
/*			inner join out.priorclaim_xPeril_fac	  f34 on f1.casenumber=f34.casenumber*/
			inner join out.priorclaim_Peril_fac		  f35 on f1.casenumber=f35.casenumber
			inner join out.priorclaim_xWater_fac	  f36 on f1.casenumber=f36.casenumber
			inner join out.priorclaim_Water_fac		  f37 on f1.casenumber=f37.casenumber
			inner join out.new_home_fac				  f39 on f1.casenumber=f39.casenumber
			inner join out.new_home_purch_fac 		  f40 on f1.casenumber=f40.casenumber
			inner join out.new_roof_fac				  f41 on f1.casenumber=f41.casenumber
			inner join out.protective_dev_fac		  f42 on f1.casenumber=f42.casenumber
			inner join out.rowhouse_fac				  f43 on f1.casenumber=f43.casenumber
			inner join out.Seasonal_Secondary_Fac	  f45 on f1.casenumber=f45.casenumber
			inner join out.r401_add_amt_ins			  f46 on f1.casenumber=f46.casenumber
			inner join out.r403_CovC_Repl_Cost        f47 on f1.casenumber=f47.casenumber
			inner join out.r404_Ordinance_or_Law      f48 on f1.casenumber=f48.casenumber
			inner join out.res_motor_fac              f49 on f1.casenumber=f49.casenumber
			inner join out.zipcode_fac				  f50 on f1.casenumber=f50.casenumber
			inner join out.foundation_type_year_fac   f51 on f1.casenumber=f51.casenumber
			inner join out.num_stories_fac			  f52 on f1.casenumber=f52.casenumber
			inner join out.Reason_Code_Fac			  f53 on f1.casenumber=f53.casenumber
			inner join out.uw_tier_fac				  f54 on f1.casenumber=f54.casenumber
			inner join out.roof_score_factor		  f55 on f1.casenumber=f55.casenumber
			inner join out.WF_RiskGroup_Fac			  f56 on f1.casenumber=f56.casenumber
			inner join out.Woodstove_Presence_Fac	  f57 on f1.casenumber=f57.casenumber
			inner join out.r381_fac					  f58 on f1.casenumber=f58.casenumber
			inner join out.r382_fac					  f59 on f1.casenumber=f59.casenumber
			inner join out.r386_fac					  f60 on f1.casenumber=f60.casenumber
;
	quit;
