/*2019/1/25 TM  Handle the case: Rate sheet has 'Origination Version' column*/

%macro UNDW();
data uw_tier_fac;
	set fac.'338# Underwriting Tier$'n;
	if F2=. then delete;
	drop F1;/*F1 is Origination Version column*/
	run;

	proc sql;
		create table out.uw_tier_fac as
		select p.casenumber, p.policynumber, p.PolicyIssueDate, p.PolicyEffectiveDate
			,p.undwtier
			,f.fire as fire_uw_tier
			,f.theft as theft_uw_tier
			,f.'Water Non-Weather'n as waternw_uw_tier 
			,f.'All Other Non-Weather'n as othernw_uw_tier
			,f.lightning as lightning_uw_tier 
			,f.'Water Weather'n as Waterw_uw_tier 
			,f.Wind as wind_uw_tier
			,f.hail as hail_uw_tier
			%if &Hurricane = 1 %then %do;  
			,f.hurricane as hurr_uw_tier
			%end;
			,f.'Cov C - Fire'n as Cov_C_Fire_uw_tier
			,f.'Cov C - EC'n as Cov_C_EC_uw_tier
			,f.'Property Damage due to Burglary'n as PD_Burg_uw_tier
			,f.'Liability - Bodily Injury (Perso'n as Liab_BI_uw_tier
			,f.'Liability - Bodily Injury (Medic'n as Liab_MedPay_uw_tier
			,f.'Liability - Property Damage to O'n as Liab_PD_uw_tier
			,f.'Water Back-up'n as WBU_uw_tier
			,f.'Limited Theft Coverage'n as Lim_Theft_uw_tier
			,f.'Expense Fee'n as Exp_Fee_uw_tier
		from in.policy_info p left join uw_tier_fac f
		on p.undwtier = f.F2;
	quit;

%mend;
%UNDW();

