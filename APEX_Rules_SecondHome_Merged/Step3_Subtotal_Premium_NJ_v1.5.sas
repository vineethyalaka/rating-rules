/*********** STEP 3: SUBTOTAL PREMIUM (RULE 301.A.3) *******************************************************/

**=====================================================================================**
Notes: 2017 05 10 NAG Added Responsible Motorist
Notes: 2017 09 14 AWH NJ Changes - Add County, Coastal, Smoke...Remove RMD
**=====================================================================================**;


	proc sql;
		create table out.step2_base_premium as
		select 
/*fire_base_rate*/
/*			, fire_census*/
/*			, fire_cova*/
/*			, fire_home_age*/
/*			, fire_num_res*/
/*			, fire_heat_sys*/
/*			, fire_ins_score*/
/*			, fire_rowhouse*/
/*			, fire_sec_res*/
/*			, fire_naogclaimxPeril*/
/*			, fire_claim*/
/*			, fire_ded,**/
		*
		 	, fire_base_rate
			* fire_census
/*CTY*/     * fire_county
			* fire_cova
			* fire_home_age
			* fire_num_res
			* fire_heat_sys
			* fire_ins_score
			* fire_rowhouse
			* fire_sec_res
			* fire_naogclaimxPeril
			* fire_claim
			* fire_ded
			* case when f1.PolicyFormNumber = 9 then 1 else fire_sp_personal_prop end
			* case when f1.PolicyFormNumber = 3 then 1 else fire_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else fire_r404_OrdinanceorLaw_fac end
			as fire_base_premium

			, round(calculated fire_base_premium
			* fire_new_home
			* fire_new_home_purch
			* fire_prot_dev,.0001)
			as fire_disc_base_premium


			, theft_base_rate
/*CTY*/     * theft_county
			* theft_census
			* theft_covcbase
			* theft_cust_age
/*			* theft_drc*/
			* theft_ins_score
			* theft_length_res
			* theft_num_res
			* theft_num_units
			* theft_pool
			* theft_sqft
			* theft_sec_res
			* theft_naogclaimxPeril
			* theft_claim
			* theft_ded
			* theft_sp_personal_prop
			as theft_base_premium
		
			, round(calculated theft_base_premium
			* theft_prot_dev,.0001)
			as theft_disc_base_premium


			, waternw_base_rate
			* waternw_grid
/*CTY*/     * waternw_county
			* waternw_census
			* waternw_cova	
			* waternw_fin_bsmnt
			* waternw_home_age
			* waternw_ins_score
			* case when f1.PolicyFormNumber = 9 then 1 else waternw_length_res end
			* waternw_num_res
			* waternw_bsmnt
			* waternw_pool
			* waternw_rcsqft
			* waternw_rowhouse
			* waternw_bathroom
			* waternw_sec_res
			* waternw_claimxPeril
			* waternw_claim
			* case when f1.PolicyFormNumber = 3 then 1 else waternw_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else waternw_r404_OrdinanceorLaw_fac end
			*waternw_ded
			* case when f1.PolicyFormNumber = 9 then 1 else waternw_sp_personal_prop end
			as waternw_base_premium
		
			, round(calculated waternw_base_premium
			* waternw_new_home,.0001)
			as waternw_disc_base_premium


			, othernw_base_rate
/*CTY*/     * othernw_county
			* othernw_cova
			* case when f1.PolicyFormNumber = 9 then 1 else othernw_cust_age end
			* othernw_home_age
			* othernw_ins_score
			* othernw_pool
			* othernw_sec_res
			* othernw_naogclaim
			* othernw_claim
			* othernw_ded
			* case when f1.PolicyFormNumber = 9 then 1 else othernw_sp_personal_prop end
			* case when f1.PolicyFormNumber = 3 then 1 else othernw_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else othernw_r404_OrdinanceorLaw_fac end
			as othernw_base_premium
		
			, round(calculated othernw_base_premium
			* othernw_new_home,.0001)
			as othernw_disc_base_premium


			, lightning_base_rate
			* lightning_census
/*CTY*/     * lightning_county
			* lightning_cova
			* lightning_ins_score
			* lightning_pool
			* lightning_rcsqft
			* lightning_sec_res
			* lightning_ded
			* case when f1.PolicyFormNumber = 9 then 1 else lightning_sp_personal_prop end
			* lightning_naogclaim
			* lightning_claim
			* case when f1.PolicyFormNumber = 3 then 1 else lightning_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else lgting_r404_OrdinanceorLaw_fac end
			as lightning_base_premium
		
			, round(calculated lightning_base_premium
			* lightning_new_home_purch,.0001)
			as lightning_disc_base_premium


			, waterw_base_rate
			* waterw_grid
/*CTY*/     * waterw_county
			* waterw_cova		
			* waterw_home_age
			* waterw_ins_score
			* waterw_rcsqft
			* waterw_roof_age
			* waterw_sec_res
			* waterw_claimxPeril
			* waterw_claim
			* waterw_ded
			* case when f1.PolicyFormNumber = 3 then 1 else waterw_r401_HO_04_20_fac end 
			* case when f1.PolicyFormNumber = 3 then 1 else waterw_r404_OrdinanceorLaw_fac end
			as waterw_base_premium
		
			, round(calculated waterw_base_premium
			* waterw_new_roof
			* waterw_new_home,.0001)
			as waterw_disc_base_premium


			, wind_base_rate
			* wind_grid
			* wind_census
			* wind_const_class
			* wind_cova
			* wind_home_age
			* wind_ins_score
			* wind_fence
			* wind_rcsqft
			* wind_ACV_roof
			* wind_roof_age
			* wind_roof_type
			* wind_sqft_story
			* wind_sec_res
			* wind_naogclaim
			* wind_claim
			* wind_ded
			* case when f1.PolicyFormNumber = 3 then 1 else wind_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else wind_r404_OrdinanceorLaw_fac end
			as wind_base_premium
		
			, round(calculated wind_base_premium
			* wind_new_home
			* wind_new_roof
			* wind_new_home_purch,.0001)
			as wind_disc_base_premium
		

			, hail_base_rate
			* hail_grid
			* hail_census
			* hail_cova
			* hail_home_age
			* hail_ACV_roof
			* hail_roof_type
			* hail_roof_age_type
			* hail_sqft_story
			* hail_sec_res
			* hail_reason
			* hail_ded
			* case when f1.PolicyFormNumber = 3 then 1 else hail_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else hail_r404_OrdinanceorLaw_fac end
			as hail_base_premium
		
			, round(calculated hail_base_premium
			* hail_new_home
			* hail_new_roof,.0001)
			as hail_disc_base_premium


			, hurr_base_rate
			* hurr_grid
			* hurr_cova
			* hurr_ded
			* hurr_ins_score
			* hurr_home_age
			* hurr_roof_age
			* hurr_ACV_roof
			* hurr_sec_res
			* hurr_claim
			* hurr_naogclaim
			* case when f1.PolicyFormNumber = 3 then 1 else hurr_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else hurr_r404_OrdinanceorLaw_fac end
			as hurr_base_premium
		
			, round(calculated hurr_base_premium
			* hurr_new_roof
/*			* hurr_new_home,.0001)*/
/*CSTAL*/	* hurr_new_home
			+ Coastal_Premium,.0001)
			as hurr_disc_base_premium
			
			
			, Cov_C_Fire_base_rate
			* Cov_C_Fire_census
/*CTY*/     * Cov_C_Fire_county
			* Cov_C_Fire_covcbase
			* Cov_C_Fire_home_age
			* Cov_C_Fire_num_res
			* Cov_C_Fire_heat_sys
			* Cov_C_Fire_ins_score
			* Cov_C_Fire_rowhouse
			* Cov_C_Fire_sec_res
			* Cov_C_Fire_naogclaimxPeril
			* Cov_C_Fire_claim
			* Cov_C_Fire_ded
			* case when f1.PolicyFormNumber = 3 then 1 else Cov_C_r403_HO_04_90_fac end
			as Cov_C_Fire_base_premium

			, round(calculated Cov_C_Fire_base_premium
			* Cov_C_Fire_new_home
			* Cov_C_Fire_new_home_purch
			* Cov_C_Fire_prot_dev,.0001)
			as Cov_C_Fire_disc_base_premium


			, Cov_C_EC_base_rate
			* Cov_C_EC_covcbase
			* Cov_C_EC_grid
			* Cov_C_EC_census
/*CTY*/     * Cov_C_EC_county
			* Cov_C_EC_ins_score
			* Cov_C_EC_naogclaim
			* Cov_C_EC_ded
			* case when f1.PolicyFormNumber = 3 then 1 else Cov_C_EC_r403_HO_04_90_fac end
			* Cov_C_EC_sec_res
			as Cov_C_EC_base_premium


			, Prop_Dam_Burglary_base_rate
			* PD_Burglary_county
			* PD_Burglary_census
			* PD_Burg_cova
/*			* PD_Burg_drc*/
			* PD_Burg_ins_score
			* PD_Burg_num_res
			* PD_Burg_num_units
			* PD_Burg_pool
			* PD_Burg_sqft
			* PD_Burg_naogclaimxperil
			* PD_Burg_claim
			* case when f1.PolicyFormNumber = 3 then 1 else PD_Burg_r401_HO_04_20_fac end
			* case when f1.PolicyFormNumber = 3 then 1 else PD_r404_OrdinanceorLaw_fac end
			* PD_Burg_ded
			* PD_Burg_sec_res
			as PD_Burg_base_premium

			, round(calculated PD_Burg_base_premium
			* PD_Burg_prot_dev,.0001)
			as PD_Burg_disc_base_premium


		from out.base_rate f1
			inner join out.grid_fac 			 	   f2 on f1.casenumber=f2.casenumber
			inner join out.census_fac   		 	   f3 on f1.casenumber=f3.casenumber
			inner join out.county_fac 				   f4 on f1.casenumber=f4.casenumber
			inner join out.ACV_Roof_fac 		 	   f5 on f1.casenumber=f5.casenumber
			inner join out.construction_class_fac	   f6 on f1.casenumber=f6.casenumber
			inner join out.coverage_a_fac 		 	   f7 on f1.casenumber=f7.casenumber
			inner join out.coverage_c_base_fac	 	   f8 on f1.casenumber=f8.casenumber
			inner join out.customer_age_fac 	 	   f9 on f1.casenumber=f9.casenumber
			inner join out.ded_fac	 				  f10 on f1.casenumber=f10.casenumber
/*			inner join out.drc_fac 					  f12 on f1.casenumber=f12.casenumber*/
			inner join out.finished_bsmnt_fac 		  f13 on f1.casenumber=f13.casenumber
			inner join out.heat_system_fac 			  f14 on f1.casenumber=f14.casenumber
			inner join out.home_age_fac 			  f15 on f1.casenumber=f15.casenumber
			inner join out.ins_score_fac 			  f16 on f1.casenumber=f16.casenumber
			inner join out.length_res_fac 			  f17 on f1.casenumber=f17.casenumber
/*			inner join out.num_dogs_fac 			  f18 on f1.casenumber=f18.casenumber*/
			inner join out.num_residents_fac 		  f19 on f1.casenumber=f19.casenumber
			inner join out.num_units_fac 			  f20 on f1.casenumber=f20.casenumber
			inner join out.basement_fac				  f21 on f1.casenumber=f21.casenumber
			inner join out.fence_fac 				  f22 on f1.casenumber=f22.casenumber
			inner join out.pool_fac					  f23 on f1.casenumber=f23.casenumber
			inner join out.rcsqft_fac				  f24 on f1.casenumber=f24.casenumber
			inner join out.roof_age_fac				  f25 on f1.casenumber=f25.casenumber
			inner join out.roof_type_fac			  f26 on f1.casenumber=f26.casenumber
			inner join out.roof_age_type_fac		  f27 on f1.casenumber=f27.casenumber
			inner join out.sqft_fac				 	  f28 on f1.casenumber=f28.casenumber
			inner join out.sqft_story_fac		 	  f29 on f1.casenumber=f29.casenumber
			inner join out.special_personal_prop_fac  f30 on f1.casenumber=f30.casenumber
			inner join out.bathroom_fac			 	  f31 on f1.casenumber=f31.casenumber
			inner join out.priorclaim_NAOG_fac		  f32 on f1.casenumber=f32.casenumber
			inner join out.priorclaim_NAOG_xPeril_fac f33 on f1.casenumber=f33.casenumber
/*			inner join out.priorclaim_xPeril_fac	  f34 on f1.casenumber=f34.casenumber*/
			inner join out.priorclaim_Peril_fac		  f35 on f1.casenumber=f35.casenumber
			inner join out.priorclaim_xWater_fac	  f36 on f1.casenumber=f36.casenumber
			inner join out.priorclaim_Water_fac		  f37 on f1.casenumber=f37.casenumber
			inner join out.new_home_fac				  f39 on f1.casenumber=f39.casenumber
			inner join out.new_home_purch_fac 		  f40 on f1.casenumber=f40.casenumber
			inner join out.new_roof_fac				  f41 on f1.casenumber=f41.casenumber
			inner join out.protective_dev_fac		  f42 on f1.casenumber=f42.casenumber
			inner join out.rowhouse_fac				  f43 on f1.casenumber=f43.casenumber
			inner join out.Seasonal_Secondary_Fac	  f45 on f1.casenumber=f45.casenumber
			inner join out.r401_add_amt_ins			  f46 on f1.casenumber=f46.casenumber
			inner join out.r403_CovC_Repl_Cost        f47 on f1.casenumber=f47.casenumber
			inner join out.r404_Ordinance_or_Law      f48 on f1.casenumber=f48.casenumber
/*			left  join out.res_motor_fac			  f49 on f1.casenumber=f49.casenumber;*/
/*COAST*/	inner join out.Coastal_Modifier			  f50 on f1.casenumber=f50.casenumber
            inner join out.REASON_CODE_FAC            f51 on f1.casenumber =f51.casenumber
/*where f1.casenumber = 133198081*/
	;quit;
