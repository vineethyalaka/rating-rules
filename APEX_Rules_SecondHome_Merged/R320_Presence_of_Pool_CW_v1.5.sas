** RULE 320. PRESENCE OF POOL **;
/*12/11/2018, for ID:  remove input() function, applied macro in excel and take factor as nunmber, so remove the convert function*/
/*02/13/2020 format all the Presence of Pool factor*/

%macro POP();
	proc sql;
		create table out.pool_fac as
		select p.casenumber, p.policynumber, p.poolpresent
			, input(f.theft, 8.3) as theft_pool
			, f.'Water Non-Weather'n as waternw_pool
			, input(f.'All Other Non-Weather'n, 8.3) as othernw_pool
			, f.lightning as lightning_pool
			, f.'Property Damage due to Burglary'n as PD_Burg_pool
			, Case when p.PolicyFormNumber = 9 then 1.000 else f.'Liability - Bodily injury (Perso'n end as liabipi_pool
			, Case when p.PolicyFormNumber = 9 then 1.000 else f.'Liability - Bodily injury (Medic'n end as liabimp_pool
			, f.'Limited Theft Coverage'n as Lim_Theft_pool
		from in.policy_info p left join fac.'320# Presence of Pool$'n f
			on p.poolpresent=substr(f1,1,1);
	quit;

%if &Process = 0 %then %do;
	title 'RULE 320. PRESENCE OF POOL';
	proc freq data=out.pool_fac;
	tables poolpresent
		*theft_pool
		*waternw_pool
		*othernw_pool
		*lightning_pool
		*PD_Burg_pool 
		*liabipi_pool
		*liabimp_pool
		*Lim_Theft_pool  / list missing;
	run;
%end;
%mend;
%POP();
