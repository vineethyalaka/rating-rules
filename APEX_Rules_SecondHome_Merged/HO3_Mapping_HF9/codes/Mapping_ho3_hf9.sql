/***************Purpose: ********************/
/*Develop HF9 ratingtable base on HO3
step1: copy rule info with insert format from excel and paste below, create ##New_State_HO3 for new state
step2: run code, analyze */
/*****************************************************************************************************/
IF OBJECT_ID('tempdb.dbo.##New_State_HO3', 'U') IS NOT NULL
  DROP TABLE ##New_State_HO3; 

CREATE TABLE ##New_State_HO3 (
HO3_for_NewState varchar(200)
)
INSERT INTO ##New_State_HO3 SELECT 'R205_Minimum_Premium_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R301_A_Base_Rates_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R301_A_Census_Block_Group_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R301_A_County_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R301_A_Coverage_A_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R301_A_Coverage_C_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R301_A_Coverage_E_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R301_A_Coverage_F_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R301_A_Grid_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R310_Townhouse_or_Rowhouse_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R311_Number_of_Units_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R312_Construction_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R313_Age_of_Home_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R314_Roof_Age_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R315_Roof_Type_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R316_Roof_Age_by_Roof_Type_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R317_Heating_System_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R318_Total_Rooms_with_Bath_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R319_Presence_of_Basement_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R320_Presence_of_Pool_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R321_Presence_of_Fence_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R322_Presence_of_Dog_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R323_Square_Footage_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R324_Square_Foot_per_Story_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R325_RC_per_SQFT_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R326_Finished_Basement_SQFT_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R327_Seasonal_Secondary_Home_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R330_Number_of_Residents_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R331_Length_of_Residence_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R332_Customer_Age_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R333_Insurance_Score_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R334_DRC_Code_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R335_Prior_Claims_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R340_New_Home_Purchase_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R341_New_Home_Construction_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R342_New_Roof_Discount_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R343_Protective_Devices_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R344_Affinity_Marketing_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R345_Policy_Conversion_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R350_Base_Expense_Factor_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R351_Bill_Plan_By_Lapse_CW_v1.2';
INSERT INTO ##New_State_HO3 SELECT 'R352_Age_of_Home_Bill_Plan_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R353_Insurance_Score_Bill_Plan_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R354_Insurance_Score_Lapse_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R355_Age_of_Home_Insurance_Score_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R356_Age_of_Home_Lapse_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R357_Customer_Age_NB_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R401_Additional_Amount_Insurance_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R403_Personal_Property_RC_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R404_Ordinance_or_Law_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R406_Deductibles_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R408_Special_Personal_Property_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R409_Basic_Enhanced_Package_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R410_ACV_Roof_CW_v1.2';
INSERT INTO ##New_State_HO3 SELECT 'R503_Business_Property_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R505_C_EQ_Loss_Assessment_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R505_D_Earthquake_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R511_Loss_Assessment_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R512_Loss_of_Use_Increased_Limit_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R514_Other_Structures_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R515_A_Increased_Cov_C_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R515_B_Decreased_Cov_C_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R515_C_Special_Limits_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R515_D_Refrigerated_Property_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R516_Scheduled_Personal_Property_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R519_Special_Computer_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R521_Water_Back_Up_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R528_Home_Business_Property_CW_v1.2';
INSERT INTO ##New_State_HO3 SELECT 'R529_Limited_Fungi_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R530_Identity_Fraud_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'R603_Residence_Employees_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R609_Business_Pursuites_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R610_Personal_Injury_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R612_Watercraft_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R613_Owned_Snowmobile_CW_v1.2';
INSERT INTO ##New_State_HO3 SELECT 'R617_Dangerous_Dog_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'R628_Home_Business_Liability_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'Step3_Subtotal_Premium_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'Step3_Supplement_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'Step4_Adjusted_Subtotal_Premium_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'Step5_Total_Property_Endorsement_Premium_CW_v1.1';
INSERT INTO ##New_State_HO3 SELECT 'Step6_Liability_Premium_CW_v1.4';
INSERT INTO ##New_State_HO3 SELECT 'Step7_Policy_Expense_Fee_CW_v1.0';
INSERT INTO ##New_State_HO3 SELECT 'Step8_Final_Premium_CW_v1.0';


IF OBJECT_ID('tempdb.dbo.##HO3_mapping_HF9', 'U') IS NOT NULL
  DROP TABLE ##HO3_mapping_HF9; 

CREATE TABLE ##HO3_mapping_HF9 (
[HO3] varchar(200),
[HF9] varchar(200)
)
INSERT INTO ##HO3_mapping_HF9 SELECT 'R205_Minimum_Premium_CW_v1.0','R205_Minimum_Premium_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R301_A_Base_Rates_CW_v1.1','R301_A_Base_Rates_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R301_A_Census_Block_Group_CW_v1.0','R301_A_Census_Block_Group_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R301_A_County_CW_v1.0','R301_A_County_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R301_A_County_CW2_v1.0','R301_A_County_KY_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R301_A_Coverage_A_CW_v1.1','R301_A_Coverage_A_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R301_A_Coverage_C_CW_v1.0','R301_A_Coverage_C_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R301_A_Coverage_E_CW_v1.0','R301_A_Coverage_E_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R301_A_Coverage_F_CW_v1.0','R301_A_Coverage_F_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R301_A_Grid_CW_v1.1','R301_A_Grid_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R310_Townhouse_or_Rowhouse_CW_v1.0','R310_Townhouse_or_Rowhouse_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R311_Number_of_Units_CW_v1.0','R311_Number_of_Units_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R312_Construction_CW_v1.0','R312_Construction_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R312_Construction_CW_v1.1','R312_Construction_CW_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R313_Age_of_Home_CW_v1.1','R313_Age_of_Home_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R314_Roof_Age_CW_v1.1','R314_Roof_Age_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R315_Roof_Type_CW_v1.0','R315_Roof_Type_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R315_Roof_Type_CW_v1.0','R315_Roof_Type_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R316_Roof_Age_by_Roof_Type_CW_v1.0','R316_Roof_Age_by_Roof_Type_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R317_Heating_System_CW_v1.0','R317_Heating_System_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R318_Total_Rooms_with_Bath_CW_v1.0','R318_Total_Rooms_with_Bath_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R318_Total_Rooms_with_Bath_CW_v1.1','R318_Total_Rooms_with_Bath_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R318_Total_Rooms_with_Bath_NV_v1.0','R318_Total_Rooms_with_Bath_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R319_Presence_of_Basement_CW_v1.0','R319_Presence_of_Basement_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R320_Presence_of_Pool_CW_v1.0','R320_Presence_of_Pool_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R320_Presence_of_Pool_PA_v1.0','R320_Presence_of_Pool_CW_v1.3';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R321_Presence_of_Fence_CW_v1.0','R321_Presence_of_Fence_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R322_Presence_of_Dog_CW_v1.0','R322_Presence_of_Dog_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R323_Square_Footage_CW_v1.0','R323_Square_Footage_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R324_Square_Foot_per_Story_CW_v1.0','R324_Square_Foot_per_Story_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R325_RC_per_SQFT_CW_v1.0','R325_RC_per_SQFT_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R326_Finished_Basement_SQFT_CW_v1.0','R326_Finished_Basement_SQFT_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R327_Seasonal_Secondary_Home_CW_v1.1','R327_Dwelling_Type_CW_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R328_Foundation_by_Year_Built_AZ_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R329_Number_of_Stories_AZ_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R330_Number_of_Residents_CW_v1.0','R330_Number_of_Residents_Tenants_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R331_Length_of_Residence_CW_v1.1','R331_Length_of_Residence_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R332_Customer_Age_CW_v1.0','R332_Customer_Age_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R333_Insurance_Score_CW_v1.1','R333_Insurance_Score_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R333_Underwriting_Tier_PA_vTA','R333_Underwriting_Tier_PA_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R334_DRC_Code_CW_v1.0','R334_DRC_Code_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R335_Prior_Claims_CW_v1.1','R335_Prior_Claims_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R335_Prior_Claims_NV_v1.0','R335_Prior_Claims_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R338_Underwriting_Tier_CW_v1.2','R338_Underwriting_Busniess_Tier_PA _v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R339_Rate_Adjustment_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R340_New_Home_Construction_CW_v1.0','R340_New_Home_Purchase_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R341_New_Home_Construction_CW_v1.1','R341_New_Home_Construction_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R342_New_Roof_Discount_CW_v1.1','R342_New_Roof_Discount_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R343_Protective_Devices_CW_v1.0','R343_Protective_Devices_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R343_Protective_Devices_CW_v1.1','R343_Protective_Devices_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R344_Partner_Discounts_CW_v1.0','R344_Partner_Discounts_KY_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R345_Policy_Conversion_CW_v1.0','R345_Policy_Conversion_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R346_Responsible_Motorist_CW_v1.3';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R350_Base_Expense_Factor_CW_v1.0','R350_Base_Expense_Factor_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R350_Base_Expense_Factor_CW_v1.0','R350_Base_Expense_Factor_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R351_Bill_Plan_By_Lapse_CW_v1.2','R351_Bill_Plan_By_Lapse_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R352_Age_of_Home_Bill_Plan_CW_v1.0','R352_Age_of_Home_Bill_Plan_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R353_Insurance_Score_Bill_Plan_CW_v1.0','R353_Insurance_Score_Bill_Plan_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R353_Underwriting_Tier_Bill_Plan_PA_v1.0','R353_Underwriting_Tier_Bill_Plan_PA_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R354_Insurance_Score_Lapse_CW_v1.0','R354_Insurance_Score_Lapse_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R354_Underwriting_Tier_Lapse_PA_v1.0','R354_Underwriting_Tier_Lapse_PA_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R355_Age_of_Home_Insurance_Score_CW_v1.0','R355_Age_of_Home_Insurance_Score_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R355_Age_of_Home_Underwriting_Tier_PA_v1.0','R355_Age_of_Home_Underwriting_Tier_PA_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R356_Age_of_Home_Lapse_CW_v1.0','R356_Age_of_Home_Lapse_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R357_Customer_Age_NB_CW_v1.0','R357_Customer_Age_NB_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R401_Additional_Amount_Insurance_CW_v1.0','R401_Additional_Amount_Insurance_CW_v1.5';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R401_Additional_Amount_Insurance_CW_v1.0','R401_Additional_Amount_Insurance_CW_v1.7';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R403_Personal_Property_RC_CW_v1.0','R403_Personal_Property_RC_CW_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R404_Ordinance_or_Law_CW_v1.0','R404_Ordinance_or_Law_CW_v1.3';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R406_Deductibles_CW_v1.1','R406_Deductibles_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R406_Deductibles_CW_vBroll','R406_Deductibles_CW_v1.3';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R408_Special_Personal_Property_CW_v1.0','R408_Special_Personal_Property_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R409_Basic_Enhanced_Package_CW_v1.0','R409_Basic_Enhanced_Package_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R410_ACV_Roof_CW_v1.2','R410_ACV_Roof_CW_v1.3';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R503_Business_Property_CW_v1.0','R503_Business_Property_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R505_C_EQ_Loss_Assessment_CW_v1.0','R505_C_EQ_Loss_Assessment_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R505_C_EQ_Loss_Assessment_CW_v1.1','R505_C_EQ_Loss_Assessment_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R505_D_Earthquake_CW_v1.0','R505_D_Earthquake_KY_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R505_D_Earthquake_NV_v1.0','R505_D_Earthquake_NV_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R511_Loss_Assessment_CW_v1.0','R511_Loss_Assessment_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R512_Loss_of_Use_Increased_Limit_CW_v1.0','R512_Loss_of_Use_Increased_Limit_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R514_Other_Structures_CW_v1.1','R514_Other_Structures_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R515_A_Increased_Cov_C_CW_v1.0','R515_A_Increased_Cov_C_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R515_B_Decreased_Cov_C_CW_v1.0','R515_B_Decreased_Cov_C_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R515_C_Special_Limits_CW_v1.0','R515_C_Special_Limits_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R515_C_Special_Limits_PA_v1.1','R515_C_Special_Limits_PA_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R515_D_Refrigerated_Property_CW_v1.0','R515_D_Refrigerated_Property_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R516_Scheduled_Personal_Property_CW_v1.0','R516_Scheduled_Personal_Property_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R516_Scheduled_Personal_Property_CW_v1.1','R516_Scheduled_Personal_Property_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R519_Special_Computer_CW_v1.0','R519_Special_Computer_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R521_Water_Back_Up_CW_v1.0','R521_Water_Back_Up_CW_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R521_Water_Back_Up_CW_v1.1','R521_Water_Back_Up_CW_v1.4';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R521_Water_Back_Up_PA_v1.1','R521_Water_Back_Up_PA_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R528_Home_Business_Property_CW_v1.0','R528_Home_Business_Property_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R528_Home_Business_Property_CW_v1.1','R528_Home_Business_Property_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R529_Limited_Fungi_CW_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R530_Identity_Fraud_CW_v1.1','R530_Identity_Fraud_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R532_Limited_Theft_CW_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R532_Limited_Theft_NV_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R532_Limited_Theft_PA_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R590_Mine_Subsidence_CW_v1.1','R590_Mine_Subsidence_KY_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R603_Residence_Employees_CW_v1.0','R603_Residence_Employees_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R609_Business_Pursuites_CW_v1.0','R609_Business_Pursuites_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R610_Personal_Injury_CW_v1.0','R610_Personal_Injury_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R612_Watercraft_CW_v1.0','R612_Watercraft_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R613_Owned_Snowmobile_CW_v1.1','R613_Owned_Snowmobile_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R613_Owned_Snowmobile_CW_v1.2','R613_Owned_Snowmobile_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R617_Dangerous_Dog_CW_v1.0','R617_Dangerous_Dog_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R620_Domestic_Animal_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT NULL,'R621_Pool_Liability_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'R628_Home_Business_Liability_CW_v1.0','R628_Home_Business_Liability_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step3_Subtotal_Premium_CW_v1.2','Step3_Subtotal_Premium_KY_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step3_Subtotal_Premium_NV_v1.0','Step3_Subtotal_Premium_NV_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step3_Supplement_CW_v1.0','Step3_Supplement_CW_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step3_Supplement_CW_v1.1','Step3_Supplement_CW_v1.2';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step4_Adjusted_Subtotal_Premium_CW_v1.0','Step4_Adjusted_Subtotal_Premium_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step5_Total_Property_Endorsement_Premium_CW_v1.0','Step5_Total_Property_Endorsement_Premium_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step5_Total_Property_Endorsement_Premium_CW_v1.1','Step5_Total_Property_Endorsement_Premium_CW_w_Mine_Sub_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step6_Liability_Premium_CW_v1.3','Step6_Liability_Premium_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step6_Liability_Premium_CW_v1.4','Step6_Liability_Premium_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step7_Policy_Expense_Fee_CW_v1.0','Step7_Policy_Expense_Fee_CW_v1.1';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step8_Final_Premium_CW_v1.0','Step8_Final_Premium_CW_v1.0';
INSERT INTO ##HO3_mapping_HF9 SELECT 'Step8_Final_Premium_CW_v1.0','Step8_Final_Premium_KY_v1.0';



SELECT * FROM ##HO3_mapping_HF9
SELECT * FROM ##New_State_HO3

/***** Matched in mapping table, 1 to 1, 1 to mutiple then choose the latest version*****/
/****Rules with frequency greater than 1, should double check when apply to new state****/
SELECT *,
CASE WHEN version IS NOT NULL THEN CONCAT(LEFT(HO3_for_NewState, LEN(HO3_for_NewState) - 3), version)
     ELSE NULL  END AS HF9_for_id
FROM
(SELECT HO3_for_NewState,MAX(RIGHT(HF9, 3)) AS version, COUNT(*) AS Frequency  FROM ##New_State_HO3  
LEFT JOIN ##HO3_mapping_HF9
ON HO3 = HO3_for_NewState
GROUP BY HO3_for_NewState) AS temp;

/******** Rules are not found in maping table********/
IF OBJECT_ID('tempdb.dbo.##NOT_FIND_HF9', 'U') IS NOT NULL
    DROP TABLE ##NOT_FIND_HF9;
SELECT TEMP.HO3_for_NewState, TEMP.HF9 INTO ##NOT_FIND_HF9 
FROM (SELECT * FROM ##New_State_HO3 
LEFT JOIN ##HO3_mapping_HF9
ON HO3 = HO3_for_NewState 
WHERE HF9 IS NULL) AS temp
 
SELECT * FROM  ##NOT_FIND_HF9

/*****Rules are not found in maping table, but there are similar rules existing in mapping table which we can get reference******/
IF OBJECT_ID('tempdb.dbo.##NOT_FIND_HF9_refer_mapping', 'U') IS NOT NULL
    DROP TABLE ##NOT_FIND_HF9_refer_mapping;
SELECT * INTO ##NOT_FIND_HF9_refer_mapping FROM (
SELECT HO3_for_NewState,LEFT(HO3_for_NewState, LEN(HO3_for_NewState)-8) AS Rule_name, M.HF9  FROM ##NOT_FIND_HF9
JOIN ##HO3_mapping_HF9 AS M 
ON LEFT(HO3, LEN(HO3)-8) = LEFT (HO3_for_NewState, LEN(HO3_for_NewState)-8)) AS temp 

SELECT * FROM ##NOT_FIND_HF9_refer_mapping

/*****Rules are not found in maping table, and no reference found in maping table****************/
SELECT b.HO3_for_NewState
FROM ##NOT_FIND_HF9_refer_mapping AS a
    RIGHT JOIN
    (SELECT * FROM ##NOT_FIND_HF9) AS b
        ON a.HO3_for_NewState = b.HO3_for_NewState
WHERE a.HO3_for_NewState IS NULL; 

/*****New rules added for typical HF9, according to BRD, reference HF9 in mapping table********/
SELECT *
FROM ##HO3_mapping_HF9
WHERE HO3 IS NULL;

