SET NOCOUNT ON

-- SET Max Excel Column
DECLARE	@maxExcelColumn VARCHAR(3)
SET		@maxExcelColumn = 'D'

DECLARE	@insertTableName VARCHAR(100)
SET		@insertTableName = '#CaseNumber'

DECLARE	@selectFormula	VARCHAR(MAX)
DECLARE	@valuesFormula	VARCHAR(MAX)

/*-----------------------------------------------------------------------------------------------
CREATE/INSERT into @SQLtoEXCEL variable table
-----------------------------------------------------------------------------------------------*/
BEGIN

	DECLARE	@SQLtoEXCEL TABLE
	(
		number INT,
		alphaGrouping INT,
		alphaPosition INT,
		excelColumnLetter VARCHAR(3)
	)

	;WITH tbl_raw AS
	(
		SELECT	number,
				(((number - 1) / 26)) AS alphaGrouping,
				(((number - 1) % 26) + 1) AS alphaPosition,
				CHAR(64 + ((number - 1) % 26) + 1) AS rightAlphaChar
		FROM	
			(
				SELECT	TOP (26 * 27) number = ROW_NUMBER() OVER (ORDER BY [object_id]) 
				FROM	sys.all_objects 
			)	tbl

	),
	tbl_final AS 
	(
		SELECT	tbl_raw.number,
				tbl_raw.alphaGrouping,
				tbl_raw.alphaPosition,
				CASE	WHEN tbl_raw.alphaGrouping = 0
						THEN tbl_raw.rightAlphaChar
						ELSE CHAR(64 + (tbl_raw.alphaGrouping)) + tbl_raw.rightAlphaChar
				END AS 'excelColumnLetter'
		FROM	tbl_raw
	)

	INSERT INTO @SQLtoEXCEL

	SELECT	tbl_final.number ,
			tbl_final.alphaGrouping ,
			tbl_final.alphaPosition ,
			RTRIM(tbl_final.excelColumnLetter)
	FROM	tbl_final;

END

/*-----------------------------------------------------------------------------------------------
Generate excel formula concatenated string 
-----------------------------------------------------------------------------------------------*/

SELECT	@selectFormula = 
			'SELECT "&'+
				REPLACE
					(
						STUFF	
							(	
								(    
									SELECT 'CHAR(39)&(SUBSTITUTE('+(excelColumnLetter+'2')+',CHAR(39),CHAR(39)&"+CHAR(39)+"&CHAR(39)))&CHAR(39)&","&'
									FROM	@SQLtoEXCEL main
									WHERE	EXISTS
												(
													SELECT	1
													FROM	@SQLtoEXCEL max_col
													WHERE	max_col.excelColumnLetter = @maxExcelColumn
													AND		max_col.number >= main.number
												)
									FOR XML PATH('')
								), 1, 0, '' 
							)
						,'&amp;','&'
					)


SELECT	@valuesFormula = 
			'VALUES ("&'+
				REPLACE
					(
						STUFF	
							(	
								(    
									SELECT 'CHAR(39)&(SUBSTITUTE('+(excelColumnLetter+'2')+',CHAR(39),CHAR(39)&"+CHAR(39)+"&CHAR(39)))&CHAR(39)&","&'
									FROM	@SQLtoEXCEL main
									WHERE	EXISTS
												(
													SELECT	1
													FROM	@SQLtoEXCEL max_col
													WHERE	max_col.excelColumnLetter = @maxExcelColumn
													AND		max_col.number >= main.number
												)
									FOR XML PATH('')
								), 1, 0, '' 
							)
						,'&amp;','&'
					)

SET NOCOUNT OFF



-- Remove rightmost 2 characters (comma and semicolon)
SELECT	@selectFormula = SUBSTRING(@selectFormula,1,LEN(@selectFormula)-5)
SELECT	@valuesFormula = SUBSTRING(@valuesFormula,1,LEN(@valuesFormula)-5)


-- return Excel Formula
-- put this value in the 2nd row 
SELECT	'=SUBSTITUTE(SUBSTITUTE("INSERT INTO '+@insertTableName+' '+@valuesFormula+'&");","'+CHAR(39)+'NULL'+CHAR(39)+'","NULL"),"'+CHAR(39)+CHAR(39)+'","NULL")' AS 'excelValuesFormula',
		'=SUBSTITUTE(SUBSTITUTE("INSERT INTO '+@insertTableName+' '+@selectFormula+'&";","'+CHAR(39)+'NULL'+CHAR(39)+'","NULL"),"'+CHAR(39)+CHAR(39)+'","NULL")' AS 'excelSelectFormula',
		'=SUBSTITUTE("INSERT INTO '+@insertTableName+' '+@valuesFormula+'&");","'+CHAR(39)+'NULL'+CHAR(39)+'","NULL")' AS 'excelValuesFormula'


/*-----------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
"''","NULL")
-------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------*/

