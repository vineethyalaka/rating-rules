** RULE 515.D REFRIGERATED PROPERTY **;
				data r515d;
				set fac.'515#D Refrigerated Property$'n;
				if strip(f1) = '' then delete;
				rename 'Rate Per Policy'n = Rate;
				run;

				proc sql;
					create table out.Refrigerated_Property as
					select e.casenumber, e.policynumber, e.HO_0498_FLAG
					, case when e.HO_0498_FLAG = "1" then round(f.charge,1.0) else 0 end as Refrigerated_Prop_Rate
					from in.Endorsement_Info e, r515d f;
				quit;