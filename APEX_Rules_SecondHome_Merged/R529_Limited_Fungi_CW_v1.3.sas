** RULE 529. LIMITED FUNGI COVERAGE **;

**Version History:
 	12 29 2016 	XX	Initial Draft
	09 26 2017	JL	Corrected code to replace Special Computer Coverage flag with Limited Fungi flag and to include rates for HF9-A and HF9-B;

%macro sc();
	data r529;
	set fac.'529# Limited Fungi$'n;
	if strip(f1) = '' then delete;
	run;
	proc sql;
		create table out.Limited_Fungi_Rate as
		select unique 
			p.CaseNumber, p.PolicyNumber, 
			p.HO_04_32,  /*Need to get a field for limited fungi*/
			p.PolicyFormNumber , Secondary_Home_Flag,
			p.Limited_Fungi as Limited_Fungi_Rate
		from r529 f,  in.PRODUCTION_INFO p
		join in.Policy_Info i on p.casenumber= i.casenumber;
	quit;


%if &Process = 0 %then %do;/
	title 'RULE 529. LIMITED FUNGI COVERAGE';
	proc freq data=out.Limited_Fungi_Rate;
	tables HO_0414_FLAG*Limited_Fungi_Rate / list missing;
	run;
%end;
%mend;
%sc();