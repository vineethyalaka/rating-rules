﻿/*2018/11/01 TM change from hard code to read in table
Assumption: hit = 1, no hit = 0, no score = -1; model 2 is TU model, model 3 is TR model*/
/*Wanwan: change no hit = 0, no score = -1*/
/*Two score models*/



/*Xi created to map insurance score class for non-mosaic prior versions in rate stabilization. 
TR -> TU :  should use the mapped class after capping
TU -> TU :  map to TR(using rate sheet, not the mapping sheet in RS), do the capping,then map back to TU */

/*Xi Add TR capping before mapping*/

data work.R333;
	set fac.'333# Insurance Risk Score$'n;
	if fire =. then delete;

	/* Parse score range to 2 columns, one score model take in F1, 2 score models take in F1, F3*/
	array parsed_tu(2) tu1-tu2;
	i=1;
	do while(scan(F1,i)ne"");
	parsed_tu(i) = scan(F1,i);
	i+1;
	end;

	drop i;
	rename tu1=TU_low tu2=TU_high;
run;


PROC SQL;
	update R333
	     set TU_high = 9999
		 where F1 like '%Above';

	select min(TU_low)
 		into :mintu
        from R333;
		 
	update R333
	     set TU_high = &mintu, TU_low = -100
		 where F1 like '%Below';

	update R333
	     set TU_high =0, TU_low = 0
		 where F1 = 'No Score';

	update R333
	     set TU_high =-1, TU_low = -1
		 where F1 = 'No Hit';

QUIT;


/*for non-mosaic versions, use mapping to get the insurance score class for TR, use the rate sheet for TU*/


%macro insscore();

proc sql;



create table ins_score_cap as
   select p.casenumber, p.policynumber, p.insscore, 
	      p.policytermyears, p.policyformnumber, p.seqno, p.scoremodel,p.matchcode,p.PCP_No_of_Matches, p.Prior_Insurance_Score_Class as PriorISC,
		  p.Current_Insurance_Score_Class as IT_CurrentISC, p.Used_Insurance_Score_Class as IT_UsedISC,
		  input(f.TR_Tier,2.) as SAS_CurrentISC /*Current ISC*/
		  /*Credit Capping does not apply if PCP is empty or if prior term/current term is no hit/no score.*/
		  /*Set Credit_Capping_flag = 1 if credit capping apply. Otherwise Set Credit_Capping_flag = 0*/
		  , case when p.Prior_Credit_Pointer = . or p.PCP_No_of_Matches in (0,-1) or p.matchcode in (0,-1) then 0 else 1 end as Credit_Capping_flag
		  /*Find cap floor and ceiling if credit capping apply from the control table*/
		  , case when calculated Credit_Capping_flag = 1 then p.Prior_Insurance_Score_Class + c.F2 else . end as Credit_Cap_Floor
		  , case when calculated Credit_Capping_flag = 1 then p.Prior_Insurance_Score_Class + c.F3 else . end as Credit_Cap_Ceiling
		  /*Check if Curent_ISC within the range of floor and ceiling*/
		  , case when calculated Credit_Capping_flag = 0 then calculated SAS_CurrentISC 
				 when calculated SAS_CurrentISC between calculated Credit_Cap_Floor and calculated Credit_Cap_Ceiling then calculated SAS_CurrentISC
				 when calculated SAS_CurrentISC < calculated Credit_Cap_Floor then calculated Credit_Cap_Floor
				 when calculated SAS_CurrentISC > calculated Credit_Cap_Ceiling then calculated Credit_Cap_Ceiling
			end as ins_tier
		  , calculated ins_tier * 1 as ins_tier_n
		  , put(calculated ins_tier_n,2.) as ins_tier_char1
		  ,strip(calculated ins_tier_char1) as ins_tier_char2

/*		  , f2.TU_tier as ins_tier_capped*/

from  in.drc_info p
left outer join ins_mapping2  f
on (p.matchcode = f.TU_high
/*or (p.insscore between f.TU_low and f.TU_high and p.scoremodel = 2 and p.matchcode not in(0,-1))*/
or (p.insscore between f.TR_low and f.TR_high and p.scoremodel = 3 and p.matchcode not in(0,-1)))
left join ratestab.'Rule 333$'n c
on c.'Prior Term Insurance'n = p.Prior_Insurance_Score_Class
/*left outer join ins_mapping2  f2*/
/*on calculated ins__tier = f2.TR_tier*/

;







create table out.ins_score_fac as
   select c.casenumber, c.policynumber, c.insscore, f.TU_low,f.TU_high,
	      c.policytermyears, c.policyformnumber, c.seqno, c.scoremodel,c.matchcode 
		  , m.TU_TIER as ins_tier
		  , m.TU_TIER as ins_tier_n

		  , f.fire as fire_ins_score
		  , f.theft as theft_ins_score
		  , f.'Water Non-Weather'n as waternw_ins_score
		  , f.'All Other Non-Weather'n as othernw_ins_score
		  , f.lightning as lightning_ins_score
		  , f.'Water Weather'n as waterw_ins_score
		  , f.wind as wind_ins_score
		  , %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_ins_score
		  , f.'Cov C - Fire'n as Cov_C_Fire_ins_score
	 	  , f.'Cov C - EC'n as Cov_C_EC_ins_score
		  , f.'Property Damage due to Burglary'n as PD_Burg_ins_score
		  , f.'Liability - Bodily injury (Perso'n as liabipi_ins_score
		  , f.'Liability - Bodily injury (Medic'n as liabimp_ins_score
		  , f.'Liability - Property Damage to O'n as liapd_ins_score		
		  , f.'Water Back-up'n as waterbu_ins_score
		  , f.'Limited Theft Coverage'n as Lim_Theft_ins_score
/*   from  in.drc_info p, r333 f ,ins_mapping2 m*/
/*   where */
/*	((p.matchcode = m.TU_high)*/
/*    or (p.insscore between m.TU_low and m.TU_high and p.scoremodel = 2 and p.matchcode not in(0,-1))*/
/*    or (p.insscore between m.TR_low and m.TR_high and p.scoremodel = 3 and p.matchcode not in(0,-1)))*/
/**/
/*	and */
/**/
/*	m.TU_Tier = f.F3;*/

	from ins_score_cap c,ins_mapping2 m ,r333 f
	where c.ins_tier_char2 = m.TR_Tier and m.TU_Tier = f.F3


/*	where 	((c.matchcode = m.TU_high)*/
/*	or (c.insscore between m.TR_low and m.TR_high and c.scoremodel = 3 and c.matchcode not in(0,-1)))*/
;

/*, r333 f ,ins_mapping2 m */
/*	where 	((c.matchcode = m.TU_high)*/
/*	or (c.insscore between m.TR_low and m.TR_high and c.scoremodel = 3 and c.matchcode not in(0,-1)))*/
/*	and m.TU_Tier = f.F3*/
/*	and c.ins_tier_n = input(m.TR_Tier,2.);*/








	insert into out.ins_score_fac
	select p.casenumber, p.policynumber, p.insscore, f.TU_low,f.TU_high,
	      p.policytermyears, p.policyformnumber, p.seqno, p.scoremodel,p.matchcode 
		  , f.F3 as ins_tier
		  , f.F3 as ins_tier_n

		  , f.fire as fire_ins_score
		  , f.theft as theft_ins_score
		  , f.'Water Non-Weather'n as waternw_ins_score
		  , f.'All Other Non-Weather'n as othernw_ins_score
		  , f.lightning as lightning_ins_score
		  , f.'Water Weather'n as waterw_ins_score
		  , f.wind as wind_ins_score
		  , %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_ins_score
		  , f.'Cov C - Fire'n as Cov_C_Fire_ins_score
	 	  , f.'Cov C - EC'n as Cov_C_EC_ins_score
		  , f.'Property Damage due to Burglary'n as PD_Burg_ins_score
		  , f.'Liability - Bodily injury (Perso'n as liabipi_ins_score
		  , f.'Liability - Bodily injury (Medic'n as liabimp_ins_score
		  , f.'Liability - Property Damage to O'n as liapd_ins_score		
		  , f.'Water Back-up'n as waterbu_ins_score
		  , f.'Limited Theft Coverage'n as Lim_Theft_ins_score

	from in.drc_info p, r333 f
	where (p.insscore between f.TU_low and f.TU_high and p.scoremodel = 2 and p.matchcode not in(0,-1));
	 



   	create table ins_tier as  /**also used in rules 353, 354, step 7*/
	      select i.casenumber, i.policynumber, i.insscore, 
	        	 i.policytermyears, i.policyformnumber, i.seqno, i.scoremodel,
				 i.matchcode, i.ins_tier_n, i.ins_tier
	      from out.ins_score_fac i;*/;
   quit;

%if &Process = 0 %then %do;
	title 'RULE 333. INSURANCE RISK SCORE';
	proc print data=out.ins_score_fac;
	where fire_ins_score*theft_ins_score*waternw_ins_score*othernw_ins_score*lightning_ins_score*waterw_ins_score
	*wind_ins_score*hurr_ins_score*Cov_C_Fire_ins_score*Cov_C_EC_ins_score*PD_Burg_ins_score
	*liabipi_ins_score*liabimp_ins_score*liapd_ins_score*waterbu_ins_score*Lim_Theft_ins_score = .;
	run;
	proc gplot data=out.ins_score_fac;
		plot (fire_ins_score theft_ins_score waternw_ins_score othernw_ins_score lightning_ins_score waterw_ins_score
	 		wind_ins_score hurr_ins_score Cov_C_Fire_ins_score Cov_C_EC_ins_score PD_Burg_ins_score
			liabipi_ins_score liabimp_ins_score liapd_ins_score waterbu_ins_score Lim_Theft_ins_score)*ins_tier_n 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.ins_score_fac;
		histogram ins_tier_n;
	run;
%end;
%mend;
%insscore();
 





