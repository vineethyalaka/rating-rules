/*Wanwan : Territory calc specific for CT without location ID, with distance to shore*/
proc sql;
	create table out.territory as
		select 
			  p.policynumber
			, p.casenumber
			, p.policyeffectivedate
			, p.countyid
			, p.census_ab as census_block_group_id
			, case
				  when input(p.census_ab,12.)<10000000000 then input(p.census_ab,12.)*10+1 
				  else input(p.census_ab,12.) 
			  end as census_block_group_id_num
			, p.distancetoshore

		from in.policy_info as p;
quit;

