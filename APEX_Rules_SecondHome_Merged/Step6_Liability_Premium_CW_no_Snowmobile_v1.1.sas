** LIABILITY PREMIUM **;

**=====================================================================================**
History: 2015 03 18 JT  Initial draft
		 2015 06 25 SY  Added endorsement premium by peril
**=====================================================================================**;


%macro liab();
	proc sql;
		create table out.step6_Liability_Premium as
		select *
			, round(liabipi_base_rate
			* liabipi_grid
			* liabipi_census
			* liabipi_cove
			* liabipi_drc
			* liabipi_ins_score
			* liabipi_num_dogs
			* liabipi_num_units
			* liabipi_pool
			* liabipi_naogclaim
			* Liab_BI_sec_res
            * Liab_BI_uw_tier,.0001)
			as liabipi_base_premium

			, round(liabimp_base_rate
			* liabimp_grid
			* liabimp_census
			* liabimp_covf
			* liabimp_drc
			* liabimp_ins_score
			* liabimp_num_dogs
			* liabimp_num_units
			* liabimp_pool
			* liabimp_naogclaim
			* Liab_MedPay_sec_res
            * Liab_MedPay_uw_tier,.0001)
			as liabimp_base_premium

			, round(liapd_base_rate
			* liapd_census
			* liapd_cove
			* liapd_ins_score
			* liapd_num_res
			* liapd_claimxPeril
			* liapd_claim
			* Liab_PD_sec_res
            * Liab_PD_uw_tier,.0001)
			as liapd_base_premium

			, round(calculated liabipi_base_premium + calculated liapd_base_premium,1.0) as Liab_Personal
			, round(calculated liabimp_base_premium, 1.0) as Liab_Medical

			, round(calculated liabipi_base_premium * liabipi_ResEmployee,.0001) as ResEmployee_bipi
			, round(calculated liabimp_base_premium * liabimp_ResEmployee,.0001) as ResEmployee_bimp
			, round(calculated ResEmployee_bipi + calculated ResEmployee_bimp,1.0) as ResEmployee

			, round(calculated liabipi_base_premium * liabipi_busipursuit,.0001) as BusiPursuit_bipi
			, round(calculated liabimp_base_premium * liabimp_busipursuit,.0001) as BusiPursuit_bimp
			, round(calculated BusiPursuit_bipi + calculated BusiPursuit_bimp,1.0) as BusiPursuit

			, round(calculated liabipi_base_premium * liabipi_personalinjury,.0001) as PersonalInjury_bipi
			, round(calculated PersonalInjury_bipi,1.0) as PersonalInjury

			, round(round(calculated liabipi_base_premium * liabipi_watercraft_1,.0001)
			+ round(calculated liabimp_base_premium * liabimp_watercraft_1,.0001)
			+ round(calculated liapd_base_premium * liapd_watercraft_1,.0001),1.0) as Watercraft_1

			, round(round(calculated liabipi_base_premium * liabipi_watercraft_2,.0001)
			+ round(calculated liabimp_base_premium * liabimp_watercraft_2,.0001)
			+ round(calculated liapd_base_premium * liapd_watercraft_2,.0001),1.0) as Watercraft_2

			, round(round(calculated liabipi_base_premium * liabipi_watercraft_3,.0001)
			+ round(calculated liabimp_base_premium * liabimp_watercraft_3,.0001)
			+ round(calculated liapd_base_premium * liapd_watercraft_3,.0001),1.0) as Watercraft_3

			, round(round(calculated liabipi_base_premium * liabipi_watercraft_4,.0001)
			+ round(calculated liabimp_base_premium * liabimp_watercraft_4,.0001)
			+ round(calculated liapd_base_premium * liapd_watercraft_4,.0001),1.0) as Watercraft_4

			, round(round(calculated liabipi_base_premium * liabipi_watercraft_5,.0001)
			+ round(calculated liabimp_base_premium * liabimp_watercraft_5,.0001)
			+ round(calculated liapd_base_premium * liapd_watercraft_5,.0001),1.0) as Watercraft_5

			, round(round(calculated liabipi_base_premium * liabipi_watercraft_1,.0001)
			+ round(calculated liabipi_base_premium * liabipi_watercraft_2,.0001)
			+ round(calculated liabipi_base_premium * liabipi_watercraft_3,.0001)
			+ round(calculated liabipi_base_premium * liabipi_watercraft_4,.0001)
			+ round(calculated liabipi_base_premium * liabipi_watercraft_5,.0001),1.0) as Watercraft_bipi

			, round(round(calculated liabimp_base_premium * liabimp_watercraft_1,.0001)
			+ round(calculated liabimp_base_premium * liabimp_watercraft_2,.0001)
			+ round(calculated liabimp_base_premium * liabimp_watercraft_3,.0001)
			+ round(calculated liabimp_base_premium * liabimp_watercraft_4,.0001)
			+ round(calculated liabimp_base_premium * liabimp_watercraft_5,.0001),1.0) as Watercraft_bimp

			, round(round(calculated liapd_base_premium * liapd_watercraft_1,.0001)
			+ round(calculated liapd_base_premium * liapd_watercraft_2,.0001)
			+ round(calculated liapd_base_premium * liapd_watercraft_3,.0001)
			+ round(calculated liapd_base_premium * liapd_watercraft_4,.0001)
			+ round(calculated liapd_base_premium * liapd_watercraft_5,.0001),1.0) as Watercraft_pd

			, calculated Watercraft_1 + calculated Watercraft_2 + calculated Watercraft_3 + calculated Watercraft_4 + calculated Watercraft_5 as Watercraft 

	/*		, round(num_snowmobile * (round(calculated liabipi_base_premium * liabipi_snowmobile,.0001)*/
	/*		+ round(calculated liabimp_base_premium * liabimp_snowmobile,.0001)*/
	/*		+ round(calculated liapd_base_premium * liapd_snowmobile,.0001)),1.0) as Snowmobile*/

			, round(calculated liabipi_base_premium * liabipi_DangerousDog,.0001) as DangerousDog_bipi
			, round(calculated liabimp_base_premium * liabimp_DangerousDog,.0001) as DangerousDog_bimp
			, round(calculated liapd_base_premium * liapd_DangerousDog,.0001) as DangerousDog_pd
			, round(calculated DangerousDog_bipi + calculated DangerousDog_bimp + calculated DangerousDog_pd,1.0) as DangerousDog

			, round(calculated liabipi_base_premium * liabipi_DomesticAnimal,.0001) as DomesticAnimal_bipi
			, round(calculated liabimp_base_premium * liabimp_DomesticAnimal,.0001) as DomesticAnimal_bimp
			, round(calculated DomesticAnimal_bipi + calculated DomesticAnimal_bimp,1.0) as DomesticAnimal

			, round(calculated liabipi_base_premium * liabipi_Pool_Liab,.0001) as Poolliab_bipi
			, round(calculated liabimp_base_premium * liabimp_Pool_Liab,.0001) as Poolliab_bimp
			, round(calculated Poolliab_bipi + calculated Poolliab_bimp,1.0) as Poolliab

/*			, round(calculated liabipi_base_premium * liabipi_Limited_Fungi,.0001) as Limited_Fungi_bipi*/
/*			, round(calculated liabimp_base_premium * liabimp_Limited_Fungi,.0001) as Limited_Fungi_bimp*/
/*			, round(calculated Limited_Fungi_bipi + calculated Limited_Fungi_bimp,1.0) as Limited_Fungi*/

			, (calculated liabipi_base_premium * Home_Bus_liabipi_fac / liabipi_grid / liabipi_census) as Home_Bus_Liability_bipi
			, (calculated liabimp_base_premium * Home_Bus_liabimp_fac / liabimp_grid / liabimp_census) as Home_Bus_Liability_bimp
			, (calculated liapd_base_premium   * Home_Bus_liabipd_fac / liapd_census) as Home_Bus_Liability_pd
			, round(calculated Home_Bus_Liability_bipi + calculated Home_Bus_Liability_bimp + calculated Home_Bus_Liability_pd,1.0) as Home_Bus_Liability

			, calculated ResEmployee_bipi + calculated BusiPursuit_bipi + calculated PersonalInjury_bipi 
			+ calculated Watercraft_bipi + calculated DangerousDog_bipi + calculated Home_Bus_Liability_bipi
			as liabipi_opt

			, calculated ResEmployee_bimp + calculated BusiPursuit_bimp + calculated Watercraft_bimp 
			+ calculated DangerousDog_bimp + calculated Home_Bus_Liability_bimp
			as liabimp_opt

			, calculated Watercraft_pd + calculated DangerousDog_pd + calculated Home_Bus_Liability_pd
			as liapd_opt

			, calculated Liab_Personal + calculated Liab_Medical + calculated ResEmployee
			+ calculated BusiPursuit + calculated PersonalInjury + calculated Watercraft
			/*+ calculated Snowmobile*/ + calculated DangerousDog + calculated DomesticAnimal 
			+ calculated Poolliab /*+ calculated Limited_Fungi*/ + calculated Home_Bus_Liability
			as Total_Liability_Premium

		from out.base_rate f1
			inner join out.grid_fac					  	 f2 on f1.casenumber=f2.casenumber
			inner join out.census_fac					 f3 on f1.casenumber=f3.casenumber
			inner join out.coverage_e_fac				 f4 on f1.casenumber=f4.casenumber
			inner join out.coverage_f_fac			 	 f5 on f1.casenumber=f5.casenumber
			inner join out.drc_fac					 	 f6 on f1.casenumber=f6.casenumber
			inner join out.ins_score_fac				 f7 on f1.casenumber=f7.casenumber
			inner join out.num_dogs_fac				 	 f8 on f1.casenumber=f8.casenumber
			inner join out.num_residents_fac		 	 f9 on f1.casenumber=f9.casenumber
			inner join out.num_units_fac		    	 f10 on f1.casenumber=f10.casenumber
			inner join out.priorclaim_NAOG_fac			 f11 on f1.casenumber=f11.casenumber
			inner join out.priorclaim_xPeril_fac		 f12 on f1.casenumber=f12.casenumber
			inner join out.priorclaim_Peril_fac			 f13 on f1.casenumber=f13.casenumber
	/*		inner join out.lia_OtherInsLoc_fac			 f14 on f1.casenumber=f14.casenumber*/
			inner join out.lia_ResEmployee_fac			 f15 on f1.casenumber=f15.casenumber
			inner join out.lia_busipursuit_fac			 f16 on f1.casenumber=f16.casenumber
			inner join out.lia_personalinjury_fac		 f17 on f1.casenumber=f17.casenumber
			inner join out.lia_watercraft_fac			 f18 on f1.casenumber=f18.casenumber
	/*		inner join out.lia_snowmobile_fac			 f20 on f1.casenumber=f20.casenumber*/
			inner join out.lia_DangerousDog_fac			 f21 on f1.casenumber=f21.casenumber
			inner join out.pool_fac						 f23 on f1.casenumber=f23.casenumber
			inner join out.Home_Business_Liability_Fac   f24 on f1.casenumber=f24.casenumber 
			inner join out.Seasonal_Secondary_Fac		 f25 on f1.casenumber=f25.casenumber
			inner join out.lia_DomesticAnimal_fac   	 f26 on f1.casenumber=f26.casenumber
			inner join out.lia_Pool_Liab_fac        	 f27 on f1.casenumber=f27.casenumber
/*			inner join out.lia_Limited_Fungi_fac    	 f28 on f1.casenumber=f28.casenumber*/
			inner join out.uw_tier_fac                      f29 on f1.casenumber=f29.casenumber
;
	quit;

%if &Process = 0 %then %do;
	title 'STEP 6: LIABILITY PREMIUM (RULE 301.A.6)';
	proc print data=out.step6_Liability_Premium;
	where Total_Liability_Premium = .;
	run;
	proc sql;
		select mean(liabipi_base_premium) as liabipi_base_prem_avg		
			, mean(liabimp_base_premium) as liabimp_base_prem_avg		
			, mean(liapd_base_premium) as liapd_base_prem_avg
			, mean(Total_Liability_Premium) as Total_Liability_prem_avg
		from out.step6_Liability_Premium;
	quit;
%end;
/*****************************************************************************************************************/

%mend;
%liab();
