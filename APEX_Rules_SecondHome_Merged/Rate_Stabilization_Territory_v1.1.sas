
/*Xi different data format in cencus_block_group_id*/

proc sql;
	create table out.territory as
	select p.policynumber
      , p.CaseNumber
      , p.policytermyears
      , p.policyeffectivedate
      , input(p.countyiD,5.) as countyiD
      , p.Census2010 as census_block_group_ID
      , p.LocationID
	from in.policy_info as p;
quit;