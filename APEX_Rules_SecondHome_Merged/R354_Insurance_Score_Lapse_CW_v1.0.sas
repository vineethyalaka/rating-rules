** RULE 354. INSURANCE SCORE BY LAPSE **;
%macro inslapse();
	data exp_ins_score_x_lapse_fac (keep=lapse ins_tier factor);
	set fac.'354# Ins Score by Lapse$'n;
	if f3=. then delete;
	lapse=0; factor = 'Expense Fee'n; output;
	lapse=1; factor = f3; output;
	rename f1 = ins_tier;
	run;
	proc sql;
		create table out.exp_ins_score_x_lapse_fac as
		select p1.*, p2.matchcode, p2.ScoreModel, p2.insscore, p2.ins_tier, p2.ins_tier_n, f.factor as exp_ins_score_x_lapse_fac
		from lapse p1 
			inner join ins_tier p2 on p1.casenumber = p2.casenumber
			left join exp_ins_score_x_lapse_fac f on f.lapse = p1.lapse and f.ins_tier = p2.ins_tier;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 354. INSURANCE SCORE BY LAPSE';
	proc freq data=out.exp_ins_score_x_lapse_fac;
	tables lapse*ins_tier_n*exp_ins_score_x_lapse_fac / list missing;
	run;
%end;
%mend;
%inslapse();