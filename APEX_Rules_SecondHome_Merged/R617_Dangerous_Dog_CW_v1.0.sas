** RULE 617. DANGEROUS DOG **;
%macro dd();
	data r617fac;
	set fac.'617# Dangerous Dog$'n;
	if f1="" then delete;
	fac1='Liability - Bodily injury (Perso'n*1;
	fac2='Liability - Bodily injury (Medic'n*1;
	fac3='Liability - Property Damage to O'n*1;
	run;
	data DangerousDog; 
	set in.Policy_Info (keep=casenumber policynumber PolicyFormNumber DogBreed);
	if upcase(DogBreed) in ("","GERMAN SHEPHERD","NONE OF THE ABOVE") or PolicyFormNumber = 9 then DangerousDog_flag = 0;
	else DangerousDog_flag = 1;
	run;
	proc sql;
	    create table out.lia_DangerousDog_fac as
		select p.*
			, f.fac1 * DangerousDog_flag as liabipi_DangerousDog
			, f.fac2 * DangerousDog_flag as liabimp_DangerousDog
			, f.fac3 * DangerousDog_flag as liapd_DangerousDog
		from r617fac f, DangerousDog p;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 617. DANGEROUS DOG';
	proc freq data=out.lia_DangerousDog_fac;
	tables dogbreed
		*liabipi_DangerousDog
		*liabimp_DangerousDog
		*liapd_DangerousDog / list missing;
	run;
%end;
%mend;
%dd();
