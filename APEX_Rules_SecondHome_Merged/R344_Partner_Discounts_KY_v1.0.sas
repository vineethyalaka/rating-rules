
data affinity; 
set in.policy_info (keep=casenumber policynumber policyaccountnumber autopolicynumber);
if policyaccountnumber in (
 1020 :1026 
,4000 :4499 
,11510:11520
,13200:13299
,13800:13899
,14000:14500
,14900:14999
)
	then partner = 1;
else partner = 0;
if autopolicynumber = '' then auto = 0;
else auto = 1;
run;


	proc sql;
		create table out.affinity_fac as
		select p.*, a.partnerflag, 
case 
 	
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

        when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

        when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/


		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

        when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

        when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

        when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=30000 and a.PolicyAccountNumber<=30099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=30000 and a.PolicyAccountNumber<=30099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=30000 and a.PolicyAccountNumber<=30099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=30000 and a.PolicyAccountNumber<=30099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=14000 and a.PolicyAccountNumber<=14500 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=14000 and a.PolicyAccountNumber<=14500 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=14000 and a.PolicyAccountNumber<=14500 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.1 /*Auto Partner*/
		when a.PolicyAccountNumber>=14000 and a.PolicyAccountNumber<=14500 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.1 /*Affiliate Auto Discounts*/

else auto*partner*0.1
/*'Affinity Discount'n*/ end as affinity
		from affinity p inner join in.policy_info a on p.casenumber=a.casenumber/*, affinity_fac f*/;
	quit;