
/*wanwan: remove DRC from 1.2 version*/

** RULE 531. LIMITED THEFT COVERAGE **;
%macro ltc();
	*Base Premium*;
	proc sql;
		create table out.limited_theft_disc_base_prem as
		select *
			, LimitedTheftCov 
			* /*round(*/ limited_theft_base_rate
			* Limited_Theft_county
			* Limited_Theft_census
			* case when sec_residence = 'HF9-A' then Lim_Theft_A_covcbase
			when sec_residence = 'HF9-B' then Lim_Theft_B_covcbase
			when sec_residence = 'HF9-C' then Lim_Theft_C_covcbase else 0 end 
/*			* Lim_Theft_drc*/
			* Lim_Theft_num_res
			* Lim_Theft_ins_score
			* Lim_Theft_num_units
			* Lim_Theft_pool
			* Lim_Theft_sqft
			* Lim_Theft_naogclaimxperil
			* Lim_Theft_claim
			* Lim_Theft_ded
			* case when f1.PolicyFormNumber = 3 then 1 else Lim_Theft_r403_HO_04_90_fac end
			* Lim_Theft_sec_res /*,1.0)*/
			as lim_theft_base_premium
/*			, round(calculated lim_theft_base_premium*/
/*			* Lim_Theft_prot_dev,1.0)*/
/*			as lim_theft_disc_base_prem*/

			, round(round(calculated lim_theft_base_premium
			* Lim_Theft_prot_dev
			* limited_theft_resmotor,.0001),1.0)
			as lim_theft_disc_base_prem
			, LimitedTheftCov
			, case when sec_residence = 'HF9-A' then Lim_Theft_A_covcbase
			when sec_residence = 'HF9-B' then Lim_Theft_B_covcbase
			when sec_residence = 'HF9-C' then Lim_Theft_C_covcbase 
			else 0 end as Lim_Theft_covcbase
		from out.base_rate f1
			inner join out.census_fac			  f3 on f1.casenumber=f3.casenumber
			inner join out.num_residents_fac	  f6 on f1.casenumber=f6.casenumber
			inner join out.ins_score_fac		  f8 on f1.casenumber=f8.casenumber
			inner join out.r403_CovC_Repl_Cost	 f11 on f1.casenumber=f11.casenumber
			inner join out.Seasonal_Secondary_Fac f12 on f1.casenumber=f12.casenumber
			inner join out.county_fac			f13 on f1.casenumber=f13.casenumber
/*			inner join out.drc_fac				f14 on f1.casenumber=f14.casenumber*/
			inner join out.coverage_c_base_fac  f15 on f1.casenumber=f15.casenumber
			inner join out.num_units_fac		f16 on f1.casenumber=f16.casenumber
			inner join out.pool_fac				f17 on f1.casenumber=f17.casenumber
			inner join out.sqft_fac				f18 on f1.casenumber=f18.casenumber
			inner join out.priorclaim_NAOG_xPeril_fac f19 on f1.casenumber=f19.casenumber
			inner join out.priorclaim_Peril_fac f20 on f1.casenumber=f20.casenumber
			inner join out.ded_fac				f21 on f1.casenumber=f21.casenumber
			inner join out.protective_dev_fac   f22 on f1.casenumber=f22.casenumber
			inner join out.res_motor_fac        f23 on f1.casenumber=f23.casenumber
			left  join (select Casenumber, LimitedTheftCov from in.Policy_Info) p 	on f1.casenumber=p.casenumber;

	quit;

%if &Process = 0 %then %do;
	title 'RULE 531. LIMITED THEFT COVERAGE';
	proc print data=out.limited_theft_disc_base_prem;
	where lim_theft_base_premium = .;
	run;
	proc sql;
		select mean(lim_theft_base_premium) as lim_theft_base_prem_avg,
		mean(lim_theft_disc_base_prem) as lim_theft_disc_base_prem_avg
		from out.limited_theft_disc_base_prem; 
	quit;
%end;
%mend;
%ltc();
