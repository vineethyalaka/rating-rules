** RULE 690. Lead Liability **;
     proc sql;
           create table out.lia_LeadLiabilityExcl_Credit as
           select t1.casenumber
				, t1.policynumber
				, t1.policyformnumber
				, s.sec_residence
				, t1.propertyunitsinbuilding
				, t2.LeadLiability_Coverage_Flag
				, t2.LeadLiability_Exclusion_Flag
				, case when t2.LeadLiability_Exclusion_Flag = 0 then 0 /*No exclusion, so no credit */
				       when t2.LeadLiability_Coverage_Flag = 1 then 0 /*Exclusion on policy but has the buyback, so no credit */
				       else f.Credit
				  end as lead_excl_Credit
           from in.policy_info t1 
           inner join in.endorsement_info t2
                on t1.casenumber = t2.casenumber
           left join fac.'690# Lead Liability$'n f
                on t1.propertyunitsinbuilding=f.'Number of Units in Building'n
           inner join out.seasonal_secondary_fac s
                on t1.casenumber = s.casenumber;
     quit;

     proc sql;
           create table out.lia_LeadLiabilityExcl_Charge as
           select t1.casenumber
                , t1.policynumber
				, t1.policyformnumber
                , s.sec_residence
				, t1.propertyunitsinbuilding
				, t2.LeadLiability_Coverage_Flag
				, case when t1.policyformnumber = 9 and t2.LeadLiability_Coverage_Flag = 1 then f.Charge
				       else 0 
				   end as lead_excl_Charge
             from in.policy_info t1 
       inner join in.endorsement_info t2
               on t1.casenumber = t2.casenumber
        left join fac.'690# Lead Liability$'n f
               on t1.propertyunitsinbuilding=f.'Number of Units in Building'n
           inner join out.seasonal_secondary_fac s
                on t1.casenumber = s.casenumber;
     quit;
