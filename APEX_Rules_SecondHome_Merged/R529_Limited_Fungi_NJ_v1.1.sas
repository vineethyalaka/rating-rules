** RULE 529. LIMITED FUNGI COVERAGE **;
** 2017 09 15 AWH Allow rates to vary by form, dwelling type;
** 2017 09 22 AWH Introduce base rates for HF9;


%macro fungi();
/*data r529_base;*/
/*	set fac.'529# Limited Fungi$'n;*/
/*	if coverage ^= 'Base Limits' then delete;*/
/*run;*/
/*data r529_prop;*/
/*	set fac.'529# Limited Fungi$'n;*/
/*	if coverage ^= 'Section I - Property' then delete;*/
/*	Incr_Limit = 'Limit'n;*/
/*run;*/
/*data r529_liab;*/
/*	set fac.'529# Limited Fungi$'n;*/
/*	if coverage ^= 'Section II - Liability' then delete;*/
/*	Incr_Limit = 'Limit'n;*/
/*run;*/

data limit_fungi;
 set fac.'529# Limited Fungi$'n;
length coverage $ 40.;
if _n_=1 then coverage = 'Base Limits';
if _n_=2 then coverage = 'Section I - Property';
if _n_=3 then coverage = 'Section I - Property';
if _n_=4 then coverage = 'Section II - Liability';
run;

data r529_base;
	set limit_fungi;
	if coverage ^= 'Base Limits' then delete;
run;
data r529_prop;
	set limit_fungi;
	if coverage ^= 'Section I - Property' then delete;
	Incr_Limit = 'Limit'n;
run;
data r529_liab;
	set limit_fungi;
	if coverage ^= 'Section II - Liability' then delete;
	Incr_Limit = 'Limit'n;
run;
proc sql; 
create table work.Limited_Fungi_Rate as
	select 
		p.CaseNumber, 		p.PolicyNumber, 
		p.PolicyFormNumber,	p.Secondary_Home_Flag,	
		r.Property_Amount, 	r.Liability_Amount 
	from in.policy_info as p
	inner join in.production_info as r on r.casenumber=p.casenumber
;quit;
proc sql; 
create table work.Limited_Fungi_Rate as
	select 	p.*, 
		case	
			when p.PolicyFormNumber = 3 then round(f.'HO3'n,1.0) 
			when p.PolicyFormNumber = 9 then 
			(case 	when Secondary_Home_Flag=1 then round(f.'HF9-A'n,1.0) 
		 			when Secondary_Home_Flag=2 then round(f.'HF9-B'n,1.0) 
					when Secondary_Home_Flag=3 then round(f.'HF9-C'n,1.0)
	 				else 0 end) 
	else 0 end as Limited_Fungi_Base_Rate
	from  work.Limited_Fungi_Rate as p
		, work.r529_base as f
;quit;
proc sql; 
create table out.Limited_Fungi_Rate as
	select 	p.*
	, case	when f1.Incr_Limit =. then 0
			when p.PolicyFormNumber = 3 then round(f1.'HO3'n,1.0) 
			when p.PolicyFormNumber = 9 then 
			(case 	when Secondary_Home_Flag=1 then round(f1.'HF9-A'n,1.0) 
		 			when Secondary_Home_Flag=2 then round(f1.'HF9-B'n,1.0) 
					when Secondary_Home_Flag=3 then round(f1.'HF9-C'n,1.0)
	 				else 0 end) 
	else 0 end as Limited_Fungi_Property_Rate

	, case	when f2.Incr_Limit =. then 0
			when p.PolicyFormNumber = 3 then round(f2.'HF9-C'n,1.0) 
			when p.PolicyFormNumber = 9 then 
			(case 	when Secondary_Home_Flag=1 then round(f2.'HF9-A'n,1.0) 
		 			when Secondary_Home_Flag=2 then round(f2.'HF9-B'n,1.0) 
					when Secondary_Home_Flag=3 then round(f2.'HF9-C'n,1.0) 
					else 0 end)
	else 0 end as Limited_Fungi_Liability_Rate

	, 	Limited_Fungi_Base_Rate +
		calculated Limited_Fungi_Property_Rate +
		calculated Limited_Fungi_Liability_Rate 
	as Limited_Fungi_Rate

from Limited_Fungi_Rate as p
	left join work.r529_prop as f1 on p.Property_Amount = f1.Incr_Limit
	left join work.r529_liab as f2 on p.Liability_Amount = f2.Incr_Limit
;quit;

%if &Process = 0 %then %do;
	title 'RULE 529. LIMITED FUNGI COVERAGE';
	proc freq data=out.Limited_Fungi_Rate;
	tables 	Property_Amount*Liability_Amount*
				Limited_Fungi_Base_Rate*
				Limited_Fungi_Property_Rate*
				Limited_Fungi_Liability_Rate*
			Limited_Fungi_Rate / list missing;
	run;
%end;
%mend;
%fungi();
