** RULE 332. CUSTOMER AGE **;
/*2019/06/24 TM add more perils*/
%macro cust_age();
	data cust_age;
	set in.policy_info (keep=casenumber policynumber policytermyears policyissuedate policyeffectivedate DOB1 DOB2);
	length cust_age $4.;
	if policytermyears=1 and &effst=0 then age=max(floor((policyissuedate - dob1)/365),floor((policyissuedate - dob2)/365));
	else age=max(floor((policyeffectivedate - dob1)/365),floor((policyeffectivedate - dob2)/365));
	if age>=110 then cust_age='110+';
	else if age<18 then cust_age='18';
	else cust_age=compress(age);
	run;

	proc sql;
		create table out.customer_age_fac as

		select distinct p.*
			, f.fire as fire_cust_age
			, f.theft as theft_cust_age
			, f.'Water Non-Weather'n as waternw_cust_age
			, f.'All Other Non-Weather'n as othernw_cust_age
			, f.lightning as lightning_cust_age
			, f.'Water Weather'n as waterw_cust_age
			, f.Wind as Wind_cust_age
			, f.Hail as Hail_cust_age
			, f.Hurricane as Hurr_cust_age
			, f.'Cov C - Fire'n as Cov_C_fire_cust_age
			, f.'Cov C - EC'n as Cov_c_EC_cust_age
			, f.'Property Damage due to Burglary'n as PD_Burg_cust_age
			, f.'Liability - Bodily injury (Perso'n as liabipi_cust_age
			, f.'Liability - Bodily Injury (Medic'n as liabimp_cust_age
			, f.'Liability - Property Damage to O'n as liapd_cust_age
			, f.'Water Back-up'n as waterbu_cust_age
			, f.'Limited Theft Coverage'n as limit_theft_cust_age

		from cust_age p left join fac.'332# Customer Age$'n f
			on p.cust_age=f.f1
			order by casenumber;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 332. CUSTOMER AGE';
	proc print data=out.customer_age_fac;
	where theft_cust_age*othernw_cust_age = .;
	run;
	proc gplot data=out.customer_age_fac;
		plot (theft_cust_age othernw_cust_age)*age 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
%end;

%mend;
%cust_age;