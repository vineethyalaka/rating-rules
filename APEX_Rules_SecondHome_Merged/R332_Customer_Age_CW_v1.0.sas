** RULE 332. CUSTOMER AGE **;
%macro cust_age();
	data cust_age;
	set in.policy_info (keep=casenumber policynumber policytermyears policyissuedate policyeffectivedate DOB1 DOB2);
	length cust_age $4.;
	if policytermyears=1 and &effst=0 then age=max(floor((policyissuedate - dob1)/365),floor((policyissuedate - dob2)/365));
	else age=max(floor((policyeffectivedate - dob1)/365),floor((policyeffectivedate - dob2)/365));
	if age>=110 then cust_age='110+';
	else if age<18 then cust_age='18';
	else cust_age=compress(age);
	run;

	proc sql;
		create table out.customer_age_fac as

		select distinct p.*
			, f.theft as theft_cust_age			
			, f.'All Other Non-Weather'n as othernw_cust_age			
		from cust_age p left join fac.'332# Customer Age$'n f
			on p.cust_age=f.f1
			order by casenumber;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 332. CUSTOMER AGE';
	proc print data=out.customer_age_fac;
	where theft_cust_age*othernw_cust_age = .;
	run;
	proc gplot data=out.customer_age_fac;
		plot (theft_cust_age othernw_cust_age)*age 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
%end;

%mend;
%cust_age;