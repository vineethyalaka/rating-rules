** RULE 404. ORDINANCE OR LAW **;


**=====================================================================================**
History: 2017 10 03 WZ      Version Control: Extract directly from RatingQA OH Apex Rating Engine
**=====================================================================================**;


%macro or();
	data r404;
	set fac.'404# Ordinance or Law$'n;
	if 'HO3'n = . then delete;
	run;
	proc sql;
	    create table out.r404_Ordinance_or_Law as
		select p.CaseNumber, p.PolicyNumber, p.PolicyFormNumber, p.ORD_LAW_FLAG
			, case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 3 then f.'HO3'n else 1 end as r404_Ordinance_or_Law_fac,
			case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 9 then f.'HF9-P1'n
			else 1 end as fire_r404_OrdinanceorLaw_fac,
			case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 9 then f.'HF9-P3'n
			else 1 end as waternw_r404_OrdinanceorLaw_fac,
			case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 9 then f.'HF9-P4'n
			else 1 end as othernw_r404_OrdinanceorLaw_fac,
			case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 9 then f.'HF9-P5'n
			else 1 end as lgting_r404_OrdinanceorLaw_fac,
			case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 9 then f.'HF9-P6'n
			else 1 end as waterw_r404_OrdinanceorLaw_fac,
			case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 9 then f.'HF9-P7'n
			else 1 end as wind_r404_OrdinanceorLaw_fac,
			case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 9 then f.'HF9-P8'n
			else 1 end as hail_r404_OrdinanceorLaw_fac,
			case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 9 then f.'HF9-P12'n
			else 1 end as PD_r404_OrdinanceorLaw_fac
		from in.Endorsement_Info p, r404 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 404. ORDINANCE OR LAW';
	proc freq data=out.r404_Ordinance_or_Law;
	tables ORD_LAW_FLAG*r404_Ordinance_or_Law_fac*PolicyFormNumber / list missing;
	run;
%end;
%mend;
%or();


