** RULE 512. LOSS OF USE � INCREASED LIMIT **;
%macro lou();
data r512;
set fac.'512# Loss of Use Inc Time Pd$'n;
if 'Loss of Use - Increased Time Per'n ne '24 Months' then delete;
run;
proc sql;
	create table out.loss_of_use_rate as
	select p1.casenumber, p1.policynumber, p1.Loss_of_Use_pay_period, p1.coveragea, f.factor, p2.subtotal_premium
		 , case when p1.Loss_of_Use_pay_period=24 then p1.coverageA*0.05 else 0 end as Incr_CovD
		 , round(case when p1.Loss_of_Use_pay_period=24 then p2.subtotal_premium*f.factor else 0 end, 1) as loss_of_use_rate
		 , case when p1.Loss_of_Use_pay_period=24 then p2.subtotal_premium*f.factor else 0 end as loss_of_use_rate_nr
	from in.policy_info p1 inner join out.step3_subtotal_premium p2 
		on p1.casenumber=p2.casenumber
		,r512 f;

%if &Process = 0 %then %do;
title 'RULE 512. LOSS OF USE � INCREASED TIME PERIOD';
proc freq data=out.loss_of_use_rate;
tables Incr_CovD*loss_of_use_rate / list missing;
run;
%end;
%mend;
%lou();