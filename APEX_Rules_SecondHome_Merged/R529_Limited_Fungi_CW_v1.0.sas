** RULE 529. LIMITED FUNGI COVERAGE **;
%macro sc();
	data r529;
	set fac.'529# Limited Fungi$'n;
	if strip(f1) = '' then delete;
	run;
	proc sql;
		create table out.Limited_Fungi_Rate as
		select p.CaseNumber, p.PolicyNumber, 
		p.HO_0414_FLAG,  /*Need to get a field for limited fungi*/
		case when p.HO_0414_FLAG="1" and PolicyFormNumber = 9 then round(f.'HF9-C'n,1.0) else 0 end as Limited_Fungi_Rate
		from in.Endorsement_Info p, r529 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 529. LIMITED FUNGI COVERAGE';
	proc freq data=out.Limited_Fungi_Rate;
	tables HO_0414_FLAG*Limited_Fungi_Rate / list missing;
	run;
%end;
%mend;
%sc();
