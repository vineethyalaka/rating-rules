** RULE 311. NUMBER OF UNITS **;
proc sql;
	create table out.num_units_fac as
		select p.casenumber, p.policynumber, p.propertyunitsinbuilding
			, f.theft as theft_num_units
			, f.'Liability - Bodily injury (Perso'n as liabipi_num_units
			, f.'Liability - Bodily injury (Medic'n as liabimp_num_units
		from in.policy_info p left join fac.'311# Number of Units$'n f
			on p.propertyunitsinbuilding=f.f1;
quit;