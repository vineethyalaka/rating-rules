/* --------------------------------------------------------------------------------------------------												
														HISTORY
	1. 02-06-2020 VR ITR-15050 Code the initial file
----------------------------------------------------------------------------------------------------- */


data r301a_WFRG_sheet_manipulation;
	set fac.'301#A Wildfire Risk Group$'n(drop=f4 f5);
	where Wildfire is not missing;
	rename f1=Risk_Group Wildfire=Interface f3=All_Other;
run;

proc sql;

create table out.WF_RiskGroup_Fac as
	select p.casenumber
			, p.PBWFScore
			, p.PB_RiskType
			, wfrg.Interface
			, wfrg.All_Other
			, case when p.PB_RiskType='IF' then wfrg.Interface
			  	   else wfrg.All_Other
			  end as WF_wfrg_fac
	from in.policy_info p
	join r301a_WFRG_sheet_manipulation wfrg
		on p.PBWFScore = wfrg.Risk_Group;         /*Score 1 maps to group 1, score 2 maps to group 2 and so on...*/
quit;
