
%macro UNDW();
/*	data Underwriting_Tier;*/
/*	set in.policy_info;*/
/*	if policyaccountnumber in (14000,14200,14300,14310,14400) then undwtier = 2;*/
/*	else undwtier = 1;*/
/*	run;*/

	data Underwriting_Tier;
	set in.policy_info;
	if policyaccountnumber in (14000,14200,14300,14310,14400) then undwtier = 2;
	else undwtier = 1;
	run;

	
	data R338;
	set fac.'338# Underwriting Tier$'n;
	if F1=. then delete;
	rename F2 = Factor;
	run;


	proc sql;
		create table out.undwtier as
		select    p.casenumber
                , p.policynumber
				/*,f.factor as undwtier*/
				, Fire as Fire_undwtier
				, Theft as Theft_undwtier
				, 'Water Non-Weather'n as waternw_undwtier
				, 'All Other Non-Weather'n as allothernw_undwtier
				, Lightning as Lightning_undwtier
				, 'Water Weather' as waterw_undwtier
				, Wind as wind_undwtier
				, Hail as Hail_undwtier
				, Hurricane as hurr_undwtier
				, 'Cov C - Fire'n as CovCFire_undwtier
				, 'Cov C - EC'n as CovCEC_undwtier
				, 'Property Damage due to Burglary'n as PD_Blry_undwtier
				, 'Liability - Bodily Injury (Perso'n as lia_BI_PI_undwtier
				, 'Liability - Bodily Injury (Medic'n as lia_BI_MP_undwtier
				, 'Liability - Property Damage to O'n as lia_PD_undwtier
				, 'Water Back-up'n as Water_bkup_Fire_undwtier
				, 'Limited Theft Coverage'n as Limited_Theft_undwtier
				, 'Expense Fee'n as Expense_undwtier
		from Underwriting_Tier p
		left join R338 f on p.undwtier=f.f1;
	quit;

%mend;
%UNDW();


