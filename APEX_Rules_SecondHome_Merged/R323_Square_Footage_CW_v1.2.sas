** RULE 323. SQUARE FOOTAGE **;
%macro SF();
	data sqft; set in.policy_info;
	if propertysquarefootage = 0 then propertysquarefootage = 1850;
	run;

	data sqft_fac;
		set fac.'323# Square Footage$'n (obs=21 firstobs=2);
		if f3=. and theft ne . then f3=99999;
	run;

	proc sql;
		create table out.sqft_fac as
		select p.casenumber, p.policynumber, p.propertysquarefootage
			, f.theft as theft_sqft, f.'Property Damage due to Burglary'n as PD_Burg_sqft
			, f.'Limited Theft Coverage'n as Lim_Theft_sqft			
		from sqft p left join sqft_fac f
			on p.propertysquarefootage>=f.f1
			and p.propertysquarefootage<=f.f3;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 323. SQUARE FOOTAGE';
	proc print data=out.sqft_fac;
	where theft_sqft= .;
	run;
	proc freq data=out.sqft_fac;
		tables theft_sqft*propertysquarefootage*PD_Burg_sqft*Lim_Theft_sqft
			/ list missing;
	run;
	quit; 
%end;
%mend;
%SF();
