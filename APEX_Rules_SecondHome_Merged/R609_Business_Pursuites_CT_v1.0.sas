** RULE 609. BUSINESS PURSUITS **;
			data r609fac;
			set fac.'609# Business Pursuits$'n;
			if f1="" then delete;
			fac1='Liability - Bodily injury (Perso'n*1;
			fac2='Liability - Bodily injury (Medic'n*1;
			run;
			proc sql;
			    create table out.lia_busipursuit_fac as
				select p.casenumber, p.policynumber, p.Business_Pursuits
					, f.fac1 * input(p.Business_Pursuits,1.) as liabipi_busipursuit
					, f.fac2 * input(p.Business_Pursuits,1.) as liabimp_busipursuit
				from r609fac f, in.Endorsement_Info p;
			quit;