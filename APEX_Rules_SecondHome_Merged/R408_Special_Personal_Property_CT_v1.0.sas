** RULE 408. SPECIAL PERSONAL PROPERTY COVERAGE **;
data special_personal_prop_fac;
	set fac.'408# Special Personal Prop$'n;

	if f1='' then
		delete;
	fire_fac	= 1*Fire;
	theft_fac	= 1*Theft;
	waternw_fac	= 1*'Water Non-Weather'n;
	othernw_fac	= 1*'All Other Non-Weather'n;
	lightning_fac = 1*lightning;
run;
proc sql;
	create table out.special_personal_prop_fac as
	select p1.casenumber, p1.policynumber, p1.HO_0015_FLAG, p2.HH0015
		, case when p1.HO_0015_FLAG = '0' and p2.HH0015 = '0' then 0 else 1 end as HH0015_flag
		, case when calculated HH0015_flag = 1 then f.fire_fac		else 1 end as fire_sp_personal_prop
		, case when calculated HH0015_flag = 1 then f.waternw_fac	else 1 end as waternw_sp_personal_prop
		, case when calculated HH0015_flag = 1 then f.theft_fac		else 1 end as theft_sp_personal_prop
		, case when calculated HH0015_flag = 1 then f.othernw_fac	else 1 end as othernw_sp_personal_prop
		, case when calculated HH0015_flag = 1 then f.lightning_fac	else 1 end as lightning_sp_personal_prop
	from in.Endorsement_Info p1
		inner join in.Production_Info p2 on p1.casenumber = p2.casenumber
		, special_personal_prop_fac f
	;
quit;