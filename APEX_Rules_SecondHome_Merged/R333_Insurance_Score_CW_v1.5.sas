/*Assumption: hit = 1, no hit = 0, no score = -1; model 2 is TU model, model 3 is TR model*/
/*Two score models*/

/*2018 12 03	WZ	read two rate sheets by score model type*/
/*2019 06 05    TM  change matchcode=0 to No Score, marchcode=-1 to No hit, ITR14235*/
/*2020 01 21    DG  Add Cov_C_Fire_ins_score; Cov_C_EC_ins_score; PD_Burg_ins_score for GA Apex Mosaic, ITR-14827
					Changed from fb.f3 -> fb.f2*/

/*TUIRS*/
data work.R333_1;
	set fac.'333#1 Insurance Risk Score$'n;
	if fire =. then delete;

	/* Parse score range to 2 columns*/
	array parsed_tu(2) tu1-tu2;
	i=1;
	do while(scan(F1,i)ne"");
	parsed_tu(i) = scan(F1,i);
	i+1;
	end;

	drop i;
	rename tu1=TU_low tu2=TU_high;
run;

/*TRC*/
data work.R333_2;
	set fac.'333#2 Insurance Risk Score$'n;
	if fire =. then delete;

	/* Parse score range to 2 columns*/
	array parsed_tr(2) tr1-tr2;
	i=1;
	do while(scan(F1,i)ne"");
	parsed_tr(i) = scan(F1,i);
	i+1;
	end;

	drop i;
	rename tr1=TR_low tr2=TR_high;
run;

/*TUIRS*/
/*change max to 9999, min to -100, no hit = 0, no score = -1  */
proc sql noprint;
	update R333_1
	     set TU_high = 9999
		 where F1 like '%Above';

	select min(TU_low)
 		into :mintu
        from R333_1;

	update R333_1
	     set TU_high = &mintu, TU_low = -100
		 where F1 like '%Below';
		 
	update R333_1
	     set TU_high = -1, TU_low = -1
		 where F1 = 'No Score';

	update R333_1
	     set TU_high =0, TU_low = 0
		 where F1 = 'No Hit';
quit;

/*TRC*/
/*change max to 9999, min to -100, no hit = 0, no score = -1  */
proc sql noprint;
	update R333_2
	     set TR_high = 9999
		 where F1 like '%Above';

	select min(TR_low)
 		into :mintr
        from R333_2;

	update R333_2
	     set TR_high = &mintr, TR_low = -100
		 where F1 like '%Below';
		 
	update R333_2
	     set TR_high = 0, TR_low = 0
		 where F1 = 'No Score';

	update R333_2
	     set TR_high =-1, TR_low = -1
		 where F1 = 'No Hit';
quit;

%macro insscore();
proc sql;
create table out.ins_score_fac as
   select p.casenumber, p.policynumber, p.insscore, 
	      p.policytermyears, p.policyformnumber, p.seqno, p.scoremodel,p.matchcode,
		  coalesce(fa.f3, fb.f2) as ins_tier, input(calculated ins_tier,8.) as ins_tier_n  /*2020/01/21  DG  Changed from fb.f3 -> fb.f2*/
  			, round(coalesce(fa.fire, fb.fire), .001) as fire_ins_score
			, round(coalesce(fa.theft,fb.theft), .001) as theft_ins_score
			, round(coalesce(fa.'Water Non-Weather'n,fb.'Water Non-Weather'n), .001) as waternw_ins_score
			, round(coalesce(fa.'All Other Non-Weather'n,fb.'All Other Non-Weather'n), .001) as othernw_ins_score
			, round(coalesce(fa.lightning,fb.lightning), .001) as lightning_ins_score
			, round(coalesce(fa.'Water Weather'n,fb.'Water Weather'n), .001) as waterw_ins_score
			, round(coalesce(fa.wind,fb.wind), .001) as wind_ins_score
			, %if &Hurricane = 1 %then round(coalesce(fa.Hurricane,fb.Hurricane), .001); %else 0; as hurr_ins_score

			, round(coalesce(fa.'Cov C - Fire'n, fb.'Cov C - Fire'n), .001) as Cov_C_Fire_ins_score
			, round(coalesce(fa.'Cov C - EC'n, fb.'Cov C - EC'n), .001) as Cov_C_EC_ins_score
			, round(coalesce(fa.'Property Damage due to Burglary'n, fb.'Property Damage due to Burglary'n), .001) as PD_Burg_ins_score

			, round(coalesce(fa.'Liability - Bodily injury (Perso'n, fb.'Liability - Bodily injury (Perso'n), .001) as liabipi_ins_score
			, round(coalesce(fa.'Liability - Bodily injury (Medic'n, fb.'Liability - Bodily injury (Medic'n), .001) as liabimp_ins_score
			, round(coalesce(fa.'Liability - Property Damage to O'n, fb.'Liability - Property Damage to O'n), .001) as liapd_ins_score		
			, round(coalesce(fa.'Water Back-up'n,fb.'Water Back-up'n), .001) as waterbu_ins_score

			, round(coalesce(fa.'Limited Theft Coverage'n,fb.'Limited Theft Coverage'n), .001) as Lim_Theft_ins_score

   from  in.drc_info p
		left join r333_1 fa /* These are the TUIRS (APEX) factors */
			on p.ScoreModel = 2 and 
				case when p.matchcode in (0,-1) then p.matchcode = fa.TU_high else p.insscore between fa.TU_low and fa.TU_high end
		left join r333_2 fb /* These are the TRC (Mosaic) factors */
			on p.ScoreModel = 3 and
				case when p.matchcode in (0,-1) then p.matchcode = fb.TR_high else p.insscore between fb.TR_low and fb.TR_high end;

create table ins_tier as  /**also used in rules 353, 354, step 7*/
	      select i.casenumber, i.policynumber, i.insscore, 
	        	 i.policytermyears, i.policyformnumber, i.seqno, i.scoremodel,
				 i.matchcode, i.ins_tier_n, i.ins_tier
	      from out.ins_score_fac i;*/;
   quit;

%if &Process = 0 %then %do;
	title 'RULE 333. INSURANCE RISK SCORE';
	proc print data=out.ins_score_fac;
	where fire_ins_score*theft_ins_score*waternw_ins_score*othernw_ins_score*lightning_ins_score*waterw_ins_score
	*wind_ins_score*hurr_ins_score*Cov_C_Fire_ins_score*Cov_C_EC_ins_score*PD_Burg_ins_score
	*liabipi_ins_score*liabimp_ins_score*liapd_ins_score*waterbu_ins_score*Lim_Theft_ins_score = .;
	run;
	proc gplot data=out.ins_score_fac;
		plot (fire_ins_score theft_ins_score waternw_ins_score othernw_ins_score lightning_ins_score waterw_ins_score
	 		wind_ins_score hurr_ins_score Cov_C_Fire_ins_score Cov_C_EC_ins_score PD_Burg_ins_score
			liabipi_ins_score liabimp_ins_score liapd_ins_score waterbu_ins_score Lim_Theft_ins_score)*ins_tier_n 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.ins_score_fac;
		histogram ins_tier_n;
	run;
%end;
%mend;
%insscore();
 

