data r339_fac;
	set fac.'''339# RATE ADJUSTMENT$'''N (firstobs=2 obs=2);
	fac = input(f1, 10.);
	keep fac;
	call symput('quick_rate', fac);
run;

%put &quick_rate;
%global quk_rte;
%let quk_rte= round(round((p1.adjusted_subtotal_premium
			+ p2.Property_Premium
			+ p3.Total_Liability_Premium
			+ p4.policy_expense_fee),1)*&quick_rate.,1)
			as final_calc_prem;

