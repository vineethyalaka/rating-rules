** RULE 320. PRESENCE OF POOL **;

*Second Home:
	*2017 08 30 JL 1.1 Pool facs numeric in some manuals and char vars in others. Data step added so code will work for both cases;

%macro POP();
	data pool;
		set fac.'320# Presence of Pool$'n;
		if vtype(Theft)='C' then do;
			Theft1=input(Theft,8.3);
			WaterNW=input('Water Non-Weather'n,8.3);
			OtherNW=input('All Other Non-Weather'n,8.3);
			Lightning1=input(lightning,8.3);
			PDBurg=input('Property Damage due to Burglary'n,8.3);
			LiaBIPer=input('Liability - Bodily injury (Perso'n,8.3);
			LiaBIMed=input('Liability - Bodily injury (Medic'n,8.3);
			LimTheft=input('Limited Theft Coverage'n,8.3);
		end;
		else do;
			Theft1=Theft;
			WaterNW='Water Non-Weather'n;
			OtherNW='All Other Non-Weather'n;
			Lightning1=lightning;
			PDBurg='Property Damage due to Burglary'n;
			LiaBIPer='Liability - Bodily injury (Perso'n;
			LiaBIMed='Liability - Bodily injury (Medic'n;
			LimTheft='Limited Theft Coverage'n;
		end;
		keep F1 Theft1 WaterNW OtherNW Lightning1 PDBurg LiaBIPer LiaBIMed LimTheft;
		rename Theft1=Theft;
		rename Lightning1=Lightning;
	run;

	proc sql;
		create table out.pool_fac as
		select p.casenumber, p.policynumber, p.poolpresent
			, f.Theft as theft_pool
			, f.WaterNW as waternw_pool
			, f.OtherNW as othernw_pool
			, f.Lightning as lightning_pool
			, f.PDBurg as PD_Burg_pool
			, Case when p.PolicyFormNumber = 9 then 1 else f.LiaBIPer end as liabipi_pool
			, Case when p.PolicyFormNumber = 9 then 1 else f.LiaBIMed end as liabimp_pool
			, f.LimTheft as Lim_Theft_pool
		from in.policy_info p left join pool f
			on p.poolpresent=substr(f1,1,1);
	quit;

%if &Process = 0 %then %do;
	title 'RULE 320. PRESENCE OF POOL';
	proc freq data=out.pool_fac;
	tables poolpresent
		*theft_pool
		*waternw_pool
		*othernw_pool
		*lightning_pool
		*PD_Burg_pool 
		*liabipi_pool
		*liabimp_pool
		*Lim_Theft_pool  / list missing;
	run;
%end;
%mend;
%POP();
