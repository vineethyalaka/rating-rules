** Rule 540 Equipment Breakdown **;

/*ITR 14094/13846 MD/IN Replacement Cost in Base and Equipment Breakdown Addition */

/* 08-16-2019 Vamsi Ramineedi - ITR 14558 - Changed the logic on left join from '>' to '>=' */

	data equip_break_fac;
	set fac.'540# Equipment Breakdown$'n;
	if Rate = . then delete;
	f6 = lag(f3);/*find the upper limit of cova in previous row*/
	if f3 = . and Rate ne . then do;
		'Coverage A'n = f6 + 1;		/*lower limit for the last row*/
		f3 = 9999999; end;	/*upper limit for the last row*/
	rename 'Coverage A'n = CovA_L f3 = CovA_H;
	run;

	proc sql;
		create table out.equipment_breakdown_fac as
		select c.casenumber, c.policynumber, c.coveragea, c.HO_0456_FLAG, c.HO_04_56_pct, c.CoverageA_adj, p.Equipment_Breakdown_Flag
			  , case when p.Equipment_Breakdown_Flag = 1 then f.Rate else 0 end as Equip_Break_Rate
		from Coveragea c
		left join in.policy_info p on c.casenumber=p.casenumber
		left join equip_break_fac f on c.CoverageA_adj <= f.CovA_H and c.CoverageA_adj >= f.CovA_L
		;
	quit;
