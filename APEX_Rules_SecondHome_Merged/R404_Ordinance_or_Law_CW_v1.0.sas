** RULE 404. ORDINANCE OR LAW **;
%macro or();
	data r404;
	set fac.'404# Ordinance or Law$'n;
	if 'HO3'n = . then delete;
	run;
	proc sql;
	    create table out.r404_Ordinance_or_Law as
		select p.CaseNumber, p.PolicyNumber, p.PolicyFormNumber, p.ORD_LAW_FLAG
			, case when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 3 then f.'HO3'n 
			when p.ORD_LAW_FLAG = '1' and p.PolicyFormNumber = 9 then f.'HF9-P1'n
			else 1 end as r404_Ordinance_or_Law_fac
		from in.Endorsement_Info p, r404 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 404. ORDINANCE OR LAW';
	proc freq data=out.r404_Ordinance_or_Law;
	tables ORD_LAW_FLAG*r404_Ordinance_or_Law_fac*PolicyFormNumber / list missing;
	run;
%end;
%mend;
%or();
