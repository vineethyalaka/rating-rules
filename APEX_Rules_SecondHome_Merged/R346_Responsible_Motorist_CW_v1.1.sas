** RULE 346. RESPONSIBLE MOTORIST **;

**=====================================================================================**
History: 2016 08 29 SH  Initial draft
		 2017 04 05 BF	Adding Rounding to 3 digits

**=====================================================================================**;
%macro responsible_motorist();

proc sql;
create table work.Autoclaim as
select 
a.policynumber,
a.casenumber,
a.policytermyears,
/*b.requestID,*/
b.losstype,
b.lossdate,
b.amount,
c.*
from in.policy_info as a
left join in.loss_info_moto as b on a.casenumber=b.casenumber
left join mapping.autoclaim_losscode as c on c.autolosscode=b.losstype
;
quit;

/* important: summarize # of perils */
proc sql;
create table work.AutoClaim_Group as
select 
policynumber, 
casenumber, 
policytermyears, 
peril, 
count(perilnumber) as peril_count 
from work.autoclaim
group by policynumber, casenumber, policytermyears, peril
order by policynumber, casenumber;
quit;

proc sql;
create table work.AutoClaim_Group_Rotated as
select 
policynumber, 
casenumber, 
policytermyears, 
sum(case when peril = 'P1' then peril_count else 0 end) as P1,
sum(case when peril = 'P2' then peril_count else 0 end) as P2,
sum(case when peril = 'P3' then peril_count else 0 end) as P3,
sum(case when peril = 'P4' then peril_count else 0 end) as P4,
sum(case when peril = 'P5' then peril_count else 0 end) as P5,
sum(case when peril = 'P6' then peril_count else 0 end) as P6,
sum(case when peril = 'P8' then peril_count else 0 end) as P8
from work.AutoClaim_Group
group by policynumber, casenumber, policytermyears
order by policynumber, casenumber;
quit;


*Rule 346 Responsible Motorist;
data r346fac;
set fac.'346# Responsible Motorist$'n;
if Fire=1 then F1=5;
else if Fire=. then delete;
drop F9 F10;
run;

proc sql;
create table out.res_motor_fac as
select 
a.*,
/* if it's marked, corresponding factor should be 1*/
case when P1>0 then 1 else round(b.Fire,.001) end as fire_resmotor,
case when P2>0 then 1 else round(b.Theft,.001) end as theft_resmotor,
case when P3>0 then 1 else round(b.'Water Non-Weather'n,.001) end as waternw_resmotor,
case when P4>0 then 1 else round(b.'All Other Non-Weather'n,.001) end as othernw_resmotor,
case when P5>0 then 1 else round(b.Lightning,.001) end as lightning_resmotor,
case when P6>0 then 1 else round(b.'Water Weather'n,.001) end as waterw_resmotor,
case when P8>0 then 1 else round(b.Hail,.001) end as hail_resmotor
from Autoclaim_Group_Rotated as a
left join r346fac as b
on (case when a.policytermyears>5 then 5 else a.policytermyears end)=b.F1
;
quit;

%mend;
%responsible_motorist;