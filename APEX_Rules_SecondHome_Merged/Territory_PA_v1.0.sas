/*Wanwan : Territory calc specific for CT without location ID, with distance to shore*/
proc sql;
	create table out.territory as
	select p.policynumber
      , p.CaseNumber
      , p.policytermyears
      , p.policyeffectivedate
      , p.countyiD
/*      , input(p.Census2010,12.) as census_block_group_ID*/
	  , p.Census2010 as census_block_group_ID
      , p.LocationID
	from in.policy_info as p;
quit;
