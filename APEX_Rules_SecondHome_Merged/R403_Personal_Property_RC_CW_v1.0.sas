** RULE 403. PERSONAL PROPERTY REPLACEMENT COST **;
%macro RC();
	data r403;
	set fac.'403# Personal Prop Repl Cost$'n;
	if 'HO3'n = . then delete;
	run;
	proc sql;
		create table out.r403_CovC_Repl_Cost as
		select p.CaseNumber, p.PolicyNumber, p.PolicyFormNumber, p.HO_0490_FLAG
			 , case when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ and p.PolicyFormNumber = 3 then f.'HO3'n
			when p.HO_0490_FLAG = '1' /*and a.AdvInd = 'N'*/ and p.PolicyFormNumber = 9 then f.'HF9-P10'n
			else 1 end as r403_HO_04_90_fac
		from in.Endorsement_Info p, r403 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 403. PERSONAL PROPERTY REPLACEMENT COST';
	proc freq data=out.r403_CovC_Repl_Cost;
	tables HO_0490_FLAG*r403_HO_04_90_fac*PolicyFormNumber / list missing;
	run;
%end;
%mend;
%RC();
