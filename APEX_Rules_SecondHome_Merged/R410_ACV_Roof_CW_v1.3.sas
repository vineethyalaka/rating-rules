** RULE 410. ACV ROOF  **;
%macro ACV();
	data acv_roof_fac;
	set fac.'410# ACV Roof$'n;
	if wind = . then delete;
	if f1=. then f1=31;
	run;
/*		proc sql;*/
/*		create table acv_end_info as */
/*		select e.casenumber, e.policynumber, p.policytermyears, p.ratingversion, */
/*			   p.ADDRESS_LINE3_STATE, p.group_ind_c,e.HO_0493_FLAG, r.roof_age,*/
/*		case when (p.group_ind_c ne 'apex' or p.ratingversion < v.version) and r.roof_age >= v.acv_year then '1' */
/*			 else e.HO_0493_FLAG end as HO_0493_FLAG_r*/
/*		from in.endorsement_info as e*/
/*	 		left join in.policy_info as p on e.casenumber = p.casenumber*/
/*			left join mapping.acvmapping as v on p.ADDRESS_LINE3_STATE = v.state*/
/*			left join roof_age r on e.casenumber = r.casenumber*/
/*	;quit;*/
	proc sql;
		create table out.ACV_Roof_fac as
		select p1.casenumber, p1.policynumber, p1.roof_age, p2.propertyrooftypecode, e.HO_0493_FLAG
			, case when e.HO_0493_FLAG = '1' then f.wind      else 1 end as wind_ACV_roof 
			, case when e.HO_0493_FLAG = '1' then f.hail      else 1 end as hail_ACV_roof
			%if &Hurricane = 1 %then %do; 
			, case when e.HO_0493_FLAG = '1' then f.hurricane else 1 end as hurr_ACV_roof
			%end;
			%else %do;
			, 1 as hurr_ACV_roof
			%end;
		from roof_age p1 
			left join roof_type			  p2 on p1.casenumber=p2.casenumber
			left join acv_roof_fac		  f on p1.roof_age=f.f1 and lowcase(p2.propertyrooftypecode)=lowcase(f.f2)
			left join in.endorsement_info e on p1.casenumber=e.casenumber;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 410. ACV ROOF';
	proc print data=out.ACV_Roof_fac;
	where wind_ACV_roof*hail_ACV_roof/**hurr_ACV_roof*/= .;
	run;
	proc sort data=out.ACV_Roof_fac; by propertyrooftypecode; run;
	proc gplot data=out.ACV_Roof_fac;
		by propertyrooftypecode;
		plot (wind_ACV_roof hail_ACV_roof /*hurr_ACV_roof*/)*roof_age 
			/ overlay legend vaxis=axis1;
		where HO_0493_FLAG = '1';
	run;
	quit; 
	proc freq data=out.ACV_Roof_fac;
	tables wind_ACV_roof*hail_ACV_roof/**hurr_ACV_roof*/ / list missing;
	where HO_0493_FLAG = '0';
	run;
%end;
%mend;
%ACV();
