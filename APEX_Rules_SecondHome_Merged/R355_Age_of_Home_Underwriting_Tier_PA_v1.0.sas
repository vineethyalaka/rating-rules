** RULE 355. AGE OF HOME BY INSURANCE SCORE **;
%macro agescore();
data exp_home_age_x_uw_tier_fac (keep=home_age uw_tier factor);
set fac.'355# Age of Home by UW Tier$'n (rename=('Expense Fee'n=f2 'Expense Fee1'n=f23 'Expense Fee2'n=f44 'Expense Fee3'n=f65 'Expense Fee4'n=f86));
if f3=. or f1 = 'Home Age' or f1 = 'Age of Home' then delete;
home_age=input(f1,3.);
array f (105) f2 - f106;
do i = 1 to 105;
	uw_tier = strip(put(i,3.)); factor = f(i);
	output;
end;
run;
proc sql;
	create table out.exp_home_age_x_uw_tier_fac as
	select p1.casenumber, p1.policynumber, p1.PolicyIssueDate, p1.PolicyEffectiveDate, p1.PropertyYearBuilt, p1.home_age
		, p2.matchcode, p2.ScoreModel, p2.insscore, p2.ins_tier, p2.drc_class, p2.uw_tier, p2.uw_tier_n
			, f.factor as exp_home_age_x_uw_tier_fac
	from home_age p1 
		inner join uw_tier p2 on p1.casenumber = p2.casenumber
		left join exp_home_age_x_uw_tier_fac f on f.home_age = p1.home_age and f.uw_tier = p2.uw_tier;
quit;

%if &Process = 0 %then %do;
title 'RULE 355. AGE OF HOME BY INSURANCE SCORE';
proc print data=out.exp_home_age_x_ins_score_fac;
where exp_home_age_x_ins_score_fac = .;
run;
proc sort data=out.exp_home_age_x_ins_score_fac; by ins_tier; run;
proc gplot data=out.exp_home_age_x_ins_score_fac; by ins_tier;
	plot exp_home_age_x_ins_score_fac*home_age 
		/ overlay legend vaxis=axis1;
run;
quit; 
%end;
%mend;
%agescore();