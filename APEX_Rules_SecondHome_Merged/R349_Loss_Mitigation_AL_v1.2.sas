
data loss_mitigation_fac_0;
set fac.'349# Loss Mitigation Discounts$'n;
if wind = . then delete;
drop  f2 f9-f16;
run;

data loss_mitigation_fac;
 set loss_mitigation_fac_0;
retain Category	Zone 'Roof Age'n;
if f1 ne ' ' then Category = f1;
if f3 ne ' ' then Zone = f3;
if f4 ne ' ' then 'Roof Age'n = f4;
rename f5='Retrofit Level'n;
drop f1 f3 f4;
run;



proc import datafile='\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\AL\vnew_11462\County Zone and FIPS for IT.xlsx'
out=County_zone replace 
DBMS = xlsx;
sheet= Sheet2;
getnames=yes;
run;

data County_zone_IT;
 set County_zone;
retain zone;
if area ne '' then Zone = area;
rename 'FIPS ID'n= FIPSID;
drop Area;
run;


proc sql;
create table loss_mitigation_info as
select p1.casenumber, p1.policynumber, p1.construction_completed
, p2.home_age, p3.roof_age
from in.production_info p1
inner join work.home_age as p2
	on p1.casenumber =p2.casenumber
inner join work.roof_age as p3
	on p1.casenumber = p3.casenumber;
quit;

data loss_mitigation;
set work.loss_mitigation_info;
length mitigationcode $50.;
	if Construction_Completed = 100 then mitigationcode = 'IRC';
	else if Construction_Completed = 200 then mitigationcode = 'Gold';
	else if Construction_Completed = 300 then mitigationcode = 'Silver';
	else if Construction_Completed = 400 then mitigationcode = 'Bronze';
	else if Construction_Completed = 500 then mitigationcode = 'None of the above';
	else if Construction_Completed = 600 then mitigationcode = 'FFSLS';
	else mitigationcode = 'None';
run;
/**/
/**Get county and city name;*/
/*LIBNAME sqlsrvqa oledb init_string = */
/*"Provider=SQLOLEDB.1; Integrated Security=SSPI; Persist Security Info=False;*/
/*Initial Catalog=ISO_PROD; read_lock_type=nolock; update_lock_type=nolock; READ_ISOLATION_LEVEL = RU; Data Source=CAMBOSQASQL21\SQL01" */
/*schema=dbo;*/
/**/
/*%let server = sqlsrvqa;*/
/**/
/*proc sql noprint;*/
/*select "'"||compress(policynumber)||"'" into : policynumber separated by ',' */
/*from in.policy_info;*/
/*quit;*/
/**/
/**/
/*proc sql;*/
/*create table county_city_name_BP as */
/*select */
/* p.policy_number as Policynumber */
/*,p.rating_case_number as Casenumber*/
/*,p.form_code*/
/*,ADDRESS_LINE_1            */
/*,ADDRESS_LINE_2*/
/*,ADDRESS_LINE_3_CITY as PropertyAddressCity*/
/*,ADDRESS_LINE_3_STATE*/
/*,ADDRESS_LINE_COUNTY as County*/
/*from &server..dh0200bp as p*/
/*inner join &server..dh0200bX as x*/
/*on p.PRIME_KEY = x.PRIME_KEY_X  */
/*inner join &server..PolicyGeoSpatialData as G  */
/*on P.GEOCODE_POINTER = G.GeoCodePointer*/
/*where p.policy_number in (&policynumber)*/
/*;*/
/*quit;*/
/**/
/**/
/*proc sql;*/
/*create table county_city_name_CP as */
/*select */
/* p.policy_number as Policynumber */
/*,p.rating_case_number as Casenumber*/
/*,p.form_code*/
/*,ADDRESS_LINE_1            */
/*,ADDRESS_LINE_2*/
/*,ADDRESS_LINE_3_CITY as PropertyAddressCity*/
/*,ADDRESS_LINE_3_STATE*/
/*,ADDRESS_LINE_COUNTY as County*/
/*from &server..dh0200cp as p*/
/*inner join &server..dh0200cX as x*/
/*on p.PRIME_KEY = x.PRIME_KEY_X  */
/*inner join &server..PolicyGeoSpatialData as G  */
/*on P.GEOCODE_POINTER = G.GeoCodePointer*/
/*where p.policy_number in (&policynumber)*/
/*;*/
/*quit;*/
/**/
/*data county_city_name; */
/*set county_city_name_BP county_city_name_CP; */
/*run;*/

proc sql;
create table county_FIPSID_id as
select p.policynumber, p.casenumber,t.countyid as FIPSID
from in.policy_info p
left join out.TERRITORY t
on p.casenumber = t.casenumber
;
quit;


*Roof type metal flag;
data roof_type;
set in.policy_info;
if propertyrooftypecode='Wood shake' then propertyrooftypecode='Wood shingles/shakes';
if propertyrooftypecode='Wood shingles' then propertyrooftypecode='Wood shingles/shakes';
if lowcase(propertyrooftypecode) in (' ','flat','gambrel','hip','mansard','plexiglass','truss-joist') then propertyrooftypecode='All Other';
keep PolicyNumber CaseNumber PropertyRoofTypeCode;
run;


*Metal flag only "Tin/Membrane" is metal rooftype;
data metal_roof;
 set roof_type;
If propertyrooftypecode = '' Then 
do; 
   	 Put "ERROR: roof type is invalid.";
end;
else 
do;
	 if upcase(compress(propertyrooftypecode,'/ '))= upcase(compress('Tin/Membrane','/ '))
	 then MetalRoofFlag= 'y';
	 else MetalRoofFlag= 'n';
end;
run;



/*%let a = %sysfunc(%upcase(compress('Wood Shingles/Shakes','/ ')));*/

/*%put &a.;*/



*Roof age, metal, zone, category,mitigationcode;
proc sql;
create table mitigation_0 as
select *
from loss_mitigation_info l
inner join metal_roof m
on l.casenumber = m.casenumber
inner join loss_mitigation mt
on l.casenumber = mt.casenumber
inner join county_FIPSID_id c
on l.casenumber = c.casenumber
inner join County_zone_IT i
on compress(c.FIPSID) = compress(i.FIPSID)
;
quit;


*Set Category;
/*data mitigation_1;*/
/* set mitigation_0;*/
/*if (MetalRoofFlag= 'y' and roof_age le 5)or (MetalRoofFlag= 'n' and roof_age le 10)then*/
/*do;*/
/*	 if zone = 'Northern' then Category = 1;*/
/*	 if zone = 'Central' then Category = 2;*/
/*	 if zone = 'Coastal' then Category = 3;*/
/*end;*/
/*if (MetalRoofFlag= 'y' and roof_age > 5)or (MetalRoofFlag= 'n' and roof_age > 10)then*/
/*do;*/
/*	 if zone = 'Northern' then Category = 4;*/
/*	 if zone = 'Central' then Category = 5;*/
/*	 if zone = 'Coastal' then Category = 6;*/
/*end;*/
/*run;*/

data mitigation_1;
 set mitigation_0;
if (MetalRoofFlag= 'y' and roof_age le 10)or (MetalRoofFlag= 'n' and roof_age le 5)then
do;
	 if zone = 'Northern' then Category = 1;
	 if zone = 'Central' then Category = 2;
	 if zone = 'Coastal' then Category = 3;
end;
if (MetalRoofFlag= 'y' and roof_age > 10)or (MetalRoofFlag= 'n' and roof_age > 5)then
do;
	 if zone = 'Northern' then Category = 4;
	 if zone = 'Central' then Category = 5;
	 if zone = 'Coastal' then Category = 6;
end;
run;


proc sql;
	create table out.mitigation_fac as
	select p.*
	, case when mitigationcode = 'None of the above' or mitigationcode = 'None' then 1 else f1.wind end as wind_loss_mitigation
	, case when mitigationcode = 'None of the above' or mitigationcode = 'None' then 1 else f1.hail end as hail_loss_mitigation
	, case when mitigationcode = 'None of the above' or mitigationcode = 'None' then 1 else f1.hurricane end as hurr_loss_mitigation
	from mitigation_1 p
	left join loss_mitigation_fac f1 on p.Category = input(f1.Category,1.)
	and p.mitigationcode = f1.'Retrofit Level'n
	;
quit;

