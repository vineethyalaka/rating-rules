** RULE 344. PARTNER DISCOUNTS **;

*Second Home:
	*            MM Initial draft
	* 2017 08 30 JL Added AmFam and removed expired partnerships;
	* 2017 09 05 TA Added back expired partnerships for OH;

%macro aff();
	data affinity; 
	set in.policy_info (keep=casenumber policynumber policyaccountnumber autopolicynumber /*Partner_Customer*/);
	if policyaccountnumber in (1020:1026,1051,1130:1132,4000:4499,11510:11520,13220:13242,13800:13830,14000:14500)
/*							,1550:1553,1610,2000:2199,2300:2699,3300:3899,7010,7030,8000:8999,11000*/ /*Expired Partnerships*/
		then partner = 1;
	else if (policyaccountnumber in (1550:1553,1610,2000:2199,2300:2699,3300:3899,7010,7030,8000:8999,11000) and &state=OH) /*Expired Partnerships*/
		then partner = 1;
	else if policyaccountnumber in (50000:50099,     /*Wells Fargo*/
									51000:51099		/*AmFam group*/)
		then partner = 3;
	else partner = 0;
	if autopolicynumber = '' then auto = 0;
	else auto = 1;
	run;

%if %sysfunc(exist(fac.'344# Partner Discounts$'n)) %then %do;

	data affinity_fac;
	set fac.'344# Partner Discounts$'n;
	if f1 = '' then delete;
	Discount_Indicator = input(substr(f1,1,1),1.);
  	if missing(f2) then f2='Partner Discount'n;
	rename f2 = Affinity_Discount;
	run;

%end;
%else %do;
	data affinity_fac;
	set fac.'344# Affinity Marketing$'n;
	if f1 = '' then delete;
	Discount_Indicator = input(substr(f1,1,1),1.);
	if missing(f2) then f2='Partner Discount'n;
	rename f2 = Affinity_Discount;
	run;
%end;


	proc sql;
		create table affinity_type as
		select p.*,  
		case when p.auto = 1 and p.partner = 1 then 1 
/*		when p.partner = 2 then 2 */
		when p.partner = 3 and p.auto = 1 then 3
		when p.partner = 3 and p.auto = 0 /*and p.Partner_Customer = '1'*/ then 2
		else 0 end as Discount_Indicator
		from affinity p;
	quit;

	proc sql;
		create table out.affinity_fac as
		select a.*,
		case when b.Affinity_Discount = . then 0 else b.Affinity_Discount end as Affinity
		from affinity_type a
		left join affinity_fac b on a.Discount_Indicator = b.Discount_Indicator
	;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 344. PARTNER DISCOUNTS';
	proc freq data=out.affinity_fac;
	tables partner*auto*affinity / list missing;
	run;
%end;
%mend;
%aff();
