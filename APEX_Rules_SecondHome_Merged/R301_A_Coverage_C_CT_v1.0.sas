** RULE 301.A COVERAGE C **;
data coverage_c;
	*also used in rules 505, 515;
	set in.policy_info (keep=casenumber policynumber CoverageA CoverageC PropertyUnitsInBuilding);
	CovC_pctA = CoverageC/CoverageA;

	if PropertyUnitsInBuilding in (1,2) then
		Base_CovC = 0.5*CoverageA;
	else if PropertyUnitsInBuilding = 3 then
		Base_CovC = 0.3*CoverageA;
	else if PropertyUnitsInBuilding = 4 then
		Base_CovC = 0.25*CoverageA;

	if CoverageC > Base_CovC then
		Incr_CovC = CoverageC - Base_CovC;
	else Incr_CovC = 0;

	if CoverageC < Base_CovC then
		Decr_CovC = Base_CovC - CoverageC;
	else Decr_CovC = 0;
	format CovC_pctA percent8.1;
run;

data coveragec_aoi;
	set fac.'301#A Coverage C$'n;

	if f1 = . and f2 = . then
		delete;
	aoi_c_L = lag(f1);
	theft_fac_L = lag(theft);
	rename f1 = aoi_c_H theft = theft_fac_H;

	if aoi_c_L = . then
		delete;
run;

proc sql;
	create table out.coverage_c_fac as
		select p.*, f.aoi_c_L, f.aoi_c_H, f.theft_fac_L, f.theft_fac_H
			, 
		case 
			when p.CoverageC = 0 then .
			when f.aoi_c_H = . then round(f.theft_fac_H*(p.CoverageC - f.aoi_c_L)/100000 + f.theft_fac_L,.0001)
			else round((f.theft_fac_H - f.theft_fac_L)*(p.CoverageC - f.aoi_c_L)/(f.aoi_c_H - f.aoi_c_L) + f.theft_fac_L,.0001)
		end 
	as theft_covc
		from coverage_c p 
			left join coveragec_aoi f on (p.CoverageC <= f.aoi_c_H or f.aoi_c_H = .) and p.CoverageC > f.aoi_c_L
	;
quit;

proc sql;
	create table out.coverage_c_base_fac as
		select p.*, f.aoi_c_L, f.aoi_c_H, f.theft_fac_L, f.theft_fac_H
			, 
		case 
			when p.Base_CovC = 0 then .
			when f.aoi_c_H = . then round(f.theft_fac_H*(p.Base_CovC - f.aoi_c_L)/100000 + f.theft_fac_L,.0001)
			else round((f.theft_fac_H - f.theft_fac_L)*(p.Base_CovC - f.aoi_c_L)/(f.aoi_c_H - f.aoi_c_L) + f.theft_fac_L,.0001)
		end 
	as theft_covcbase
		from coverage_c p 
			left join coveragec_aoi f on (p.Base_CovC <= f.aoi_c_H or f.aoi_c_H = .) and p.Base_CovC > f.aoi_c_L
				order by theft_covcbase
	;
quit;