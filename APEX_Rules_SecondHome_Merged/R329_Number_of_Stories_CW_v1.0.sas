** RULE 329. NUMBER OF STORIES **;
%macro NOS();
	
	data numstories;
	set in.policy_info;
	if propertystories < 2 then numstories='Less than 2'; else numstories='2 or More';
	run;

	data numstoriesfac;
	set fac.'329# Number of Stories$'n;
	run;


	proc sql;
		create table out.num_stories_fac as
		select p.casenumber, p.policynumber, p.propertystories
			, f.'Water Non-Weather'n as waternw_num_stories 
		from numstories p left join numstoriesfac f
			on p.numstories=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 329. NUMBER OF STORIES';
	proc freq data=out.num_stories_fac;
	tables PropertyStories
		*waternw_num_stories / list missing;
	run;
%end;

%mend;
%NOS();