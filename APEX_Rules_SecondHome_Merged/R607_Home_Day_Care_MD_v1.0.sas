** RULE 607. Home Day Care **;
%macro hdc();
	data r607covErate;
	set fac.'607# Home Day Care Cov E$'n;
	if strip(f1)="" then delete;
	Limit = input(f1,comma8.);
	run;

	data r607covFrate;
	set fac.'607# Home Day Care Cov F$'n;
	if strip(f1)="" then delete;
	run; 

	proc sql;
	    create table out.lia_DayCare_Rate as
		select p.CaseNumber, p.PolicyNumber, p.CoverageE, p.CoverageF, e.DayCare_FLAG
			 , case when (e.DayCare_FLAG = '1' and p.CoverageE = 300000) then input(hdce.f2, 4.) else 0 end as Home_DayCare_E_Rate
			 , case when (e.DayCare_FLAG = '1' and p.CoverageF ^= 1000) then input(hdcf.f2, 4.) else 0 end as Home_DayCare_F_Rate
		from in.Policy_Info as p
		inner join in.Endorsement_Info as e
		on e.CaseNumber = p.CaseNumber
		left join r607covErate as hdce
		on p.CoverageE = hdce.Limit
		left join r607covFrate as hdcf
		on p.CoverageF = hdcf.f1
		;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 607. HOME DAY CARE';
	proc print data=out.lia_DayCare_Rate;
	where Home_DayCare_E_Rate = .;
	run;
%end;
%mend;
%hdc();
