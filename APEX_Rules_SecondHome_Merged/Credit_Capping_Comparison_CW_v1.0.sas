proc sql;
create table out.credit_cap_comparison as 
select p1.casenumber,
       p1.policynumber,
	   i.IT_CurrentISC,
	   i.SAS_CurrentISC,
	   i.IT_UsedISC,
	   i.ins_tier_n as SAS_UsedISC,
		/*check if CurrentISC and UsedISC match between SAS and IT*/
	   case when (i.IT_CurrentISC = . or i.IT_CurrentISC = i.SAS_CurrentISC) and i.IT_UsedISC = i.ins_tier_n then 1 else 0 end as Credit_Cap_Match
from in.policy_info p1
inner join out.ins_score_fac i
		on p1.casenumber = i.casenumber
order by Credit_Cap_Match asc
;
quit;