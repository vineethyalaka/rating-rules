
%macro step9();

	proc sql;
		create table out.step9_final as
		select p1.*
			/*get the capped premium*/
			, round(round(p1.uncapped_calc_premium) * p1.capping_factor ,1) as capped_premium
	
			/*get the final premium by compare to minimum and add the endorsement strategy*/
			, case when calculated capped_premium <= p4.min_premium then p4.min_premium
			  	   else calculated capped_premium 
			  end as max_capped_premium
			, p2.Equip_Break_Rate
			, p3.Service_Line_Rate
			, (calculated max_capped_premium + p2.Equip_Break_Rate + p3.Service_Line_Rate) as final_premium

		from out.step9_capping_factor p1
			inner join out.equipment_breakdown_fac  p2 on p1.casenumber = p2.casenumber
			inner join out.Service_Line_Rate        p3 on p1.casenumber = p3.casenumber
			, out.min_premium p4;		

	quit;


%mend;

%step9();
