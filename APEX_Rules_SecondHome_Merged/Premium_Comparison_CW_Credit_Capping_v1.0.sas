/*1/25/2019 	WZ	Create CW premium_comparison rule with Credit Capping check ITR 13735*/

proc sql;
create table out.premium_comparison as 
select p1.casenumber,
       p1.policynumber,
       p1.premiuminforce as IT_Premium,
       s.final_premium as SAS_Premium,
	   c.cap,
	   (p1.premiuminforce-s.final_premium - c.cap) as IT_SAS_Premium_diff
from in.policy_info p1
inner join out.STEP8_FINAL_PREMIUM s
        on p1.casenumber = s.casenumber
inner join	in.capping_info c
		on s.casenumber = c.casenumber
order by abs(p1.premiuminforce-s.final_premium) desc
;
quit;

proc sql;
create table out.credit_cap_comparison as 
select p1.casenumber,
       p1.policynumber,
	   i.IT_CurrentISC,
	   i.SAS_CurrentISC,
	   i.IT_UsedISC,
	   i.ins_tier_n as SAS_UsedISC,
		/*check if CurrentISC and UsedISC match between SAS and IT*/
	   case when (i.IT_CurrentISC = . or i.IT_CurrentISC = i.SAS_CurrentISC) and i.IT_UsedISC = i.ins_tier_n then 1 else 0 end as Credit_Cap_Match
from in.policy_info p1
inner join out.ins_score_fac i
		on p1.casenumber = i.casenumber
order by Credit_Cap_Match asc
;
quit;