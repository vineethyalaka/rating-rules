** RULE 503. BUSINESS PROPERTY � INCREASED LIMITS **;
%macro BP();
	data r503;
	set fac.'503# Business Prop Inc Lim$'n;
	if strip(f1) = '' then delete;
	rename 'Coverage Limit Increment (i#e# r'n = Increment;
	run;
	proc sql;
		create table out.Business_Prop_rate as
		select p.CaseNumber, p.PolicyNumber, p.PolicyFormNumber, p.HO_0412_flag, p.HO_0412_Total_Limit
			, case when p.HO_0412_flag="1" and p.PolicyFormNumber = 3 then round(max((p.HO_0412_Total_Limit-2500)*f.Rate/Increment,0),1.0) else 0 end as Business_Prop_rate
		from in.Endorsement_Info p, r503 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 503. BUSINESS PROPERTY � INCREASED LIMITS';
	proc freq data=out.Business_Prop_rate;
	tables HO_0412_flag*HO_0412_Total_Limit*Business_Prop_rate / list missing;
	run;
%end;
%mend;
%BP();
