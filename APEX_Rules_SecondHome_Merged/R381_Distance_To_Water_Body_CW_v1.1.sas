/*

1. ITR 14678 CT Mosaic VR Initial file
2. ITR 15050 CO Mosaic VR Changed r381_policy_info_manipulation table to map to (0-1500) row
						  even when NearestWaterBody = -999999, in order to get a factor of 1.
*/

data r381_sheet_manipulation;
	retain min_dist max_dist; 
	set fac.'381# Distance to Water Body$'n;
	where 'Water Weather'n is not missing;
	drop f1 f2 f3 f5;
	min_dist = input(f1, 10.);
	length max_dist 8;
	if f3 = "up" then max_dist = 1000000000;
	else max_dist = input(f3, 10.);
run;

proc sql;
	select min_dist 
		into :neutral_fac_dist 
	from r381_sheet_manipulation 
	where 'Water Weather'n = 1;

	select max(min_dist) 
		into :last_row_dist 
	from r381_sheet_manipulation;

quit;

data r381_policy_info_manipulation;
	set in.policy_info;
	length NearestWaterBody_Updated 8;
	if NearestWaterBody in (., -999999) then NearestWaterBody_Updated = &neutral_fac_dist; *this gives factor of 1;
	else NearestWaterBody_Updated = NearestWaterBody;
	NearestWaterBody_Updated = round(NearestWaterBody_Updated, 1);
run;

proc sql;
	create table out.r381_fac as
		select 
		p.*,
		s.'Water Weather'n as waterw_nearest_water_body
		from r381_policy_info_manipulation p
		left join r381_sheet_manipulation s
			on p.NearestWaterBody_Updated >= s.min_dist and p.NearestWaterBody_Updated <= s.max_dist;
quit;




