
/*1/25/2019 	WZ	Create CW premium_comparison rule*/
proc sql;
create table out.premium_comparison as 
select p1.casenumber,
       p1.policynumber,
       p1.premiuminforce as IT_Premium,
       s.final_premium as SAS_Premium,
	   c.cap,
	   (p1.premiuminforce-s.final_premium - c.cap) as IT_SAS_Premium_diff
from in.policy_info p1
inner join out.STEP8_FINAL_PREMIUM s
        on p1.casenumber = s.casenumber
inner join	in.capping_info c
		on s.casenumber = c.casenumber
order by abs(p1.premiuminforce-s.final_premium - c.cap) desc
;
quit;
