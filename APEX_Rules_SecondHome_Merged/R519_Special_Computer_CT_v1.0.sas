** RULE 519. SPECIAL COMPUTER COVERAGE **;
				data r519;
				set fac.'519# Special Computer Cov.$'n;
				if strip(f1) = '' then delete;
				run;
				proc sql;
					create table out.Computer_Rate as
					select p.CaseNumber, p.PolicyNumber, p.HO_0414_FLAG
						, case when p.HO_0414_FLAG="1" then round(f.charge,1.0) else 0 end as Computer_Rate
					from in.Endorsement_Info p, r519 f;
				quit;