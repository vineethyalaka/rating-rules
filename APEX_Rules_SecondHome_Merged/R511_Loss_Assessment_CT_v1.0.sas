** RULE 511. LOSS ASSESSMENT COVERAGE **;
				data r511;
				set fac.'511# Loss Assessment$'n;
				if 'Amount of Increase'n = . and Charge = . then delete;
				retain rate amount;
				if 'Amount of Increase'n ^= . then do;
				     rate=charge;
				     amount='Amount of Increase'n;
				     lower=amount;
				     upper=amount;
				     end;
				else do;
				     lower=amount+1;
				     upper=999999;
				     end;
				run;
				proc sql;
				     create table out.Loss_Assess_rate as
				     select p.casenumber, p.policynumber, p.HO_0435_INCREASED_AMOUNT, f.*
				           , case when p.HO_0435_INCREASED_AMOUNT = 0 then 0
				                when f.'Amount of Increase'n = . then round(max(f.rate + (p.HO_0435_INCREASED_AMOUNT - f.amount)*Charge/F2,0),1.0)
				                else round(max(Charge,0),1.0) end
				             as Loss_Assess_rate
				     from in.Endorsement_Info p
				           left join r511 f 
				           on p.HO_0435_INCREASED_AMOUNT >= f.lower 
				           and p.HO_0435_INCREASED_AMOUNT <= f.upper;
				quit;