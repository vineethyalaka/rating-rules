** LIABILITY PREMIUM **;

**=====================================================================================**
History: 2015 03 18 JT  1.0 - Initial draft
		 2015 06 25 SY  1.1 - Added endorsement premium by peril
		 2015 07 07 SY  1.2 - Include all liability endorsements in one code
		 2015 07 28 JT	1.3 - Removed reference to "out.out.lia_DangerousDog_fac" and "calculated" in 617 portion of code. Changed rounding rules in 613 portion of code to match IT rounding.	
		 2015 10 06 JT	1.4 - Changed reference in "exist" function of rule 610 from "out.lia_ResEmployee_fac" to "out.lia_personalinjury_fac"
		 2017 03 15 NG  1.0 - Added in rating factors for Second Home Program, and added the Domestic Animal and Pool Liability Rules
		 2017 08 29 JL  1.1 - Updated Domestic Animal and Pool Liability Rule Numbers
**=====================================================================================**;

%macro liab();

	proc sql;
		create table step6_Liability_Premium_base as 
		select *
			, round(liabipi_base_rate
			* liabipi_grid
			* liabipi_census
			* liabipi_cove
/*			* liabipi_drc*/
			* liabipi_ins_score
			* liabipi_num_dogs
			* liabipi_num_units
			* liabipi_pool
			* liabipi_naogclaim
			* Liab_BI_sec_res,.0001)
			as liabipi_base_premium

			, round(liabimp_base_rate
			* liabimp_grid
			* liabimp_census
			* liabimp_covf
/*			* liabimp_drc*/
			* liabimp_ins_score
			* liabimp_num_dogs
			* liabimp_num_units
			* liabimp_pool
			* liabimp_naogclaim
			* Liab_MedPay_sec_res,.0001)
			as liabimp_base_premium

			, round(liapd_base_rate
			* liapd_census
			* liapd_cove
			* liapd_ins_score
			* liapd_num_res
			* liapd_claimxPeril
			* liapd_claim
			* Liab_PD_sec_res,.0001)
			as liapd_base_premium

			, round(calculated liabipi_base_premium + calculated liapd_base_premium,1.0) as Liab_Personal
			, round(calculated liabimp_base_premium, 1.0) as Liab_Medical

		from out.base_rate f1
			inner join out.grid_fac					 f2 on f1.casenumber=f2.casenumber
			inner join out.census_fac				 f3 on f1.casenumber=f3.casenumber
			inner join out.coverage_e_fac			 f4 on f1.casenumber=f4.casenumber
			inner join out.coverage_f_fac			 f5 on f1.casenumber=f5.casenumber
/*			inner join out.drc_fac					 f6 on f1.casenumber=f6.casenumber*/
			inner join out.ins_score_fac			 f7 on f1.casenumber=f7.casenumber
			inner join out.num_dogs_fac				 f8 on f1.casenumber=f8.casenumber
			inner join out.num_residents_fac		 f9 on f1.casenumber=f9.casenumber
			inner join out.num_units_fac		    f10 on f1.casenumber=f10.casenumber
			inner join out.priorclaim_NAOG_fac		f11 on f1.casenumber=f11.casenumber
			inner join out.priorclaim_xPeril_fac	f12 on f1.casenumber=f12.casenumber
			inner join out.priorclaim_Peril_fac		f13 on f1.casenumber=f13.casenumber
			inner join out.pool_fac					f23 on f1.casenumber=f23.casenumber
			inner join out.Seasonal_Secondary_Fac	f25 on f1.casenumber=f25.casenumber

;quit;

********************603	Residence Employees*************************;

%if %sysfunc(exist(out.lia_ResEmployee_fac)) %then %do;
		proc sql;
			create table step6_Liability_Premium_603 as 
			select *
			, round(liabipi_base_premium * liabipi_ResEmployee,.0001) as ResEmployee_bipi
			, round(liabimp_base_premium * liabimp_ResEmployee,.0001) as ResEmployee_bimp
			, round(calculated ResEmployee_bipi + calculated ResEmployee_bimp,1.0) as ResEmployee
			from step6_Liability_Premium_base p1
			inner join out.lia_ResEmployee_fac		f15 on p1.casenumber=f15.casenumber
		;quit;
%end;
%else %do;
		proc sql;
			create table step6_Liability_Premium_603 as 
			select *
			, 0 as ResEmployee_bipi
			, 0 as ResEmployee_bimp
			, round(calculated ResEmployee_bipi + calculated ResEmployee_bimp,1.0) as ResEmployee
			from step6_Liability_Premium_base p1
		;quit;
%end;

********************609	Business Pursuits*************************;

%if %sysfunc(exist(out.lia_busipursuit_fac)) %then %do;
		proc sql;
			create table step6_Liability_Premium_609 as 
			select *
			, round(liabipi_base_premium * liabipi_busipursuit,.0001) as BusiPursuit_bipi
			, round(liabimp_base_premium * liabimp_busipursuit,.0001) as BusiPursuit_bimp
			, round(calculated BusiPursuit_bipi + calculated BusiPursuit_bimp,1.0) as BusiPursuit
			from step6_Liability_Premium_603 p2
			inner join out.lia_busipursuit_fac		f16 on p2.casenumber=f16.casenumber
		;quit;
%end;
%else %do;
		proc sql;
			create table step6_Liability_Premium_609 as 
			select *
			, 0 as BusiPursuit_bipi
			, 0 as BusiPursuit_bimp
			, round(calculated BusiPursuit_bipi + calculated BusiPursuit_bimp,1.0) as BusiPursuit
			from step6_Liability_Premium_603 p2
		;quit;
%end;

********************610	Personal Injury*************************;

%if %sysfunc(exist(out.lia_personalinjury_fac)) %then %do;
		proc sql;
			create table step6_Liability_Premium_610 as 
			select *
			, round(liabipi_base_premium * liabipi_personalinjury,.0001) as PersonalInjury_bipi
			, round(calculated PersonalInjury_bipi,1.0) as PersonalInjury
			from step6_Liability_Premium_609 p3
			inner join out.lia_personalinjury_fac	f17 on p3.casenumber=f17.casenumber
		;quit;
%end;
%else %do;
		proc sql;
			create table step6_Liability_Premium_610 as 
			select *
			, 0 as PersonalInjury_bipi
			, round(calculated PersonalInjury_bipi,1.0) as PersonalInjury
			from step6_Liability_Premium_609 p3
		;quit;
%end;

********************612	Outboard Motors and Watercraft*************************;

%if %sysfunc(exist(out.lia_watercraft_fac)) %then %do;
		proc sql;
			create table step6_Liability_Premium_612 as 
			select *
			, round(round(liabipi_base_premium * liabipi_watercraft_1,.0001)
			+ round(liabimp_base_premium * liabimp_watercraft_1,.0001)
			+ round(liapd_base_premium * liapd_watercraft_1,.0001),1.0) as Watercraft_1

			, round(round(liabipi_base_premium * liabipi_watercraft_2,.0001)
			+ round(liabimp_base_premium * liabimp_watercraft_2,.0001)
			+ round(liapd_base_premium * liapd_watercraft_2,.0001),1.0) as Watercraft_2

			, round(round(liabipi_base_premium * liabipi_watercraft_3,.0001)
			+ round(liabimp_base_premium * liabimp_watercraft_3,.0001)
			+ round(liapd_base_premium * liapd_watercraft_3,.0001),1.0) as Watercraft_3

			, round(round(liabipi_base_premium * liabipi_watercraft_4,.0001)
			+ round(liabimp_base_premium * liabimp_watercraft_4,.0001)
			+ round(liapd_base_premium * liapd_watercraft_4,.0001),1.0) as Watercraft_4

			, round(round(liabipi_base_premium * liabipi_watercraft_5,.0001)
			+ round(liabimp_base_premium * liabimp_watercraft_5,.0001)
			+ round(liapd_base_premium * liapd_watercraft_5,.0001),1.0) as Watercraft_5

			, round(round(liabipi_base_premium * liabipi_watercraft_1,.0001)
			+ round(liabipi_base_premium * liabipi_watercraft_2,.0001)
			+ round(liabipi_base_premium * liabipi_watercraft_3,.0001)
			+ round(liabipi_base_premium * liabipi_watercraft_4,.0001)
			+ round(liabipi_base_premium * liabipi_watercraft_5,.0001),1.0) as Watercraft_bipi

			, round(round(liabimp_base_premium * liabimp_watercraft_1,.0001)
			+ round(liabimp_base_premium * liabimp_watercraft_2,.0001)
			+ round(liabimp_base_premium * liabimp_watercraft_3,.0001)
			+ round(liabimp_base_premium * liabimp_watercraft_4,.0001)
			+ round(liabimp_base_premium * liabimp_watercraft_5,.0001),1.0) as Watercraft_bimp

			, round(round(liapd_base_premium * liapd_watercraft_1,.0001)
			+ round(liapd_base_premium * liapd_watercraft_2,.0001)
			+ round(liapd_base_premium * liapd_watercraft_3,.0001)
			+ round(liapd_base_premium * liapd_watercraft_4,.0001)
			+ round(liapd_base_premium * liapd_watercraft_5,.0001),1.0) as Watercraft_pd

			, calculated Watercraft_1 + calculated Watercraft_2 + calculated Watercraft_3 + calculated Watercraft_4 + calculated Watercraft_5 as Watercraft 

			from step6_Liability_Premium_610 p4
			inner join out.lia_watercraft_fac		f18 on p4.casenumber=f18.casenumber

		;quit;
%end;
%else %do;
		proc sql;
			create table step6_Liability_Premium_612 as 
			select *
			, 0 as Watercraft_1
			, 0 as Watercraft_2
			, 0 as Watercraft_3
			, 0 as Watercraft_4
			, 0 as Watercraft_5
			, 0 as Watercraft_bipi
			, 0 as Watercraft_bimp
			, 0 as Watercraft_pd
			, calculated Watercraft_1 + calculated Watercraft_2 + calculated Watercraft_3 + calculated Watercraft_4 + calculated Watercraft_5 as Watercraft 

			from step6_Liability_Premium_610 p4
		;quit;
%end;

********************613	Owned Snowmobile*************************;

%if %sysfunc(exist(out.lia_snowmobile_fac)) %then %do;
		proc sql;
			create table step6_Liability_Premium_613 as 
			select *
			, round(liabipi_base_premium * liabipi_snowmobile,.0001) as Snowmobile_bipi
			, round(liabimp_base_premium * liabimp_snowmobile,.0001) as Snowmobile_bimp
			, round(liapd_base_premium * liapd_snowmobile,.0001) as Snowmobile_pd
			, num_snowmobile * round((calculated Snowmobile_bipi + calculated Snowmobile_bimp + calculated Snowmobile_pd),1.0) as Snowmobile
			from step6_Liability_Premium_612 p5
			inner join out.lia_snowmobile_fac	 	f20 on p5.casenumber=f20.casenumber
		;quit;
%end;
%else %do;
		proc sql;
			create table step6_Liability_Premium_613 as 
			select *
			, 0 as Snowmobile_bipi
			, 0 as Snowmobile_bimp
			, 0 as Snowmobile_pd
			, round((calculated Snowmobile_bipi + calculated Snowmobile_bimp + calculated Snowmobile_pd),1.0) as Snowmobile
			from step6_Liability_Premium_612 p5
		;quit;
%end;

********************617	Dangerous Dog Premium*************************;

%if %sysfunc(exist(out.lia_DangerousDog_fac)) %then %do;
		proc sql;
			create table step6_Liability_Premium_617 as 
			select *
			, round(liabipi_base_premium * liabipi_DangerousDog,.0001) as DangerousDog_bipi
			, round(liabimp_base_premium * liabimp_DangerousDog,.0001) as DangerousDog_bimp
			, round(liapd_base_premium * liapd_DangerousDog,.0001) as DangerousDog_pd
			, round(calculated DangerousDog_bipi + calculated DangerousDog_bimp + calculated DangerousDog_pd,1.0) as DangerousDog
			from step6_Liability_Premium_613 p6
			inner join out.lia_DangerousDog_fac		f21 on p6.casenumber=f21.casenumber
		;quit;
%end;
%else %do;
		proc sql;
			create table step6_Liability_Premium_617 as 
			select *
			, 0 as DangerousDog_bipi
			, 0 as DangerousDog_bimp
			, 0 as DangerousDog_pd
			, round(calculated DangerousDog_bipi + calculated DangerousDog_bimp + calculated DangerousDog_pd,1.0) as DangerousDog
			from step6_Liability_Premium_613 p6
		;quit;
%end;

********************620	Domestic Animal Premium*************************;

%if %sysfunc(exist(out.lia_DomesticAnimal_fac)) %then %do;
		proc sql;
			create table step6_Liability_Premium_620 as 
			select *
			, round(liabipi_base_premium * liabipi_DomesticAnimal,.0001) as DomesticAnimal_bipi
			, round(liabimp_base_premium * liabimp_DomesticAnimal,.0001) as DomesticAnimal_bimp
			, round(calculated DomesticAnimal_bipi + calculated DomesticAnimal_bimp,1.0) as DomesticAnimal
			from step6_Liability_Premium_617 p6
			inner join out.lia_DomesticAnimal_fac		f26 on p6.casenumber=f26.casenumber
		;quit;
%end;
%else %do;
		proc sql;
			create table step6_Liability_Premium_620 as 
			select *
			, 0 as DomesticAnimal_bipi
			, 0 as DomesticAnimal_bimp
			, round(calculated DomesticAnimal_bipi + calculated DomesticAnimal_bimp,1.0) as DomesticAnimal
			from step6_Liability_Premium_617 p6
		;quit;
%end;

********************621	Pool Liability Premium*************************;

%if %sysfunc(exist(out.lia_Pool_Liab_fac)) %then %do;
		proc sql;
			create table step6_Liability_Premium_621 as 
			select *
			, round(liabipi_base_premium * liabipi_Pool_Liab,.0001) as Poolliab_bipi
			, round(liabimp_base_premium * liabimp_Pool_Liab,.0001) as Poolliab_bimp
			, round(calculated Poolliab_bipi + calculated Poolliab_bimp,1.0) as Poolliab
			from step6_Liability_Premium_620 p6
			inner join out.lia_Pool_Liab_fac		f27 on p6.casenumber=f27.casenumber
		;quit;
%end;
%else %do;
		proc sql;
			create table step6_Liability_Premium_621 as 
			select *
			, 0 as Poolliab_bipi
			, 0 as Poolliab_bimp
			, round(calculated Poolliab_bipi + calculated Poolliab_bimp,1.0) as Poolliab
			from step6_Liability_Premium_620 p6
		;quit;
%end;

********************628	Home Business Endorsement - Liability*************************;

%if %sysfunc(exist(out.Home_Business_Liability_Fac)) %then %do;
		proc sql;
			create table step6_Liability_Premium_628 as 
			select *
			, (liabipi_base_premium * Home_Bus_liabipi_fac / liabipi_grid / liabipi_census) as Home_Bus_Liability_bipi
			, (liabimp_base_premium * Home_Bus_liabimp_fac / liabimp_grid / liabimp_census) as Home_Bus_Liability_bimp
			, (liapd_base_premium   * Home_Bus_liabipd_fac / liapd_census) as Home_Bus_Liability_pd
			, round(calculated Home_Bus_Liability_bipi + calculated Home_Bus_Liability_bimp + calculated Home_Bus_Liability_pd,1.0) as Home_Bus_Liability
			from step6_Liability_Premium_621 p7
			inner join out.Home_Business_Liability_Fac f24 on p7.casenumber=f24.casenumber 
		;quit;
%end;
%else %do;
		proc sql;
			create table step6_Liability_Premium_628 as 
			select *
			, 0 as Home_Bus_Liability_bipi
			, 0 as Home_Bus_Liability_bimp
			, 0 as Home_Bus_Liability_pd
			, round(calculated Home_Bus_Liability_bipi + calculated Home_Bus_Liability_bimp + calculated Home_Bus_Liability_pd,1.0) as Home_Bus_Liability
			from step6_Liability_Premium_621 p7
		;quit;
%end;


********************   Step6_Liability_Premium   *************************;

	proc sql;
		create table out.step6_Liability_Premium as
		select *
			, ResEmployee_bipi + BusiPursuit_bipi + PersonalInjury_bipi + Watercraft_bipi 
			+ Snowmobile_bipi + DangerousDog_bipi + Home_Bus_Liability_bipi + DomesticAnimal_bipi
			+ Poolliab_bipi
			as liabipi_opt

			, ResEmployee_bimp + BusiPursuit_bimp + Watercraft_bimp + Snowmobile_bimp
			+ DangerousDog_bimp + Home_Bus_Liability_bimp + DomesticAnimal_bimp + Poolliab_bimp
			as liabimp_opt

			, Watercraft_pd + Snowmobile_bipi + DangerousDog_pd + Home_Bus_Liability_pd
			as liapd_opt

			, Liab_Personal + Liab_Medical + ResEmployee
			+ BusiPursuit + PersonalInjury + Watercraft 
			+ Snowmobile + DangerousDog + DomesticAnimal + Poolliab + Home_Bus_Liability
			as Total_Liability_Premium
		from step6_Liability_Premium_628 
;
	quit;


%if &Process = 0 %then %do;
	title 'STEP 6: LIABILITY PREMIUM (RULE 301.A.6)';
	proc print data=out.step6_Liability_Premium;
	where Total_Liability_Premium = .;
	run;
	proc sql;
		select mean(liabipi_base_premium) as liabipi_base_prem_avg		
			, mean(liabimp_base_premium) as liabimp_base_prem_avg		
			, mean(liapd_base_premium) as liapd_base_prem_avg
			, mean(Total_Liability_Premium) as Total_Liability_prem_avg
		from out.step6_Liability_Premium;
	quit;
%end;

%mend;
%liab();
