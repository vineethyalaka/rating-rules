** RULE 342. NEW ROOF DISCOUNT **;
data roof_age2;
	set roof_age;

	if roof_age>10 then
		roof_age=10;
run;

data new_roof;
	set fac.'342# New Roof Discount$'n;

	if f1=. and wind ne . then
		f1=10;
run;

proc sql;
	create table out.new_roof_fac as
		select p.casenumber, p.policynumber, p.PropertyYearRoofInstalled, p.roof_age, p2.propertyrooftypecode
			, f.'Water Weather'n as waterw_new_roof
			, f.wind as wind_new_roof
			, f.hail as hail_new_roof
			, f.hurricane as hurr_new_roof
		from roof_age2 p 
			left join roof_type	p2 on p.casenumber=p2.casenumber
			left join new_roof	f on p.roof_age=f.f1 and lowcase(p2.propertyrooftypecode)=lowcase(f.f2);
quit;