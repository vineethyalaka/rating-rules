** RULE 323. SQUARE FOOTAGE **;
data sqft;
	set in.policy_info;

	if propertysquarefootage = 0 then
		propertysquarefootage = 1850;
run;

data sqft_fac;
	set fac.'323# Square Footage$'n;

	if f3=. and theft ne . then
		f3=99999;

	if f1=. and f3=. then
		delete;
run;

proc sql;
	create table out.sqft_fac as
		select p.casenumber, p.policynumber, p.propertysquarefootage
			, f.theft as theft_sqft
		from sqft p left join sqft_fac f
			on p.propertysquarefootage>=f.f1
			and p.propertysquarefootage<=f.f3;
quit;