/*Add 402.C back for CT Specific*/
/*** RULE 402.C SPECIAL LOSS SETTLEMENT **;*/
proc sql;
	create table out.r402_C_Special_Loss_Settlement as
		select p.CaseNumber, p.PolicyNumber, p.HO_0456_FLAG, p.HO_04_56_PCT
			, 
		case 
			when p.HO_0456_FLAG='0' then 1 
			else f.fac2 
		end 
	as r402c_HO_04_56_fac
		from in.Endorsement_Info p
			left join r402c f
				on p.HO_04_56_PCT = f.pct;
quit;