** RULE 505.C EARTHQUAKE LOSS ASSESSMENT **;
%macro eql();
	data r505c (keep=ConstructionType zone Rate Increment);
	length ConstructionType $14.;
	set fac.'505#C EQ Loss Assessment$'n;
	if strip('Zone/Construction Type'n) = . then delete;
	ConstructionType = "Frame"; Rate = Frame; output;
	ConstructionType = "Masonry"; Rate = Masonry; output;
	ConstructionType = "Masonry Veneer"; Rate = 'Masonry Veneer'n; output;
	ConstructionType = "Superior"; Rate = Superior; output;

	rename 'Coverage Limit Increment (i#e# r'n=Increment 'Zone/Construction Type'n=zone;
	run;
	proc sql;
		create table out.EQ_Loss_Assess_rate as
		select p.casenumber, p.policynumber, p.PropertyQuakeZone, p1.propertyconstructionclass, 
			p1.ConstructionType, p2.HO_0436_flag, p2.HO_0436_LIMIT_01
			, case when p2.HO_0436_flag = '0' then 0 else round(p2.HO_0436_LIMIT_01*f.Rate/f.Increment,1.0) end as EQ_Loss_Assess_rate
		from construction p1
			inner join in.Endorsement_Info	p2 on p1.casenumber = p2.casenumber
			inner join in.Policy_Info		p on p1.casenumber = p.casenumber
			left join r505c f on p1.ConstructionType = f.ConstructionType and f.zone = input(p.PropertyQuakeZone,2.);
	quit;

%if &Process = 0 %then %do;
	title 'RULE 505.C EARTHQUAKE LOSS ASSESSMENT';
	proc freq data=out.EQ_Loss_Assess_rate;
	tables HO_0436_flag*propertyconstructionclass*ConstructionType*PropertyQuakeZone*EQ_Loss_Assess_rate / list missing;
	run;
%end;
%mend;
%eql();