** RULE 353. UNDERWRITING TIER BY BILL PLAN **;
** Add ScoreModel = 3 **;
%macro insbill();
	*PA Specific: UW Tier = Insurance Score x DRC;
	data exp_uw_tier_x_bill_plan_fac_1 (keep=bill_plan uw_tier factor);
	length bill_plan $20.;
	set fac.'353#1 UW Tier by Bill Plan$'n;
	if f3=. then delete;
	bill_plan='10-pay'; factor = 'Expense Fee'n; output;
	bill_plan='4-pay'; factor = f3; output;
	bill_plan='Full-pay or Mortgage'; factor = f4; output;
	rename f1 = uw_tier;
	run;

	data exp_uw_tier_x_bill_plan_fac_2 (keep=bill_plan uw_tier factor);
	length bill_plan $20.;
	set fac.'353#2 UW Tier by Bill Plan$'n;
	if f3=. then delete;
	bill_plan='10-pay'; factor = 'Expense Fee'n; output;
	bill_plan='4-pay'; factor = f3; output;
	bill_plan='Full-pay or Mortgage'; factor = f4; output;
	rename f1 = uw_tier;
	run;

	proc sql;
		create table out.exp_uw_tier_x_bill_plan_fac as
		select p1.*, p2.matchcode, p2.ScoreModel, p2.insscore, p2.ins_tier, p2.ins_tier_n, p2.drc_class, p2.uw_tier, p2.uw_tier_n
					, coalesce(fa.factor, fb.factor) as exp_uw_tier_x_bill_plan_fac
		from bill_plan p1 
			inner join uw_tier p2 on p1.casenumber = p2.casenumber
			left join exp_uw_tier_x_bill_plan_fac_1 fa /* These are the TUIRS (APEX) factors */
				on p2.ScoreModel = 2 and 
					fa.bill_plan = p1.BillPlanType and fa.uw_tier = p2.uw_tier
			left join exp_uw_tier_x_bill_plan_fac_2 fb /* These are the TRC (Mosaic) factors */
				on p2.ScoreModel = 3 and
					fb.bill_plan = p1.BillPlanType and fb.uw_tier = p2.uw_tier;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 353. UNDERWRITING TIER BY BILL PLAN';
	proc freq data=out.exp_uw_tier_x_bill_plan_fac;
	tables BillPlanType*uw_tier_n*exp_uw_tier_x_bill_plan_fac / list missing;
	run;
%end;
%mend;
%insbill();