/* 02-13-2020 ITR 15050 VR Limit capping based on capcounter */

/*Old broll policies Add Capping*/
%macro get_capping_factor();

/*********** parse the renewal capping factors table *******************************************************************************************/
%renewal_capping_factors;

/*********** parse the broll capping factors table *******************************************************************************************/
%broll_capping_factors;

/*********** parse the new broll group table *******************************************************************************************/
%Group_Parsing;

	
/*********** select the max term of old capping dynamically *******************************************************************************************/
	proc sql noprint;
		select max(Term_lower) into :maxterm
			from cap.'capping$'n;
	quit;

/*********** old broll and new broll capping *******************************************************************************************/

	libname CapAcc "\\cambosvnxcifs01\ITRatingTeam\Capping";

	proc sql;
		create table out.step9_ratio as
		select p1.*
			, p.RatingVersion
			, p.PolicyFormNumber
			, c.CapVersion
			, c.CapCounter
			, c.Current_Rate_Stab_Capping_Factor
		
			/*Add below logic for old broll capping*/
			, case when c.PolicyAccountNumber in (select AccountNumber from CapAcc.BROLL_ACCOUNT_NUMBER) then  1 else 0 end as Old_Broll_Account

			/*less than 4->old broll*/
			, case when calculated Old_Broll_Account = 1 and f.Term_Upper < &maxterm. then 1  else 0 end as Old_Broll_Policy

			, c.cap as DB_Cap_Amount  /*database cap amount*/
			, case when calculated Old_Broll_Policy = 1 then f.Capping_Floor else . end as Capping_Floor
			, case when calculated Old_Broll_Policy = 1 then f.Capping_Ceiling else . end as Capping_Ceiling
			, case when calculated Old_Broll_Policy = 1 then c.prior_premium*(1+f.Capping_Floor) else . end as Premium_Floor
			, case when calculated Old_Broll_Policy = 1 then c.prior_premium*(1+f.Capping_Ceiling) else . end as Premium_Ceiling
			, c.HomesitePreviousCasePremium - c.CapDiscount as HomesiteManualPremium 
				/*If broll policy is amended, calculated with percentage*/
			, case when calculated Old_Broll_Policy = 1 and p.POLICY_TRANS_TYPE = 1 and c.prior_premium = 0 then 
			  ( p1.uncapped_calc_premium - calculated HomesiteManualPremium)/calculated HomesiteManualPremium else . end as Cap_Amend_Factor
				/*If broll policy is amended, calculated with percentage*/
			, case when calculated Old_Broll_Policy = 1 and p.POLICY_TRANS_TYPE = 1 and c.prior_premium = 0 then 
				 p1.uncapped_calc_premium + c.CapDiscount+
				round(c.HomesitePreviousCasePremium * calculated Cap_Amend_Factor - ( p1.uncapped_calc_premium - calculated HomesiteManualPremium),1)
				/*If broll policy is not amended, cap regularly*/
				when calculated Old_Broll_Policy = 1 then 
					case when  p1.uncapped_calc_premium between calculated Premium_Floor and calculated Premium_Ceiling then  p1.uncapped_calc_premium
				/*round the Cap Amount to match with Database*/
					when  p1.uncapped_calc_premium < calculated Premium_Floor then round((calculated Premium_Floor -  p1.uncapped_calc_premium),1) +  p1.uncapped_calc_premium
					when  p1.uncapped_calc_premium > calculated Premium_Ceiling then round((calculated Premium_Ceiling -  p1.uncapped_calc_premium),1) +  p1.uncapped_calc_premium end
				else  p1.uncapped_calc_premium 
				end	as capped_premium
			, case when calculated Old_Broll_Policy = 1 then calculated capped_premium -  p1.uncapped_calc_premium else 0 end as SAS_Cap_Amount
		


			/*Add below logic for new broll capping*/
			/*1 : new broll capping*/
			/*0 : not new broll capping or old broll capping -> renewal business or new business */
			/*. :  */
			, case when c.PolicyAccountNumber in (select Account from work.BROLL_GROUP_PARSED) then  1 else 0 end as New_Broll_Account
			, case when calculated New_Broll_Account = 1 and c.PolicyTermYears <&term_count.+1 then 1  
				   when calculated Old_Broll_Policy = 0 then 0
			       else . end as New_Broll_Policy
			, c.policyAccountNumber
			, case when calculated New_Broll_Policy = 1 then b.group
				   else . end as group


			/*Add below logic for amended polciy*/
			/*1 : amended policy*/
			/*0 : new broll policies or renewal business */
			/*. :  */
			, case when calculated New_Broll_Policy = 1 and p.POLICY_TRANS_TYPE = 1  then 1 
				   when calculated New_Broll_Policy = 1 then 0
				   when calculated New_Broll_Policy = 0 and c.PolicyTermYears <> 1 and p.POLICY_TRANS_TYPE = 1 then 1
				   when calculated New_Broll_Policy = 0 and c.PolicyTermYears <> 1 then 0
					else . end as Amended_Policy

			
			/*Add below logic for no broll renewal business capping and new broll capping for policies not amended*/
			, case when calculated Amended_Policy = 0 and c.PolicyTermYears = 1 then  1 
				   when calculated Amended_Policy = 0 then c.Prior_Rate_Stab_Capping_Factor 
				   else . end as Prior_Capping_Factor

			/*Add below logic for no broll new business capping*/
			, case when calculated New_Broll_Policy = 0 and c.PolicyTermYears = 1 then 1  else 0 end as NB_Policy

			/*get the temp Premium*/
			, case when calculated Amended_Policy = 0 and c.PolicyTermYears = 1 then c.Prior_Premium 
				   when calculated Amended_Policy = 0 then t.temp_premium
				   else . end as temp_premium

			/*get the capping ratio*/
			, case when calculated Amended_Policy = 0 and round(p1.uncapped_calc_premium/calculated temp_premium,.01) > 10 then 11
				   when calculated Amended_Policy = 0 then round(p1.uncapped_calc_premium/calculated temp_premium,.01)
				   else . end as change_ratio
			

		from out.step8_uncapped_calc_premium p1
			inner join in.capping_info c on p1.casenumber = c.casenumber
			inner join in.policy_info p on p1.casenumber = p.casenumber
			inner join cap.'capping$'n as f	on c.PolicyTermYears between f.Term_Lower and f.Term_Upper
			inner join out.temp_premium t on p1.casenumber = t.casenumber
			left join work.BROLL_GROUP_PARSED b on c.PolicyAccountNumber = b.account;
	quit;


	proc sql;
		create table out.step9_capping_factor as
		select p1.*,bc.group as bc_group, bc.term as bc_term, bc.ratio as bc_ratio, bc.factor as bc_factor, rc.ratio as rc_ratio, rc.factor as rc_factor
			/*get the capping factor*/
			, case when  p1.CapCounter not between ccl.MinCapTerm and ccl.MaxCapTerm then 1 
				   when  p1.NB_Policy = 1 then 1 
				   when  p1.Amended_Policy = 1 then p1.Current_Rate_Stab_Capping_Factor
				   when  p1.Amended_Policy = 0 then rc.factor
				   when  p1.New_Broll_Policy = 1 then bc.factor 
				   when  p1.New_Broll_Policy = 0 then rc.factor
				   else .
				   end as capping_factor
			/*get the capped premium*/
			
		from out.step9_ratio p1
			left join work.broll_capping_factors bc on p1.group = bc.group and p1.policyTermYears = bc.term and p1.change_ratio = bc.ratio
			left join work.renewal_capping_factors rc on p1.change_ratio = rc.ratio and p1.PolicyEffectiveDate between rc.after and rc.before
			left join in.CapCounterLimits ccl on p1.RatingVersion = ccl.Version and p1.PolicyFormNumber = ccl.FormNumber 
			;		

	quit;


%mend;

%get_capping_factor();

