** RULE 590. MINE SUBSIDENCE INSURANCE **;

%macro ms();
data r590;
set fac.'590# Mine Subsidence-Dwelling$'n;
if f1 = . and f3 = . then delete;
run;
proc sql;
	create table out.Mine_Sub_Rate as
	select p.CaseNumber, p.PolicyNumber, e.HO2388_FLAG, p.CoverageA
		, case when e.HO2388_FLAG = '1' then f4 else 0 end as Mine_Sub_Rate
	from in.policy_info p
		inner join in.Endorsement_Info e on p.casenumber = e.casenumber
		left join r590 f on p.CoverageA >= f.f1 and (p.CoverageA <= f.f3 or f.f3 = .);
quit;

title 'RULE 590. MINE SUBSIDENCE INSURANCE';

%if &Process = 0 %then %do;
proc print data=out.Mine_Sub_Rate;
where Mine_Sub_Rate = .;
run;
proc sort data=out.Mine_Sub_Rate; by HO2388_FLAG; run;
proc gplot data=out.Mine_Sub_Rate; by HO2388_FLAG;
	plot Mine_Sub_Rate*CoverageA 
		/ overlay legend vaxis=axis2;
run;
quit;

%end;
%mend;
%ms();