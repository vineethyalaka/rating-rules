** RULE 310. TOWNHOUSE OR ROWHOUSE **;
*ITR 15050 CO Mosaic VR - Add wildfire factor ;

%macro town();

	data r310_sheet_manipulation;
		set fac.'310# Townhouse or Rowhouse$'n (obs=3 firstobs=2);
		keep f1 Fire 'Water Non-Weather'n 'Cov C - Fire'n Wildfire;
	run;

	data rowhouse;
	set in.policy_info;
	if propertyunitsinbuilding >= 2 then num_units='2+     ';
	else num_units='1';
	run;
	proc sql;
		create table out.rowhouse_fac as
		select p.CaseNumber, p.PolicyNumber, p.PropertyTypeofDwelling, p.propertyunitsinbuilding, p.num_units
			 , case when p.PropertyTypeofDwelling in ('Rowhouse','Townhouse') then f.fire else 1 end as fire_rowhouse
			 , case when p.PropertyTypeofDwelling in ('Rowhouse','Townhouse') then f.'Water Non-Weather'n else 1 end as waternw_rowhouse
			 , case when p.PropertyTypeofDwelling in ('Rowhouse','Townhouse') then f.'Cov C - Fire'n else 1 end as Cov_C_Fire_rowhouse
			 , case when p.PropertyTypeofDwelling in ('Rowhouse','Townhouse') then f.Wildfire else 1 end as WF_rowhouse
		from rowhouse p left join r310_sheet_manipulation f
			on p.num_units=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 310. TOWNHOUSE OR ROWHOUSE';
	proc freq data=out.rowhouse_fac;
	tables PropertyTypeofDwelling*propertyunitsinbuilding
		*fire_rowhouse
		*waternw_rowhouse
		*Cov_C_Fire_rowhouse / list missing;
	run;
%end;
%mend;
%town();
