*HO6 Tier 1 Insurance Score Class PA - uses only TU score model;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance)
            in.Production_Info
            (CaseNumber PolicyNumber PriorInsurance)
            in.drc_info
			(CaseNumber PolicyNumber InsScore ScoreModel)
            out.Tier4_PriorClaims
			(CaseNumber PolicyNumber Prior_Claims_Class)
			Fac.TierClaimsCreditClass
			(PriorClaimClass Inslower Insupper Tier)
			Fac.TierClaimsCreditFactor
			(Tier Factor)
			;
*Creates: 	out.Tier2_ClaimsCredit_Tier out.Tier2_ClaimsCredit_Factor;

proc sql;
	create table out.Tier2_ClaimsCredit_Tier as
	select p.CaseNumber, p.PolicyNumber, c.Prior_Claims_Class, d.ScoreModel 
           , d.InsScore, p.Lapse_of_Insurance, pd.PriorInsurance, t.tier as ClaimsCredit_Tier
	from in.Policy_Info as p
		left join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		left join in.drc_info as d
		on p.CaseNumber = d.CaseNumber
		left join out.Tier4_PriorClaims as c
		on p.CaseNumber = c.CaseNumber
		left join Fac.TierClaimsCreditClass as t
		on t.Insupper>d.InsScore
		and t.Inslower<=d.InsScore
		and t.ScoreModel = d.ScoreModel
		and t.PriorClaimClass=c.Prior_Claims_Class
	;
quit;

proc sql;
	create table out.Tier2_ClaimsCredit_Factor as
	select t.CaseNumber, t.PolicyNumber, t.ClaimsCredit_Tier, f.Factor as ClaimsCredit_Factor
	from out.Tier2_ClaimsCredit_Tier as t
	left join Fac.TierClaimsCreditFactor as f
	on t.ClaimsCredit_Tier = f.Tier
	;
quit;
