*Rule 604 Liability Additional Residence Rented to Others CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PropertyUnitsInBuilding ResidencesRentedToOthers CoverageE CoverageF)
            Fac.LiabilityRentedResidenceEFactor
            (Factor Families CoverageE)
            Fac.LiabilityRentedResidenceFFactor
            (Factor Families CoverageF)
			;
*Creates: 	out.Liability_Rented_Residence;

proc sql;
    create table out.Liability_Rented_Residence as
	select distinct p.CaseNumber, p.PolicyNumber
		 , p.ResidencesRentedToOthers*ce.Factor as Rented_Residence_E label="Rented Residence E"
		 , p.ResidencesRentedToOthers*cf.Factor as Rented_Residence_F label="Rented Residence F"
	from in.Policy_Info as p
	inner join Fac.LiabilityRentedResidenceEFactor as ce
	on p.PropertyUnitsInBuilding = ce.Families
	and p.CoverageE = ce.CoverageE
	inner join Fac.LiabilityRentedResidenceFFactor as cf
	on p.PropertyUnitsInBuilding = cf.Families
	and p.CoverageF = cf.CoverageF
	;
quit;
