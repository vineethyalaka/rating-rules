*Wind Rule 407.C.2 Additioanl Amount of Insurance for Coverage A, B, C, and D CW (HO-04-11);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0411_FLAG)
            Fac.WindHO0411Factor
            (Factor)
			;
*Creates: 	out.Wind_R407_2_Add_AMT_CovABCD;

proc sql;
	create table out.Wind_R407_2_Add_AMT_CovABCD as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0411_FLAG = '1' then hf411.Factor else 1 end as HO_04_11
	from out.policy_year y
		inner join in.Endorsement_Info as e
		on y.CaseNumber = e.CaseNumber
		inner join Fac.WindHO0411Factor as hf411
		on hf411.PolicyYear = y.PolicyYear
	;
quit;
