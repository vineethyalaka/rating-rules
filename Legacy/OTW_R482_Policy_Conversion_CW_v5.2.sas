*OTW Rule 482 Policy Conversion CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears RolloverCode)
            Fac.OTWPolicyConversionFactor
            (Factor TermYear_upper TermYear_lower)
			;
*Creates: 	out.OTW_R482_Policy_Conversion;

/*KM 11/7/17 Capitalized "HOMER" to fix issue with CT*/


proc sql;
	create table out.OTW_R482_Policy_Conversion as
	select p.CaseNumber, p.PolicyNumber, p.RolloverCode
		 , find(p.RolloverCode,"HOMER",'I') as Code
		 , case when calculated code>0 then pcf.Factor else 1 end as Conversion
	from in.Policy_Info as p
	inner join Fac.OTWPolicyConversionFactor as pcf
	on  p.PolicyTermYears <= pcf.TermYear_upper
	and p.PolicyTermYears > pcf.TermYear_lower
	;
quit;
