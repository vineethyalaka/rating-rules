*HO4 Rule 590.2 Hurricane Base Premium CW;
*Uses:    	out.Hurricane_Territory_Rate
            (CaseNumber PolicyNumber Hurricane_Territory_Rate)
            out.Wind_AOI_Factor
            (CaseNumber PolicyNumber Wind_AOI)
			;
*Creates: 	out.Hurricane_Base_Premium;

proc sql;
    create table out.Hurricane_Base_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Hurricane_Territory_Rate
		 , d.Wind_AOI
		 , round(a.Hurricane_Territory_Rate*d.Wind_AOI,1) 
			as Hurricane_Base_Premium
	from out.Hurricane_Territory_Rate as a 
	inner join out.Wind_AOI_Factor as d 
	on a.CaseNumber = d.CaseNumber
	;
quit;
