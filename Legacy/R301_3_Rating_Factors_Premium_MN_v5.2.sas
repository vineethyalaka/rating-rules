*Rule 301.3 OTW Rating Factors Premium MN;
*Uses:    	out.OTW_Tiered_Base_Premium
			(CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
            out.OTW_R302_B_ACV_Loss_Settlement
			(CaseNumber PolicyNumber HO_04_81)
            out.OTW_R401_Superior_Constr
			(CaseNumber PolicyNumber OTW_Superior_Construction)
			out.OTW_R402_Row_House
			(CaseNumber PolicyNumber OTW_Row_House)
			out.OTW_R403_CovC_Replacement_Cost
			(CaseNumber PolicyNumber HO_04_90)
			out.OTW_R404_Protective_Devices
			(CaseNumber PolicyNumber Protective_Devices)
			out.OTW_R406_Deductibles
			(CaseNumber PolicyNumber OTW_Deductible_Factor)
			out.OTW_R407_1_Add_AMT_CovA
			(CaseNumber PolicyNumber HO_04_20)
			out.OTW_R408_ACV_Roof
			(CaseNumber PolicyNumber HO_04_93)
			out.OTW_R411_Home_Purchase
			(CaseNumber PolicyNumber Home_Purchase)
			out.OTW_R470_Age_of_ES
			(CaseNumber PolicyNumber Age_of_Electrical_System)
			out.OTW_R471_Roofing_Age_Materials
			(CaseNumber PolicyNumber Roof_Age_Materials)
			out.OTW_R472_Saft_Heat
			(CaseNumber PolicyNumber Safe_Heat)
			out.OTW_R480_Mature_Owner
			(CaseNumber PolicyNumber Mature_Owner)
			out.OTW_R482_Policy_Conversion
			(CaseNumber PolicyNumber Conversion)
			out.OTW_R486_Advantage
			(CaseNumber PolicyNumber Advantage)
			;
*Creates: 	out.OTW_Rating_Factors_Premium;

proc sql;
    create table out.OTW_Rating_Factors_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , b.HO_04_81 as OTW_HO_04_81
		 , c.OTW_Superior_Construction
		 , d.HO_04_90 as OTW_HO_04_90
		 , e.Protective_Devices as OTW_Protective_Devices
		 , f.OTW_Deductible_Factor
		 , g.HO_04_20 as OTW_HO_04_20
		 , i.HO_04_93 as OTW_HO_04_93
		 , j.Home_Purchase as Home_Purchase
		 , k.Age_of_Electrical_System as OTW_Age_of_Electrical_System
		 , l.Roof_Age_Materials as OTW_Roof_Age_Materials
		 , m.Safe_Heat as OTW_Safe_Heat
		 , n.Mature_Owner as OTW_Mature_Owner
		 , o.Conversion as OTW_Conversion
		 , p.Advantage as OTW_Advantage
		 , q.OTW_Row_House as OTW_Row_House
		 , a.OTW_Tiered_Base_Premium
		 , a.OTW_Tiered_Base_Premium*b.HO_04_81*c.OTW_Superior_Construction*d.HO_04_90*e.Protective_Devices*f.OTW_Deductible_Factor
			*g.HO_04_20*i.HO_04_93*j.Home_Purchase*k.Age_of_Electrical_System*l.Roof_Age_Materials*m.Safe_Heat*n.Mature_Owner*o.Conversion*p.Advantage*q.OTW_Row_House
			as OTW_Rating_Factors_Premium format=10.4
	from out.OTW_Tiered_Base_Premium as a 
	inner join out.OTW_R302_B_ACV_Loss_Settlement as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_R401_Superior_Constr as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.OTW_R403_CovC_Replacement_Cost as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_R404_Protective_Devices as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_R406_Deductibles as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_R407_1_Add_AMT_CovA as g 
	on a.CaseNumber = g.CaseNumber
	inner join out.OTW_R408_ACV_Roof as i 
	on a.CaseNumber = i.CaseNumber
	inner join out.OTW_R411_Home_Purchase as j 
	on a.CaseNumber = j.CaseNumber
	inner join out.OTW_R470_Age_of_ES as k 
	on a.CaseNumber = k.CaseNumber
	inner join out.OTW_R471_Roofing_Age_Materials as l 
	on a.CaseNumber = l.CaseNumber
	inner join out.OTW_R472_Saft_Heat as m 
	on a.CaseNumber = m.CaseNumber
	inner join out.OTW_R480_Mature_Owner as n 
	on a.CaseNumber = n.CaseNumber
	inner join out.OTW_R482_Policy_Conversion as o 
	on a.CaseNumber = o.CaseNumber
	inner join out.OTW_R486_Advantage as p 
	on a.CaseNumber = p.CaseNumber
	inner join out.OTW_R402_Row_House as q 
	on a.CaseNumber = q.CaseNumber
	;
quit;
