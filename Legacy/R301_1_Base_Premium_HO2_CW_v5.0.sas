*Rule 301.1 OTW Base Premium CW (HO2);
*Uses:    	out.OTW_Territory_Rate
            (CaseNumber PolicyNumber OTW_Territory_Rate)
            out.OTW_Form_Factor
            (CaseNumber PolicyNumber OTW_Policy_Form)
            out.OTW_Prot_Constr_Factor
            (CaseNumber PolicyNumber OTW_Prot_Constr)
            out.OTW_AOI_Factor
            (CaseNumber PolicyNumber OTW_AOI)
            out.OTW_Multiple_Families_Factor
            (CaseNumber PolicyNumber OTW_Multiple_Families_Factor)
			;
*Creates: 	out.OTW_Base_Premium;

proc sql;
    create table out.OTW_Base_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.OTW_Territory_Rate
		 , b.OTW_Policy_Form
		 , c.OTW_Prot_Constr
		 , d.OTW_AOI
		 , e.OTW_Multiple_Families_Factor
		 , a.OTW_Territory_Rate*b.OTW_Policy_Form*c.OTW_Prot_Constr*d.OTW_AOI
		   *e.OTW_Multiple_Families_Factor as OTW_Base_Premium
	from out.OTW_Territory_Rate as a 
	inner join out.OTW_Form_Factor as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_Prot_Constr_Factor as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.OTW_AOI_Factor as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_Multiple_Families_Factor as e 
	on a.CaseNumber = e.CaseNumber
	;
quit;
