*HO4 Wind Rule 513 Ordinance or Law MA;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageC)
            in.Endorsement_Info
            (CaseNumber PolicyNumber ORD_LAW_FLAG)
            Fac.WindOrdinanceOrLawFactor
            (Factor)
			;
*Creates: 	out.Wind_R513_Ordinance_or_Law;

proc sql;
	create table out.Wind_R513_Ordinance_or_Law as
	select distinct p.CaseNumber,p.PolicyNumber
		 , case when e.ORD_LAW_FLAG="1" then round(olf.factor*p.CoverageC*0.09/1000,1) else 0 end as Ordinance_or_Law
	from Fac.WindOrdinanceOrLawFactor as olf
		, in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber
	;
quit;
