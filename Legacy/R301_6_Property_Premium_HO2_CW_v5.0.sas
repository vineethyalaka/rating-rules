*Rule 301.6 Property Premium CW (HO2);
*Uses:    	out.OTW_R512_Loss_of_Use
			(CaseNumber PolicyNumber Coverage_D)
			out.OTW_R515_A_Incr_CovC
			(CaseNumber PolicyNumber Coverage_C)
			out.Wind_Premium
			(CaseNumber PolicyNumber Wind_Premium)
			;
*Creates: 	out.Property_Premium;

proc sql;
    create table out.Property_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.Coverage_D
		 , g.Coverage_C
		 , m.Wind_Premium
		 , a.Coverage_D
			+ g.Coverage_C
			+ m.Wind_Premium
			as Property_Premium format=10.4
		 , 0 as Other_Premium format=10.4
	from out.OTW_R512_Loss_of_Use as a 
	inner join out.OTW_R515_A_Incr_CovC as g 
	on a.CaseNumber = g.CaseNumber
	inner join out.Wind_Premium as m 
	on a.CaseNumber = m.CaseNumber
	;
quit;
