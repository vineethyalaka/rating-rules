*OTW Rule 471 Roofing Age/Materials CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate PropertyYearRoofInstalled PropertyRoofTypeCode)
            Fac.OTWRoofFactor
            (Factor Material)
			source.MappingRoofType
			;
*Creates: 	out.OTW_R471_Roofing_Age_Materials;

proc sql;
	create table work.OTW_R471_Roofing_Age_Materials as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
		 , (case when p.PolicyTermYears=1 and &effst = 0 then year(p.PolicyIssueDate) else year(p.PolicyEffectiveDate) end
	 		- case when p.PropertyYearRoofInstalled <= 0 then year(date()) else p.PropertyYearRoofInstalled end) as Roof_Age
	from in.Policy_Info as p
	;

	create table out.OTW_R471_Roofing_Age_Materials as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
		 , rf.Factor as Roof_Age_Materials label="Roof Age Materials"
	from work.OTW_R471_Roofing_Age_Materials as p
	left join source.MappingRoofType_NV as mrt
	on lower(trim(left(p.PropertyRoofTypeCode))) = lower(trim(left(mrt.PropertyRoofType)))
	inner join Fac.OTWRoofFactor as rf
	on  lower(trim(left(rf.Material))) = lower(trim(left(mrt.Type)))
	;
quit;
