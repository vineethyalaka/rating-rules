*Rule 590.1.b1 Multiple Family CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyUnitsInBuilding)
            Fac.WindMultipleFamilyFactor
            (Factor NumFamilies)
			;
*Creates: 	out.Wind_Multiple_Families_Factor;

proc sql;
    create table out.Wind_Multiple_Families_Factor as
	select distinct p.CaseNumber, p.PolicyNumber, mff.Factor as Wind_Multiple_Families_Factor label="Wind Multiple Families Factor"
	from in.policy_info as p
    inner join Fac.WindMultipleFamilyFactor as mff
    on p.PropertyUnitsInBuilding = mff.NumFamilies
	;
quit;
