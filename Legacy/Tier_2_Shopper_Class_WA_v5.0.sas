*Tier 2 Shopper Class CW;
*Uses:    	in.policy_info
			(CaseNumber PolicyNumber PolicyIssueDate)
			in.shopper_info
			(CaseNumber PolicyNumber end_code_2 end_code_3_PI end_code_3_NH)
            Fac.TierShopperClass
            (Shopper_lower Shopper_upper class)
			;
*Creates: 	out.Tier2_ShopperClass;

proc sql;
	create table out.Tier2_ShopperClass as 
	select distinct q.CaseNumber, q.PolicyNumber
		  ,pd.Lapse_Date_Used
		  ,q.Lapse_of_Insurance
		  ,q.end_code_3_NH as ProposeEffDate format=date9.
		  ,q.end_code_3_PI as CurrentExpDate format=date9.
		  ,q.end_code_3_NH - q.end_code_3_PI as period
		  ,l.LapseDays
		  ,case when pd.Lapse_Date_Used = 0 then 1
				when calculated period -1 <= l.LapseDays then 1 
				else 2
		   end as Regular_Lapse_Class
		, t.class as Regular_Shopper_Class label="Regular Shopper Class"
		, case when calculated Regular_Lapse_Class = 2 then 4 else t.class end as Shopper_Class label="Shopper Class"



		from (
			select distinct p.CaseNumber, p.PolicyNumber
				, case when s.end_code_3_PI > .
					then s.end_code_3_PI
					else s.end_code_3_NH
					end
					as Policy_Start_Date
				, case when s.end_code_2 is null
					then p.PolicyIssueDate
					else s.end_code_2
					end
					as Shopping_Date
				, case when calculated Policy_Start_Date is null
					then 8
					else (calculated Policy_Start_Date - calculated Shopping_Date)
					end
					as Number_of_Shopping_Days
				, p.Lapse_of_Insurance
				, p.end_code_3_NH
				, p.end_code_3_PI
			from in.policy_info as p
				inner join in.shopper_info as s
				on p.CaseNumber = s.CaseNumber
				and p.PolicyNumber = s.PolicyNumber
		) as q
		inner join Fac.TierShopperClass as t
		on q.Number_of_Shopping_Days >= t.Shopper_lower
		and q.Number_of_Shopping_Days < t.Shopper_upper
		inner join in.Production_Info as pd
		on q.CaseNumber = pd.CaseNumber
		, Fac.TierLapseDays as l
	;
quit;



