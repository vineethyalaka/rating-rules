/**Rule 590.3 Windstorm Deductible Adjusted for Minimum Deductible (with Wind Regions depending on distance to shore and terriotry);*/
/**Uses:    	in.Policy_Info*/
/*            (CaseNumber PolicyNumber CoverageA WindHailDeductible PropertyLatitude DistanceToShore TerritoryH)*/
/*            Fac.WindDeductibleFactor*/
/*            (class AOI_Lower AOI_Upper Factor_Lower Factor_Upper Deductible Region)*/
/*			;*/
/**Creates: 	out.Wind_Deductible;*/

proc sql;
	create table work.Wind_Deductible as
	select p.casenumber, p.policynumber
		, p.coverageA
		, p.TerritoryH
		, d.DeductibleTerritory
		, p.distancetoshore

		, p.WindHailDeductible
		, input(left(p.WindHailDeductible),5.) as WindHailDeductible_num
		, case when calculated WindHailDeductible_num <= 10
		 	then calculated WindHailDeductible_num * CoverageA /100
			else calculated WindHailDeductible_num
			end as WindHailDed_Dollar

		, p.HurricaneDeductible
		, input(left(p.HurricaneDeductible),5.) as HurricaneDeductible_num
		, case when calculated HurricaneDeductible_num <= 10
		 	then calculated HurricaneDeductible_num * CoverageA /100
			else calculated HurricaneDeductible_num
			end as HurrDed_Dollar

 		, case when p.ratingversion < 997 and &WindAdj = 0 then 0 
			when not missing(p.distancetoshore) and p.distancetoshore <=2500 then 1 
			else d.deductibleterritory end as ded_terr
		, case when &WindAdj = 0 then 0
			when calculated ded_terr in (1,2,4) then 5000
			else 2500 end as minwinddollar
		, case when &WindAdj = 0 then 0
			when calculated ded_terr in (1,2,4) then .02
			else .01 end as minwindpercent
		, case when &WindAdj = 0 then 0
			else calculated minwinddollar end as minhurrdollar
		, case when &WindAdj = 0 then 0
			when calculated ded_terr in (1) then .1
			when calculated ded_terr in (2,3) then .05
			else calculated minwindpercent end as minhurrpercent

		, calculated minwindpercent*CoverageA as CovA_pct_Wind
		, calculated minhurrpercent*CoverageA as CovA_pct_Hurr
		, max(calculated CovA_pct_Wind, calculated minwinddollar, calculated WindHailDed_Dollar) as min_wind_ded
		, max(calculated CovA_pct_Hurr, calculated minhurrdollar, calculated HurrDed_Dollar) as min_hurr_ded

		, case when calculated min_wind_ded = calculated CovA_pct_Wind then calculated minwindpercent*100
				when calculated min_wind_ded = calculated minwinddollar then calculated minwinddollar
		 		else calculated WindHailDeductible_num
					end as WindHailDeductible_adj

		, case when calculated min_hurr_ded = calculated CovA_pct_Hurr then calculated minhurrpercent*100
				when calculated min_hurr_ded = calculated minhurrdollar then calculated minhurrdollar
		 		else calculated HurricaneDeductible_num
					end as HurricaneDeductible_adj

from in.Policy_Info as p
	inner join fac.winddeductibleterritory as d
	on p.TerritoryH = d.Territory
	;
quit;
	
proc sql;
	    create table out.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber
		, p.coverageA
		, p.TerritoryH
		, p.DeductibleTerritory
		, p.distancetoshore
		, p.ded_terr

		, p.WindHailDeductible
		, p.WindHailDeductible_num
		, p.WindHailDed_Dollar

		, p.HurricaneDeductible
		, p.HurricaneDeductible_num
		, p.HurrDed_Dollar

		, p.minwinddollar
		, p.minwindpercent
		, p.minhurrdollar
		, p.minhurrpercent
		, p.CovA_pct_Wind
		, p.CovA_pct_Hurr
		, p.min_wind_ded
		, p.min_hurr_ded
		, p.WindHailDeductible_adj
		, p.HurricaneDeductible_adj

		, df.AOI_Lower
		, df.AOI_Upper
		, df.Factor_Lower
		, df.Factor_Upper

		, (p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
		, (df.AOI_Upper - p.CoverageA)/(df.AOI_Upper - df.AOI_Lower) as D2
		, (calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind
		, round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind
			+ (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 - calculated Ind)
	                , 0.001) as Wind_Deductible label="Wind Deductible"

		from work.Wind_Deductible as p
		inner join Fac.WindDeductibleFactor as df
		on p.WindHailDeductible_adj = df.WindHailDeductible
		and p.HurricaneDeductible_adj = df.HurricaneDeductible
		and p.CoverageA <= df.AOI_Upper
		and p.CoverageA > df.AOI_Lower
		and p.Ded_Terr = df.DeductibleTerritory
		;
quit;