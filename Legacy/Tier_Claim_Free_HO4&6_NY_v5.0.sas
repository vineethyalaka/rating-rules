*temp.PriorClaims_e;;
proc sql;
	create table work.PriorClaims_e as
	select p.casenumber, p.policynumber, p.policytermyears, p.policyeffectivedate, l.lossdate
	,l.LossCode, l.LossAmount, y.Group
			, Case 
				when y.PolicyYear = 2 then 2			
				when y.PolicyYear = 1  then 1 
					else 0 end as Age_Adj
			, round(((p.PolicyEffectiveDate - l.LossDate - 60)/365 - Calculated Age_Adj),1) as LossAgeOrg
			, p.ageofNYpolicy - Calculated Age_Adj as Term_Org
			, case when t.ClaimTypeGod is null then 0
				else t.ClaimTypeGod
			  end as ClaimAOG
			, case when t.ClaimTypeGod is null then 1
				else 0
			  end as ClaimNAG
			, case when Calculated Term_Org = 1 then (p.PolicyIssueDate - l.LossDate)/365
				else Calculated LossAgeOrg
			  end as LossAge
			, calculated ClaimAOG + calculated ClaimNAG as ClaimsTOT
	from in.policy_info as p
		inner join out.policy_year as y
			on p.casenumber = y.casenumber
		left join in.loss_info as l
			on p.PolicyNumber=l.policynumber
			and p.casenumber=l.casenumber
		left join mapping.ActofGod_PriorClaims as t
			on l.LossCode=t.ActOfGodCode
	where l.LossAmount>0 and Calculated LossAge >= 0 and p.PolicyTermYears >= 6;

quit;


*temp.PriorClaims_f;;
proc sql;
	create table work.PriorClaims_f as
	select a.casenumber, a.policynumber, a.Group, min(a.LossAge) as Claim_Free
	from work.PriorClaims_e as a
	group by a.PolicyNumber, a.casenumber, a.Group
;
quit;


*out.Tier4_PriorClaims;;
proc sql;
    create table out.ClaimFreeYears as
	select a.CaseNumber, a.PolicyNumber
	, case when f.Claim_Free is null then a.policytermyears
		when f.Claim_Free <= 6 then 0
		when f.Claim_Free > a.policytermyears then a.policytermyears
		when y.policyyear <> 0 and f.Claim_Free < a.policytermyears then f.Claim_Free - y.policyyear
		else f.Claim_Free
		end as Claim_Free_Years
	from in.policy_info as a
	inner join out.policy_year as y
		on a.casenumber = y.casenumber
	left join work.PriorClaims_f as f
		on a.CaseNumber = f.CaseNumber
		and a.PolicyNumber = f.PolicyNumber
    ;
quit;
