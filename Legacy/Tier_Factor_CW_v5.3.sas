
proc sql;
    create table out.Tier_Factor as
    select distinct 
      a.CaseNumber
	, a.PolicyNumber
/*	, a.LengthOfResidenceFactor*/
/*	, c.Factor as ShopperFactor*/
/*	, d.Occupancy_Tier_Factor as OccupancyFactor*/
	, f.Factor as PriorClaimsFactor
	, round(a.LengthOfResidenceFactor * c.Factor * d.Occupancy_Tier_Factor * f.Factor,0.000001) as Tier_Factor
	from out.Tier1_LengthOfResidence as a
/*	inner join out.Tier2_ShopperClass as b*/
/*	on a.CaseNumber = b.CaseNumber*/
/*	inner join Fac.tiershopperfactor c*/
/*	on b.Shopper_Class = c.Class*/
/*	inner join out.Tier3_Occupancy_Tier as d*/
/*	on a.CaseNumber = d.CaseNumber*/
	inner join out.Tier4_PriorClaims as e
	on a.CaseNumber = e.CaseNumber
	inner join Fac.tierpriorclaimsfactor f
	on e.Prior_Claims_Class = f.Class
	;
quit;
