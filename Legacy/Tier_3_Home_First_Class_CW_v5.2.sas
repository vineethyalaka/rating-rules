*Tier 3 Home First Class CW;
*Uses:    	in.hf_info
			(CaseNumber PolicyNumber AutoPolicyNumber PolicyAccountNumber PolicyTermYears PolicyEffectiveDate Ratingversion)
            source.HomeFirst
            (Class State_Abbrev NBVerLower NBVerUpper AccNumMin AccNumMax AutoPolicyFlag)
			;
*Creates: 	out.Tier3_HomeFirstClass;


proc sql;
	create table out.Tier3_HomeFirstClass as
	select distinct a.CaseNumber
		 , a.PolicyNumber, a.PolicyEffectiveDate, a.FirstTerm
		 , a.PolicyAccountNumber
		 , a.AutoPolicyNumber
		 , case when trim(a.AutoPolicyNumber) = "" then "N"
		 	else "Y"
			end as AutoPolicyFlag
		 , a.ratingversion
		 , c.Class as Home_First_Class 
	from in.hf_info as a
	inner join source.HomeFirst as c
	on case when trim(a.AutoPolicyNumber) = "" then "N"
		 	else "Y"
			end = c.AutoPolicyFlag
	and a.PolicyAccountNumber <= c.AccNumMax
	and a.PolicyAccountNumber >= c.AccNumMin
	and a.Ratingversion <= c.NBVerUpper
	and a.Ratingversion >= c.NBVerLower
	and c.state = &statecode
	;
quit;

