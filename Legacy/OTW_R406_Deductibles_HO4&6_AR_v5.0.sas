*HO6 OTW Rule 406 Deductibles CW (IT calculation);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible)
            out.OTW_Tiered_Base_Premium
            (CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
            Fac.OTWDeductibleFactor
            (CoverageDeductible Factor Max_Credit)
			;
*Creates: 	out.OTW_R406_Deductibles;
proc sql;
create table policy_info as
select *
, case 	when CoverageDeductible = '$1000' then 1000
 	 	when CoverageDeductible = '$250/$1000 W&H' then 250
 		when CoverageDeductible = '$250/$500 W&H' then 250
 		when CoverageDeductible = '$2500' then 2500
 		when CoverageDeductible = '$500' then 500
 		when CoverageDeductible = '$500/$1000 W&H' then 500
		else 0
end as Deductible
, case  when CoverageDeductible = '$250/$1000 W&H' then 1000
		when CoverageDeductible = '$250/$500 W&H'  then 500
		else calculated Deductible
 end as WindHailDed

from in.policy_info
 ;
quit;

data OTWDeductibleFactor;
set Fac.OTWDeductibleFactor;
CoverageDeductible=compress(CoverageDeductible,'$');
run;


proc sql;
create table DedFactor as
select *
, case 	when CoverageDeductible = '250' then 250
		when CoverageDeductible = '500' then 500
when CoverageDeductible = '1000' then 1000
when CoverageDeductible = '2500' then 2500
when CoverageDeductible = '100' then 100 
else 0
end as CovDed
, case 	when WindHailDeductible = '250' then 250
when WindHailDeductible = '500' then 500
when WindHailDeductible = '1000' then 1000
when WindHailDeductible = '2500' then 2500
when WindHailDeductible = '100' then 100
when WindHailDeductible = '250' then 250
when WindHailDeductible = '2' then 2
else 0 
end as WindDED
from OTWDeductibleFactor
;
quit;

proc sql;
	create table out.OTW_R406_Deductibles as
	select p.CaseNumber, p.PolicyNumber
		 , df.Factor as Deductible label="Deductible"
		 , df.Max_Credit
		 , case when (1 - df.Factor)*t.OTW_Tiered_Base_Premium > df.Max_Credit 
				then (1 - df.Max_Credit/t.OTW_Tiered_Base_Premium)
			else df.Factor end as OTW_Deductible_Factor
	from Policy_Info as p
	inner join out.OTW_Tiered_Base_Premium as t
	on p.CaseNumber = t.CaseNumber
	inner join DedFactor as df
	on p.Deductible = df.CovDed
	and p.WindHailDed = df.WindDED
	;
quit;
