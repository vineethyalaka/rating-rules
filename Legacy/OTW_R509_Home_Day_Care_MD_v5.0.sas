*OTW Rule 509 Home Day Care Coverage MD;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageD)
            Fac.OTWPropertyLossOfUseFactor
            (BaseCovD Factor)
			;
*Creates: 	out.OTW_R509_Home_Day_Care;

proc sql;
	create table out.OTW_R509_Home_Day_Care as
	select e.CaseNumber, e.PolicyNumber
		 , case when e.DayCare_Flag = "1" then round(e.Day_Care_amt*hdc.Factor/1000,1) else 0 end as Day_Care
	from in.endorsement_info as e
	, Fac.OTWPropertyHomeDayCareFactor as hdc
	;
quit;

