*OTW Rule 486 Renovated Home;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate PropertyYearPlumbingInstalled PropertyYearHeatingInstalled
			PropertyYearWiringInstalled PropertyYearRoofInstalled)
            Fac.OTWRenovatedHomeFactor
            (Factor Years_lower Years_upper)
			;
*Creates: 	out.OTW_R486_Renovated_Home;

proc sql;
	create table work.OTW_R486_Renovated_Home as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.PropertyYearPlumbingInstalled, p.PropertyYearHeatingInstalled,
	p.PropertyYearWiringInstalled,p.PropertyYearRoofInstalled
		 , (case when p.PolicyTermYears=1 and &effst = 0 then year(p.PolicyIssueDate) else year(p.PolicyEffectiveDate) end
	 		- case when p.PropertyYearPlumbingInstalled = . then year(date()) else p.PropertyYearPlumbingInstalled end) as Plumbing_Age
		 , (case when p.PolicyTermYears=1 and &effst = 0 then year(p.PolicyIssueDate) else year(p.PolicyEffectiveDate) end
	 		- case when p.PropertyYearHeatingInstalled = . then year(date()) else p.PropertyYearHeatingInstalled end) as Heating_Age
		 , (case when p.PolicyTermYears=1 and &effst = 0 then year(p.PolicyIssueDate) else year(p.PolicyEffectiveDate) end
	 		- case when p.PropertyYearWiringInstalled = . then year(date()) else p.PropertyYearWiringInstalled end) as Wiring_Age
		 , (case when p.PolicyTermYears=1 and &effst = 0 then year(p.PolicyIssueDate) else year(p.PolicyEffectiveDate) end
	 		- case when p.PropertyYearRoofInstalled = . then year(date()) else p.PropertyYearRoofInstalled end) as Roof_Age
	from in.Policy_Info as p
	;

	create table out.OTW_R486_Renovated as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.Plumbing_Age
		 , ahf.Factor1 as Plumbing_Age_Factor label="Age of Plumbing", p.Heating_Age
		 , bhf.Factor2 as Heating_Age_Factor label="Age of Heating", p.Wiring_Age
		 , chf.Factor3 as Wiring_Age_Factor label="Age of Wiring", p.Roof_Age
		 , dhf.Factor4 as Roof_Age_Factor label="Age of Roof"
		 ,(1-(ahf.Factor1+bhf.Factor2+chf.Factor3+dhf.Factor4)) as Renovated_Factor
	from work.OTW_R486_Renovated_Home as p
	inner join Fac.OTWRenovatedHomeFactor as ahf 
	on  p.Plumbing_Age >= ahf.Years_lower
	and p.Plumbing_Age <=  ahf.Years_upper
	inner join Fac.OTWRenovatedHomeFactor as bhf 
	on  p.Heating_Age >= bhf.Years_lower
	and p.Heating_Age <=  bhf.Years_upper
	inner join Fac.OTWRenovatedHomeFactor as chf 
	on  p.Wiring_Age >= chf.Years_lower
	and p.Wiring_Age <=  chf.Years_upper
	inner join Fac.OTWRenovatedHomeFactor as dhf 
	on  p.Roof_Age >= dhf.Years_lower
	and p.Roof_Age <=  dhf.Years_upper
	;

quit;
