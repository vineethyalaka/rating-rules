*Rule 301 Experience Modifier CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyAccountNumber)
            in.ExperienceFactor
            (Factor Account)
			;
*Creates: 	out.R301_ExperienceFactor;
/*libname source '\\cambosnapp01\PricingData\SAS\Source_Data\03_Data';*/
proc sql;
    create table work.R301_ExperienceModifier as
	select distinct p.CaseNumber, p.PolicyNumber
		  ,p.PolicyAccountNumber
	      , case when m.Description="" then "Others" else m.Description end as Account
	from in.Policy_Info as p
	     left join source.Experiencemodifier as m
		 on p.PolicyAccountNumber <= m.Account_u
		 and p.PolicyAccountNumber >= m.Account_l
	;
quit;


proc sql;
    create table out.R301_ExperienceFactor as
	select distinct m.CaseNumber
		  , m.PolicyNumber
		  , m.PolicyAccountNumber
	      , m.Account
		  , case when m.PolicyAccountNumber between 1088 and 1099 then 1.063 else e.Factor end as Experience
	from work.R301_ExperienceModifier as m
		 inner join Fac.experiencefactor e
		 on m.Account = e.Description
	;
quit;
