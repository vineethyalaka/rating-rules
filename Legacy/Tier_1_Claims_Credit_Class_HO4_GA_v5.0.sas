*HO6 Tier 1 Insurance Score Class CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance)
            in.Production_Info
            (CaseNumber PolicyNumber PriorInsurance)
            in.drc_info
			(CaseNumber PolicyNumber InsScore ScoreModel)
            out.Tier4_PriorClaims
			(CaseNumber PolicyNumber Prior_Claims_Class)
			Fac.TierClaimsCreditClass
			(PriorClaimClass ScoreModel Inslower Insupper Tier)
			Fac.TierClaimsCreditFactor
			(Tier Factor)
			;
*Creates: 	out.Tier2_ClaimsCredit_Tier out.Tier2_ClaimsCredit_Factor;

proc sql;
	create table out.Tier1_ClaimsCredit_Tier as
	select distinct p.CaseNumber, p.PolicyNumber, c.Prior_Claims_Class
		 , d.ScoreModel, d.InsScore, p.Lapse_of_Insurance, pd.PriorInsurance
/*		 , case when p.DOB1 < mdy(1,1,1900) then mdy(1,1,2008) else p.DOB1 end as DOB1a format=mmddyy10.*/
/*		 , case when p.DOB2 < mdy(1,1,1900) then mdy(1,1,2008) else p.DOB2 end as DOB2a format=mmddyy10.*/
/*		 , case when calculated DOB1a > calculated DOB2a then calculated DOB2a else calculated DOB1a end as DOB format=mmddyy10.*/
/*		 , floor((case when p.PolicyTermYears=1 and &effst = 0 then p.PolicyIssueDate else p.PolicyEffectiveDate end - calculated DOB)/365) as Age*/
		 , Year(p.PolicyEffectiveDate)-Year(p.DOB1) as Age
		 , t.Tier as ClaimsCredit_Tier
	from in.Policy_Info as p
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		inner join in.drc_info as d
		on p.CaseNumber = d.CaseNumber
		inner join out.Tier4_PriorClaims as c
		on p.CaseNumber = c.CaseNumber
		left join Fac.TierClaimsCreditClass as t
		on t.Insupper>d.InsScore
		and t.Inslower<=d.InsScore
		and t.ScoreModel=d.ScoreModel
		and t.PriorClaimClass=c.Prior_Claims_Class
	;
quit;



proc sql;
	create table out.Tier1_InsuranceScore as
	select distinct t.CaseNumber, t.PolicyNumber, t.ClaimsCredit_Tier as Insurance_Class, f.Factor as insurance_class_factor
	from out.Tier1_ClaimsCredit_Tier as t
	left join Fac.TierClaimsCreditFactor as f
	on t.ClaimsCredit_Tier = f.Class
	and t.Age <= f.Age_upper
	and t.Age >= f.Age_lower
	;
quit;
