/**Rule 590.3 Windstorm Deductible Adjusted for Minimum Deductible (with Wind Regions depending on distance to shore and terriotry);*/
/**Uses:    	in.Policy_Info*/
/*            (CaseNumber PolicyNumber CoverageA WindHailDeductible PropertyLatitude DistanceToShore TerritoryH)*/
/*            Fac.WindDeductibleFactor*/
/*            (class AOI_Lower AOI_Upper Factor_Lower Factor_Upper Deductible Region)*/
/*			;*/
/**Creates: 	out.Wind_Deductible;*/

proc sql;
	create table work.Wind_Deductible as
	select p.casenumber, p.policynumber
		, p.coverageA
		, p.TerritoryH
		, d.DeductibleTerritory

		, p.WindHailDeductible
		, input(left(p.WindHailDeductible),5.) as WindHailDeductible_num
		, case when calculated WindHailDeductible_num <= 10
		 	then calculated WindHailDeductible_num * CoverageA /100
			else calculated WindHailDeductible_num
			end as WindHailDed_Dollar

		, p.HurricaneDeductible
		, input(left(p.HurricaneDeductible),5.) as HurricaneDeductible_num
		, case when calculated HurricaneDeductible_num <= 10
		 	then calculated HurricaneDeductible_num * CoverageA /100
			else calculated HurricaneDeductible_num
			end as HurrDed_Dollar

from in.Policy_Info as p
	left join fac.winddeductibleterritory as d
	on p.TerritoryH = d.Territory
	;
quit;
	
proc sql;
	    create table out.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber
		, p.coverageA
		, p.TerritoryH
		, p.DeductibleTerritory

		, p.WindHailDeductible
		, p.WindHailDeductible_num
		, p.WindHailDed_Dollar

		, p.HurricaneDeductible
		, p.HurricaneDeductible_num
		, p.HurrDed_Dollar


		, df.AOI_Lower
		, df.AOI_Upper
		, df.Factor_Lower
		, df.Factor_Upper

		, (p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
		, (df.AOI_Upper - p.CoverageA)/(df.AOI_Upper - df.AOI_Lower) as D2
		, (calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind
		, round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind
			+ (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 - calculated Ind)
	                , 0.001) as Wind_Deductible label="Wind Deductible"

		from work.Wind_Deductible as p
		inner join Fac.WindDeductibleFactor as df
		on p.WindHailDeductible_num = df.WindHailDeductible
		and p.HurricaneDeductible_num = df.HurricaneDeductible
		and p.CoverageA <= df.AOI_Upper
		and p.CoverageA > df.AOI_Lower
		and p.DeductibleTerritory = df.DeductibleTerritory
		;
quit;

