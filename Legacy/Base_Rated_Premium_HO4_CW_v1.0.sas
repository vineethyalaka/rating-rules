*HO4 Rule created to Match IT "BaseRatedPremium" field in v3ratingresult;
*Uses:    	out.OTW_Tiered_Base_Premium
            (CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
            out.Wind_Base_Premium
            (CaseNumber Wind_Base_Premium)
			fac.OTWPolicyFeeFactor
			(factor)
			;
*Creates: 	out.Base_Rated_Premium;

proc sql;
    create table out.Base_Rated_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , a.OTW_Tiered_Base_Premium
		 , d.Wind_Base_Premium
		 , e.factor as PolicyFee label = "PolicyFee"
		 , round(a.OTW_Tiered_Base_Premium + d.Wind_Base_Premium + e.Factor,1) 
			as Base_Rated_Premium
	from fac.OTWPolicyFeeFactor as e
	, out.OTW_Tiered_Base_Premium as a 
	inner join out.Wind_Base_Premium as d 
	on a.CaseNumber = d.CaseNumber
	;
quit;
