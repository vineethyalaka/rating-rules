*Rule 590.5 Hurricane Premium AL;
*Uses:    	out.Hurr_Rating_Factors_Premium
            (CaseNumber PolicyNumber Wind_Rating_Factors_Premium)
            out.Wind_Property_Premium
            (CaseNumber PolicyNumber Wind_Property_Premium)
			;
*Creates: 	out.Hurr_Premium;

proc sql;
    create table out.Hurr_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , (1-e.exwind)*round(a.Hurr_Rating_Factors_Premium /*+ Wind_Property_Premium*/) as Hurr_Premium
	from out.Hurr_Rating_Factors_Premium as a 
	inner join out.Wind_Property_Premium as b 
	on a.CaseNumber = b.CaseNumber
	inner join in.endorsement_info as e
	on a.CaseNumber = e.CaseNumber
	;
quit;
