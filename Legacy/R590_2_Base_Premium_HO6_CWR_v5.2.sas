*HO6 Rule 590.2 Wind Base Premium CW;
*Uses:    	out.Wind_Territory_Rate
            (CaseNumber PolicyNumber Wind_Territory_Rate)
            out.Wind_Form_Factor
            (CaseNumber PolicyNumber Wind_Policy_Form)
            out.Wind_AOI_Factor
            (CaseNumber PolicyNumber Wind_AOI)
			;
*Creates: 	out.Wind_Base_Premium;

proc sql;
    create table out.Wind_Base_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , round(a.Wind_Territory_Rate * b.Wind_Policy_Form, .01) as Wind_Policy_Form
		 , round(calculated Wind_Policy_Form * d.Wind_AOI, .01) as Wind_AOI
		 , calculated Wind_AOI as Wind_Base_Premium
	from out.Wind_Territory_Rate as a 
	inner join out.Wind_Form_Factor as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Wind_AOI_Factor as d 
	on a.CaseNumber = d.CaseNumber
	;
quit;
