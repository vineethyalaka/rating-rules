*Rule 593 Tenants Relocation Expense MA;
*Uses:		in.DataMAinfo
			(CaseNumber PolicyNumber PropertyUnitsInBuilding Tenants_Relocation)
			Fac.otwpropertyrelocationfactor
			(TenRelocation MutiFamily Factor);

proc sql;
	create table out.OTW_R593_Tenants_Relocation as
	select distinct p.CaseNumber, p.PolicyNumber, q.Factor as Tenants
	from in.DataMAinfo as p
	inner join Fac.otwpropertyrelocationfactor as q
	on p.PropertyUnitsInBuilding = q.MultiFamily
		and p.Tenants_Relocation = q.TenRelocation
	;
quit;
