*OTW Rule 471 Roofing Age/Materials CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate PropertyYearRoofInstalled PropertyRoofTypeCode)
            Fac.OTWRoofFactor
            (Factor Material)
			;
*Creates: 	out.OTW_R471_Roofing_Age_Materials;

proc sql;
	create table work.OTW_R471_Roofing_Age_Materials as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
		 , (case when p.PolicyTermYears=1 then year(p.PolicyIssueDate) else year(p.PolicyEffectiveDate) end
	 		- case when p.PropertyYearRoofInstalled <= 0 then year(date()) else p.PropertyYearRoofInstalled end) as Roof_Age
	from in.Policy_Info as p
	; 

	create table out.OTW_R471_Roofing_Age_Materials as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
		 , rf.Factor as Roof_Age_Materials label="Roof Age Materials"
	from work.OTW_R471_Roofing_Age_Materials as p
	inner join Fac.OTWRoofFactor as rf
	on  lower(trim(left(rf.Material))) = 
		case when lower(trim(left(p.PropertyRoofTypeCode))) in 
				("asphalt shingles","composition shingles","clay tile","concrete tile","slate"
				,"spanish tile","tar or gravel","tin/membrane","wood shake","wood shingles")
			 then lower(trim(left(p.PropertyRoofTypeCode))) 
			 else "other" 
		end
	and p.Roof_Age <= rf.Age_upper
	and p.Roof_age >= rf.Age_lower
	;
quit;
