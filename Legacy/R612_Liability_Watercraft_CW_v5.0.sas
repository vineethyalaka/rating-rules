*Rule 612 Liability outboard motors and Watercraft CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF)
            in.Endorsement_Info
            (CaseNumber PolicyNumber HO_2475_IN_USE_TO_MONTH_01 HO_2475_IN_USE_FROM_MONTH_01 HO_2475_HORSEPOWER_SPEED_01 HO_2475_BOATTYPE_01)
            out.LiabilityWatercraftCovEFactor
            (Factor Speed_upper Speed_lower MotorType CoverageE)
            Fac.LiabilityWatercraftCovFFactor
            (Factor Speed_upper Speed_lower MotorType CoverageF)
			;
*Creates: 	out.Liability_Watercraft;

proc sql;
    create table out.Liability_Watercraft as
	select distinct p.CaseNumber, p.PolicyNumber
		 , round(wce.Factor*(abs(e.HO_2475_IN_USE_TO_MONTH_01 - e.HO_2475_IN_USE_FROM_MONTH_01)+1)/12,1) as Watercraft_E
		 , round(wcf.Factor*(abs(e.HO_2475_IN_USE_TO_MONTH_01 - e.HO_2475_IN_USE_FROM_MONTH_01)+1)/12,1) as Watercraft_F
	from in.Policy_Info as p
	inner join in.Endorsement_Info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.LiabilityWatercraftCovEFactor as wce
	on e.HO_2475_HORSEPOWER_SPEED_01 < wce.Speed_upper
	and e.HO_2475_HORSEPOWER_SPEED_01 >= wce.Speed_lower
	and case when e.HO_2475_BOATTYPE_01 = "" then "none" else lower(e.HO_2475_BOATTYPE_01) end = wce.MotorType
	and p.CoverageE = wce.CoverageE
	inner join Fac.LiabilityWatercraftCovFFactor as wcf
	on e.HO_2475_HORSEPOWER_SPEED_01 < wcf.Speed_upper
	and e.HO_2475_HORSEPOWER_SPEED_01 >= wcf.Speed_lower
	and case when e.HO_2475_BOATTYPE_01 = "" then "none" else lower(e.HO_2475_BOATTYPE_01) end = wcf.MotorType
	and p.CoverageF = wcf.CoverageF
	;
quit;
