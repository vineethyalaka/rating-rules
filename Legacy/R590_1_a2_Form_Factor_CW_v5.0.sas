*Rule 590.1.a2 Form Factor CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber)
            Fac.WindFormFactor
            (Factor)
			;
*Creates: 	out.Wind_Form_Factor;

proc sql;
    create table out.Wind_Form_Factor as
	select distinct p.CaseNumber, p.PolicyNumber, ff.Factor as Wind_Policy_Form label="Wind Policy Form Factor"
	from in.policy_info as p, Fac.WindFormFactor as ff
	;
quit;
