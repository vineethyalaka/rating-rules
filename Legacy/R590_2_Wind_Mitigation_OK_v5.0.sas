** RULE 590. D Loss Mitigation Discounts **;
** For OK ONLY **
**=====================================================================================**
History: 2019 01 18 TM Initial Draft
**=====================================================================================*;
proc sql;
	create table out.Wind_R590_Mitigation as
	select p.CaseNUmber, p.PolicyNumber,
    case when p.construction_completed =0 then 1  else f.factor end as wind_mitigation

	from in.production_info p, fac.windmitigation f
	;
quit;