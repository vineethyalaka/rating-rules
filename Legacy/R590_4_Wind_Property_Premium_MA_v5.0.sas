*Rule 590.4 Wind Property Premium MA;
*Uses:    	out.Wind_R503_Business_Property
			(CaseNumber PolicyNumber Business_Property)
			out.Wind_R511_Loss_Assessment
			(CaseNumber PolicyNumber Loss_Assessment)
			out.Wind_R512_Loss_of_Use
			(CaseNumber PolicyNumber Coverage_D)
			out.Wind_R514_Other_Structures
			(CaseNumber PolicyNumber Other_Structures)
			out.Wind_R515_A_Incr_CovC
			(CaseNumber PolicyNumber Coverage_C)
			out.Wind_R515_C_Unsched_Incr_CovC
			(CaseNumber PolicyNumber Unscheduled_Personal_Property)
			out.Coastal_Premium
			(CaseNumber PolicyNumber DistanceToShore CoverageA Coastal_Premium)
			;
*Creates: 	out.Wind_Property_Premium;

proc sql;
    create table out.Wind_Property_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Business_Property
		 , b.Loss_Assessment
		 , c.Coverage_D
		 , d.Other_Structures
		 , e.Coverage_C
		 , f.Unscheduled_Personal_Property
		 , g.Coastal_Premium
		 , a.Business_Property
			+ b.Loss_Assessment
			+ c.Coverage_D
			+ d.Other_Structures
			+ e.Coverage_C
			+ f.Unscheduled_Personal_Property
			+ g.Coastal_Premium
			as Wind_Property_Premium format=10.4
	from out.Wind_R503_Business_Property as a 
	inner join out.Wind_R511_Loss_Assessment as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Wind_R512_Loss_of_Use as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.Wind_R514_Other_Structures as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Wind_R515_A_Incr_CovC as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.Wind_R515_C_Unsched_Incr_CovC as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.Coastal_Premium as g
	on a.CaseNumber = g.CaseNumber
	;
quit;
