*HO6 Rule 301.1.a4 AOI CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber CoverageC)
            Fac.OTWAOIFactor
            (AOI_Lower AOI_Upper Factor_Lower Factor_Upper)
			;
*Creates: 	out.OTW_AOI_Factor;

proc sql;
    create table out.OTW_AOI_Factor as
	select  distinct p.CaseNumber, p.PolicyNumber, p.CoverageC, f.AOI_Lower, f.AOI_Upper, f.Factor_Lower, f.Factor_Upper
	      ,(p.CoverageC - f.AOI_Lower)/(f.AOI_Upper - f.AOI_Lower) as I1
		  ,(f.AOI_Upper - p.CoverageC)/(f.AOI_Upper - f.AOI_Lower) as I2
		  ,(calculated I1 + abs(calculated I1))/(2*calculated I1) as Ind
		  ,round((f.Factor_Upper*calculated I1 + f.Factor_Lower*calculated I2)*calculated Ind
		          + (f.Factor_Lower + f.Factor_Upper*(p.CoverageC - f.AOI_Lower)/1000)*(1 - calculated Ind)
                , 0.001) as OTW_AOI label="OTW AOI Factor"
	from in.policy_info as p
	inner join Fac.OTWAOIFactor as f
	on (p.CoverageC <= f.AOI_Upper or f.AOI_Upper=0) 
	and p.CoverageC > f.AOI_Lower
	;
quit;
