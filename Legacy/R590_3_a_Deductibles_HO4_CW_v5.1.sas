*HO4 Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible)
            out.Wind_Base_Premium
            (CaseNumber PolicyNumber Wind_Base_Premium)
            Fac.WindDeductibleFactor
            (CoverageDeductible Factor Max_Credit)
			;
*Creates: 	out.Wind_Deductible;
*If p.WindHailDeductible is null then use Coverage deductible;

proc sql;
	create table out.Wind_Deductible as
	select distinct p.CaseNumber, p.PolicyNumber
		 , df.Factor as Deductible label="Deductible"
		 , df.Max_Credit
		 , case when (1 - df.Factor)*t.Wind_Base_Premium > df.Max_Credit then (1 - df.Max_Credit/t.Wind_Base_Premium)
			else df.Factor end as Wind_Deductible
	from in.Policy_Info as p
	inner join out.Wind_Base_Premium as t
	on p.CaseNumber = t.CaseNumber
	inner join Fac.WindDeductibleFactor as df
	on case when p.WindHailDeductible is null then left(put(input(p.CoverageDeductible,dollar8.),8.))
			when substr(p.WindHailDeductible,1,1) = "$" then left(substr(p.WindHailDeductible,2,5)) 
			else left(p.WindHailDeductible) end 
	   = left(df.CoverageDeductible)
	;
quit;
