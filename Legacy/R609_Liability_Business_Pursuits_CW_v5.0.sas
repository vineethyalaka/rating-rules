*Rule 609 Liability Business Pursuits CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF)
            in.Endorsement_Info
            (CaseNumber PolicyNumber Business_Pursuits)
            Fac.LiabilityBusinessPursuitsEFactor
            (Factor CoverageE)
            Fac.LiabilityBusinessPursuitsFFactor
            (Factor CoverageF)
			;
*Creates: 	out.Liability_Business_Pursuits;

proc sql;
    create table out.Liability_Business_Pursuits as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when e.Business_Pursuits = "1" then bpe.Factor else 0 end as Business_Pursuits_E
		 , case when e.Business_Pursuits = "1" then bpf.Factor else 0 end as Business_Pursuits_F
	from in.Policy_Info as p
	inner join in.Endorsement_Info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.LiabilityBusinessPursuitsEFactor as bpe
	on p.CoverageE = bpe.CoverageE
	inner join Fac.LiabilityBusinessPursuitsFFactor as bpf
	on p.CoverageF = bpf.CoverageF
	;
quit;
