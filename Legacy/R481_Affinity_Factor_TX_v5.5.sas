
proc sql;
      create table out.R481_Affinity as
      select distinct a.CaseNumber, a.PolicyNumber, a.PolicyAccountNumber, a.partnerflag, AutoPolicyNumber, AutoPolicyFlag,
      case 
	        when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 1 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

			when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 1 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/


			when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

			when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

			when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

			when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 1 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

			when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.95 /*Auto Partner*/
            when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 1 /*Affiliate Auto Discounts*/

			when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 1 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

			when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.95 /*Auto Partner*/
            when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 1 /*Affiliate Auto Discounts*/

			when a.PolicyAccountNumber>=52400 and a.PolicyAccountNumber<=52499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=52400 and a.PolicyAccountNumber<=52499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 1 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=52400 and a.PolicyAccountNumber<=52499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=52400 and a.PolicyAccountNumber<=52499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

			when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.95 /*Auto Partner*/
            when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 1 /*Affiliate Auto Discounts*/




			/*AmFam.com account range has been changed from (51100, 51101) to (51100, 51109), accoding to ITR 11209*/


          else b.Factor end as Affinity label="Affinity"
      from in.policy_info as a
      left join Fac.AffinityFactor as b
      on a.PolicyAccountNumber <= b.AccNumMax
      and a.PolicyAccountNumber >= b.AccNumMin
      and (a.AutoPolicyNumber is null and b.AutoPolicyFlag="N"
            or a.AutoPolicyNumber is not null and b.AutoPolicyFlag="Y")
      ;
quit;
