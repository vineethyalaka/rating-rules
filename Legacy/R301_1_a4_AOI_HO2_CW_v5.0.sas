*Rule 301.1.a4 AOI CW (HO2);
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber orig_RPL_Cc)
            Fac.OTWAOIFactor
            (AOI_Lower AOI_Upper Factor_Lower Factor_Upper)
			;
*Creates: 	out.Coverage out.OTW_AOI_Factor;

proc sql;
    create table out.Coverage as
	select  distinct p.CaseNumber, p.PolicyNumber, round(input(p.orig_RPL_Cc,8.)*0.8,1000) as Coverage
	from in.policy_info as p
	;
quit;
proc sql;
    create table out.OTW_AOI_Factor as
	select  distinct t.CaseNumber, t.PolicyNumber, t.Coverage, f.AOI_Lower, f.AOI_Upper, f.Factor_Lower, f.Factor_Upper
	      ,(t.Coverage - f.AOI_Lower)/(f.AOI_Upper - f.AOI_Lower) as I1
		  ,(f.AOI_Upper - t.Coverage)/(f.AOI_Upper - f.AOI_Lower) as I2
		  ,(calculated I1 + abs(calculated I1))/(2*calculated I1) as Ind
		  ,round((f.Factor_Upper*calculated I1 + f.Factor_Lower*calculated I2)*calculated Ind
		          + (f.Factor_Lower + f.Factor_Upper*(t.Coverage - f.AOI_Lower)/10000)*(1 - calculated Ind)
                , 0.001) as OTW_AOI label="OTW AOI Factor"
	from out.Coverage as t
	inner join Fac.OTWAOIFactor as f
	on (t.Coverage <= f.AOI_Upper or f.AOI_Upper=0) 
	and t.Coverage > f.AOI_Lower
	;
quit;