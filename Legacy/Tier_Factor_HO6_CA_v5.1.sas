*HO6 Tier Factor CA;
/*2019/02/12 TM add prior claim factor ITR 13759 */
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class insurance_class_factor)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier Occupancy_Tier_Factor)
			;
*Creates: 	out.Tier_Factor;


proc sql;
    create table out.Tier_Factor as
	select distinct 
           o.CaseNumber, o.PolicyNumber
          ,o.Occupancy_Tier, o.Occupancy_Tier_Factor
		  ,f.Factor as PriorClaimsFactor
		  ,round(o.Occupancy_Tier_Factor * f.Factor,0.000001) as Tier_Factor /*add prior claim factor*/
    from out.Tier3_Occupancy_Tier as o
	inner join out.Tier4_PriorClaims as e
	on o.CaseNumber = e.CaseNumber
	inner join Fac.tierpriorclaimsfactor as f
	on e.Prior_Claims_Class = f.Class
	;
quit;
