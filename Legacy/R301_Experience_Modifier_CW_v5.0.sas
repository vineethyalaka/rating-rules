*Rule 301 Experience Modifier CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyAccountNumber)
            in.ExperienceFactor
            (Factor Account)
			;
*Creates: 	out.R301_ExperienceFactor;

proc sql;
    create table out.R301_ExperienceFactor as
	select distinct p.CaseNumber, p.PolicyNumber
	      ,case when f.Factor=. then 1 else f.Factor end as Experience
	from in.Policy_Info as p
	     left join in.ExperienceFactor as f
		 on p.PolicyAccountNumber = f.Account
	;
quit;
