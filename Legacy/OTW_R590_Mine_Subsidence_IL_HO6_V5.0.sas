*OTW Rule 590 Mine Subsidence;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO2398_FLAG)
            in.Policy_Info
            (CaseNumber PolicyNumber CoverageA)
            Fac.OTWPropertyMineSubsidenceFactor
            (AOI_lower AOI_upper Factor)
			;
*Creates: 	out.OTW_590_Mine_Subsidence;


proc sql;
	create table out.OTW_590_Mine_Subsidence as
	select e.CaseNumber, e.PolicyNumber
		 , e.HO2398_FLAG
		 , p.CoverageA
		 , case when e.HO2398_FLAG="1" or e.HO2388_FLAG="1" 
			then round(ms.Factor,1) else 0 end as Mine_Subsidence
	from Fac.OTWPropertyMineSubsidenceFactor as ms, in.Endorsement_Info as e
		 inner join in.Policy_Info as p
		 on e.CaseNumber = p.CaseNumber
	;
quit;
