*HO6 Wind Rule 403 Coverage C (Personal property) Replacement Cost Coverage CW (HO-04-90);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0490_FLAG)
            Fac.WindHO0490Factor
            (Factor)
			;
*Creates: 	out.Wind_R403_CovC_Replacement_Cost;

proc sql;
	create table out.Wind_R403_CovC_Replacement_Cost as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0490_FLAG = '1' then hf490.Factor else 1 end as HO_04_90 label="Wind HO-04-90"
	from Fac.WindHO0490Factor as hf490
    , in.Endorsement_Info as e
	;
quit;
