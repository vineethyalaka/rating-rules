

proc sql;
	create table out.R481_Affinity as
	select distinct a.CaseNumber, a.PolicyNumber, a.PolicyAccountNumber, a.Life, b.AutoPolicyFlag,
		case 
		when a.PolicyAccountNumber = 10005 then 0.95 
/*		when a.PolicyAccountNumber <=1099 and a.PolicyAccountNumber >=1088 then 0.95*/
/*		when a.PolicyAccountNumber <=5001 and a.PolicyAccountNumber >=5000 then 0.95*/


		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.9 
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.9 


        when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
        when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
        when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
        when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
        when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 1 /*Non-Auto Partner*/
        when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
        when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

		/*AmFam.com account range has been changed from (51100, 51101) to (51100, 51109), accoding to ITR 11209*/

		when a.PolicyAccountNumber>=5000 and a.PolicyAccountNumber<=5001 and a.AutoPolicyNumber is null and a.Life is not null then 0.95   
		when a.PolicyAccountNumber>=5000 and a.PolicyAccountNumber<=5001 and a.AutoPolicyNumber is not null then 0.90


 		else b.Factor end as Affinity label="Affinity"
	from in.policy_info as a
	left join Fac.AffinityFactor as b
	on a.PolicyAccountNumber <= b.AccNumMax
	and a.PolicyAccountNumber >= b.AccNumMin
	and (a.AutoPolicyNumber is null and a.Life is null and a.Mortgage is null and b.AutoPolicyFlag="N"
	    or a.AutoPolicyNumber is null and a.Life is not null and b.AutoPolicyFlag="N"
		or a.AutoPolicyNumber is not null and b.AutoPolicyFlag="Y"
		
/*/*		or a.Life is not null and b.AutoPolicyFlag="Y"*/
		or a.Mortgage is not null and b.AutoPolicyFlag="Y"
	)
	;
quit;