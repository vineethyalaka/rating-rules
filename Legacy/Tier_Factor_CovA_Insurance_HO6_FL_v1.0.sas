proc sql;
	create table out.Tier_Factor_CovA_Insurance as
	select a.CaseNumber, a.PolicyNumber, a.Insurance_Class
	, b.Factor
	from out.Tier_InsuranceScore as a
	inner join fac.tiercovainsurancefactor as b
	on a.Insurance_Class = b.Tier
	;
quit;
