*Liability Rule 618 Limited Fungi Coverage - FL;
*Uses:    	in.Production_Info
			(CaseNumber PolicyNumber Liability_Amount)
			Fac.LiabilityFungiFactor
			(Coverage Factor)
			;
*Creates: 	out.Liability_Limited_Fungi;
/*May need to be edited to include endorsement flag and/or to change source of limit field*/

proc sql;
	create table out.Liability_Limited_Fungi as
	select 
		  a.CaseNumber
		, a.PolicyNumber
		, b.Rate as Limited_Fungi
	from in.Production_Info as a
	inner join Fac.LiabilityFungiFactor as b
		on a.Liability_Amount = b.Coverage
	;
quit;
