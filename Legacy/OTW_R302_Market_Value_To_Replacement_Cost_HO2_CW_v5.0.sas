*OTW Rule 302 Market Value to Replacement Cost (HO2 only);
*Uses:    	in.policy_Info
            (CaseNumber PolicyNumber PropertyMarketValue PropertyReplacementCost)
            Fac.OTWMVRCFactor
            (Ratio_Lower Ratio_Upper Factor)
			;
*Creates: 	out.OTW_R302_MV_To_RC;

proc sql;
/*	create table work.MVRC as*/
/*	select p.CaseNumber, p.PolicyNumber, p.PropertyMarketValue, p.PropertyReplacementCost*/
/*		 , round(p.PropertyMarketValue/p.PropertyReplacementCost,0.01) as MVRC*/
/*	from in.policy_Info as p*/
/*	;*/
	create table work.MVRC as
	select distinct p.CaseNumber, p.PolicyNumber, p.orig_RPL_cc, p.orig_Mkt_Val
		 , round(input(p.orig_Mkt_Val,8.)/input(p.orig_RPL_cc,8.),0.01) as MVRC
	from in.policy_Info as p
	;

	create table out.OTW_R302_MV_To_RC as
	select distinct p.CaseNumber, p.PolicyNumber, p.MVRC
		 , f.factor as OTW_MVRC
	from work.MVRC as p
	left join Fac.OTWMVRCFactor as f
	on p.MVRC >= f.ratio_lower
	and p.MVRC <= f.ratio_upper
	;
quit;
