*OTW Rule 512 Loss of Use - Increase Coverage D CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageD)
            Fac.OTWPropertyLossOfUseFactor
            (BaseCovD Factor)
			;
*Creates: 	out.OTW_R512_Loss_of_Use;

proc sql;
	create table out.OTW_R512_Loss_of_Use as
	select p.CaseNumber, p.PolicyNumber
		 , case when &Broll_CovD = 1 and p.policyaccountnumber in (14300,14310) and p.policytermyears = 1 then 0 
		   when p.CoverageD <= p.CoverageA*lou.BaseCovD then 0
		   else (p.CoverageD - p.CoverageA*lou.BaseCovD) end as IncrCovD
		 , round(calculated IncrCovD/1000*lou.Factor,1) as Coverage_D
	from Fac.OTWPropertyLossOfUseFactor as lou
		, in.Policy_Info as p
	;
quit;
