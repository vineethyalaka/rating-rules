/* doesn't round AOI or Key Premium */

proc sql;
    create table out.Base_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.OTW_Territory_Rate
		 , b.OTW_Policy_Form
		 , c.OTW_Prot_Constr
		 , d.Wind_Exclusion
		 , a.OTW_Territory_Rate*b.OTW_Policy_Form*c.OTW_Prot_Constr-d.Wind_Exclusion as Key_Premium
		 , e.OTW_AOI
		 , f.OTW_HO_04_56
		 , g.OTW_Ordinance_or_Law
		 , h.OTW_HH_00_15
		 , i.Cov_C AS BaseCovC
		 , i.OTW_Multiple_Families_Factor
		 , round(calculated Key_Premium*e.OTW_AOI*f.OTW_HO_04_56*g.OTW_Ordinance_or_Law*h.OTW_HH_00_15*OTW_Multiple_Families_Factor,1)
			as Base_Premium

	from out.OTW_Territory_Rate as a
	inner join out.OTW_Form_Factor as b
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_Prot_Constr_Factor as c
	on a.CaseNumber = c.CaseNumber
	inner join out.Wind_Exclusion_Factor as d
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_AOI_Factor as e
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_Loss_Settlement_Factor as f
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_Ordinance_or_Law_Factor as g
	on a.CaseNumber = g.CaseNumber
	inner join out.OTW_HH_00_15_Factor as h
	on a.CaseNumber = h.CaseNumber
	inner join out.OTW_Multiple_Families_Factor as i
	on a.CaseNumber = i.CaseNumber
	;
quit;
