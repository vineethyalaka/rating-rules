*OTW Rule 514 Other Sructures CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0448_Flag HO_0448_ADDL_LIMIT_01)
            in.Policy_Info
            (CaseNumber CoverageA CoverageB)
			out.OTW_Multiple_Families_Factor
			(CaseNumber Cov_B)
            Fac.OTWPropertyOtherStructuresFactor
            (Factor1, Factor2)
			;
*Creates: 	out.OTW_R514_Other_Structures;

proc sql;
	create table out.OTW_R514_Other_Structures as
	select distinct e.CaseNumber,e.PolicyNumber
		 , round(p.CoverageB - p.CoverageA*b.Cov_B,1) as IncrCovB
		 , case when calculated IncrCovB < 0 then round(calculated IncrCovB*os.Factor2/1000,1) else 0 end as Other_Structures_1
		 , case when e.HO_0448_Flag="1" then round(e.HO_0448_ADDL_LIMIT_01*os.Factor1/1000,1) else 0 end as Other_Structures_2
		 , calculated Other_Structures_1 + calculated Other_Structures_2 as Other_Structures
	from Fac.OTWPropertyOtherStructuresFactor as os, in.Endorsement_Info as e
	inner join in.Policy_Info as p
	on e.CaseNumber = p.CaseNumber
	inner join out.OTW_Multiple_Families_Factor as b
	on e.CaseNumber = b.CaseNumber
	;
quit;
