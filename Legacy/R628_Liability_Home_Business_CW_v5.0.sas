*Rule 628 Liability Home Business TX - APEX for other states;
*Uses:    	in.endorsement_info
			(CaseNumber, PolicyNumber, Home_Bus_Flag, Home_Bus_Type, Home_Bus_Receipt, Home_Bus_Visits)
			in.Policy_Info
            (CoverageE CoverageF)
            out.LiabilityHomeBusCovEFactor
            (Factor CovE)
            Fac.LiabilityHomeBusCovFFactor
            (Factor CovF)
			;
*Creates: 	out.Liability_Home_Business;

proc sql;
	create table out.Liability_Home_Business
	as select e.CaseNumber, e.PolicyNumber, e.Home_Bus_Flag, e.Home_Bus_Type, e.Home_Bus_Receipt, e.Home_Bus_Visits
		, p.CoverageE, p.CoverageF
		, hbe.Business_Type	as Business_Type_E
		, hbe.Min_Receipts 	as Min_Receipts_E
		, hbe.Max_Receipts 	as Max_Receipts_E
		, hbe.Min_Visits 	as Min_Visits_E
		, hbe.Max_Visits 	as Max_Visits_E
		, hbf.Business_Type	as Business_Type_F
		, hbf.Min_Receipts 	as Min_Receipts_F
		, hbf.Max_Receipts 	as Max_Receipts_F
		, hbf.Min_Visits 	as Min_Visits_F
		, hbf.Max_Visits 	as Max_Visits_F

		, case when Home_Bus_Flag = 0 then 0 else hbe.Factor end as Home_Bus_E
		, case when Home_Bus_Flag = 0 then 0 else hbf.Factor end as Home_Bus_F

	from in.Policy_Info as p
	inner join in.endorsement_info as e on p.casenumber = e.casenumber
	left join Fac.LiabilityHomeBusCovEFactor as hbe
	on p.CoverageE = hbe.CovE and (e.Home_Bus_Type = hbe.Business_Type
					 and e.Home_Bus_Receipt >= hbe.Min_Receipts
					 and e.Home_Bus_Receipt <= hbe.Max_Receipts
					 and e.Home_Bus_Visits 	>= hbe.Min_Visits
					 and e.Home_Bus_Visits 	<= hbe.Max_Visits)
	left join Fac.LiabilityHomeBusCovFFactor as hbf
	on p.CoverageF = hbf.CovF and (e.Home_Bus_Type = hbf.Business_Type
					 and e.Home_Bus_Receipt >= hbf.Min_Receipts
					 and e.Home_Bus_Receipt <= hbf.Max_Receipts
					 and e.Home_Bus_Visits 	>= hbf.Min_Visits
					 and e.Home_Bus_Visits 	<= hbf.Max_Visits)
;
quit;
run;