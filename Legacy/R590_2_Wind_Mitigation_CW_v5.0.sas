

proc sql;
    create table out.Wind_R590_Mitigation as
	select distinct p.CaseNumber, p.PolicyNumber
	, wm1.Factor as WM_RoofAttachment
	, wm2.Factor as WM_RoofConnection
	, wm3.Factor as WM_RoofCover
	, wm4.Factor as WM_RoofShape
	, wm5.Factor as WM_SecondaryWater
	, wm6.Factor as WM_Shutters
	, wm7.Factor as WM_WallRestraint
	, round(wm1.Factor*wm2.Factor*wm3.Factor*wm4.Factor*wm5.Factor*wm6.Factor*wm7.Factor,0.001) as Wind_Mitigation

	from in.Production_Info as p
	inner join Fac.WindMitigationRoofAttachment as wm1 
    on p.RoofAttachment = wm1.Code
	inner join Fac.WindMitigationRoofConnection as wm2 
    on p.RooftoWallConnection = wm2.Code
	inner join Fac.WindMitigationRoofCover as wm3 
    on p.RoofCovering = wm3.Code
	inner join Fac.WindMitigationRoofShape as wm4 
    on p.RoofShape = wm4.Code
	inner join Fac.WindMitigationSecondaryWater as wm5 
    on p.SecondaryWaterResistance = wm5.Code
	inner join Fac.WindMitigationShutters as wm6 
    on p.Shutters = wm6.Code
	inner join Fac.WindMitigationWallRestraint as wm7 
    on p.WalltoFoundationRestraint = wm7.Code
	;
quit;
