*HO4 Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible)
            out.Wind_Base_Premium
            (CaseNumber PolicyNumber Wind_Base_Premium)
            Fac.WindDeductibleFactor
            (CoverageDeductible Factor Max_Credit)
			;
*Creates: 	out.Wind_Deductible;

%macro AOPDed;
	%if &AOPDedAdj46 = 1 %then %do;
		proc sql;
			create table work.Wind_Deductible as
			select distinct p.CaseNumber, p.PolicyNumber
				 , case when p.CoverageDeductible is null then 0
						when substr(p.CoverageDeductible,1,1) = "$" then input(substr(p.CoverageDeductible,2,5),4.) 
						else input(p.CoverageDeductible,4.) end as CoverageDeductible_num
				 , case when calculated CoverageDeductible_num < &minAOP46 then &minAOP46
					else calculated CoverageDeductible_num
					end as CoverageDeductible_adj
			from in.Policy_Info as p
			;
		quit;
	%end;
	%if &AOPDedAdj46 = 0 %then %do;
		proc sql;
			create table work.Wind_Deductible as
			select distinct p.CaseNumber, p.PolicyNumber
				 , case when p.CoverageDeductible is null then 0
						when substr(p.CoverageDeductible,1,1) = "$" then input(substr(p.CoverageDeductible,2,5),4.) 
						else input(p.CoverageDeductible,4.) end as CoverageDeductible_adj
			from in.Policy_Info as p
			;
		quit;
	%end;
		proc sql;
			create table out.Wind_Deductible as
			select distinct p.CaseNumber, p.PolicyNumber
				 , p.CoverageDeductible_adj
				 , df.Max_Credit
				 , case when (1 - df.Factor)*t.Wind_Base_Premium > df.Max_Credit then (1 - df.Max_Credit/t.Wind_Base_Premium)
					else df.Factor end as Wind_Deductible
			from work.Wind_Deductible as p
			inner join out.Wind_Base_Premium as t
			on p.CaseNumber = t.CaseNumber
			inner join Fac.WindDeductibleFactor as df
			on CoverageDeductible_adj = df.CoverageDeductible
			;
		quit;
%mend;
%AOPDed;
