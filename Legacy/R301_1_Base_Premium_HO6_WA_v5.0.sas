*HO6 Rule 301.1 OTW Base Premium CW;
*Uses:    	out.OTW_Territory_Rate
            (CaseNumber PolicyNumber OTW_Territory_Rate)
            out.OTW_Form_Factor
            (CaseNumber PolicyNumber OTW_Policy_Form)
            out.OTW_AOI_Factor
            (CaseNumber PolicyNumber OTW_AOI)
            out.OTW_HH_17_31_Factor
            (CaseNumber PolicyNumber OTW_HH_17_31)
			;
*Creates: 	out.OTW_Base_Premium;

proc sql;
    create table out.OTW_Base_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.OTW_Territory_Rate
		 , b.OTW_Policy_Form
		 , d.OTW_AOI
		 , h.OTW_HH_17_31
		 , i.OTW_Prot_Constr
		 , a.OTW_Territory_Rate*b.OTW_Policy_Form*d.OTW_AOI
		   *h.OTW_HH_17_31*i.OTW_Prot_Constr as OTW_Base_Premium
	from out.OTW_Territory_Rate as a 
	inner join out.OTW_Form_Factor as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_AOI_Factor as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_HH_17_31_Factor as h 
	on a.CaseNumber = h.CaseNumber
	inner join out.OTW_Prot_Constr_Factor as i
	on a.CaseNumber = i.CaseNumber
	;
quit;
