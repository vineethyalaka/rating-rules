*HO6 Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber WindHailDeductible)
            Fac.WindHurricaneDeductibleFactor
            (Factor CoverageDeductible)
			;
*Creates: 	out.Wind_Deductible;

* Temporary update to account for 3 year coverage change rule -- may have to be changed in next rating validation*;

* v5.3 was updated for the 2016-1 validation (two lines were added to look at policies with policy year = 2 and policy term years = 3) *;

proc sql;
    create table out.Wind_R590_Hurricane_Deductible as
	select p.CaseNumber
		 , p.PolicyNumber
		 , p.WindHailDeductible
		 , case when missing(d.Factor) then 1 
			when y.PolicyYear = 0 and p.PolicyEffectiveDate >= 20012 then d.factor 
			when p.PolicyTermYears = 1 and p.PolicyIssueDate >= 19957 then d.factor
			when y.PolicyYear = 1 and p.PolicyEffectiveDate >= 20377 and p.PolicyTermYears <> 2 then d.factor
			when p.PolicyTermYears = 2 and p.PolicyIssueDate >= 19957 then d.factor
			when y.PolicyYear = 2 and p.PolicyEffectiveDate >= 20743 then d.factor
			when p.PolicyTermYears = 3 and p.PolicyIssueDate >= 19957 then d.factor
			else d.factor1 end as Hurricane_Deductible
	from in.Policy_Info as p
	left join Fac.WindHurricaneDeductibleFactor as d
	on case when left(substr(p.WindHailDeductible,1,1)) = "$" then left(substr(p.WindHailDeductible,2,5)) 
			when p.WindHailDeductible is null then "0"
			else left(p.WindHailDeductible) end 
	   = d.HurricaneDeductible
	inner join out.policy_year as y
	on p.CaseNumber = y.CaseNumber
	;
quit;

