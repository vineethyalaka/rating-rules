*Tier Factor PA (Combined Ins Score and DRC in Tier 8);
*Uses:    	out.Tier_Classes
            (CaseNumber PolicyNumber Underwriting_Class Shopper_Class Home_First_Class Prior_Claims_Class RCMV_Class Lapse_of_Insurance_Class)
            Fac.TierTable
            (Factor InsuranceScore Shopper Auto Prior RCMV Lapse)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
           c.CaseNumber, c.PolicyNumber
          ,c.Underwriting_Class, c.Shopper_Class, c.Home_First_Class, c.Prior_Claims_Class
          ,c.RCMV_Class, c.Lapse_of_Insurance_Class
          ,t.Factor as IRM_Factor as Tier_Factor
    from out.Tier_Classes as c
	inner join Fac.TierTable as t
	on  c.Underwriting_Class       = t.InsuranceScore
	and c.Shopper_Class            = t.Shopper
	and c.Home_First_Class         = t.Auto
	and c.Prior_Claims_Class       = t.Prior
	and c.RCMV_Class               = t.RCMV
	and c.Lapse_of_Insurance_Class = t.Lapse
	;
quit;
