*HO4 OTW Adjusted Premium - FL;
*Uses:    	out.otw_base_premium
			(CaseNumber PolicyNumber OTW_Base_Premium)
			out.OTW_R403_CovC_Replacement_Cost
			(CaseNumber HO_04_90)
			out.OTW_R406_Deductibles
			(CaseNumber OTW_Deductible_Factor)
			out.otw_r480_customer_age
			(CaseNumber Mature_Owner)
			out.OTW_R488_Number_of_Units
			(CaseNumber Number_of_Units)
			out.otw_r490_theft_exclusion
			(CaseNumber Theft_Exclusion)
			;
*Creates: 	out.otw_adjusted_premium;

proc sql;
		create table out.otw_adjusted_premium as
	select a.CaseNumber, a.PolicyNumber, a.OTW_Base_Premium
	, round((c.OTW_Deductible_Factor * a.OTW_Base_Premium),1.0) as OTW_Ded_Adj
	, round((b.HO_04_90 * calculated OTW_Ded_Adj),1.0) as OTW_PPRC_Adj
	, round((d.Mature_Owner * calculated OTW_PPRC_Adj),1.0) as OTW_Age_Adj
	, round((e.Number_of_Units * calculated OTW_Age_Adj),1.0) as OTW_Units_Adj
	, round((f.Theft_Exclusion * calculated OTW_Units_Adj),1.0) as OTW_Theft_Adj
	, calculated OTW_Theft_Adj as OTW_Adjusted_Premium
	from out.otw_base_premium as a
	inner join out.otw_r403_covc_replacement_cost as b
	on a.CaseNumber = b.CaseNumber
	inner join	out.otw_r406_deductibles as c
	on a.CaseNumber = c.CaseNumber
	inner join	out.otw_r480_customer_age as d
	on a.CaseNumber = d.CaseNumber
	inner join out.otw_r488_number_of_units as e
	on a.CaseNumber = e.CaseNumber
	inner join	out.otw_r490_theft_exclusion as f
	on a.CaseNumber = f.CaseNumber
	;
quit;
