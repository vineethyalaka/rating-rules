*Wind Rule 514 Other Sructures CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber EXWIND HO_0448_Flag HO_0448_ADDL_LIMIT_01)
            Fac.WindPropertyOtherStrFactor
            (Factor)
			;
*Creates: 	out.Wind_R514_Other_Structures;

proc sql;
	create table out.Wind_R514_Other_Structures as
	select  distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0448_Flag="1" then (1-e.EXWIND)*round(e.HO_0448_ADDL_LIMIT_01*os.Factor/1000,1) else 0 end as Other_Structures
	from in.Endorsement_Info as e
		, Fac.WindPropertyOtherStrFactor as os
	;
quit;
