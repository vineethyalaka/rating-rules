** RULE 406. DEDUCTIBLES **;
* Revised by DW according to CT APEX 8970 owner's rules;

%macro ded();

data deductible_fac;
				set fac.'406# Deductibles$'n;

				format f1 f2 12.2;

				if f1 < 1 then f1 = f1*100;
				if f1 = . then delete;

				/*Lag function takes data from the prior row*/
				cova_L = lag(f2); 
				if cova_L > f2 or cova_L = . then cova_L = 0; 
				rename f2 = cova_H; 

				/*Remove NAs*/
				Wind_n = Wind*1; 
				Hail_n = Hail*1;
				Hurr_n = Hurricane*1;
			
				array peril (9) 
					Fire	
					Theft 	
					'Water Non-Weather'n 
					'All Other Non-Weather'n 
					Lightning 
					'Water Weather'n 
					Wind_n 
					Hail_n 
					Hurr_n;
				array fac_l (9) 
					fire_l  
					theft_l 
					waternw_l 			  
					othernw_l 			  
					lightning_l 
					waterw_l 
					wind_l 
					hail_l 
					hurr_l;
				array fac_h (9) 
					fire_h  
					theft_h 
					waternw_h 			  
					othernw_h 
					lightning_h 
					waterw_h 
					wind_h 
					hail_h 
					hurr_h;
				do i = 1 to 9; 
					fac_l(i) = lag(peril(i));
					fac_h(i) = peril(i);
					if cova_L = 0 then fac_l(i) = fac_h(i);
				end;
				drop i;
			run;

			proc sql noprint;
				select max(cova_H)
				into: maxdedcova
				from deductible_fac
				;
			quit;
			%put &maxdedcova;

			

/*			data deductible;*/
/*			set in.policy_info (keep=casenumber policynumber coveragea CoverageDeductible WindHailDeductible HurricaneDeductible);*/
/*			AOPDeductible = input(CoverageDeductible,dollar8.);*/
/*			WHDeductible =  input(WindHailDeductible,8.);*/
/*			HurrDeductible =  input(HurricaneDeductible,8.);*/
/*			if AOPDeductible = 250 then AOPDeductible = 500;*/
/*			if 10 < WHDeductible < 500 then WHDeductible = AOPDeductible;*/
/*			if HurrDeductible = . or 10 < HurrDeductible < 500 then HurrDeductible = WHDeductible;*/
/*			run;*/

			proc sql;
			create table deductible as
			select distinct
			a.casenumber,
			a.policynumber,
			a.coveragea,
				
			case when 0<a.distancetoshore<=2600 then 1
				 when 2600<a.distancetoshore and a.premiums_02 = 1 then 3
				 else c.deductible_group end as Ded_Group_Reassigned,
			
			 input(a.coveragedeductible,dollar8.) as AOPDeductible,
			case when calculated Ded_Group_Reassigned=1 then calculated AOPDeductible
				 when calculated Ded_Group_Reassigned=2 then calculated AOPDeductible
				 else input(a.windhaildeductible,8.) end as WHDeductible,
			
			 case when calculated Ded_Group_Reassigned=1 then 5
			 	  when calculated Ded_Group_Reassigned=2 then 2
				  else  calculated WHDeductible end as HurrDeductible

			from in.policy_info as a inner join out.census_fac as b
			on a.casenumber=b.casenumber
			left join out.deductible_group as c
			on b.territory=c.territory;
			quit;


			proc sql;
				create table deductible1 as
				select p.*, f1.cova_L, f1.cova_H
					, f1.fire_l, f1.fire_h
					, f1.theft_l, f1.theft_h
					, f1.waternw_l, f1.waternw_h
					, f1.othernw_l, f1.othernw_h
					, f1.lightning_l, f1.lightning_h
					, f1.waterw_l, f1.waterw_h
					, f2.wind_l, f2.wind_h
					, f2.hail_l, f2.hail_h
					, f3.hurr_l, f3.hurr_h		
				from deductible p 
					left join deductible_fac f1 on (p.Coveragea <= f1.cova_H or f1.cova_H = &maxdedcova) 
						and p.Coveragea > f1.cova_L and p.AOPDeductible = f1.F1
					left join deductible_fac f2 on (p.Coveragea <= f2.cova_H or f2.cova_H = &maxdedcova) 
						and p.Coveragea > f2.cova_L and p.WHDeductible = f2.F1
					left join deductible_fac f3 on (p.Coveragea <= f3.cova_H or f3.cova_H = &maxdedcova) 
						and p.Coveragea > f3.cova_L and p.HurrDeductible = f3.F1
			;quit;

			data out.ded_fac; 
				set deductible1;
				array fac_l (9) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l;
				array fac_h (9) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h;
				array fac   (9) fire_ded theft_ded waternw_ded othernw_ded lightning_ded waterw_ded wind_ded hail_ded hurr_ded;
				do i = 1 to 9;
					if coveragea > &maxdedcova or coveragea = cova_H 
					then fac(i) = fac_h(i);
					else fac(i) = round((fac_h(i) - fac_l(i))*(coveragea - cova_L)/(cova_H - cova_L) + fac_l(i),.0001);
				end;
				drop i;
			run;
			%mend;

			%ded();
