*Rule 301.1.b5 Special Personal Property CW (HH 00 15);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0015_FLAG)
            in.Production_Info
            (CaseNumber PolicyNumber HH0015)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.OTWHH0015Factor
            (Factor AdvInd)
			;
*Creates: 	out.OTW_HH_00_15_Factor;

proc sql;
    create table out.OTW_HH_00_15_Factor as
	select distinct e.CaseNumber, e.PolicyNumber, e.HO_0015_FLAG, pd.HH0015
		 , case when e.HO_0015_FLAG = '0' and pd.HH0015 = '0' then 1
			else hf15.Factor 
			end as OTW_HH_00_15 label="OTW HH-00-15"
	from in.Endorsement_Info as e
	inner join in.Production_Info as pd
	on e.CaseNumber = pd.CaseNumber
    inner join out.OTW_R486_Advantage as a
	on e.CaseNumber = a.CaseNumber
    inner join Fac.OTWHH0015Factor as hf15
    on a.AdvInd = hf15.AdvInd
	;
quit;
