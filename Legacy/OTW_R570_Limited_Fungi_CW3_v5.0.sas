*OTW Rule 570 Limited Fungi Coverage CW2;
*Uses:    	in.Production_Info
			(CaseNumber PolicyNumber HO_04_32 HO_04_33 Property_Amount Liability_Amount)
			Fac.OTWPropertyFungiFactor
			(Factor)
			;
*Creates: 	out.OTW_R570_Limited_Fungi;

proc sql;
	create table out.OTW_R570_Limited_Fungi as
	select 
		  a.CaseNumber
		, a.PolicyNumber
		, b.Factor as Limited_Fungi
	from in.Policy_Info as a
	inner join Fac.OTWPropertyFungiFactor as b
		on a.propertyfungizone = b.propertyfungizone
	;
quit;
