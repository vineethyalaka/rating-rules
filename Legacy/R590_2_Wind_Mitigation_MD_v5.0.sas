proc sql;
	create table out.Wind_R590_Mitigation as
	select p1.casenumber, p1.policynumber, 
			case p1.construction_completed when 100 then 'Reinforced Concrete Roof Deck'
										   when 200 then 'Wood'
										   when 300 then 'All Other'
				end as RoofCovering,
				case when wm_roof_cover.factor=. then 1 else wm_roof_cover.factor end as wind_roof_cover,
 
			case p1.comp_window_door_protection when 1 then '2.5" nail / 12" in field of plywood' 
												when 2 then '2.5" nail / 6" in field of plywood' 
												when 3 then '2" nail / 12" in field of plywood' 
				end as RoofAttachment,
				case when wm_roof_attach.factor=. then 1 else wm_roof_attach.factor end as wind_roof_attach, 

		    case p1.comp_engineered_status when 1 then 'Clips'
										   when 2 then 'Single Wraps'
										   when 3 then 'Double Wraps'
										   when 4 then 'Toe Nails'
				end as RoofToWallConnection,
				case when wm_connection.factor=. then 1 else wm_connection.factor end as wind_wall_connect,
				 
			case p1.comp_roof_equipment when 1 then 'Hurricane Protection'
										when 2 then 'Intermediate'
										else 'None'
				end as Shutters,
				case when wm_shutters.factor=. then 1 else wm_shutters.factor end as wind_shutters,
 
			case p1.comp_roof_wall_anchor when 1 then 'SWR'
										  else 'None'
				end as SecWaterResistance,
				case when wm_swr.factor=. then 1 else wm_swr.factor end as wind_SWR,
				 
			case p1.question_code_20 when 1 then 'Hip'
									 else 'Other'
				end as RoofShape,
				case when wm_roof_shape.factor=. then 1 else wm_roof_shape.factor end as wind_roof_shape,
				 
			case p1.ml_70_number_of_families_01 when 1 then 'Wall-to-Foundation Restraint'
											 else 'No Wall-to-Foundation Restraint'
				end as WallToFoundationRestraint,
				case when wm_wall_res.factor=. then 1 else wm_wall_res.factor end as wind_wall_res,

round(calculated wind_roof_cover* calculated wind_roof_attach * calculated wind_wall_connect 
* calculated wind_shutters * calculated wind_SWR * calculated wind_roof_shape * calculated wind_wall_res, 0.001) 
 as wind_mitigation

	from in.production_info p1
	left join fac.Windmitigationroofcover as wm_roof_cover
	on	p1.construction_completed=wm_roof_cover.code

	left join fac.Windmitigationroofattachment as wm_roof_attach
	on  p1.comp_window_door_protection=wm_roof_attach.code

	left join fac.Windmitigationroofconnection as wm_connection
	on p1.comp_engineered_status=wm_connection.code

	left join fac.Windmitigationshutters as wm_shutters
	on p1.comp_roof_equipment=wm_shutters.code
	
	left join fac.Windmitigationsecondarywater as wm_swr
	on p1.comp_roof_wall_anchor=wm_swr.code

	left join fac.Windmitigationroofshape as wm_roof_shape
	on p1.question_code_20=wm_roof_shape.code

	left join fac.Windmitigationwallrestraint as wm_wall_res
	on p1.ml_70_number_of_families_01=wm_wall_res.code;

	quit;
