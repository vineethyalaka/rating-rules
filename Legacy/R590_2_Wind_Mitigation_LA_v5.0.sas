*HO3 Rule 590.2 Wind Mitigation MS;
*Uses:    	in.Production_Info
				(CaseNumber PolicyNumber Question_Code_20 SecondaryWaterResistance 
					Comp_Engineered_Status Construction_Completed ML_70_NUMBER_OF_FAMILIES_01 
					Comp_Roof_Equipment Comp_Roof_Wall_Anchor)
			in.Policy_Info
				(CaseNumber PolicyNumber PREMIUMS_04 PREMIUMS_05 PREMIUMS_06)
			Fac.windmitigationroofgeometry
    			(RoofShapeDescr factor)
			Fac.windmitigationroofwallconnect
    			(RoofWallConnectDescr factor)
			Fac.windmitigationsecondwaterresist
    			(ScndWaterResistDescr factor)
			Fac.windmitigationconstructioncode
    			(RoofShapeDescr BldgCodeDescr factor)
			Fac.windmitigationdoorstrength
    			(DoorStrngthDescr factor)
			Fac.windmitigationopeningprotection
    			(RoofShapeDescr OpeningProtectDesc factor)
			Fac.windmitigationroofdeckattachment
    			(RoofShapeDescr RoofDeckAttachLevel factor)
*Creates: 	out.Wind_R590_Mitigation;

proc sql;
	create table work.Wind_R590_Mitigation as
	select p.CaseNumber, p.PolicyNumber
	, case when prod.Question_Code_20 = 0 then '' 
		when prod.Question_Code_20 = 1 then 'Hip' 
		when prod.Question_Code_20 = 3 and p.PREMIUMS_05 = 1 then 'Gable' 
			else 'Other' end as RoofShapeDescr
	, case when p.PREMIUMS_04 = 0 then '' 
		when p.PREMIUMS_04 in (1,2) then 'Single/Double Wraps'
		when p.PREMIUMS_04 = 3 then 'Clips'
		when p.PREMIUMS_04 in (4,5,6,7) then 'Toe Nails'
		else '' end as RoofWallConnectDescr
	, case when prod.Comp_Engineered_Status = 0 then '' 
		when prod.Comp_Engineered_Status in (1) then 'SWR'
		when prod.Comp_Engineered_Status in (2,3) then 'NO SWR'
		else '' end as ScndWaterResistDescr
	, case when prod.Construction_Completed = 0 then '' 
		when prod.Construction_Completed in (100,200) then 'LSUCC'
		else 'Non-LSUCC' end as BldgCodeDescr
	, case when p.PREMIUMS_06 = 0 then '' 
		when p.PREMIUMS_06 in (1) then 'Unreinforced Double Width'
		when p.PREMIUMS_06 in (2) then 'Reinforced Sliding Door'
		when p.PREMIUMS_06 in (3) then 'Unknown'
		when p.PREMIUMS_06 in (4) then 'Single Width Doors'
		when p.PREMIUMS_06 in (5) then 'Reinforced Double Width Doors'
		when p.PREMIUMS_06 in (6) then 'Reinforced Single Width Doors'
		else '' end as DoorStrngthDescr
	, case when prod.ML_70_NUMBER_OF_FAMILIES_01 = 0 then ''
		when prod.ML_70_NUMBER_OF_FAMILIES_01 in (1,6) then 'Level A'
		when prod.ML_70_NUMBER_OF_FAMILIES_01 in (2) then 'Level B'
		when prod.ML_70_NUMBER_OF_FAMILIES_01 in (3,4) then 'Level C & D'
		else 'Other PCR' end as RoofDeckAttachLevel
	, case when prod.Comp_Roof_Equipment	= 0 or prod.Comp_Roof_Wall_Anchor = 0 then ''
		when prod.Comp_Roof_Wall_Anchor in (3,4,5,6,7) 
			and prod.Comp_Roof_Equipment in (5,6,7) then 'None'
		when prod.Comp_Roof_Wall_Anchor in (3,4,5,6,7) 
			and prod.Comp_Roof_Equipment in (1,2,3,4) then 'Glass w/o Shutters'
		when prod.Comp_Roof_Wall_Anchor in (2) then 'Basic Shutter'
		when prod.Comp_Roof_Wall_Anchor in (1) then 'Hurricane Shutter'
		else '' end as OpeningProtectDesc
	from in.Production_Info as prod	inner join in.Policy_Info as p on prod.casenumber = p.casenumber
;
quit;

proc sql;
	create table out.Wind_R590_Mitigation as
	select wm.CaseNumber, wm.PolicyNumber 
	, case when missing(wm1.factor) then 1 else wm1.factor end as RoofShapeFactor
	, case when missing(wm2.factor) then 1 else wm2.factor end as RoofWallConnectFactor
	, case when missing(wm3.factor) then 1 else wm3.factor end as ScndWaterResistFactor
	, case when missing(wm4.factor) then 1 else wm4.factor end as BldgCodeFactor
	, case when missing(wm5.factor) then 1 else wm5.factor end as DoorStrngthFactor
	, case when missing(wm6.factor) then 1 else wm6.factor end as OpeningProtectFactor
	, case when missing(wm7.factor) then 1 else wm7.factor end as RoofDeckAttachFactor
	, round(calculated RoofShapeFactor * 
		calculated RoofWallConnectFactor * 
		calculated ScndWaterResistFactor * 
		calculated BldgCodeFactor * 
		calculated DoorStrngthFactor * 
		calculated OpeningProtectFactor * 
		calculated RoofDeckAttachFactor, 0.001) as Wind_Mitigation

from work.Wind_R590_Mitigation wm
	left join Fac.windmitigationroofgeometry as wm1 
    on wm.RoofShapeDescr = wm1.RoofShapeDescr
	left join Fac.windmitigationroofwallconnect as wm2 
    on wm.RoofWallConnectDescr = wm2.RoofWallConnectDescr
	left join Fac.windmitigationsecondwaterresist as wm3 
    on wm.ScndWaterResistDescr = wm3.ScndWaterResistDescr
	left join Fac.windmitigationconstructioncode as wm4 
    on wm.RoofShapeDescr = wm4.RoofShapeDescr 
	and wm.BldgCodeDescr = wm4.BldgCodeDescr
	left join Fac.windmitigationdoorstrength as wm5 
    on wm.DoorStrngthDescr = wm5.DoorStrngthDescr
	left join Fac.windmitigationopeningprotection as wm6 
    on wm.RoofShapeDescr = wm6.RoofShapeDescr 
	and wm.OpeningProtectDesc = wm6.OpeningProtectDesc
	left join Fac.windmitigationroofdeckattachment as wm7 
    on wm.RoofShapeDescr = wm7.RoofShapeDescr 
	and wm.RoofDeckAttachLevel = wm7.RoofDeckAttachLevel
;
quit;
