*OTW Rule 407.C.1 Additioanl Amount of Insurance for Coverage A Only CW (HO-04-20);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0420_FLAG HO_04_20_PCT)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.OTWHO0420Factor
            (Factor HO0420_PCT)
			;
*Creates: 	out.OTW_R407_1_Add_AMT_CovA;

proc sql;
	create table out.OTW_R407_1_Add_AMT_CovA as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0420_FLAG = '1' and b.AdvInd = 'N' then hf420.Factor else 1 end as HO_04_20
	from in.Endorsement_Info as e
	inner join out.OTW_R486_Advantage as b
	on b.CaseNumber = e.CaseNumber
	inner join Fac.OTWHO0420Factor as hf420
	on e.HO_04_20_PCT = hf420.HO0420_PCT
	;
quit;
