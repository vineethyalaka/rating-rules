*OTW Rule 515.A/B Personal Property (Increased/Reduction in Coverage C) CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageC PropertyUnitsInBuilding)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.OTWPropertyCovCFactor
            (Factor1 Factor2 AdvInd FamilyUnits)
			;
*Creates: 	out.OTW_R515_A_Incr_CovC;

proc sql;
	create table out.OTW_R515_A_Incr_CovC as
	select p.CaseNumber, p.PolicyNumber
		 , cc.BaseCovC
		 , (p.CoverageC - p.CoverageA*cc.BaseCovC) as IncrCovC
		 , case when calculated IncrCovC > 0  and cc.AdvInd = "N" 
											 then round(calculated IncrCovC/1000*cc.Factor1,1)
		 else 0 end as Coverage_C,
		 case when calculated IncrCovC <= 0 and cc.AdvInd = "N"
											 then round(calculated IncrCovC/1000*cc.Factor2,1)
		else 0 end as OTW_HD_006
	from in.Policy_Info as p
		inner join out.OTW_R486_Advantage as b 
		on p.CaseNumber = b.CaseNumber
		inner join Fac.OTWPropertyCovCFactor as cc
		on  b.AdvInd = cc.AdvInd
		and p.PropertyUnitsInBuilding = cc.FamilyUnits
	;
quit;
