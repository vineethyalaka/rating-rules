*HO6 Tier 6 Lapse Class HO6 DE;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance)
            in.Production_Info
            (CaseNumber PolicyNumber PriorInsurance)
						Fac.tierlapsefactor
						(Tier Factor)
						;
*Creates: 	out.Tier6_Lapse_Tier;

proc sql;
	create table out.Tier6_Lapse_Tier as
	select distinct 
		p.CaseNumber, 
		p.PolicyNumber, 
	  p.Lapse_of_Insurance, 
		pd.PriorInsurance, 
		case when (p.Lapse_of_Insurance = 1 or pd.PriorInsurance = 1)  then 1 else 0 end as Lapse_class,
    f.Factor as Lapse_class_Factor
	from in.Policy_Info as p
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		inner join fac.tierlapsefactor as f
		on (case when (p.Lapse_of_Insurance = 1 or pd.PriorInsurance = 1)  then 1 else 0 end ) = f.Tier
	;
quit;


