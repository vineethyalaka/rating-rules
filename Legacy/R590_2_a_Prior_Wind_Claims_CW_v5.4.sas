*Rule 590.2 Prior Wind Claims CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyEffectiveDate)
            in.Loss_Info
            (PolicyNumber LossCode LossDate LossAmount)
            Fac.WindPropertyPriorClaimsFactor
            (Factor Claims)
            mapping.ActofGod_PriorClaims
            (Peril ActOfGodCode)
			;
*Creates: 	out.Wind_Prior_Claims;

proc sql;
    create table work.Wind_a_All_Claims as
	select distinct p.PolicyNumber, p.CaseNumber, l.LossCode, l.LossDate, p.PolicyEffectiveDate
			, case when p.PolicyTermYears=1 then 
					case when &effst = 0 then (p.PolicyIssueDate - l.LossDate)/365
					else (p.PolicyEffectiveDate - l.LossDate)/365
					end
				else (p.PolicyEffectiveDate - l.LossDate - 60)/365
			  end as LossAge
		 , case when calculated LossAge>=3 then 0 else 1 end as LossInd
		 , l.LossAmount
	from in.Loss_Info as l
	inner join in.Policy_Info as p
	on l.CaseNumber = p.CaseNumber
	inner join mapping.ActofGod_PriorClaims as m
	on l.LossCode = m.ActOfGodCode
	where l.LossAmount^=0 and m.Peril in ("Hail","Wind")
	;
quit;

proc sql;
    create table work.Wind_b_Recent_Claims as
	select distinct a.CaseNumber, sum(a.LossInd) as SumOfLossInd
	from work.Wind_a_All_Claims as a
	group by a.CaseNumber
	;
quit;

proc sql;
    create table work.Wind_c_Prior_Claims as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when c.SumOfLossInd = . then 0 else c.SumOfLossInd end as Prior_Losses
	from in.Policy_Info as p
	left join work.Wind_b_Recent_Claims as c
	on p.CaseNumber = c.CaseNumber
	;
quit;

proc sql;
    create table out.Wind_Prior_Claims as
	select distinct c.CaseNumber, c.PolicyNumber, c.Prior_Losses
		 , cf.Factor as Wind_Prior_Claims label="Wind Prior Claims"
	from work.Wind_c_Prior_Claims as c
	inner join Fac.WindPropertyPriorClaimsFactor as cf
	on c.Prior_Losses = cf.Claims 
	;
quit;

