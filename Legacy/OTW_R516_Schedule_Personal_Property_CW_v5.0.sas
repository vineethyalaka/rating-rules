*OTW Rule 516 Scheduled Personal Property CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0461_flag HO_0461_01 - HO_0461_11)
            Fac.OTWPropertyUnschedPropFactor
            (Jewelry Furs Cameras Music Silverware Golf Art Stamps Coins Bicycles Misc)
			;
*Creates: 	out.OTW_R516_Sched_Personal_Prop;

proc sql;
	create table out.OTW_R516_Sched_Personal_Prop as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0461_flag = '0' then 0 
					else round(sp.Jewelry   *e.HO_0461_01/100,1)
						+round(sp.Furs      *e.HO_0461_02/100,1)
						+round(sp.Cameras   *e.HO_0461_03/100,1)
						+round(sp.Music     *e.HO_0461_04/100,1)
						+round(sp.Silverware*e.HO_0461_05/100,1)
						+round(sp.Golf      *e.HO_0461_06/100,1)
						+round(sp.Art       *e.HO_0461_07/100,1)
						+round(sp.Stamps    *e.HO_0461_08/100,1)
						+round(sp.Coins     *e.HO_0461_09/100,1)
						+round(sp.Bicycles  *e.HO_0461_10/100,1)
						+round(sp.Misc      *e.HO_0461_11/100,1) end as Scheduled_Personal_Property
	from Fac.OTWPropertySchedPropFactor as sp
		, in.Endorsement_Info as e
	;
quit;
