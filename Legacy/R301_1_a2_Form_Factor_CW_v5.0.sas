*Rule 301.1.a2 Form Factor CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber)
            Fac.OTWFormFactor
            (Factor)
			;
*Creates: 	out.OTW_Form_Factor;

proc sql;
    create table out.OTW_Form_Factor as
	select distinct p.CaseNumber, p.PolicyNumber, ff.Factor as OTW_Policy_Form label="OTW Policy Form Factor"
	from in.policy_info as p, Fac.OTWFormFactor as ff
	;
quit;
