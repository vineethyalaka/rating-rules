*Rule 301.1.b5 Special Personal Property CW (HH 00 15);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0015_FLAG)
            in.Production_Info
            (CaseNumber PolicyNumber HH0015)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.OTWHO0015Factor
            (Factor AdvInd HH0015 HO0015)
			;
*Creates: 	out.OTW_HH_00_15_Factor;

proc sql;
    create table out.OTW_HH_00_15_Factor as
	select  distinct e.CaseNumber, e.PolicyNumber, hf15.Factor as OTW_HH_00_15 label="OTW HH-00-15"
	from in.Endorsement_Info as e
	inner join out.Policy_Year as y
	on e.CaseNumber = y.CaseNumber
	inner join in.Production_Info as pd
	on e.CaseNumber = pd.CaseNumber
    inner join out.OTW_R486_Advantage as a
	on e.CaseNumber = a.CaseNumber
    inner join Fac.OTWHO0015Factor as hf15
    on a.AdvInd = hf15.AdvInd
    and pd.HH0015 = hf15.HH0015
    and e.HO_0015_FLAG = hf15.HO0015
	and y.PolicyYear = hf15.PolicyYear
	;
quit;
