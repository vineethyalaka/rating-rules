*Rule 590.1.a1 Territory Base Rate CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryH)
            in.endorsement_info
            (CaseNumber PolicyNumber EXWind)
            Fac.WindTerritoryFactor
            (Territory Territory_Rate)
			;
*Creates: 	out.Wind_Territory_Rate;

proc sql;
    create table out.Wind_Territory_Rate as
	select distinct p.CaseNumber, p.PolicyNumber, (1-e.EXWind)*t.Territory_Rate as Wind_Rate_current label="Wind Territory Base Rate current"
	, case when ytable.PremiumH=. then calculated Wind_Rate_current else (1-e.EXWind)*ytable.PremiumH end as Wind_Territory_Rate label="Wind Territory Base Rate"
	from in.policy_info as p
	inner join in.endorsement_info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.WindTerritoryFactor as t
	on p.TerritoryH = t.Territory
	left join in.ytable_ho3_baserate as ytable
  on p.CaseNumber = ytable.CaseNumber

	;
quit;
