*OTW Rule 506 Sink Hole TN;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber SinkHole_Flag GeoSinkHole)
            Fac.OTWPropertySinkHoleFactor
            (Territory Factor)
			;
*Creates: 	out.OTW_R506_Sink_Hole;

proc sql;
	create table out.OTW_R506_Sink_Hole as
	select distinct p.CaseNumber, p.PolicyNumber, p.SinkHole_Flag, p.GeoSinkHole
		 , case when p.SinkHole_Flag=100 then 
				sh.Factor
				else 1
			end as Sink_Hole_Factor
		 , round(b.OTW_Base_Premium*(calculated Sink_Hole_Factor-1),1) as Sink_Hole
	from in.Policy_Info as p
		 inner join Fac.OTWPropertySinkHoleFactor as sh
		 on p.GeoSinkHole = sh.Territory
		 inner join out.OTW_Base_Premium as b
		 on p.CaseNumber = b.CaseNumber
	;
quit;
