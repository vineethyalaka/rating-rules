*HO4 OTW Rule 505.C Earthquake - Loss Assessment VA HO 04;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0436_flag HO_0436_LIMIT_01)
			in.Policy_Info
			(CaseNumber PolicyNumber PropertyQuakeZone_map)
            Fac.OTWPropertyEQLossAssessment
            (Factor Amount Zone)
			Fac.otweqzipcode
			(Zip_Code EQ_Zone)
			;
*Creates:	out.OTW_R505_Zone
			out.OTW_R505C_EQ_Loss_Assessment;


proc sql;
create table out.OTW_R505_Zone as
	select distinct a.CaseNumber, a.PolicyNumber, a.PropertyZip5, b.EQ_Zone
	from in.Policy_Info as a
		inner join fac.otweqzipcode as b
		on a.PropertyZip5 = b.Zip_Code
	;
quit;

proc sql;
	create table out.OTW_R505C_EQ_Loss_Assessment as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0436_flag = '0' then 0 else round(e.HO_0436_LIMIT_01*eql.Factor/eql.Amount,1) end as EQ_Loss_Assessment
	from in.Endorsement_Info as e
		inner join out.OTW_R505_Zone as p
		on e.CaseNumber = p.CaseNumber 
		and e.PolicyNumber = p.PolicyNumber
		inner join Fac.OTWPropertyEQLossAssessment as eql
		on input(p.EQ_Zone,3.) = input(eql.Zone,3.)
	;
quit;
