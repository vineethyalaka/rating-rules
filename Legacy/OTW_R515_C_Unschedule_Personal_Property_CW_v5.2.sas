*OTW Rule 515.C Personal Property Increased Special Limits of Liability (Unshceduled) CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0465_flag HO_0466_flag HO_04_65_01_AMT - HO_04_65_07_AMT )
			in.Production_Info
            (CaseNumber PolicyNumber HH0015)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.OTWPropertyJewelryFactor
            (Factor Base amount AdvInd)
            Fac.OTWPropertyUnschedPropFactor
            (Money Securities Firearms Silverware Apparatus)
			;
*Creates: 	out.OTW_R515_C_Unsched_Incr_CovC;

proc sql;
	create table out.OTW_R515_C_Unsched_Incr_CovC as
	select distinct e.CaseNumber, e.PolicyNumber, e.HO_04_65_03_AMT, pd.HH0015, b.AdvInd, e.HO_0465_flag, e.HO_0466_flag
		 , round(pj.Factor*(case when (e.HO_04_65_03_AMT - pj.Base)<0 then 0 else (e.HO_04_65_03_AMT - pj.Base) end)/pj.amount,1) as Jewelry
		 , case when pd.HH0015 = '0' and b.AdvInd = 'N' then 0 else 1 end as HH0015_flag
		 , case when calculated HH0015_flag = 1 and HO_0466_flag = "1" or e.HO_0465_flag = "1" then 1 else 0 end as HO0465_flag
		 , case when calculated HO0465_flag = 0 then 0
				else calculated Jewelry
					  +round(usp.Money     *e.HO_04_65_01_AMT/100,1)
					  +round(usp.Securities*e.HO_04_65_02_AMT/100,1)
					  +round(usp.Firearms  *e.HO_04_65_04_AMT/100,1)
					  +round(usp.Silverware*e.HO_04_65_05_AMT/500,1)
					  +round(usp.Apparatus *(e.HO_04_65_06_AMT + e.HO_04_65_07_AMT)/500,1) 
				end as Unscheduled_Personal_Property
	from Fac.OTWPropertyUnschedPropFactor as usp
		, in.Endorsement_Info as e
		inner join in.Production_Info as pd 
		on e.CaseNumber = pd.CaseNumber
		inner join out.OTW_R486_Advantage as b 
		on e.CaseNumber = b.CaseNumber
		inner join Fac.OTWPropertyJewelryFactor as pj
		on b.AdvInd = pj.AdvInd;
	;
quit;
