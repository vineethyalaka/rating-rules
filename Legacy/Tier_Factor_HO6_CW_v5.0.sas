*HO6 Tier Factor CW;
*Uses:    	out.Tier2_ClaimsCredit_Factor
            (CaseNumber PolicyNumber ClaimsCredit_Tier ClaimsCredit_Factor)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier Occupancy_Tier_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
            c.CaseNumber, c.PolicyNumber
          , c.ClaimsCredit_Tier, c.ClaimsCredit_Factor
		  , o.Occupancy_Tier, o.Occupancy_Tier_Factor
		  , round(c.ClaimsCredit_Factor * o.Occupancy_Tier_Factor,0.000001) as Tier_Factor
    from out.Tier2_ClaimsCredit_Factor as c
	inner join out.Tier3_Occupancy_Tier as o
	on c.CaseNumber = o.CaseNumber
	;
quit;
