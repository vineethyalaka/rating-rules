*Rule 527 OTW Workers Compensation CA;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber DomesticEmployeesCount CoverageE CoverageF)
            Fac.OTWWorkersComp
            (Factor)
			;
*Creates: 	out.OTW_Workers_Comp;

proc sql;
    create table out.OTW_Workers_Comp as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when p.DomesticEmployeesCount > 2 then w.Factor*(p.DomesticEmployeesCount-2) else 0 end as Workers_Comp
	from in.Policy_Info as p
	, Fac.OTWWorkersComp as w
	;
quit;
