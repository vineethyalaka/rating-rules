*OTW Rule 521 Water Back Up of Sewers or Drains IL (with 486);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_04_95_IND)
            in.Production_Info
            (CaseNumber PolicyNumber WaterSump)
			out.otw_r486_advantage
			(CaseNumber PolicyNumber Advantage)
            Fac.OTWPropertyWaterSumpFactor
            (Amount Factor1 Factor2)
			;
*Creates: 	out.OTW_R521_Water_Sump;

proc sql;
	create table out.OTW_R521_Water_Sump as
	select distinct e.CaseNumber, e.PolicyNumber, pd.WaterSump as WaterSump_Limit, adv.Advantage
		 , case when e.HO_04_95_IND="Y" then 
		 		case when adv.Advantage > 1 then ws.Factor2 else ws.Factor1 end
				else 0 end as Water_Sump
	from in.Endorsement_Info as e
		 inner join in.Production_Info as pd
		 on e.CaseNumber = pd.CaseNumber
		 inner join Fac.OTWPropertyWaterSumpFactor as ws
		 on pd.WaterSump = ws.Amount
		 inner join out.otw_r486_advantage as adv
		 on e.CaseNumber = adv.CaseNumber
	;
quit;
