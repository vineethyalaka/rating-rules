*OTW Rule 490 Number of Stories;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyStories)
            Fac.OTWNumberOfStoriesFactor
            (NumberOfStories Factor)
			;
*Creates: 	out.OTW_R490_Number_of_Stories;

proc sql;
	create table out.OTW_R490_Number_of_Stories as
	select p.CaseNumber, p.PolicyNumber
		 , p.PropertyStories
		 , nosf.Factor as OTW_Number_Of_Stories label="OTW Stories"
	from in.policy_info as p
	inner join Fac.OTWNumberOfStoriesFactor as nosf
	on  case when p.PropertyStories < 2 
			then 'Less than 2' 
			else '2 or More' end = nosf.NumberOfStories
	;
quit;