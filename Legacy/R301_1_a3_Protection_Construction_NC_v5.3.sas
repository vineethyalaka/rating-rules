*Rule 301.1.a3 Protection/Construction CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyConstructionClass ProtectionClass)
            Source.ConstructionMapping
            (PropertyConstructionClass Type2)
            Fac.OTWProtectionConstructionFactor
            (Factor Protection_upper Protection_lower Construction)
			;
*Creates: 	out.OTW_Prot_Constr_Factor;
*5/30/2017 SL Use NC specific construction mapping table;
*10/16/2017 SL Updated new construction mapping table;

proc sql;
    create table out.OTW_Prot_Constr_Factor as
	select p.CaseNumber, p.PolicyNumber
		 , p.ProtectionClass, cm.Type2 as Construction
		 , pcf.Factor as OTW_Prot_Constr label="OTW Protection/Construction Factor"
	from in.Policy_Info as p

	inner join Source.ConstructionMapping_NC as cm
	on upper(trim(left(p.PropertyConstructionClass))) = upper(trim(left(cm.PropertyConstructionClass)))

	inner join Fac.TerritoryGroup as t
	on p.TerritoryA = t.Territory

	inner join Fac.OTWProtectionConstructionFactor as pcf
	on p.ProtectionClass = pcf.Protection
	and cm.Type2 = pcf.Construction
	and t.Territory_Group = pcf.Territory_Group
	;
quit;
