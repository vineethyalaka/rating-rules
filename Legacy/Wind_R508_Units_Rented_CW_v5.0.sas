*HO6 Wind Rule 508 Units Rented CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber Weeks_Rented_to_Others)
            Fac.WindPropertyUnitRentedFactor
            (Factor)
			out.Wind_Base_Premium
			(CaseNumber Wind_Base_Premium)
			out.Wind_Deductible
			(CaseNumber Wind_Deductible)
			;
*Creates: 	out.Wind_R508_Units_Rented;

proc sql;
	create table out.Wind_R508_Units_Rented as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when input(Weeks_Rented_to_Others,3.) < 9 then 0 
			else round(p.Wind_Base_Premium*d.Wind_Deductible*(ur.Factor-1),1) end as Units_Rented
	from Fac.WindPropertyUnitRentedFactor as ur
		, in.Endorsement_Info as e
		inner join out.Wind_Base_Premium as p
		on e.casenumber = p.casenumber
		inner join out.Wind_Deductible as d
		on e.casenumber = d.casenumber
	;
quit;
