*HO4 Tier Factor CW;
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class insurance_class_factor)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier Occupancy_Tier_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct i.CaseNumber, i.PolicyNumber
          ,i.Insurance_Class, i.insurance_class_factor, o.Occupancy_Tier, o.Occupancy_Tier_Factor
		  ,case when pd.PriorInsurance = 1 then "No" else "Yes" end as PriorInsurance
		  ,case when pd.PriorInsurance = 1 then round(1.17*i.insurance_class_factor * o.Occupancy_Tier_Factor,0.000001)
           else round(i.insurance_class_factor * o.Occupancy_Tier_Factor,0.000001) end as Tier_Factor_current
		  ,case when ytable.tierfactor=. then calculated Tier_Factor_current else ytable.tierfactor end as Tier_Factor
    from out.Tier1_InsuranceScore as i
	inner join out.Tier3_Occupancy_Tier as o
	on i.CaseNumber = o.CaseNumber
    inner join in.production_info as pd
	on i.CaseNumber = pd.CaseNumber
	left join in.ytable_ho4_tier as ytable
	on i.CaseNumber = ytable.CaseNumber
	;
quit;
