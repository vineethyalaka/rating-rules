*OTW Rule 505.D Earthquake CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber ORD_LAW_FLAG HO_0448_ADDL_LIMIT_01 HO_0454_FLAG HO_0448_FLAG)
            in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageC CoverageD PropertyUnitsInBuilding PropertyConstructionClass PropertyQuakeZone)
			out.OTW_R486_Advantage
			(CaseNumber PolicyNumber AdvInd)
			Fac.OTWPropertyEarthquakeFactor
			(FactorA FactorD FactorF Territory Construction)
			Fac.OTWPropertyLossOfUseFactor
			(BaseCovD)
			Fac.OTWPropertyCovCFactor
			(BaseCovC FamilyUnits)
			Source.ConstructionMapping
			(Type2 PropertyConstructionClass)
			;
*Creates: 	out.OTW_R505D_Earthquake;

proc sql;
	create table out.OTW_R505D_Earthquake as
	select distinct e.CaseNumber, e.PolicyNumber
		 , p.CoverageA
		 , (p.CoverageC - p.CoverageA*cc.BaseCovC) as IncrCovC
		 , case when p.CoverageD > p.CoverageA*lou.BaseCovD then (p.CoverageD - p.CoverageA*lou.BaseCovD) else 0 end as IncrCovD
		 , eq.FactorA*p.CoverageA*1.1/1000 as EQ_CovA
		 , (case when e.ORD_LAW_FLAG = '1' then eq.FactorA else 0 end)*p.CoverageA*0.15/1000 as EQ_ORD_LAW
		 , eq.FactorD*(case when calculated IncrCovC>0 then calculated IncrCovC else 0 end)/1000 as EQ_IncrCovC
		 , eq.FactorF*calculated IncrCovD/1000 as EQ_IncrCovD
		 , eq.FactorG*e.HO_0448_ADDL_LIMIT_01/1000 as EQ_HO_0448
		 , case when e.HO_0454_FLAG = '1' then  round(eq.FactorA*p.CoverageA*1.1/1000
            + (case when e.ORD_LAW_FLAG = '1' then eq.FactorA else 0 end)*p.CoverageA*0.15/1000
            + eq.FactorD*(case when calculated IncrCovC>0 then calculated IncrCovC else 0 end)/1000
            + eq.FactorF*calculated IncrCovD/1000
            + eq.FactorG*(case when e.HO_0448_FLAG='1' then e.HO_0448_ADDL_LIMIT_01 else 0 end)/1000,1)
			else 0
		   end as Earthquake
	from Fac.OTWPropertyLossOfUseFactor as lou
		, in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber
		inner join out.OTW_R486_Advantage as b 
		on e.CaseNumber = b.CaseNumber
		inner join Fac.OTWPropertyCovCFactor as cc
		on  b.AdvInd = cc.AdvInd
		and p.PropertyUnitsInBuilding = cc.FamilyUnits
		inner join Source.ConstructionMapping as cm 
		on lower(trim(left(p.PropertyConstructionClass))) = lower(trim(left(cm.PropertyConstructionClass)))
		inner join Fac.OTWPropertyEarthquakeFactor as eq
		on input(p.PropertyQuakeZone,3.) = input(eq.Territory,3.)
		and cm.Type2 = eq.Construction
	;
quit;
