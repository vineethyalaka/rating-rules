*Wind Rule 515.C Personal Property Increased Special Limits of Liability (Unshceduled) CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber EXWIND HO_0465_flag HO_04_65_01_AMT - HO_04_65_07_AMT)
			out.Wind_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.WindPropertyJewelryFactor
            (Factor Base amount AdvInd)
            Fac.WindPropertyUnschedPropFactor
            (Money Securities Firearms Silverware Apparatus)
			;
*Creates: 	out.Wind_R515_C_Unsched_Incr_CovC;

proc sql;
	create table out.Wind_R515_C_Unsched_Incr_CovC as
	select distinct e.CaseNumber, e.PolicyNumber
		 , (1-e.EXWIND)*round(pj.Factor*(case when (e.HO_04_65_03_AMT - pj.Base)<0 then 0 else (e.HO_04_65_03_AMT - pj.Base) end)/pj.amount,1) as Jewelry
		 , calculated Jewelry
			+ (1-e.EXWIND)*(round(usp.Money     *e.HO_04_65_01_AMT/100,1)
		  				+round(usp.Securities*e.HO_04_65_02_AMT/100,1)
		  				+round(usp.Firearms  *e.HO_04_65_04_AMT/100,1)
		  				+round(usp.Silverware*e.HO_04_65_05_AMT/500,1)
		  				+round(usp.Apparatus *(e.HO_04_65_06_AMT + e.HO_04_65_07_AMT)/500,1)) 
			as Unscheduled_current
			, case when ytable.PremiumH=. then calculated Unscheduled_current else ytable.PremiumH end as Unscheduled_Personal_Property


	from Fac.WindPropertyUnschedPropFactor as usp
		, in.Endorsement_Info as e
		inner join out.Wind_R486_Advantage as b 
		on e.CaseNumber = b.CaseNumber
		inner join Fac.WindPropertyJewelryFactor as pj
		on b.AdvInd = pj.AdvInd
		left join in.ytable_ho3_unschdeule as ytable
		on e.CaseNumber=ytable.CaseNumber;
	;
quit;
