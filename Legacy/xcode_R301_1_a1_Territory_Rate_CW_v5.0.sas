*Rule 301.1.a1 Territory Base Rate CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryA)
            Fac.OTWTerritoryFactor
            (Territory Territory_Rate)
			;
*Creates: 	out.OTW_Territory_Rate;

proc sql;
    create table out.OTW_Territory_Rate as
	select distinct p.CaseNumber, p.PolicyNumber, t.Territory_Rate as OTW_Rate_current label="OTW Territory Base Rate Current"

	,case when ytable.PremiumA=. then OTW_Rate_current else ytable.PremiumA end as OTW_Territory_Rate label="OTW Territory Base Rate"



	from in.policy_info as p
	inner join Fac.OTWTerritoryFactor as t
	on p.TerritoryA = t.Territory
	left join in.ytable_ho3_baserate as ytable
	on p.CaseNumber = ytable.CaseNumber
	;
quit;
