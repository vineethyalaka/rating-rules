*OTW Rule 403 Coverage C (Personal property) Replacement Cost Coverage CW (HO-04-90);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0490_FLAG)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.OTWHO0490Factor
            (Factor)
			;
*Creates: 	out.OTW_R403_CovC_Replacement_Cost;

proc sql;
	create table out.OTW_R403_CovC_Replacement_Cost as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0490_FLAG = '1' then hf490.Factor else 1 end as HO_04_90 label="OTW HO-04-90"
	from Fac.OTWHO0490Factor as hf490
    , in.Endorsement_Info as e
	inner join out.OTW_R486_Advantage as a
	on  a.CaseNumber = e.CaseNumber
	;
quit;
