*Rule 610 Liability Personal Injury CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE)
            in.Endorsement_Info
            (CaseNumber PolicyNumber HO_2482_FLAG)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.LiabilityPersonalInjuryFactor
            (Factor CoverageE)
			;
*Creates: 	out.Liability_Personal_Injury;

proc sql;
    create table out.Liability_Personal_Injury as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when b.AdvInd in ("N","A") and e.HO_2482_FLAG = "1" then pi.Factor else 0 end as Personal_Injury_E
	from in.Policy_Info as p
	inner join in.Endorsement_Info as e
	on e.CaseNumber = p.CaseNumber
	inner join out.OTW_R486_Advantage as b
	on p.CaseNumber = b.CaseNumber
	inner join Fac.LiabilityPersonalInjuryFactor as pi
	on p.CoverageE = pi.CoverageE
	;
quit;
