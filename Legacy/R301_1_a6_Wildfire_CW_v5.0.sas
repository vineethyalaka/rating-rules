/*2019/02/12 TM initial draft ITR 13759*/
/* Vendor type is hard coded to '1', due to horison doesnot store vendertype info in table */
/* code need to be modified once horizon has a field to pass vendertype*/

proc sql;
	create table out.OTW_Wildfire_Factor as
	select p.CaseNumber, p.PolicyNumber, p.county, p.GEOWFScore,
	case when p.GEOWFScore LT 60 or p.GEOWFScore = . then f.low_wildfire_factor /* if wildfire score <60 or null, use low factor*/
	     else f.high_wildfire_factor end as OTW_Wildfire_Factor
	from in.policy_info p
	left join fac.otwwildfirefactor f
	on p.county = f.county
	;
quit;

