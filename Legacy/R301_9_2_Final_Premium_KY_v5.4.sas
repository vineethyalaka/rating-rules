*Rule 301.9.2 Final Premium CW;
*Uses:    	out.Affinity_Adjusted_Premium
			(CaseNumber PolicyNumber Affinity_Adjusted_Premium)
			out.Capping_Amount
			(CaseNumber PolicyNumber Capping_Amount)
			Fac.ChargeMinimum
			(Factor)
			;
*Creates: 	out.Final_Premium;


/*%libname(4, QA1, QA1, V1148);*/


proc sql;
    create table	out.Final_Premium as
	select			a.CaseNumber,
					a.PolicyNumber,
					f.Cap as Capping_Amount,
					a.Affinity_Adjusted_Premium,
					f.Factor as min_premium label="Minimum Premium",
					case	when Affinity_Adjusted_Premium > f.Factor then Affinity_Adjusted_Premium 
							else f.Factor
							end as Min_Adjusted_Premium,
					round(calculated Min_Adjusted_Premium * 0.018) as KY_Surcharge,
					g.KYTax as KY_Municiple_Tax label="KY Municiple Tax",
					h.KYTax as KY_County_Tax label="KY County Tax",
					calculated Min_Adjusted_Premium + calculated KY_Surcharge  +KY_Municiple_Tax+ KY_County_Tax as Final_Premium

		 , c.Experience*a.Affinity*e.OTW_Premium as Adj_OTW_Premium
		 , c.Experience*a.Affinity*d.Wind_Premium as Adj_Wind_Premium
		 , c.Experience*a.Affinity*e.Tiered_Liability_Premium as Adj_Liability_Premium
		 , c.Experience*a.Affinity*(c.Property_Premium - d.Wind_Premium - d.Other_Premium) as Adj_Property_Premium
		 , c.Experience*a.Affinity*(d.Other_Premium) as Adj_Other_Premium
		 , Calculated Final_Premium 
			- Calculated Adj_OTW_Premium 
			- Calculated Adj_Wind_Premium 
			- Calculated Adj_Liability_Premium 
			- Calculated Adj_Property_Premium 
			- Calculated Adj_Other_Premium 
			as Adj_PolicyFee

	from Fac.ChargeMinimum as f, out.Affinity_Adjusted_Premium as a 
	inner join in.Policy_Info as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Expense_Adjusted_Premium as c
	on a.CaseNumber = c.CaseNumber
	inner join out.Property_Premium as d
	on a.CaseNumber = d.CaseNumber
	inner join out.Tiered_Premium as e
	on a.CaseNumber = e.CaseNumber
	inner join in.Capping_Info as f
	on a.CaseNumber = f.CaseNumber
	inner join in.Misc_info as g
	on a.CaseNumber = g.CaseNumber
	inner join in.Misc_Tax as h
	on a.CaseNumber = h.CaseNumber
	;
quit;
