*Rule 301.5 Tiered Premium CW;
*Uses:    	out.Tier_Factor
			(CaseNumber PolicyNumber Tier_Factor)
			out.Liability_Premium
			(CaseNumber PolicyNumber Liability_Premium)
			out.OTW_Rating_Factors_Premium
			(CaseNumber PolicyNumber OTW_Rating_Factors_Premium)
			;
*Creates: 	out.Tiered_Premium;

proc sql;
    create table out.Tiered_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.Tier_Factor
		 , b.Liability_Premium
		 , b.Liability_Premium as Tiered_Liability_Premium
		 , c.OTW_Rating_Factors_Premium as OTW_Premium
		 , b.Liability_Premium 
			+ c.OTW_Rating_Factors_Premium 
			as Tiered_Premium format=10.4
	from out.Tier_Factor as a 
	inner join out.Liability_Premium as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_Rating_Factors_Premium as c 
	on a.CaseNumber = c.CaseNumber
	;
quit;
