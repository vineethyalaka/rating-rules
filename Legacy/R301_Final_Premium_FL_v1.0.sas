proc sql;
	create table out.Final_Premium as
		select a.CaseNumber, a.PolicyNumber
		, a.otw_adjusted_premium + b.wind_adjusted_premium + c.hurr_adjusted_premium
		+ d.property_premium + e.tiered_liability_premium + f.factor as subtotal_prem1
		, round(g.affinity * calculated subtotal_prem1,1) as affinity_adj_prem
		, case when calculated affinity_adj_prem < h.factor then h.factor
			   else calculated affinity_adj_prem end as subtotal_prem2
		, calculated subtotal_prem2 + i.EMPA + i.MGA as final_premium
	from fac.chargeminimum as h, fac.recoupments as i, fac.otwpolicyfeefactor as f, out.otw_adjusted_premium as a
	inner join out.wind_adjusted_premium as b
	on a.CaseNumber = b.CaseNumber
	inner join	out.hurr_adjusted_premium as c
	on a.CaseNumber = c.CaseNumber
	inner join out.property_premium as d
	on a.CaseNumber = d.CaseNumber
	inner join out.liability_premium as e
	on a.CaseNumber = e.CaseNumber
	inner join out.R481_Affinity as g
	on a.CaseNumber = g.CaseNumber
	;
quit;
