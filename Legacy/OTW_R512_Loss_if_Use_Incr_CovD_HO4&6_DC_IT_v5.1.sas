*HO6 OTW Rule 512 Loss of Use - Increase Coverage D CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageC CoverageD)
            Fac.OTWPropertyLossOfUseFactor
            (BaseCovD Factor)
			;
*Creates: 	out.OTW_R512_Loss_of_Use;
*If Coverage C is 0, increased Coverage D does not apply;

proc sql;
	create table out.OTW_R512_Loss_of_Use as
	select p.CaseNumber, p.PolicyNumber
		 , case when p.CoverageD > p.CoverageC*lou.BaseCovD and p.CoverageC is not missing and p.CoverageC > 0 then (p.CoverageD - p.CoverageC*lou.BaseCovD) else 0 end as IncrCovD
		 , round(calculated IncrCovD/1000*lou.Factor,1) as Coverage_D
	from Fac.OTWPropertyLossOfUseFactor as lou
		, in.Policy_Info as p
	;
quit;
