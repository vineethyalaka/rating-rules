*OTW Rule 505.D Earthquake VA (IT version);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber ORD_LAW_FLAG HO_0448_ADDL_LIMIT_01 HO_0454_FLAG HO_0448_FLAG)
            in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageC CoverageD PropertyUnitsInBuilding PropertyConstructionClass PropertyQuakeZone_map)
			out.OTW_R486_Advantage
			(CaseNumber PolicyNumber AdvInd)
			Fac.OTWPropertyEarthquakeFactor
			(FactorA FactorD FactorF Territory Construction)
			Fac.OTWPropertyLossOfUseFactor
			(BaseCovD)
			Fac.OTWPropertyCovCFactor
			(BaseCovC FamilyUnits)
			Source.ConstructionMapping
			(Type2 PropertyConstructionClass)
			out.OTW_R505_Zone
			(CaseNumber PolicyNumber PropertyZip5 EQ_Zone)
			;
*Creates: 	out.OTW_R505D_Earthquake;

*Rule created to test fix from ITR 8526;
*ITR 8526 fixed EQ calculation without ordinance or law but kept the EQ charge for Ord/Law at 25% Cov A;

proc sql;
	create table out.OTW_R505D_Earthquake as
	select e.CaseNumber, e.PolicyNumber
		 , p.CoverageA
		 , (p.CoverageC - p.CoverageA*cc.BaseCovC) as IncrCovC
		 , case when p.CoverageD > p.CoverageA*lou.BaseCovD then (p.CoverageD - p.CoverageA*lou.BaseCovD) else 0 end as IncrCovD
		 , eq.FactorA*p.CoverageA*1.0/1000 as EQ_CovA  /* Does not include ordinance or law since there is no base ord/law in VA*/
		 , (case when e.ORD_LAW_FLAG = '1' then eq.FactorA else 0 end)*p.CoverageA*0.25/1000 as EQ_ORD_LAW /* calculated EQ Ord_Law additional premium */
		 , eq.FactorD*(case when calculated IncrCovC>0 then calculated IncrCovC else 0 end)/1000 as EQ_IncrCovC
		 , eq.FactorF*calculated IncrCovD/1000 as EQ_IncrCovD
		 , eq.FactorF*(case when e.HO_0448_FLAG='1' then e.HO_0448_ADDL_LIMIT_01 else 0 end)/1000 as EQ_HO_0448 
		 , case when e.HO_0454_FLAG = '1' then  round(eq.FactorA*p.CoverageA*1.0/1000 
            + (case when e.ORD_LAW_FLAG = '1' then eq.FactorA else 0 end)*p.CoverageA*0.25/1000
            + eq.FactorD*(case when calculated IncrCovC>0 then calculated IncrCovC else 0 end)/1000
            + eq.FactorF*calculated IncrCovD/1000
            + eq.FactorF*(case when e.HO_0448_FLAG='1' then e.HO_0448_ADDL_LIMIT_01 else 0 end)/1000,1)
			else 0
		   end as Earthquake
	from Fac.otwearthquakeordlawfactor as ord,  Fac.OTWPropertyLossOfUseFactor as lou
		, in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber
		inner join out.OTW_R486_Advantage as b 
		on e.CaseNumber = b.CaseNumber
		inner join Fac.OTWPropertyCovCFactor as cc
		on  b.AdvInd = cc.AdvInd
		and p.PropertyUnitsInBuilding = cc.FamilyUnits
		inner join Source.ConstructionMapping as cm 
		on lower(trim(left(p.PropertyConstructionClass))) = lower(trim(left(cm.PropertyConstructionClass)))
		inner join Fac.OTWPropertyEarthquakeFactor as eq
		on input(p.PropertyQuakeZone,3.) = input(eq.Territory,3.)
		and cm.Type2 = eq.Construction
	;
quit;
