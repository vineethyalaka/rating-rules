*HO4 Rule 301.4 Liability - FL;
*Uses:    	out.liability_base_rate
			(CaseNumber PolicyNumber Coverage_E Coverage_F)
			out.liability_business_pursuits
			(CaseNumber Business_Pursuits_E Business_Pursuits_F)
			out.liability_dangerous_dog
			(CaseNumber Dangerous_Dog_E Dangerous_Dog_F)
			out.liability_limited_fungi
			(CaseNumber Limited_Fungi)
			out.liability_other_location
			(CaseNumber Other_Location_E Other_Location_F)
			out.liability_personal_injury
			(CaseNumber Personal_Injury_E)
			out.liability_rented_residence
			(CaseNumber Rented_Residence_E Rented_Residence_F)
			out.liability_residence_employees
			(CaseNumber Domestic_Employees_E Domestic_Employees_F)
			out.liability_snowmobile
			(CaseNumber Snowmobile_E Snowmobile_F)
			out.liability_watercraft
			(CaseNumber Watercraft_E Watercraft_F)
			out.insurancetier_factor
			(CaseNumber Tier_Factor)
			;

*Creates: 	out.liability_premium;


proc sql;
	create table out.liability_premium as
	select
	  a.CaseNumber, a.PolicyNumber
	, round(k.Tier_Factor * a.Coverage_E,1.0) as Tiered_Coverage_E
	, round(k.Tier_Factor * a.Coverage_F,1.0) as Tiered_Coverage_F
	, round(k.Tier_Factor * b.Business_Pursuits_E,1.0) as Tiered_Business_Pursuits_E
	, round(k.Tier_Factor * b.Business_Pursuits_F,1.0) as Tiered_Business_Pursuits_F
	, round(k.Tier_Factor * c.Dangerous_Dog_E,1.0) as Tiered_Dangerous_Dog_E
	, round(k.Tier_Factor * c.Dangerous_Dog_F,1.0) as Tiered_Dangerous_Dog_F
	, round(k.Tier_Factor * d.Limited_Fungi,1.0) as Tiered_Limited_Fungi
	, round(k.Tier_Factor * e.Other_Location_E,1.0) as Tiered_Other_Location_E
	, round(k.Tier_Factor * e.Other_Location_F,1.0) as Tiered_Other_Location_F
	, round(k.Tier_Factor * f.Personal_Injury_E,1.0) as Tiered_Personal_Injury_E
	, round(k.Tier_Factor * g.Rented_Residence_E,1.0) as Tiered_Rented_Residence_E
	, round(k.Tier_Factor * g.Rented_Residence_F,1.0) as Tiered_Rented_Residence_F
	, round(k.Tier_Factor * h.Domestic_Employees_E,1.0) as Tiered_Domestic_Employees_E
	, round(k.Tier_Factor * h.Domestic_Employees_F,1.0) as Tiered_Domestic_Employees_F
	, round(k.Tier_Factor * i.Snowmobile_E,1.0) as Tiered_Snowmobile_E
	, round(k.Tier_Factor * i.Snowmobile_F,1.0) as Tiered_Snowmobile_F
	, round(k.Tier_Factor * j.Watercraft_E,1.0) as Tiered_Watercraft_E
	, round(k.Tier_Factor * j.Watercraft_F,1.0) as Tiered_Watercraft_F
	, (calculated Tiered_Coverage_E + calculated Tiered_Coverage_F + calculated Tiered_Business_Pursuits_E
	+ calculated Tiered_Business_Pursuits_F + calculated Tiered_Dangerous_Dog_E + calculated Tiered_Dangerous_Dog_F
	+ calculated Tiered_Limited_Fungi + calculated Tiered_Other_Location_E + calculated Tiered_Other_Location_F
	+ calculated Tiered_Personal_Injury_E + calculated Tiered_Rented_Residence_E + calculated Tiered_Rented_Residence_F
	+ calculated Tiered_Domestic_Employees_E + calculated Tiered_Domestic_Employees_F + calculated Tiered_Snowmobile_E
	+ calculated Tiered_Snowmobile_F + calculated Tiered_Watercraft_E + calculated Tiered_Watercraft_F) as Tiered_Liability_Premium
	from out.liability_base_rate as a
	inner join out.liability_business_pursuits as b
	on a.CaseNumber = b.CaseNumber
	inner join out.liability_dangerous_dog as c
	on a.CaseNumber = c.CaseNumber
	inner join out.liability_limited_fungi as d
	on a.CaseNumber = d.CaseNumber
	inner join out.liability_other_location as e
	on a.CaseNumber = e.CaseNumber
	inner join out.liability_personal_injury as f
	on a.CaseNumber = f.CaseNumber
	inner join out.liability_rented_residence as g
	on a.CaseNumber = g.CaseNumber
	inner join out.liability_residence_employees as h
	on a.CaseNumber = h.CaseNumber
	inner join out.liability_snowmobile as i
	on a.CaseNumber = i.CaseNumber
	inner join out.liability_watercraft as j
	on a.CaseNumber = j.CaseNumber
	inner join out.insurancetier_factor as k
	on a.CaseNumber = k.CaseNumber
	;
quit;
