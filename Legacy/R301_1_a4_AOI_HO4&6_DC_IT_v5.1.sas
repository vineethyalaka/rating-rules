*HO6 Rule 301.1.a4 AOI CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber CoverageC)
            Fac.OTWAOIFactor
            (AOI_Lower AOI_Upper Factor_Lower Factor_Upper)
			;
*Creates: 	out.OTW_AOI_Factor;
*Uses 5 x CoverageD if a policy has no Coverage C (per IT);

proc sql;
create table work.policy_info as 
	select p.CaseNumber, p.PolicyNumber, p.CoverageC, p.CoverageD, 
		case when p.CoverageC = 0 or p.CoverageC is missing then 5*p.CoverageD else p.CoverageC end as Coverage
	from in.policy_info as p
	;
quit;

proc sql;
    create table out.OTW_AOI_Factor as
	select p.CaseNumber, p.PolicyNumber, p.CoverageC, p.CoverageD, p.Coverage, f.AOI_Lower, f.AOI_Upper, f.Factor_Lower, f.Factor_Upper
	      ,(p.Coverage - f.AOI_Lower)/(f.AOI_Upper - f.AOI_Lower) as I1
		  ,(f.AOI_Upper - p.Coverage)/(f.AOI_Upper - f.AOI_Lower) as I2
		  ,(calculated I1 + abs(calculated I1))/(2*calculated I1) as Ind
		  ,round((f.Factor_Upper*calculated I1 + f.Factor_Lower*calculated I2)*calculated Ind
		          + (f.Factor_Lower + f.Factor_Upper*(p.Coverage - f.AOI_Lower)/1000)*(1 - calculated Ind)
                , 0.001) as OTW_AOI label="OTW AOI Factor"
	from work.policy_info as p
	inner join Fac.OTWAOIFactor as f
	on (p.Coverage <= f.AOI_Upper or f.AOI_Upper=0) 
	and p.Coverage > f.AOI_Lower
	;
quit;
