*Tier Classes NC;
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class)
            out.Tier2_Units_Tier
            (CaseNumber PolicyNumber Units_Tier)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier)
            out.Tier4_PriorClaims
            (CaseNumber PolicyNumber Prior_Claims_Class)
			;
*Creates: 	out.Tier_Classes;

proc sql;
    create table out.Tier_Classes as
    select distinct 
    a.CaseNumber
	, a.PolicyNumber, a.insurance_class, b.Units_Tier, c.Occupancy_Tier, d.Prior_Claims_Class
    from out.Tier1_InsuranceScore as a
	inner join out.Tier2_Units_Tier as b
	on a.CaseNumber = b.CaseNumber
	inner join out.Tier3_Occupancy_Tier as c
	on a.CaseNumber = c.CaseNumber
	inner join out.Tier4_PriorClaims as d
	on a.CaseNumber = d.CaseNumber
	;
quit;
