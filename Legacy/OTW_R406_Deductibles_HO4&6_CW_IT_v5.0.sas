*HO6 OTW Rule 406 Deductibles CW (IT calculation);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible)
            out.OTW_Tiered_Base_Premium
            (CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
            Fac.OTWDeductibleFactor
            (CoverageDeductible Factor Max_Credit)
			;
*Creates: 	out.OTW_R406_Deductibles;

proc sql;
	create table out.OTW_R406_Deductibles as
	select distinct p.CaseNumber, p.PolicyNumber
		 , df.Factor as Deductible label="Deductible"
		 , df.Max_Credit
		 , df.Factor as OTW_Deductible_Factor
	from in.Policy_Info as p
	inner join out.OTW_Tiered_Base_Premium as t
	on p.CaseNumber = t.CaseNumber
	inner join Fac.OTWDeductibleFactor as df
	on p.CoverageDeductible = df.CoverageDeductible
	;
quit;
