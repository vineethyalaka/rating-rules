/*Capping Rule*/

*Wen Zhang
*Read from previous premium in Capping_Info table;
*Validate against Cap amount in database;

/*If new broll account added, add account to BROLL_ACCOUNT_NUMBER SAS table in below folder*/
libname CapAcc "\\cambosvnxcifs01\ITRatingTeam\Capping";


proc sql;
    create table out.Capping_Amount as
	select p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, c.prior_premium, p.POLICY_TRANS_TYPE, a.Affinity_Adjusted_Premium,
	case when p.PolicyAccountNumber in (select AccountNumber from CapAcc.BROLL_ACCOUNT_NUMBER) then  1 else 0 end as Broll_Policy,
	c.CapDiscount,
	c.HomesitePreviousCasePremium,
	a.Affinity_Adjusted_Premium,
	c.cap as DB_Cap_Amount,
	case when calculated Broll_Policy = 1 then f.Capping_Floor else . end as Capping_Floor,
	case when calculated Broll_Policy = 1 then f.Capping_Ceiling else . end as Capping_Ceiling,
	case when calculated Broll_Policy = 1 then round(c.prior_premium*(1+f.Capping_Floor),1) else . end as Premium_Floor,
	case when calculated Broll_Policy = 1 then round(c.prior_premium*(1+f.Capping_Ceiling),1) else . end as Premium_Ceiling,	

	c.HomesitePreviousCasePremium - c.CapDiscount as HomesiteManualPremium ,
	/*If broll policy is amended, calculated with percentage*/
	case when calculated Broll_Policy = 1 and p.POLICY_TRANS_TYPE = 1 and c.prior_premium = 0 then 
	(Affinity_Adjusted_Premium - calculated HomesiteManualPremium)/calculated HomesiteManualPremium else . end as Cap_Amend_Factor,

	/*If broll policy is amended, calculated with percentage*/
	case when calculated Broll_Policy = 1 and p.POLICY_TRANS_TYPE = 1 and c.prior_premium = 0 then 
	Affinity_Adjusted_Premium + c.CapDiscount+
	round(c.HomesitePreviousCasePremium * calculated Cap_Amend_Factor - (Affinity_Adjusted_Premium - calculated HomesiteManualPremium),1)
	/*If broll policy is not amended, cap regularly*/
		when calculated Broll_Policy = 1 then 
			case when Affinity_Adjusted_Premium between calculated Premium_Floor and calculated Premium_Ceiling then Affinity_Adjusted_Premium
			when Affinity_Adjusted_Premium < calculated Premium_Floor then calculated Premium_Floor
			when Affinity_Adjusted_Premium > calculated Premium_Ceiling then calculated Premium_Ceiling end 

	else Affinity_Adjusted_Premium 
	end	as capped_premium,
	case when calculated Broll_Policy = 1 then calculated capped_premium - a.Affinity_Adjusted_Premium else 0 end as SAS_Cap_Amount

from in.policy_info as p
	inner join in.Capping_Info as c
	on p.CaseNumber = c.CaseNumber and p.PolicyNumber = c.PolicyNumber
	inner join Fac.capping as f
	on p.PolicyTermYears between f.Term_Lower and f.Term_Upper
	inner join out.Affinity_Adjusted_Premium a
	on p.CaseNumber = a.CaseNumber and p.PolicyNumber = a.PolicyNumber;
quit;



