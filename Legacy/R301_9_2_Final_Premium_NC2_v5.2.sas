*Consent to rate Premium NC (Check prior term CTR statues);
*Uses:    	out.NCRB_Premium
            (CaseNumber PolicyNumber NCRB_Premium)
            out.CTROTW_R407_2_Add_AMT_CovABCD
            (CaseNumber PolicyNumber HO_04_11)
            out.R301_ExperienceFactor
            (CaseNumber PolicyNumber Experience)
            out.CTR_Wind_Factor
            (CaseNumber PolicyNumber CTR_Wind_Factor)
            out.Tier_Factor
            (CaseNumber PolicyNumber Tier_Factor)
            out.OTW_R480_Mature_Owner
            (CaseNumber PolicyNumber Age)
            out.OTW_R470_Age_of_Home
            (CaseNumber PolicyNumber Property_Age)
            out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber Insurance_Class)
			out.Deviation_PriorClaims
            (CaseNumber PolicyNumber Deviation_Prior_Claims)
			fac.CTRChargeMinmum
			(factor)
			in.Policy_Info
			(CaseNumber PolicyNumber TerritoryA)
			;
*Creates: 	out.Final_Premium;

proc sql;
    create table out.Final_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , a.Base_Premium
		 , a.NCRB_Premium
		 , p.policyTermYears as CurrentTerm
		 , s.policyTermYears as PriorTerm
		 , s.CTRFlag as Prior_CTR
		 , b.HO_04_11
		 , c.Experience
		 , d.CTR_Wind_Factor
		 , e.Tier_Factor
		 , p.CTR_Info_cutoff_date - p.policyeffectivedate as days_after_eff_date

		 /*Deviation*/
		 , case when 
		 	input(p.TerritoryA,8.) not in (7,8,48,49,52)
			and h.Insurance_Class <= 9
			and i.Deviation_Prior_Claims = 0
			then 
				case when (f.Age >= 50 and g.Property_Age <= 9) then 0.81
					 when (f.Age >= 50 or g.Property_Age <= 9) then 0.9
				else 1.0 end
			else 1.0
			end as deviation

		 , round(
			round(
			 round(
			  round(a.NCRB_Premium*b.HO_04_11,1)
		 	 *c.Experience,1)
		 	*d.CTR_Wind_Factor,1)
		   *e.Tier_Factor,1) as CTR_Premium_s2

		 , j.factor as Min_CTR_Premium
		 , case when calculated CTR_Premium_s2 < j.factor then j.factor
		 	else calculated CTR_Premium_s2 end as CTR_Premium_s3

		 , case when calculated deviation < 1 then round(a.NCRB_Premium * calculated deviation,1)
		 		when calculated CTR_Premium_s3 > a.NCRB_Premium 
					and (s.CTRFlag = 'C' or p.policytermyears = 1)
					then
						case when calculated CTR_Premium_s3 > 2.5*a.NCRB_Premium then floor(2.5*a.NCRB_Premium)
							else calculated CTR_Premium_s3 end
				else a.NCRB_Premium  
			end as final_premium
		  , q.cap as capping_amount

	from fac.ctrchargeminimum as j
		, out.NCRB_Premium as a 
	inner join out.CTROTW_R407_2_Add_AMT_CovABCD as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.R301_ExperienceFactor as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.CTR_Wind_Factor as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Tier_Factor as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_R480_Mature_Owner as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_R470_Age_of_Home as g 
	on a.CaseNumber = g.CaseNumber
	inner join out.Tier1_InsuranceScore as h 
	on a.CaseNumber = h.CaseNumber
	inner join out.Deviation_PriorClaims as i 
	on a.CaseNumber = i.CaseNumber
	inner join in.Policy_Info as p
	on a.CaseNumber = p.CaseNumber
	inner join in.Capping_Info as q 
	on a.CaseNumber = q.CaseNumber
	left join sourcep.NC_CTR_Flag as s
	on p.policynumber = s.policynumber
	and (p.policytermyears = s.policytermyears + 1)
	;
quit;
