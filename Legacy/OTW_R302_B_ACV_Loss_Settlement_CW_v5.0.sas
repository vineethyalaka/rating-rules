*OTW Rule 302.B Actual Cash Value Loss Settlement CW (HO-04-81);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0481_FLAG)
            Fac.OTWHO0481Factor
            (Factor)
			;
*Creates: 	out.OTW_R302_B_ACV_Loss_Settlement;

proc sql;
	create table out.OTW_R302_B_ACV_Loss_Settlement as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0481_FLAG = '1' then hf481.Factor else 1 end as HO_04_81
	from Fac.OTWHO0481Factor as hf481, in.Endorsement_Info as e
	;
quit;
