*Tier 1 Length of Residence Class MD (looks at first term version to determine whether length of residence applies);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber)
            in.Production_Info
            (CaseNumber LengthOfResidenceInd LengthOfResidence)
            Fac.tierlengthofresidencefactor
            (Flag)
			;
*Creates: 	out.Tier1_LengthOfResidence;


proc sql;
    create table out.Tier1_LengthOfResidence as
	select distinct p.CaseNumber
	, p.PolicyNumber
	, p.PolicyIssueDate
	, pd.LengthofResidence
	, case when p.PolicyIssueDate < '09MAR2012'd or pd.LengthofResidenceInd =0 then 0 else 1 end as flag
	, r.Factor as LengthOfResidenceFactor
	from in.Policy_Info as p
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		inner join Fac.tierlengthofresidencefactor r
		on (case when p.PolicyIssueDate < '09MAR2012'd or pd.LengthofResidenceInd = 0 then 0 else 1 end)  = r.Flag
		and (case when pd.LengthOfResidence > 11 then 11 else pd.LengthOfResidence end) = r.Years
    ;
quit;
