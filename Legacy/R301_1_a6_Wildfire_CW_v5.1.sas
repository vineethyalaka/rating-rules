/*2019/02/12 TM initial draft ITR 13759*/
/* Vendor type is hard coded to '1', due to horison doesnot store vendertype info in table */
/* code need to be modified once horizon has a field to pass vendertype*/

/*2019/08/14 Wanwan ITR 14458 activate the PB Fire Risk */
/*use PBWFScore to define the factor*/
/*vendor type which is defined by effective date, 
for non-effective states, police renewal date before 10/02 using Corelogic model, after 10/02 using Pitney Bows score model  
for effetive states, the date depends on specific state*/



proc sql;
	create table out.OTW_Wildfire_Factor as
	select p.CaseNumber, p.PolicyNumber, p.county, p.PBWFScore,p.GEOWFScore
	, p.PolicyTermYears
    , p.RatingVersion
	, p.PolicyIssueDate
	, p.PolicyEffectiveDate
/*Renew business using effective date as 10/02/2019, new business using effective date as 08/08/2019
	before effective date apply CoreLogic model, after effective date apply Pitney Boews model*/
	/* if PB score <24 or null, use low factor, if CL score <60 or null, use low factor*/
	,case when p.PolicyTermYears > 1 and p.PolicyEffectiveDate gt '01OCT2019'd and (p.PBWFScore LT 24 or p.PBWFScore = . )then f.low_wildfire_factor
	     when p.PolicyTermYears > 1 and p.PolicyEffectiveDate gt '01OCT2019'd and (p.PBWFScore gt 24 or p.PBWFScore = 24 ) then f.high_wildfire_factor
	     when p.PolicyTermYears > 1 and p.PolicyEffectiveDate lt '02OCT2019'd and (p.GEOWFScore LT 60 or p.GEOWFScore = .) then f.low_wildfire_factor
	     when p.PolicyTermYears > 1 and p.PolicyEffectiveDate lt '02OCT2019'd and (p.GEOWFScore gt 60 or p.GEOWFScore = 60) then f.high_wildfire_factor
         when p.PolicyTermYears = 1 and p.PolicyEffectiveDate gt '07AUG2019'd and (p.PBWFScore LT 24 or p.PBWFScore = . )then f.low_wildfire_factor
	     when p.PolicyTermYears = 1 and p.PolicyEffectiveDate gt '07AUG2019'd and (p.PBWFScore gt 24 or p.PBWFScore = 24 ) then f.high_wildfire_factor
	     when p.PolicyTermYears = 1 and p.PolicyEffectiveDate lt '08AUG2019'd and (p.GEOWFScore LT 60 or p.GEOWFScore = .) then f.low_wildfire_factor
	     when p.PolicyTermYears = 1 and p.PolicyEffectiveDate lt '08AUG2019'd and (p.GEOWFScore gt 60 or p.GEOWFScore = 60) then f.high_wildfire_factor
   
	   end as OTW_Wildfire_Factor
	from input3.policy_info p
	left join fac.otwwildfirefactor f
	on p.county = f.county
	;
quit;

