*HO3 OTW Rule 406 Deductibles CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible)
            out.OTW_Tiered_Base_Premium
            (CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
            Fac.OTWDeductibleFactor
            (CoverageDeductible Factor Max_Credit)
			;
*If policy didn't revert to pre-v4.2 coverage, allow old deductible;
*Creates: 	out.OTW_R406_Deductibles;

%macro AOPDed;
	%if &AOPDedAdj = 0 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber, 
			/*The "Reverted" field used to have a different purpose in IT's database; all values greater than 9 should not be considered*/
			/*Reverted can be 7, 8, 9, or 0. 7 means they accepted new coverage, 9 means they kept prior coverage, then changed their minds and accepted new coverage*/
			/*8 means they kept prior coverage. 0 means they haven't received the option to be grandfathered (i.e. older cases prior to wind/hail filing) */
			case when p.Reverted > 9 then 0 else p.Reverted end as Reverted
			 , p.CoverageDeductible	as CoverageDeductible_adj
		from in.Policy_Info as p
		;
	quit;
	%end;

	%if &AOPDedAdj = 1 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber, case when p.Reverted > 9 then 0 else p.Reverted end as Reverted
			/*If policy didn't revert to prior coverage, adjust their deductible to 1000. Otherwise, use their old deductible.*/
			 , case when substr(left(p.CoverageDeductible),1,5) in ("$250","$500","$100","$2000","0") 
			and p.Reverted not in (8) then "$1000"
				else p.CoverageDeductible
				end as CoverageDeductible_adj
		from in.Policy_Info as p
		;
	quit;
	%end;

	/*Get deductible factor based on Reverted status and deductible.*/
	proc sql;
		create table out.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber, p.CoverageDeductible_adj, p.Reverted
			 , df.Factor as Deductible label="Deductible"
			 , df.Max_Credit
			 , case when (1 - df.Factor)*t.OTW_Tiered_Base_Premium > df.Max_Credit then round((1 - df.Max_Credit/t.OTW_Tiered_Base_Premium),0.001)
				else round(df.Factor, 0.001) end as OTW_Deductible_Factor
		from work.OTW_R406_Deductibles as p
		inner join out.OTW_Tiered_Base_Premium as t
		on p.CaseNumber = t.CaseNumber
		inner join Fac.OTWDeductibleFactor as df
		on p.Reverted = df.Reverted
		and p.CoverageDeductible_adj = df.CoverageDeductible
		;
	quit;
%mend;
%AOPDed;
