


/*ITR 13618 DC, remove shopper, first home, lapse of insurance class*/

*Tier Classes CW (DRC);
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class)
            out.Tier2_ShopperClass
            (CaseNumber PolicyNumber Shopper_Class)
            out.Tier3_HomeFirstClass
            (CaseNumber PolicyNumber Home_First_Class)
            out.Tier4_PriorClaims
            (CaseNumber PolicyNumber Prior_Claims_Class)
            out.Tier5_RCMVClass
            (CaseNumber PolicyNumber RCMV_Class)
            out.Tier6_LapseInsurance
            (CaseNumber PolicyNumber Lapse_of_Insurance_Class)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class)
			;
*Creates: 	out.Tier_Classes;

proc sql;
    create table out.Tier_Classes as
    select distinct a.CaseNumber
	, a.PolicyNumber, a.insurance_class
        , d.Prior_Claims_Class
	, e.RCMV_Class
        , g.DRC_Class
    from out.Tier1_InsuranceScore as a
	inner join out.Tier4_PriorClaims as d
	on a.CaseNumber = d.CaseNumber
	inner join out.Tier5_RCMVClass as e
	on a.CaseNumber = e.CaseNumber

	inner join out.Tier7_DRC as g
	on a.CaseNumber = g.CaseNumber
	;
quit;
