*Rule 590.1.a1 Territory Base Rate CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryH TerritoryH)
            in.endorsement_info
            (CaseNumber PolicyNumber EXWind)
            Fac.CTRWindFactor
            (NCRB_Territory Homesite_Wind_Zone CTR_WindFactor)
			Fac.CTRWindExFactor
			(factor)
			;
*Creates: 	out.CTR_Wind_Factor;

proc sql;
    create table out.CTR_Wind_Factor as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when t.CTR_Wind_Factor is null then 1.0
		 	else t.CTR_Wind_Factor
			end as Wind_Factor
		 , e.EXWind
		 , (1-e.EXWind)*calculated Wind_Factor + e.EXWind*ex.Factor as CTR_Wind_Factor label="CTR Wind Factor"
	from Fac.CTRWindExFactor as ex,
	in.policy_info as p
	inner join in.endorsement_info as e
	on e.CaseNumber = p.CaseNumber
	left join Fac.CTRWindFactor as t
	on input(p.TerritoryH,8.) = t.Homesite_Wind_Zone
	and input(p.TerritoryA,8.) = t.NCRB_Territory
	;
quit;
