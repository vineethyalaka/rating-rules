*Tier Factor CW (DRC);
*Uses:    	in.Tierfactor_info
            (CaseNumber PolicyNumber Insurance_Class Shopper_Class Home_First_Class Prior_Claims_Class RCMV_Class Lapse_of_Insurance_Class)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class DRC_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
           c.CaseNumber, c.PolicyNumber
          ,c.CreditTier as Insurance_Class, c.ShopperTier as Shopper_Class, c.MonolineTier as Home_First_Class, c.ClaimsTier as Prior_Claims_Class
          ,c.ReplacementCostTier as RCMV_Class, c.LapseTier as Lapse_of_Insurance_Class
		  ,d.DRC_Class, d.DRC_Factor
		  ,c.TierFactor as Tier_Factor
    from in.Tierfactor_info as c
	inner join out.Tier7_DRC as d
	on c.CaseNumber = d.CaseNumber
	;
quit;
