*Rule 590.3 Windstorm Deductible Adjusted for Minimum Deductible (with Wind Regions depending on distance to shore and terriotry);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA WindHailDeductible PropertyLatitude DistanceToShore TerritoryH)
            Fac.WindDeductibleFactor
            (class AOI_Lower AOI_Upper Factor_Lower Factor_Upper Deductible Region)
			;
*Creates: 	out.Wind_Deductible;



%macro WindDed;
	%if &WindAdj = 0 %then %do;
	proc sql;
		create table work.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber
			 , p.CoverageA
			 , p.PropertyLatitude
			 , p.DistanceToShore
			 , p.TerritoryH
			 , p.WindHailDeductible
			 , case when . < p.DistanceToShore <= 5000 then "A"
			 		when Wind_Region = "B" then "B"
					else "C"
				end as Region
			 , case when substr(left(p.CoverageDeductible),1,1) = "$" 
					 then substr(left(p.CoverageDeductible),2,5) 
				else left(p.WindHailDeductible) 
				end as CoverageDeductibleText
			 , case when substr(left(p.WindHailDeductible),1,1) = "$" 
					 then input(substr(left(p.WindHailDeductible),2,5),5.) 
				when p.WindHailDeductible is null then input(Calculated CoverageDeductibleText,5.)
				else input(left(p.WindHailDeductible),5.) 
		   		end as WindHailDeductible_num
			 , case when calculated WindHailDeductible_num <= 10
			 		then calculated WindHailDeductible_num * CoverageA /100
				else calculated WindHailDeductible_num
				end as WindHailDed_Dollar
		from in.Policy_Info as p
		left join fac.WindTerritoryFactor as f
		on p.TerritoryH = f.Territory
		;
	quit;
	proc sql;
	    create table out.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber, p.DistanceToShore, p.TerritoryH, p.Region
			 , p.CoverageA, p.WindHailDeductible, df.class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper


/*		     ,(p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1*/
/*			 ,(df.AOI_Upper - p.CoverageA)/(df.AOI_Upper - df.AOI_Lower) as D2*/
/*			 ,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind*/
/*			 , round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind*/
/*			         + (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 - calculated Ind)*/
/*	                , 0.001) as Wind_Deductible label="Wind Deductible" */


			 	 ,(df.Factor_Upper - df.Factor_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
			  	 ,round((calculated D1 * (p.CoverageA - df.AOI_Lower)),0.0001) as D2
			 	 ,round((df.Factor_Lower + calculated D2),0.001) as Wind_Deductible label="Wind Deductible"


		from work.Wind_Deductible as p
		inner join Fac.WindDeductibleFactor as df
		on case when substr(left(p.WindHailDeductible),1,1) = "$" 
					 then input(substr(left(p.WindHailDeductible),2,5),5.) 
				when p.WindHailDeductible = "" then 0
				else input(left(p.WindHailDeductible),5.) 
		   end 
		   = df.Deductible
		and p.CoverageA <= df.AOI_Upper
		and p.CoverageA > df.AOI_Lower
		;
	quit;
	%end;

	%if &WindAdj = 1 %then %do;
	proc sql;
		create table work.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber
			 , p.CoverageA
			 , p.PropertyLatitude
			 , p.DistanceToShore
			 , p.TerritoryH
			 , p.WindHailDeductible
			 , case when . < p.DistanceToShore <= 2500 then "A"
			 		when Wind_Region = "B" then "B"
					else "C"
				end as Region
			 , case when substr(left(p.CoverageDeductible),1,1) = "$" 
					 then substr(left(p.CoverageDeductible),2,5) 
				else left(p.WindHailDeductible) 
				end as CoverageDeductibleText
			 , case when substr(left(p.WindHailDeductible),1,1) = "$" 
					 then input(substr(left(p.WindHailDeductible),2,5),5.) 
				when p.WindHailDeductible is null then input(Calculated CoverageDeductibleText,5.)
				else input(left(p.WindHailDeductible),5.) 
		   		end as WindHailDeductible_num
			 , case when calculated WindHailDeductible_num <= 10
			 		then calculated WindHailDeductible_num * CoverageA /100
				else calculated WindHailDeductible_num
				end as WindHailDed_Dollar
			 , case when calculated region in ("A","B") then 0 else &mindollar end as mindollar
			 , case when calculated region = "A" then 0.05
					when calculated region = "B" then 0.02
				else &minpercent end as minpercent
			 , calculated minpercent*CoverageA as CovA_pct
			 , max(calculated CovA_pct, calculated mindollar, calculated WindHailDed_Dollar) as min_deductible
			 , case when calculated min_deductible = calculated CovA_pct then calculated minpercent*100
					when calculated min_deductible = &mindollar then &mindollar
			 		else calculated WindHailDeductible_num
				end as WindHailDeductible_adj
		from in.Policy_Info as p
		left join fac.WindTerritoryFactor as f
		on p.TerritoryH = f.Territory
		;
	quit;
	proc sql;
	    create table out.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber
			 , p.CoverageA, p.PropertyLatitude, p.DistanceToShore, p.TerritoryH, p.Region
			 , p.WindHailDeductible, p.min_deductible, p.WindHailDeductible_adj, df.class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
/*		     ,(p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1*/
/*			 ,(df.AOI_Upper - p.CoverageA)/(df.AOI_Upper - df.AOI_Lower) as D2*/
/*			 ,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind*/
/*			 , round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind*/
/*			         + (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 - calculated Ind)*/
/*	                , 0.001) as Wind_Deductible label="Wind Deductible"*/



			     ,(df.Factor_Upper - df.Factor_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
			  	 ,round((calculated D1 * (p.CoverageA - df.AOI_Lower)),0.0001) as D2
			 	 ,round((df.Factor_Lower + calculated D2),0.001) as Wind_Deductible label="Wind Deductible"



		from work.Wind_Deductible as p
		inner join Fac.WindDeductibleFactor as df
		on p.WindHailDeductible_adj
		   = df.Deductible
		and p.CoverageA <= df.AOI_Upper
		and p.CoverageA > df.AOI_Lower
		;
	quit;
	%end;
%mend;
%WindDed;

