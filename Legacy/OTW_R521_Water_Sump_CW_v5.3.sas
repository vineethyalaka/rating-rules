proc sql;
	create table out.OTW_R521_Water_Sump as
	select e.CaseNumber, e.PolicyNumber, pd.WaterSump as WaterSump_Limit, rc.HO_04_90
		 , case when e.HO_04_95_IND="Y" then 
				case when rc.HO_04_90 = 1 then ws.Factor1 else ws.Factor2 end
				else 0 end as Water_Sump
	from in.Endorsement_Info as e
		 inner join in.Production_Info as pd
		 on e.CaseNumber = pd.CaseNumber
		 inner join Fac.OTWPropertyWaterSumpFactor as ws
		 on pd.WaterSump = ws.Amount
		 inner join out.OTW_R403_CovC_Replacement_Cost as rc
		 on e.CaseNumber = rc.CaseNumber
	;
quit;