proc sql;
	create table out.Tier_Factor_CovA_Residents as
	select a.CaseNumber, a.PolicyNumber, a.Occupancy_Tier
	, b.Factor
	from out.tier_occupancy as a
	inner join fac.tiercovaresidentsfactor as b
	on a.Occupancy_Tier = b.Tier
	;
quit;
