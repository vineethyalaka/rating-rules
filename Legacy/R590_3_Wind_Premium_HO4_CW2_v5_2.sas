*HO4 Rule 590.3 Wind Premium CW;
*Uses:    	out.Wind_Base_Premium
			(CaseNumber PolicyNumber Wind_Base_Premium)
			out.Wind_Deductible
			(CaseNumber PolicyNumber Wind_Deductible)
			out.Wind_R403_CovC_Replacement_Cost
			(CaseNumber PolicyNumber HO_04_90)
			;
*Creates: 	out.Wind_Premium;

proc sql;
    create table out.Wind_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , b.Wind_Deductible
		 , c.Wind_Mitigation as Wind_Mitigation
		 , d.HO_04_90 as Wind_HO_04_90
		 , a.Wind_Base_Premium
/*		 , (1-e.exwind)*round(a.Wind_Base_Premium*b.Wind_Deductible*c.Wind_Mitigation*d.HO_04_90,1)*/
, (1-e.exwind)*round(a.Wind_Base_Premium*b.OTW_Deductible_Factor*c.Wind_Mitigation*d.HO_04_90,1)
as Wind_Premium format=10.4
	from out.Wind_Base_Premium as a 
/*	inner join out.Wind_Deductible as b */
/*	on a.CaseNumber = b.CaseNumber*/
	inner join out.Wind_R590_Mitigation as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.Wind_R403_CovC_Replacement_Cost as d 
	on a.CaseNumber = d.CaseNumber
	inner join in.endorsement_info as e
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_R406_Deductibles as b
	on a.CaseNumber = b.CaseNumber
	;
quit;
