

proc sql;
	create table out.OTW_R505D_Earthquake as
	select distinct e.CaseNumber, e.PolicyNumber
		 , p.CoverageA
		 , case when p.CoverageD > p.CoverageA*lou.BaseCovD then (p.CoverageD - p.CoverageA*lou.BaseCovD) else 0 end as IncrCovD


		 , (case when e.HO_0015_Flag='1' then eq.FactorC else eq.FactorA end)*p.CoverageA*1.1/1000 as EQ_CovA
		 , (case when e.ORD_LAW_FLAG = '1' then eq.FactorA else 0 end)*p.CoverageA*0.15/1000 as EQ_ORD_LAW
		 , eq.FactorB*(case when cc.IncrCovC>0 then cc.IncrCovC else 0 end)/1000 as EQ_IncrCovC
		 , eq.FactorC*calculated IncrCovD/1000 as EQ_IncrCovD
		 , eq.FactorC*e.HO_0448_ADDL_LIMIT_01/1000 as EQ_HO_0448

		 , case when e.HO_0454_FLAG = '1' then  round(calculated EQ_CovA
            + calculated EQ_ORD_LAW
            + calculated EQ_IncrCovC
            + calculated EQ_IncrCovD
            + calculated EQ_HO_0448,1)
			else 0
		   end as Earthquake
	from Fac.OTWPropertyLossOfUseFactor as lou
		, in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber
		inner join out.OTW_R486_Advantage as b 
		on e.CaseNumber = b.CaseNumber
		inner join out.otw_r515_a_incr_covc as cc
		on e.CaseNumber = cc.CaseNumber
		inner join Source.ConstructionMapping_NC as cm 
		on lower(trim(left(p.PropertyConstructionClass))) = lower(trim(left(cm.PropertyConstructionClass)))
		inner join Fac.OTWPropertyEarthquakeFactor as eq
		on input(p.PropertyQuakeZone,3.) = input(eq.Territory,3.)
		and cm.Type2 = eq.Construction
	;
quit;
