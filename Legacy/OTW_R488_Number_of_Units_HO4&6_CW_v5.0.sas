*HO6 OTW Rule 488 Number of Units CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber UnitsinBuilding)
            Fac.OTWUnitsFactor
            (Factor Unitslower Unitsupper)
			;
*Creates: 	out.OTW_R488_Number_of_Units;

proc sql;
	create table out.OTW_R488_Number_of_Units as
	select distinct p.CaseNumber, p.PolicyNumber, p.UnitsinBuilding
		 , nuf.Factor as Number_of_Units label="Number of Units"
	from in.Policy_Info as p
	inner join Fac.OTWUnitsFactor as nuf
	on p.UnitsinBuilding >= nuf.Unitslower
	and p.UnitsinBuilding <= nuf.Unitsupper
	;
quit;
