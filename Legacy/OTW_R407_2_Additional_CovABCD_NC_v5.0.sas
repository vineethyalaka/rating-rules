*OTW Rule 407.C.2 Additioanl Amount of Insurance for Coverage A, B, C, and D CW (HO-04-11) for CTR;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0411_FLAG)
            Fac.CTROTWHO0411Factor
            (Factor)
			;
*Creates: 	out.CTROTW_R407_2_Add_AMT_CovABCD;

proc sql;
	create table out.CTROTW_R407_2_Add_AMT_CovABCD as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0411_FLAG = '1' then hf411.Factor else 1 end as HO_04_11
	from Fac.CTROTWHO0411Factor as hf411, in.Endorsement_Info as e
	;
quit;
