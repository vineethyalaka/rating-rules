

proc sql;
    create table out.Wind_R491_Wind_Prot_Devices as
	select distinct e.CaseNumber, e.PolicyNumber, w.Factor as Windstorm_Protective_Devices
	from in.Endorsement_Info as e
    inner join Fac.WindProtectiveDevicesFactor as w 
    on e.Wind_Credit_Flag = w.Code
	;
quit;
