*Rule 301.1.a1 Territory Base Rate CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryA)
            Fac.OTWTerritoryFactor
            (Territory Territory_Rate)
			;
*Creates: 	out.OTW_Territory_Rate;

proc sql;
    create table out.OTW_Territory_Rate as
	select p.CaseNumber, p.PolicyNumber, t.Territory_Rate as OTW_Territory_Rate label="OTW Territory Base Rate"
	from in.policy_info as p
	inner join Fac.OTWTerritoryFactor as t
	on p.TerritoryA = t.Territory
	;
quit;
