*HO4 OTW Rule 505.C Earthquake - Loss Assessment - FL;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0436_flag HO_0436_LIMIT_01)
			in.Policy_Info
			(CaseNumber PolicyNumber PropertyQuakeZone)
            Fac.OTWPropertyEQLossAssessment
            (Factor Amount Zone)
			;
*Creates: 	out.OTW_R505C_EQ_Loss_Assessment;

/*Needs to be updated with BCEG*/
proc sql;
	create table out.OTW_R505C_EQ_Loss_Assessment as
	select e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0436_flag = '0' then 0 else e.HO_0436_LIMIT_01*eql.Factor/eql.Amount end as EQ_Loss_Assessment
/*Placeholder for when have field name from IT*/
/*		 , round(calculated EQ_Loss_Assessment * bceg.factor,1) as BCEG_EQ_Loss_Assessment*/
	from in.Endorsement_Info as e, Fac.OTWPropertyEQLossAssessment as eql
/*		inner join fac.otwpropertyeqbcegfactor as bceg*/
/*		on bceg.MinGrade <= BCEG_Grade*/
/*		and bceg.MaxGrade >= BCEG_Grade*/
	;
quit;
