*HO4&6 Rule 515.C Personal Property Increased Special Limits of Liability (Unshceduled) - FL;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0465_flag HO_04_65_01_AMT - HO_04_65_07_AMT)
            Fac.OTWPropertyJewelryFactor
            (Factor Base amount AdvInd)
            Fac.OTWPropertyUnschedPropFactor
            (Money Securities Firearms Silverware Apparatus)
			;
*Creates: 	out.OTW_R515_C_Unsched_Incr_CovC;

proc sql;
	create table out.OTW_R515_C_Unsched_Incr_CovC as
	select e.CaseNumber, e.PolicyNumber
		 , (pj.Factor*e.HO_04_65_03_AMT/pj.amount) as OTW_Jewelry
		 , case when HO_0015_Flag = '1' or HH0017 = '1' or e.HO_1731_Flag = '1' then 1 else 0 end as HH0015_Flag
		 , case when calculated HH0015_flag = 1  and HO_0466_flag = "1" or e.HO_0465_flag = "1" 
				then 1 else 0 end as HO0465_flag
		, calculated HO0465_flag * usp.Money * e.HO_04_65_01_AMT / 100 as OTW_Money
		, calculated HO0465_flag * usp.Securities * e.HO_04_65_02_AMT / 100 as OTW_Securities
		, calculated HO0465_flag * usp.Firearms * e.HO_04_65_04_AMT / 100 as OTW_Firearms
		, calculated HO0465_flag * usp.Silverware * e.HO_04_65_05_AMT / 500 as OTW_Silverware
		, calculated HO0465_flag * usp.Apparatus *(e.HO_04_65_06_AMT + e.HO_04_65_07_AMT) / 500 as OTW_Apparatus
	from Fac.OTWPropertyUnschedPropFactor as usp, Fac.OTWPropertyJewelryFactor as pj
		, in.Endorsement_Info as e
		  inner join in.Production_Info as pd 
		  on e.CaseNumber = pd.CaseNumber
	;
quit;

proc sql;
	create table out.Wind_R515_C_Unsched_Incr_CovC as
	select e.CaseNumber, e.PolicyNumber
		 , (pj.Factor*e.HO_04_65_03_AMT/pj.amount) as Wind_Jewelry
		 , case when HO_0015_Flag = '1' or HH0017 = '1' or e.HO_1731_Flag = '1' then 1 else 0 end as HH0015_Flag
		 , case when calculated HH0015_flag = 1  and HO_0466_flag = "1" or e.HO_0465_flag = "1" then 1 
				when e.EXWIND = 1 then 0
				else 0 end as HO0465_flag
		, calculated HO0465_flag * usp.Money * e.HO_04_65_01_AMT / 100 as Wind_Money
		, calculated HO0465_flag * usp.Securities * e.HO_04_65_02_AMT / 100 as Wind_Securities
		, calculated HO0465_flag * usp.Firearms * e.HO_04_65_04_AMT / 100 as Wind_Firearms
		, calculated HO0465_flag * usp.Silverware * e.HO_04_65_05_AMT / 500 as Wind_Silverware
		, calculated HO0465_flag * usp.Apparatus *(e.HO_04_65_06_AMT + e.HO_04_65_07_AMT) / 500 as Wind_Apparatus
	from Fac.WindPropertyUnschedPropFactor as usp, Fac.WindPropertyJewelryFactor as pj
		, in.Endorsement_Info as e
		  inner join in.Production_Info as pd 
		  on e.CaseNumber = pd.CaseNumber
	;
quit;

proc sql;
	create table out.R515_C_Unsched_Incr_CovC as
	select
	  o.CaseNumber, o.PolicyNumber
	, round(o.OTW_Jewelry + w.Wind_Jewelry,1.0) as Jewelry
	, round(o.OTW_Money + w.Wind_Money,1.0) as Money
	, round(o.OTW_Securities + w.Wind_Securities,1.0) as Securities
	, round(o.OTW_Firearms + w.Wind_Firearms,1.0) as Firearms
	, round(o.OTW_Silverware + w.Wind_Silverware,1.0) as Silverware
	, round(o.OTW_Apparatus + w.Wind_Apparatus,1.0) as Apparatus
	, calculated Jewelry + calculated Money + calculated Securities
	+ calculated Firearms + calculated Silverware + calculated Apparatus as Unscheduled_Personal_Property
	from out.OTW_R515_C_Unsched_Incr_CovC as o
	inner join out.Wind_R515_C_Unsched_Incr_CovC as w
	on o.CaseNumber = w.CaseNumber
	;
quit;
