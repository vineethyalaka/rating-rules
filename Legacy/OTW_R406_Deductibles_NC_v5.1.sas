*Include adjusted deductible credit calculation;


proc sql;
	create table out.OTW_R406_Deductibles as
	select p.CaseNumber, p.PolicyNumber
		  , p.CoverageA, p.CoverageDeductible, p.WindHailDeductible
		  , p.TerritoryA, w.territory_rate as xWind_Credit, k.OTW_AOI, b.base_premium
		  , w.territory_rate * k.OTW_AOI * 0.9 as adjusted_deductible_credit
		  , df.AOI_Lower, df.AOI_Upper
		  , df.Factor as Deductible_Factor_0
		  , b.base_premium * (1 - df.Factor) as WH_deductible_credit
		  , case when xWind_Credit > 0 and calculated adjusted_deductible_credit < calculated WH_deductible_credit 
					then 1 - calculated adjusted_deductible_credit/b.base_premium
				else df.Factor
				end as Deductible_Factor
	from in.Policy_Info as p
	inner join Fac.OTWDeductibleFactor as df
		on input(p.CoverageDeductible,dollar8.) = df.Deductible	
		and (input(p.windhaildeductible,best24.) = df.WindHailDeductible 
			or	p.windhaildeductible="" and input(p.CoverageDeductible,dollar8.) = df.WindHailDeductible)
		and p.CoverageA <= df.AOI_Upper
		and p.CoverageA >= df.AOI_Lower
	inner join out.OTW_AOI_Factor as k
		on p.casenumber = k.casenumber
	inner join fac.OTWxWindFactor as w
		on p.TerritoryA = w.Territory
	inner join out.Base_Premium as b
		on b.casenumber = p.casenumber
	;
quit;
