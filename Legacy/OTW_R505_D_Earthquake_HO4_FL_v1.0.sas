/*Needs to be updated with BCEG*/
proc sql;
	create table out.OTW_R505D_Earthquake as
	select e.CaseNumber, e.PolicyNumber
		 , p.CoverageC
		 , case when p.CoverageD > p.CoverageC*lou.BaseCovD then (p.CoverageD - p.CoverageC*lou.BaseCovD) else 0 end as IncrCovD
		 , eq.FactorB*p.CoverageC*1.01/1000 as EQ_CovC
		 , eq.FactorF*calculated IncrCovD/1000 as EQ_IncrCovD
		 , case when e.HO_0454_FLAG = '1' then  round(eq.FactorB*p.CoverageC*1.01/1000
            + eq.FactorF*calculated IncrCovD/1000)
			else 0
		   end as Earthquake
	from Fac.OTWPropertyLossOfUseFactor as lou
		, in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber
		inner join Source.ConstructionMapping as cm 
		on lower(trim(left(p.PropertyConstructionClass))) = lower(trim(left(cm.PropertyConstructionClass)))
		inner join Fac.OTWPropertyEarthquakeFactor as eq
		on cm.Type2 = eq.Construction
	;
quit;
