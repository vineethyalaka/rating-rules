*Add secondary residence;

proc sql;
    create table out.Base_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.OTW_Territory_Rate
		 , b.OTW_Policy_Form
		 , c.OTW_Prot_Constr
		 , d.Wind_Exclusion
		 , round(round(a.OTW_Territory_Rate*b.OTW_Policy_Form,1)*c.OTW_Prot_Constr,1)-d.Wind_Exclusion as Key_Premium
		 , e.OTW_AOI
		 , j.OTW_Secondary_Residence_Factor
		 , round(calculated Key_Premium*e.OTW_AOI,1)-j.OTW_Secondary_Residence_Factor as Base_Premium
	from out.OTW_Territory_Rate as a 
	inner join out.OTW_Form_Factor as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_Prot_Constr_Factor as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.Wind_Exclusion_Factor as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_AOI_Factor as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_Secondary_Residence_Factor as j
	on a.CaseNumber = j.CaseNumber
	;
quit;
