*Rule 105 Secondary Residence NC;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyResidenceType)
            Fac.OTWResidenceTypeFactor
            (Factor PropertyResidenceType)
			;
*Creates: 	out.OTW_Secondary_Residence_Factor;

proc sql;
    create table out.OTW_Secondary_Residence_Factor as
	select  distinct p.CaseNumber, p.PolicyNumber
		 , p.PropertyResidenceType
		 , f.Factor as OTW_Secondary_Residence_Factor label="OTW Multiple Families Factor"
	from in.policy_info as p
    inner join Fac.OTWResidenceTypeFactor as f
    on p.PropertyResidenceType = f.PropertyResidenceType
	;
quit;
