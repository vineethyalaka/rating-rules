*OTW Rule 590 Mine Subsidence;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO2388_FLAG)
            in.Policy_Info
            (CaseNumber PolicyNumber Name)
            Fac.OTWPropertyMineSubsidenceFactor
            (County)
			;
*Creates: 	out.OTW_590_Mine_Subsidence;


proc sql;
	create table out.OTW_590_Mine_Subsidence as
	select distinct e.CaseNumber, e.PolicyNumber
		 , e.HO2388_FLAG
		 , p.NAME as County
		 , case when e.HO2388_FLAG="1" 
			then round(ms.Factor,1) else 0 end as Mine_Subsidence
	from in.Policy_Info as p 
		 inner join Fac.OTWPropertyMineSubsidenceFactor as ms
		 on ms.County=p.Name
		 inner join in.Endorsement_Info as e
		 on e.CaseNumber = p.CaseNumber
	;
quit;
