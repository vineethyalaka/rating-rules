*OTW Rule 519 Special Computer Coverage CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0414_FLAG)
            Fac.OTWPropertyComputerFactor
            (Factor)
			;
*Creates: 	out.OTW_R519_Computer;

proc sql;
	create table out.OTW_R519_Computer as
	select distinct e.CaseNumber, e.PolicyNumber
		 , round(case when e.HO_0414_FLAG = '1' then com.Factor else 0 end,1) as Computer
	from Fac.OTWPropertyComputerFactor as com
		, in.Endorsement_Info as e
	;
quit;
