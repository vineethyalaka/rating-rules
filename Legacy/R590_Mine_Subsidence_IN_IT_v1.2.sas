** RULE 590. MINE SUBSIDENCE INSURANCE **;
*add non dwelling;

%macro ms();
data r590_Dwelling;
set fac.'590# Mine Subsidence-Dwelling$'n end=eof;
where f1 ne .;
output;
if eof then do;
output; 
F1=F3+1; F3=.; F4=F4;output; 
end;
run;

data r590_NonDwelling;
set fac.'590# Mine Subsidence-NonDwell$'n end=eof;
rename 'Non-Dwelling'n=F1 Rate=F4;
/*where f1 ne .;*/
run;



/*dwelling rates: coverage A*/
proc sql;
	create table out.Mine_Sub_Rate_Dwelling as
	select p.CaseNumber, p.PolicyNumber, e.HO2388_FLAG, p.CoverageA
		, case when e.HO2388_FLAG = '1' then f4 else 0 end as Mine_Sub_Dwelling_Rate
	from in.policy_info p
		inner join in.Endorsement_Info e on p.casenumber = e.casenumber
		left join r590_Dwelling f on p.CoverageA >= f.f1 and (p.CoverageA <= f.f3 or f.f3 = .);
quit;


/*non-dwelling rates: increased coverage B*/
proc sql;
	create table out.Mine_Sub_Rate_NonDwelling as
	select p.casenumber, p.policynumber, 
	case when p.HO_0448_Addl_limit_01_pi=0 then 0 else t1.f4 end as ND1,
	case when p.HO_0448_Addl_limit_02_pi=0 then 0 else t2.f4 end as ND2,
	case when p.HO_0448_Addl_limit_03_pi=0 then 0 else t3.f4 end as ND3,
	case when p.HO_0448_Addl_limit_04_pi=0 then 0 else t4.f4 end as ND4,
	case when p.HO_0448_Addl_limit_05_pi=0 then 0 else t5.f4 end as ND5,
	case when p.HO_0448_Addl_limit_06_pi=0 then 0 else t6.f4 end as ND6,
	case when p.HO_0448_Addl_limit_07_pi=0 then 0 else t7.f4 end as ND7,
	case when p.HO_0448_Addl_limit_08_pi=0 then 0 else t8.f4 end as ND8,
	case when p.HO_0448_Addl_limit_09_pi=0 then 0 else t9.f4 end as ND9,
	case when p.HO_0448_Addl_limit_10_pi=0 then 0 else t10.f4 end as ND10,
	
	case when e.HO2388_FLAG = '1' then 
		calculated ND1 +
		calculated ND2 +
		calculated ND3 +
		calculated ND4 +
		calculated ND5 +
		calculated ND6 +
		calculated ND7 +
		calculated ND8 +
		calculated ND9 +
		calculated ND10  else 0 end as Mine_Sub_Rate_NonDwelling_Rate

	from in.policy_info p inner join in.Endorsement_Info e on p.casenumber = e.casenumber
	left join r590_NonDwelling as t1 on p.HO_0448_Addl_limit_01_pi>=t1.f1 and p.HO_0448_Addl_limit_01_pi<=t1.f3
	left join r590_NonDwelling as t2 on p.HO_0448_Addl_limit_02_pi>=t2.f1 and p.HO_0448_Addl_limit_02_pi<=t2.f3
	left join r590_NonDwelling as t3 on p.HO_0448_Addl_limit_03_pi>=t3.f1 and p.HO_0448_Addl_limit_03_pi<=t3.f3
	left join r590_NonDwelling as t4 on p.HO_0448_Addl_limit_04_pi>=t4.f1 and p.HO_0448_Addl_limit_04_pi<=t4.f3
	left join r590_NonDwelling as t5 on p.HO_0448_Addl_limit_05_pi>=t5.f1 and p.HO_0448_Addl_limit_05_pi<=t5.f3
	left join r590_NonDwelling as t6 on p.HO_0448_Addl_limit_06_pi>=t6.f1 and p.HO_0448_Addl_limit_06_pi<=t6.f3
	left join r590_NonDwelling as t7 on p.HO_0448_Addl_limit_07_pi>=t7.f1 and p.HO_0448_Addl_limit_07_pi<=t7.f3
	left join r590_NonDwelling as t8 on p.HO_0448_Addl_limit_08_pi>=t8.f1 and p.HO_0448_Addl_limit_08_pi<=t8.f3
	left join r590_NonDwelling as t9 on p.HO_0448_Addl_limit_09_pi>=t9.f1 and p.HO_0448_Addl_limit_09_pi<=t9.f3
	left join r590_NonDwelling as t10 on p.HO_0448_Addl_limit_10_pi>=t10.f1 and p.HO_0448_Addl_limit_10_pi<=t10.f3;
	quit;


/*additional living expense*/

proc sql;
create table out.Mine_Sub_Ale as 
select 
p.casenumber,
p.policynumber,
case when e.HO2388_FLAG = '1' and substr(p.partnerflag,49,1) = '1' then 5
	 else 0 end as Aleid_rate
from in.policy_info p inner join in.Endorsement_Info e on p.casenumber = e.casenumber;
quit;



proc sql;
create table out.Mine_sub_rate as
select 
a.casenumber,
a.policynumber,
a.Mine_Sub_Dwelling_Rate,
b.Mine_Sub_Rate_NonDwelling_Rate,
c.Aleid_rate,
a.Mine_Sub_Dwelling_Rate+b.Mine_Sub_Rate_NonDwelling_Rate+c.Aleid_rate as Mine_Sub_Rate
from out.Mine_Sub_Rate_Dwelling as a
inner join out.Mine_Sub_Rate_NonDwelling as b
on a.casenumber=b.casenumber
inner join out.Mine_Sub_Ale as c
on a.casenumber=c.casenumber;
quit;



*title 'RULE 590. MINE SUBSIDENCE INSURANCE';

/*%if &Process = 0 %then %do;*/
/*proc print data=out.Mine_Sub_Rate;*/
/*where Mine_Sub_Rate = .;*/
/*run;*/
/*proc sort data=out.Mine_Sub_Rate; by HO2388_FLAG; run;*/
/*proc gplot data=out.Mine_Sub_Rate; by HO2388_FLAG;*/
/*	plot Mine_Sub_Rate*CoverageA */
/*		/ overlay legend vaxis=axis2;*/
/*run;*/
/*quit;*/
/**/
/*%end;*/


%mend;
%ms();
