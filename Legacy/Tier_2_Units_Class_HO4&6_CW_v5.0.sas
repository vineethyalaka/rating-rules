*HO4/6 Tier 2 Number of Units Class CW;
*Uses:    	in.policy_info
			(CaseNumber PolicyNumber UnitsinBuilding)
			Fac.TierUnitFactor
			(Units_upper Units_lower Tier)
			;
*Creates: 	out.Tier2_Units_Tier;

proc sql;
	create table out.Tier2_Units_Tier as 
	select distinct p.CaseNumber, p.PolicyNumber, p.UnitsinBuilding
		, f.tier as Units_Tier, f.factor as Units_Tier_Factor
	from in.policy_info as p
		left join Fac.TierUnitFactor as f
		on f.Units_upper>=p.UnitsinBuilding
		and f.Units_lower<=p.UnitsinBuilding
	;
quit;
