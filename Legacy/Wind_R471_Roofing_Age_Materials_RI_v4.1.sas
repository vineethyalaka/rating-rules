*Wind Rule 471 Roofing Age/Materials RI;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate PropertyYearRoofInstalled PropertyRoofTypeCode)
            Fac.WindRoofFactor
            (Factor Material)
			;
*Creates: 	out.Wind_R471_Roofing_Age_Materials;

proc sql;
	create table work.Wind_R471_Roofing_Age_Materials as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
		 , floor((case when p.PolicyTermYears=1 and &effst = 0 then p.PolicyIssueDate else p.PolicyEffectiveDate end
	 		- case when p.PropertyYearRoofInstalled <= 0 then date() else mdy(1,1,p.PropertyYearRoofInstalled) end)/365) as Roof_Age
	from in.Policy_Info as p
	; 

	create table out.Wind_R471_Roofing_Age_Materials as
	select p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
		 , rf.Factor as Roof_Age_Materials label="Roof Age Materials"
	from work.Wind_R471_Roofing_Age_Materials as p
	left join source.MappingRoofType_RI as mrt
	on lower(trim(left(p.PropertyRoofTypeCode))) = lower(trim(left(mrt.PropertyRoofType)))
	inner join Fac.WindRoofFactor as rf
	on  lower(trim(left(rf.Material))) = lower(trim(left(mrt.Type)))
	and p.Roof_Age <= rf.Age_upper
	and p.Roof_age >= rf.Age_lower
	;
quit;
