*HO4 Windstorm Deductible - FL;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber WindHailDeductible)
            Fac.WindDeductibleFactor
            (CoverageDeductible Factor)
			;
*Creates: 	out.Wind_r406_Deductibles;

proc sql;
	create table out.Wind_r406_Deductibles as
	select p.CaseNumber, p.PolicyNumber
		 , p.WindHailDeductible as Deductible label="Deductible"
		 , df.Factor as Wind_Deductible
	from in.Policy_Info as p
	inner join Fac.WindDeductibleFactor as df
	on case when substr(p.WindHailDeductible,1,1) = "$" then left(substr(p.WindHailDeductible,2,5)) 
			else left(p.WindHailDeductible) end 
	   = left(df.CoverageDeductible)
	;
quit;
