*Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA WindHailDeductible)
            Fac.WindDeductibleFactor
            (class AOI_Lower AOI_Upper Factor_Lower Factor_Upper Deductible)
			;
*Creates: 	out.Wind_Deductible;

proc sql;
    create table out.Wind_Deductible as
	select distinct p.CaseNumber, p.PolicyNumber
		 , p.CoverageA, p.WindHailDeductible, df.class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
	     ,(p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
		 ,(df.AOI_Upper - p.CoverageA)/(df.AOI_Upper - df.AOI_Lower) as D2
		 ,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind
		 , round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind
		         + (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 - calculated Ind)
                , 0.001) as Wind_Deductible label="Wind Deductible"
	from in.Policy_Info as p
	inner join Fac.WindDeductibleFactor as df
	on case when substr(left(p.WindHailDeductible),1,1) = "$" 
				 then input(substr(left(p.WindHailDeductible),2,5),5.) 
			when p.WindHailDeductible = "" then 0
			else input(left(p.WindHailDeductible),5.) 
	   end 
	   = df.Deductible
	and p.CoverageA <= df.AOI_Upper
	and p.CoverageA > df.AOI_Lower
	;
quit;

