*HO6 Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber WindHailDeductible)
            Fac.WindDeductibleFactor
            (Factor CoverageDeductible)
			;
*Creates: 	out.Wind_Deductible;

proc sql;
    create table out.Wind_Deductible as
	select distinct p.CaseNumber, p.PolicyNumber, p.WindHailDeductible, d.CoverageDeductible
		 , d.Factor as Wind_Deductible label="Wind Deductible"
	from in.Policy_Info as p
	left join Fac.WindDeductibleFactor as d
     on p.CoverageDeductible = d.CoverageDeductible
	;
quit;

