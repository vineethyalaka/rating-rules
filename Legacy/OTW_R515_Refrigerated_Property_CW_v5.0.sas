

proc sql;
	create table out.OTW_R515_Refrigerated_Property as
	select distinct e.CaseNumber, e.PolicyNumber
		 , rf.Factor as Refrigerated_Property
	from Fac.OTWPropertyRefrideratedFactor as rf
		, in.Endorsement_Info as e
	;
quit;
