*HO6 Tier 1 Insurance Score Class CW3 (not consider lapse or prior insurance, inclusive upper point, use default score if without DRC Info);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance)
            in.Production_Info
            (CaseNumber PolicyNumber PriorInsurance)
            in.drc_info
			(CaseNumber PolicyNumber InsScore ScoreModel)
            out.Tier4_PriorClaims
			(CaseNumber PolicyNumber Prior_Claims_Class)
			Fac.TierClaimsCreditClass
			(PriorClaimClass ScoreModel Inslower Insupper Tier)
			Fac.TierDefaultCreditClass
			(ScoreModel InsuranceScore)
			Fac.TierClaimsCreditFactor
			(Tier Factor)
			;
*Creates: 	out.Tier2_ClaimsCredit_Tier out.Tier2_ClaimsCredit_Factor;

proc sql;
	create table work.Tier2_ClaimsCredit_Tier as
	select p.CaseNumber, p.PolicyNumber
		 , case when d.InsScore = . then df.ScoreModel
		 	else d.ScoreModel
			end as ScoreModel
		 , case when d.matchcode = 0 or d.matchcode = -1  then df.InsuranceScore
		        when d.InsScore = . then df.InsuranceScore
		 	else d.InsScore
			end as InsScore
		 , p.Lapse_of_Insurance, pd.PriorInsurance
		, case when d.InsScore = . then "Missing DRC Info Record" 
			   else ""
		  end as Comment
	from in.Policy_Info as p
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		left join in.drc_info as d
		on p.CaseNumber = d.CaseNumber
		left join Fac.TierDefaultCreditClass as df
		on d.scoremodel = df.scoremodel
	;

	create table out.Tier2_ClaimsCredit_Tier as
	select p.CaseNumber, p.PolicyNumber, c.Prior_Claims_Class
		 , p.ScoreModel
		 , p.InsScore
		 , p.Lapse_of_Insurance, p.PriorInsurance
		 , t.Tier as ClaimsCredit_Tier
		 , p.Comment
	from work.Tier2_ClaimsCredit_Tier as p
		inner join out.Tier4_PriorClaims as c
		on p.CaseNumber = c.CaseNumber
		left join Fac.TierClaimsCreditClass as t
		on t.Insupper>=p.InsScore
		and t.Inslower<=p.InsScore
		and t.ScoreModel=p.ScoreModel
		and t.PriorClaimClass=c.Prior_Claims_Class
	;
quit;

proc sql;
	create table out.Tier2_ClaimsCredit_Factor as
	select t.CaseNumber, t.PolicyNumber, t.ClaimsCredit_Tier, f.Factor as ClaimsCredit_Factor
	from out.Tier2_ClaimsCredit_Tier as t
	left join Fac.TierClaimsCreditFactor as f
	on t.ClaimsCredit_Tier = f.Tier
	;
quit;
