*HO6 Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber WindHailDeductible)
            Fac.WindDeductibleFactor
            (Factor CoverageDeductible)
			;
*Creates: 	out.Wind_Deductible;

proc sql;
    create table out.Wind_R590_Hurricane_Deductible as
	select distinct p.CaseNumber
		 , p.PolicyNumber
		 , p.WindHailDeductible
		 , d.Factor as Hurricane_Deductible
	from in.Policy_Info as p
	inner join Fac.WindHurricaneDeductibleFactor as d
	on case when left(substr(p.WindHailDeductible,1,1)) = "$" then left(substr(p.WindHailDeductible,2,5)) 
			when p.WindHailDeductible is null then "0"
			else left(p.WindHailDeductible) end 
	   = d.HurricaneDeductible
	;
quit;

