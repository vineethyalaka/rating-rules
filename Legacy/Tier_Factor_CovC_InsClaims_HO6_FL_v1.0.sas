proc sql;
    create table out.Tier_Factor_CovC_InsClaims as
	select  o.CaseNumber, o.PolicyNumber
          , o.Prior_Claims_Class, i.insurance_class 
          , t.Factor as  Tier_Factor
    from out.tier4_priorclaims as o, out.Tier_InsuranceScore as i, fac.tiercovcinsuranceclaimsfactor as t
	where o.CaseNumber = i.CaseNumber
	and o.Prior_Claims_Class = t.ClaimsTier
	and i.Insurance_Class = t.InsuranceScoreTier
	;
quit;
