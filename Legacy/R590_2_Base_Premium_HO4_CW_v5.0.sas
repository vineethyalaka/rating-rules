*HO4 Rule 590.2 Wind Base Premium CW;
*Uses:    	out.Wind_Territory_Rate
            (CaseNumber PolicyNumber Wind_Territory_Rate)
            out.Wind_AOI_Factor
            (CaseNumber PolicyNumber Wind_AOI)
			;
*Creates: 	out.Wind_Base_Premium;

proc sql;
    create table out.Wind_Base_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Wind_Territory_Rate
		 , d.Wind_AOI
		 , round(a.Wind_Territory_Rate*d.Wind_AOI,1) 
			as Wind_Base_Premium
	from out.Wind_Territory_Rate as a 
	inner join out.Wind_AOI_Factor as d 
	on a.CaseNumber = d.CaseNumber
	;
quit;
