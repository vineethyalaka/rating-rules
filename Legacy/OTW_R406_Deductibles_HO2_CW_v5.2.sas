*OTW Rule 406 Deductibles CW (HO2);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible CoverageA)
            Fac.OTWDeductibleFactor
            (Deductible AOI_Lower AOI_Upper Factor_Lower Factor_Upper Class)
			;
*Creates: 	out.OTW_R406_Deductibles;

%macro AOPDed;
proc sql;
	%if &AOPDedAdj = 1 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select distinct p.CaseNumber, p.PolicyNumber
			 , c.Coverage
			 , case when input(p.CoverageDeductible,dollar8.) < &minAOP then &minAOP
				else input(p.CoverageDeductible,dollar8.)
				end as CoverageDeductible_adj
		from in.Policy_Info as p
		inner join out.Coverage as c
		on p.CaseNumber = c.CaseNumber
		;
	quit;
	%end;

	%if &AOPDedAdj = 0 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select distinct p.CaseNumber, p.PolicyNumber
			 , c.Coverage
			 , input(p.CoverageDeductible,dollar8.) as CoverageDeductible_adj
		from in.Policy_Info as p
		inner join out.Coverage as c
		on p.CaseNumber = c.CaseNumber
		;
	quit;
	%end;

	proc sql;
		create table out.OTW_R406_Deductibles as
		select distinct p.CaseNumber, p.PolicyNumber
		  	 , c.Coverage, p.CoverageDeductible_adj, df.Class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
	         ,(c.Coverage - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
		  	 ,(df.AOI_Upper - c.Coverage)/(df.AOI_Upper - df.AOI_Lower) as D2
		  	 ,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind
		  	 ,round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind
		          + (df.Factor_Lower + df.Factor_Upper*(c.Coverage - df.AOI_Lower)/10000)*(1 - calculated Ind)
                , 0.001) as OTW_Deductible_Factor label="OTW Deductible"
		from work.OTW_R406_Deductibles as p
		inner join out.Coverage as c
		on p.CaseNumber = c.CaseNumber
		inner join Fac.OTWDeductibleFactor as df
		on CoverageDeductible_adj = df.Deductible
		and c.Coverage <= df.AOI_Upper
		and c.Coverage > df.AOI_Lower
		;
	quit;
%mend;
%AOPDed;
