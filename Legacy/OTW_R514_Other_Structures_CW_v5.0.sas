*OTW Rule 514 Other Sructures CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0448_Flag HO_0448_ADDL_LIMIT_01)
            Fac.OTWPropertyOtherStructuresFactor
            (Factor)
			;
*Creates: 	out.OTW_R514_Other_Structures;

proc sql;
	create table out.OTW_R514_Other_Structures as
	select distinct e.CaseNumber,e.PolicyNumber
		 , case when e.HO_0448_Flag="1" then round(e.HO_0448_ADDL_LIMIT_01*os.Factor/1000,1) else 0 end as Other_Structures
	from in.Endorsement_Info as e
		, Fac.OTWPropertyOtherStructuresFactor as os
	;
quit;
