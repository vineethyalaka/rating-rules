*HO6 Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber WindHailDeductible)
            Fac.WindDeductibleFactor
            (Factor CoverageDeductible)
			;
*Creates: 	out.Wind_Deductible;

proc sql;
	create table work.Wind_Deductible as
	select p.casenumber, p.policynumber, p.WindHailDeductible, input(left(substr(p.CoverageDeductible,2,5)),8.) as AOP_deductible
		, case when left(substr(p.WindHailDeductible,1,1)) = "$" then left(substr(p.WindHailDeductible,2,5)) 
			when p.WindHailDeductible is null then "0"
			else left(p.WindHailDeductible) end as WHDeductible
		, input(calculated WHDeductible,8.)  as WHDeductible_adj
	from in.Policy_Info as p
	;

    create table out.Wind_Deductible as
	select p.CaseNumber, p.PolicyNumber, p.WindHailDeductible, p.WHDeductible
		 , p.AOP_deductible, p.WHDeductible_adj, d.CoverageDeductible
		 , d.Factor as Wind_Deductible label="Wind Deductible"
	from work.Wind_Deductible as p
	left join Fac.WindDeductibleFactor as d
	on p.WHDeductible_adj = input(d.CoverageDeductible, 8.)
	;
quit;

