*OTW Rule 512 Loss of Use - Increase Coverage D CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageD)
			out.otw_tiered_base_premium
			(otw_tiered_base_premium)
            Fac.OTWPropertyLossOfUseFactor
            (BaseCovD Factor)
			;
*Creates: 	out.OTW_R512_Loss_of_Use;

proc sql;
	create table out.OTW_R512_Loss_of_Use as
	select p.CaseNumber, p.PolicyNumber, tbp.otw_tiered_base_premium
		 , case when p.Loss_of_Use_pay_period=24 then p.coverageA*0.05 else 0 end as IncrCovD
		 , round(case when p.Loss_of_Use_pay_period=24 then tbp.otw_tiered_base_premium*0.02 else 0 end, 1) as Coverage_D
	from in.Policy_Info as p 
		inner join out.otw_tiered_base_premium as tbp
		on p.CaseNumber=tbp.CaseNumber
	;
quit;
