*HO6 OTW Rule 507.Coverage A Increase Limits CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA)
            Fac.OTWPropertyCovACondoFactor
            (Factor)
			;
*Creates: 	out.OTW_R507_Incr_CovA;

proc sql;
	create table out.OTW_R507_Incr_CovA as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when p.CoverageA > 5000 then (p.CoverageA - 5000)
			else 0 end as IncrCovA
		 , round(calculated IncrCovA/1000*a.Factor,1) as Coverage_A
	from in.Policy_Info as p
		, Fac.OTWPropertyCovACondoFactor as a
	;
quit;
