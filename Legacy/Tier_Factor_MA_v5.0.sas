*Tier Classes MA ;
*Uses:    	out.Tier1_LengthOfResidence
            (CaseNumber PolicyNumber LengthOfResidence LengthOfResidenceFactor)
            out.Tier2_ShopperClass
            (CaseNumber PolicyNumber Shopper_Class Shopper_Factor)
            out.Tier3b_HomeFirstClass
            (CaseNumber PolicyNumber Home_First_Class Home_First_Factor)
            out.Tier4_PriorClaims
            (CaseNumber PolicyNumber Prior_Claims_Class Prior_Claims_Factor)
            out.Tier5_RCMVClass
            (CaseNumber PolicyNumber RCMV_Class RCMV_Factor)
            out.Tier6b_LapseInsurance
            (CaseNumber PolicyNumber Lapse_of_Insurance_Class Lapse_of_Insurance_Factor)
          	;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
    select distinct 
      a.CaseNumber
	, a.PolicyNumber, a.LengthOfResidence, b.Shopper_Class, c.Home_First_Class, d.Prior_Claims_Class
	, e.RCMV_Class, f.Lapse_of_Insurance_Class, round(LengthOfResidenceFactor*Shopper_Factor*Home_First_Factor*Prior_Claims_Factor*RCMV_Factor*Lapse_of_Insurance_Factor,0.001) as Tier_Factor

	from out.Tier1_LengthOfResidence as a
	inner join out.Tier2_ShopperClass as b
	on a.CaseNumber = b.CaseNumber
	inner join out.Tier3b_HomeFirstClass as c
	on a.CaseNumber = c.CaseNumber
	inner join out.Tier4_PriorClaims as d
	on a.CaseNumber = d.CaseNumber
	inner join out.Tier5_RCMVClass as e
	on a.CaseNumber = e.CaseNumber
	inner join out.Tier6b_LapseInsurance as f
	on a.CaseNumber = f.CaseNumber
	;
quit;
