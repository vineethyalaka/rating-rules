*Rule 590.1.a3 Construction CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyConstructionClass ProtectionClass)
            Source.ConstructionMapping
            (PropertyConstructionClass Type2)
            Fac.WindConstructionFactor
            (Factor Construction)
			;
*Creates: 	out.Wind_Construction_Factor;

proc sql;
    create table out.Wind_Construction_Factor as
	select distinct p.CaseNumber, p.PolicyNumber, pcf.Factor as Wind_Construction label="Wind Construction Factor"
	from in.Policy_Info as p

	inner join Source.ConstructionMapping as cm
	on upper(trim(left(p.PropertyConstructionClass))) = upper(trim(left(cm.PropertyConstructionClass)))

	inner join Fac.WindConstructionFactor as pcf
	on cm.Type2 = pcf.Construction
	;
quit;
