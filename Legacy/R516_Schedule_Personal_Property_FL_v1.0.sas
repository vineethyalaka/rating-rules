*OTW Rule 516 Scheduled Personal Property CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0461_flag HO_0461_01 - HO_0461_11)
            Fac.OTWPropertyUnschedPropFactor
            (Jewelry Furs Cameras Music Silverware Golf Art Stamps Coins Bicycles Misc)
			;
*Creates: 	out.OTW_R516_Sched_Personal_Prop;

proc sql;
	create table out.OTW_R516_Sched_Personal_Prop as
	select e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0461_flag = '0' then 0 else 1 end as HO_0461_Flag
		 , calculated HO_0461_Flag * sp.Jewelry   *e.HO_0461_01/100 as OTW_Jewelry
		 , calculated HO_0461_Flag * sp.Furs      *e.HO_0461_02/100 as OTW_Furs
		 , calculated HO_0461_Flag * sp.Cameras   *e.HO_0461_03/100 as OTW_Cameras
		 , calculated HO_0461_Flag * sp.Music     *e.HO_0461_04/100 as OTW_Music
		 , calculated HO_0461_Flag * sp.Silverware*e.HO_0461_05/100 as OTW_Silverware
		 , calculated HO_0461_Flag * sp.Golf      *e.HO_0461_06/100 as OTW_Golf
		 , calculated HO_0461_Flag * sp.Art       *e.HO_0461_07/100 as OTW_Art
		 , calculated HO_0461_Flag * sp.Stamps    *e.HO_0461_08/100 as OTW_Stamps
		 , calculated HO_0461_Flag * sp.Coins     *e.HO_0461_09/100 as OTW_Coins
		 , calculated HO_0461_Flag * sp.Bicycles  *e.HO_0461_10/100 as OTW_Bicycles
		 , calculated HO_0461_Flag * sp.Misc      *e.HO_0461_11/100 as OTW_Misc
	from Fac.OTWPropertySchedPropFactor as sp
		, in.Endorsement_Info as e
	;
quit;


proc sql;
	create table out.Wind_R516_Sched_Personal_Prop as
	select e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0461_flag = '0' then 0 
				when e.EXWIND = 1 then 0
				else 1 end as HO_0461_Flag
		 , calculated HO_0461_Flag * sp.Jewelry   *e.HO_0461_01/100 as Wind_Jewelry
		 , calculated HO_0461_Flag * sp.Furs      *e.HO_0461_02/100 as Wind_Furs
		 , calculated HO_0461_Flag * sp.Cameras   *e.HO_0461_03/100 as Wind_Cameras
		 , calculated HO_0461_Flag * sp.Music     *e.HO_0461_04/100 as Wind_Music
		 , calculated HO_0461_Flag * sp.Silverware*e.HO_0461_05/100 as Wind_Silverware
		 , calculated HO_0461_Flag * sp.Golf      *e.HO_0461_06/100 as Wind_Golf
		 , calculated HO_0461_Flag * sp.Art       *e.HO_0461_07/100 as Wind_Art
		 , calculated HO_0461_Flag * sp.Stamps    *e.HO_0461_08/100 as Wind_Stamps
		 , calculated HO_0461_Flag * sp.Coins     *e.HO_0461_09/100 as Wind_Coins
		 , calculated HO_0461_Flag * sp.Bicycles  *e.HO_0461_10/100 as Wind_Bicycles
		 , calculated HO_0461_Flag * sp.Misc      *e.HO_0461_11/100 as Wind_Misc
	from Fac.WindPropertySchedPropFactor as sp
		, in.Endorsement_Info as e
	;
quit;

proc sql;
	create table out.R516_Sched_Personal_Prop as
	select o.CaseNumber, o.PolicyNumber
	, round(o.OTW_Jewelry + w.Wind_Jewelry,1) as Jewelry
	, round(o.OTW_Furs + w.Wind_Furs,1) as Furs
	, round(o.OTW_Cameras + w.Wind_Cameras,1) as Cameras
	, round(o.OTW_Music + w.Wind_Music,1) as Music
	, round(o.OTW_Silverware + w.Wind_Silverware,1) as Silverware
	, round(o.OTW_Golf + w.Wind_Golf,1) as Golf
	, round(o.OTW_Art + w.Wind_Art,1) as Art
	, round(o.OTW_Stamps + w.Wind_Stamps,1) as Stamps
	, round(o.OTW_Coins + w.Wind_Coins,1) as Coins
	, round(o.OTW_Bicycles + w.Wind_Bicycles,1) as Bicycles
	, round(o.OTW_Misc + w.Wind_Misc,1) as Misc
	, calculated Jewelry + calculated Furs + calculated Cameras
	 + calculated Music + calculated Silverware + calculated Golf
	 + calculated Art + calculated Stamps + calculated Coins 
	 + calculated Bicycles + calculated Misc as Scheduled_Personal_Property
	from out.OTW_R516_Sched_Personal_Prop as o
	inner join out.Wind_R516_Sched_Personal_Prop as w
	on o.CaseNumber = w.CaseNumber
	;
quit;
