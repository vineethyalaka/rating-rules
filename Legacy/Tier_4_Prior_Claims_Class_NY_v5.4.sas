*Tier 4 Prior Claims Class CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate)
            in.loss_info
            (PolicyNumber LossDate LossCode LossAmount)
            Fac.ActofGod
            (ClaimTypeGod ActOfGodCode)
            Fac.TierPriorClaimClass
            (Class NumberofNAGMax NumberofNAGMin NumberofAOGMax NumberofAOGMin AgeType)
			;
*Creates: 	out.Tier4_PriorClaims;
*NB = 26-Oct-12	RB = 20-Dec-12;

proc sql;
	create table work.PriorClaims_a as
	select distinct l.casenumber, l.policynumber, p.PolicyTermYears, p.PolicyEffectiveDate, l.lossdate, l.LossCode, l.LossAmount, y.Group
			, Case 
				when p.PolicyTermYears = 3 and p.PolicyEffectiveDate - (365+366) >= '26OCT2012'd 
					and p.PolicyEffectiveDate - (366) < '26OCT2012'd then 2
				when y.PolicyYear = 2 and p.PolicyEffectiveDate - (365+366) >= '20DEC2012'd 
					and p.PolicyEffectiveDate - (366) < '20DEC2012'd then 2
				when p.PolicyTermYears = 3 and p.PolicyEffectiveDate - (366) >= '26OCT2012'd then 1
				when y.PolicyYear = 2 and p.PolicyEffectiveDate - (366) >= '20DEC2012'd then 1
				when p.PolicyTermYears = 2 and p.PolicyEffectiveDate - (366) >= '26OCT2012'd then 1
				when y.PolicyYear = 1 and p.PolicyEffectiveDate - (366) >= '20DEC2012'd then 1
					else 0 end as Age_Adj
			, (p.PolicyEffectiveDate - l.LossDate - 60)/365 - Calculated Age_Adj as LossAgeOrg
			, p.ageofNYpolicy - Calculated Age_Adj as Term_Org
			, case when t.ClaimTypeGod is null then 0
				else t.ClaimTypeGod
			  end as ClaimAOG
			, case when t.ClaimTypeGod is null then 1
				else 0
			  end as ClaimNAG
			, case when Calculated Term_Org = 1 then (p.PolicyIssueDate - l.LossDate)/365
				else Calculated LossAgeOrg
			  end as LossAge
			, case when calculated lossage>3 then 1 /*within 3 years*/
				else 2
			  end as AgeType
			, calculated ClaimAOG + calculated ClaimNAG as ClaimsTOT
	from in.policy_info as p
		inner join out.policy_year as y
		on p.casenumber = y.casenumber
		inner join in.loss_info as l
		on p.PolicyNumber=l.policynumber
		and p.casenumber=l.casenumber
		left join mapping.ActofGod_PriorClaims as t
		on l.LossCode=t.ActOfGodCode
	where l.LossAmount>0
		and (
			(Calculated Term_Org >= 4 and Calculated LossAgeOrg < 5 and Calculated LossAgeOrg >= 0)
		or  
			(Calculated Term_Org in (2,3) and Calculated LossAgeOrg < 5)
		or  
			(Calculated Term_Org = 1 and (p.PolicyIssueDate - l.LossDate)/365 >=0 and (p.PolicyIssueDate - l.LossDate)/365 <5)
			)
	;
quit;

*temp.PriorClaims_b;;
proc sql;
	create table work.PriorClaims_b as
	select a.casenumber, a.policynumber, a.Group, sum(a.claimAOG) as SumOfClaimAOG, sum(a.ClaimNAG) as SumOfClaimNAG, sum(a.ClaimsTOT) as SumOfClaimsTOt, a.AgeType
	from work.PriorClaims_a as a
	group by a.PolicyNumber, a.casenumber, a.AgeType, a.Group
	;
quit;

*temp.PriorClaims_c;;
proc sql;
	create table work.PriorClaims_c as
	select b.casenumber, b.PolicyNumber, b.AgeType, c.Class, b.group
	from work.PriorClaims_b as b
		inner join Fac.TierPriorClaimClass as c
		on  b.SumOfClaimsTOt <= c.NumberofCLAIMMax
		and b.SumOfClaimsTOt >= c.NumberofCLAIMMin
		and b.SumOfClaimNAG  <= c.NumberofNAGMax
		and b.SumOfClaimNAG  >= c.NumberofNAGMin
		and b.SumOfClaimAOG  <= c.NumberofAOGMax
		and b.SumOfClaimAOG  >= c.NumberofAOGMin
		and b.AgeType = c.AgeType
		and b.Group = c.Group
	;
quit;

*temp.PriorClaims_d;;
proc sql;
    create table work.PriorClaims_d as
	select c.casenumber, c.PolicyNumber, max(c.class) as MaxOfClass
	from work.PriorClaims_c as c
	group by c.PolicyNumber, c.casenumber
	;
quit;

*out.Tier4_PriorClaims;;
proc sql;
    create table out.Tier4_PriorClaims as
	select a.CaseNumber, a.PolicyNumber, case when d.MaxOfClass is Null then 1 else d.MaxOfClass end as Prior_Claims_Class
	from in.policy_info as a
	left join work.PriorClaims_d as d
	on a.PolicyNumber = d.PolicyNumber
	and a.CaseNumber = d.CaseNumber
    ;
quit;

