*HO4 Insurance Class (assigne No Hit to cases without DRC Info);
*Uses:    	in.policy_info
			(CaseNumber PolicyNumber)
			in.drc_info
			(CaseNumber PolicyNumber InsScore ScoreModel MatchCode)
			Fac.TierInsuranceClass
			(score_upper score_lower ScoreModel MatchCode Class)
			Fac.TierInsuranceClassFactor
			(Class Factor)
			;
*Creates: 	out.Tier_InsuranceScore;

proc sql;
	create table out.Tier_InsuranceScore as 
	select p.CaseNumber, p.PolicyNumber, d.MatchCode, d.ScoreModel, d.InsScore
		, case when d.CaseNumber = . then 20
			   when d.InsScore = . and d.MatchCode = 0 then 20
			   when d.InsScore = . and d.MatchCode = -1 then 21
			   when d.MatchCode = . then 21
			   else i.class 
		  end as insurance_class label="Insurance Class"
		, case when d.CaseNumber = . then "Missing DRC Info Record" 
			   else ""
		  end as Comment
	from in.policy_info as p 
		left join in.drc_info as d
		on p.CaseNumber=d.CaseNumber
		left join Fac.TierInsuranceClass as i
		on i.score_upper>d.InsScore
		and i.score_lower<=d.InsScore
		and i.ScoreModel=d.ScoreModel
		and i.MatchCode=d.MatchCode
	;
