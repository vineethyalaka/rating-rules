*HO6 Rule 590.2 Hurricane Base Premium NJ;
*Uses:    	out.Hurricane_Territory_Rate
            (CaseNumber PolicyNumber Hurricane_Territory_Rate)
            out.Wind_Form_Factor
            (CaseNumber PolicyNumber Wind_Policy_Form)
            out.Wind_AOI_Factor
            (CaseNumber PolicyNumber Wind_AOI)
            out.Wind_Prior_Claims
            (CaseNumber PolicyNumber Wind_Prior_Claims)
			;
*Creates: 	out.Hurricane_Base_Premium;

proc sql;
    create table out.Hurricane_Base_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Hurricane_Territory_Rate
		 , b.Wind_Policy_Form
		 , d.Wind_AOI
		 , h.Wind_Prior_Claims
		 , round(a.Hurricane_Territory_Rate*b.Wind_Policy_Form*d.Wind_AOI
		   *h.Wind_Prior_Claims,1) 
			as Hurricane_Base_Premium
	from out.Hurricane_Territory_Rate as a 
	inner join out.Wind_Form_Factor as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Wind_AOI_Factor as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Wind_Prior_Claims as h 
	on a.CaseNumber = h.CaseNumber
	;
quit;
