*Rule 602 Liability Other Insured Location Occupied by Insured CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PropertyUnitsInBuilding SecondaryResidences CoverageE CoverageF)
            Fac.LiabilityOtherInsuredCovEFactor
            (Factor Families CoverageE)
            Fac.LiabilityOtherInsuredCovFFactor
            (Factor Families CoverageF)
			;
*Creates: 	out.Liability_Other_Location;

proc sql;
    create table work.Liability_Other_Location_1 as
	select distinct a.CaseNumber
		 , a.PolicyNumber
		 , a.CoverageE
		 , a.CoverageF
		 , a.IDX_NO_M
		 , a.add_dwelling_type
		 , a.HO70_Flag
		 , (case when a.add_dwelling_type = 2 then 1 else 0 end) * ce.Factor as Secondary_Residence_E
		 , (case when a.add_dwelling_type = 2 then 1 else 0 end) * cf.Factor as Secondary_Residence_F
	from in.Additional_Residences_Info as a
	inner join Fac.LiabilityOtherInsuredCovEFactor as ce on a.Add_Number_of_Families = ce.Families and a.CoverageE = ce.CoverageE
	inner join Fac.LiabilityOtherInsuredCovFFactor as cf on a.Add_Number_of_Families = cf.Families and a.CoverageF = cf.CoverageF
;
quit;

proc sql;
	create table work.Liability_Other_Location_2 as
	select distinct a.CaseNumber
	  	 , sum(a.Secondary_Residence_E) as Secondary_E_Total
		 , sum(a.Secondary_Residence_F) as Secondary_F_Total
	from work.Liability_Other_Location_1 as a
	group by a.CaseNumber;
quit;

proc sql;
    create table out.Liability_Other_Location as
	select distinct p.CaseNumber
		 , p.PolicyNumber
		 , case when a.Secondary_E_Total = . then 0 else a.Secondary_E_Total end as Other_Location_E label="Other Location E"
		 , case when a.Secondary_F_Total = . then 0 else a.Secondary_F_Total end as Other_Location_F label="Other Location F"
	from in.Policy_Info as p
	left join work.Liability_Other_Location_2 as a
	on p.CaseNumber = a.CaseNumber
;
quit;
