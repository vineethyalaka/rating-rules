*Rule 613 Liability Snowmobile CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF)
            in.Endorsement_Info
            (CaseNumber PolicyNumber HO_2464_FLAG HO_2464_Make_Model_01 HO_2464_Make_Model_02 HO_2464_Make_Model_03)
            out.LiabilitySnowmobileCovEFactor
            (Factor CoverageE)
            Fac.LiabilitySnowmobileCovFFactor
            (Factor CoverageF)
			;
*Creates: 	out.Liability_Snowmobile;

proc sql;
    create table out.Liability_Snowmobile as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when upper(substr(trim(e.HO_2464_Make_Model_01), length(trim(e.HO_2464_Make_Model_01))-1,2)) = " Y" then 1
			else 0 end as Snowmobile_Light1
		 , case when upper(substr(trim(e.HO_2464_Make_Model_02), length(trim(e.HO_2464_Make_Model_02))-1,2)) = " Y" then 1
			else 0 end as Snowmobile_Light2
		 , case when upper(substr(trim(e.HO_2464_Make_Model_03), length(trim(e.HO_2464_Make_Model_03))-1,2)) = " Y" then 1
			else 0 end as Snowmobile_Light3
		 , case when e.HO_2464_Make_Model_01="" then 0 
			else 1 end as Snowmobile1
		 , case when e.HO_2464_Make_Model_02="" then 0 
			else 1 end as Snowmobile2
		 , case when e.HO_2464_Make_Model_03="" then 0 
			else 1 end as Snowmobile3
		 , calculated Snowmobile_Light1 + calculated Snowmobile_Light2 + calculated Snowmobile_Light3 as num_snowmobile_Light
		 , calculated Snowmobile1 + calculated Snowmobile2 + calculated Snowmobile3 as num_snowmobile 
		 , calculated num_snowmobile - calculated num_snowmobile_Light as num_snowmobile_noLight
		 , case when e.HO_2464_FLAG="1" then 
			sme.Factor_Light*calculated num_snowmobile_Light + sme.Factor_noLight*calculated num_snowmobile_noLight
			else 0 end as Snowmobile_E
		 , case when e.HO_2464_FLAG="1" then 
			smf.Factor_Light*calculated num_snowmobile_Light + smf.Factor_noLight*calculated num_snowmobile_noLight
			else 0 end as Snowmobile_F
	from in.Policy_Info as p
	inner join in.Endorsement_Info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.LiabilitySnowmobileCovEFactor as sme
	on p.CoverageE = sme.CoverageE
	inner join Fac.LiabilitySnowmobileCovFFactor as smf
	on p.CoverageF = smf.CoverageF
	;
quit;
