*Consent to rate Premium NC (Check if CTR form signed);
*Uses:    	out.NCRB_Premium
            (CaseNumber PolicyNumber NCRB_Premium)
            out.CTROTW_R407_2_Add_AMT_CovABCD
            (CaseNumber PolicyNumber HO_04_11)
            out.R301_ExperienceFactor
            (CaseNumber PolicyNumber Experience)
            out.CTR_Wind_Factor
            (CaseNumber PolicyNumber CTR_Wind_Factor)
            out.Tier_Factor
            (CaseNumber PolicyNumber Tier_Factor)
            out.OTW_R480_Mature_Owner
            (CaseNumber PolicyNumber Age)
            out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber Insurance_Class)
			out.Deviation_PriorClaims
            (CaseNumber PolicyNumber Deviation_Prior_Claims)
			fac.CTRChargeMinmum
			(factor)
			in.Policy_Info
			(CaseNumber PolicyNumber TerritoryA)
			;
*Creates: 	out.Final_Premium;

proc sql;
    create table out.Final_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , a.NCRB_Premium
		 , c.Experience
		 , d.CTR_Wind_Factor
		 , e.Tier_Factor
		 , p.CTR_signed
		 , p.CTR_Info_cutoff_date - p.policyeffectivedate as days_after_eff_date

		 /*Deviation*/
		 , case when 
		 	p.policyaccountnumber = 15004
			then 0.85
			else 1.0
			end as deviation

		 , round(
			round(
			 round(
			  a.NCRB_Premium
		 	 *c.Experience,1)
		 	*d.CTR_Wind_Factor,1)
		   *e.Tier_Factor,1) as CTR_Premium_s2

		 , j.factor as Min_CTR_Premium
		 , case when calculated CTR_Premium_s2 < j.factor then j.factor
		 	else calculated CTR_Premium_s2 end as CTR_Premium_s3

		 , case when calculated deviation < 1 then round(a.NCRB_Premium * calculated deviation,1)
		 		when calculated CTR_Premium_s3 > a.NCRB_Premium 
					and (p.CTR_signed = 1 or (calculated days_after_eff_date <= 30 and p.policytermyears = 1))
					then
						case when calculated CTR_Premium_s3 > 2.5*a.NCRB_Premium then floor(2.5*a.NCRB_Premium)
							else calculated CTR_Premium_s3 end
				else a.NCRB_Premium  
			end as final_premium
		  , q.cap as capping_amount

	from fac.ctrchargeminimum as j
		, out.NCRB_Premium as a 
	inner join out.R301_ExperienceFactor as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.CTR_Wind_Factor as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Tier_Factor as e 
	on a.CaseNumber = e.CaseNumber
	inner join in.Policy_Info as p
	on a.CaseNumber = p.CaseNumber
	inner join in.Capping_Info as q 
	on a.CaseNumber = q.CaseNumber
	;
quit;
