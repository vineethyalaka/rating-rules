*Rule 619 Liability Escaped Fuel;
*Uses:    	in.Production_Info
            (CaseNumber PolicyNumber Remediation HO_03_Escaped_Liquid_Fuel)
			in.Policy_Info
			(CaseNumber PolicyNumber Oil_Tank_Type)
			Fac.liabilityescapedliquidfuelfactor
			(Remediation HH_05_47 Oil_Tank_Type Factor)
*Creates: 	out.Liability_Escaped_Fuel;

proc sql;
    create table out.Liability_Escaped_Fuel as
	Select p.CaseNumber, p.PolicyNumber, m.Remediation, m.HO_03_Escaped_Liquid_Fuel,q.Factor as Fuel
	from in.Production_Info as p 
	inner join in.Policy_Info as i
		on p.CaseNumber = i.CaseNumber
	inner join in.datamainfo as m
		on p.CaseNumber = m.CaseNumber
	inner join Fac.liabilityescapedliquidfuelfactor as q
		on m.Remediation = q.Remediation
		and m.HO_03_Escaped_Liquid_Fuel = q.HH_05_47

	;
quit;
