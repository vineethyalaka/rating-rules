*HO6 Rule 301.4 Liability Premium CW;
*Uses:    	out.Liability_Base_Rate
			(CaseNumber PolicyNumber Coverage_E Coverage_F)
			out.Liability_Other_Location
			(CaseNumber PolicyNumber Other_Location_E Other_Location_F)
			out.Liability_Residence_Employees
			(CaseNumber PolicyNumber Domestic_Employees_E Domestic_Employees_F)
			out.Liability_Rented_Residence
			(CaseNumber PolicyNumber Rented_Residence_E Rented_Residence_F)
			out.Liability_Business_Pursuits
			(CaseNumber PolicyNumber Business_Pursuits_E Business_Pursuits_F)
			out.Liability_Personal_Injury
			(CaseNumber PolicyNumber Personal_Injury_E)
			out.Liability_Watercraft
			(CaseNumber PolicyNumber Watercraft_E Watercraft_F)
			out.Liability_Snowmobile
			(CaseNumber PolicyNumber Snowmobile_E Snowmobile_F)
			out.Liability_Dangerous_Dog
			(CaseNumber PolicyNumber Dangerous_Dog_E Dangerous_Dog_F)
			out.Liability_Escaped_Fuel
			(CaseNumber PolicyNumber Fuel)
			;
*Creates: 	out.Liability_Premium;

proc sql;
    create table out.Liability_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.Coverage_E
		 , a.Coverage_F
		 , b.Domestic_Employees_E
		 , b.Domestic_Employees_F
		 , b.Domestic_Employees_E + b.Domestic_Employees_F as Domestic_Employees
		 , c.Personal_Injury_E as Personal_Injury
		 , d.Watercraft_E
		 , d.Watercraft_F
		 , d.Watercraft_E + d.Watercraft_F as Watercraft
		 , e.Snowmobile_E
		 , e.Snowmobile_F
		 , e.Snowmobile_E + e.Snowmobile_F as Snowmobile
		 , f.Dangerous_Dog_E
		 , f.Dangerous_Dog_F
		 , f.Dangerous_Dog_E + f.Dangerous_Dog_F as Dangerous_Dog
		 , i.Other_Location_E
		 , i.Other_Location_F
		 , i.Other_Location_E + i.Other_Location_F as Other_Location
		 , j.Rented_Residence_E
		 , j.Rented_Residence_F
		 , j.Rented_Residence_E + j.Rented_Residence_F as Rented_Residence
		 , k.Business_Pursuits_E
		 , k.Business_Pursuits_F
		 , k.Business_Pursuits_E + k.Business_Pursuits_F as Business_Pursuits
		 , l.Fuel
		 , a.Coverage_E 
			+ a.Coverage_F 
			+ b.Domestic_Employees_E 
			+ b.Domestic_Employees_F 
			+ c.Personal_Injury_E 
			+ d.Watercraft_E 
			+ d.Watercraft_F
			+ e.Snowmobile_E 
			+ e.Snowmobile_F 
			+ f.Dangerous_Dog_E 
			+ f.Dangerous_Dog_F 
			+ i.Other_Location_E
			+ i.Other_Location_F
			+ j.Rented_Residence_E
			+ j.Rented_Residence_F
			+ k.Business_Pursuits_E
			+ k.Business_Pursuits_F
			+ l.Fuel
			as Liability_Premium format=10.4
		 , h.Tier_Factor*calculated Liability_Premium as Tiered_Liability_Premium
	from out.Liability_Base_Rate as a 
	inner join out.Liability_Residence_Employees as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Liability_Personal_Injury as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.Liability_Watercraft as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Liability_Snowmobile as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.Liability_Dangerous_Dog as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.Tier_Factor as h
	on a.CaseNumber = h.CaseNumber
	inner join out.Liability_Other_Location as i
	on a.CaseNumber = i.CaseNumber
	inner join out.Liability_Rented_Residence as j
	on a.CaseNumber = j.CaseNumber
	inner join out.Liability_Business_Pursuits as k
	on a.CaseNumber = k.CaseNumber
	inner join out.Liability_Escaped_Fuel as l
	on a.CaseNumber = l.CaseNumber
	;
quit;
