*OTW Rule 570 Limited Fungi Coverage CW2 HO6;
*Uses:    	in.ProductionInfo
			(CaseNumber PolicyNumber HO_04_31 HO_04_33 Property_Amount Liability_Amount)
			Fac.OTWPropertyFungiFactor
			(Factor)
			;
*Creates: 	out.OTW_R570_Limited_Fungi;

proc sql;
	create table out.OTW_R570_Limited_Fungi as
	select distinct 
		  a.CaseNumber
		, a.PolicyNumber
		, b.Factor as Limited_Fungi
	from in.Production_Info as a
	inner join in.datamainfo as d
	on a.casenumber = d.casenumber
	inner join Fac.OTWPropertyFungiFactor as b
		on d.HO_04_31 = b.HH_05_37
		and	d.HO_04_33 = b.HH_05_39
		and	d.Property_Amount = b.Property
		and	d.Liability_Amount = b.Liability
	;
quit;
