*HO6 Tier 1 Insurance Score Class PA - uses only TU score model;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance)
            in.Production_Info
            (CaseNumber PolicyNumber PriorInsurance)
            in.drc_info
			(CaseNumber PolicyNumber InsScore ScoreModel)
            out.Tier4_PriorClaims
			(CaseNumber PolicyNumber Prior_Claims_Class)
			Fac.TierClaimsCreditClass
			(PriorClaimClass Inslower Insupper Tier)
			Fac.TierClaimsCreditFactor
			(Tier Factor)
			;
*Creates: 	out.Tier2_ClaimsCredit_Tier out.Tier2_ClaimsCredit_Factor;

proc sql;
	create table work.Tier2_ClaimsCredit_Tier as
	select p.CaseNumber, p.PolicyNumber, c.Prior_Claims_Class, case when d.ScoreModel > 0 then 2 else 0 end as ScoreModel
		  ,	d.InsScore, p.Lapse_of_Insurance, pd.PriorInsurance
	from in.Policy_Info as p
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		inner join in.drc_info as d
		on p.CaseNumber = d.CaseNumber
		inner join out.Tier4_PriorClaims as c
		on p.CaseNumber = c.CaseNumber
	;
quit;

proc sql;
	create table out.Tier2_ClaimsCredit_Tier as
	select p.CaseNumber, p.PolicyNumber, p.Prior_Claims_Class, p.ScoreModel
		  ,	p.InsScore, p.Lapse_of_Insurance, p.PriorInsurance
		  , case when (p.Lapse_of_Insurance = 1 or p.PriorInsurance = 1) and t.Tier < 20 then t.Tier + 1
		 	  else t.Tier end as ClaimsCredit_Tier
	from work.Tier2_ClaimsCredit_Tier as p
		left join Fac.TierClaimsCreditClass as t
		on t.Insupper>p.InsScore
		and t.ScoreModel = p.ScoreModel
		and t.Inslower<=p.InsScore
		and t.PriorClaimClass=p.Prior_Claims_Class
	;
quit;

proc sql;
	create table out.Tier2_ClaimsCredit_Factor as
	select t.CaseNumber, t.PolicyNumber, t.ClaimsCredit_Tier, f.Factor as ClaimsCredit_Factor
	from out.Tier2_ClaimsCredit_Tier as t
	left join Fac.TierClaimsCreditFactor as f
	on t.ClaimsCredit_Tier = f.Tier
	;
quit;
