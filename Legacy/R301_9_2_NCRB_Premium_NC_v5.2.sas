

proc sql;
    create table out.NCRB_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		, a.Base_Premium
		, b.Rating_Factors_Premium
		, c.Property_Premium
		, d.Liability_Premium
		, case when a.Base_Premium+b.Rating_Factors_Premium+c.Property_Premium+d.Liability_Premium < f.Factor then f.Factor else
			a.Base_Premium+b.Rating_Factors_Premium+c.Property_Premium+d.Liability_Premium end as NCRB_Premium
	from Fac.ChargeMinimum as f, out.Base_Premium as a
	inner join out.rating_factors_premium as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Property_Premium as c
	on a.CaseNumber = c.CaseNumber
	inner join out.Liability_Premium as d
	on a.CaseNumber = d.CaseNumber
	;
quit;
