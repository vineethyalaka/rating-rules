*Wind Rule 408 Actual Cash Value Loss Settlement - Windstorm or Hail Losses to Roof Surfacing CW2 with HH-04-93 mandatory for age > 10 (HO-04-93);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate 
			PropertyYearRoofInstalled PropertyRoofTypeCode TerritoryH)
			in.Endorsement_info
			(CaseNumber PolicyNumber HO_0493_FLAG)
            Fac.WindHO0493Factor
            (Factor Roof_Age Roof_Age_Group Roof_type Territory_Group)
            Fac.WindTerritoryFactor
            (Territory Territory_Group)
			source.MappingRoofType
			;
*Creates: 	out.Wind_R408_ACV_Roof;

proc sql;
	create table work.Wind_R408_ACV_Roof as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.TerritoryH
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
		 , floor((case when p.PolicyTermYears=1 and &effst = 0 then p.PolicyIssueDate else p.PolicyEffectiveDate end
	 		- case when p.PropertyYearRoofInstalled <= 0 then date() else mdy(1,1,p.PropertyYearRoofInstalled) end)/365) as Roof_Age
		 , case when calculated Roof_Age >= 30 then '30+'
		 		when calculated Roof_Age < 0 then '-1'
				else left(put(calculated Roof_Age,3.))
			end	as Roof_Age_char
	from in.Policy_Info as p
	;

quit;



proc sql noprint;
select casenumber into:casenumber separated by ','
from in.policy_info;
quit;

LIBNAME sqlsrvqa oledb init_string = 
"Provider=SQLOLEDB.1; Integrated Security=SSPI; Persist Security Info=False;
Initial Catalog=ISO_PROD; read_lock_type=nolock; update_lock_type=nolock; READ_ISOLATION_LEVEL = RU; Data Source=cambosuwsql01" 
schema=dbo;

%let server = sqlsrvqa;



%macro DH200B_table(table_name,columns);
/*data dh_200_b&table_name.;*/
/* set &server..dh0200b&table_name.;*/
/*where rating_case_number in (&casenumber);*/
/*keep &columns.;*/
/*run;*/
data dh_200_c&table_name.;
 set &server..dh0200c&table_name.;
where rating_case_number in (&casenumber);
keep &columns.;
run;
data dh_200P;
 set  dh_200_c&table_name.;
run;
%mend();


%macro DH200Z_table(table_name,columns);
/*data dh_200_b&table_name.;*/
/* set &server..dh0200b&table_name.;*/
/*where prime_key_z in (&prime_key);*/
/*keep &columns.;*/
/*run;*/
data dh_200_c&table_name.;
 set &server..dh0200c&table_name.;
where prime_key_z in (&prime_key);
keep &columns.;
run;
data dh_200Z;
 set  dh_200_c&table_name.;
run;
%mend();


%DH200B_table(p,%nrstr(prime_key policy_number rating_case_number));

proc sql noprint;
select prime_key into:prime_key separated by ','
from dh_200P;
quit;

%DH200Z_table(z,%nrstr(PRIME_KEY_Z ML_40_LIMIT_02));




proc sql;
create table HH_0493_Flag as 
select rating_case_number as Casenumber
      ,policy_number as Policynumber
	  ,ML_40_LIMIT_02 as HH_04_93_FLAG
from dh_200P as p
inner join dh_200Z as z
on p.prime_key = z.prime_key_z
;
quit;



proc sql;

	create table out.Wind_R408_ACV_Roof as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode, p.Roof_Age, r.Roof_Age_Group, r.Territory_Group
		 , case 
		       when input(e.HO_0493_FLAG,2.) = 1 then r.HO_04_93 
               when p.Roof_Age > 10 and HH_04_93_FLAG = 1 then r.HO_04_93 
               else 1 
           end as HO_04_93
	from work.Wind_R408_ACV_Roof as p
	inner join in.Endorsement_info as e
	on p.CaseNumber = e.CaseNumber
	inner join Fac.WindTerritoryFactor as t
	on p.TerritoryH = t.Territory
	left join source.MappingRoofType as mrt
	on lower(trim(left(p.PropertyRoofTypeCode))) = lower(trim(left(mrt.PropertyRoofType)))
	inner join Fac.WindRoofFactor as r
	on  lower(trim(left(r.Roof_type))) = lower(trim(left(mrt.Type)))
	and p.Roof_Age_char = r.Roof_Age
	and t.Territory_Group = r.Territory_Group
	inner join HH_0493_Flag as h
	on p.casenumber = h.casenumber 
	;
quit;