*OTW Rule 590 Mine Subsidence;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO2388_FLAG)
            in.Policy_Info
            (CaseNumber PolicyNumber CoverageA)
            Fac.OTWPropertyMineSubsidenceFactor
            (AOI_lower AOI_upper Factor)
			;
*Creates: 	out.OTW_590_Mine_Subsidence;

proc sql;
	create table counties as
	select CaseNumber, tranwrd(upcase(Name),'COUNTY','') as County
	, CoverageA
	from in.Policy_Info;
quit;
proc sql;
	create table out.OTW_590_Mine_Subsidence as
	select e.CaseNumber, e.PolicyNumber
		 , e.HO2388_FLAG
		 , p.County
		 , p.CoverageA
		 , i.Indicator
		 , ms.Factor
		 , case when e.HO2388_FLAG="1" and i.Indicator = 1
			then round(ms.Factor,1) else 0 end as Mine_Subsidence
	from in.Endorsement_Info as e
		 inner join counties as p
		 on e.CaseNumber = p.CaseNumber
		 inner join Fac.OTWPropertyMineSubsidenceFactor as ms
		 on p.CoverageA >= ms.AOI_lower
		 and p.CoverageA <= ms.AOI_upper
		 inner join Fac.OTWMineCountyIndicator as i
		 on p.County = i.County
	;
quit;
