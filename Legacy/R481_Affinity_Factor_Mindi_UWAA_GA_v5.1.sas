
proc sql;
      create table out.R481_Affinity as
      select distinct a.CaseNumber, a.PolicyNumber, a.PolicyAccountNumber, a.partnerflag, AutoPolicyNumber, AutoPolicyFlag,
      case 
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

          else b.Factor end as Affinity label="Affinity"
		case 
		        when a.PolicyAccountNumber=14200 and e.partner_ee=1 and e.employee=1 then 0.10 /*employment discount*/
		        else 0
		end as Employee_fac

      from in.policy_info as a
	  left join out.employee as e
      on a.casenumber = e.casenumber
      left join Fac.AffinityFactor as b
      on a.PolicyAccountNumber <= b.AccNumMax
      and a.PolicyAccountNumber >= b.AccNumMin
      and (a.AutoPolicyNumber is null and b.AutoPolicyFlag="N"
            or a.AutoPolicyNumber is not null and b.AutoPolicyFlag="Y")
      ;
quit;
