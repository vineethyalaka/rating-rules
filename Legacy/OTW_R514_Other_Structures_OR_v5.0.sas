*OTW Rule 514 Other Sructures CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0448_Flag HO_0448_ADDL_LIMIT_01)
			;
*Creates: 	out.OTW_R514_Other_Structures;

proc sql;
	create table out.OTW_R514_Other_Structures as
	select distinct e.CaseNumber,e.PolicyNumber
		 , case when e.HO_0448_Flag="1" then (1-e.EXWIND)*round(e.HO_0448_ADDL_LIMIT_01*1.8/1000,1)
		 		when e.HO_0448_Flag="1" and e.HO_0448_ADDL_LIMIT_01 < 0 then (1-e.EXWIND)*round(e.HO_0448_ADDL_LIMIT_01*.9/1000,1)
				else 0 end as Other_Structures	
	from in.Endorsement_Info as e
	;
quit;
