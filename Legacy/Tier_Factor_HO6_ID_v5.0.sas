*HO6 Tier Factor CW;
*Uses:    	out.Tier2_ClaimsCredit_Factor
            (CaseNumber PolicyNumber ClaimsCredit_Tier ClaimsCredit_Factor)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier Occupancy_Tier_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
            cs.CaseNumber, cs.PolicyNumber
          , cs.InsuranceScore_Tier, cs.InsuranceScore_Factor
		  , pc.Prior_Claims_Class, pc.PriorClaims_Factor
		  , round(cs.InsuranceScore_Factor * pc.PriorClaims_Factor,0.000001) as Tier_Factor
    from out.Tier2_InsuranceScore_Factor as cs
	inner join out.tier4_priorclaims_factor as pc
	on cs.CaseNumber = pc.CaseNumber
	;
quit;
