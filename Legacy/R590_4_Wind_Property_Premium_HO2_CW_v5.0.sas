*Rule 590.4 Wind Property Premium CW (HO2);
*Uses:    	out.Wind_R512_Loss_of_Use
			(CaseNumber PolicyNumber Coverage_D)
			out.Wind_R515_A_Incr_CovC
			(CaseNumber PolicyNumber Coverage_C)
			;
*Creates: 	out.Wind_Property_Premium;

proc sql;
    create table out.Wind_Property_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Coverage_D
		 , e.Coverage_C
		 , a.Coverage_D
			+ e.Coverage_C
			as Wind_Property_Premium format=10.4
	from out.Wind_R512_Loss_of_Use as a 
	inner join out.Wind_R515_A_Incr_CovC as e 
	on a.CaseNumber = e.CaseNumber
	;
quit;
