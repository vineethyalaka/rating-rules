*Tier 6 a Lapse Class HO6 NJ;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance end_code_3_PI end_code_3_NH New_Home_Flag)
            in.Production_Info
            (CaseNumber PolicyNumber Lapse_Date_Used)
            Fac.TierLapseDays
            (LapseDays)
			;
*Creates: 	out.Tier6_Lapse;

proc sql;
    create table out.Tier6_Lapse as
	select distinct p.CaseNumber, p.PolicyNumber
		  ,pd.Lapse_Date_Used
		  ,p.Lapse_of_Insurance
		  ,p.end_code_3_NH as ProposeEffDate format=date9.
		  ,p.end_code_3_PI as CurrentExpDate format=date9.
		  ,case when p.end_code_3_PI = . then p.end_code_3_NH - mdy(1,1,1900)
		   		else p.end_code_3_NH - p.end_code_3_PI 
		   end as period
		  ,l.LapseDays
		  ,case when p.Lapse_of_Insurance = 1 then 2
				when pd.Lapse_Date_Used = 0 then 1
				when calculated period  < l.LapseDays then 1 /*when calculated period -1 <= l.LapseDays then 1 */
				else 2
		   end as Lapse_class
	from in.Policy_Info as p
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		, Fac.TierLapseDays as l
    ;
quit;
