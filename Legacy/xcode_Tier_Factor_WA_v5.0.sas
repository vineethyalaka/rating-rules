*Tier Factor CW (DRC);
*Uses:    	out.Tier_Classes
            (CaseNumber PolicyNumber Insurance_Class Shopper_Class Home_First_Class Prior_Claims_Class RCMV_Class Lapse_of_Insurance_Class)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class DRC_Factor)
            Fac.TierTable
            (Factor InsuranceScore Shopper Auto Prior RCMV Lapse)
			;
*Creates: 	out.Tier_Factor;



proc sql;
    create table out.Tier_Factor as
	select distinct c.CaseNumber, c.PolicyNumber
          ,c.Insurance_Class, c.Shopper_Class, c.Home_First_Class, c.Prior_Claims_Class
          ,c.RCMV_Class
		  , t.Factor as IRM_Factor
		  ,d.DRC_Class, d.DRC_Factor
		  ,e.Occupancy_Tier, e.Occupancy_Tier_Factor
		  ,round(t.Factor * e.Occupancy_Tier_Factor * d.DRC_Factor,0.000001) as Tier_Factor_current
		  ,case when ytable.tierfactor=. then calculated Tier_Factor_current else ytable.tierfactor end as Tier_Factor 

    from out.Tier_Classes as c
	inner join Fac.TierTable as t
	on  c.Insurance_Class          = t.InsuranceScore
	and c.Shopper_Class            = t.Shopper
	and c.Home_First_Class         = t.Auto
	and c.Prior_Claims_Class       = t.Prior
	and c.RCMV_Class               = t.RCMV
	and c.Lapse_of_Insurance_Class = t.Lapse
	inner join out.Tier7_DRC as d
	on c.CaseNumber = d.CaseNumber
	inner join out.Tier8_Occupancy_Tier as e
	on c.CaseNumber = e.CaseNumber
	left join in.ytable_ho3_tier as ytable
	on c.CaseNumber = ytable.CaseNumber
	;
quit;