proc sql;
    create table out.Tier_Factor_CovC_InsRes as
	select  o.CaseNumber, o.PolicyNumber
          , o.Occupancy_Tier, i.insurance_class 
          , t.Factor as  Tier_Factor
    from out.Tier_Occupancy as o, out.Tier_InsuranceScore as i, fac.tiercovcinsuranceresidentsfactor as t
	where o.CaseNumber = i.CaseNumber
	and o.Occupancy_Tier = t.ResidentsTier
	and i.Insurance_Class = t.InsuranceScoreTier
	;
quit;
