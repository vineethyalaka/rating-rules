proc sql;
	create table work.Occ as
	select distinct p.CaseNumber, p.PolicyNumber, case when i.Residents_TotalCount is null then p.Residents_TotalCount else i.Residents_TotalCount end as Residents
	from in.policy_info as p
		inner join out.policy_year as y
		on p.CaseNumber = y.CaseNumber
		left join in.priorterm_info as i
		on y.PolicyNumber = i.PolicyNumber
		and (y.PolicyTermYears - y.PolicyYear) = i.PolicyTermYears
	;
quit;

proc sql;
	create table out.Tier3_Occupancy_Tier as 
	select distinct o.CaseNumber, o.PolicyNumber, o.Residents
		, f.tier as Occupancy_Tier
	from work.Occ as o
		left join Fac.TierOccupancyClass as f
		on f.Occupants_upper>=o.Residents
		and f.Occupants_lower<=o.Residents
	;
quit;
