*OTW Rule 408 Actual Cash Value Loss Settlement - Windstorm or Hail Losses to Roof Surfacing CW (HO-04-93);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0493_FLAG)
            Fac.OTWHO0493Factor
            (Factor)
			;
*Creates: 	out.OTW_R408_ACV_Roof;

proc sql;
	create table out.Wind_R408_ACV_Roof as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0493_FLAG = '1' then hf493.Factor else 1 end as HO_04_93
	from in.Endorsement_Info as e, Fac.WINDHO0493Factor as hf493
	;
quit;
