*HO6 Tier 1 Insurance Score Class CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance)
            in.Production_Info
            (CaseNumber PolicyNumber PriorInsurance)
            in.drc_info
			(CaseNumber PolicyNumber InsScore ScoreModel)
            out.Tier4_PriorClaims
			(CaseNumber PolicyNumber Prior_Claims_Class)
			Fac.TierClaimsCreditClass
			(PriorClaimClass ScoreModel Inslower Insupper Tier)
			Fac.TierClaimsCreditFactor
			(Tier Factor)
			;
*Creates: 	out.Tier2_ClaimsCredit_Tier out.Tier2_ClaimsCredit_Factor;

*revised by Di 08/30/2016: tier increment;


proc sql;

select casenumber into :case_number separated by ',' from in.policy_info;
connect to oledb as dblink
(init_string = 
"Provider=SQLOLEDB.1; Integrated Security=SSPI; Persist Security Info=False;
Initial Catalog=ISO_PROD; read_lock_type=nolock; update_lock_type=nolock; READ_ISOLATION_LEVEL = RU; Data Source=CAMBOSQASQL21\SQL01" );

create table out.Tier2_ClaimsCredit_Tier as


	select p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, c.Prior_Claims_Class
		 , d.ScoreModel, d.InsScore, p.Lapse_of_Insurance, pd.PriorInsurance
		 , case when HD030_Indicatorcount is missing then 0 else HD030_Indicatorcount end as HD030_Indicator
		 , case when ((p.Lapse_of_Insurance = 1 or pd.PriorInsurance = 1) and /*p.PolicyTermYears < 5*/ calculated HD030_Indicator=0) and t.Tier < 20 then t.Tier + 1 /*if HD-030 doesn't exist, credit tier increases by 1*/
		 	else t.Tier
			end as ClaimsCredit_Tier
	from in.Policy_Info as p 		
		left join connection to dblink (select casenumber,count(*) as HD030_Indicatorcount from V3RatingResultEndorsements where casenumber in (&case_number) and endorsement='HD-030' group by casenumber) as ed
		on p.casenumber=ed.casenumber
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		inner join in.drc_info as d
		on p.CaseNumber = d.CaseNumber
		inner join out.Tier4_PriorClaims as c
		on p.CaseNumber = c.CaseNumber
		left join Fac.TierClaimsCreditClass as t
		on t.Insupper>d.InsScore
		and t.Inslower<=d.InsScore
		and t.ScoreModel=d.ScoreModel
		and t.PriorClaimClass=c.Prior_Claims_Class
	;

disconnect from dblink;

quit;

proc sql;
	create table out.Tier2_ClaimsCredit_Factor as
	select t.CaseNumber, t.PolicyNumber, t.ClaimsCredit_Tier, f.Factor as ClaimsCredit_Factor
	from out.Tier2_ClaimsCredit_Tier as t
	left join Fac.TierClaimsCreditFactor as f
	on t.ClaimsCredit_Tier = f.Tier
	;
quit;
