*Rule 301.8 Affinity Adjusted Premium CW Costco;
*Uses:    	out.Expense_Adjusted_Premium
			(CaseNumber PolicyNumber Expense_Adjusted_Premium)
			out.R481_Affinity
			(CaseNumber PolicyNumber Affinity)
			;
*Creates: 	out.Affinity_Adjusted_Premium;
/* DG:      for Costco Discount Calculation  
			Add Quick Rate*/

proc sql;
    create table out.Affinity_Adjusted_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.QuickRate_Adjusted_Premium
		 , b.affinity
/*		 , round(a.Expense_Adjusted_Premium*b.affinity,1) as Affinity_Adjusted_Premium */
		 ,	case 
				when b.affinity = 0.85 then round(a.QuickRate_Adjusted_Premium - round(a.QuickRate_Adjusted_Premium*0.1,1) - round(a.QuickRate_Adjusted_Premium*0.05,1), 1)
				else round(a.QuickRate_Adjusted_Premium*b.affinity,1)
			end as Affinity_Adjusted_Premium

	from out.QuickRate_Adjusted_Premium as a 
	inner join out.Affinity_Fac as b 
	on a.CaseNumber = b.CaseNumber
	;
quit;
