*HO4/6 Tier 3 Occupancy Class CW;
*Uses:    	in.policy_info
			(CaseNumber PolicyNumber Residents_TotalCount)
			Fac.TierOccupancyFactor
			(Occupants_upper Occupants_lower Tier Factor)
			;
*Creates: 	out.Tier3_Occupancy_Tier;

proc sql;
	create table out.OTW_R409_Number_of_Residents as 
	select distinct p.CaseNumber, p.PolicyNumber, p.Residents_TotalCount
		, f.Factor as Number_of_Residents
	from in.policy_info as p
		inner join Fac.OTWResidentsFactor as f
		on f.NumberResidentsUpper>=p.Residents_TotalCount
		and f.NumberResidentsLower<=p.Residents_TotalCount
	;
quit;
