*OTW Rule 470 Age of Electrical System;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate PropertyYearWiringInstalled)
            Fac.OTWAgeOfElectricalFactor
            (Factor Years_lower Years_upper)
			;
*Creates: 	out.OTW_R470_Age_of_Electrical_System;

proc sql;
	create table work.OTW_R470_Age_of_ES as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.PropertyYearWiringInstalled
		 , (case when p.PolicyTermYears=1 then year(p.PolicyIssueDate) else year(p.PolicyEffectiveDate) end
	 		- case when p.PropertyYearWiringInstalled = . then year(date()) else p.PropertyYearWiringInstalled end) as Electrical_System_Age
	from in.Policy_Info as p
	;

	create table out.OTW_R470_Age_of_ES as
	select p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.PropertyYearWiringInstalled
		 , ahf.Factor as Age_of_Electrical_System label="Age of Electrical System"
	from work.OTW_R470_Age_of_ES as p
	inner join Fac.OTWAgeOfElectricalFactor as ahf 
	on  p.Electrical_System_Age >= ahf.Years_lower
	and p.Electrical_System_Age <=  ahf.Years_upper
	;
quit;
