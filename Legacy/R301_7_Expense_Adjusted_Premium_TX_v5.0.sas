*Rule 301.7 Expense Adjusted Premium CW;
*Uses:    	out.Tiered_Premium
			(CaseNumber PolicyNumber Tiered_Premium)
			out.Property_Premium
			(CaseNumber PolicyNumber Property_Premium)
			out.R301_ExperienceFactor
			(CaseNumber PolicyNumber Experience)
			Fac.OTWPolicyFeeFactor
			(Factor)
			;
*Creates: 	out.Expense_Adjusted_Premium;

proc sql;
    create table out.Expense_Adjusted_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Tiered_Premium
		 , b.Property_Premium
		 , a.Liability_Premium
		 , c.Factor as PolicyFee label="PolicyFee"
		 , d.Experience
		 , round(a.Liability_Premium,1) + d.Experience*(a.OTW_Premium + b.Property_Premium + c.Factor)
			as Expense_Adjusted_Premium format=10.4
	from Fac.OTWPolicyFeeFactor as c
	, out.Tiered_Premium as a 
	inner join out.Property_Premium as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.R301_ExperienceFactor as d 
	on a.CaseNumber = d.CaseNumber
	;
quit;
