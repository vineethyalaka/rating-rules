*Tier 8 Underwriting Class PA;
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber MatchCode ScoreModel InsScore Insurance_Clas)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class DRC_Factor)
			;
*Creates: 	out.Tier8_UW_Class;


proc sql;
	create table out.Tier8_UW_Class as
	select distinct a.CaseNumber, a.PolicyNumber, a.Insurance_Class
		, case when b.DRC_Class = 1 then "A"
			   when b.DRC_Class = 2 then "B"
			   when b.DRC_Class = 3 then "C"
			   when b.DRC_Class = 4 then "D"
			   else "E"
		  end as DRC_Name
		, trim(left(put(a.Insurance_Class,8.)||calculated DRC_Name)) as Underwriting_Class
	from out.Tier1_InsuranceScore as a
		left join out.Tier7_DRC as b
		on a.CaseNumber = b.CaseNumber
	;
quit;
