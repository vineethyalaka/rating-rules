*Rule 301.1.a1 Territory Base Rate NJ (vary by UW Company);
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryA UNDERWRITING_CO)
            Fac.OTWTerritoryFactor
            (Territory Territory_Rate_07 Territory_Rate_21)
			;
*Creates: 	out.OTW_Territory_Rate;

proc sql;
    create table out.OTW_Territory_Rate as
	select p.CaseNumber, p.PolicyNumber
	, case when p.UNDERWRITING_CO in (2,7) then t.Territory_Rate_07
			when p.UNDERWRITING_CO = 21 then t.Territory_Rate_21 
		end as OTW_Territory_Rate label="OTW Territory Base Rate"
	from in.policy_info as p
	inner join Fac.OTWTerritoryFactor as t
	on p.TerritoryA = t.Territory
	;
quit;
