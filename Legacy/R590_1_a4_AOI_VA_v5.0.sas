*Rule 590.1.a4 AOI VA;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber CoverageA)
            in.endorsement_info
            (CaseNumber PolicyNumber HO_04_56_pct HO256_PERCENT_EST_REPL)
            Fac.WindHO0456Factor
            (Factor1 Percent)
            Fac.WindAOIFactor
            (AOI_Lower AOI_Upper Factor_Lower Factor_Upper)
			;
*Creates: 	out.Coverage out.WindW_AOI_Factor;

proc sql;
    create table out.Coverage as
	select p.CaseNumber, p.PolicyNumber, case when e.HO_0456_FLAG='0' and e.HO256_FLAG='0' then p.CoverageA 
		else round(h.Factor1*i.Factor1*p.CoverageA,1000) end as Coverage
	from in.policy_info as p
	inner join in.endorsement_info as e
	on p.CaseNumber = e.CaseNumber
	inner join Fac.WindHO0456Factor as h
	on e.HO_04_56_pct = h.Percent
	inner join Fac.WindHO0522Factor as i
	on (case when e.HO256_PERCENT_EST_REPL < 1 then e.HO256_PERCENT_EST_REPL*100 else e.HO256_PERCENT_EST_REPL end) =i.Percent  
	
	;
quit;

proc sql;
    create table out.Wind_AOI_Factor as
	select t.CaseNumber, t.PolicyNumber, t.Coverage, f.AOI_Lower, f.AOI_Upper, f.Factor_Lower, f.Factor_Upper
	      ,(t.Coverage - f.AOI_Lower)/(f.AOI_Upper - f.AOI_Lower) as I1
		  ,(f.AOI_Upper - t.Coverage)/(f.AOI_Upper - f.AOI_Lower) as I2
		  ,(calculated I1 + abs(calculated I1))/(2*calculated I1) as Ind
		  ,round((f.Factor_Upper*calculated I1 + f.Factor_Lower*calculated I2)*calculated Ind
		          + (f.Factor_Lower + f.Factor_Upper*(t.Coverage - f.AOI_Lower)/10000)*(1 - calculated Ind)
                , 0.001) as Wind_AOI label="Wind AOI Factor"
	from out.Coverage as t
	inner join Fac.WindAOIFactor as f
	on (t.Coverage <= f.AOI_Upper or f.AOI_Upper=0) 
	and t.Coverage > f.AOI_Lower
	;
quit;
