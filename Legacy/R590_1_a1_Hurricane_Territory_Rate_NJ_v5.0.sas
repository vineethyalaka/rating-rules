*Rule 590.1.a1 Hurricane Territory Base Rate CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryA)
            in.endorsement_info
            (CaseNumber PolicyNumber EXWind)
            Fac.HurricaneTerritoryFactor
            (Territory Territory_Rate)
			;
*Creates: 	out.Hurricane_Territory_Rate;

proc sql;
    create table out.Hurricane_Territory_Rate as
	select distinct p.CaseNumber, p.PolicyNumber, (1-e.EXWind)*t.Territory_Rate as Hurricane_Territory_Rate label="Hurricane Territory Base Rate"
	from in.policy_info as p
	inner join in.endorsement_info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.HurricaneTerritoryFactor as t
	on p.TerritoryA = t.Territory
	;
quit;
