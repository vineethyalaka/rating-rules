*Tier 2 Shopper Class MA;
*Uses:    	in.policy_info
			(CaseNumber PolicyNumber PolicyIssueDate)
			in.shopper_info
			(CaseNumber PolicyNumber end_code_2 end_code_3_PI end_code_3_NH)
            Fac.TierShopperClass
            (Shopper_lower Shopper_upper class factor)
			;
*Creates: 	out.Tier2_ShopperClass;

proc sql;
	create table out.Tier2_ShopperClass as 
	select distinct q.CaseNumber, q.PolicyNumber
		, t.class as Shopper_Class label="Shopper Class", t.Factor as Shopper_Factor label="Shopper Factor"
		from (
			select p.CaseNumber, p.PolicyNumber
				, case when s.end_code_3_PI > .
					then s.end_code_3_PI
					else s.end_code_3_NH
					end
					as Policy_Start_Date
				, case when s.end_code_2 is null
					then p.PolicyIssueDate
					else s.end_code_2
					end
					as Shopping_Date
				, case when calculated Policy_Start_Date is null
					then 8
					else (calculated Policy_Start_Date - calculated Shopping_Date)
					end
					as Number_of_Shopping_Days
			from in.policy_info as p
				inner join in.shopper_info as s
				on p.CaseNumber = s.CaseNumber
				and p.PolicyNumber = s.PolicyNumber
		) as q
		inner join Fac.TierShopperClass as t
		on q.Number_of_Shopping_Days >= t.Shopper_lower
		and q.Number_of_Shopping_Days < t.Shopper_upper
	;
quit;
