*Rule 301.1.b5 Special Personal Property PA (HO 00 15 and HH 00 15);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0015_FLAG)
            in.Production_Info
            (CaseNumber PolicyNumber HH0015)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.OTWHH0015Factor
            (Factor AdvInd)
			;
*Creates: 	out.OTW_HH_00_15_Factor;

proc sql;
    create table out.OTW_HH_00_15_Factor as
	select  distinct e.CaseNumber, e.PolicyNumber, e.HO_0015_FLAG, pd.HH0015, hf15.Factor as OTW_HH_00_15 label = "OTW HH-00-15"
	from in.Endorsement_Info as e
	inner join in.Production_Info as pd
	on e.CaseNumber = pd.CaseNumber
    inner join out.OTW_R486_Advantage as a
	on e.CaseNumber = a.CaseNumber
	inner join Fac.OTWHH0015Factor as hf15
	on (a.AdvInd = hf15.AdvInd) and (e.HO_0015_FLAG = hf15.HO_00_15) and (pd.HH0015 = hf15.HH_00_15)
	;
quit;
