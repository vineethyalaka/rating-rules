*Rule 590.1.b3 Loss Settlement VA (HO 05 22);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO256_PERCENT_EST_REPL)
            Fac.WindHO0522Factor
            (Factor2 Percent)
			;
*Creates: 	out.Wind_Loss_Settlement_Factor;

proc sql;
    create table out.Wind_Loss_Settlement_Factor_2 as
	select distinct e.CaseNumber, e.PolicyNumber, case when e.HO256_FLAG='0' then 1 else hf522.Factor2 end as Wind_HO_05_22 label="Wind HO-05-22"
	from in.Endorsement_Info as e
	inner join Fac.WindHO0522Factor as hf522
	on (case when e.HO256_PERCENT_EST_REPL < 1 then e.HO256_PERCENT_EST_REPL*100 else e.HO256_PERCENT_EST_REPL end) = hf522.Percent
	;
quit;
