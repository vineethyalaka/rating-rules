*Rule 612 Liability outboard motors and Watercraft CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF)
            in.Endorsement_Info
            (CaseNumber PolicyNumber 
			HO_2475_IN_USE_TO_MONTH_01 HO_2475_IN_USE_FROM_MONTH_01 HO_2475_HORSEPOWER_SPEED_01 HO_2475_BOATTYPE_01
			HO_2475_IN_USE_TO_MONTH_02 HO_2475_IN_USE_FROM_MONTH_02 HO_2475_HORSEPOWER_SPEED_02 HO_2475_BOATTYPE_02
			HO_2475_IN_USE_TO_MONTH_03 HO_2475_IN_USE_FROM_MONTH_03 HO_2475_HORSEPOWER_SPEED_03 HO_2475_BOATTYPE_03
			HO_2475_IN_USE_TO_MONTH_04 HO_2475_IN_USE_FROM_MONTH_04 HO_2475_HORSEPOWER_SPEED_04 HO_2475_BOATTYPE_04
			HO_2475_IN_USE_TO_MONTH_05 HO_2475_IN_USE_FROM_MONTH_05 HO_2475_HORSEPOWER_SPEED_05 HO_2475_BOATTYPE_05)
            out.LiabilityWatercraftCovEFactor
            (Factor Speed_upper Speed_lower MotorType CoverageE)
            Fac.LiabilityWatercraftCovFFactor
            (Factor Speed_upper Speed_lower MotorType CoverageF)
			;
*Creates: 	out.Liability_Watercraft;

%macro WaterC (num);
	proc sql;
	    create table work.Liability_Watercraft_&num as
		select distinct p.CaseNumber, p.PolicyNumber
			 , round(wce.Factor*(abs(e.HO_2475_IN_USE_TO_MONTH_0&num - e.HO_2475_IN_USE_FROM_MONTH_0&num)+1)/12,1) as Watercraft_E_&num
			 , round(wcf.Factor*(abs(e.HO_2475_IN_USE_TO_MONTH_0&num - e.HO_2475_IN_USE_FROM_MONTH_0&num)+1)/12,1) as Watercraft_F_&num
		from in.Policy_Info as p
		inner join in.Endorsement_Info as e
		on e.CaseNumber = p.CaseNumber
		inner join Fac.LiabilityWatercraftCovEFactor as wce
		on e.HO_2475_HORSEPOWER_SPEED_0&num <= wce.Speed_upper
		and e.HO_2475_HORSEPOWER_SPEED_0&num >= wce.Speed_lower
		and case when e.HO_2475_BOATTYPE_0&num = "" then "none" else lower(e.HO_2475_BOATTYPE_0&num) end = wce.MotorType
		and p.CoverageE = wce.CoverageE
		inner join Fac.LiabilityWatercraftCovFFactor as wcf
		on e.HO_2475_HORSEPOWER_SPEED_0&num <= wcf.Speed_upper
		and e.HO_2475_HORSEPOWER_SPEED_0&num >= wcf.Speed_lower
		and case when e.HO_2475_BOATTYPE_0&num = "" then "none" else lower(e.HO_2475_BOATTYPE_0&num) end = wcf.MotorType
		and p.CoverageF = wcf.CoverageF
		;
	quit;
%mend;
%WaterC (1);
%WaterC (2);
%WaterC (3);
%WaterC (4);
%WaterC (5);

proc sql;
	create table out.Liability_Watercraft as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Watercraft_E_1, a.Watercraft_F_1
		 , b.Watercraft_E_2, b.Watercraft_F_2
		 , c.Watercraft_E_3, c.Watercraft_F_3
		 , d.Watercraft_E_4, d.Watercraft_F_4
		 , e.Watercraft_E_5, e.Watercraft_F_5
		 , (a.Watercraft_E_1 + b.Watercraft_E_2 + c.Watercraft_E_3 + d.Watercraft_E_4 + e.Watercraft_E_5) as Watercraft_E
		 , (a.Watercraft_F_1 + b.Watercraft_F_2 + c.Watercraft_F_3 + d.Watercraft_F_4 + e.Watercraft_F_5) as Watercraft_F
	from work.Liability_Watercraft_1 as a
	inner join work.Liability_Watercraft_2 as b
	on a.CaseNumber = b.CaseNumber
	inner join work.Liability_Watercraft_3 as c
	on a.CaseNumber = c.CaseNumber
	inner join work.Liability_Watercraft_4 as d
	on a.CaseNumber = d.CaseNumber
	inner join work.Liability_Watercraft_5 as e
	on a.CaseNumber = e.CaseNumber
	;
quit;
