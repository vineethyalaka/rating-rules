*OTW Rule 402 Row House CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyStyleOfHome)
            Fac.OTWRowHouseFactor
            (RowHouse Factor)
			;
*Creates: 	out.OTW_R402_Row_House;

proc sql;
	create table out.OTW_R402_Row_House as
	select distinct p.CaseNumber, p.PolicyNumber
		 , p.PropertyStyleOfHome
		 , rhf.Factor as OTW_Row_House label="OTW Row"
	from in.policy_info as p
	inner join Fac.OTWRowHouseFactor as rhf
	on  upper(trim(left(p.PropertyStyleOfHome))) = upper(trim(left(rhf.RowHouse)))
	;
quit;
