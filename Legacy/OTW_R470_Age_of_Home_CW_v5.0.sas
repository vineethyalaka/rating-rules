*OTW Rule 470 Age of Home;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate PropertyYearBuilt)
            Fac.OTWAgeOfHomeFactor
            (Factor Years_lower Years_upper)
			;
*Creates: 	out.OTW_R470_Age_of_Home;

proc sql;
	create table work.OTW_R470_Age_of_Home as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.PropertyYearBuilt
		 , (case when p.PolicyTermYears=1 then year(p.PolicyIssueDate) else year(p.PolicyEffectiveDate) end
	 		- case when p.PropertyYearBuilt = . then year(date()) else p.PropertyYearBuilt end) as Property_Age
	from in.Policy_Info as p
	;

	create table out.OTW_R470_Age_of_Home as
	select p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.PropertyYearBuilt
		 , ahf.Factor as Age_of_Home label="Age of Home"
	from work.OTW_R470_Age_of_Home as p
	inner join Fac.OTWAgeOfHomeFactor as ahf 
	on  p.Property_Age >= ahf.Years_lower
	and p.Property_Age <  ahf.Years_upper
	;
quit;
