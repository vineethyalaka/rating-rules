*HO6 OTW Rule 508 Units Rented CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber Weeks_Rented_to_Others)
            Fac.OTWPropertyUnitRentedFactor
            (Factor)
			out.OTW_Base_Premium
			(CaseNumber OTW_Base_Premium)
			out.OTW_R406_Deductibles
			(CaseNumber OTW_Deductible_Factor)
			;
*Creates: 	out.OTW_R508_Units_Rented;

proc sql;
	create table out.OTW_R508_Units_Rented as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when input(Weeks_Rented_to_Others,3.) < 9 then 0 
			else round(p.OTW_Base_Premium*d.OTW_Deductible_Factor*(ur.Factor-1),1) end as Units_Rented
	from Fac.OTWPropertyUnitRentedFactor as ur
		, in.Endorsement_Info as e
		inner join out.OTW_Base_Premium as p
		on e.casenumber = p.casenumber
		inner join out.OTW_R406_Deductibles as d
		on e.casenumber = d.casenumber
	;
quit;
