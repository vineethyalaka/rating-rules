*HO3 Rule 590.2 Wind Mitigation MS;
*Uses:    	in.Production_Info
			(CaseNumber RoofCovering)
			in.Policy_Info
			(CaseNumber PolicyNumber PropertyConstructionClass)
			Source.ConstructionMapping
			(PropertyConstructionClass Type2)
			Fac.WindMitigationRoofCover
			(Factor Code Construction)
			;
*Creates: 	out.Wind_R590_Mitigation;

proc sql;
    create table out.Wind_R590_Mitigation as
	select distinct p.CaseNumber, p.PolicyNumber
	, wm.Factor as WM_RoofCover
	, wm.Factor as Wind_Mitigation

	from in.Production_Info as pd

	inner join in.Policy_Info as p
	on pd.casenumber = p.casenumber

	inner join Source.ConstructionMapping as cm
	on upper(trim(left(p.PropertyConstructionClass))) = upper(trim(left(cm.PropertyConstructionClass)))

	inner join Fac.WindMitigationRoofCover as wm
	on pd.RoofCovering = wm.Code
	and cm.Type2 = wm.Construction
;
quit;
