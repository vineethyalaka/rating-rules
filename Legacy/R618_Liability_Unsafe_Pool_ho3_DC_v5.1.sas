*Rule 617 Liability Unsafe Swimming Pool CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF PoolPresent PoolHasFourFootFence Ladder)
            out.LiabilityUnsafePoolCovEFactor
            (Factor CoverageE)
            Fac.LiabilityUnsafePoolCovFFactor
            (Factor CoverageF)
			;
*Creates: 	out.Liability_Unsafe_Pool;

proc sql;
    create table out.Liability_Unsafe_Pool as
	select distinct p.CaseNumber, p.PolicyNumber, p.PoolPresent, p.PoolHasFourFootFence, p.Ladder
		 , case when p.PoolPresent="Y" and p.PoolHasFourFootFence="N" and p.Ladder="0" then spe.Factor else 0 end as Unsafe_Pool_E
		 , case when p.PoolPresent="Y" and p.PoolHasFourFootFence="N" and p.Ladder="0" then spf.Factor else 0 end as Unsafe_Pool_F
	from in3.Policy_Info as p
	inner join Fac3.LiabilityUnsafePoolCovEFactor as spe
	on p.CoverageE = spe.CoverageE
	inner join Fac3.LiabilityUnsafePoolCovFFactor as spf
	on p.CoverageF = spf.CoverageF
/*	where p.casenumber = 145850908*/
	;
quit;
