*HO4 Rule 301.4 Liability Premium CW;
*Uses:    	out.Liability_Base_Rate
			(CaseNumber PolicyNumber Coverage_E Coverage_F)
			out.Liability_Other_Location
			(CaseNumber PolicyNumber Other_Location_E Other_Location_F)
			out.Liability_Rented_Residence
			(CaseNumber PolicyNumber Rented_Residence_E Rented_Residence_F)
			out.Liability_Business_Pursuits
			(CaseNumber PolicyNumber Business_Pursuits_E Business_Pursuits_F)
			out.Liability_Personal_Injury
			(CaseNumber PolicyNumber Personal_Injury_E)
			out.Liability_Watercraft
			(CaseNumber PolicyNumber Watercraft_E Watercraft_F)
			out.Liability_Snowmobile
			(CaseNumber PolicyNumber Snowmobile_E Snowmobile_F)
			out.Liability_Dangerous_Dog
			(CaseNumber PolicyNumber Dangerous_Dog_E Dangerous_Dog_F)
			;
*Creates: 	out.Liability_Premium;

proc sql;
    create table out.Liability_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , round(a.Coverage_E*h.Tier_Factor,1) as Coverage_E
		 , round(a.Coverage_F*h.Tier_Factor,1) as Coverage_F
		 , round(c.Personal_Injury_E*h.Tier_Factor,1) as Personal_Injury
		 , round(d.Watercraft_E*h.Tier_Factor,1) as Watercraft_E
		 , round(d.Watercraft_F*h.Tier_Factor,1) as Watercraft_F
		 , calculated Watercraft_E + calculated Watercraft_F as Watercraft
		 , round(e.Snowmobile_E*h.Tier_Factor,1) as Snowmobile_E
		 , round(e.Snowmobile_F*h.Tier_Factor,1) as Snowmobile_F
		 , calculated Snowmobile_E + calculated Snowmobile_F as Snowmobile
		 , round(f.Dangerous_Dog_E*h.Tier_Factor,1) as Dangerous_Dog_E
		 , round(f.Dangerous_Dog_F*h.Tier_Factor,1) as Dangerous_Dog_F
		 , calculated Dangerous_Dog_E + calculated Dangerous_Dog_F as Dangerous_Dog
		 , round(i.Other_Location_E*h.Tier_Factor,1) as Other_Location_E
		 , round(i.Other_Location_F*h.Tier_Factor,1) as Other_Location_F
		 , calculated Other_Location_E + calculated Other_Location_F as Other_Location
		 , round(j.Rented_Residence_E*h.Tier_Factor,1) as Rented_Residence_E
		 , round(j.Rented_Residence_F*h.Tier_Factor,1) as Rented_Residence_F
		 , calculated Rented_Residence_E + calculated Rented_Residence_F as Rented_Residence
		 , round(k.Business_Pursuits_E*h.Tier_Factor,1) as Business_Pursuits_E
		 , round(k.Business_Pursuits_F*h.Tier_Factor,1) as Business_Pursuits_F
		 , calculated Business_Pursuits_E + calculated Business_Pursuits_F as Business_Pursuits
		 , calculated Coverage_E 
			+ calculated Coverage_F 
			+ calculated Personal_Injury
			+ calculated Watercraft_E 
			+ calculated Watercraft_F
			+ calculated Snowmobile_E 
			+ calculated Snowmobile_F 
			+ calculated Dangerous_Dog_E 
			+ calculated Dangerous_Dog_F 
			+ calculated Other_Location_E
			+ calculated Other_Location_F
			+ calculated Rented_Residence_E
			+ calculated Rented_Residence_F
			+ calculated Business_Pursuits_E
			+ calculated Business_Pursuits_F
			as Liability_Premium format=10.4
		 , calculated Liability_Premium as Tiered_Liability_Premium
	from out.Liability_Base_Rate as a 
	inner join out.Liability_Personal_Injury as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.Liability_Watercraft as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Liability_Snowmobile as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.Liability_Dangerous_Dog as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.Tier_Factor as h
	on a.CaseNumber = h.CaseNumber
	inner join out.Liability_Other_Location as i
	on a.CaseNumber = i.CaseNumber
	inner join out.Liability_Rented_Residence as j
	on a.CaseNumber = j.CaseNumber
	inner join out.Liability_Business_Pursuits as k
	on a.CaseNumber = k.CaseNumber
	;
quit;
