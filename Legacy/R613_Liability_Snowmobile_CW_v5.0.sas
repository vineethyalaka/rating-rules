*Rule 613 Liability Snowmobile CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF)
            in.Endorsement_Info
            (CaseNumber PolicyNumber HO_2464_FLAG)
            out.LiabilitySnowmobileCovEFactor
            (Factor CoverageE)
            Fac.LiabilitySnowmobileCovFFactor
            (Factor CoverageF)
			;
*Creates: 	out.Liability_Snowmobile;

proc sql;
    create table out.Liability_Snowmobile as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when e.HO_2464_FLAG="1" then sme.Factor else 0 end as Snowmobile_E
		 , case when e.HO_2464_FLAG="1" then smf.Factor else 0 end as Snowmobile_F
	from in.Policy_Info as p
	inner join in.Endorsement_Info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.LiabilitySnowmobileCovEFactor as sme
	on p.CoverageE = sme.CoverageE
	inner join Fac.LiabilitySnowmobileCovFFactor as smf
	on p.CoverageF = smf.CoverageF
	;
quit;
