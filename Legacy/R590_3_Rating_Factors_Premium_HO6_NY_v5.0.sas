*HO6 Rule 590.3 Wind Rating Factors Premium CW;
*Uses:    	out.Wind_Base_Premium
			(CaseNumber PolicyNumber Wind_Base_Premium)
			out.Wind_Deductible
			(CaseNumber PolicyNumber Wind_Deductible)
			out.Wind_R403_CovC_Replacement_Cost
			(CaseNumber PolicyNumber HO_04_90)
			;
*Creates: 	out.Wind_Rating_Factors_Premium;

proc sql;
    create table out.Wind_Rating_Factors_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , b.Wind_Deductible
		 , d.HO_04_90 as Wind_HO_04_90
		 , k.Windstorm_Protective_Devices
		 , a.Wind_Base_Premium
		 , a.Wind_Base_Premium*b.Wind_Deductible*d.HO_04_90*k.Windstorm_Protective_Devices
			as Wind_Rating_Factors_Premium format=10.4
	from out.Wind_Base_Premium as a 
	inner join out.Wind_Deductible as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Wind_R403_CovC_Replacement_Cost as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Wind_R491_Wind_Prot_Devices as k 
	on a.CaseNumber = k.CaseNumber
	;
quit;
