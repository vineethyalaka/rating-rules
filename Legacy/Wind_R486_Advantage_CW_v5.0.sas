*Wind Rule 486 Homesite basic Package and Enhanced Package CW (Advantage);
*Uses:    	out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.WindAdvantageFactor
            (Endorsement Factor)
			;
*Creates: 	out.Wind_R486_Advantage;

proc sql;
    create table out.Wind_R486_Advantage as
	select distinct b.CaseNumber, b.PolicyNumber, b.AdvInd, a.Factor as Advantage label="Wind Advantage Factor"
	from out.OTW_R486_Advantage as b
    inner join Fac.WindAdvantageFactor as a 
    on b.AdvInd = a.Endorsement
	;
quit;
