*OTW Rule 404 Protective Devices CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber ProtDevBurglarAlarm ProtDevFireAlarm ProtDevSprinklers)
            Fac.OTWProtDevBurglarAlarmFactor
            (Factor ProtDevBurglarAlarm)
            Fac.OTWProtDevFireAlarmFactor
            (Factor ProtDevFireAlarm)
            Fac.OTWProtDevSprinklersFactor
            (Factor ProtDevSprinklers)
			;
*Creates: 	out.OTW_R404_Protective_Devices;
/*2015 10 30 SY ProtectionClass of 10 won't be given crediets.*/
/*Intentionally applying incorrrect rate for prot class 10 to compare premium with IT. Chagne back to Protectionclass = 10*/
proc sql;
	create table out.OTW_R404_Protective_Devices as
	select p.CaseNumber, p.PolicyNumber
/*		 , case when p.ProtectionClass = 11 then 1 else baf.Factor end as Burglar_Alarm*/
/*		 , case when p.ProtectionClass = 11 then 1 else faf.Factor end as Fire_Alarm*/
/*		 , case when p.ProtectionClass = 11 then 1 else sdf.Factor end as Smoke_Detector*/
/*		 , case when p.ProtectionClass = 11 then 1 else sf.Factor end as Sprinklers*/

		 , case when p.ProtectionClass = 10 then 1 else baf.Factor end as Burglar_Alarm
		 , case when p.ProtectionClass = 10 then 1 else faf.Factor end as Fire_Alarm
		 , case when p.ProtectionClass = 10 then 1 else sdf.Factor end as Smoke_Detector
		 , case when p.ProtectionClass = 10 then 1 else sf.Factor end as Sprinklers

	from in.Policy_Info as p

	inner join Fac.OTWProtDevBurglarAlarmFactor as baf
	on ((p.ProtDevBurglarAlarm = "" and baf.ProtDevBurglarAlarm = "") 
	or p.ProtDevBurglarAlarm = baf.ProtDevBurglarAlarm)
	and ((p.ProtDevFireAlarm = "" and baf.ProtDevFireAlarm = "")
	or p.ProtDevFireAlarm = baf.ProtDevFireAlarm)

	inner join Fac.OTWProtDevFireAlarmFactor as faf
	on ((p.ProtDevBurglarAlarm = "" and faf.ProtDevBurglarAlarm = "") 
	or p.ProtDevBurglarAlarm = faf.ProtDevBurglarAlarm)
	and ((p.ProtDevFireAlarm = "" and faf.ProtDevFireAlarm = "")
	or p.ProtDevFireAlarm = faf.ProtDevFireAlarm)

	inner join Fac.OTWProtDevSprinklersFactor as sf
	on p.ProtDevSprinklers = sf.ProtDevSprinklers
	and p.TXSprinkler = sf.TXSprinklers
	inner join Fac.otwprotdevsmokedetectorsfactor as sdf
	on p.ProtDevSmokeDetector = sdf.Smoke
	;
quit;
