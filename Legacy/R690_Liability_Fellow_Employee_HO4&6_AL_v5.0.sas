*R690 Fellow Employee AL;

proc sql;
    create table out.Liability_Fellow_Employee as
	select distinct p.CaseNumber, p.PolicyNumber,
		 /***case when e.ML_208_FLAG = "1" then pi.Factor else 0 end as Fellow_Employee_E*/
	0 as Fellow_Employee_E
	from in.Policy_Info as p
	inner join in.Endorsement_Info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.LIABILITYFELLOWEMPLOYEE as pi
	on p.CoverageE = pi.CoverageE
	;
quit;
