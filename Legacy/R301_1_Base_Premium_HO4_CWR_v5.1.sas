*HO4 Rule 301.1 OTW Base Premium CW;
*Uses:    	out.OTW_Territory_Rate
            (CaseNumber PolicyNumber OTW_Territory_Rate)
            out.OTW_AOI_Factor
            (CaseNumber PolicyNumber OTW_AOI)
			;
*Creates: 	out.OTW_Base_Premium;

proc sql;
    create table out.OTW_Base_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , a.OTW_Territory_Rate
		 , b.OTW_AOI
		 , round(a.OTW_Territory_Rate*b.OTW_AOI,1) as OTW_Base_Premium
	from out.OTW_Territory_Rate as a 
	inner join out.OTW_AOI_Factor as b 
	on a.CaseNumber = b.CaseNumber
	;
quit;
