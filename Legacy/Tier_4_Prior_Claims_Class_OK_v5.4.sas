




*Tier 4 Prior Claims Class CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate)
            in.loss_info
            (PolicyNumber LossDate LossCode LossAmount)
            mapping.ActofGod_PriorClaims
            (ClaimTypeGod ActOfGodCode)
            Fac.TierPriorClaimClass
            (Class NumberofNAGMax NumberofNAGMin NumberofAOGMax NumberofAOGMin AgeType)
			;
*Creates: 	out.Tier4_PriorClaims;

*work.PriorClaims_a;;
proc sort data=in.loss_info; by Source Casenumber lossdate; run;
data work.loss_info; set in.loss_info; by Source Casenumber; 
	if first.casenumber and last.casenumber  then delete; run;

proc sql;
	create table work.PriorClaims_a as
	select distinct l.casenumber, l.policynumber, p.PolicyTermYears, p.PolicyEffectiveDate, l.lossdate, l.LossCode, l.LossAmount, l.Source
			, case when t.ClaimTypeGod is null then 0
				else t.ClaimTypeGod
			  end as ClaimAOG
			, case when t.ClaimTypeGod is null then 1
				else 0
			  end as ClaimNAG
			, case when p.PolicyTermYears=1 then (p.PolicyIssueDate - l.LossDate)/365
				else (p.PolicyEffectiveDate - l.LossDate - 60)/365
			  end as LossAge
			, case when calculated lossage>3 then 1	/*within 3 years*/
				else 2
			  end as AgeType
	from in.policy_info as p
		inner join in.loss_info as l
		on p.PolicyNumber=l.policynumber
		and p.casenumber=l.casenumber
		left join mapping.ActofGod_PriorClaims as t
		on l.LossCode=t.ActOfGodCode
	where l.LossAmount>0 
		and (p.PolicyTermYears=1 and (p.PolicyIssueDate - l.LossDate)/365 <5	/*within 5 years*/
		or  p.PolicyTermYears^=1 and (p.PolicyEffectiveDate - l.LossDate - 60)/365 <5)
	;
quit;

*work.PriorClaims_b;;
proc sql;
	create table work.PriorClaims_b as
	select distinct a.casenumber, a.policynumber, sum(a.claimAOG) as SumOfClaimAOG, sum(a.ClaimNAG) as SumOfClaimNAG, a.AgeType
	from work.PriorClaims_a as a
	group by a.PolicyNumber, a.casenumber, a.AgeType
	;
quit;

*work.PriorClaims_c;;
proc sql;
	create table work.PriorClaims_c as
	select distinct b.casenumber, b.PolicyNumber, b.AgeType, c.Class
	from work.PriorClaims_b as b
		inner join Fac.TierPriorClaimClass as c
		on b.SumOfClaimNAG <= c.NumberofNAGMax
		and b.SumOfClaimNAG >= c.NumberofNAGMin
		and b.SumOfClaimAOG <= c.NumberofAOGMax
		and b.SumOfClaimAOG >= c.NumberofAOGMin
		and b.AgeType = c.AgeType
	;
quit;

*work.PriorClaims_d;;
proc sql;
    create table work.PriorClaims_d as
	select distinct c.casenumber, c.PolicyNumber, max(c.class) as MaxOfClass
	from work.PriorClaims_c as c
	group by c.PolicyNumber, c.casenumber
	;
quit;





proc sql;

create table a as
select casenumber, case when count(source)=1 then 1 else 0 end as EclipseClaimsCount 
from in.loss_info l
where  source  ='Eclipse' and l.LossAmount>0 
group by casenumber

;
quit;

proc sql;

create table b as
select casenumber,case when count(LossDate)=1 then 1 else 0 end as ClaimsCount
from in.loss_info l
where l.LossAmount>0 and AgeOfClaim < 5
group by casenumber


;
quit;

proc sql;

create table c as 
select distinct p.casenumber, EclipseClaimsCount,ClaimsCount
from in.policy_info p
left join a a
        on a.casenumber = p.casenumber
left join b b
        on a.casenumber = b.casenumber


;
quit;

proc sql;
    create table work.PriorClaims_d as
	select distinct c.casenumber, c.PolicyNumber, e.EclipseClaimsCount,
           case when e.EclipseClaimsCount=1 and e.ClaimsCount=1 then 1 else max(c.class) end as MaxOfClass
	from work.PriorClaims_c as c
	inner join work.c e
	       on c.casenumber = e.casenumber
	group by c.PolicyNumber, c.casenumber
;
quit;
*out.Tier4_PriorClaims;;
proc sql;
    create table out.Tier4_PriorClaims as
	select distinct a.CaseNumber, a.PolicyNumber, case when d.MaxOfClass is Null then 1 else d.MaxOfClass end as Prior_Claims_Class
	from in.policy_info as a
	left join work.PriorClaims_d as d
	on a.PolicyNumber = d.PolicyNumber
	and a.CaseNumber = d.CaseNumber

	
    ;
quit;




