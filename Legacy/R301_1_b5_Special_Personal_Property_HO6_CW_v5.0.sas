*HO6 Rule 301.1.b5 Special Personal Property CW (HH 17 31);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_1731_FLAG)
            in.Production_Info
            (CaseNumber PolicyNumber HH0017)
            Fac.OTWHO1731Factor
            (Factor HH1731 HO1731)
			;
*Creates: 	out.OTW_HH_17_31_Factor;

proc sql;
    create table out.OTW_HH_17_31_Factor as
	select  distinct e.CaseNumber, e.PolicyNumber, hf17.Factor as OTW_HH_17_31 label="OTW HH-17-31"
	from in.Endorsement_Info as e
	inner join in.Production_Info as pd
	on e.CaseNumber = pd.CaseNumber
    inner join Fac.OTWHO1731Factor as hf17
    on pd.HH0017 = hf17.HH1731
    and e.HO_1731_FLAG = hf17.HO1731
	;
quit;
