*Rule Workers Comp Domestic Servants NJ;
*Uses:    	in.NJ_Info
            (CaseNumber PolicyNumber HO_2492_FLAG HO_2494_FLAG Servant_Count)
            Fac.WorkersCompDomesticServants
            (Base PerEmployee)
			;
*Creates: 	out.WorkersComp_Domestic_Servants;

proc sql;
    create table out.WorkersComp_Domestic_Servants as
	select distinct p.CaseNumber, p.PolicyNumber, lds.Base + lds.PerEmployee*p.Servant_Count as Domestic_Servants
	from in.NJ_Info as p
	inner join Fac.WorkersCompDomesticServants as lds
	on p.HO_2492_FLAG = lds.HO2492_FLAG
	and p.HO_2494_FLAG = lds.HO2494_FLAG
	;
quit;
