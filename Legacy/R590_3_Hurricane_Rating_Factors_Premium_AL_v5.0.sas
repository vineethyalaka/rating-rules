*With Mitigation;

proc sql;
    create table out.Hurr_Rating_Factors_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , b.Wind_Deductible
		 , c.HO_04_81 as Wind_HO_04_81
		 , d.HO_04_90 as Wind_HO_04_90
		 , e.HO_04_20 as Wind_HO_04_20
		 , g.HO_04_93 as Wind_HO_04_93
		 , h.Roof_Age_Materials as Wind_Roof_Age_Materials
		 , i.Advantage as Wind_Advantage
		 , j.Wind_Row_House
		 , k.Hurr_Mitigation
		 , a.Hurr_Base_Premium
		 , a.Hurr_Base_Premium*b.Wind_Deductible*c.HO_04_81*d.HO_04_90*e.HO_04_20
		 	*g.HO_04_93*h.Roof_Age_Materials*i.Advantage*j.Wind_Row_House*k.Hurr_Mitigation
			as Hurr_Rating_Factors_Premium format=10.4
	from out.Hurr_Base_Premium as a 
	inner join out.Wind_Deductible as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Wind_R302_B_ACV_Loss_Settlement as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.Wind_R403_CovC_Replacement_Cost as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Wind_R407_1_Add_AMT_CovA as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.Wind_R408_ACV_Roof as g 
	on a.CaseNumber = g.CaseNumber
	inner join out.Wind_R471_Roofing_Age_Materials as h 
	on a.CaseNumber = h.CaseNumber
	inner join out.Wind_R486_Advantage as i 
	on a.CaseNumber = i.CaseNumber
	inner join out.Wind_R402_Row_House as j 
	on a.CaseNumber = j.CaseNumber
	inner join out.Hurr_R590_Mitigation as k
	on a.CaseNumber = k.CaseNumber
	;
quit;
