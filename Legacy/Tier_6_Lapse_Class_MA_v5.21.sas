*Tier 6 Lapse Class MA;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance end_code_3_PI end_code_3_NH New_Home_Flag)
            in.Production_Info
            (CaseNumber PolicyNumber Lapse_Date_Used)
            Fac.TierLapseDays
            (LapseDays)
			Fac.TierLapseInsuranceClass
			(Factor Class)
			;
*Creates: 	out.Tier6a_LapseInsurance
			out.Tier6b_LapseInsurance;


proc sql;
    create table out.Tier6a_LapseInsurance as
	select distinct p.CaseNumber, p.PolicyNumber
		  ,pd.Lapse_Date_Used
		  ,p.Lapse_of_Insurance
		  ,p.end_code_3_NH as ProposeEffDate format=date9.
		  ,p.end_code_3_PI as CurrentExpDate format=date9.
		  ,case when p.end_code_3_PI = . then p.end_code_3_NH - mdy(1,1,1900)
		   		else p.end_code_3_NH - p.end_code_3_PI 
		   end as period
		  ,l.LapseDays
		  ,case when pd.Lapse_Date_Used = 0 then 1
				when calculated period -1 <= l.LapseDays then 1 
				else 2
		   end as Lapse_of_Insurance_Class
	from in.Policy_Info as p
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		, Fac.TierLapseDays as l
    ;
quit;


proc sql;
	create table out.Tier6b_LapseInsurance as
	select distinct a.CaseNumber, a.PolicyNumber, a.Lapse_of_Insurance_Class, b.Factor as Lapse_of_Insurance_Factor
	from out.Tier6a_LapseInsurance as a
		inner join Fac.TierLapseInsuranceClass as b
		on a.Lapse_of_Insurance_Class = b.Class
	;
quit;
