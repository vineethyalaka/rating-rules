*Wind Rule 515.A/B Personal Property (Increased/Reduction in Coverage C) CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageC PropertyUnitsInBuilding)
            in.Endorsement_Info
            (CaseNumber PolicyNumber EXWIND)
            out.Wind_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.WindPropertyCovCFactor
            (Factor1 Factor2 AdvInd FamilyUnits)
			;
*Creates: 	out.Wind_R515_A_Incr_CovC;

proc sql;
	create table out.Wind_R515_A_Incr_CovC as
	select distinct p.CaseNumber, p.PolicyNumber
		 , (p.CoverageC - p.CoverageA*cc.BaseCovC) as IncrCovC
		 , case when calculated IncrCovC > 0 then (1-e.EXWIND)*round(calculated IncrCovC/1000*cc.Factor1,1)
		 	
		 		else 0 end as Coverage_C,
		   case 	when calculated IncrCovC <= 0 and cc.AdvInd="N"
											 then (1-e.EXWIND)*round(calculated IncrCovC/1000*cc.Factor2,1)
				else 0 end as Wind_HD_006
	from in.Policy_Info as p
		inner join in.Endorsement_Info as e
		on p.CaseNumber = e.CaseNumber
		inner join out.Wind_R486_Advantage as b 
		on p.CaseNumber = b.CaseNumber
		inner join Fac.WindPropertyCovCFactor as cc
		on  b.AdvInd = cc.AdvInd
		and p.PropertyUnitsInBuilding = cc.FamilyUnits
	;
quit;
