



/*Wanwan: ITR 15175 remove wind deductible in premium calcualtion */
proc sql;
    create table out.Wind_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
/*		 , b.Wind_Deductible*/
		 , d.HO_04_90 as Wind_HO_04_90
		 , a.Wind_Base_Premium
		 , (1-e.exwind)*round(a.Wind_Base_Premium
		 * OTW_Deductible_Factor
/**b.Wind_Deductible*/
*d.HO_04_90,1)
			as Wind_Premium format=10.4
	from out.Wind_Base_Premium as a 
	inner join out.OTW_R406_Deductibles b
	on b.casenumber = a.casenumber 
/*	inner join out.Wind_Deductible as b */
/*	on a.CaseNumber = b.CaseNumber*/
	inner join out.Wind_R403_CovC_Replacement_Cost as d 
	on a.CaseNumber = d.CaseNumber
	inner join in.endorsement_info as e
	on a.CaseNumber = e.CaseNumber
	;
quit