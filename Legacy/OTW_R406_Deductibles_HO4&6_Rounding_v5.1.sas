*HO6 OTW Rule 406 Deductibles CW (IT calculation);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible)
            out.OTW_Tiered_Base_Premium
            (CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
            Fac.OTWDeductibleFactor
            (CoverageDeductible Factor Max_Credit)
			;
*Creates: 	out.OTW_R406_Deductibles;

%macro AOPDed;
	%if &AOPDedAdj46 = 1 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			 , case when input(p.CoverageDeductible,dollar8.) < &minAOP46 then &minAOP46
				else input(p.CoverageDeductible,dollar8.)
				end as CoverageDeductible_adj
		from in.Policy_Info as p
		;
	quit;
	%end;

	%if &AOPDedAdj46 = 0 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			 , input(p.CoverageDeductible,dollar8.) as CoverageDeductible_adj
		from in.Policy_Info as p
		;
	quit;
	%end;

	proc sql;
		create table out.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			 , df.Factor as Deductible label="Deductible"
			 , df.Max_Credit
			 , case when (1 - df.Factor)*t.OTW_Tiered_Base_Premium > df.Max_Credit 
					then (1 - df.Max_Credit/t.OTW_Tiered_Base_Premium)
				else df.Factor end as OTW_Deductible_Factor
		from work.OTW_R406_Deductibles as p
		inner join out.OTW_Tiered_Base_Premium as t
		on p.CaseNumber = t.CaseNumber
		inner join Fac.OTWDeductibleFactor as df
		on p.CoverageDeductible_adj = df.CoverageDeductible
		;
	quit;
%mend;
%AOPDed;
