*HO6 Wind Rule 515.C Personal Property Increased Special Limits of Liability (Unshceduled) CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber EXWIND HO_0465_flag HO_04_65_01_AMT - HO_04_65_07_AMT)
            Fac.WindPropertyJewelryFactor
            (Factor amount)
            Fac.WindPropertyUnschedPropFactor
            (Money Securities Firearms Silverware Apparatus)
			;
*Creates: 	out.Wind_R515_C_Unsched_Incr_CovC;

proc sql;
	create table out.Wind_R515_C_Unsched_Incr_CovC as
	select distinct e.CaseNumber, e.PolicyNumber
		 , (1-e.EXWIND)*round(pj.Factor*e.HO_04_65_03_AMT/pj.amount,1) as Jewelry
		 , case when e.HO_0465_flag = "0" then 0
			else calculated Jewelry
			+ (1-e.EXWIND)*(round(usp.Money     *e.HO_04_65_01_AMT/100,1)
		  				+round(usp.Securities*e.HO_04_65_02_AMT/100,1)
		  				+round(usp.Firearms  *e.HO_04_65_04_AMT/100,1)
		  				+round(usp.Silverware*e.HO_04_65_05_AMT/500,1)
		  				+round(usp.Apparatus *(e.HO_04_65_06_AMT + e.HO_04_65_07_AMT)/500,1)) 
			end as Unscheduled_Personal_Property
	from Fac.WindPropertyUnschedPropFactor as usp, Fac.WindPropertyJewelryFactor as pj
		, in.Endorsement_Info as e
	;
quit;
