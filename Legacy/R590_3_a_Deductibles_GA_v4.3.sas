*Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA WindHailDeductible)
            Fac.WindDeductibleFactor
            (class AOI_Lower AOI_Upper Factor_Lower Factor_Upper Deductible)
			;
*Creates: 	out.Wind_Deductible;


%macro WindDed;

	%if &WindAdj = 0 %then %do;
	proc sql;
	    create table work.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber
			/*The "Reverted" field used to have a different purpose in IT's database; all values greater than 9 should not be considered*/
			/*Reverted can be 7, 8, 9, or 0. 
				7 means they accepted new coverage, 
				9 means they kept prior coverage, then changed their minds and accepted new coverage
				8 means they kept prior coverage. 
				0 means they haven't received the option to be grandfathered (i.e. older cases prior to wind/hail filing) */
			 , case when p.Reverted > 9 then 0 else p.Reverted end as Reverted 

			 /*If the Region field was pulled from ISO_PROD.policygeospatialdata, use that. Otherwise, manually map policies and create "Group" Field (use text format).*/
			, case when p.Region is missing or p.Region = "0" then p.Group else p.Region end as Region,
				p.CoverageDeductible
		,case when calculated Reverted in (7, 9) and strip(p.HurricaneDeductible) = "2500" then "2500.1" /*If they accepted new coverage and their wind/hail deductible is 2500, recode it as 2500.1 (2500.1 = roof deductible)*/
			when calculated Reverted in (7, 9) and strip(p.HurricaneDeductible) ^= "2500" then strip(p.HurricaneDeductible) 
			when calculated Reverted not in (7,9) and p.WindHailDeductible is not missing then strip(p.WindHailDeductible) /*If these are old cases (reverted = 0) or they reverted to prior coverage (reverted = 8) and the WindHailDeductible field is populated, use that.*/
			else input(substr(p.CoverageDeductible, 2,5), $CHAR6.) end as WindDeductible /*If no other case applies, use the coverage deductible field.*/
		from in.Policy_Info as p
		;
	quit;


	/*Determine the w/h ded factor based on Reverted status, Region, and W/H Deductible.*/
	proc sql;
	    create table out.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber
			 , p.Reverted, p.CoverageDeductible, p.Region
			 , p.WindDeductible, df.Factor as Wind_Deductible
		from work.Wind_Deductible as p
		inner join Fac.WindDeductibleFactor as df
			on p.Reverted = df.Reverted		
			and p.Region = df.Region
			and case when p.WindDeductible = "" then 0
				else input(p.WindDeductible, 6.) 
				end 
			   = df.Deductible

		;
	quit;

	%end;

	%if &WindAdj = 1 %then %do;
	proc sql;
	    create table work.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber, p.CoverageDeductible, strip(p.WindHailDeductible) as WindDed, strip(p.HurricaneDeductible) as HurrDed
			 , case when p.Reverted > 9 then 0 else p.Reverted end as Reverted
			 , case when p.Region is missing or p.Region = "0" then p.Group else p.Region end as Region,
			 	/*If policy didn't revert to prior coverage and is in Region 1, adjust deductible to 5% if it's not 5 or 10.*/
				case when calculated Region in ("1", "01") and calculated HurrDed not in ("5", "10") 
					and calculated Reverted ^= 8 then "5"
			
					when calculated Region in ("1", "01") and calculated HurrDed in ("5", "10") 
						and calculated Reverted  ^= 8 then calculated HurrDed

				/*If policy didn't revert to prior coverage and is in Region 2, adjust deductible to 2% if it's not 2, 5 or 10.*/
					when calculated Region in ("2", "02") and calculated HurrDed not in ("2", "5", "10")
						and calculated Reverted  ^= 8 then "2"
					when calculated Region in ("2", "02") and calculated HurrDed in ("2", "5", "10")
						and calculated Reverted  ^= 8 then calculated HurrDed

				/*If policy didn't revert to prior coverage and is in Region 3, adjust deductible to 2500.1 (roof only) if it's not 2, 5 or 10.*/
					when calculated Region in ("3", "03") and calculated HurrDed not in ("2", "5", "10") 
						and calculated Reverted ^= 8 then "2500.1"

				/*If policy didn't revert to prior coverage and is in Region 3, keep deductible the same if it's 2, 5 or 10.*/
					when calculated Region in ("3", "03") and calculated HurrDed in ("2", "5", "10") 
						and calculated Reverted ^= 8 then calculated HurrDed 
				
				/*If policy reverted to prior coverage, they have an all-peril deductible. Use their AOP.*/
					when calculated Reverted  = 8 then substr(input(p.CoverageDeductible, $CHAR6.),2,5)

					else substr(input(p.CoverageDeductible, $CHAR6.),2,5)
				end as WindDeductible_adj
			, input(calculated WindDeductible_adj, 6.) as WindDeductible
		from in.Policy_Info as p
		;
	quit;

	/*Determine the w/h ded factor based on Reverted status, Region, and W/H Deductible.*/
	proc sql;
	    create table out.Wind_Deductible as
		select p.CaseNumber, p.PolicyNumber
			 , p.Reverted, p.CoverageDeductible, p.WindDed, p.HurrDed, p.Region
			 , p.WindDeductible, df.Factor as Wind_Deductible
		from work.Wind_Deductible as p
		inner join Fac.WindDeductibleFactor as df
			on p.Reverted = df.Reverted
			and p.Region = df.Region
			and p.WindDeductible  = df.Deductible

		;
	quit;
	%end;
%mend;



%WindDed;
/**/
/*proc sql;*/
/*    create table work.Wind_Deductible as*/
/*	select p.CaseNumber, p.PolicyNumber*/
/*		 , p.CoverageA, p.CoverageDeductible, case when p.region is missing then p.group else put(p.region, 2.) end as Region, */
/*			p.Reverted*/
/*		 , 	case when p.reverted in (7,9) then*/
/*				case when calculated Region in ('1', '01')  then "5"*/
/*			 	when calculated Region in ('2', '02') then "2"*/
/*				else "2500.1" end*/
/*			else p.CoverageDeductible*/
/*			end	as WindDeductible */
/*	from in.Policy_Info as p*/
/*	;*/
/*quit;*/
/**/
/*proc sql;*/
/*    create table out.Wind_Deductible as*/
/*	select p.CaseNumber, p.PolicyNumber*/
/*		 , p.CoverageA, p.CoverageDeductible, p.Region*/
/*		 , p.WindDeductible, df.Factor as Wind_Deductible*/
/*	from work.Wind_Deductible as p*/
/*	inner join Fac.WindDeductibleFactor as df*/
/*	on case when substr(left(p.WindDeductible),1,1) = "$" */
/*				 then input(substr(left(p.WindDeductible),2,5),6.) */
/*			when p.WindDeductible = "" then 0*/
/*			else input(left(p.WindDeductible),6.) */
/*	   end */
/*	   = df.Deductible*/
/**/
/*	;*/
/*quit;*/
