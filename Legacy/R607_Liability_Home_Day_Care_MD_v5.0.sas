*Rule 609 Liability Home Day Care MD;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF)
            in.Endorsement_Info
            (CaseNumber PolicyNumber DayCare_FLAG)
            Fac.LiabilityDayCareEFactor
            (Factor CoverageE)
            Fac.LiabilityDayCareFFactor
            (Factor CoverageF)
			;
*Creates: 	out.Liability_Day_Care;
/**/
proc sql;
    create table out.Liability_Day_Care as
	select p.CaseNumber, p.PolicyNumber
		 , case when e.DayCare_FLAG = '1' then hdce.Factor else 0 end as Home_DayCare_E
		 , case when e.DayCare_FLAG = '1' then hdcf.Factor else 0 end as Home_DayCare_F
	from in.Policy_Info as p
	inner join in.Endorsement_Info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.LiabilityDayCareEFactor as hdce
	on p.CoverageE = hdce.CoverageE
	inner join Fac.LiabilityDayCareFFactor as hdcf
	on p.CoverageF = hdcf.CoverageF
	;
quit;
