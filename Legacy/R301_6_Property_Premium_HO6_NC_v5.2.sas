

proc sql;
    create table out.Property_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.Business_Property
		 , c.Earthquake
		 , d.Loss_Assessment
		 , e.Coverage_D
		 , f.Other_Structures
		 , g.Unscheduled_Personal_Property
		 , h.Scheduled_Personal_Property
		 , i.Computer
		 , r.Refrigerated_Property
		 , s.Coverage_A
		 , t.Units_Rented
		 , a.Business_Property
			+ c.Earthquake
			+ d.Loss_Assessment
		 	+ e.Coverage_D
			+ f.Other_Structures
			+ g.Unscheduled_Personal_Property
		 	+ h.Scheduled_Personal_Property
			+ i.Computer
			+ r.Refrigerated_Property
			+ s.Coverage_A
			+ t.Units_Rented
			+ y.Residence_Trust
			+ z.Water_Sump
			as Property_Premium format=10.4
		 , c.Earthquake
			as Other_Premium format=10.4
	from out.OTW_R503_Business_Property as a
	inner join out.OTW_R505D_Earthquake as c
	on a.CaseNumber = c.CaseNumber
	inner join out.OTW_R511_Loss_Assessment as d
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_R512_Loss_of_Use as e
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_R514_Other_Structures as f
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_R515_C_Unsched_Incr_CovC as g
	on a.CaseNumber = g.CaseNumber
	inner join out.OTW_R516_Sched_Personal_Prop as h
	on a.CaseNumber = h.CaseNumber
	inner join out.OTW_R519_Computer as i
	on a.CaseNumber = i.CaseNumber
	inner join out.OTW_R515_Refrigerated_Property as r
	on a.CaseNumber =r.CaseNumber
	inner join out.otw_r507_incr_cova as s
	on a.CaseNumber =s.CaseNumber
	inner join out.otw_r508_units_rented as t
	on a.CaseNumber = t.CaseNumber
	inner join out.OTW_R526_Residence_Trust as y
	on a.CaseNumber = y.CaseNumber
	inner join out.OTW_R521_Water_Sump as z
	on a.casenumber = z.casenumber
	;
quit;
