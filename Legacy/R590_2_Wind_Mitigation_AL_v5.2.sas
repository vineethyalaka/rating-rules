
/*LIBNAME sqlsrvqa oledb init_string = */
/*"Provider=SQLOLEDB.1; Integrated Security=SSPI; Persist Security Info=False;*/
/*Initial Catalog=ISO_PROD; read_lock_type=nolock; update_lock_type=nolock; READ_ISOLATION_LEVEL = RU; Data Source=CAMBOSQASQL21\SQL01" */
/*schema=dbo;*/
/*%let server = sqlsrvqa;*/

proc sql noprint;
select Casenumber into : Casenumber separated by ',' 
from in.policy_info;
quit;


proc sql;
create table county_name as 
select 
 Casenumber
,County
from &server..v3ratingrequest 
where Casenumber in (&casenumber.)
;
quit;



*Zone_roofAge_roofType;
proc sql;
create table Zone_roofAgeType_MitiCode as
select i.*,d.*,r.*,pd.Construction_Completed as Mitigation_Code
from fac.County_zone_IT i
inner join county_name d
on i.county= d.county
inner join out.Wind_R471_Roofing_Age_Materials r
on d.casenumber = r.casenumber
inner join in.production_info as pd
on d.casenumber = pd.casenumber
;
quit;


data roof_age_type_metal_category;
 set Zone_roofAgeType_MitiCode;
If propertyrooftypecode = '' Then 
	do; 
	   	 Put "ERROR: roof type is invalid.";
	end;
else 
	do;
		 if upcase(compress(propertyrooftypecode,'/ '))= upcase(compress('Tin/Membrane','/ '))
		 then MetalRoofFlag= 'y';
		 else MetalRoofFlag= 'n';
	end;
if (MetalRoofFlag = 'y' and Roof_Age le 10)or(MetalRoofFlag='n' and Roof_Age le 5) then 
    do;
         Category=1;
	end;
else if (MetalRoofFlag = 'y' and Roof_Age > 10)or(MetalRoofFlag='n' and Roof_Age > 5) then 
    do;
	     Category=2;
	end;
keep PolicyNumber CaseNumber PropertyRoofTypeCode Roof_Age MetalRoofFlag Category Zone Mitigation_Code;
run;



proc sql;
	create table out.Wind_R590_Mitigation as 
	select  r.CaseNumber, r.PolicyNumber, r.Category, r.Zone, r.MetalRoofFlag, r.Mitigation_Code,
            r.PropertyRoofTypeCode, r.Roof_Age, f.Factor as Wind_Mitigation
	from roof_age_type_metal_category as r
	inner join fac.loss_Mitigation_factors as f
		on (r.Category=f.Category and r.Zone=f.zone and r.Mitigation_Code=f.code)
	;
quit;