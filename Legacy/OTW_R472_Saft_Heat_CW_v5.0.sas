*OTW Rule 472 Saft Heat CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PropertyWoodStove PropertyHeatingSystem)
            Fac.OTWSafeHeatFactor
            (Factor Stove Type)
			;
*Creates: 	out.OTW_R472_Saft_Heat;

proc sql;
	create table out.OTW_R472_Saft_Heat as
	select distinct p.CaseNumber, p.PolicyNumber, p.PropertyWoodStove, p.PropertyHeatingSystem
		 , shf.Factor as Safe_Heat label="Safe Heat"
	from in.Policy_Info as p
	inner join Fac.OTWSafeHeatFactor as shf
	on shf.Stove = p.PropertyWoodStove
	and lower(trim(left(shf.Type))) = case when p.PropertyHeatingSystem = "" or substr(p.PropertyHeatingSystem,1,1) = " " 
												then "blank" else lower(trim(left(p.PropertyHeatingSystem))) end
	;
quit;
