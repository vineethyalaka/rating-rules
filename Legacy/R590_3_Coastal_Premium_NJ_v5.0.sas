*Rule 590.3 Coastal Premium NJ and MA;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber DistanceToShore CoverageA)
            Fac.CoastalPremium
            (Distance_Lower Distance_Upper Factor)
			;
*Creates: 	out.Coastal_Premium;


proc sql;
    create table out.Coastal_Premium as
	select distinct p.CaseNumber, p.PolicyNumber, p.DistanceToShore, p.CoverageA
		, case when p.DistanceToShore = 0 then 0 else (p.CoverageA / 1000) * c.factor end as Coastal_Premium
	from in.Policy_Info as p
	left join Fac.CoastalPremium c
	on (case when p.DistanceToShore is null then -1 else p.DistanceToShore end) >= c.distance_lower
		and (case when p.DistanceToShore is null then -1 else p.DistanceToShore end) <= c.distance_upper
	;

quit;
