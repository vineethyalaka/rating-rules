*Wind Rule 512 Loss of Use - Increase Coverage D CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageD)
            in.Endorsement_Info
            (CaseNumber PolicyNumber EXWIND)
            Fac.WindPropertyLossOfUseFactor
            (BaseCovD Factor)
			;
*Creates: 	out.Wind_R512_Loss_of_Use;

proc sql;
	create table out.Wind_R512_Loss_of_Use as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when p.CoverageD > p.CoverageA*lou.BaseCovD then (p.CoverageD - p.CoverageA*lou.BaseCovD) else 0 end as IncrCovD
		 , (1-e.EXWIND)*round(calculated IncrCovD/1000*lou.Factor,1) as Coverage_D
	from Fac.WindPropertyLossOfUseFactor as lou
		, in.Policy_Info as p
		inner join in.Endorsement_Info as e
		on p.CaseNumber = e.CaseNumber
	;
quit;
