*HO6 Rule 590.1.a4 AOI CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber CoverageC)
            Fac.HurrAOIFactor
            (AOI_Lower AOI_Upper Factor_Lower Factor_Upper)
			;
*Creates: 	out.Hurr_AOI_Factor;

proc sql;
    create table out.Hurr_AOI_Factor as
	select p.CaseNumber, p.PolicyNumber, p.CoverageC, f.AOI_Lower, f.AOI_Upper, f.Factor_Lower, f.Factor_Upper
	      ,(p.CoverageC - f.AOI_Lower)/(f.AOI_Upper - f.AOI_Lower) as I1
		  ,(f.AOI_Upper - p.CoverageC)/(f.AOI_Upper - f.AOI_Lower) as I2
		  ,(calculated I1 + abs(calculated I1))/(2*calculated I1) as Ind
		  ,round((f.Factor_Upper*calculated I1 + f.Factor_Lower*calculated I2)*calculated Ind
		          + (f.Factor_Lower + f.Factor_Upper*(p.CoverageC - f.AOI_Lower)/1000)*(1 - calculated Ind)
                , 0.001) as Hurr_AOI label="Hurr AOI Factor"
	from in.policy_info as p
	inner join Fac.HurrAOIFactor as f
	on (p.CoverageC <= f.AOI_Upper or f.AOI_Upper=0) 
	and p.CoverageC > f.AOI_Lower
	;
quit;
