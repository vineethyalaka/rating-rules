*OTW Rule 590 Mine Subsidence;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO2388_FLAG)
            in.Policy_Info
            (CaseNumber PolicyNumber CoverageA)
            Fac.OTWPropertyMineSubsidenceFactor
            (AOI_lower AOI_upper Factor)
			;
*Creates: 	out.OTW_590_Mine_Subsidence;


proc sql;

/*Dwelling*/
create table out.otw_590_mine_sub_dwell as
	select p.CaseNumber, p.PolicyNumber, e.HO2388_FLAG, p.CoverageA
		, case when e.HO2388_FLAG = '1' then factor else 0 end as Mine_Sub_Dwelling_Rate
	from in.policy_info p
		inner join in.Endorsement_Info e on p.casenumber = e.casenumber
		left join fac.Otwpropertyminesubdwellingfac f on p.CoverageA >= f.cova_lower and (p.CoverageA <= f.cova_upper or f.cova_upper = .);

/*Non-Dwelling*/
create table out.otw_590_mine_sub_nondwell as
	select p.casenumber, p.policynumber,
	case when p.HO_0448_Addl_limit_01_pi=0 then 0 else t1.factor end as ND1,
	case when p.HO_0448_Addl_limit_02_pi=0 then 0 else t2.factor end as ND2,
	case when p.HO_0448_Addl_limit_03_pi=0 then 0 else t3.factor end as ND3,
	case when p.HO_0448_Addl_limit_04_pi=0 then 0 else t4.factor end as ND4,
	case when p.HO_0448_Addl_limit_05_pi=0 then 0 else t5.factor end as ND5,
	case when p.HO_0448_Addl_limit_06_pi=0 then 0 else t6.factor end as ND6,
	case when p.HO_0448_Addl_limit_07_pi=0 then 0 else t7.factor end as ND7,
	case when p.HO_0448_Addl_limit_08_pi=0 then 0 else t8.factor end as ND8,
	case when p.HO_0448_Addl_limit_09_pi=0 then 0 else t9.factor end as ND9,
	case when p.HO_0448_Addl_limit_10_pi=0 then 0 else t10.factor end as ND10,

	case when e.HO2388_FLAG = '1' then

	calculated ND1 +
	calculated ND2 +
	calculated ND3 +
	calculated ND4 +
	calculated ND5 +
	calculated ND6 +
	calculated ND7 +
	calculated ND8 +
	calculated ND9 +
	calculated ND10 else 0 end as Mine_Sub_Rate_NonDwelling_Rate

from in.policy_info p inner join in.Endorsement_info as e on p.casenumber=e.casenumber

	left join fac.Otwpropertyminesubnondwellingfac as t1 on p.HO_0448_Addl_limit_01_pi>=t1.os_lower and p.HO_0448_Addl_limit_01_pi<=t1.os_upper
	left join fac.Otwpropertyminesubnondwellingfac as t2 on p.HO_0448_Addl_limit_02_pi>=t2.os_lower and p.HO_0448_Addl_limit_02_pi<=t2.os_upper
	left join fac.Otwpropertyminesubnondwellingfac as t3 on p.HO_0448_Addl_limit_03_pi>=t3.os_lower and p.HO_0448_Addl_limit_03_pi<=t3.os_upper
	left join fac.Otwpropertyminesubnondwellingfac as t4 on p.HO_0448_Addl_limit_04_pi>=t4.os_lower and p.HO_0448_Addl_limit_04_pi<=t4.os_upper
	left join fac.Otwpropertyminesubnondwellingfac as t5 on p.HO_0448_Addl_limit_05_pi>=t5.os_lower and p.HO_0448_Addl_limit_05_pi<=t5.os_upper
	left join fac.Otwpropertyminesubnondwellingfac as t6 on p.HO_0448_Addl_limit_06_pi>=t6.os_lower and p.HO_0448_Addl_limit_06_pi<=t6.os_upper
	left join fac.Otwpropertyminesubnondwellingfac as t7 on p.HO_0448_Addl_limit_07_pi>=t7.os_lower and p.HO_0448_Addl_limit_07_pi<=t7.os_upper
	left join fac.Otwpropertyminesubnondwellingfac as t8 on p.HO_0448_Addl_limit_08_pi>=t8.os_lower and p.HO_0448_Addl_limit_08_pi<=t8.os_upper
	left join fac.Otwpropertyminesubnondwellingfac as t9 on p.HO_0448_Addl_limit_09_pi>=t9.os_lower and p.HO_0448_Addl_limit_09_pi<=t9.os_upper
	left join fac.Otwpropertyminesubnondwellingfac as t10 on p.HO_0448_Addl_limit_10_pi>=t10.os_lower and p.HO_0448_Addl_limit_10_pi<=t10.os_upper;

/*ale*/
create table out.otw_590_mine_sub_ale as
select p.casenumber, p.policynumber, p.partnerflag, 
		case when e.HO2388_FLAG = '1' and substr(p.partnerflag,49,1)='1' then 5
		else 0 end as aleid_rate
from in.policy_info as p inner join in.endorsement_info as e
on p.casenumber=e.casenumber;


/*total*/
create table out.OTW_590_MINE_SUBSIDENCE as
select a.casenumber, a.policynumber, a.Mine_Sub_Dwelling_Rate, b.Mine_Sub_Rate_NonDwelling_Rate, c.aleid_rate,


case when b.county in (
'Clay', 'Martin', 'Putnam',
'Crawford', 'Monroe', 'Spencer',
'Davies', 'Montgomery', 'Sullivan',
'Dubois', 'Orange', 'Vanderburgh',
'Fountain', 'Owen', 'Vermillion',
'Gibson', 'Parke', 'Vigo',
'Greene', 'Perry', 'Warren',
'Knox', 'Pike', 'Warrick',
'Lawrence', 'Posey') then a.Mine_Sub_Dwelling_Rate + b.Mine_Sub_Rate_NonDwelling_Rate + c.aleid_rate else 0 end as Mine_Subsidence,
b.county

from 
out.otw_590_mine_sub_dwell as a
inner join out.otw_590_mine_sub_nondwell as b
on a.casenumber=b.casenumber
inner join out.otw_590_mine_sub_ale as c
on a.casenumber=c.casenumber
join	work.dh0200P p
on 		a.casenumber = p.RATING_CASE_NUMBER
join inputall.VWPOLICYGEOSPATIALDATA b
on p.prime_key=b.prime_key
;
quit;


proc sql;
create table work.MineWithCounty as 
select a.*,b.* from out.OTW_590_MINE_SUBSIDENCE a
join	work.dh0200P p
on 		a.casenumber = p.RATING_CASE_NUMBER
join inputall.VWPOLICYGEOSPATIALDATA b
on p.prime_key=b.prime_key
where	b.county not in (
'Clay', 'Martin', 'Putnam',
'Crawford', 'Monroe', 'Spencer',
'Davies', 'Montgomery', 'Sullivan',
'Dubois', 'Orange', 'Vanderburgh',
'Fountain', 'Owen', 'Vermillion',
'Gibson', 'Parke', 'Vigo',
'Greene', 'Perry', 'Warren',
'Knox', 'Pike', 'Warrick',
'Lawrence', 'Posey')
;
quit;
