*Rule 301 Experience Modifier CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyAccountNumber)
            in.ExperienceFactor
            (Factor Account)
			;
*Creates: 	out.R301_ExperienceFactor;

proc sql;
    create table work.R301_ExperienceModifier as
	select distinct p.CaseNumber, p.PolicyNumber
	      , case when m.Description="" then "Others" else m.Description end as Account
	from in.Policy_Info as p
	     left join source.Experiencemodifier as m
		 on p.PolicyAccountNumber <= m.Account_u
		 and p.PolicyAccountNumber >= m.Account_l
	;
quit;


proc sql;
    create table out.R301_ExperienceFactor as
	select distinct m.CaseNumber
		  , m.PolicyNumber
	      , m.Account
		  , e.Factor as current_Experience
			, case when ytable.CaseNumber=. then e.Factor else round(ytable.PremiumAmount/(ytable.PremiumA+ytable.PremiumH),0.001) end as Experience

	from work.R301_ExperienceModifier as m
		 inner join Fac.experiencefactor e
		 on m.Account = e.Description
		 left join in.ytable_ho3_experience as ytable
		 on m.CaseNumber=ytable.CaseNumber
	;
quit;
