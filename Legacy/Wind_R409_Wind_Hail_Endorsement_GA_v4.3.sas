*Wind Rule 409 Wind Hail Endorsemnt GA;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate PropertyYearRoofInstalled)
            Fac.WindHH80610Factor
            (Factor Roof Age)
			;

* If policy reverted to pre-v4.2 coverage, there are no mandatory ACV terms;
*Creates: 	out.Wind_R409_Wind_Hail_Endorsement;

proc sql;
	create table work.Wind_R409_Wind_Hail_Endorsement as
	select p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.Reverted
		 , floor((case when p.PolicyTermYears=1 and &effst = 0 then p.PolicyIssueDate else p.PolicyEffectiveDate end
	 		- case when p.PropertyYearRoofInstalled <= 0 then date() else mdy(1,1,p.PropertyYearRoofInstalled) end)/365) as Roof_Age
	from in.Policy_Info as p
	; 

	create table out.Wind_R409_Wind_Hail_Endorsement as
	select p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.Reverted
		 , case when p.Reverted in (7,9) then rf.Factor else 1 end as HH8061 label="HH 80 61"

	from work.Wind_R409_Wind_Hail_Endorsement as p
	inner join Fac.WindHH80610Factor as rf
	on p.Roof_Age <= rf.Age_upper
	and p.Roof_age >= rf.Age_lower
	;
quit;
