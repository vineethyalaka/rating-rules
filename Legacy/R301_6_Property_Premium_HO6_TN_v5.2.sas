*HO6 Rule 301.6 Property Premium TN (add sink hole);
*Uses:    	out.OTW_R503_Business_Property
			(CaseNumber PolicyNumber Business_Property)
			out.OTW_R505C_EQ_Loss_Assessment
			(CaseNumber PolicyNumber EQ_Loss_Assessment)
			out.OTW_R505D_Earthquake
			(CaseNumber PolicyNumber Earthquake)
			out.OTW_R507_Incr_CovA
			(CaseNumber PolicyNumber Coverage_A)
			out.OTW_R508_Units_Rented
			(CaseNumber PolicyNumber Units_Rented)
			out.OTW_R511_Loss_Assessment
			(CaseNumber PolicyNumber Loss_Assessment)
			out.OTW_R512_Loss_of_Use
			(CaseNumber PolicyNumber Coverage_D)
			out.OTW_R513_Ordinance_or_Law
			(CaseNumber PolicyNumber Ordinance_or_Law)
			out.OTW_R514_Other_Structures
			(CaseNumber PolicyNumber Other_Structures)
			out.OTW_R515_C_Unsched_Incr_CovC
			(CaseNumber PolicyNumber Unscheduled_Personal_Property)
			out.OTW_R516_Sched_Personal_Prop
			(CaseNumber PolicyNumber Scheduled_Personal_Property)
			out.OTW_R519_Computer
			(CaseNumber PolicyNumber Computer)
			out.OTW_R520_ID_Theft
			(CaseNumber PolicyNumber ID_Theft)
			out.OTW_R521_Water_Sump
			(CaseNumber PolicyNumber Water_Sump)
			out.OTW_R570_Limited_Fungi
			(CaseNumber PolicyNumber Limited_Fungi)
			out.Wind_Premium
			(CaseNumber PolicyNumber Wind_Premium)
			out.OTW_R506_Sink_Hole
			(CaseNumber PolicyNumber Sink_Hole)
			;
*Creates: 	out.Property_Premium;

proc sql;
    create table out.Property_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.Business_Property
		 , b.EQ_Loss_Assessment
		 , c.Earthquake
		 , d.Coverage_A
		 , e.Units_Rented
		 , f.Loss_Assessment
		 , g.Coverage_D
		 , h.Ordinance_or_Law
		 , i.Other_Structures
		 , j.Unscheduled_Personal_Property
		 , k.Scheduled_Personal_Property
		 , l.Computer
		 , m.ID_Theft
		 , n.Water_Sump
		 , o.Wind_Premium
		 , p.Limited_Fungi
		 , q.Sink_Hole
		 , a.Business_Property
			+ b.EQ_Loss_Assessment
			+ c.Earthquake
			+ d.Coverage_A
		 	+ e.Units_Rented
			+ f.Loss_Assessment
			+ g.Coverage_D
			+ h.Ordinance_or_Law
		 	+ i.Other_Structures
			+ j.Unscheduled_Personal_Property
			+ k.Scheduled_Personal_Property
			+ l.Computer
			+ m.ID_Theft
			+ n.Water_Sump
			+ o.Wind_Premium
			+ p.Limited_Fungi
			+ q.Sink_Hole
			as Property_Premium format=10.4
		 , b.EQ_Loss_Assessment
			+ c.Earthquake
			+ m.ID_Theft
			+ n.Water_Sump
			+ q.Sink_Hole
			as Other_Premium format=10.4
	from out.OTW_R503_Business_Property as a 
	inner join out.OTW_R505C_EQ_Loss_Assessment as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_R505D_Earthquake as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.OTW_R507_Incr_CovA as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_R508_Units_Rented as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_R511_Loss_Assessment as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_R512_Loss_of_Use as g 
	on a.CaseNumber = g.CaseNumber
	inner join out.OTW_R513_Ordinance_or_Law as h 
	on a.CaseNumber = h.CaseNumber
	inner join out.OTW_R514_Other_Structures as i 
	on a.CaseNumber = i.CaseNumber
	inner join out.OTW_R515_C_Unsched_Incr_CovC as j 
	on a.CaseNumber = j.CaseNumber
	inner join out.OTW_R516_Sched_Personal_Prop as k 
	on a.CaseNumber = k.CaseNumber
	inner join out.OTW_R519_Computer as l 
	on a.CaseNumber = l.CaseNumber
	inner join out.OTW_R520_ID_Theft as m 
	on a.CaseNumber = m.CaseNumber
	inner join out.OTW_R521_Water_Sump as n 
	on a.CaseNumber = n.CaseNumber
	inner join out.Wind_Premium as o 
	on a.CaseNumber = o.CaseNumber	
	inner join out.OTW_R570_Limited_Fungi as p 
	on a.CaseNumber = p.CaseNumber
	inner join out.OTW_R506_Sink_Hole as q 
	on a.CaseNumber = q.CaseNumber
	;
quit;
