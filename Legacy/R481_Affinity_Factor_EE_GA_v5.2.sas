
%macro EE();
data Employee_fac;
	set affinity_fac;
	where Discount_indicator = 4;
run;

proc sql;
	create table employee as
	select a.casenumber, a.policynumber, a.policyaccountnumber
		, b.Employee_associate_number, c.HO_2492_0913_SERVANT_IND as HD_078_flag
		, case when a.policyaccountnumber in (14200)
			then 1
			else 0		
		  end as partner_ee
		, case when b.Employee_associate_number = '' then 0
			else 1
			end as employee
		, d.Affinity_discount as Employee_Discount
		,calculated partner_ee* calculated employee*Employee_Discount as Employee_fac
	from in.policy_info a 
		inner join in.production_info b on a.casenumber = b.casenumber
		inner join in.endorsement_info c on a.casenumber = c.casenumber,
		Employee_fac d
;
quit;

/*proc sql;*/
/*	create table out.affinity_fac as*/
/*	select p.**/
/*		, e.**/
/*		, partner*auto*'Affinity Discount'n as affinity*/
/*		, partner_ee*employee*'Employee Discount'n as Employee_fac*/
/*		  from affinity_fac f, affinity p*/
/*	inner join employee e on p.casenumber = e.casenumber*/
/*		;*/
/*quit;*/

proc sql;
	create table out.affinity_fac as
	select a.*, b.Employee_fac
	from out.affinity_fac as a left join employee as b
	on a.casenumber = b.casenumber;
quit;	

%if &Process = 0 %then %do;
	title 'RULE 344. AFFINITY MARKETING - EE';
	proc freq data=out.affinity_fac;
	tables partner_ee*employee*Employee_fac / list missing;
	run;
%end;
%mend;
%EE();