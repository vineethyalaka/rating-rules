*Rule 339 Quick Rate Adjusted Premium CW;
*Uses:    	out.Expense_Adjusted_Premium
			(CaseNumber PolicyNumber Expense_Adjusted_Premium)
			fac.quickratefactor
			(factor)
			;
*Creates: 	out.QuickRate_Adjusted_Premium;

proc sql;
    create table out.QuickRate_Adjusted_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , round(a.Expense_Adjusted_Premium,1) As Rounded_Expense_Adjusted_Premium
		 , b.factor as QuickRate
		 , round(calculated Rounded_Expense_Adjusted_Premium*b.factor,1)
			as QuickRate_Adjusted_Premium 
	from out.Expense_Adjusted_Premium as a 
	cross join fac.quickratefactor as b 
	;
quit;
