*Wind Rule 408 Actual Cash Value Loss Settlement - Windstorm or Hail Losses to Roof Surfacing CW (HO-04-93);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate 
			PropertyYearRoofInstalled PropertyRoofTypeCode TerritoryH)
            Fac.WindHO0493Factor
            (Factor Roof_Age Roof_Age_Group Roof_type Territory_Group)
            Fac.WindTerritoryFactor
            (Territory Territory_Group)
			;
*Creates: 	out.Wind_R408_ACV_Roof;

proc sql;
	create table work.Wind_R408_ACV_Roof as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.TerritoryH
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
/*		 *Manual Calculation;*/
/*		 , (case when p.PolicyTermYears=1 then year(p.PolicyIssueDate) else year(p.PolicyEffectiveDate) end*/
/*	 		- case when p.PropertyYearRoofInstalled <= 0 then year(date()) else p.PropertyYearRoofInstalled end) as Roof_Age*/
/*		 *IT Calculation;*/
		 , floor((case when p.PolicyTermYears=1 then p.PolicyIssueDate else p.PolicyEffectiveDate end
	 		- case when p.PropertyYearRoofInstalled <= 0 then date() else mdy(1,1,p.PropertyYearRoofInstalled) end)/365) as Roof_Age
		 , case when calculated Roof_Age >= 30 then '30+'
		 		when calculated Roof_Age < 0 then '-1'
				else left(put(calculated Roof_Age,3.))
			end	as Roof_Age_char
	from in.Policy_Info as p
	;

	create table out.Wind_R408_ACV_Roof as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode, p.Roof_Age, r.Roof_Age_Group, r.Territory_Group
		 , r.HO_04_93 as HO_04_93
	from work.Wind_R408_ACV_Roof as p
	inner join Fac.WindTerritoryFactor as t
	on p.TerritoryH = t.Territory
	inner join Fac.WindRoofFactor as r
	on 	lower(trim(left(r.Roof_Type))) = 
		case when lower(trim(left(p.PropertyRoofTypeCode))) in 
				("asphalt shingles","composition shingles","clay tile","concrete tile","slate"
				,"spanish tile","tar or gravel","tin/membrane","wood shake","wood shingles")
			 then lower(trim(left(p.PropertyRoofTypeCode))) 
			 else "other" 
		end
	and p.Roof_Age_char = r.Roof_Age
	and t.Territory_Group = r.Territory_Group
	;
quit;
