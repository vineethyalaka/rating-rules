*HO4 Hurricane Adjusted Premium - FL;
*Uses:    	out.hurr_base_premium
			(CaseNumber PolicyNumber Hurr_Base_Premium)
			out.hurr_R403_CovC_Replacement_Cost
			(CaseNumber HO_04_90)
			out.hurr_R406_Deductibles
			(CaseNumber Hurr_Deductible)
			out.hurr_r410_BCEG
			(CaseNumber BCEG_Factor)
			out.hurr_r411_loss_mitigation
			(CaseNumber Mitigation_Factor)
			;
*Creates: 	out.hurr_adjusted_premium;


proc sql;
	create table out.hurr_adjusted_premium as
	select a.CaseNumber, a.PolicyNumber, a.hurr_Base_Premium
	, round((c.Hurr_Deductible * a.Hurr_Base_Premium),1.0) as Hurr_Ded_Adj
	, round((b.HO_04_90 * calculated Hurr_Ded_Adj),1.0) as Hurr_PPRC_Adj
	, round((d.BCEG_Factor * calculated Hurr_PPRC_Adj),1.0) as Hurr_BCEG_Adj
	, round((e.Mitigation_Factor * calculated Hurr_BCEG_Adj),1.0) as Hurr_Mitigation_Adj
	, calculated Hurr_Mitigation_Adj as Hurr_Adjusted_Premium
	from out.hurr_base_premium as a
	inner join out.hurr_r403_covc_replacement_cost as b
	on a.CaseNumber = b.CaseNumber
	inner join	out.hurr_r406_deductibles as c
	on a.CaseNumber = c.CaseNumber
	inner join	out.hurr_r410_BCEG as d
	on a.CaseNumber = d.CaseNumber
	inner join out.hurr_r411_loss_mitigation as e
	on a.CaseNumber = e.CaseNumber
	;
quit;
