*Rule 301.9.1 Capping Amount CW (from System);
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber)
			in.Capping_Info
            (CaseNumber PolicyNumber cap)
			;
*Creates: 	out.Capping_Amount;

proc sql;
    create table out.Capping_Amount as
	select distinct p.CaseNumber, p.PolicyNumber, c.cap as capping_amount
	from in.policy_info as p
	inner join in.Capping_Info as c
	on p.CaseNumber = c.CaseNumber and p.PolicyNumber = c.PolicyNumber
	;
quit;
