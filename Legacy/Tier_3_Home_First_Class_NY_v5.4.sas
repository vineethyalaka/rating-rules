*Tier 3 Home First Class CW;
*Uses:    	in.hf_info
			(CaseNumber PolicyNumber AutoPolicyNumber PolicyAccountNumber PolicyTermYears PolicyEffectiveDate Ratingversion)
            source.HS_V4Rating_AffinityDiscount
            (version minmarketgroup maxmarketgroup factor)
			;
*Creates: 	out.Tier3_HomeFirstClass;


proc sql;
	create table out.Tier3_HomeFirstClass as
	select distinct a.CaseNumber
		 , a.PolicyNumber, a.PolicyEffectiveDate, a.FirstTerm, y.Group, y.ageofnypolicy
		 , a.PolicyAccountNumber
		 , a.AutoPolicyNumber
		 , case when trim(a.AutoPolicyNumber) = "" then "N"
		 	else "Y"
			end as AutoPolicyFlag
		 , a.ratingversion
		 , aff.description
		 , case when a.PolicyAccountNumber in (1050, 1400, 10001, 30001) then 2
		 		when aff.factor in (.,1) then 1 				/*not eligible for affinity discount*/
				when calculated AutoPolicyFlag = "Y" then 1	/*eligible for affinity discount and have auto policy*/
				else 2
				end as Home_First_Class 
	from in.hf_info as a
	inner join out.policy_year as y
		on a.casenumber = y.casenumber
	left join source.HS_V4Rating_AffinityDiscount as aff
		on a.PolicyAccountNumber <= input(aff.maxmarketgroup,8.)
		and a.PolicyAccountNumber >= input(aff.minmarketgroup,8.)
		and a.Ratingversion = aff.version
	;
quit;

