*Tier 6 b Lapse Class HO6 NJ;
*Uses:    	out.Tier6_Lapse
            (CaseNumber PolicyNumber Lapse_Class)
            Fac.TierLapseFactor
            (Factor)
			;
*Creates: 	out.Tier6_Lapse_Tier;

proc sql;
    create table out.Tier6_Lapse_Tier as
	select distinct c.CaseNumber, c.PolicyNumber
		  , c.Lapse_class
		  , f.Factor as Lapse_class_Factor
	from out.Tier6_Lapse as c
		inner join Fac.TierLapseFactor as f
		on c.Lapse_class = f.Tier
	
    ;
quit;
