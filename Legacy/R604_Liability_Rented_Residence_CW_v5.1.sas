*Rule 604 Liability Additional Residence Rented to Others CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PropertyUnitsInBuilding ResidencesRentedToOthers CoverageE CoverageF)
            Fac.LiabilityRentedResidenceEFactor
            (Factor Families CoverageE)
            Fac.LiabilityRentedResidenceFFactor
            (Factor Families CoverageF)
			;
*Creates: 	out.Liability_Rented_Residence;

proc sql;
    create table work.Liability_Rented_Residence_1 as
	select distinct a.CaseNumber
		 , a.PolicyNumber
		 , a.CoverageE
		 , a.CoverageF
		 , a.IDX_NO_M
		 , a.add_dwelling_type
		 , a.HO70_Flag
		 , (case when a.add_dwelling_type = 3 then 1 else 0 end) * ce.Factor as Rented_Residence_E
		 , (case when a.add_dwelling_type = 3 then 1 else 0 end) * cf.Factor as Rented_Residence_F
	from in.Additional_Residences_Info as a
	inner join Fac.LiabilityRentedResidenceEFactor as ce on a.Add_Number_of_Families = ce.Families and a.CoverageE = ce.CoverageE
	inner join Fac.LiabilityRentedResidenceFFactor as cf on a.Add_Number_of_Families = cf.Families and a.CoverageF = cf.CoverageF
;
quit;

proc sql;
	create table work.Liability_Rented_Residence_2 as
	select distinct a.CaseNumber
	  	 , sum(a.Rented_Residence_E) as Rented_E_Total
		 , sum(a.Rented_Residence_F) as Rented_F_Total
	from work.Liability_Rented_Residence_1 as a
	group by a.CaseNumber;
quit;

proc sql;
    create table out.Liability_Rented_Residence as
	select distinct p.CaseNumber
		 , p.PolicyNumber
		 , case when a.Rented_E_Total = . then 0 else a.Rented_E_Total end as Rented_Residence_E label="Rented Residence E"
		 , case when a.Rented_F_Total = . then 0 else a.Rented_F_Total end as Rented_Residence_F label="Rented Residence F"
	from in.Policy_Info as p
	left join work.Liability_Rented_Residence_2 as a
	on p.CaseNumber = a.CaseNumber
;
quit;
