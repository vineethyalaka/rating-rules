*Rule 301.2 OTW Tiered Base Premium CW;
*Uses:    	out.OTW_Base_Premium
            (CaseNumber PolicyNumber OTW_Base_Premium)
			out.Tier_Factor
            (CaseNumber PolicyNumber Tier_Factor)
			;
*Creates: 	out.OTW_Tiered_Base_Premium;

proc sql;
    create table out.OTW_Tiered_Base_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.OTW_Base_Premium
		 , b.Tier_Factor
		 , round(a.OTW_Base_Premium*Tier_Factor,0.01) as OTW_Tiered_Base_Premium
	from out.OTW_Base_Premium as a 
	inner join out.Tier_Factor as b 
	on a.CaseNumber = b.CaseNumber
	;
quit;
