*Rule 590.3 Wind Rating Factors Premium CW (with Row House, HO2);
*Uses:    	out.Wind_Base_Premium
			(CaseNumber PolicyNumber Wind_Base_Premium)
			out.Wind_Deductible
			(CaseNumber PolicyNumber Wind_Deductible)
			out.Wind_R402_Row_House
			(CaseNumber PolicyNumber Wind_Row_House)
			out.Wind_R471_Roofing_Age_Materials
			(CaseNumber PolicyNumber Roof_Age_Materials)
			;
*Creates: 	out.Wind_Rating_Factors_Premium;

proc sql;
    create table out.Wind_Rating_Factors_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , b.Wind_Deductible
		 , h.Roof_Age_Materials as Wind_Roof_Age_Materials
		 , j.Wind_Row_House 
		 , a.Wind_Base_Premium
		 , a.Wind_Base_Premium*b.Wind_Deductible*h.Roof_Age_Materials*j.Wind_Row_House
			as Wind_Rating_Factors_Premium format=10.4
	from out.Wind_Base_Premium as a 
	inner join out.Wind_Deductible as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Wind_R471_Roofing_Age_Materials as h 
	on a.CaseNumber = h.CaseNumber
	inner join out.Wind_R402_Row_House as j 
	on a.CaseNumber = j.CaseNumber
	;
quit;
