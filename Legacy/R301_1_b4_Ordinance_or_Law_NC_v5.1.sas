*Rule 301.1.b4 Ordinance or Law CW (HO 04 77);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber ORD_LAW_FLAG)
            Fac.OTWOrdinanceOrLawFactor
            (Factor)
			;
*Creates: 	out.OTW_Ordinance_or_Law_Factor;
*SL 09/29/2017 Added factor for $60,000 to $140,000 Coverage A;

proc sql;
    create table out.OTW_Ordinance_or_Law_Factor as
	select e.CaseNumber, e.PolicyNumber, case when e.ORD_LAW_FLAG = '1' and p.CoverageA >= 60000 and p.CoverageA <= 140000 then olf.Factor2
		when  e.ORD_LAW_FLAG = '0' then 1
		else olf.Factor1 
		end as OTW_Ordinance_or_Law
	from Fac.OTWOrdinanceOrLawFactor as olf, in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber 
	;
quit;
