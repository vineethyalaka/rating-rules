*Rule 619 Liability Escaped Fuel;
*Uses:    	in.Production_Info
            (CaseNumber PolicyNumber Remediation HO_04_Escaped_Liquid_Fuel)
			in.Policy_Info
			(CaseNumber PolicyNumber Oil_Tank_Type)
			Fac.liabilityescapedliquidfuelfactor
			(Remediation HH_05_47 Oil_Tank_Type Factor)
*Creates: 	out.Liability_Escaped_Fuel;

proc sql;
    create table out.Liability_Escaped_Fuel as
	Select p.CaseNumber, p.PolicyNumber, m.Remediation, p.HO_04_Escaped_Liquid_Fuel, o.Oil_Tank_Type, q.Factor as Fuel
	from in.Production_Info as p 
	inner join in.Policy_Info as i
		on p.CaseNumber = i.CaseNumber
	inner join in.datamainfo as m
		on p.CaseNumber = m.CaseNumber
	inner join in.oiltank_info as o
		on p.CaseNumber = o.CaseNumber

	inner join Fac.liabilityescapedliquidfuelfactor as q
		on m.Remediation = q.Remediation
		and p.HO_04_Escaped_Liquid_Fuel = q.HH_05_47
		and q.Oil_Tank_Type = o.Oil_Tank_Type

	;
quit;
