*HO4 Rule 301.1 OTW Base Premium CW;
*Uses:    	out.OTW_Territory_Rate
            (CaseNumber PolicyNumber OTW_Territory_Rate)
            out.OTW_AOI_Factor
            (CaseNumber PolicyNumber OTW_AOI)
			;
*Creates: 	out.OTW_Base_Premium;


proc sql;
create table Out.Database_OTW_Base_Premium as
select 
a.policynumber,
a.casenumber,
m.premiumA
from in.Policy_Info as a
inner join &server..v3ratingresult as m
on a.casenumber=m.casenumber
;
quit;

proc sql;
    create table out.OTW_Base_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , a.OTW_Territory_Rate
		 , b.OTW_AOI
		 , round(a.OTW_Territory_Rate*b.OTW_AOI,1) as Origin_OTW_Base_Premium
		 , db.premiumA as OTW_Base_Premium
	from out.OTW_Territory_Rate as a 
	inner join out.OTW_AOI_Factor as b 
	on a.CaseNumber = b.CaseNumber
	inner join Out.Database_OTW_Base_Premium as db 
	on a.CaseNumber = db.CaseNumber

	;
quit;
