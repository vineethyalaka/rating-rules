*Rule 481 Affinity Factor Mindi CW;

/*Versions*/
/*2/1/16 5.0 SH - Added Mindi partner discount structure to Rule 481 Affinity Factor CW 5.0*/

proc sql;
	create table out.R481_Affinity as
	select  a.CaseNumber, 
			a.PolicyNumber,
			a.Partner_Customer,
			a.PolicyAccountNumber,
			b.AutoPolicyFlag,
			case when a.PolicyAccountNumber in (50000:50099 /*Wells Fargo*/) and b.AutoPolicyFlag="N" and a.Partner_Customer="0"
				then 1
			else b.Factor end as Affinity label="Affinity"
	from in.policy_info as a
	inner join Fac.AffinityFactor as b
	on a.PolicyAccountNumber <= b.AccNumMax
	and a.PolicyAccountNumber >= b.AccNumMin
	and (a.AutoPolicyNumber is null and b.AutoPolicyFlag="N"
		or a.AutoPolicyNumber is not null and b.AutoPolicyFlag="Y")
	;
quit;

