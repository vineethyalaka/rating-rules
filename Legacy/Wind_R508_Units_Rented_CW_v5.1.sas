*HO6 Wind Rule 508 Units Rented CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber Weeks_Rented_to_Others)
            Fac.WindPropertyUnitRentedFactor
            (Week_upper Week_lower Factor)
			out.Wind_Base_Premium
			(CaseNumber Wind_Base_Premium)
			out.Wind_Deductible
			(CaseNumber Wind_Deductible)
			;
*Creates: 	out.Wind_R508_Units_Rented;

proc sql;
	create table out.Wind_R508_Units_Rented as
	select distinct e.CaseNumber, e.PolicyNumber, ur.Factor
		 , round(p.Wind_Base_Premium*d.Wind_Deductible*(ur.Factor-1),1) as Units_Rented
	from in.Endorsement_Info as e
		inner join out.Wind_Base_Premium as p
		on e.casenumber = p.casenumber
		inner join out.Wind_Deductible as d
		on e.casenumber = d.casenumber
		inner join Fac.WindPropertyUnitRentedFactor as ur
		on input(Weeks_Rented_to_Others,3.) <= ur.week_upper
		and input(Weeks_Rented_to_Others,3.) >= ur.week_lower
	;
quit;
