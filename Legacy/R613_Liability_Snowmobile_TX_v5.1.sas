*Rule 613 Liability Snowmobile CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF)
            in.Endorsement_Info
            (CaseNumber PolicyNumber HO_2464_FLAG HO_2464_Make_Model_01 HO_2464_Make_Model_02 HO_2464_Make_Model_03)
            out.LiabilitySnowmobileCovEFactor
            (Factor CoverageE)
            Fac.LiabilitySnowmobileCovFFactor
            (Factor CoverageF)
			;
*Creates: 	out.Liability_Snowmobile;

proc sql;
    create table out.Liability_Snowmobile as
	select p.CaseNumber, p.PolicyNumber
		 , case when e.HO_2464_Make_Model_01="" then 0 else 1 end as Snowmobile1
		 , case when e.HO_2464_Make_Model_02="" then 0 else 1 end as Snowmobile2
		 , case when e.HO_2464_Make_Model_03="" then 0 else 1 end as Snowmobile3
		 , calculated Snowmobile1 + calculated Snowmobile2 + calculated Snowmobile3 as num_snowmobile 
		 , case when e.HO_2464_FLAG="1" then sme.Factor*calculated num_snowmobile else 0 end as Snowmobile_E
		 , case when e.HO_2464_FLAG="1" then smf.Factor*calculated num_snowmobile else 0 end as Snowmobile_F
	from in.Policy_Info as p
	inner join in.Endorsement_Info as e
	on e.CaseNumber = p.CaseNumber
/*	inner join Fac2.LiabilitySnowmobileCovEFactor as sme*/
		inner join Fac.LiabilitySnowmobileCovEFactor as sme

	on p.CoverageE = sme.CoverageE
/*	inner join Fac2.LiabilitySnowmobileCovFFactor as smf*/
		inner join Fac.LiabilitySnowmobileCovFFactor as smf
	on p.CoverageF = smf.CoverageF
	;
quit;
