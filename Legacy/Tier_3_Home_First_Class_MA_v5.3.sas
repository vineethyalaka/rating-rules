*Tier 3 Home First Class MA;
*Uses:    	in.hf_info
			(CaseNumber PolicyNumber AutoPolicyNumber PolicyAccountNumber PolicyTermYears PolicyEffectiveDate Ratingversion)
            source.HS_V4Rating_AffinityDiscount
            (version minmarketgroup maxmarketgroup factor)
			Fac.tierHFclass
			(class factor)
			;
*Creates: 	out.Tier3a_HomeFirstClass
			out.Tier3b_HomeFirstClass;


proc sql;
	create table out.Tier3a_HomeFirstClass as
	select distinct a.CaseNumber
		 , a.PolicyNumber, a.PolicyEffectiveDate, a.FirstTerm
		 , a.PolicyAccountNumber
		 , a.AutoPolicyNumber
		 , case when trim(a.AutoPolicyNumber) = "" then "N"
		 	else "Y"
			end as AutoPolicyFlag
		 , a.ratingversion
		 , aff.description
		 , case when a.PolicyAccountNumber in (1050, 1400, 10001, 30001) then 2
		 		when aff.factor in (.,1) then 1 				/*not eligible for affinity discount*/
				when calculated AutoPolicyFlag = "Y" then 1	/*eligible for affinity discount and have auto policy*/
				else 2
				end as Home_First_Class 
	from in.hf_info as a
	left join source.HS_V4Rating_AffinityDiscount as aff
	on a.PolicyAccountNumber <= input(aff.maxmarketgroup,8.)
	and a.PolicyAccountNumber >= input(aff.minmarketgroup,8.)
	and a.Ratingversion = aff.version
	;
quit;

proc sql;
	create table out.Tier3b_HomeFirstClass as
	select distinct b.CaseNumber
		 , b.PolicyNumber
		 , b.Home_First_Class
		 , c.Factor as Home_First_Factor
	from out.Tier3a_HomeFirstClass as b
	inner join fac.tierHFclass as c
	on b.Home_First_Class = c.Class
	;
quit;
