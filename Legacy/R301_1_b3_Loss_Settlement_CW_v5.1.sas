*Rule 301.1.b3 Loss Settlement CW (HO 04 56);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_04_56_PCT)
            Fac.OTWHO0456Factor
            (Factor2 Percent)
			;
*Creates: 	out.OTW_Loss_Settlement_Factor;

proc sql;
    create table out.OTW_Loss_Settlement_Factor as
	select  distinct e.CaseNumber, e.PolicyNumber, case when e.HO_0456_FLAG='0' then 1 else hf456.Factor2 end as OTW_HO_04_56 label="OTW HO-04-56"
	from in.Endorsement_Info as e
	inner join Fac.OTWHO0456Factor as hf456
	on e.HO_04_56_PCT = hf456.Percent
	;
quit;
