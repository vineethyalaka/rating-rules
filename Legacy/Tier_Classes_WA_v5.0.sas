*Tier Classes CW (DRC);
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class)
            out.Tier2_ShopperClass
            (CaseNumber PolicyNumber Shopper_Class)
            out.Tier3_HomeFirstClass
            (CaseNumber PolicyNumber Home_First_Class)
            out.Tier4_PriorClaims
            (CaseNumber PolicyNumber Prior_Claims_Class)
            out.Tier5_RCMVClass
            (CaseNumber PolicyNumber RCMV_Class)
            out.Tier6_LapseInsurance
            (CaseNumber PolicyNumber Lapse_of_Insurance_Class)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class)
			;
*Creates: 	out.Tier_Classes;

proc sql;
    create table out.Tier_Classes as
    select distinct 
    a.CaseNumber
	, a.PolicyNumber, a.insurance_class, b.Shopper_Class, c.Home_First_Class, d.Prior_Claims_Class
	, e.RCMV_Class,  f.Lapse_of_Insurance_Class, g.DRC_Class,  h.Occupancy_Tier  
    from out.Tier1_InsuranceScore as a
	inner join out.Tier2_ShopperClass as b
	on a.CaseNumber = b.CaseNumber
	inner join out.Tier3_HomeFirstClass as c
	on a.CaseNumber = c.CaseNumber
	inner join out.Tier4_PriorClaims as d
	on a.CaseNumber = d.CaseNumber
	inner join out.Tier5_RCMVClass as e
	on a.CaseNumber = e.CaseNumber
	inner join out.Tier6_LapseInsurance as f
	on a.CaseNumber = f.CaseNumber
	inner join out.Tier7_DRC as g
	on a.CaseNumber = g.CaseNumber
	inner join out.tier8_occupancy_tier as h
	on a.CaseNumber = h.CaseNumber
	;
quit;
