*HO6 Rule 590.4 Wind Property Premium CW;
*Uses:    	out.Wind_R503_Business_Property
			(CaseNumber PolicyNumber Business_Property)
			out.Wind_R507_Incr_CovA
			(CaseNumber PolicyNumber Coverage_A)
			out.Wind_R508_Units_Rented
			(CaseNumber PolicyNumber Units_Rented)
			out.Wind_R511_Loss_Assessment
			(CaseNumber PolicyNumber Loss_Assessment)
			out.Wind_R512_Loss_of_Use
			(CaseNumber PolicyNumber Coverage_D)
			out.Wind_R513_Ordinance_or_Law
			(CaseNumber PolicyNumber Ordinance_or_Law)
			out.Wind_R514_Other_Structures
			(CaseNumber PolicyNumber Other_Structures)
			out.Wind_R515_C_Unsched_Incr_CovC
			(CaseNumber PolicyNumber Unscheduled_Personal_Property)
			;
*Creates: 	out.Wind_Property_Premium;

proc sql;
    create table out.Wind_Property_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Business_Property
		 , b.Coverage_A
		 , c.Units_Rented
		 , d.Loss_Assessment
		 , e.Coverage_D
		 , f.Ordinance_or_Law
		 , g.Other_Structures
		 , h.Unscheduled_Personal_Property
		 , a.Business_Property
			+ b.Coverage_A
			+ c.Units_Rented
			+ d.Loss_Assessment
			+ e.Coverage_D
			+ f.Ordinance_or_Law
			+ g.Other_Structures
			+ h.Unscheduled_Personal_Property
			as Wind_Property_Premium format=10.4
	from out.Wind_R503_Business_Property as a 
	inner join out.Wind_R507_Incr_CovA as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Wind_R508_Units_Rented as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.Wind_R511_Loss_Assessment as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Wind_R512_Loss_of_Use as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.Wind_R513_Ordinance_or_Law as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.Wind_R514_Other_Structures as g 
	on a.CaseNumber = g.CaseNumber
	inner join out.Wind_R515_C_Unsched_Incr_CovC as h 
	on a.CaseNumber = h.CaseNumber
	;
quit;
