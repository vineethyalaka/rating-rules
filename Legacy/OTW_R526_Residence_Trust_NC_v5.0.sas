*R526 for residence trust created by Di -- 01/03/2016;

proc sql;
	create table out.OTW_R526_Residence_Trust as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_3212_FLAG ne 0 then round(rt.Factor,1) else 0 end as Residence_Trust
	from Fac.OTWPropertyresidencetrust as rt
		, in.Endorsement_Info as e
	;
quit;
