

proc sql;
	create table out.OTW_R490_Theft_Exclusion as
	select distinct p.CaseNumber, p.PolicyNumber, f.Factor as Theft_Exclusion
	from in.Policy_Info as p
	inner join in.Endorsement_Info as e
	on p.casenumber = e.casenumber
	inner join Fac.OTWTheftExclusionFactor as f
	on e.TheftExclusion = f.Endorsement
	and p.TerritoryA = f.Territory
	;
quit;
