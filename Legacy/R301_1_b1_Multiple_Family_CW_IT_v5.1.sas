/* uses IT factor table */

proc sql;
    create table out.OTW_Multiple_Families_Factor as
	select  distinct p.CaseNumber, p.PolicyNumber
		 , mff.Cov_B as Cov_B, mff.Cov_C as Cov_C
		 , mff.Factor as OTW_Multiple_Families_Factor label="OTW Multiple Families Factor"
	from in.policy_info as p
    inner join Fac.OTWMultipleFamilyFactorIT as mff
    on p.PropertyUnitsInBuilding = mff.NumFamilies
	;
quit;
