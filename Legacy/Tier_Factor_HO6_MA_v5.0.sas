*HO6 Tier Factor MA (occupancy class not used);
*Uses:    	out.Tier4_PriorClaims
            (CaseNumber PolicyNumber Prior_Claims_Class Prior_Claims_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
            c.CaseNumber, c.PolicyNumber
          , c.Prior_Claims_Class
		  , round(c.Prior_Claims_Factor,0.000001) as Tier_Factor
    from out.Tier4_PriorClaims as c
	;
quit;
