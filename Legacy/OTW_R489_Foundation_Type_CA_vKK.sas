*OTW Rule 489 Foundation Type;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyFoundationType)
            Fac.OTWFoundationTypeFactor
            (FoundationType Factor)
			;
*Creates: 	out.OTW_R489_Foundation_Type;

proc sql;
	create table out.OTW_R489_Foundation_Type as
	select p.CaseNumber, p.PolicyNumber
		 , p.PropertyFoundationType
		 , ftf.Factor as OTW_Foundation_Type label="OTW Foundation"
	from in.policy_info as p
	inner join Fac.OTWFoundationTypeFactor as ftf
	on  p.PropertyFoundationType = ftf.FoundationTypeCode
	;
quit;