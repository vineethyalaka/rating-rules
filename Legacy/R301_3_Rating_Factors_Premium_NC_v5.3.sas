**=====================================================================================**
History: 2015 09 29 SY  Based on NC_v5.2: Delete R302 & R407_2, Added R401 & R408
**=====================================================================================**;

proc sql;
    create table out.Rating_Factors_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , a.Base_Premium
		 , b.OTW_Superior_Construction
		 , case when c.Coverage_C < 0 then 0 else c.Coverage_C end as IncrC
		 , d.HO_04_90
		 , e.Burglar_Alarm
		 , e.Fire_Alarm
		 , e.Smoke_Detector
		 , e.Sprinklers
		 , f.Deductible_Factor
		 , g.HO_04_20
		 , h.HO_04_93
		 , i.Age_of_Home
		 , round(a.Base_Premium*(b.OTW_Superior_Construction-1),1) as OTW_Superior_Prem
		 , round((a.Base_Premium+calculated IncrC)*(d.HO_04_90-1),1) as HO0490_Prem_pre
		 , case when (calculated HO0490_Prem_pre < max.HO0490 and calculated HO0490_Prem_pre > 0) then max.HO0490 else calculated HO0490_Prem_pre end as HO0490_Prem
		 , round((a.Base_Premium+calculated IncrC)*(e.Burglar_Alarm+e.Fire_Alarm+e.Smoke_Detector+e.Sprinklers-4),1) as ProtectiveDevices_Prem_pre
		 , case when calculated ProtectiveDevices_Prem_pre < -max.ProtectiveDevices then -max.ProtectiveDevices else calculated ProtectiveDevices_Prem_pre end as ProtectiveDevices_Prem
		 , round(a.Base_Premium*(f.Deductible_Factor-1),1) as Deductible_Prem
		 , round(a.Base_Premium*(g.HO_04_20-1),1) as HO0420_Prem
		 , round(a.Base_Premium*(h.HO_04_93-1),1) as HO0493_Prem
		 , round(a.Base_Premium*(i.Age_of_Home-1),1) as AgeOfHome_Prem
		 , calculated OTW_Superior_Prem+calculated HO0490_Prem+calculated ProtectiveDevices_Prem+Calculated Deductible_Prem+
		 	calculated HO0420_Prem+calculated HO0493_Prem+calculated AgeOfHome_Prem as Rating_Factors_Premium
	from Fac.OTWMaxCreditsFactor as max, out.Base_Premium as a
	inner join out.OTW_R401_Superior_Constr as b
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_R515_A_Incr_CovC as c
	on a.CaseNumber = c.CaseNumber
	inner join out.OTW_R403_CovC_Replacement_Cost as d
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_R404_Protective_Devices as e
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_R406_Deductibles as f
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_R407_1_Add_AMT_CovA as g
	on a.CaseNumber = g.CaseNumber
	inner join out.OTW_R408_ACV_Roof as h
	on a.CaseNumber = h.CaseNumber
	inner join out.OTW_R470_Age_of_Home as i
	on a.CaseNumber = i.CaseNumber
	;
quit;
