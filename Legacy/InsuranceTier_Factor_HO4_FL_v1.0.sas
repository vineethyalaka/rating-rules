*HO4 Insurance Score Tier Factor - FL;
*Uses:    	out.Tier_Occupancy
			(CaseNumber PolicyNumber Occupancy_Tier)
			out.Tier_InsuranceScore
			(CaseNumber insurance_class)
            Fac.InsuranceResidentsTierFactor
            (InsuranceScoreTier ResidentsTier Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor_Insurance as
	select  o.CaseNumber, o.PolicyNumber
          , o.Occupancy_Tier, i.insurance_class
          , t.Factor as  Tier_Factor
    from out.Tier_Occupancy as o, out.Tier_InsuranceScore as i, fac.InsuranceResidentsTierFactor as t
	where o.CaseNumber = i.CaseNumber
	and o.Occupancy_Tier = t.ResidentsTier
	and i.Insurance_Class = t.InsuranceScoreTier
	;
quit;
