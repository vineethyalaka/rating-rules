*Wind Rule 402 Row House CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyStyleOfHome)
            Fac.WindRowHouseFactor
            (RowHouse Factor)
			;
*Creates: 	out.Wind_R402_Row_House;

proc sql;
	create table out.Wind_R402_Row_House as
	select distinct p.CaseNumber, p.PolicyNumber
		 , p.PropertyStyleOfHome
		 , rhf.Factor as Wind_Row_House label="Wind Row House"
	from in.policy_info as p
	inner join Fac.WindRowHouseFactor as rhf
	on  upper(trim(left(p.PropertyStyleOfHome))) = upper(trim(left(rhf.RowHouse)))
	;
quit;
