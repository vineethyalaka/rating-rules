*HO6 Tier Factor CW;
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class insurance_class_factor)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier Occupancy_Tier_Factor)
            out.Tier4_PriorClaims
            (CaseNumber PolicyNumber Prior_Claims_Class Prior_Claims_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
            c.CaseNumber, c.PolicyNumber
          , c.Prior_Claims_Class, f.Factor as PriorClaimsFactor
		  , round(f.factor,0.000001) as Tier_Factor
    from out.Tier4_PriorClaims as c
	inner join Fac.tierpriorclaimsfactor f
	on c.Prior_Claims_Class = f.Class
	;
quit;
