*OTW Rule 404 Protective Devices CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber ProtDevBurglarAlarm ProtDevFireAlarm ProtDevSprinklers)
            Fac.OTWProtDevBurglarAlarmFactor
            (Factor ProtDevBurglarAlarm)
            Fac.OTWProtDevFireAlarmFactor
            (Factor ProtDevFireAlarm)
            Fac.OTWProtDevSprinklersFactor
            (Factor ProtDevSprinklers)
			;
*Creates: 	out.OTW_R404_Protective_Devices;

proc sql;
	create table out.OTW_R404_Protective_Devices as
	select distinct p.CaseNumber, p.PolicyNumber
		 , baf.Factor as Burglar_Alarm
		 , faf.Factor as Fire_Alarm
		 , sdf.Factor as Smoke_Detector
		 , sf.Factor as Sprinklers

	from in.Policy_Info as p

	inner join Fac.OTWProtDevBurglarAlarmFactor as baf
	on ((p.ProtDevBurglarAlarm = "" and baf.ProtDevBurglarAlarm = "") 
	or p.ProtDevBurglarAlarm = baf.ProtDevBurglarAlarm)
	and ((p.ProtDevFireAlarm = "" and baf.ProtDevFireAlarm = "")
	or p.ProtDevFireAlarm = baf.ProtDevFireAlarm)

	inner join Fac.OTWProtDevFireAlarmFactor as faf
	on ((p.ProtDevBurglarAlarm = "" and faf.ProtDevBurglarAlarm = "") 
	or p.ProtDevBurglarAlarm = faf.ProtDevBurglarAlarm)
	and ((p.ProtDevFireAlarm = "" and faf.ProtDevFireAlarm = "")
	or p.ProtDevFireAlarm = faf.ProtDevFireAlarm)

	inner join Fac.OTWProtDevSprinklersFactor as sf
	on p.ProtDevSprinklers = sf.ProtDevSprinklers
	and p.TXSprinkler = sf.TXSprinklers
	inner join Fac.otwprotdevsmokedetectorsfactor as sdf
	on p.ProtDevSmokeDetector = sdf.Smoke
	;
quit;
