** RULE 541. IBHS_fortified_roof **;
%macro IBHS_fortified_roof();

	proc sql;
		create table out.OTW_R542_IBHS_fortified_roof as
		select p.CaseNumber, p.PolicyNumber, p.HA_0638_FLAG, PolicyFormNumber
			, case when p.HA_0638_FLAG <> '' and PolicyFormNumber = 3 then round(f.factor,1.0) else 0 end as FortifiedRoof_Rate
		from in.Endorsement_Info p, fac.OTWIBHSFORTIFIEDROOF f;
	quit;


%mend;
%IBHS_fortified_roof();
