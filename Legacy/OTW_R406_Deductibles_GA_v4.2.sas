*HO3 OTW Rule 406 Deductibles CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible)
            out.OTW_Tiered_Base_Premium
            (CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
            Fac.OTWDeductibleFactor
            (CoverageDeductible Factor Max_Credit)
			;
*Creates: 	out.OTW_R406_Deductibles;

proc sql;
	create table work.OTW_R406_Deductibles as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when substr(left(p.CoverageDeductible),1,5) in("$250","$500","$100","$2000","0") then "$1000"
			else p.CoverageDeductible
			end as CoverageDeductible_adj
	from in.Policy_Info as p
	;
quit;

proc sql;
	create table out.OTW_R406_Deductibles as
	select p.CaseNumber, p.PolicyNumber
		 , df.Factor as Deductible label="Deductible"
		 , df.Max_Credit
		 , case when (1 - df.Factor)*t.OTW_Tiered_Base_Premium > df.Max_Credit then (1 - df.Max_Credit/t.OTW_Tiered_Base_Premium)
			else df.Factor end as OTW_Deductible_Factor
	from work.OTW_R406_Deductibles as p
	inner join out.OTW_Tiered_Base_Premium as t
	on p.CaseNumber = t.CaseNumber
	inner join Fac.OTWDeductibleFactor as df
	on p.CoverageDeductible_adj = df.CoverageDeductible
	;
quit;
