

proc sql;
    create table out.OTW_Rating_Factors_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , d.HO_04_90 as OTW_HO_04_90
		 , f.OTW_Deductible_Factor
		 , n.Mature_Owner as OTW_Mature_Owner
		 , o.Number_of_Units as OTW_Number_of_Units
		 , s.Theft_Exclusion
		 , a.OTW_Tiered_Base_Premium
		 , a.OTW_Tiered_Base_Premium*d.HO_04_90*f.OTW_Deductible_Factor
			*n.Mature_Owner*o.Number_of_Units*s.Theft_Exclusion
			as OTW_Rating_Factors_Premium format=10.4
	from out.OTW_Tiered_Base_Premium as a 
	inner join out.OTW_R403_CovC_Replacement_Cost as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_R406_Deductibles as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_R480_Mature_Owner as n 
	on a.CaseNumber = n.CaseNumber
	inner join out.OTW_R488_Number_of_Units as o 
	on a.CaseNumber = o.CaseNumber
	inner join out.OTW_R490_Theft_Exclusion as s 
	on a.CaseNumber = s.CaseNumber
	;
quit;
