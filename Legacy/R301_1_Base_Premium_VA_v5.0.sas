*Rule 301.1 OTW Base Premium VA;
*Uses:    	out.OTW_Territory_Rate
            (CaseNumber PolicyNumber OTW_Territory_Rate)
            out.OTW_Form_Factor
            (CaseNumber PolicyNumber OTW_Policy_Form)
            out.OTW_Prot_Constr_Factor
            (CaseNumber PolicyNumber OTW_Prot_Constr)
            out.OTW_AOI_Factor
            (CaseNumber PolicyNumber OTW_AOI)
            out.OTW_Multiple_Families_Factor
            (CaseNumber PolicyNumber OTW_Multiple_Families_Factor)
            out.OTW_Loss_Settlement_Factor
            (CaseNumber PolicyNumber OTW_HO_04_56)
            out.OTW_Ordinance_or_Law_Factor
            (CaseNumber PolicyNumber OTW_Ordinance_or_Law)
            out.OTW_HH_00_15_Factor
            (CaseNumber PolicyNumber OTW_HH_00_15)
			out.OTW_Loss_Settlement_Factor_2
			(CaseNumber PolicyNumber OTW_HO_05_22)
			;
*Creates: 	out.OTW_Base_Premium;

proc sql;
    create table out.OTW_Base_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.OTW_Territory_Rate
		 , b.OTW_Policy_Form
		 , c.OTW_Prot_Constr
		 , d.OTW_AOI
		 , e.OTW_Multiple_Families_Factor
		 , f.OTW_HO_04_56
		 , i.OTW_HO_05_22
		 , g.OTW_Ordinance_or_Law
		 , h.OTW_HH_00_15
		 , a.OTW_Territory_Rate*b.OTW_Policy_Form*c.OTW_Prot_Constr*d.OTW_AOI
		   *e.OTW_Multiple_Families_Factor*f.OTW_HO_04_56*g.OTW_Ordinance_or_Law*h.OTW_HH_00_15*i.OTW_HO_05_22 as OTW_Base_Premium
	from out.OTW_Territory_Rate as a 
	inner join out.OTW_Form_Factor as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_Prot_Constr_Factor as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.OTW_AOI_Factor as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_Multiple_Families_Factor as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_Loss_Settlement_Factor as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_Ordinance_or_Law_Factor as g 
	on a.CaseNumber = g.CaseNumber
	inner join out.OTW_HH_00_15_Factor as h 
	on a.CaseNumber = h.CaseNumber
	inner join out.OTW_Loss_Settlement_Factor_2 as i
	on a.CaseNumber = i.CaseNumber
	;
quit;
