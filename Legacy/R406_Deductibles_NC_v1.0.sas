** RULE 406. DEDUCTIBLES **;
* revised by Di according to BRD ITR-10566;

data deductible_fac;
	set fac.'406# Deductibles$'n;
	format f1 f2 12.2;
	if f1 < 1 then f1 = f1*100;
	if f1 = . then delete;
	cova_L = lag(f2); if cova_L > f2 or cova_L = . then cova_L = 0; rename f2 = cova_H; 
	Fire_n = Fire*1; Theft_n = Theft*1; waternw_n = 'Water Non-Weather'n*1; 
	othernw_n = 'All Other Non-Weather'n*1; Lightning_n = Lightning*1; waterw_n = 'Water Weather'n*1; 
	Wind_n = Wind*1; Hail_n = Hail*1; Hurricane_n = Hurricane*1;
	array peril (9) Fire_n Theft_n waternw_n othernw_n Lightning_n waterw_n Wind_n Hail_n Hurricane_n;
	array fac_l (9) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l;
	array fac_h (9) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h;
	do i = 1 to 9; 
		fac_l(i) = lag(peril(i));
		fac_h(i) = peril(i);
		if cova_L = 0 then fac_l(i) = fac_h(i);
	end;
	drop i;
	run;
	proc sql noprint;
		select max(cova_H)
		into: maxdedcova
		from deductible_fac
		;
	quit;
	%put &maxdedcova;


	proc sql;
	create table out.deductible_prep as
	select 
	a.casenumber,
	a.policynumber,
	a.coveragea,
	a.CoverageDeductible,
	a.WindHailDeductible,
	a.HurricaneDeductible,
	b.locationid,
	c.f2 as deductible_group
	from in.policy_info as a 
	inner join out.territory as b
	on a.casenumber=b.casenumber
	inner join fac.'301#A Location ID$'n as c
	on b.locationid=c.f1;
	quit;

data work.deductible; *revised based on NC ITR-10566 BRD;
length msg $40; /*indicator for whether correct w/h deductible amt is used or not*/
set out.deductible_prep;
AOPDeductible = input(CoverageDeductible,dollar8.);

if deductible_group=1 then do;

		if input(WindHailDeductible,8.) not in (5) then do; /*type I error: not using correct amt*/
			msg="INCORRECT AMT";
			WHDeductible=.;
			HurrDeductible=WHDeductible;
		end;
		
		else do;
			msg="Good";	
			WHDeductible=input(WindHailDeductible,8.);
			HurrDeductible=WHDeductible; /*The windstorm or hail deductible will apply in the event of a Hurricane*/
		end;

end;

else if deductible_group=2 then do; /*Minimum 2% WHDeductible*/
		if coveragea<=125000 then do;
			 if input(WindHailDeductible,8.) not in (2,5,2500,5000) then do; /*type I error: not using correct amt*/
			 	msg="INCORRECT AMT";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else if coveragea*input(WindHailDeductible,8.)<coveragea*0.02 then do; /*type II error: doesn't fulfill minimum*/
			 	msg="DO NOT FULFILL MINIMUM";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else do; /*Good: No issues*/
			 	msg="Good";
			 	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			 end;
	     end;
		 else if 125000<coveragea<=250000 then do;
		 	  if input(WindHailDeductible,8.) not in (2,5,5000) then do; /*type I error: not using correct amt*/
			  	msg="INCORRECT AMT";
				WHDeductible=.;
				HurrDeductible=WHDeductible;
			   end;

			   else if coveragea*input(WindHailDeductible,8.)<coveragea*0.02 then do; /*type II error: doesn't fulfill minimum*/
			   	msg="DO NOT FULFILL MINIMUM";
				WHDeductible=.;
				HurrDeductible=WHDeductible;
			   end;

			   else do; /*Good: No issues*/
			   	msg="Good";
				WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			   end;
		end;

		else do; /*>250000*/
			 if input(WindHailDeductible,8.) not in (2,5) then do; /*type I error: not using correct amt*/
				msg="INCORRECT AMT";
				WHDeductible=.;
				HurrDeductible=WHDeductible;
			 end;
			
			/*No need to check type II, both 2% and 5% will always satisfy minimum requirement*/
			else do;
			   	msg="Good";
			   	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			end;
		end;		
end;

else if deductible_group=3 then do; /*Minimum 1% WHDeductible*/
		if coveragea<=150000 then do;
			 if input(WindHailDeductible,8.) not in (1,2,5,1500,2000,2500,5000) then do; /*type I error: not using correct amt*/
			 	msg="INCORRECT AMT";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else if coveragea*input(WindHailDeductible,8.)<coveragea*0.01 then do; /*type II error: doesn't fulfill minimum*/
			 	msg="DO NOT FULFILL MINIMUM";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else do; /*Good: No issues*/
			 	msg="Good";
			 	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			 end;
	     end;

		else if 150000<coveragea<=200000 then do;
			 if input(WindHailDeductible,8.) not in (1,2,5,2000,2500,5000) then do; /*type I error: not using correct amt*/
			 	msg="INCORRECT AMT";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else if coveragea*input(WindHailDeductible,8.)<coveragea*0.01 then do; /*type II error: doesn't fulfill minimum*/
			 	msg="DO NOT FULFILL MINIMUM";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else do; /*Good: No issues*/
			 	msg="Good";
			 	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			 end;
	     end;

		else if 150000<coveragea<=200000 then do;
			 if input(WindHailDeductible,8.) not in (1,2,5,2000,2500,5000) then do; /*type I error: not using correct amt*/
			 	msg="INCORRECT AMT";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else if coveragea*input(WindHailDeductible,8.)<coveragea*0.01 then do; /*type II error: doesn't fulfill minimum*/
			 	msg="DO NOT FULFILL MINIMUM";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else do; /*Good: No issues*/
			 	msg="Good";
			 	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			 end;
	     end;

		else if 200000<coveragea<=250000 then do;
			 if input(WindHailDeductible,8.) not in (1,2,5,2500,5000) then do; /*type I error: not using correct amt*/
			 	msg="INCORRECT AMT";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else if coveragea*input(WindHailDeductible,8.)<coveragea*0.01 then do; /*type II error: doesn't fulfill minimum*/
			 	msg="DO NOT FULFILL MINIMUM";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else do; /*Good: No issues*/
			 	msg="Good";
			 	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			 end;
	     end;

		else if 250000<coveragea<=500000 then do;
			 if input(WindHailDeductible,8.) not in (1,2,5,5000) then do; /*type I error: not using correct amt*/
			 	msg="INCORRECT AMT";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else if coveragea*input(WindHailDeductible,8.)<coveragea*0.01 then do; /*type II error: doesn't fulfill minimum*/
			 	msg="DO NOT FULFILL MINIMUM";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else do; /*Good: No issues*/
			 	msg="Good";
			 	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			 end;
	     end;

		else do;
			 if input(WindHailDeductible,8.) not in (1,2,5) then do; /*type I error: not using correct amt*/
			 	msg="INCORRECT AMT";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else if coveragea*input(WindHailDeductible,8.)<coveragea*0.01 then do; /*type II error: doesn't fulfill minimum*/
			 	msg="DO NOT FULFILL MINIMUM";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else do; /*Good: No issues*/
			 	msg="Good";
			 	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			 end;
	     end;

end;	

else do; /*Group 4: Minimum $1000*/
		if 50000<coveragea<=100000 then do;
			 if input(WindHailDeductible,8.) not in (2,5,1000,1500,2000,2500,5000) then do; /*type I error: not using correct amt*/
			 	msg="INCORRECT AMT";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else if coveragea*input(WindHailDeductible,8.)<1000 then do; /*type II error: doesn't fulfill minimum*/
			 	msg="DO NOT FULFILL MINIMUM";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else do; /*Good: No issues*/
			 	msg="Good";
			 	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			 end;
	     end;

		 else if coveragea>=100000 then do;
			 if input(WindHailDeductible,8.) not in (1,2,5,1000,1500,2000,2500,5000) then do; /*type I error: not using correct amt*/
			 	msg="INCORRECT AMT";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else if coveragea*input(WindHailDeductible,8.)<1000 then do; /*type II error: doesn't fulfill minimum*/
			 	msg="DO NOT FULFILL MINIMUM";
			 	WHDeductible=.;
			 	HurrDeductible=WHDeductible;
			 end;
			 else do; /*Good: No issues*/
			 	msg="Good";
			 	WHDeductible=input(WindHailDeductible,8.);
			 	HurrDeductible=WHDeductible;
			 end;
	     end;
		 /*placeholder for coverage below 50000*/
end;
run;

	proc sql;
		create table deductible1 as
		select p.*, f1.cova_L, f1.cova_H
			, f1.fire_l, f1.fire_h
			, f1.theft_l, f1.theft_h
			, f1.waternw_l, f1.waternw_h
			, f1.othernw_l, f1.othernw_h
			, f1.lightning_l, f1.lightning_h
			, f1.waterw_l, f1.waterw_h
			, f2.wind_l, f2.wind_h
			, f2.hail_l, f2.hail_h

			, Case when &Hurricane = 1 then f3.hurr_l else 0 end as hurr_l
			, Case when &Hurricane = 1 then f3.hurr_h else 0 end as hurr_h	
	
		from deductible p 
			left join deductible_fac f1 on (p.Coveragea <= f1.cova_H or f1.cova_H = &maxdedcova) 
				and p.Coveragea > f1.cova_L and p.AOPDeductible = f1.F1
			left join deductible_fac f2 on (p.Coveragea <= f2.cova_H or f2.cova_H = &maxdedcova) 
				and p.Coveragea > f2.cova_L and p.WHDeductible = f2.F1
			left join deductible_fac f3 on (p.Coveragea <= f3.cova_H or f3.cova_H = &maxdedcova) 
				and p.Coveragea > f3.cova_L and p.HurrDeductible = f3.F1
		;
	quit;
	data out.ded_fac; set deductible1;
	array fac_l (9) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l;
	array fac_h (9) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h;
	array fac   (9) fire_ded theft_ded waternw_ded othernw_ded lightning_ded waterw_ded wind_ded hail_ded hurr_ded;
	do i = 1 to 9;
		if coveragea > &maxdedcova or coveragea = cova_H then fac(i) = fac_h(i);
		else fac(i) = round((fac_h(i) - fac_l(i))*(coveragea - cova_L)/(cova_H - cova_L) + fac_l(i),.0001);
	end;
	drop i;
	run;