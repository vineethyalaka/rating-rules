*OTW Rule 486 Homesite basic Package and Enhanced Package CW (Advantage);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0015_FLAG)
            in.Production_Info
            (CaseNumber PolicyNumber PolicyTermYears AdvFactor HH0015)
            Fac.OTWAdvantageFactor
            (Factor Flag)
			;
*Creates: 	out.OTW_R486_Advantage;

proc sql;
    create table out.OTW_R486_Advantage as
	select distinct e.CaseNumber, e.PolicyNumber, a.AdvInd, e.HO_0015_FLAG, pd.HH0015
		 , case when e.HO_0015_FLAG = '0' and pd.HH0015 = '0' then 1
			else a.Factor 
			end as Advantage label="OTW Advantage Factor"
	from in.Endorsement_Info as e
	inner join in.Production_Info as pd
	on e.CaseNumber = pd.CaseNumber
	inner join out.Policy_Year as y
	on e.CaseNumber = y.CaseNumber
    inner join Fac.OTWAdvantageFactor as a
	on pd.AdvFactor = a.Flag
	and (case when y.policyyear > 0 and HO_0015_FLAG = '1' then 'HO' else 'HH' end) = a.Endorsement
	;
quit;

