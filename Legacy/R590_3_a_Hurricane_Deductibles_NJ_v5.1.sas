*Rule 590.3 Hurricane Deductible NJ;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA WindHailDeductible)
            Fac.HurricaneDeductibleFactor
            (class AOI_Lower AOI_Upper Factor_Lower Factor_Upper Deductible)
			;
*Creates: 	out.Hurricane_Deductible;


proc sql;
    create table work.Hurricane_Deductible as
	select distinct p.CaseNumber, p.PolicyNumber, p.PropertyZip5, p.DistanceToShore, dm.HurricaneDeductibleGroup
		 , case when dm.HurricaneDeductibleGroup is null and WindHailDeductible not in ('0002','0003','0004') then 0 
		 	when WindHailDeductible = '0002' then 2
			when WindHailDeductible = '0003' then 3
			when WindHailDeductible = '0004' then 4
		 	when dm.HurricaneDeductibleGroup = 1 and p.DistanceToShore > 5280 then 2
			when dm.HurricaneDeductibleGroup = 1 and p.DistanceToShore <= 5280 then 3
			when dm.HurricaneDeductibleGroup = 2 then 4
			end as HurricaneDeductible
	from in.Policy_Info as p
	left join Fac.HurricaneDeductibleMinimum dm
	on p.PropertyZip5 = dm.Zip
	;

	create table out.Hurricane_Deductible as
	select distinct p.*, df.Factor as Hurricane_Deductible
	from work.Hurricane_Deductible p
	inner join Fac.HurricaneDeductibleFactor as df
	on p.HurricaneDeductible = df.Deductible
	;

quit;
