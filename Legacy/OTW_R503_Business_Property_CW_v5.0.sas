*OTW Rule 503 Business Property CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0412_flag HO_0412_Total_Limit)
            Fac.OTWPropertyBusinessFactor
            (Factor Amount)
			;
*Creates: 	out.OTW_R503_Business_Property;

proc sql;
	create table out.OTW_R503_Business_Property as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0412_flag = '0' then 0 else round((e.HO_0412_Total_Limit-2500)*pb.Factor/pb.Amount,1) end as Business_Property
	from in.Endorsement_Info as e
		, Fac.OTWPropertyBusinessFactor as pb
	;
quit;
