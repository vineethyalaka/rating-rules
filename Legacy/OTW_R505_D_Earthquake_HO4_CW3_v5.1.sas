/*uses Construction class*/

proc sql;
	create table out.OTW_R505D_Earthquake as
	select distinct e.CaseNumber, e.PolicyNumber
		 , p.CoverageC
		 , case when p.CoverageD > p.CoverageC*lou.BaseCovD then (p.CoverageD - p.CoverageC*lou.BaseCovD) else 0 end as IncrCovD
		 , eq.FactorB*p.CoverageC*1.01/1000 as EQ_CovC
		 , (case when e.ORD_LAW_FLAG = '1' then eq.FactorB else 0 end)*p.CoverageC*0.09/1000 as EQ_ORD_LAW
		 , eq.FactorC*calculated IncrCovD/1000 as EQ_IncrCovD
		 , eq.FactorC*e.HO_0448_ADDL_LIMIT_01/1000 as EQ_HO_0448
		 , case when e.HO_0454_FLAG = '1' then  round(calculated EQ_CovC
            + calculated EQ_ORD_LAW
            + calculated EQ_IncrCovD
            + calculated EQ_HO_0448,1)
			else 0
		   end as Earthquake
	from Fac.OTWPropertyLossOfUseFactor as lou
		, in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber
		inner join Source.ConstructionMapping_NC as cm 
		on lower(trim(left(p.PropertyConstructionClass))) = lower(trim(left(cm.PropertyConstructionClass)))
		inner join Fac.OTWPropertyEarthquakeFactor as eq
		on input(p.PropertyQuakeZone,3.) = input(eq.Territory,3.)
		and cm.Type2 = eq.Construction
	;
quit;
