*OTW Rule 521 Water Back Up of Sewers or Drains CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_04_95_IND)
            Fac.OTWPropertyWaterSumpFactor
            (Factor)
			;
*Creates: 	out.OTW_R521_Water_Sump;

proc sql;
	create table out.OTW_R521_Water_Sump as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_04_95_IND="Y" then round(ws.Factor,1) else 0 end as Water_Sump
	from Fac.OTWPropertyWaterSumpFactor as ws
		, in.Endorsement_Info as e
	;
quit;
