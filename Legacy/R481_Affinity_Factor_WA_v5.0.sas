*Rule 481 Affinity Factor CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PolicyAccountNumber AutoPolicyNumber)
            Fac.AffinityFactor
            (Factor AccNumMax AccNumMin AutoPolicyFlag)
			;
*Creates: 	out.R481_Affinity;

proc sql;
	create table out.R481_Affinity as
	select distinct a.CaseNumber, a.PolicyNumber, a.PolicyAccountNumber, b.Factor as Affinity label="Affinity"
	from in.policy_info as a
	inner join Fac.AffinityFactor as b
	on a.PolicyAccountNumber <= b.AccNumMax
	and a.PolicyAccountNumber >= b.AccNumMin
	and (a.AutoPolicyNumber is null and a.Mortgage is null and b.AutoPolicyFlag="N"
		or a.AutoPolicyNumber is not null and b.AutoPolicyFlag="Y"
        or a.Mortgage is not null and b.AutoPolicyFlag="Y")
	;
quit;


