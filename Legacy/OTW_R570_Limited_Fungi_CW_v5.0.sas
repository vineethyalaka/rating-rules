*OTW Rule 570 Limited Fungi Coverage CW;
*Uses:    	in.Production_Info
            (CaseNumber PolicyNumber Limited_Fungi)
            out.r301_experiencefactor
            (Factor)
			;
*Creates: 	out.OTW_R570_Limited_Fungi;

proc sql;
	create table out.OTW_R570_Limited_Fungi as
	select distinct p.CaseNumber, p.PolicyNumber
		 , round(p.Limited_Fungi/e.Experience,1) as Limited_Fungi
	from in.Production_Info as p 
	inner join out.r301_experiencefactor as e 
	on p.CaseNumber = e.CaseNumber
	;
quit;
