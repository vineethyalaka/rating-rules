*HO4 Tier Factor CW;
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class insurance_class_factor)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier Occupancy_Tier_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
           o.CaseNumber, o.PolicyNumber
          ,o.Occupancy_Tier, o.Occupancy_Tier_Factor
		  ,round(o.Occupancy_Tier_Factor,0.000001) as Tier_Factor
    from out.Tier3_Occupancy_Tier as o
	;
quit;
