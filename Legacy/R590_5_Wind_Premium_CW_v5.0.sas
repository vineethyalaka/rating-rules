*Rule 590.5 Wind Premium CW;
*Uses:    	out.Wind_Rating_Factors_Premium
            (CaseNumber PolicyNumber Wind_Rating_Factors_Premium)
            out.Wind_Property_Premium
            (CaseNumber PolicyNumber Wind_Property_Premium)
			;
*Creates: 	out.Wind_Premium;

proc sql;
    create table out.Wind_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , round(a.Wind_Rating_Factors_Premium + Wind_Property_Premium) as Wind_Premium
	from out.Wind_Rating_Factors_Premium as a 
	inner join out.Wind_Property_Premium as b 
	on a.CaseNumber = b.CaseNumber
	;
quit;
