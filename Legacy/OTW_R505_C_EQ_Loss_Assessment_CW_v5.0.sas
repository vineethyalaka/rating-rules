*OTW Rule 505.C Earthquake - Loss Assessment CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0436_flag HO_0436_LIMIT_01)
			in.Policy_Info
			(CaseNumber PolicyNumber PropertyConstructionClass PropertyQuakeZone)
            Fac.OTWPropertyEQLossAssessment
            (Factor Amount Zone)
			Fac.ConstructionMapping
			(Type2 PropertyConstructionClass)
			;
*Creates: 	out.OTW_R505C_EQ_Loss_Assessment;

proc sql;
	create table out.OTW_R505C_EQ_Loss_Assessment as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0436_flag = '0' then 0 else round(e.HO_0436_LIMIT_01*eql.Factor/eql.Amount,1) end as EQ_Loss_Assessment
	from in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber 
		and e.PolicyNumber = p.PolicyNumber
		inner join Fac.ConstructionMapping as cm 
		on lower(trim(left(p.PropertyConstructionClass))) = lower(trim(left(cm.PropertyConstructionClass)))
		inner join Fac.OTWPropertyEQLossAssessment as eql
		on input(p.PropertyQuakeZone,3.) = input(eql.Zone,3.)
		and cm.Type2 = eql.Construction
	;
quit;
