proc sql;
	create table work.OTW_R406_Deductibles as
	select distinct p.CaseNumber, p.PolicyNumber
		 , p.CoverageA
		 , p.CoverageDeductible as CoverageDeductible_adj
	from in.Policy_Info as p
	;
quit;

	proc sql;
		create table out.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			  , p.CoverageA, p.CoverageDeductible_adj, df.Class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
		      ,(df.Factor_Upper - df.Factor_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
			  ,round((calculated D1 * (p.CoverageA - df.AOI_Lower)),0.0001) as D2
			  ,round((df.Factor_Lower + calculated D2),0.001) as OTW_Deductible_Factor label="OTW Deductible"
		from work.OTW_R406_Deductibles as p
		inner join Fac.OTWDeductibleFactor as df
		on input(p.CoverageDeductible_adj,dollar8.) = df.Deductible
		and p.CoverageA <= df.AOI_Upper
		and p.CoverageA > df.AOI_Lower
		;
	quit;