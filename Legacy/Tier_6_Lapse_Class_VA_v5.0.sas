*Tier 6 Lapse Class VA;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance end_code_3_PI end_code_3_NH New_Home_Flag)
            in.Production_Info
            (CaseNumber PolicyNumber Lapse_Date_Used)
            Fac.TierLapseDays
            (LapseDays)
			;
*Creates: 	out.Tier6_LapseInsurance;

*Based off of CW v5.3;

proc sql;
    create table out.Tier6_LapseInsurance as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears
		  ,pd.Lapse_Date_Used
		  ,p.Lapse_of_Insurance
		  ,p.end_code_3_NH as ProposeEffDate format=date9.
		  ,p.end_code_3_PI as CurrentExpDate format=date9.
		  ,case when CurrentExpDate = . then ProposeEffDate - mdy(1,1,1900)
		   		else ProposeEffDate - CurrentExpDate 
		   end as period
		  ,l.LapseDays
		  ,case when pd.Lapse_Date_Used = 0 then 1
				when calculated period -1 <= l.LapseDays then 1 
				when p.PolicyTermYears >= 5 then 1
				else 2
		   end as Lapse_of_Insurance_Class
	from in.Policy_Info as p
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		, Fac.TierLapseDays as l
    ;
quit;
