*HO6 OTW Rule 507.Coverage A Increase Limits CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA)
            Fac.OTWPropertyCovACondoFactor
            (Factor)
			;
*Creates: 	out.OTW_R507_Incr_CovA;

proc sql;
	create table out.OTW_R507_Incr_CovA as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when p.CoverageA > 5000 then (p.CoverageA - 5000)
			else 0 end as IncrCovA
		 , round(calculated IncrCovA/1000*a.Factor,1) as Coverage_A_current
, case when ytable.casenumber=. then round(calculated IncrCovA/1000*a.Factor,1) else ytable.PremiumA end as Coverage_A
	from in.Policy_Info as p
	inner join Source.ConstructionMapping as cm
	on upper(trim(left(p.PropertyConstructionClass))) = upper(trim(left(cm.PropertyConstructionClass)))
	inner join Fac.OTWPropertyCovACondoFactor as a
	on p.ProtectionClass <= a.Protection_upper
	and p.ProtectionClass >= a.Protection_lower
	and cm.Type2 = a.Construction
	left join in.ytable_ho6_cova as ytable
	on p.CaseNumber=ytable.CaseNumber
	;
quit;
