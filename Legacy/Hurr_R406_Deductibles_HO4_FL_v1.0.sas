*HO4 Hurricane Deductible - FL;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber HurricaneDeductible)
            Fac.HurrDeductibleFactor
            (CoverageDeductible Factor)
			;
*Creates: 	out.Hurr_r406_Deductibles;

proc sql;
	create table out.Hurr_r406_Deductibles as
	select p.CaseNumber, p.PolicyNumber
		 , p.HurricaneDeductible as Deductible label="Deductible"
		 , df.Factor as Hurr_Deductible
	from in.Policy_Info as p
	inner join Fac.HurrDeductibleFactor as df
	on case when substr(p.HurricaneDeductible,1,1) = "$" then left(substr(p.HurricaneDeductible,2,5)) 
			else left(p.HurricaneDeductible) end 
	   = left(df.CoverageDeductible)
	;
quit;
