*Rule 601 Liability Residence Premises Basic and Increased Limits CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PropertyUnitsInBuilding CoverageE CoverageF)
            Fac.LiabilityBasicIncrCovEFactor
            (Factor Families CoverageE)
            Fac.LiabilityBasicIncrCovFFactor
            (Factor Families CoverageF)
			;
*Creates: 	out.Liability_Base_Rate;

proc sql;
    create table out.Liability_Base_Rate as
	select distinct p.CaseNumber, p.PolicyNumber
		 , ce.Factor as Coverage_E label="Coverage E"
		 , cf.Factor as Coverage_F label="Coverage F"
	from in.Policy_Info as p
	inner join Fac.LiabilityBasicIncrCovEFactor as ce
	on p.PropertyUnitsInBuilding = ce.Families
	and p.CoverageE = ce.CoverageE
	inner join Fac.LiabilityBasicIncrCovFFactor as cf
	on p.PropertyUnitsInBuilding = cf.Families
	and p.CoverageF = cf.CoverageF
	;
quit;
