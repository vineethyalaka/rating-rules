*OTW Rule 401 Superior Construction CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyConstructionClass)
            Fac.ConstructionMapping
            (PropertyConstructionClass Type2)
            Fac.OTWSuperiorConstructionFactor
            (Type Factor)
			;
*Creates: 	out.OTW_R401_Superior_Constr;

proc sql;
	create table out.OTW_R401_Superior_Constr as
	select distinct p.CaseNumber, p.PolicyNumber, scf.Factor as OTW_Superior_Construction label="OTW Superior Construction"
	from in.policy_info as p
	inner join Fac.ConstructionMapping as cm
	on  upper(trim(left(p.PropertyConstructionClass))) = upper(trim(left(cm.PropertyConstructionClass)))
	inner join Fac.OTWSuperiorConstructionFactor as scf 
	on cm.Type2 = scf.Type
	;
quit;
