*Wind Rule 512 Loss of Use - Increase Coverage D CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageD)
            in.Endorsement_Info
            (CaseNumber PolicyNumber EXWIND)
			out.wind_base_premium
			(Wind_Base_Premium)
            Fac.WindPropertyLossOfUseFactor
            (BaseCovD Factor)
			;
*Creates: 	out.Wind_R512_Loss_of_Use;

proc sql;
	create table out.Wind_R512_Loss_of_Use as
	select p.CaseNumber, p.PolicyNumber, wbp.Wind_Base_Premium
		 , case when p.Loss_of_Use_pay_period=24 then p.CoverageA*0.05 else 0 end as IncrCovD
		 , (1-e.EXWIND)*(case when p.Loss_of_Use_pay_period=24 then wbp.Wind_Base_Premium*0.02 else 0 end) as Coverage_D
	from in.Policy_Info as p
		inner join in.Endorsement_Info as e
		on p.CaseNumber = e.CaseNumber
		inner join out.wind_base_premium as wbp
		on p.CaseNumber = wbp.CaseNumber
	;
quit;



