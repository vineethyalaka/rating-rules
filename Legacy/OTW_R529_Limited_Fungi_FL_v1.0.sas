*OTW Rule 570 Limited Fungi Coverage - FL;
*Uses:    	in.Production_Info
			(CaseNumber PolicyNumber HO_04_31 Property_Amount)
			Fac.OTWPropertyFungiFactor
			(Coverage Factor)
			;
*Creates: 	out.OTW_R529_Limited_Fungi;


proc sql;
	create table out.OTW_R529_Limited_Fungi as
	select 
		  a.CaseNumber
		, a.PolicyNumber
		, a.HO_04_31
		, a.Property_Amount
		, b.Rate as Limited_Fungi
	from in.Production_Info as a
	inner join Fac.otwpropertyfungifactor as b
		on a.Property_Amount = b.Coverage
	;
quit;
