*HO4 Rule 590.3 Hurricane Premium CW;
*Uses:    	out.Hurricane_Base_Premium
			(CaseNumber PolicyNumber Hurricane_Base_Premium)
			out.Wind_R403_CovC_Replacement_Cost
			(CaseNumber PolicyNumber HO_04_90)
			;
*Creates: 	out.Hurricane_Premium;

proc sql;
    create table out.Hurricane_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , d.HO_04_90 as Wind_HO_04_90
		 , a.Hurricane_Base_Premium
		 , round(a.Hurricane_Base_Premium*d.HO_04_90,1)
			as Hurricane_Premium format=10.4
	from out.Hurricane_Base_Premium as a 
	inner join out.Wind_R403_CovC_Replacement_Cost as d 
	on a.CaseNumber = d.CaseNumber
	;
quit;
