/* includes HO0490 premium in the Protective Devices calculation */

proc sql;
    create table out.Rating_Factors_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.Base_Premium
		 , d.HO_04_90
		 , e.Burglar_Alarm
		 , e.Fire_Alarm
		 , e.Smoke_Detector
		 , e.Sprinklers
		 , f.Deductible_Factor
		 , round(a.Base_Premium*(d.HO_04_90-1),1) as HO0490_Prem_pre
		 , case when (calculated HO0490_Prem_pre < max.HO0490 and calculated HO0490_Prem_pre > 0) then max.HO0490 else calculated HO0490_Prem_pre end as HO0490_Prem
		 , round((a.Base_Premium + calculated HO0490_Prem)*(e.Burglar_Alarm+e.Fire_Alarm+e.Smoke_Detector+e.Sprinklers-4),1) as ProtectiveDevices_Prem_pre
		 , case when calculated ProtectiveDevices_Prem_pre < -max.ProtectiveDevices then -max.ProtectiveDevices else calculated ProtectiveDevices_Prem_pre end as ProtectiveDevices_Prem
		 , round(a.Base_Premium*(f.Deductible_Factor-1),1) as Deductible_Prem
		 , calculated HO0490_Prem+calculated ProtectiveDevices_Prem+Calculated Deductible_Prem as Rating_Factors_Premium
	from Fac.OTWMaxCreditsFactor as max, out.Base_Premium as a
	inner join out.OTW_R403_CovC_Replacement_Cost as d
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_R404_Protective_Devices as e
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_R406_Deductibles as f
	on a.CaseNumber = f.CaseNumber
	;
quit;
