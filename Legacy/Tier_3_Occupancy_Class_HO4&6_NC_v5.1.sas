
proc sql;
	create table out.Tier3_Occupancy_Tier as 
	select distinct o.CaseNumber, o.PolicyNumber, o.Residents_TotalCount as Residents
		, f.tier as Occupancy_Tier
	from in.Policy_info as o
		left join Fac.TierOccupancyClass as f
		on f.Occupants_upper>=o.Residents_TotalCount
		and f.Occupants_lower<=o.Residents_TotalCount
	;
quit;
