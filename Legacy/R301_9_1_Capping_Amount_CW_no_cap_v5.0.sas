*Rule 301.9.1 Capping Amount CW (no capping);
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber)
			;
*Creates: 	out.Capping_Amount;

proc sql;
    create table out.Capping_Amount as
	select distinct p.CaseNumber, p.PolicyNumber, 0 as capping_amount
	from in.policy_info as p
	;
quit;
