*OTW Rule 406 Deductibles CW (incooperate &minAOP);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible CoverageA)
            Fac.OTWDeductibleFactor
            (Deductible AOI_Lower AOI_Upper Factor_Lower Factor_Upper Class)
			;
*Creates: 	out.OTW_R406_Deductibles;

/*Update 5.4 10/2/2015 JT Changed rounding rules to match IT*/

%macro AOPDed;
	%if &AOPDedAdj = 1 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			 , p.CoverageA
			 , case when input(p.CoverageDeductible,dollar8.) < &minAOP then &minAOP
				else input(p.CoverageDeductible,dollar8.)
				end as CoverageDeductible_adj
		from in.Policy_Info as p
		;
	quit;
	%end;

	%if &AOPDedAdj = 0 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			 , p.CoverageA
			 , input(p.CoverageDeductible,dollar8.) as CoverageDeductible_adj
		from in.Policy_Info as p
		;
	quit;
	%end;

	proc sql;
		create table out.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			  , p.CoverageA, p.CoverageDeductible_adj, df.Class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
		      ,(df.Factor_Upper - df.Factor_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
			  ,round((calculated D1 * (p.CoverageA - df.AOI_Lower)),0.001) as D2
			  ,round((df.Factor_Lower + calculated D2),0.001) as OTW_Deductible_Factor label="OTW Deductible"
		from work.OTW_R406_Deductibles as p
		inner join Fac.OTWDeductibleFactor as df
		on CoverageDeductible_adj = df.Deductible
		and p.CoverageA <= df.AOI_Upper
		and p.CoverageA > df.AOI_Lower
		;
	quit;
%mend;
%AOPDed;
