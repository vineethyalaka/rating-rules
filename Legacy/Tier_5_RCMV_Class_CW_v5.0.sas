*Tier 5 RCMV Class CW;
*Uses:    	in.RCMV_info
            (CaseNumber PolicyNumber RCMV)
            Fac.TierRCMVClass
            (Class Ratio_upper Ratio_lower)
			;
*Creates: 	out.Tier5_RCMVClass;

proc sql;
    create table out.Tier5_RCMVClass as
	select distinct r.CaseNumber, r.PolicyNumber, t.Class as RCMV_Class label="RCMV Class"
	from in.RCMV_info as r
         inner join Fac.TierRCMVClass as t
         on r.RCMV <= t.Ratio_upper
		 and r.RCMV > t.Ratio_lower
	;
quit;
