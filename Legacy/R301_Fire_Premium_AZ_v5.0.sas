*Rule 301 Fire Premium AZ;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PropertyZip5)
            Fac.FirePremium
            (ZipCode FireFactor)
			;
*Creates: 	out.R301_FireFactor;

proc sql;
    create table out.R301_FireFactor as
	select distinct p.CaseNumber, p.PolicyNumber
	      ,case when f.FireFactor=. then 1 else f.FireFactor end as FireFactor
	from in.Policy_Info as p
	     left join Fac.FirePremium as f
		 on p.PropertyZip5 = f.ZipCode
	;
quit;
