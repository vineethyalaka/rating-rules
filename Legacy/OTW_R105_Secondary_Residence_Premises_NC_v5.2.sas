*OTW R105 Secondary Residence Premises NC;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyResidenceType)
            Fac.secondaryresidence 
            (Factor)
			;
*Creates: 	out.OTW_Secondary_Residence_Factor;

*out.OTW_Secondary_Residence_Factor;;
proc sql;
	create table out.OTW_Secondary_Residence_Factor as
	select distinct p.CaseNumber, p.PolicyNumber, p.PropertyResidenceType
		 , case when p.PropertyResidenceType in ("Primary") then 0 else f.factor end as OTW_Secondary_Residence_Factor
	from in.policy_info as p, Fac.secondaryresidence as f
	;
quit;
