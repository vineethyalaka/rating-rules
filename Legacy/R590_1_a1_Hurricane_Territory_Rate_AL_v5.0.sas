*Rule 590.1.a1 Territory Base Rate AL Hurricane;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryH)
            in.endorsement_info
            (CaseNumber PolicyNumber)
            Fac.HurrTerritoryFactor
            (Territory Territory_Rate)
			;
*Creates: 	out.Hurr_Territory_Rate;


proc sql;
    create table out.Hurr_Territory_Rate as
	select p.CaseNumber, p.PolicyNumber, t.Territory_Rate as Hurr_Territory_Rate label="Hurr Territory Base Rate"
	from in.policy_info as p
	inner join in.endorsement_info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.HurrTerritoryFactor as t
	on p.TerritoryH = t.Territory
	;
quit;
