*OTW Rule 480 Mature Owner/Resident CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate DOB1 DOB2 PolicyEffectiveDate Retired)
            Fac.OTWMatureOwnerFactor
            (Factor Years_lower Years_upper)
			;

*divides by 365 rather than 365.25
*Creates: 	out.OTW_R480_Mature_Owner;

*Notes: 
*Update to version 5.5: DOB# <= 1900.01.01 instead of DOB# < 1900.01.01
;

proc sql;
	create table work.OTW_R480_Mature_Owner as
	select p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.DOB1, p.DOB2
		 , case when p.DOB1 <= mdy(1,1,1900) then mdy(1,1,1900) else p.DOB1 end as DOB1a format=mmddyy10.
		 , case when p.DOB2 <= mdy(1,1,1900) then mdy(1,1,1900) else p.DOB2 end as DOB2a format=mmddyy10.
		 , case when calculated DOB1a > calculated DOB2a then calculated DOB2a else calculated DOB1a end as DOB format=mmddyy10.
		 , floor((case when p.PolicyTermYears=1 and &effst = 0 then p.PolicyIssueDate else p.PolicyEffectiveDate end - calculated DOB)/365) as Age
		 , Retired
	from in.Policy_Info as p
	;

	create table out.OTW_R480_Mature_Owner as
	select p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.DOB1, p.DOB2, p.DOB, p.Age, p.Retired
		 , case when p.DOB1 <= mdy(1,1,1900) and p.DOB2 <= mdy(1,1,1900) and p.Retired='Y' then 0.9 else 
       (case when p.DOB1 <= mdy(1,1,1900) and p.DOB2 <= mdy(1,1,1900) and p.Retired='N' then 1.0 else mof.Factor end) end as Mature_Owner label="Mature Owner"
	from work.OTW_R480_Mature_Owner as p
	inner join Fac.OTWMatureOwnerFactor as mof
	on p.Age >= mof.Years_lower
	and p.Age < mof.Years_upper
	and mof.Retired = p.Retired
	;
quit;
