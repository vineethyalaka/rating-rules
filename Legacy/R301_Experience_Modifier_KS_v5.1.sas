*Rule 301 Experience Modifier CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyAccountNumber)
            in.ExperienceFactor
            (Factor Account)
			;
*Creates: 	out.R301_ExperienceFactor;
*Does not consider Wells Fargo account code 1091 as Wells Fargo (ie will not receive experience modifier);

proc sql;
    create table work.R301_ExperienceModifier as
	select p.CaseNumber, p.PolicyNumber
		  ,p.PolicyAccountNumber
	      , case when m.Description="" then "Others" else m.Description end as Account
	from in.Policy_Info as p
	     left join source.Experiencemodifier as m
		 on p.PolicyAccountNumber <= m.Account_u
		 and p.PolicyAccountNumber >= m.Account_l
	;
quit;


proc sql;
    create table out.R301_ExperienceFactor as
	select m.CaseNumber
		  , m.PolicyNumber
		  , m.PolicyAccountNumber
	      , m.Account
		  , case when m.PolicyAccountNumber = 1091 then 1 else e.Factor end as Experience
	from work.R301_ExperienceModifier as m
		 inner join Fac.experiencefactor e
		 on m.Account = e.Description
	;
quit;
