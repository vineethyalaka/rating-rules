*OTW Rule 406 Deductibles CW (incooperate &minAOP);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible CoverageA)
            Fac.OTWDeductibleFactor
            (Deductible AOI_Lower AOI_Upper Factor_Lower Factor_Upper Class)
			;
*Creates: 	out.OTW_R406_Deductibles;

%macro AOPDed;
	%if &AOPDedAdj = 1 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			 , p.CoverageA
			 , case when input(p.CoverageDeductible,dollar8.) < &minAOP then &minAOP
				else input(p.CoverageDeductible,dollar8.)
				end as CoverageDeductible_adj
		from in.Policy_Info as p
		;
	quit;
	%end;

	%if &AOPDedAdj = 0 %then %do;
	proc sql;
		create table work.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			 , p.CoverageA
			 , input(p.CoverageDeductible,dollar8.) as CoverageDeductible_adj
		from in.Policy_Info as p
		;
	quit;
	%end;

	proc sql;
		create table out.OTW_R406_Deductibles as
		select p.CaseNumber, p.PolicyNumber
			  , p.CoverageA, p.CoverageDeductible_adj, df.Class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
		      ,(p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - 1 - df.AOI_Lower) as D1 /******/
			  ,(df.AOI_Upper - 1 - p.CoverageA)/(df.AOI_Upper - 1 - df.AOI_Lower) as D2 /******/
			  ,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind
			  ,round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind
			          + (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 - calculated Ind)
	                , 0.001) as OTW_Deductible_Factor label="OTW Deductible"
		from work.OTW_R406_Deductibles as p
		inner join Fac.OTWDeductibleFactor as df
		on CoverageDeductible_adj = df.Deductible
		and p.CoverageA <= df.AOI_Upper
		and p.CoverageA > df.AOI_Lower
		;
	quit;
%mend;
%AOPDed;
