*Rule 593 Tenants Relocation Expense MA;
*Uses:		in.DataMAinfo
			(CaseNumber PolicyNumber PropertyUnitsInBuilding Tenants_Relocation)
			Fac.otwpropertyrelocationfactor
			(TenRelocation MutiFamily Factor);

proc sql;
	create table work.Interaction_Class_Factor as
		select 
		  p.CaseNumber
		, p.PolicyNumber
		, p.CoverageC
		, p.UnitsinBuilding
		, case when p.UnitsinBuilding in (1,2) then 1 else 0 end as Unit_Red_Flag
		, a.age
		, case when age in (18,19,20,21,22,23,24,25) then 1 else 0 end as Age_Red_Flag
		, i.insurance_class
		, case when i.insurance_class in (16,17,18,19) then 1 else 0 end as Insurance_Red_Flag
		, t.Red_Flags as GEO_Red_Flag
		, calculated Unit_Red_Flag + calculated Age_Red_Flag + calculated Insurance_Red_Flag + t.Red_Flags
			as Red_Flags
	from in.Policy_info as p
	inner join fac.otwterritoryfactor as t
	on p.TerritoryA = t.Territory
	inner join out.otw_r480_mature_owner as a
	on p.casenumber = a.casenumber
	inner join out.tier1_insurancescore as i
	on p.casenumber = i.casenumber
	;
quit;
proc sql;
	create table out.Interaction_Class_Factor as
		select 
		  p.CaseNumber
		, p.PolicyNumber
		, p.CoverageC
		, p.UnitsinBuilding
		, p.Unit_Red_Flag
		, p.age
		, p.Age_Red_Flag
		, p.insurance_class
		, p.Insurance_Red_Flag
		, p.GEO_Red_Flag
		, p.Red_Flags
		, f.RedFlags
		, f.CoverageCMin
		, f.CoverageCMax
		, f.Factor as Red_Flag_Factor
	from work.Interaction_Class_Factor as p
	inner join fac.otwredflagfactor as f
	on p.Red_Flags = f.RedFlags
	and p.CoverageC >= CoverageCMin
	and p.CoverageC <= CoverageCMax
	;
quit;




