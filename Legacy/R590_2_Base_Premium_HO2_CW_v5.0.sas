*Rule 590.2 Wind Base Premium CW;
*Uses:    	out.Wind_Territory_Rate
            (CaseNumber PolicyNumber Wind_Territory_Rate)
            out.Wind_Form_Factor
            (CaseNumber PolicyNumber Wind_Policy_Form)
            out.Wind_Construction_Factor
            (CaseNumber PolicyNumber Wind_Construction)
            out.Wind_AOI_Factor
            (CaseNumber PolicyNumber Wind_AOI)
            out.Wind_Multiple_Families_Factor
            (CaseNumber PolicyNumber Wind_Multiple_Families_Factor)
            out.Wind_Prior_Claims
            (CaseNumber PolicyNumber Wind_Prior_Claims)
			;
*Creates: 	out.Wind_Base_Premium;

proc sql;
    create table out.Wind_Base_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Wind_Territory_Rate
		 , b.Wind_Policy_Form
		 , c.Wind_Construction
		 , d.Wind_AOI
		 , e.Wind_Multiple_Families_Factor
		 , h.Wind_Prior_Claims
		 , round(a.Wind_Territory_Rate*b.Wind_Policy_Form*c.Wind_Construction*d.Wind_AOI
		   *e.Wind_Multiple_Families_Factor*h.Wind_Prior_Claims,1) 
			as Wind_Base_Premium
	from out.Wind_Territory_Rate as a 
	inner join out.Wind_Form_Factor as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Wind_Construction_Factor as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.Wind_AOI_Factor as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.Wind_Multiple_Families_Factor as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.Wind_Prior_Claims as h 
	on a.CaseNumber = h.CaseNumber
	;
quit;
