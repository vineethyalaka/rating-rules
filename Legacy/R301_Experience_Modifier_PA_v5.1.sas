*Rule 301 Experience Modifier PA (Apply Exp Mod to HCT only); 
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyAccountNumber RatingVersion)
            in.ExperienceFactor
            (Factor Account)
			fac.underwritinginfo
			(RatingVersion UW_Company)
			;
*Creates: 	out.R301_ExperienceFactor;

proc sql;
    create table work.R301_ExperienceModifier1 as
	select distinct p.CaseNumber, p.PolicyNumber, p.RatingVersion
	      , case when m.Description="" then "Others" else m.Description end as Account
	from in.Policy_Info as p
	     left join source.Experiencemodifier as m
		 on p.PolicyAccountNumber <= m.Account_u
		 and p.PolicyAccountNumber >= m.Account_l
	;
quit;


proc sql;
	create table work.R301_ExperienceModifier2 as
	select distinct a.CaseNumber
		, a.PolicyNumber
		, a.RatingVersion
		, a.Account
		, b.UW_Company
	from work.R301_ExperienceModifier1 as a
		inner join Fac.underwritinginfo as b
		on a.RatingVersion = b.RatingVersion
	;
quit;


proc sql;
    create table out.R301_ExperienceFactor as
	select distinct c.CaseNumber
		  , c.PolicyNumber
		  , c.RatingVersion
	      , c.Account
		  , c.UW_Company
		  , d.Factor as Experience
	from work.R301_ExperienceModifier2 as c
		 inner join Fac.experiencefactor d
		 on c.Account = d.Description
		 and c.UW_Company = d.UW_Company
	;
quit;
