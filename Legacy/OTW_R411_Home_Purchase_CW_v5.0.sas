*OTW Rule 411 Home Purchase Rating;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears)
            in.Shopper_info
            (CaseNumber New_Home_Flag)
            Fac.OTWNewHomePurchaseFactor
            (Factor PolicyTerm)
			;
*Creates: 	out.OTW_R411_Home_Purchase;

proc sql;
	create table out.OTW_R411_Home_Purchase as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, s.New_Home_Flag
		 , case when s.New_Home_Flag = 1 then f.Factor else 1 end as Home_Purchase
	from in.Policy_Info as p
	inner join in.Shopper_info as s
	on p.casenumber = s.casenumber
	inner join Fac.OTWNewHomePurchaseFactor as f
	on f.PolicyTerm = case when p.PolicyTermYears <= 4 then p.PolicyTermYears else 4 end
	;
quit;
