*HO4 Rule 301.1 OTW Base Premium FL;
*Uses:    	out.OTW_Territory_Rate
            (CaseNumber PolicyNumber OTW_Territory_Rate)
            out.OTW_AOI_Factor
            (CaseNumber OTW_AOI)
            out.insurancetier_factor
            (CaseNumber Tier_Factor)
            out.interaction_class_factor
            (CaseNumber Red_Flag_Factor)
			;
*Creates: 	out.OTW_Base_Premium;

proc sql;
	create table out.otw_base_premium as
	select a.CaseNumber
	, a.PolicyNumber
	, round(a.OTW_Territory_Rate * b.OTW_AOI * c.Tier_Factor * d.Red_Flag_Factor,1) as OTW_Base_Premium
	from out.otw_territory_rate as a
	inner join out.otw_aoi_factor as b
	on a.CaseNumber = b.CaseNumber
	inner join out.insurancetier_factor as c
	on a.CaseNumber = c.CaseNumber
	inner join out.interaction_class_factor as d
	on a.CaseNumber = d.CaseNumber
	;
quit;
