*Rule 617 Liability Unsafe Swimming Pool MI;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF PoolPresent PoolHasFourFootFence Ladder)
            out.LiabilityUnsafePoolCovEFactor
            (Factor CoverageE)
            Fac.LiabilityUnsafePoolCovFFactor
            (Factor CoverageF)
			;
*Creates: 	out.Liability_Unsafe_Pool;

proc sql;
    create table out.Liability_Unsafe_Pool as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when p.PoolPresent="Y" and p.PoolHasFourFootFence="N" and p.Ladder="0" then 0 else spe.Factor end as Unsafe_Pool_E
		 , case when p.PoolPresent="Y" and p.PoolHasFourFootFence="N" and p.Ladder="0" then 0 else spf.Factor end as Unsafe_Pool_F
	from in.Policy_Info as p
	inner join Fac.LiabilityUnsafePoolCovEFactor as spe
	on p.CoverageE = spe.CoverageE
	inner join Fac.LiabilityUnsafePoolCovFFactor as spf
	on p.CoverageF = spf.CoverageF
	;
quit;
