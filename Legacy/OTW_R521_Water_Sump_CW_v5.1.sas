*OTW Rule 521 Water Back Up of Sewers or Drains CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_04_95_IND)
            in.Production_Info
            (CaseNumber PolicyNumber WaterSump)
            Fac.OTWPropertyWaterSumpFactor
            (Amount Factor)
			;
*Creates: 	out.OTW_R521_Water_Sump;

proc sql;
	create table out.OTW_R521_Water_Sump as
	select distinct e.CaseNumber, e.PolicyNumber, pd.WaterSump as WaterSump_Limit
		 , case when e.HO_04_95_IND="Y" then round(ws.Factor,1) else 0 end as Water_Sump
	from in.Endorsement_Info as e
		 inner join in.Production_Info as pd
		 on e.CaseNumber = pd.CaseNumber
		 inner join Fac.OTWPropertyWaterSumpFactor as ws
		 on pd.WaterSump = ws.Amount
	;
quit;
