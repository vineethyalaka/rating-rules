*Tier Factor CW (DRC);
*Uses:    	out.Tier_Classes
            (CaseNumber PolicyNumber Insurance_Class Shopper_Class Home_First_Class Prior_Claims_Class RCMV_Class Lapse_of_Insurance_Class)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class DRC_Factor)
            Fac.TierTable
            (Factor InsuranceScore Shopper Auto Prior RCMV Lapse)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
           c.CaseNumber, c.PolicyNumber
          ,c.Insurance_Class, c.Shopper_Class, c.Home_First_Class, c.Prior_Claims_Class
          ,c.Lapse_of_Insurance_Class
          ,t.Factor as IRM_Factor
		  ,d.DRC_Class, d.DRC_Factor
		  ,round(t.Factor * d.DRC_Factor,0.000001) as Tier_Factor
    from out.Tier_Classes as c
	inner join Fac.TierTable as t
	on  c.PolicyTermYears >= t.PolicyTermLower
	and c.PolicyTermYears <= t.PolicyTermUpper
	and	c.Insurance_Class          = t.InsuranceScore
	and c.Shopper_Class            = t.Shopper
	and c.Home_First_Class         = t.Auto
	and c.Prior_Claims_Class       = t.Prior
	and c.Lapse_of_Insurance_Class = t.Lapse
	inner join out.Tier7_DRC as d
	on c.CaseNumber = d.CaseNumber
	;
quit;
