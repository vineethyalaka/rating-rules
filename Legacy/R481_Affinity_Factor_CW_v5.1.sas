*Rule 481 Affinity Factor CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PolicyAccountNumber AutoPolicyNumber)
            Fac.AffinityFactor
            (Factor AccNumMax AccNumMin AutoPolicyFlag)
			;
*Creates: 	out.R481_Affinity;

proc sql;
	create table out.R481_Affinity as
	select distinct a.CaseNumber, a.PolicyNumber, a.PolicyAccountNumber, a.AutoPolicyNumber, b.Factor as Affinity label="Affinity"
	from in.policy_info as a
	inner join Fac.AffinityFactor as b
	on a.PolicyAccountNumber <= b.MaxMarketGroup
	and a.PolicyAccountNumber >= b.MinMarketGroup
	and (a.AutoPolicyNumber is null and b.LoanFlag="N"
		or a.AutoPolicyNumber is not null and b.LoanFlag="Y")
	;
quit;
