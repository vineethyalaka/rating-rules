*HO3 OTW Rule 406 Deductibles CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible)
            out.OTW_Tiered_Base_Premium
            (CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
            Fac.OTWDeductibleFactor
            (CoverageDeductible Factor Max_Credit)
			;
*Creates: 	out.OTW_R406_Deductibles;

proc sql;
	create table out.OTW_R406_Deductibles as
	select distinct p.CaseNumber, p.PolicyNumber
		 , df.Factor as Deductible label="Deductible"
		 , df.Max_Credit
		 , case when (1 - df.Factor)*t.OTW_Tiered_Base_Premium > df.Max_Credit then (1 - df.Max_Credit/t.OTW_Tiered_Base_Premium)
			else df.Factor end as OTW_Ded_Factor_current
		 , round(case when (1 - df.Factor)*bp.OTW_Base_Premium > df.Max_Credit then (1 - df.Max_Credit/bp.OTW_Base_Premium) 
			else df.Factor end,0.01) as OTW_Deductible_Factor	     
         ,round(
           case when (1 - df.Factor) * bp.OTW_Base_Premium > df.Max_Credit then 
	       (case when t.Tier_Factor > 1 then df.Max_Credit * t.Tier_Factor - df.Max_Credit else 0 end) else
	       (case when (1 - df.Factor) * t.OTW_Tiered_Base_Premium > df.Max_Credit then (1 - df.Factor) * t.OTW_Tiered_Base_Premium - df.Max_Credit else 0 end) end
           ,0.1) as adjdedamt
from in.Policy_Info as p
	inner join out.OTW_Tiered_Base_Premium as t
	on p.CaseNumber = t.CaseNumber
	inner join Fac.OTWDeductibleFactor as df
	on p.CoverageDeductible = df.CoverageDeductible
	inner join out.OTW_Base_Premium as bp
	on p.CaseNumber = bp.CaseNumber
	;
quit;
