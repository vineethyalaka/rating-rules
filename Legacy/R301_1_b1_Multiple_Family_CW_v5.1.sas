*Rule 301.1.b1 Multiple Family CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PropertyUnitsInBuilding)
            Fac.OTWMultipleFamilyFactor
            (Factor NumFamilies)
			;
*Creates: 	out.OTW_Multiple_Families_Factor;

proc sql;
    create table out.OTW_Multiple_Families_Factor as
	select  distinct p.CaseNumber, p.PolicyNumber
		 , mff.Cov_B as Cov_B, mff.Cov_C as Cov_C
		 , mff.Factor as OTW_Multiple_Families_Factor label="OTW Multiple Families Factor"
	from in.policy_info as p
    inner join Fac.OTWMultipleFamilyFactor as mff
    on p.PropertyUnitsInBuilding = mff.NumFamilies
	;
quit;
