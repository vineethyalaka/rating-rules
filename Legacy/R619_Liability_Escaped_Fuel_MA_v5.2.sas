*Rule 619 Liability Escaped Fuel;
*Uses:    	in.DataMAInfo
            (CaseNumber PolicyNumber Remediation HO_03_Escaped_Liquid_Fuel)
			in.Policy_Info
			(CaseNumber PolicyNumber Oil_Tank_Type)
			Fac.liabilityescapedliquidfuelfactor
			(Remediation HH_05_47 Oil_Tank_Type Factor)
*Creates: 	out.Liability_Escaped_Fuel;

proc sql;
    create table out.Liability_Escaped_Fuel as
	Select distinct p.CaseNumber, p.PolicyNumber, p.Remediation, p.HO_03_Escaped_Liquid_Fuel, i.Oil_Tank_Type, q.Factor as Fuel
	from in.DataMAInfo as p

	inner join in.Policy_Info as i
		on p.CaseNumber = i.CaseNumber
	inner join Fac.liabilityescapedliquidfuelfactor as q
		on p.Remediation = q.Remediation
		and p.HO_03_Escaped_Liquid_Fuel = q.HH_05_47
		and q.Oil_Tank_Type = i.Oil_Tank_Type

	;
quit;
