*OTW Rule 406 Deductibles CW (remove $250 AOP deductible option);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible CoverageA)
            Fac.OTWDeductibleFactor
            (Deductible AOI_Lower AOI_Upper Factor_Lower Factor_Upper Class)
			;
*Creates: 	out.OTW_R406_Deductibles;

proc sql;
	create table work.OTW_R406_Deductibles as
	select distinct p.CaseNumber, p.PolicyNumber
		 , p.CoverageA
		 , case when substr(left(p.CoverageDeductible),1,5) = "$250" then "$500"
			else p.CoverageDeductible
			end as CoverageDeductible_adj
	from in.Policy_Info as p
	;
quit;
proc sql;
	create table out.OTW_R406_Deductibles as
	select p.CaseNumber, p.PolicyNumber
		  , p.CoverageA, p.CoverageDeductible_adj, df.Class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
	      ,(p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
		  ,(df.AOI_Upper - p.CoverageA)/(df.AOI_Upper - df.AOI_Lower) as D2
		  ,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind
		  ,round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind
		          + (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 - calculated Ind)
                , 0.001) as OTW_Deductible_Factor label="OTW Deductible"
	from work.OTW_R406_Deductibles as p
	inner join Fac.OTWDeductibleFactor as df
	on input(p.CoverageDeductible_adj,dollar8.) = df.Deductible
	and p.CoverageA <= df.AOI_Upper
	and p.CoverageA > df.AOI_Lower
	;
quit;
