*Tier Factor NC;
*Uses:    	out.Tier_Classes
            (CaseNumber PolicyNumber Insurance_Class Units_Tier Occupancy_Tier Prior_Claims_Class)
            Fac.TierTable
            (Factor InsuranceScore Residents Units Prior)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
           c.CaseNumber, c.PolicyNumber
          ,c.Insurance_Class, c.Units_Tier, c.Occupancy_Tier, c.Prior_Claims_Class
          ,t.Factor as Tier_Factor
    from out.Tier_Classes as c
	inner join Fac.TierTable as t
	on  c.Insurance_Class    = t.InsuranceScore
	and c.Units_Tier         = t.Units
	and c.Occupancy_Tier     = t.Residents
	and c.Prior_Claims_Class = t.Prior
	;
quit;
