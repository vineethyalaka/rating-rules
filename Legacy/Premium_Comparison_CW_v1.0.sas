/*01/09/2019	WZ	Legacy premium_comparison to append in prod_report*/

proc sql;
create table out3.premium_comparison as 
select p1.casenumber,
       p1.policynumber,
       p1.premiuminforce as IT_Premium,
       s.final_premium as SAS_Premium,
	  c.cap as DB_Cap_Amount,
	 (p1.premiuminforce-s.final_premium-c.cap)as IT_SAS_Premium_diff
from in3.policy_info p1
inner join out3.Final_Premium s
        on p1.casenumber = s.casenumber
inner join	in3.capping_info c
		on s.casenumber = c.casenumber
order by abs(calculated IT_SAS_Premium_diff) desc
;
quit;


proc sql;
create table out4.premium_comparison as 
select p1.casenumber,
       p1.policynumber,
       p1.premiuminforce as IT_Premium,
       s.final_premium as SAS_Premium,
	  c.cap as DB_Cap_Amount,
	 (p1.premiuminforce-s.final_premium-c.cap)as IT_SAS_Premium_diff
from in4.policy_info p1
inner join out4.Final_Premium s
        on p1.casenumber = s.casenumber
inner join	in4.capping_info c
		on s.casenumber = c.casenumber
order by abs(calculated IT_SAS_Premium_diff) desc
;
quit;

proc sql;
create table out6.premium_comparison as 
select p1.casenumber,
       p1.policynumber,
       p1.premiuminforce as IT_Premium,
       s.final_premium as SAS_Premium,
	  c.cap as DB_Cap_Amount,
	 (p1.premiuminforce-s.final_premium-c.cap)as IT_SAS_Premium_diff
from in6.policy_info p1
inner join out6.Final_Premium s
        on p1.casenumber = s.casenumber
inner join	in6.capping_info c
		on s.casenumber = c.casenumber
order by abs(calculated IT_SAS_Premium_diff) desc
;
quit;
