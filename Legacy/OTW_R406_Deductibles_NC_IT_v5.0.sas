/* uses IT deductible factor table, $1000 all-perils deductible for Coverage A > 200k has factor of 1.00 rather than 0.89 */
/* now uses same correct table, but handles "missing" WH deductible, i.e., All-Perils deductible */

proc sql;
	create table out.OTW_R406_Deductibles as
	select distinct p.CaseNumber, p.PolicyNumber
		  , p.CoverageA, p.CoverageDeductible, p.WindHailDeductible, df.AOI_Lower, df.AOI_Upper
/*		  , case when p.windhaildeductible=p.CoverageDeductible then 1 else df.Factor end as Deductible_Factor */
		  , df.Factor as Deductible_Factor
	from in.Policy_Info as p
	inner join Fac.OTWDeductibleFactorIT as df
	on input(p.CoverageDeductible,dollar8.) = df.Deductible
	and 
		(input(p.windhaildeductible,best24.) = df.WindHailDeductible
		or
		p.windhaildeductible="" and input(p.CoverageDeductible,dollar8.) = df.WindHailDeductible)
and p.CoverageA <= df.AOI_Upper
	and p.CoverageA >= df.AOI_Lower
	;
quit;
