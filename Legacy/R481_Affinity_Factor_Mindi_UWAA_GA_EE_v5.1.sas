proc sql;
	create table employee as
	select a.casenumber, a.policynumber, a.policyaccountnumber
		, b.Employee_associate_number, c.HO_2492_0913_SERVANT_IND as HD_078_flag
		, case when a.policyaccountnumber in (14200)
			then 1
			else 0		
		end as partner_ee
		, case when b.Employee_associate_number = '' then 0
			else 1
			end as employee
	from in.policy_info a
		inner join in.production_info b on a.casenumber = b.casenumber
		inner join in.endorsement_info c on a.casenumber = c.casenumber
;
quit;