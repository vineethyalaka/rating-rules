*HO6 Tier Factor CW (occupancy class not used);
*Uses:    	out.Tier2_ClaimsCredit_Factor
            (CaseNumber PolicyNumber ClaimsCredit_Tier ClaimsCredit_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
            c.CaseNumber, c.PolicyNumber
          , c.ClaimsCredit_Tier, c.ClaimsCredit_Factor
		  , round(c.ClaimsCredit_Factor,0.000001) as Tier_Factor
    from out.Tier2_ClaimsCredit_Factor as c
	;
quit;
