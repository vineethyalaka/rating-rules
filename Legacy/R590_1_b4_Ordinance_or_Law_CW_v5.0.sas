*Rule 590.1.b4 Ordinance or Law CW (HO 04 77);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber ORD_LAW_FLAG)
            Fac.WindOrdinanceOrLawFactor
            (Factor)
			;
*Creates: 	out.Wind_Ordinance_or_Law_Factor;

proc sql;
    create table out.Wind_Ordinance_or_Law_Factor as
	select distinct e.CaseNumber, e.PolicyNumber, case when e.ORD_LAW_FLAG = '1' then olf.Factor else 1 end as Wind_Ordinance_or_Law
	from in.Endorsement_Info as e, Fac.WindOrdinanceOrLawFactor as olf
	;
quit;
