*Rule 590.1.a1 Hurricane Territory Base Rate NJ (vary by UW Company);
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryA UNDERWRITING_CO)
            in.endorsement_info
            (CaseNumber PolicyNumber EXWind)
            Fac.HurricaneTerritoryFactor
            (Territory Territory_Rate_07 Territory_Rate_21)
			;
*Creates: 	out.Hurricane_Territory_Rate;

proc sql;
    create table out.Hurricane_Territory_Rate as
	select p.CaseNumber, p.PolicyNumber
	, case when p.UNDERWRITING_CO = 7 then (1-e.EXWind)*t.Territory_Rate_07
			when p.UNDERWRITING_CO = 21 then (1-e.EXWind)*t.Territory_Rate_21
		end as Hurricane_Territory_Rate label="Hurricane Territory Base Rate"
	from in.policy_info as p
	inner join in.endorsement_info as e
	on e.CaseNumber = p.CaseNumber
	inner join Fac.HurricaneTerritoryFactor as t
	on p.TerritoryA = t.Territory
	;
quit;
