*Rule 301.1.a1 Territory Base Rate CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryA)
            Fac.OTWTerritoryFactor
            (Territory Territory_Rate)
			;
*Creates: 	out.OTW_Territory_Rate;

proc sql;
    create table out.Wind_Exclusion_Factor as
	select  distinct p.CaseNumber, p.PolicyNumber, case when EXWIND = 1 then w.Territory_Rate else 0 end as Wind_Exclusion
	from in.policy_info as p
	inner join Fac.OTWxWindFactor as w
	on p.TerritoryA = w.Territory
	inner join in.endorsement_info as e
	on p.CaseNumber = e.CaseNumber
	;
quit;
