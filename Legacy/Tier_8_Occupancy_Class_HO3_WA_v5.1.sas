*HO4/6 Tier 3 Occupancy Class CW;
*Uses:    	in.policy_info
			(CaseNumber PolicyNumber Residents_TotalCount)
			Fac.TierOccupancyFactor
			(Occupants_upper Occupants_lower Tier Factor)
			;
*Creates: 	out.Tier3_Occupancy_Tier;

proc sql;
	create table out.Tier8_Occupancy_Tier as 
	select distinct p.CaseNumber, p.PolicyNumber, p.Residents_TotalCount
		, f.tier as Occupancy_Tier, f.factor as Occupancy_Tier_Factor
	from in.policy_info as p
		left join Fac.TierOccupancyFactor as f
		on f.Occupants_upper>=p.Residents_TotalCount
		and f.Occupants_lower<=p.Residents_TotalCount
	;
quit;
