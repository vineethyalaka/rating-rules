*OTW Rule 515.A/B Personal Property (Increased/Reduction in Coverage C) NC;
*2019/02/06 TM, delete check HO_0490_Flag as coverage C limit in Rule403 changed from 70% to 40% for HO3. Use this version later than ITR12978
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageC PropertyUnitsInBuilding)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.OTWPropertyCovCFactor
            (Factor1 Factor2 AdvInd FamilyUnits)
			;
*Creates: 	out.OTW_R515_A_Incr_CovC;

proc sql;
	create table out.OTW_R515_A_Incr_CovC as
	select p.CaseNumber, p.PolicyNumber
		 , (p.CoverageC - p.CoverageA*d.BaseCovC) as IncrCovC
		 , case when calculated IncrCovC > 0 then round(calculated IncrCovC/1000*cc.Factor1,1)
		 		else round(calculated IncrCovC/1000*cc.Factor2,1) end as Coverage_C
	from in.Policy_Info as p
		inner join in.Endorsement_Info as e 
		on p.CaseNumber = e.CaseNumber
		inner join Fac.OTWPropertyCovCFactor as cc
		on  e.HO_0015_Flag = cc.HO0015
		inner join out.Base_Premium as d
		on p.CaseNumber = d.CaseNumber
	;
quit;
