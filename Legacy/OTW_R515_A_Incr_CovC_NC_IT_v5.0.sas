/* uses IT table with same factor whether the HO0015 endorsementis present or not */

proc sql;
	create table out.OTW_R515_A_Incr_CovC as
	select distinct p.CaseNumber, p.PolicyNumber
		 , (p.CoverageC - p.CoverageA*d.BaseCovC) as IncrCovC
		 , case when calculated IncrCovC > 0 then round(calculated IncrCovC/1000*cc.Factor1,1)
		 		when calculated IncrCovC <= 0 and e.HO_0490_Flag="0"
											 then round(calculated IncrCovC/1000*cc.Factor2,1)
		 		else 0 end as Coverage_C
	from in.Policy_Info as p
		inner join in.Endorsement_Info as e 
		on p.CaseNumber = e.CaseNumber
		inner join Fac.OTWPropertyCovCFactorIT as cc
		on  e.HO_0015_Flag = cc.HO0015
		inner join out.Base_Premium as d
		on p.CaseNumber = d.CaseNumber
	;
quit;
