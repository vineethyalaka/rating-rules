*Wind Rule 515.A/B Personal Property (Increased/Reduction in Coverage C) CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageC PropertyUnitsInBuilding)
            in.Endorsement_Info
            (CaseNumber PolicyNumber EXWIND)
            out.Wind_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.WindPropertyCovCFactor
            (Factor1 Factor2 AdvInd FamilyUnits)
			;
*Creates: 	out.Wind_R515_A_Incr_CovC;

proc sql;
	create table out.Wind_R515_A_Incr_CovC as
	select p.CaseNumber, p.PolicyNumber
		 , case when &Broll_CovC = 1 and p.policyaccountnumber in (14300,14310) and p.policytermyears = 1 and p.CoverageC < p.CoverageA*cc.BaseCovC then 0 
				else (p.CoverageC - p.CoverageA*cc.BaseCovC) end as IncrCovC
		 , case when calculated IncrCovC > 0 and cc.AdvInd="N"
											 then (1-e.EXWIND)*round(calculated IncrCovC/1000*cc.Factor1,1)
		 		when calculated IncrCovC <= 0 and cc.AdvInd="N"
											 then (1-e.EXWIND)*round(calculated IncrCovC/1000*cc.Factor2,1)
		 		else 0 end as Coverage_C
	from in.Policy_Info as p
		inner join in.Endorsement_Info as e
		on p.CaseNumber = e.CaseNumber
		inner join out.Wind_R486_Advantage as b 
		on p.CaseNumber = b.CaseNumber
		inner join Fac.WindPropertyCovCFactor as cc
		on  b.AdvInd = cc.AdvInd
		and p.PropertyUnitsInBuilding = cc.FamilyUnits
	;
quit;
