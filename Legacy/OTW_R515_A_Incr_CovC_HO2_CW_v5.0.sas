*OTW Rule 515.A/B Personal Property (Increased/Reduction in Coverage C) CW (HO2);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageC PropertyUnitsInBuilding)
            Fac.OTWPropertyCovCFactor
            (Factor1 Factor2 FamilyUnits)
			;
*Creates: 	out.OTW_R515_A_Incr_CovC;

proc sql;
	create table out.OTW_R515_A_Incr_CovC as
	select distinct p.CaseNumber, p.PolicyNumber
		 , (p.CoverageC - p.CoverageA*cc.BaseCovC) as IncrCovC
		 , case when calculated IncrCovC > 0 then round(calculated IncrCovC/1000*cc.Factor1,1)
		 		when calculated IncrCovC <= 0 then round(calculated IncrCovC/1000*cc.Factor2,1)
		 		else 0 end as Coverage_C
	from in.Policy_Info as p
		inner join Fac.OTWPropertyCovCFactor as cc
		on  p.PropertyUnitsInBuilding = cc.FamilyUnits
	;
quit;
