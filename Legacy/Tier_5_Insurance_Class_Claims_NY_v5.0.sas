*Tier Insurance Class / Claims Free NY ;
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class)
            out.Tier4_PriorClaims
            (CaseNumber PolicyNumber Prior_Claims_Class Claim_Free_Years)
			;
*Creates: 	out.Tier1_InsuranceScoreFinal;

proc sql;

create table work.Tier1_InsuranceScoreFinal as
	select a.CaseNumber, a.PolicyNumber, a.Insurance_Class
	, case when b.Claim_Free_Years = . then 0 else b.Claim_Free_Years end as Claim_Free_Years
	, case when b.Claim_Free_Years >=6 and Prior_Claims_Class = 1 and a.Insurance_Class > f.MaxInsClass 
	and y.Group <> 'NBbeforeTRP'
		then f.MaxInsClass
		else a.Insurance_Class
			end as InsuranceClassFinal
	from out.tier1_InsuranceScore as a
	inner join out.Tier4_PriorClaims as b
		on a.casenumber = b.casenumber
	inner join out.policy_year as y
		on a.casenumber = y.casenumber
	inner join fac.tierinsuranceclass_claims as f
		on b.Claim_Free_Years >= ClaimFreeYears_Min
		and b.Claim_Free_Years < ClaimFreeYears_Max
;
quit;

proc sql;
create table out.Tier1_InsuranceScoreFinal as
select a.CaseNumber, a.PolicyNumber, a.InsuranceClassFinal as insurance_class
from work.Tier1_InsuranceScoreFinal as a;
quit;