*Rule 603 Liability Residence Employees CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber DomesticEmployeesCount CoverageE CoverageF)
            Fac.LiabilityEmployeesCovEFactor
            (Factor CovE)
            Fac.LiabilityEmployeesCovFFactor
            (Factor CovF)
			;
*Creates: 	out.Liability_Residence_Employees;

proc sql;
    create table out.Liability_Residence_Employees as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when p.DomesticEmployeesCount > 2 then dee.Factor*(p.DomesticEmployeesCount-2) else 0 end as Domestic_Employees_E
		 , case when p.DomesticEmployeesCount > 2 then def.Factor*(p.DomesticEmployeesCount-2) else 0 end as Domestic_Employees_F
	from in.Policy_Info as p
	inner join Fac.LiabilityEmployeesCovEFactor as dee
	on p.CoverageE = dee.CovE
	inner join Fac.LiabilityEmployeesCovFFactor as def
	on p.CoverageF = def.CovF
	;
quit;
