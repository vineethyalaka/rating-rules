*OTW Rule 570 Limited Fungi Coverage CW2 HO6;
*Uses:    	in.ProductionInfo
			(CaseNumber PolicyNumber HO_04_31 HO_04_33 Property_Amount Liability_Amount)
			Fac.OTWPropertyFungiFactor
			(Factor)
			;
*Creates: 	out.OTW_R570_Limited_Fungi;

proc sql;
	create table out.OTW_R570_Limited_Fungi as
	select 
		  a.CaseNumber
		, a.PolicyNumber
		, b.Factor as Limited_Fungi
	from in.Production_Info as a
	inner join Fac.OTWPropertyFungiFactor as b
		on a.HO_04_31 = b.HH_05_37
		and	a.HO_04_33 = b.HH_05_39
		and	a.Property_Amount = b.Property
		and	a.Liability_Amount = b.Liability
	;
quit;
