*Tier 4 Prior Claims Class CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate)
            in.loss_info
            (PolicyNumber LossDate LossCode LossAmount)
            Fac.ActofGod
            (ClaimTypeGod ActOfGodCode)
            Fac.TierPriorClaimClass
            (Class NumberofNAGMax NumberofNAGMin NumberofAOGMax NumberofAOGMin AgeType)
			;
*Creates: 	out.Tier4_PriorClaims;

*temp.PriorClaims_a;;
proc sql;
	create table temp.PriorClaims_a as
	select distinct l.casenumber, l.policynumber, p.PolicyTermYears, p.PolicyEffectiveDate, l.lossdate, l.LossCode, l.LossAmount
			, case when t.ClaimTypeGod is null then 0
				else t.ClaimTypeGod
			  end as ClaimAOG
			, case when t.ClaimTypeGod is null then 1
				else 0
			  end as ClaimNAG
			, case when p.PolicyTermYears=1 then (p.PolicyIssueDate - l.LossDate)/365
				else (p.PolicyEffectiveDate - l.LossDate - 60)/365
			  end as LossAge
			, case when calculated lossage>3 then 1	/*within 3 years*/
				else 2
			  end as AgeType
	from in.policy_info as p
		inner join in.loss_info as l
		on p.PolicyNumber=l.policynumber
		and p.casenumber=l.casenumber
		left join Fac.ActofGod as t
		on l.LossCode=t.ActOfGodCode
	where l.LossAmount>0 
		and (p.PolicyTermYears=1 and (p.PolicyIssueDate - l.LossDate)/365 <5	/*within 5 years*/
		or  p.PolicyTermYears^=1 and (p.PolicyEffectiveDate - l.LossDate - 60)/365 <5)	/*within 5 years*/
	;
quit;

*temp.PriorClaims_b;;
proc sql;
	create table temp.PriorClaims_b as
	select distinct a.casenumber, a.policynumber, sum(a.claimAOG) as SumOfClaimAOG, sum(a.ClaimNAG) as SumOfClaimNAG, a.AgeType
	from temp.PriorClaims_a as a
	group by a.PolicyNumber, a.casenumber, a.AgeType
	;
quit;

*temp.PriorClaims_c;;
proc sql;
	create table temp.PriorClaims_c as
	select distinct b.casenumber, b.PolicyNumber, b.AgeType, c.Class
	from temp.PriorClaims_b as b
		inner join Fac.TierPriorClaimClass as c
		on b.SumOfClaimNAG <= c.NumberofNAGMax
		and b.SumOfClaimNAG >= c.NumberofNAGMin
		and b.SumOfClaimAOG <= c.NumberofAOGMax
		and b.SumOfClaimAOG >= c.NumberofAOGMin
		and b.AgeType = c.AgeType
	;
quit;

*temp.PriorClaims_d;;
proc sql;
    create table temp.PriorClaims_d as
	select distinct c.casenumber, c.PolicyNumber, max(c.class) as MaxOfClass
	from temp.PriorClaims_c as c
	group by c.PolicyNumber, c.casenumber
	;
quit;

*temp.PriorClaims_e;;
proc sql;
    create table temp.PriorClaims_e as
	select distinct a.CaseNumber, a.PolicyNumber, case when d.MaxOfClass is Null then 1 else d.MaxOfClass end as Prior_Claims_Class
	from in.policy_info as a
	left join temp.PriorClaims_d as d
	on a.PolicyNumber = d.PolicyNumber
	and a.CaseNumber = d.CaseNumber
    ;
quit;

*out.Tier4_PriorClaims;;
proc sql;
    create table out.Tier4_PriorClaims as
	select distinct a.CaseNumber, a.PolicyNumber , a.Prior_Claims_Class
		 , factor as Prior_Claims_Factor
	from temp.PriorClaims_e as a
	left join Fac.TierPriorClaimClassFactor as f
	on a.Prior_Claims_Class = f.Class
    ;
quit;
