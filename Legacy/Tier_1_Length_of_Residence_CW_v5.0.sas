*Tier 1 Length of Residence Class CA and MA;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber)
            in.Production_Info
            (CaseNumber LengthOfResidenceInd LengthOfResidence)
            Fac.tierlengthofresidencefactor
            (Flag)
			;
*Creates: 	out.Tier1_LengthOfResidence;

proc sql;
    create table out.Tier1_LengthOfResidence as
	select distinct p.CaseNumber
	, p.PolicyNumber
	, pd.LengthOfResidenceInd
	, pd.LengthOfResidence
	, r.Factor as LengthOfResidenceFactor
	from in.Policy_Info as p
		inner join in.Production_Info as pd
		on p.CaseNumber = pd.CaseNumber
		inner join Fac.tierlengthofresidencefactor r
		on pd.LengthOfResidenceInd = r.Flag
		and (case when pd.LengthOfResidence > 11 then 11 else pd.LengthOfResidence end) = r.Years
    ;
quit;
