*Wind Rule 471 Roofing Age/Materials CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate 
			PropertyYearRoofInstalled PropertyRoofTypeCode TerritoryH)
            Fac.WindRoofFactor
            (Factor Roof_Age Roof_Age_Group Roof_type Territory_Group)
            Fac.WindTerritoryFactor
            (Territory Territory_Group)
			source.MappingRoofType
			;
*Creates: 	out.Wind_R471_Roofing_Age_Materials;

proc sql;
	create table work.Wind_R471_Roofing_Age_Materials as
	select  distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.TerritoryH
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
		 , floor((case when p.PolicyTermYears=1 and &effst = 0 then p.PolicyIssueDate else p.PolicyEffectiveDate end
	 		- case when p.PropertyYearRoofInstalled <= 0 then date() else mdy(1,1,p.PropertyYearRoofInstalled) end)/365) as Roof_Age
		 , case when calculated Roof_Age >= 30 then '30+'
		 		when calculated Roof_Age < 0 then '-1'
				else left(put(calculated Roof_Age,3.))
			end	as Roof_Age_char
	from in.Policy_Info as p
	;
quit;

proc sql;
	create table out.Wind_R471_Roofing_Age_Materials as
	select distinct p.CaseNumber
		 , p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode, p.Roof_Age, r.Roof_Age_Group, r.Territory_Group
		 , r.Factor as Roof_Age_Materials label="Roof Age Materials"
	from work.Wind_R471_Roofing_Age_Materials as p
	inner join Fac.WindTerritoryFactor as t
	on p.TerritoryH = t.Territory
	left join source.MappingRoofType_NV as mrt
	on lower(trim(left(p.PropertyRoofTypeCode))) = lower(trim(left(mrt.PropertyRoofType)))
	inner join Fac.WindRoofFactor as r
	on  lower(trim(left(r.Roof_type))) = lower(trim(left(mrt.Type)))
	and p.Roof_Age_char = r.Roof_Age
	and t.Territory_Group = r.Territory_Group
	;
quit;
