*Rule 617 Liability Dangerous Dog MI (include "GERMAN SHEPHERD");
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageE CoverageF DogBreed)
            out.LiabilityDangerousDogCovEFactor
            (Factor CovE)
            Fac.LiabilityDangerousDogCovFFactor
            (Factor CovF)
			;
*Creates: 	out.Liability_Dangerous_Dog;

proc sql;
    create table out.Liability_Dangerous_Dog as
	select distinct p.CaseNumber, p.PolicyNumber
		 , case when upper(p.DogBreed) not in ("","GERMAN SHEPHERD","NONE OF THE ABOVE") then 0 else dde.Factor end as Dangerous_Dog_E 
		 , case when upper(p.DogBreed) not in ("","GERMAN SHEPHERD","NONE OF THE ABOVE") then 0 else ddf.Factor end as Dangerous_Dog_F 
	from in.Policy_Info as p
	inner join Fac.LiabilityDangerousDogCovEFactor as dde
	on p.CoverageE = dde.CovE
	inner join Fac.LiabilityDangerousDogCovFFactor as ddf
	on p.CoverageF = ddf.CovF
	;
quit;
