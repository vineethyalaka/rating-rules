*HO6 Tier Factor CW;
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class insurance_class_factor)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier Occupancy_Tier_Factor)
            out.Tier4_PriorClaims
            (CaseNumber PolicyNumber Prior_Claims_Class Prior_Claims_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
            i.CaseNumber, i.PolicyNumber
          , i.insurance_class, i.insurance_class_factor
		  , c.Prior_Claims_Class, c.Prior_Claims_class_Factor
          , l.Lapse_class, l.Lapse_class_Factor
		  , round(i.insurance_class_factor * c.Prior_Claims_class_Factor * l.Lapse_class_Factor,0.000001) as Tier_Factor
  from out.Tier1_InsuranceScore as i
		inner join out.Tier4_PriorClaims as c
		on i.CaseNumber = c.CaseNumber
		inner join out.Tier6_Lapse_Tier as l
		on i.CaseNumber = l.CaseNumber
	;
quit;
