*HO4 Tier Factor CW;
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class insurance_class_factor)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier Occupancy_Tier_Factor)
			;
*Creates: 	out.Tier_Factor;

/*BF 2017 11 06 Input tables now have tier factor in them. Updating to pull directly from them*/

proc sql;
create table in.tier_factor as
select a.casenumber, a.policynumber, 
a.PremiumTierLevel as TierClass, 
b.factor as Tier_Factor
from in.Policy_Info as a 
inner join fac.TierFactor as b on a.PremiumTierLevel = b.TierClass

;
quit;








/*proc sql;*/
/*    create table work.tierfactor_1 as*/
/*	select distinct i.CaseNumber, i.PolicyNumber*/
/*          ,i.Insurance_Class, o.Occupancy_Tier as Occupancy_Class*/
/*		  ,c.TierClass as TierClass1*/
/*    from out.Tier1_InsuranceScore as i*/
/*	inner join out.Tier3_Occupancy_Tier as o*/
/*		on i.CaseNumber = o.CaseNumber*/
/*	inner join out.Tier6_LapseInsurance as l*/
/*		on i.CaseNumber = l.CaseNumber*/
/*	inner join Fac.TierClass c*/
/*		on i.insurance_class = c.InsClass*/
/*		and o.Occupancy_Tier = c.OccClass*/
/*		and l.TierIncrease = c.LapseIncrease*/
/*	;*/
/*quit;*/
/**/
/*proc sql;*/
/*create table work.tierfactor_2 as*/
/*select a.CaseNumber, a.PolicyNumber*/
/*, case when y.group = 'NBbeforeTRP' then a.TierClass1*/
/*		when c.Claim_Free_Years = . then a.TierClass1*/
/*		when c.Claim_Free_Years >= 7 then min(t.MaxTier, a.TierClass1)*/
/*	   	else a.TierClass1*/
/*	end as TierClass*/
/*from work.tierfactor_1 as a*/
/*inner join out.policy_year as y*/
/*	on a.CaseNumber = y.CaseNumber*/
/*left join out.ClaimFreeYears as c*/
/*	on a.CaseNumber = c.CaseNumber*/
/*inner join fac.tier_claims as t*/
/*	on c.Claim_Free_Years >= ClaimFree_Min*/
/*	and c.Claim_Free_Years < ClaimFree_Max*/
/**/
/*;*/
/**/
/*quit;*/
/**/
/*proc sql;*/
/*create table out.tier_factor as*/
/*select b.* */
/*, f.Factor as Tier_Factor*/
/*from work.tierfactor_2 as b*/
/*inner join fac.tierfactor as f*/
/*	on b.TierClass = f.TierClass;*/
/*	quit;*/