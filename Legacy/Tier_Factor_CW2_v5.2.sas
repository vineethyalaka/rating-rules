
/*ITR 13618 DC, remove shopper, first home, lapse of insurance class*/
*Tier Factor CW (DRC) factors vary by terms;
*Uses:    	out.Tier_Classes
            (CaseNumber PolicyNumber Insurance_Class Shopper_Class Home_First_Class Prior_Claims_Class RCMV_Class Lapse_of_Insurance_Class)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class DRC_Factor)
            Fac.TierTable
            (Factor InsuranceScore Shopper Auto Prior RCMV Lapse)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
           c.CaseNumber, c.PolicyNumber
          , c.Prior_Claims_Class
          ,c.RCMV_Class

		  , t.factor


		  ,d.DRC_Class, d.DRC_Factor
		  ,round(t.factor * d.DRC_Factor,0.000001) as Tier_Factor
    from out.Tier_Classes as c
	inner join in.policy_info p
	on c.CaseNumber = p.CaseNumber
	inner join Fac.TierTable as t
	on  c.Insurance_Class          = t.InsuranceScore

	and c.Prior_Claims_Class       = t.Prior
	and c.RCMV_Class               = t.RCMV

	inner join out.Tier7_DRC as d
	on c.CaseNumber = d.CaseNumber
	;
quit;
