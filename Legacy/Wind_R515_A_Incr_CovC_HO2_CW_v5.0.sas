*Wind Rule 515.A/B Personal Property (Increased/Reduction in Coverage C) CW (HO2);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageC PropertyUnitsInBuilding)
            in.Endorsement_Info
            (CaseNumber PolicyNumber EXWIND)
            Fac.WindPropertyCovCFactor
            (Factor1 Factor2 FamilyUnits)
			;
*Creates: 	out.Wind_R515_A_Incr_CovC;

proc sql;
	create table out.Wind_R515_A_Incr_CovC as
	select distinct p.CaseNumber, p.PolicyNumber
		 , (p.CoverageC - p.CoverageA*cc.BaseCovC) as IncrCovC
		 , case when calculated IncrCovC > 0 then (1-e.EXWIND)*round(calculated IncrCovC/1000*cc.Factor1,1)
		 		when calculated IncrCovC <= 0 then (1-e.EXWIND)*round(calculated IncrCovC/1000*cc.Factor2,1)
		 		else 0 end as Coverage_C
	from in.Policy_Info as p
		inner join in.Endorsement_Info as e
		on p.CaseNumber = e.CaseNumber
		inner join Fac.WindPropertyCovCFactor as cc
		on  p.PropertyUnitsInBuilding = cc.FamilyUnits
	;
quit;
