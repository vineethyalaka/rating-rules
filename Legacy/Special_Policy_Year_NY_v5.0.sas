libname in '\\HSWEBDEV87\Share_New\Renewals_CWS\NY\v947\03_Data\Input\Form3\Test';
libname Fac '\\HSWEBDEV87\Share_New\Renewals_CWS\NY\v947\03_Data\Factors\v947_Factors_HO3';
proc sql;
    create table out.policy_year as
	select distinct hf.CaseNumber, hf.PolicyNumber, hf.FirstTerm, hf.RatingVersion, r.Group,
	hf.PolicyTermYears, mod(hf.PolicyTermYears+2,3) as PolicyYear,
	case when trim(p.RolloverCode) = 'AIGR' then 'D2' else 'D1' end as AIGGroup
	from in.Policy_Info as p
	inner join in.hf_info as hf
	on p.CaseNumber = hf.CaseNumber
	inner join Fac.RatingVersionDates r
	on (hf.FirstTerm >= datepart(r.initialPED_lower) and hf.FirstTerm <= datepart(initialPED_upper))
	;
quit;
