*Tier 5 RCMV Class CW;
*Uses:    	in.RCMV_info
            (CaseNumber PolicyNumber RCMV)
            Fac.TierRCMVClass
            (Class Ratio_upper Ratio_lower)
			;
*Creates: 	out.Tier5_RCMVClass;

proc sql;
    create table out.OTW_R410_RCMV as
	select distinct r.CaseNumber, r.PolicyNumber, t.Factor as RCMV
	from in.RCMV_info as r
         inner join Fac.OTWRCMVFactor as t
         on r.RCMV <= t.Ratio_upper
		 and r.RCMV > t.Ratio_lower
	;
quit;
