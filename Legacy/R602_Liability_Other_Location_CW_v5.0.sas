*Rule 602 Liability Other Insured Location Occupied by Insured CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PropertyUnitsInBuilding SecondaryResidences CoverageE CoverageF)
            Fac.LiabilityOtherInsuredCovEFactor
            (Factor Families CoverageE)
            Fac.LiabilityOtherInsuredCovFFactor
            (Factor Families CoverageF)
			;
*Creates: 	out.Liability_Other_Location;

proc sql;
    create table out.Liability_Other_Location as
	select distinct p.CaseNumber, p.PolicyNumber
		 , p.SecondaryResidences*ce.Factor as Other_Location_E label="Other Location E"
		 , p.SecondaryResidences*cf.Factor as Other_Location_F label="Other Location F"
	from in.Policy_Info as p
	inner join Fac.LiabilityOtherInsuredCovEFactor as ce
	on p.PropertyUnitsInBuilding = ce.Families
	and p.CoverageE = ce.CoverageE
	inner join Fac.LiabilityOtherInsuredCovFFactor as cf
	on p.PropertyUnitsInBuilding = cf.Families
	and p.CoverageF = cf.CoverageF
	;
quit;
