*HO4 OTW Rule 505.D Earthquake CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber ORD_LAW_FLAG HO_0448_ADDL_LIMIT_01 HO_0454_FLAG)
            in.Policy_Info
            (CaseNumber PolicyNumber CoverageC CoverageD PropertyUnitsInBuilding PropertyQuakeZone)
			Fac.OTWPropertyEarthquakeFactor
			(FactorB FactorF Territory)
			Fac.OTWPropertyLossOfUseFactor
			(BaseCovD)
			Fac.OTWPropertyCovCFactor
			(BaseCovC FamilyUnits)
			;
*Creates: 	out.OTW_R505D_Earthquake;

proc sql;
	create table out.OTW_R505D_Earthquake as
	select distinct e.CaseNumber, e.PolicyNumber
		 , p.CoverageC
		 , case when p.CoverageD > p.CoverageC*lou.BaseCovD then (p.CoverageD - p.CoverageC*lou.BaseCovD) else 0 end as IncrCovD
		 , eq.FactorB*p.CoverageC*1.01/1000 as EQ_CovC
		 , (case when e.ORD_LAW_FLAG = '1' then eq.FactorB else 0 end)*p.CoverageC*0.09/1000 as EQ_ORD_LAW
		 , eq.FactorF*calculated IncrCovD/1000 as EQ_IncrCovD
		 , eq.FactorG*e.HO_0448_ADDL_LIMIT_01/1000 as EQ_HO_0448
		 , case when e.HO_0454_FLAG = '1' then  round(eq.FactorB*p.CoverageC*1.01/1000
            + (case when e.ORD_LAW_FLAG = '1' then eq.FactorB else 0 end)*p.CoverageC*0.09/1000
            + eq.FactorF*calculated IncrCovD/1000
            + eq.FactorG*e.HO_0448_ADDL_LIMIT_01/1000,1)
			else 0
		   end as Earthquake
	from Fac.OTWPropertyLossOfUseFactor as lou
		, in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber
		inner join Fac.OTWPropertyEarthquakeFactor as eq
		on input(p.PropertyQuakeZone,3.) = input(eq.Territory,3.)
	;
quit;
