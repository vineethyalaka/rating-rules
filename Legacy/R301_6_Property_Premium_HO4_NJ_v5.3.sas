*HO4 Rule 301.6 Property Premium CW;
*Uses:    	out.OTW_R503_Business_Property
			(CaseNumber PolicyNumber Business_Property)
			out.OTW_R505C_EQ_Loss_Assessment
			(CaseNumber PolicyNumber EQ_Loss_Assessment)
			out.OTW_R505D_Earthquake
			(CaseNumber PolicyNumber Earthquake)
			out.OTW_R511_Loss_Assessment
			(CaseNumber PolicyNumber Loss_Assessment)
			out.OTW_R512_Loss_of_Use
			(CaseNumber PolicyNumber Coverage_D)
			out.OTW_R513_Ordinance_or_Law
			(CaseNumber PolicyNumber Ordinance_or_Law)
			out.OTW_R515_C_Unsched_Incr_CovC
			(CaseNumber PolicyNumber Unscheduled_Personal_Property)
			out.OTW_R516_Sched_Personal_Prop
			(CaseNumber PolicyNumber Scheduled_Personal_Property)
			out.OTW_R519_Computer
			(CaseNumber PolicyNumber Computer)
			out.OTW_R520_ID_Theft
			(CaseNumber PolicyNumber ID_Theft)
			out.OTW_R521_Water_Sump
			(CaseNumber PolicyNumber Water_Sump)
			out.OTW_R570_Limited_Fungi
			(CaseNumber PolicyNumber Limited_Fungi)
			out.WorkersComp_Domestic_Servants
			(CaseNumber PolicyNumber Domestic_Servants)
			out.Wind_Premium
			(CaseNumber PolicyNumber Wind_Premium)
			out.Hurricane_Premium
			(CaseNumber PolicyNumber Hurricane_Premium)
			;
*Creates: 	out.Property_Premium;

proc sql;
    create table out.Property_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , a.Business_Property
		 , b.EQ_Loss_Assessment
		 , c.Earthquake
		 , d.Loss_Assessment
		 , e.Coverage_D
		 , f.Ordinance_or_Law
		 , h.Unscheduled_Personal_Property
		 , i.Scheduled_Personal_Property
		 , j.Computer
		 , k.ID_Theft
		 , l.Water_Sump
		 , p.Domestic_Servants
		 , n.Limited_Fungi
		 , m.Wind_Premium
		 , o.Hurricane_Premium
		 , a.Business_Property
			+ b.EQ_Loss_Assessment
			+ c.Earthquake
			+ d.Loss_Assessment
		 	+ e.Coverage_D
			+ f.Ordinance_or_Law
			+ h.Unscheduled_Personal_Property
		 	+ i.Scheduled_Personal_Property
			+ j.Computer
			+ k.ID_Theft
			+ l.Water_Sump
			+ p.Domestic_Servants
			+ n.Limited_Fungi
			+ m.Wind_Premium
			+ o.Hurricane_Premium
			as Property_Premium format=10.4
		 , b.EQ_Loss_Assessment
			+ c.Earthquake
			+ k.ID_Theft
			+ l.Water_Sump
			as Other_Premium format=10.4
from out.OTW_R503_Business_Property as a 
	inner join out.OTW_R505C_EQ_Loss_Assessment as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_R505D_Earthquake as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.OTW_R511_Loss_Assessment as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_R512_Loss_of_Use as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_R513_Ordinance_or_Law as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_R515_C_Unsched_Incr_CovC as h 
	on a.CaseNumber = h.CaseNumber
	inner join out.OTW_R516_Sched_Personal_Prop as i 
	on a.CaseNumber = i.CaseNumber
	inner join out.OTW_R519_Computer as j 
	on a.CaseNumber = j.CaseNumber
	inner join out.OTW_R520_ID_Theft as k 
	on a.CaseNumber = k.CaseNumber
	inner join out.OTW_R521_Water_Sump as l 
	on a.CaseNumber = l.CaseNumber
	inner join out.Wind_Premium as m 
	on a.CaseNumber = m.CaseNumber
	inner join out.OTW_R570_Limited_Fungi as n 
	on a.CaseNumber = n.CaseNumber
	inner join out.Hurricane_Premium as o 
	on a.CaseNumber = o.CaseNumber
	inner join out.WorkersComp_Domestic_Servants as p 
	on a.CaseNumber = p.CaseNumber
	;
quit;
