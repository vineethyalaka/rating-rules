*OTW Rule 515.A/B Personal Property (Increased/Reduction in Coverage C) CW (HO2);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA CoverageC PropertyUnitsInBuilding)
            Fac.OTWPropertyCovCFactor
            (Factor1 Factor2 FamilyUnits)
			;
*Creates: 	out.OTW_R515_A_Incr_CovC;
*5/23/17 JL Adding floor to CovC limit (min 40% of CovA according to rule);

proc sql;
	create table out.OTW_R515_A_Incr_CovC as
	select p.CaseNumber, p.PolicyNumber
		 , (p.CoverageC - p.CoverageA*cc.BaseCovC) as IncrCovC
		, case when calculated IncrCovC > 0 then round(calculated IncrCovC/1000*cc.Factor1,1)
				when calculated IncrCovC <= 0 and calculated IncrCovC >= ((-0.1)*p.CoverageA)
					then round(calculated IncrCovC/1000*cc.Factor2,1)
				when calculated IncrCovC <= 0 and calculated IncrCovC < ((-0.1)*p.CoverageA)
					then round(((-0.1)*p.CoverageA)/1000*cc.Factor2,1)
		 		else 0 end as Coverage_C
	from in.Policy_Info as p
		inner join Fac.OTWPropertyCovCFactor as cc on  p.PropertyUnitsInBuilding = cc.FamilyUnits;
quit;
