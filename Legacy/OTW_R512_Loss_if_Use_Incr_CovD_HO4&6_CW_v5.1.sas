*HO6 OTW Rule 512 Loss of Use - Increase Coverage D CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageC CoverageD)
            Fac.OTWPropertyLossOfUseFactor
            (BaseCovD Factor)
			;
*Creates: 	out.OTW_R512_Loss_of_Use;
*3.20 EK changed BROLL to only effected term 1 poilcies;


proc sql;
	create table out.OTW_R512_Loss_of_Use as
	select p.CaseNumber, p.PolicyNumber, 

	case when p.PolicyAccountNumber in (14300, 14310) and p.PolicyTermYears=1 then case when p.CoverageD >= p.CoverageC * 0.4 then (p.CoverageD - p.CoverageC * 0.4) else 0 end
		else case when p.CoverageD > p.CoverageC*lou.BaseCovD then (p.CoverageD - p.CoverageC*lou.BaseCovD) else 0 end end as IncrCovD
		 , round(calculated IncrCovD/1000*lou.Factor,1) as Coverage_D
	from Fac.OTWPropertyLossOfUseFactor as lou
		, in.Policy_Info as p
	;
quit;
