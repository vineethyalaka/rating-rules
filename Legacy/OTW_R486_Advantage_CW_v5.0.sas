*OTW Rule 486 Homesite basic Package and Enhanced Package CW (Advantage);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0015_FLAG)
            in.Production_Info
            (CaseNumber PolicyNumber PolicyTermYears AdvFactor HH0015)
            Fac.OTWAdvantageFactor
            (Factor AdvInd HH0015 Flag)
			;
*Creates: 	out.OTW_R486_Advantage;

proc sql;
    create table out.OTW_R486_Advantage as
	select distinct e.CaseNumber, e.PolicyNumber, a.AdvInd, a.Factor as Advantage label="OTW Advantage Factor"
	from in.Endorsement_Info as e
	inner join in.Production_Info as pd
	on e.CaseNumber = pd.CaseNumber
    inner join Fac.OTWAdvantageFactor as a
	on pd.HH0015 = a.HH0015
	and e.HO_0015_FLAG = a.HO0015
    and pd.AdvFactor = a.Flag
	;
quit;
