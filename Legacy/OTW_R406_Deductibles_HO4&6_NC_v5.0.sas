


proc sql;
	create table out.OTW_R406_Deductibles as
	select distinct p.CaseNumber, p.PolicyNumber
		  , p.CoverageC, p.CoverageDeductible, p.WindHailDeductible, df.AOI_Lower, df.AOI_Upper
		  , df.Factor as Deductible_Factor
	from in.Policy_Info as p
	inner join Fac.OTWDeductibleFactor as df
	on input(p.CoverageDeductible,dollar8.) = df.Deductible
	and 
		(input(p.windhaildeductible,best24.) = df.WindHailDeductible
		or
		p.windhaildeductible="" and input(p.CoverageDeductible,dollar8.) = df.WindHailDeductible)
and p.CoverageC <= df.AOI_Upper
	and p.CoverageC >= df.AOI_Lower
	;
quit;
