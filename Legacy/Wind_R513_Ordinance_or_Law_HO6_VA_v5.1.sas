*HO6 Wind Rule 513 Ordinance or Law CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA)
            in.Endorsement_Info
            (CaseNumber PolicyNumber ORD_LAW_FLAG)
            Fac.WindOrdinanceOrLawFactor
            (Factor)
			;
*Creates: 	out.Wind_R513_Ordinance_or_Law;

proc sql;
	create table out.Wind_R513_Ordinance_or_Law as
	select p.CaseNumber,p.PolicyNumber
		 , case when e.ORD_LAW_FLAG="1" then round(olf.factor*p.CoverageA/1000,1) else 0 end as Ordinance_or_Law
	from Fac.WindOrdinanceOrLawFactor as olf
		, in.Endorsement_Info as e
		inner join in.Policy_Info as p
		on e.CaseNumber = p.CaseNumber
	;
quit;
