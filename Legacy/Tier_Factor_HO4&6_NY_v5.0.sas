*HO4 Tier Factor CW;
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class insurance_class_factor)
            out.Tier3_Occupancy_Tier
            (CaseNumber PolicyNumber Occupancy_Tier Occupancy_Tier_Factor)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
           i.CaseNumber, i.PolicyNumber
          ,i.Insurance_Class, o.Occupancy_Tier as Occupancy_Class
		  ,c.TierClass, f.Factor as Tier_Factor
    from out.Tier1_InsuranceScore as i
	inner join out.Tier3_Occupancy_Tier as o
		on i.CaseNumber = o.CaseNumber
	inner join out.Tier6_LapseInsurance as l
		on i.CaseNumber = l.CaseNumber
	inner join Fac.TierClass c
		on i.insurance_class = c.InsClass
		and o.Occupancy_Tier = c.OccClass
		and l.TierIncrease = c.LapseIncrease
	inner join Fac.TierFactor f
		on c.TierClass = f.TierClass
	;
quit;
