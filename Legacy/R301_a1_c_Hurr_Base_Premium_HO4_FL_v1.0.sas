*HO4 Rule 301.A.1.C Hurr Base Premium FL;
*Uses:    	out.Hurr_Territory_Rate
            (CaseNumber PolicyNumber Hurr_Territory_Rate)
            out.Hurr_AOI_Factor
            (CaseNumber PolicyNumber Hurr_AOI)
			;
*Creates: 	out.Hurr_Base_Premium;

proc sql;
    create table out.Hurr_Base_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , a.Hurr_Territory_Rate
		 , d.Hurr_AOI
		 , round(a.Hurr_Territory_Rate*d.Hurr_AOI,1) 
			as Hurr_Base_Premium
	from out.Hurr_Territory_Rate as a 
	inner join out.Hurr_AOI_Factor as d 
	on a.CaseNumber = d.CaseNumber
	;
quit;
