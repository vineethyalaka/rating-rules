*Tier Classes CW (DRC);
*Uses:    	out.Tier1_InsuranceScore
            (CaseNumber PolicyNumber insurance_class)
            out.Tier2_ShopperClass
            (CaseNumber PolicyNumber Shopper_Class)
            out.Tier3_HomeFirstClass
            (CaseNumber PolicyNumber Home_First_Class)
            out.Tier4_PriorClaims
            (CaseNumber PolicyNumber Prior_Claims_Class)
            out.Tier5_RCMVClass
            (CaseNumber PolicyNumber RCMV_Class)
            out.Tier6_LapseInsurance
            (CaseNumber PolicyNumber Lapse_of_Insurance_Class)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class)
			;
*Creates: 	out.Tier_Classes;

proc sql;
    create table out.Tier_Classes as
    select distinct 
    a.CaseNumber
	, a.PolicyNumber, a.insurance_class, b.TierIncrease as Shopper, d.Prior_Claims_Class
	, f.TierIncrease as Lapse, g.DRC_Class, t.InitialClass
	, case when InitialClass + Lapse > m.MaxClass then InitialClass else InitialClass + Lapse end as Class2
	, case when calculated Class2 + Shopper > m.MaxClass then calculated Class2 else calculated Class2 + Shopper end as FinalClass
    from out.Tier1_InsuranceScore as a
	inner join out.Tier2_ShopperClass as b
	on a.CaseNumber = b.CaseNumber
	inner join out.policy_year c
	on a.CaseNumber = c.CaseNumber
	inner join out.Tier4_PriorClaims as d
	on a.CaseNumber = d.CaseNumber
	inner join out.Tier6_LapseInsurance as f
	on a.CaseNumber = f.CaseNumber
	inner join out.Tier7_DRC as g
	on a.CaseNumber = g.CaseNumber
	inner join Fac.TierClasses t
	on a.insurance_class = t.InsuranceScore
	and d.Prior_Claims_Class = t.Prior
	and g.DRC_Class = t.DRC, Fac.TierMaxClass m
	;
quit;
