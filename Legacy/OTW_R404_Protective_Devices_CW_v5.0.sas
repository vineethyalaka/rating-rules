*OTW Rule 404 Protective Devices CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber ProtDevBurglarAlarm ProtDevFireAlarm ProtDevSprinklers)
            Fac.OTWProtDevBurglarAlarmFactor
            (Factor ProtDevBurglarAlarm)
            Fac.OTWProtDevFireAlarmFactor
            (Factor ProtDevFireAlarm)
            Fac.OTWProtDevSprinklersFactor
            (Factor ProtDevSprinklers)
			;
*Creates: 	out.OTW_R404_Protective_Devices;

proc sql;
	create table out.OTW_R404_Protective_Devices as
	select distinct p.CaseNumber, p.PolicyNumber
		 , baf.Factor as Burglar_Alarm label="Burglar Alarm"
		 , faf.Factor as Fire_Alarm label="Fire Alarm"
		 , sf.Factor as Sprinklers label="Sprinklers"
		 , baf.Factor*faf.Factor*sf.Factor as Protective_Devices  label="OTW Protective Devices"
	from in.Policy_Info as p
	inner join Fac.OTWProtDevBurglarAlarmFactor as baf
	on (p.ProtDevBurglarAlarm = "" and baf.ProtDevBurglarAlarm = "") 
	or p.ProtDevBurglarAlarm = baf.ProtDevBurglarAlarm
	inner join Fac.OTWProtDevFireAlarmFactor as faf
	on (p.ProtDevFireAlarm = "" and faf.ProtDevFireAlarm = "")
	or p.ProtDevFireAlarm = faf.ProtDevFireAlarm
	inner join Fac.OTWProtDevSprinklersFactor as sf
	on p.ProtDevSprinklers = sf.ProtDevSprinklers
	;
quit;
