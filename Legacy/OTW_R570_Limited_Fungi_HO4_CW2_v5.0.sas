*OTW Rule 570 Limited Fungi Coverage CW2 HO4;
*Uses:    	in.DataMAInfo
			(CaseNumber PolicyNumber HO_04_31 Property_Amount Liability_Amount)
			Fac.OTWPropertyFungiFactor
			(Factor)
			;
*Creates: 	out.OTW_R570_Limited_Fungi;

proc sql;
	create table out.OTW_R570_Limited_Fungi as
	select distinct 
		  a.CaseNumber
		, a.PolicyNumber
		, b.Factor as Limited_Fungi
	from in.DataMAInfo as a
	inner join Fac.OTWPropertyFungiFactor as b
		on a.HO_04_31 = b.HH_05_37
		and	a.Property_Amount = b.Property
		and	a.Liability_Amount = b.Liability
	;
quit;
