/* uses IT factor table (1 unit goes to class 3) */

proc sql;
	create table out.Tier2_Units_Tier as 
	select distinct p.CaseNumber, p.PolicyNumber, p.UnitsinBuilding
		, f.tier as Units_Tier, f.factor as Units_Tier_Factor
	from in.policy_info as p
		left join Fac.TierUnitFactorIT as f
		on f.Units_upper>=p.UnitsinBuilding
		and f.Units_lower<=p.UnitsinBuilding
	;
quit;
