proc sql;
    create table out.policy_year as
	select distinct hf.CaseNumber, hf.PolicyNumber, hf.FirstTerm, hf.RatingVersion, r.Group,
	p.ageofnypolicy, mod(p.ageofnypolicy+2,3) as PolicyYear,
	case when trim(p.RolloverCode) = 'AIGR' then 'D2' else 'D1' end as AIGGroup
	from in.Policy_Info as p
	inner join in.hf_info as hf
	on p.CaseNumber = hf.CaseNumber
	inner join Fac.RatingVersions r
	on (hf.RatingVersion >= r.Version_Lower and hf.RatingVersion <= r.Version_Upper)
	;
quit;
