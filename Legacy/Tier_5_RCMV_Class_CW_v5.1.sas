*Tier 5 RCMV Class CW;
*Uses:    	in.RCMV_info
            (CaseNumber PolicyNumber RCMV)
            Fac.TierRCMVClass
            (Class Ratio_upper Ratio_lower)
			;
*Creates: 	out.Tier5_RCMVClass;

proc sql;
    create table out.Tier5_RCMVClass as
	select DISTINCT r.CaseNumber, r.PolicyNumber, t.Class as RCMV_Class label="RCMV Class"
	from in.RCMV_info as r
         inner join Fac.TierRCMVClass as t
         on round(r.RCMV,0.01) <= t.Ratio_upper
		 and round(r.RCMV,0.01) > t.Ratio_lower
	;
quit;
