*Rule 301.9.2 Final Premium CW;
*Uses:    	out.Affinity_Adjusted_Premium
			(CaseNumber PolicyNumber Affinity_Adjusted_Premium)
			out.Capping_Amount
			(CaseNumber PolicyNumber Capping_Amount)
			Fac.ChargeMinimum
			(Factor)
			;
*Creates: 	out.Final_Premium;

proc sql;
    create table out.Final_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.Affinity_Adjusted_Premium
		 , b.Cap as Capping_Amount
		 , f.Factor as min_premium label="Minimum Premium"
		 , case when Affinity_Adjusted_Premium > f.Factor 
			then Affinity_Adjusted_Premium 
			else f.Factor end
		 	as Final_Premium
		 , c.Experience*a.Affinity*e.OTW_Premium as Adj_OTW_Premium
		 , c.Experience*a.Affinity*d.Wind_Premium as Adj_Wind_Premium
		 , c.Experience*a.Affinity*e.Tiered_Liability_Premium as Adj_Liability_Premium
		 , c.Experience*a.Affinity*(c.Property_Premium - d.Wind_Premium - d.Other_Premium) as Adj_Property_Premium
		 , c.Experience*a.Affinity*(d.Other_Premium) as Adj_Other_Premium
		 , Calculated Final_Premium 
			- Calculated Adj_OTW_Premium 
			- Calculated Adj_Wind_Premium 
			- Calculated Adj_Liability_Premium 
			- Calculated Adj_Property_Premium 
			- Calculated Adj_Other_Premium 
			as Adj_PolicyFee
	from Fac.ChargeMinimum as f, out.Affinity_Adjusted_Premium as a 
	inner join in.Capping_Info as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.Expense_Adjusted_Premium as c
	on a.CaseNumber = c.CaseNumber
	inner join out.Property_Premium as d
	on a.CaseNumber = d.CaseNumber
	inner join out.Tiered_Premium as e
	on a.CaseNumber = e.CaseNumber
	;
quit;
