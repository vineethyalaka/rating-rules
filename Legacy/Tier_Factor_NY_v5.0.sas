*Tier Factor CW (DRC);
*Uses:    	out.Tier_Classes
            (CaseNumber PolicyNumber Insurance_Class Shopper_Class Home_First_Class Prior_Claims_Class RCMV_Class Lapse_of_Insurance_Class)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class DRC_Factor)
            Fac.TierTable
            (Factor InsuranceScore Shopper Auto Prior RCMV Lapse)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
           c.CaseNumber, c.PolicyNumber
          ,c.FinalClass
          ,round(t.Factor,0.001) as MatrixFactor
		  ,hf.Factor as HomeFirstFactor
		  ,round(t.Factor * hf.Factor,0.000001) as Tier_Factor
    from out.Tier_Classes as c
	inner join Fac.TierFactors as t
		on c.FinalClass = t.Class
	inner join out.Tier3_homefirstclass as h
		on c.CaseNumber = h.CaseNumber
	inner join Fac.TierHomeFirstFactor as hf
		on h.Group = hf.Group
		and h.PolicyTermYears <= hf.Term_upper
		and h.PolicyTermYears >= hf.Term_lower
		and h.Home_First_Class = hf.Class
	;
quit;
