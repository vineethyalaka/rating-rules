proc sql;
	create table out.Tier_Factor_CovA_Claims as
	select a.CaseNumber, a.PolicyNumber, a.Prior_Claims_Class
	, b.Factor
	from out.tier4_priorclaims as a
	inner join fac.tiercovaclaimsfactor as b
	on a.Prior_Claims_Class = b.Tier
	;
quit;
