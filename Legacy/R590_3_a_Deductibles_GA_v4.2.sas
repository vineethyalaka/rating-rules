*Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA WindHailDeductible)
            Fac.WindDeductibleFactor
            (class AOI_Lower AOI_Upper Factor_Lower Factor_Upper Deductible)
			;
*Creates: 	out.Wind_Deductible;
/**/
/*proc sql;*/
/*    create table work.Wind_Deductible as*/
/*	select distinct p.CaseNumber, p.PolicyNumber*/
/*		 , p.CoverageA, p.CoverageDeductible, p.Group*/
/*		 , case when p.Group = 1 then "5"*/
/*		 	when p.Group = 2 then "2"*/
/*			else "2500.1" end*/
/*			as WindDeductible */
/*	from in.Policy_Info as p*/
/*	;*/
/*quit;*/
/**/
/*proc sql;*/
/*    create table out.Wind_Deductible as*/
/*	select distinct p.CaseNumber, p.PolicyNumber*/
/*		 , p.CoverageA, p.CoverageDeductible, p.Group*/
/*		 , p.WindDeductible, df.Factor as Wind_Deductible*/
/*	from work.Wind_Deductible as p*/
/*	inner join Fac.WindDeductibleFactor as df*/
/*	on case when substr(left(p.WindDeductible),1,1) = "$" */
/*				 then input(substr(left(p.WindDeductible),2,5),6.) */
/*			when p.WindDeductible = "" then 0*/
/*			else input(left(p.WindDeductible),6.) */
/*	   end */
/*	   = df.Deductible*/
/**/
/*	;*/
/*quit;*/


proc sql;
    create table work.Wind_Deductible as
	select distinct p.CaseNumber, p.PolicyNumber
		 , p.CoverageA, p.CoverageDeductible
/*, p.Group*/
		 , case when p.WindHailDeductible = "       2" then "2"
		 	when p.WindHailDeductible = "       5" then "5"
			else case when substr(left(p.CoverageDeductible),1,1) = "$" 
				 then substr(left(p.CoverageDeductible),2,5)
					when p.CoverageDeductible = "" then "0"
					else left(p.CoverageDeductible)
	  		 	end  
				end
			as WindDeductible 
	from in.Policy_Info as p
	;
quit;

proc sql;
    create table out.Wind_Deductible as
	select distinct p.CaseNumber, p.PolicyNumber
		 , p.CoverageA, p.CoverageDeductible
		 , p.WindDeductible, df.Factor as Wind_Deductible
	from work.Wind_Deductible as p
	inner join Fac.WindDeductibleFactor as df
	on case when substr(left(p.WindDeductible),1,1) = "$" 
				 then input(substr(left(p.WindDeductible),2,5),6.) 
			when p.WindDeductible = "" then 0
			else input(left(p.WindDeductible),6.) 
	   end 
	   = df.Deductible

	;
quit;
