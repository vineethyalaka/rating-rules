*HO6 OTW Rule 508 Units Rented CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber Weeks_Rented_to_Others)
            Fac.OTWPropertyUnitRentedFactor
            (Week_upper Week_lower Factor)
			out.OTW_Base_Premium
			(CaseNumber OTW_Base_Premium)
			out.OTW_R406_Deductibles
			(CaseNumber OTW_Deductible_Factor)
			;
*Creates: 	out.OTW_R508_Units_Rented;

proc sql;
	create table out.OTW_R508_Units_Rented as
	select distinct e.CaseNumber, e.PolicyNumber
		 , round(p.Base_Premium*d.Deductible_Factor*(ur.Factor-1),1) as Units_Rented
	from in.Endorsement_Info as e
		inner join out.Base_Premium as p
		on e.casenumber = p.casenumber
		inner join out.OTW_R406_Deductibles as d
		on e.casenumber = d.casenumber
		inner join Fac.OTWPropertyUnitRentedFactor as ur
		on input(Weeks_Rented_to_Others,3.) <= ur.week_upper
		and input(Weeks_Rented_to_Others,3.) >= ur.week_lower
	;
quit;
