*Rule 301.4 Liability Premium CW (HO2);
*Uses:    	out.Liability_Base_Rate
			(CaseNumber PolicyNumber Coverage_E Coverage_F)
			out.Liability_Dangerous_Dog
			(CaseNumber PolicyNumber Dangerous_Dog_E Dangerous_Dog_F)
			out.Liability_Unsafe_Pool
			(CaseNumber PolicyNumber Unsafe_Pool_E Unsafe_Pool_F)
			;
*Creates: 	out.Liability_Premium;

proc sql;
    create table out.Liability_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , a.Coverage_E
		 , a.Coverage_F
		 , f.Dangerous_Dog_E
		 , f.Dangerous_Dog_F
		 , f.Dangerous_Dog_E + f.Dangerous_Dog_F as Dangerous_Dog
		 , g.Unsafe_Pool_E
		 , g.Unsafe_Pool_F
		 , g.Unsafe_Pool_E + g.Unsafe_Pool_F as Unsafe_Pool
		 , a.Coverage_E 
			+ a.Coverage_F 
			+ f.Dangerous_Dog_E 
			+ f.Dangerous_Dog_F 
			+ g.Unsafe_Pool_E 
			+ g.Unsafe_Pool_F
			as Liability_Premium format=10.4
		 , h.Tier_Factor*calculated Liability_Premium as Tiered_Liability_Premium
	from out.Liability_Base_Rate as a 
	inner join out.Liability_Dangerous_Dog as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.Liability_Unsafe_Pool as g 
	on a.CaseNumber = g.CaseNumber
	inner join out.Tier_Factor as h
	on a.CaseNumber = h.CaseNumber
	;
quit;
