*Tier 4 Prior Claims Class CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate)
            in.loss_info
            (PolicyNumber LossDate LossCode LossAmount)
            Fac.ActofGod
            (ClaimTypeGod ActOfGodCode)
            Fac.TierPriorClaimClass
            (Class NumberofNAGMax NumberofNAGMin NumberofAOGMax NumberofAOGMin AgeType)
			;
*Creates: 	out.Deviation_PriorClaims;

*work.PriorClaims_a;;
proc sql;
	create table work.PriorClaims_a as
	select distinct l.casenumber, l.policynumber, p.PolicyTermYears, p.PolicyEffectiveDate, p.PolicyIssueDate, l.lossdate, l.LossCode, l.LossAmount
			, case when p.PolicyTermYears=1 then (p.PolicyIssueDate - l.LossDate)/365
				else (p.PolicyEffectiveDate - l.LossDate - 60)/365
			  end as LossAge
			, case when calculated lossage>3 then 1	/*within 3 years*/
				else 2
			  end as AgeType
	from in.policy_info as p
		inner join in.loss_info as l
		on p.PolicyNumber=l.policynumber
		and p.casenumber=l.casenumber
	where l.LossAmount>0 
		and (p.PolicyTermYears=1 and (p.PolicyIssueDate - l.LossDate)/365 <3	/*within 3 years*/
		or  p.PolicyTermYears^=1 and (p.PolicyEffectiveDate - l.LossDate - 60)/365 <3)
	;
quit;

*work.PriorClaims_b;;
proc sql;
	create table work.PriorClaims_b as
	select distinct a.casenumber, a.policynumber, count(a.casenumber) as count
	from work.PriorClaims_a as a
	group by a.PolicyNumber, a.casenumber
	;
quit;

*out.Deviation_PriorClaims;;
proc sql;
    create table out.Deviation_PriorClaims as
	select distinct a.CaseNumber, a.PolicyNumber
		 , case when b.count is Null then 0 else 1 end as Deviation_Prior_Claims
	from in.policy_info as a
	left join work.PriorClaims_b as b
	on a.PolicyNumber = b.PolicyNumber
	and a.CaseNumber = b.CaseNumber
    ;
quit;

