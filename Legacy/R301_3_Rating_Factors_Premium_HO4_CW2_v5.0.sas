*HO6 Rule 301.3 OTW Rating Factors Premium CW;
*Uses:    	out.OTW_Tiered_Base_Premium
			(CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
			out.OTW_R403_CovC_Replacement_Cost
			(CaseNumber PolicyNumber HO_04_90)
			out.OTW_R406_Deductibles
			(CaseNumber PolicyNumber OTW_Deductible_Factor)
			out.OTW_R480_Mature_Owner
			(CaseNumber PolicyNumber Mature_Owner)
			out.OTW_R488_Number_of_Units
			(CaseNumber PolicyNumber Number_of_Units)
			;
*Creates: 	out.OTW_Rating_Factors_Premium;

proc sql;
    create table out.OTW_Rating_Factors_Premium as
	select a.CaseNumber, a.PolicyNumber
		 , d.HO_04_90 as OTW_HO_04_90
		 , f.OTW_Deductible_Factor
		 , n.Mature_Owner as OTW_Mature_Owner
		 , o.Number_of_Units as OTW_Number_of_Units
		 , rf.RedFlags
		 , rf.Red_Flag_Factor
		 , a.OTW_Tiered_Base_Premium
		 , round(round(b.OTW_Territory_Rate*b.OTW_AOI*rf.Red_Flag_Factor,1.)*a.Tier_Factor,.01)*
			d.HO_04_90*f.OTW_Deductible_Factor*n.Mature_Owner*o.Number_of_Units
			as OTW_Rating_Factors_Premium format=10.4
	from out.OTW_Tiered_Base_Premium as a 
	inner join out.OTW_Base_Premium as b 
	on a.CaseNumber = b.CaseNumber
	inner join out.OTW_R403_CovC_Replacement_Cost as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_R406_Deductibles as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_R480_Mature_Owner as n 
	on a.CaseNumber = n.CaseNumber
	inner join out.OTW_R488_Number_of_Units as o 
	on a.CaseNumber = o.CaseNumber
	inner join out.Interaction_Class_Factor as rf
	on a.casenumber = rf.casenumber
	;
quit;
