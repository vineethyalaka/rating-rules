

proc sql;
    create table out.Wind_R590_Mitigation as
	select distinct p.CaseNumber, p.PolicyNumber
	, wm1.Factor as WM_ExposureCategory
	, wm2.Factor as WM_SecWaterResist
	, wm3.Factor as WM_BuildingCode
	, wm4.Factor as WM_RoofGeometry
	, wm5.Factor as WM_TypeWindBornDeb
	, wm6.Factor as WM_ExtWindBornDeb
	, wm7.Factor as WM_PredomRoofDeck
	, round(wm1.Factor*wm2.Factor*wm3.Factor*wm4.Factor*wm5.Factor*wm6.Factor*wm7.Factor,0.001) as Wind_Mitigation

	from in.Production_Info as p
	inner join Fac.WindMitigationExposureCategory as wm1 
    on p.comp_window_door_protection = wm1.Code
	inner join Fac.WindMitigationSecondWaterResist as wm2 
    on p.comp_engineered_status = wm2.Code
	inner join Fac.WindMitigationBuildingCode as wm3 
    on p.construction_completed = wm3.Code
	inner join Fac.WindMitigationRoofGeometry as wm4 
    on p.Question_code_20 = wm4.Code
	inner join Fac.WindMitigationTypeWindBornDeb as wm5 
    on p.comp_roof_wall_anchor = wm5.Code
	inner join Fac.WindMitigationExtWindBornDeb as wm6 
    on p.comp_roof_equipment = wm6.Code
	inner join Fac.WindMitigationPredomRoofDeck as wm7 
    on p.ml_70_number_of_families_01 = wm7.Code
	;
quit;
