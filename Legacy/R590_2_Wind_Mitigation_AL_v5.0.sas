*HO3 Rule 590.2 Wind Mitigation CW2;
*Uses:    	in.Production_Info
			(CaseNumber RoofCovering)
			in.Policy_Info
			(CaseNumber PolicyNumber )
			out.otw_r470_age_of_home 
			(Property_Age)
			out.wind_r471_roofing_age_materials
			(Roof_Age)
			Fac.WindMitigationRoofCover
			(Factor Code Category)
			;
*Creates: 	out.Wind_R590_Mitigation;

proc sql;
    create table work.Wind_R590_Mitigation as
	select p.CaseNumber, p.PolicyNumber, pd.RoofCovering as Mitigation_Code
	, r.Property_Age as Property_Age
	, s.Roof_Age as Roof_Age
	, case when r.Property_Age > 5 and s.Roof_Age > 5 then 1
		when r.Property_Age > 5 and s.Roof_Age <= 5 then 2
		when r.Property_Age <= 5 then 3 
		end as Category

	from in.policy_info as p
	inner join in.production_info as pd
		on p.casenumber = pd.casenumber
	inner join out.otw_r470_age_of_home as r
		on p.casenumber = r.casenumber
	inner join out.wind_r471_roofing_age_materials as s
		on p.casenumber = s.casenumber
	;
quit;

proc sql;
	create table out.Wind_R590_Mitigation as 
	select p.CaseNumber, p.PolicyNumber, p.Mitigation_Code, p.Property_Age, p.Roof_Age, p.Category, wm.Factor as Wind_Mitigation

	from work.Wind_R590_Mitigation as p

	inner join Fac.WindMitigationRoofCover as wm
	on p.Mitigation_Code = wm.Code
	and p.Category = wm.Category
;
quit;
