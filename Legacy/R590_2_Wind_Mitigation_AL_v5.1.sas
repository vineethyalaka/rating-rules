*HO3 Rule 590.2 Wind Mitigation CW2;
*Uses:    	in.Production_Info
			(CaseNumber RoofCovering)
			in.Policy_Info
			(CaseNumber PolicyNumber )
			out.otw_r470_age_of_home 
			(Property_Age)
			out.wind_r471_roofing_age_materials
			(Roof_Age)
			Fac.WindMitigationRoofCover
			(Factor Code Category)
			;
*Creates: 	out.Wind_R590_Mitigation;

proc sql;
    create table work.Wind_R590_Mitigation as
	select p.CaseNumber, p.PolicyNumber
	, case when pd.Construction_Completed = 0 then 500
		   when pd.Construction_Completed is null then 500
		   else pd.Construction_Completed end as Mitigation_Code
	, r.Property_Age as Property_Age
	, s.Roof_Age as Roof_Age
	, case when s.Roof_Age <= 10 and s.PropertyRoofTypeCode like '%Tin%' then 1
		   when s.Roof_Age <= 5 then 1
		   else 2 end as Category
	, case when p.CountyID in ('01119', '01091', '01105', '01047', '01001', '01101', '01011', '01005',
							   '01023', '01131', '13185', '01025', '01099', '01035', '01013', '01041',
							   '01109', '01031', '01045', '01067') then 2
		   when p.CountyID in ('01097', '01003', '01129', '01053', '01039', '01061', '01069') then 3
		   else 1 
		   end as Zone

	from in.policy_info as p
	inner join in.production_info as pd
		on p.casenumber = pd.casenumber
	inner join out.otw_r470_age_of_home as r
		on p.casenumber = r.casenumber
	inner join out.wind_r471_roofing_age_materials as s
		on p.casenumber = s.casenumber
	;
quit;

proc sql;
	create table out.Wind_R590_Mitigation as 
	select p.CaseNumber, p.PolicyNumber, p.Mitigation_Code, p.Property_Age, p.Roof_Age, p.Category, wm.Factor as Wind_Mitigation

	from work.Wind_R590_Mitigation as p

	inner join Fac.WindMitigationRoofCover as wm
	on p.Mitigation_Code = wm.Code
	and p.Category = wm.Category
	and p.Zone = wm.Zone
;
quit;


proc sql;
    create table work.Hurr_R590_Mitigation as
	select p.CaseNumber, p.PolicyNumber
	, case when pd.Construction_Completed = 0 then 500
		   when pd.Construction_Completed is null then 500
		   else pd.Construction_Completed end as Mitigation_Code
	, r.Property_Age as Property_Age
	, s.Roof_Age as Roof_Age
	, case when s.Roof_Age <= 10 and s.PropertyRoofTypeCode like '%Tin%' then 1
		   when s.Roof_Age <= 5 then 1
		   else 2 end as Category
	, case when p.CountyID in ('01119', '01091', '01105', '01047', '01001', '01101', '01011', '01005',
							   '01023', '01131', '13185', '01025', '01099', '01035', '01013', '01041',
							   '01109', '01031', '01045', '01067') then 2
		   when p.CountyID in ('01097', '01003', '01129', '01053', '01039', '01061', '01069') then 3
		   else 1 
		   end as Zone

	from in.policy_info as p
	inner join in.production_info as pd
		on p.casenumber = pd.casenumber
	inner join out.otw_r470_age_of_home as r
		on p.casenumber = r.casenumber
	inner join out.wind_r471_roofing_age_materials as s
		on p.casenumber = s.casenumber
	;
quit;

proc sql;
	create table out.Hurr_R590_Mitigation as 
	select p.CaseNumber, p.PolicyNumber, p.Mitigation_Code, p.Property_Age, p.Roof_Age, p.Category, wm.Factor as Hurr_Mitigation

	from work.Hurr_R590_Mitigation as p

	inner join Fac.HurrMitigationRoofCover as wm
	on p.Mitigation_Code = wm.Code
	and p.Category = wm.Category
	and p.Zone = wm.Zone
;
quit;
