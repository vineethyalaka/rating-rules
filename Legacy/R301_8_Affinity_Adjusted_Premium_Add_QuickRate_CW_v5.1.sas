*Rule 301.8 Affinity Adjusted Premium CW;
*Uses:    	out.QuickRate_Adjusted_Premium
			(CaseNumber PolicyNumber QuickRate_Adjusted_Premium)
			out.R481_Affinity
			(CaseNumber PolicyNumber Affinity)
			;
*Creates: 	out.Affinity_Adjusted_Premium;

proc sql;
    create table out.Affinity_Adjusted_Premium as
	select distinct a.CaseNumber, a.PolicyNumber
		 , a.QuickRate_Adjusted_Premium
		 , b.Affinity
		 , round(a.QuickRate_Adjusted_Premium*b.Affinity,1)
			as Affinity_Adjusted_Premium 
	from out.QuickRate_Adjusted_Premium as a 
	inner join out.R481_Affinity as b 
	on a.CaseNumber = b.CaseNumber
	;
quit;
