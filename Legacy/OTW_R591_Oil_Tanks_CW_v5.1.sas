*Rule 591 Oil Tanks CW;
*Uses:    	in.policy_Info
            (CaseNumber PolicyNumber Oil_Tank_Type)
            Fac.OTWOilTank
            (OilTankType Factor)
			;
*Creates: 	out.OTW_R591_Oil_Tank;


proc sql;
    create table out.OTW_R591_Oil_Tank as
	select distinct p.CaseNumber, p.PolicyNumber, p.Oil_Tank_Type
		, o.factor as Oil_Tank
	from in.policy_Info as p
	left join Fac.OTWOilTank o
	on p.Oil_Tank_Type = o.OilTankType
	;

quit;
