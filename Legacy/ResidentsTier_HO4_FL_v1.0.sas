*HO4 Residents Tier - FL;
*Uses:    	in.policy_info
			(CaseNumber PolicyNumber Residents_TotalCount)
			Fac.TierResidentsClass
			(Occupants_upper Occupants_lower Tier Factor)
			;
*Creates: 	out.Tier_Occupancy;

proc sql;
	create table out.Tier_Occupancy as 
	select p.CaseNumber, p.PolicyNumber, p.Residents_TotalCount
		, f.tier as Occupancy_Tier
	from in.policy_info as p
		left join Fac.TierResidentsClass as f
		on f.Occupants_upper>=p.Residents_TotalCount
		and f.Occupants_lower<=p.Residents_TotalCount
	;
quit;
