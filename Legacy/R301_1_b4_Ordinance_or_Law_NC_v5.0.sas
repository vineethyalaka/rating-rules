*Rule 301.1.b4 Ordinance or Law CW (HO 04 77);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber ORD_LAW_FLAG)
            Fac.OTWOrdinanceOrLawFactor
            (Factor)
			;
*Creates: 	out.OTW_Ordinance_or_Law_Factor;
*History: 09/28/2015 SY Revised based on cw_v5.0 for NC: bring in coverageA range;

proc sql;
    create table out.OTW_Ordinance_or_Law_Factor as
	select e.CaseNumber, e.PolicyNumber, case when e.ORD_LAW_FLAG = '1' then olf.Factor else 1 end as OTW_Ordinance_or_Law
	from in.Endorsement_Info as e
	inner join in.policy_info as p on e.casenumber = p.casenumber
	inner join Fac.OTWOrdinanceOrLawFactor as olf on (p.CoverageA<=olf.Ord_Upper and p.CoverageA >= olf.Ord_Lower)
	;
quit;
