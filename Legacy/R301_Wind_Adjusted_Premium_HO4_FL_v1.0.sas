*HO4 Wind Adjusted Premium - FL;
*Uses:    	out.wind_base_premium
			(CaseNumber PolicyNumber Wind_Base_Premium)
			out.Wind_R403_CovC_Replacement_Cost
			(CaseNumber HO_04_90)
			out.Wind_R406_Deductibles
			(CaseNumber Wind_Deductible)
			out.wind_r410_BCEG
			(CaseNumber BCEG_Factor)
			out.wind_r411_loss_mitigation
			(CaseNumber Mitigation_Factor)
			;
*Creates: 	out.wind_adjusted_premium;


proc sql;
	create table out.wind_adjusted_premium as
	select a.CaseNumber, a.PolicyNumber, a.Wind_Base_Premium
	, round((c.Wind_Deductible * a.Wind_Base_Premium),1.0) as Wind_Ded_Adj
	, round((b.HO_04_90 * calculated Wind_Ded_Adj),1.0) as Wind_PPRC_Adj
	, round((d.BCEG_Factor * calculated Wind_PPRC_Adj),1.0) as Wind_BCEG_Adj
	, round((e.Mitigation_Factor * calculated Wind_BCEG_Adj),1.0) as Wind_Mitigation_Adj
	, calculated Wind_Mitigation_Adj as Wind_Adjusted_Premium
	from out.wind_base_premium as a
	inner join out.wind_r403_covc_replacement_cost as b
	on a.CaseNumber = b.CaseNumber
	inner join	out.wind_r406_deductibles as c
	on a.CaseNumber = c.CaseNumber
	inner join	out.wind_r410_BCEG as d
	on a.CaseNumber = d.CaseNumber
	inner join out.wind_r411_loss_mitigation as e
	on a.CaseNumber = e.CaseNumber
	;
quit;
