*OTW Rule 406 Deductibles CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible CoverageA)
            Fac.OTWDeductibleFactor
            (Deductible AOI_Lower AOI_Upper Factor_Lower Factor_Upper Class)
			;
*Creates: 	out.OTW_R406_Deductibles;
data in.policy_info;	
set in.policy_info;
CovDeduct = input(compress(CoverageDeductible, '$'),20.);
drop CoverageDeductible;
rename CovDeduct = CoverageDeductible;
format WindHailDeductible $12.;
WHDed= strip(WindHailDeductible);
drop WindHailDeductible;
rename WHDed = WindHailDeductible;
run;

proc sql;
create table OUT.OTW_R406_Deductibles as 
select distinct p.CaseNumber
, p.PolicyNumber 
,p.CoverageA
, p.CoverageDeductible
, df.Class
, df.AOI_Lower
, df.AOI_Upper
, df.Factor_Lower
,df.Factor_Upper
,(p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1 
,(df.AOI_Upper - p.CoverageA)/(df.AOI_Upper - df.AOI_Lower) as D2 
,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind 
,round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind + (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 -
calculated Ind) , 0.001) as OTW_Deductible_Factor label="OTW Deductible" 
from in.Policy_Info as p inner join Fac.OTWDeductibleFactor as df 
on 
p.CoverageDeductible = df.Deductible 
and p.WindHailDeductible = df.WindHailDeductible 
and p.CoverageA <= df.AOI_Upper
and p.CoverageA > df.AOI_Lower ;
;
quit;
