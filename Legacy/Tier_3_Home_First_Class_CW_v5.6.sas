*Tier 3 Home First Class CW;
*Uses:    	in.hf_info
			(CaseNumber PolicyNumber AutoPolicyNumber PolicyAccountNumber PolicyTermYears PolicyEffectiveDate Ratingversion)
            source.HS_V4Rating_AffinityDiscount
            (version minmarketgroup maxmarketgroup factor)
			;
*Creates: 	out.Tier3_HomeFirstClass;

* 10/9/2017: not having liberty policy "Home First". ;

proc sql;
	create table out.Tier3_HomeFirstClass as
	select distinct a.CaseNumber
		 , a.PolicyNumber, a.PolicyEffectiveDate, a.FirstTerm
		 , a.PolicyAccountNumber
		 , a.AutoPolicyNumber
		 , case when trim(a.AutoPolicyNumber) = "" then "N"
		 	else "Y"
			end as AutoPolicyFlag
		 , a.ratingversion
		 , aff.description
		 , case when a.PolicyAccountNumber in (1050, 1400, 10001, 30001) then 2
		 		when left(trim(upcase(acc.description))) = "GMAC MORTGAGE"
					 and &statecode not in (29,46)	/*NJ and WA*/
					 then 1	/*according to CL's email on 03/14/2013*/
		 		when aff.factor in (.,1) then 1 				/*not eligible for affinity discount*/
				when a.PolicyAccountNumber >= 13200 and a.PolicyAccountNumber<= 13299 then 1
				when calculated AutoPolicyFlag = "Y" then 1	/*eligible for affinity discount and have auto policy*/
				else 2
				end as Home_First_Class 
	from in.hf_info as a
	left join in.HS_V4Rating_AffinityDiscount as aff
	on a.PolicyAccountNumber <= input(aff.maxmarketgroup,8.)
	and a.PolicyAccountNumber >= input(aff.minmarketgroup,8.)
	and a.Ratingversion = aff.version
	left join mapping.experiencemodifier as acc
	on a.PolicyAccountNumber <= acc.account_u
	and a.PolicyAccountNumber >= acc.account_l
	;
quit;

