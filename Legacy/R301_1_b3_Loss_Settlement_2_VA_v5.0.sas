*Rule 301.1.b3 Loss Settlement VA (HO 05 22);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO256_PERCENT_EST_REPL)
            Fac.OTWHO0456Factor
            (Factor2 Percent)
			;
*Creates: 	out.OTW_Loss_Settlement_Factor;

proc sql;
    create table out.OTW_Loss_Settlement_Factor_2 as
	select  distinct e.CaseNumber, e.PolicyNumber, case when e.HO256_FLAG='0' then 1 else hf522.Factor2 end as OTW_HO_05_22 label="OTW HO-05-22"
	from in.Endorsement_Info as e
	inner join Fac.OTWHO0522Factor as hf522
	on (case when e.HO256_PERCENT_EST_REPL < 1 then e.HO256_PERCENT_EST_REPL*100 else e.HO256_PERCENT_EST_REPL end) = hf522.Percent
	;
quit;
