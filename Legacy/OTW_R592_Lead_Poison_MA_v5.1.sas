*Rule 592 Special Requirement - Lead Poisoning Coverage Option;
*Uses:    	in.production_info
            (CaseNumber PolicyNumber Lead_Poisoning_Exclusion Lead_Poisoning_Coverage)
			Fac.otwpropertyleadpoisonfactor
            (Families CovLeadPoisoning LeadPoisoningExclusion Factor)
			;
*Creates: 	out.OTW_R592_Lead_Poisoning;


proc sql;
    create table out.OTW_R592_Lead_Poisoning as
	select distinct p.CaseNumber, p.PolicyNumber, r.Factor as Lead_Poison
	from in.production_info as p
	inner join in.policy_info as po
	on p.casenumber = po.casenumber
	inner join Fac.otwpropertyleadpoisonfactor as r
	on po.PropertyUnitsInBuilding = r.Families
		and p.Lead_Poisoning_Exclusion = r.LeadPoisoningExclusion
		and p.Lead_Poisoning_Coverage = r.CovLeadPoisoning
	;
quit;
