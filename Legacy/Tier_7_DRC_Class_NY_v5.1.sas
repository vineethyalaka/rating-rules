*Tier 7 DRC Class CW;
*Uses:    	in.DRC_Info
            (CaseNumber PolicyNumber SCORE_12_1 SCORE_13_1 SCORE_14_1 SCORE_15_1)
            Fac.TierDRCClass
            (D01 D02 D11 D21 D31 D41 Class Factor)
			;
*Creates: 	out.Tier7_DRC;

proc sql;
	create table work.Tier7_DRC as
	select distinct a.*
		 , a.SCORE_12_1||","||a.SCORE_13_1||","||a.SCORE_14_1||","||a.SCORE_15_1 as score
		 , case when index(calculated score,"D01") = 0 then 0 else 1 end as D01
		 , case when index(calculated score,"D02") = 0 then 0 else 1 end as D02
		 , case when index(calculated score,"D11") = 0 then 0 else 1 end as D11
		 , case when index(calculated score,"D21") = 0 then 0 else 1 end as D21
		 , case when index(calculated score,"D31") = 0 then 0 else 1 end as D31
		 , case when index(calculated score,"D41") = 0 then 0 else 1 end as D41
	from in.DRC_Info as a
	;
quit;

proc sql;
	create table work.Tier7_DRC2 as
	select distinct a.CaseNumber, a.PolicyNumber
		 , c.Class
	from Fac.TierDRCClass as c
	inner join work.Tier7_DRC as a
	on c.D01 = a.D01
	and c.D02 = a.D02
	and c.D11 = a.D11
	and c.D21 = a.D21
	and c.D31 = a.D31
	and c.D41 = a.D41
	;
quit;

proc sql;
	create table out.Tier7_DRC as
	select distinct p.CaseNumber, p.PolicyNumber, y.Group
		 , case when y.Group = "NBbeforeTRP" then 2
				when c.Class = . then 1 
				when a.Claim_Free_Years >= 7 then 1
				else c.Class end as DRC_Class
	from in.policy_info as p
	inner join out.policy_year as y
		on p.casenumber = y.casenumber
	inner join out.tier4_priorclaims as a
		on p.CaseNumber = a.CaseNumber
	left join work.Tier7_DRC2 as c
		on p.CaseNumber = c.CaseNumber
;
quit;
