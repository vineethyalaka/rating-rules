*Rule 301.1.a1 Territory Base Rate CW;
*Uses:    	in.policy_info
            (CaseNumber PolicyNumber TerritoryA)
            Fac.OTWTerritoryFactor
            (Territory Territory_Rate)
			;
*Creates: 	out.OTW_Territory_Rate;
*10/16/2017 SL Added Construction Mapping;

proc sql;
    create table out.Wind_Exclusion_Factor as
	select p.CaseNumber, p.PolicyNumber, cm.Type2 as Construction
	, case when EXWIND = 1 then w.Territory_Rate else 0 end as Wind_Exclusion
	from in.policy_info as p

	inner join Source.ConstructionMapping_NC as cm
	on upper(trim(left(p.PropertyConstructionClass))) = upper(trim(left(cm.PropertyConstructionClass)))

	inner join Fac.OTWxWindFactor as w
	on p.TerritoryA = w.Territory
	and cm.Type2 = w.Construction

	inner join in.endorsement_info as e
	on p.CaseNumber = e.CaseNumber
	;
quit;
