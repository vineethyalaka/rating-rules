proc sql;
	create table employee as
	select a.casenumber, a.policynumber, a.policyaccountnumber
		, b.Employee_associate_number, c.HO_2492_0913_SERVANT_IND as HD_078_flag
		, case when a.policyaccountnumber in (14200)
			then 1
			else 0		
		end as partner_ee
		, case when b.Employee_associate_number = '' then 0
			else 1
			end as employee
	from in.policy_info a
		inner join in.production_info b on a.casenumber = b.casenumber
		inner join in.endorsement_info c on a.casenumber = c.casenumber
;
quit;


proc sql;
      create table out.R481_Affinity as
      select distinct a.CaseNumber, a.PolicyNumber, a.PolicyAccountNumber, a.partnerflag, AutoPolicyNumber, AutoPolicyFlag,
      case 
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 1
            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.95 /*Non-Auto Partner*/
            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.90 /*Auto Partner*/
            when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.90 /*Affiliate Auto Discounts*/

          else b.Factor end as Affinity label="Affinity"
      from in.policy_info as a
      left join Fac.AffinityFactor as b
      on a.PolicyAccountNumber <= b.AccNumMax
      and a.PolicyAccountNumber >= b.AccNumMin
      and (a.AutoPolicyNumber is null and b.AutoPolicyFlag="N"
            or a.AutoPolicyNumber is not null and b.AutoPolicyFlag="Y")
      ;
quit;
