*Rule 301.3 OTW Rating Factors Premium CW (HO2);
*Uses:    	out.OTW_Tiered_Base_Premium
			(CaseNumber PolicyNumber OTW_Tiered_Base_Premium)
            out.OTW_R401_Superior_Constr
			(CaseNumber PolicyNumber OTW_Superior_Construction)
			out.OTW_R402_Row_House
			(CaseNumber PolicyNumber OTW_Row_House)
			out.OTW_R404_Protective_Devices
			(CaseNumber PolicyNumber Protective_Devices)
			out.OTW_R406_Deductibles
			(CaseNumber PolicyNumber OTW_Deductible_Factor)
			out.OTW_R470_Age_of_Home
			(CaseNumber PolicyNumber Age_of_Home)
			out.OTW_R471_Roofing_Age_Materials
			(CaseNumber PolicyNumber Roof_Age_Materials)
			out.OTW_R472_Saft_Heat
			(CaseNumber PolicyNumber Safe_Heat)
			out.OTW_R480_Mature_Owner
			(CaseNumber PolicyNumber Mature_Owner)
			;
*Creates: 	out.OTW_Rating_Factors_Premium;

proc sql;
    create table out.OTW_Rating_Factors_Premium as
	select  distinct a.CaseNumber, a.PolicyNumber
		 , c.OTW_Superior_Construction
		 , d.OTW_Row_House as OTW_Row_House
		 , e.Protective_Devices as OTW_Protective_Devices
		 , f.OTW_Deductible_Factor
		 , k.Age_of_Home as OTW_Age_of_Home
		 , l.Roof_Age_Materials as OTW_Roof_Age_Materials
		 , m.Safe_Heat as OTW_Safe_Heat
		 , n.Mature_Owner as OTW_Mature_Owner
		 , a.OTW_Tiered_Base_Premium
		 , a.OTW_Tiered_Base_Premium*c.OTW_Superior_Construction*d.OTW_Row_House*e.Protective_Devices*f.OTW_Deductible_Factor
			*k.Age_of_Home*l.Roof_Age_Materials*m.Safe_Heat*n.Mature_Owner
			as OTW_Rating_Factors_Premium format=10.4
	from out.OTW_Tiered_Base_Premium as a 
	inner join out.OTW_R401_Superior_Constr as c 
	on a.CaseNumber = c.CaseNumber
	inner join out.OTW_R402_Row_House as d 
	on a.CaseNumber = d.CaseNumber
	inner join out.OTW_R404_Protective_Devices as e 
	on a.CaseNumber = e.CaseNumber
	inner join out.OTW_R406_Deductibles as f 
	on a.CaseNumber = f.CaseNumber
	inner join out.OTW_R470_Age_of_Home as k 
	on a.CaseNumber = k.CaseNumber
	inner join out.OTW_R471_Roofing_Age_Materials as l 
	on a.CaseNumber = l.CaseNumber
	inner join out.OTW_R472_Saft_Heat as m 
	on a.CaseNumber = m.CaseNumber
	inner join out.OTW_R480_Mature_Owner as n 
	on a.CaseNumber = n.CaseNumber
	;
quit;
