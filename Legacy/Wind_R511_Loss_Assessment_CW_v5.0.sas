*Wind Rule 511 Loss Assessment CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber EXWIND HO_0435_INCREASED_AMOUNT)
            Fac.WindPropertyLossAssessmentFactor
            (Factor1 Factor2 Factor3 Amount_lower Amount_upper)
			;
*Creates: 	out.Wind_R511_Loss_Assessment;

proc sql;
	create table out.Wind_R511_Loss_Assessment as
	select distinct e.CaseNumber, e.PolicyNumber
		 , (1-e.EXWIND)*round(la.Factor2 + (e.HO_0435_INCREASED_AMOUNT - la.Factor1)*la.Factor3/5000,1) as Loss_Assessment
	from in.Endorsement_Info as e
		  inner join Fac.WindPropertyLossAssessmentFactor as la
		  on  e.HO_0435_INCREASED_AMOUNT >= la.Amount_lower
		  and e.HO_0435_INCREASED_AMOUNT <= la.Amount_upper
	;
quit;
