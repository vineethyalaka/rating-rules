*Tier 1 Insurance Score Class CW;
*Uses:    	in.drc_info
			(CaseNumber PolicyNumber InsScore ScoreModel MatchCode)
			Fac.TierInsuranceClass
			(score_upper score_lower ScoreModel MatchCode class)
			;
*Creates: 	out.Tier1_InsuranceScore;



proc sql;
	create table out.Tier1_InsuranceScore as 
	select y.CaseNumber, y.PolicyNumber, y.Group
		, case when (p.policytermyears - y.PolicyYear) = 1 then nb.ScoreModelType else rb.ScoreModelType end as ScoreModel
		, case when y.Group = "NBbeforeTRP" then d.InsDefault
			   when d.InsScore = . and d.MatchCode = 0 then 20
			   when d.InsScore = . and d.MatchCode = -1 then 21
			   when d.MatchCode = . then 21
			   else i.class 
		  end as insurance_class label="Insurance Class"
	from Fac.TierInsDefault d, out.policy_year as y
		left join in.drc_info as d on y.CaseNumber = d.CaseNumber
		left join in.policy_info as p on y.casenumber = p.casenumber
		/*******************************************/
		inner join source.hs_v4rating_stateconstants nb
		on 'NY' = nb.StateCode
		and p.policyeffectivedate >= datepart(nb.EffectiveDate)
		and p.policyeffectivedate <= datepart(nb.ExpirationDate)

		inner join source.hs_v4rating_stateconstants rb
		on 'NY' = rb.StateCode
		and p.policyeffectivedate >= datepart(rb.RenewalEffDate)
		and p.policyeffectivedate <= datepart(rb.RenewalExpDate)
		/*******************************************/
		left join Fac.TierInsuranceClass as i
		on i.score_upper>d.InsScore
		and i.score_lower<=d.InsScore
		and i.ScoreModel=case when (p.policytermyears - y.PolicyYear) = 1 then nb.ScoreModelType else rb.ScoreModelType end
		and i.MatchCode=d.MatchCode
	;
quit;
