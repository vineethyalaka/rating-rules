*Tier Factor CW (DRC) factors vary by terms;
*Uses:    	out.Tier_Classes
            (CaseNumber PolicyNumber Insurance_Class Shopper_Class Home_First_Class Prior_Claims_Class RCMV_Class Lapse_of_Insurance_Class)
            out.Tier7_DRC
            (CaseNumber PolicyNumber DRC_Class DRC_Factor)
            Fac.TierTable
            (Factor InsuranceScore Shopper Auto Prior RCMV Lapse)
			;
*Creates: 	out.Tier_Factor;

proc sql;
    create table out.Tier_Factor as
	select distinct 
           c.CaseNumber, c.PolicyNumber
          ,c.Insurance_Class, c.Shopper_Class, c.Home_First_Class, c.Prior_Claims_Class
          ,c.RCMV_Class, c.Lapse_of_Insurance_Class
          ,case when p.PolicyTermYears = 1 then t.Factor1
			    when p.PolicyTermYears = 2 then t.Factor2
		   		when p.PolicyTermYears = 3 then t.Factor3
		   else t.Factor4
			end as IRM_Factor
		  ,d.DRC_Class, d.DRC_Factor
		  ,round(calculated IRM_Factor * d.DRC_Factor,0.000001) as Tier_Factor
    from out.Tier_Classes as c
	inner join in.policy_info p
	on c.CaseNumber = p.CaseNumber
	inner join Fac.TierTable as t
	on  c.Insurance_Class          = t.InsuranceScore
	and c.Shopper_Class            = t.Shopper
	and c.Home_First_Class         = t.Auto
	and c.Prior_Claims_Class       = t.Prior
	and c.RCMV_Class               = t.RCMV
	and c.Lapse_of_Insurance_Class = t.Lapse
	inner join out.Tier7_DRC as d
	on c.CaseNumber = d.CaseNumber
	;
quit;
