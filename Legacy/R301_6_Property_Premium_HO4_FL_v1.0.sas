*HO4 Rule 301.6 Property Premium CW;
*Uses:    	out.OTW_R503_Business_Property
			(CaseNumber PolicyNumber Business_Property)
			out.OTW_R505C_EQ_Loss_Assessment
			(CaseNumber BCEG_EQ_Loss_Assessment)
			out.OTW_R505D_Earthquake
			(CaseNumber BCEG_Earthquake)
			out.OTW_R512_Loss_of_Use
			(CaseNumber Coverage_D)
			out.r515_c_unsched_incr_covc
			(CaseNumber Unscheduled_Personal_Property)
			out.r516_sched_personal_prop
			(CaseNumber Scheduled_Personal_Property)
			out.OTW_R519_Computer
			(CaseNumber Computer)
			out.OTW_R520_ID_Theft
			(CaseNumber ID_Theft)
			out.OTW_R529_Limited_Fungi
			(CaseNumber Limited_Fungi)
			;
*Creates: 	out.Property_Premium;

proc sql;
    create table out.Property_Premium as
		select a.CaseNumber, a.PolicyNumber
		, a.Business_Property
		+ b.BCEG_EQ_Loss_Assessment
		+ c.BCEG_Earthquake
		+ d.Coverage_D
		+ e.Computer
		+ f.ID_Theft
		+ g.Limited_Fungi
		+ h.Unscheduled_Personal_Property
		+ i.Scheduled_Personal_Property
		as Property_Premium
	from out.otw_r503_business_property as a
		inner join out.otw_r505c_eq_loss_assessment as b
		on a.CaseNumber = b.CaseNumber
		inner join out.otw_r505d_earthquake as c
		on a.CaseNumber = c.CaseNumber
		inner join out.otw_r512_loss_of_use as d
		on a.CaseNumber = d.CaseNumber
		inner join out.otw_r519_computer as e
		on a.CaseNumber = e.CaseNumber
		inner join out.otw_r520_id_theft as f
		on a.CaseNumber = f.CaseNumber
		inner join out.otw_r529_limited_fungi as g
		on a.CaseNumber = g.CaseNumber
		inner join out.r515_c_unsched_incr_covc as h
		on a.CaseNumber = h.CaseNumber
		inner join out.r516_sched_personal_prop as i
		on a.CaseNumber = i.CaseNumber
	;
quit;
