*Rule 590.3 Windstorm Deductible CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA WindHailDeductible)
            Fac.WindDeductibleFactor
            (class AOI_Lower AOI_Upper Factor_Lower Factor_Upper Deductible)
			;
*Creates: 	out.Wind_Deductible;

* Version 4 modifies the references to RI_Wind_Zone in the first query to be calculated references, and converts them to numbers in the case statement;
* Edited by David Govonlu on 4/11/2014;

%macro WindDed;
	
	%if &WindAdj = 1 %then %do;
		proc sql;
			create table work.Wind_Deductible as
			select p.CaseNumber, p.PolicyNumber
				 , p.CoverageA, p.TerritoryH as Territory, substr(p.territoryH, 1, 1) as RI_Wind_Zone /*First digit of territory determines building code zone (1, 2, 3)*/
				 , p.WindHailDeductible
				 , case when substr(left(p.CoverageDeductible),1,1) = "$" 
						 then substr(left(p.CoverageDeductible),2,5) 
					else left(p.WindHailDeductible) 
					end as CoverageDeductibleText
				 , case when substr(left(p.WindHailDeductible),1,1) = "$" 
						 then input(substr(left(p.WindHailDeductible),2,5),5.) 
					when p.WindHailDeductible is null then input(Calculated CoverageDeductibleText,5.)
					else input(left(p.WindHailDeductible),5.) 
			   		end as WindHailDeductible_num 
				 , case when calculated WindHailDeductible_num <= 10
				 		then calculated WindHailDeductible_num * CoverageA /100
					else calculated WindHailDeductible_num
					end as WindHailDed_Dollar
				 , case when input(calculated RI_Wind_Zone, 8.) = 2 then .02
					    when input(calculated RI_Wind_Zone, 8.) = 3 then .05
						else 0 end as MinPct
				 , (calculated MinPct)*CoverageA as CovA_pct
				 , max(calculated CovA_pct, &mindollar, calculated WindHailDed_Dollar) as min_deductible
				 , case when calculated min_deductible = calculated CovA_pct then (calculated MinPct)*100
						when calculated min_deductible = &mindollar then &mindollar
				 		else calculated WindHailDeductible_num
					end as WindHailDeductible_adj
			from in.Policy_Info as p
			;
		quit;

		proc sql;
		    create table out.Wind_Deductible as
			select p.CaseNumber, p.PolicyNumber, p.Territory, p.RI_Wind_Zone 
				 , p.CoverageA, p.WindHailDeductible, p.min_deductible, p.WindHailDeductible_adj, df.class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
			     ,(p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
				 ,(df.AOI_Upper - p.CoverageA)/(df.AOI_Upper - df.AOI_Lower) as D2
				 ,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind
				 , round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind
				         + (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 - calculated Ind)
		                , 0.001) as Wind_Deductible label="Wind Deductible"
			from work.Wind_Deductible as p
			inner join Fac.WindDeductibleFactor as df
			on p.RI_Wind_Zone = df.RI_Wind_Zone
			and p.WindHailDeductible_adj = df.Deductible
			and p.CoverageA <= df.AOI_Upper
			and p.CoverageA > df.AOI_Lower
			;
		quit;
	%end;

	%if &WindAdj = 0 %then %do;
		proc sql;
			create table work.Wind_Deductible as
			select p.CaseNumber, p.PolicyNumber
				 , p.CoverageA, p.TerritoryH as Territory, substr(p.territoryH, 1, 1) as RI_Wind_Zone /*First digit of territory determines building code zone (1, 2, 3)*/
				 , p.WindHailDeductible
				 , case when substr(left(p.CoverageDeductible),1,1) = "$" 
						 then substr(left(p.CoverageDeductible),2,5) 
					else left(p.WindHailDeductible) 
					end as CoverageDeductibleText
				 , case when substr(left(p.WindHailDeductible),1,1) = "$" 
						 then input(substr(left(p.WindHailDeductible),2,5),5.) 
					when p.WindHailDeductible is null then input(Calculated CoverageDeductibleText,5.)
					else input(left(p.WindHailDeductible),5.) 
			   		end as WindHailDeductible_num 
/*				 , case when p.HurricaneDeductible is null then input(left(p.WindHailDeductible),5.) */
/*				 		else input(left(p.HurricaneDeductible), 5.)*/
/*			   		end as WindHailDeductible_num*/
			from in.Policy_Info as p
			;
		quit;

		proc sql;
		    create table out.Wind_Deductible as
			select p.CaseNumber, p.PolicyNumber, p.Territory, p.RI_Wind_Zone
				 , p.CoverageA, p.WindHailDeductible_num, df.class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
			     ,(p.CoverageA - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
				 ,(df.AOI_Upper - p.CoverageA)/(df.AOI_Upper - df.AOI_Lower) as D2
				 ,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind
				 , round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind
				         + (df.Factor_Lower + df.Factor_Upper*(p.CoverageA - df.AOI_Lower)/10000)*(1 - calculated Ind)
		                , 0.001) as Wind_Deductible label="Wind Deductible"
			from work.Wind_Deductible as p
			inner join Fac.WindDeductibleFactor as df
			on p.RI_Wind_Zone = df.RI_Wind_Zone
			and p.WindHailDeductible_num = df.Deductible
			and p.CoverageA <= df.AOI_Upper
			and p.CoverageA > df.AOI_Lower
			;
		quit;
	%end;
%mend;
%WindDed;
