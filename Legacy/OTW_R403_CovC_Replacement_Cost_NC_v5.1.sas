*OTW Rule 403 Coverage C (Personal property) Replacement Cost Coverage CW (HO-04-90);
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber HO_0490_FLAG)
            out.OTW_R486_Advantage
            (CaseNumber PolicyNumber AdvInd)
            Fac.OTWHO0490Factor
            (Factor)
			;
*Creates: 	out.OTW_R403_CovC_Replacement_Cost;
* SL 12/1/2017 Modified CW version 5.0, remove AdvInd Flag as indicator for discount because NCRB does not take package into account and we
  cannot give a discount on these endorsements as we did not file that as a deviation with the state;

proc sql;
	create table out.OTW_R403_CovC_Replacement_Cost as
	select e.CaseNumber, e.PolicyNumber
		 , case when e.HO_0490_FLAG = '1' then hf490.Factor else 1 end as HO_04_90 label="OTW HO-04-90"
	from Fac.OTWHO0490Factor as hf490
    , in.Endorsement_Info as e
	;
quit;
