*Tier 3 Home First Class CW;
*Uses:    	in.hf_info
			(CaseNumber PolicyNumber AutoPolicyNumber PolicyAccountNumber PolicyTermYears PolicyEffectiveDate Ratingversion)
            Fac.AffinityFactor
            (Class AccNumMax AccNumMin AutoPolicyFlag)
			;
*Creates: 	out.Tier3_HomeFirstClass;


proc sql;
	create table out.Tier3_HomeFirstClass as
	select distinct a.CaseNumber
		 , a.PolicyNumber, a.PolicyEffectiveDate, a.FirstTerm
		 , a.PolicyAccountNumber
		 , a.AutoPolicyNumber
		 , case when trim(a.AutoPolicyNumber) = "" then "N"
		 	else "Y"
			end as AutoPolicyFlag
		 , a.ratingversion
/*		 , case when a.ratingversion <= 713 then 1 else d.Class end as Home_First_Class */
		/*Rating Vesion 713: the first to inplement IRM (IL)*/
		 , d.Class as Home_First_Class 
	from in.hf_info as a
	inner join Fac.AffinityFactor as d
	on case when trim(a.AutoPolicyNumber) = "" then "N"
		 	else "Y"
			end = d.AutoPolicyFlag
	and a.PolicyAccountNumber <= d.AccNumMax
	and a.PolicyAccountNumber >= d.AccNumMin
	;
quit;

