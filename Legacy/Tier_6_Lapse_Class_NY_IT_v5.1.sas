*Tier 6 Lapse Class CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber Lapse_of_Insurance end_code_3_PI end_code_3_NH New_Home_Flag)
            in.Production_Info
            (CaseNumber PolicyNumber Lapse_Date_Used)
            Fac.TierLapseDays
            (LapseDays)
			;
*Creates: 	out.Tier6_LapseInsurance;

proc sql;
    create table out.Tier6_LapseInsurance as
	select distinct q.CaseNumber, q.PolicyNumber
		, f.TierIncrease
		from (
			select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears
		 	 	,pd.Lapse_Date_Used
		  		,p.Lapse_of_Insurance
			  	,p.end_code_3_NH as ProposeEffDate format=date9.
			  	,p.end_code_3_PI as CurrentExpDate format=date9.
			  	,p.end_code_3_NH - p.end_code_3_PI as period
		 	 	,l.LapseDays
		  		,case when pd.Lapse_Date_Used = 0 then 1
					when calculated period -1 < l.LapseDays then 1 
					else 2
		   		end as Lapse_of_Insurance_Class
			from in.Policy_Info as p
			inner join out.policy_year as y
			on p.casenumber = y.casenumber
			inner join in.Production_Info as pd
			on p.CaseNumber = pd.CaseNumber
			, Fac.TierLapseDays as l) as q
		inner join Fac.TierLapseClass f
		on q.Lapse_of_Insurance_Class = f.class
		and q.PolicyTermYears <= f.Term_upper
		and q.PolicyTermYears >= f.Term_lower
    ;
quit;
