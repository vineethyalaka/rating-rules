*Wind Rule 408 Actual Cash Value Loss Settlement - Windstorm or Hail Losses to Roof Surfacing CW2 with HH-04-93 mandatory for age > 10 (HO-04-93);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber PolicyTermYears PolicyIssueDate PolicyEffectiveDate 
			PropertyYearRoofInstalled PropertyRoofTypeCode TerritoryH)
			in.Endorsement_info
			(CaseNumber PolicyNumber HO_0493_FLAG)
            Fac.WindHO0493Factor
            (Factor Roof_Age Roof_Age_Group Roof_type Territory_Group)
            Fac.WindTerritoryFactor
            (Territory Territory_Group)
			source.MappingRoofType
			;
*Creates: 	out.Wind_R408_ACV_Roof;

proc sql;
	create table work.Wind_R408_ACV_Roof as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate, p.TerritoryH
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode
		 , floor((case when p.PolicyTermYears=1 and &effst = 0 then p.PolicyIssueDate else p.PolicyEffectiveDate end
	 		- case when p.PropertyYearRoofInstalled <= 0 then date() else mdy(1,1,p.PropertyYearRoofInstalled) end)/365) as Roof_Age
		 , case when calculated Roof_Age >= 30 then '30+'
		 		when calculated Roof_Age < 0 then '-1'
				else left(put(calculated Roof_Age,3.))
			end	as Roof_Age_char
	from in.Policy_Info as p
	;

	create table out.Wind_R408_ACV_Roof as
	select distinct p.CaseNumber, p.PolicyNumber, p.PolicyTermYears, p.PolicyIssueDate, p.PolicyEffectiveDate
		 , p.PropertyYearRoofInstalled, p.PropertyRoofTypeCode, p.Roof_Age, r.Roof_Age_Group, r.Territory_Group
		 , case when p.Roof_Age > 1000 then 1 else 0 end as HH_04_93_FLAG
		 , input(e.HO_0493_FLAG,2.) as HO_0493_FLAG
		 , case when calculated HH_04_93_FLAG + calculated HO_0493_FLAG = 0 then 1 else r.HO_04_93 end as HO_04_93
	from work.Wind_R408_ACV_Roof as p
	inner join in.Endorsement_info as e
	on p.CaseNumber = e.CaseNumber
	inner join Fac.WindTerritoryFactor as t
	on p.TerritoryH = t.Territory
	left join source.MappingRoofType as mrt
	on lower(trim(left(p.PropertyRoofTypeCode))) = lower(trim(left(mrt.PropertyRoofType)))
	inner join Fac.WindRoofFactor as r
	on  lower(trim(left(r.Roof_type))) = lower(trim(left(mrt.Type)))
	and p.Roof_Age_char = r.Roof_Age
	and t.Territory_Group = r.Territory_Group
	;
quit;
