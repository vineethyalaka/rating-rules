
proc sql;
	create table out.OTW_R515_Refrigerated_Property as
	select e.CaseNumber, e.PolicyNumber, e.HO_0498_FLAG
		 , case when e.HO_0498_FLAG = "1" then rf.Factor else 0 end as Refrigerated_Property
	from Fac.OTWPropertyRefrideratedFactor as rf
		, in.Endorsement_Info as e
	;
quit;
