*10/25/2018: Mo;
*This programs works DE HO3 legacy;
*Added logic DRC class and factor for TrueRisk Auto-Property model(model 3).;

proc sql noprint;
select  score
into :TRC_DRC_list separated by ','
from fac.TRC_DRC_list;
QUIT;

proc sql;
	create table work.Tier7_DRC as
	select distinct a.*
		 , a.SCORE_12_1||","||a.SCORE_13_1||","||a.SCORE_14_1||","||a.SCORE_15_1 as score
		 /* if scoreModel = 2, then check if DRC code exist */
		 , case when a.scoreModel = 2 and index(calculated score,"D01") > 0 then 1 else 0 end as D01
		 , case when a.scoreModel = 2 and index(calculated score,"D02") > 0 then 1 else 0 end as D02
		 , case when a.scoreModel = 2 and index(calculated score,"D11") > 0 then 1 else 0 end as D11
		 , case when a.scoreModel = 2 and index(calculated score,"D21") > 0 then 1 else 0 end as D21
		 , case when a.scoreModel = 2 and index(calculated score,"D31") > 0 then 1 else 0 end as D31
		 , case when a.scoreModel = 2 and index(calculated score,"D41") > 0 then 1 else 0 end as D41

		 /* if scoreModel = 3, first count number of DRC code that starts with D */
		 , case when a.scoreModel = 3 and a.SCORE_12_1 like 'D%' then 1 else 0 end 
		 + case when a.scoreModel = 3 and a.SCORE_13_1 like 'D%' then 1 else 0 end 
		 + case when a.scoreModel = 3 and a.SCORE_14_1 like 'D%' then 1 else 0 end 
		 + case when a.scoreModel = 3 and a.SCORE_15_1 like 'D%' then 1 else 0 end as TRC_DRC_count 
		 /* then check if selected DRC code exist*/
		 , case when a.scoreModel = 3 and 
		   (index("&TRC_DRC_list", a.SCORE_12_1) > 0 or index("&TRC_DRC_list", a.SCORE_13_1) > 0 or 
			index("&TRC_DRC_list", a.SCORE_14_1) > 0 or index("&TRC_DRC_list", a.SCORE_15_1) > 0) then 'TRUE' else 'FALSE' end as TRC_DRC_list_flag
	from in.drc_info as a
	;
quit;

proc sql;
	create table out.Tier7_DRC as
	select distinct a.CaseNumber, a.PolicyNumber, a.scoreModel,a.matchcode,TRC_DRC_count,TRC_DRC_list_flag
		 , case when a.scoreModel = 2 then c.DRC_Class 
		        when a.scoreModel = 3 then t.Class 
		   end as DRC_Class
		 , case when a.scoreModel = 2 then c.Factor 
		        when a.scoreModel = 3 then t.Factor 
		   end as DRC_Factor
	from work.Tier7_DRC as a
	left join fac.TierDRCClass as c
	on c.D01 = a.D01
	and c.D02 = a.D02
	and c.D11 = a.D11
	and c.D21 = a.D21
	and c.D31 = a.D31
	and c.D41 = a.D41
	left join fac.tierdrcclass_ScoreModel3 t
	on a.TRC_DRC_count between t.DRC_Count_Min and t.DRC_Count_Max and a.TRC_DRC_list_flag = t.Derogatory_Code_Flag 
	;
quit;


