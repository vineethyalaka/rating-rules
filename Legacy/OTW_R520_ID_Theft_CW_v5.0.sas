*OTW Rule 520 Identity Fraud Expense Coverage CW;
*Uses:    	in.Endorsement_Info
            (CaseNumber PolicyNumber IDTheft)
            Fac.OTWPropertyIDTheftFactor
            (Factor)
			;
*Creates: 	out.OTW_R520_ID_Theft;

proc sql;
	create table out.OTW_R520_ID_Theft as
	select distinct e.CaseNumber, e.PolicyNumber
		 , case when e.IDTheft = 1 then round(id.Factor,1) else 0 end as ID_Theft
	from Fac.OTWPropertyIDTheftFactor as id
		, in.Endorsement_Info as e
	;
quit;
