*Tier 1 Insurance Score Class CW;
*Uses:    	in.drc_info
			(CaseNumber PolicyNumber InsScore ScoreModel MatchCode)
			Fac.TierInsuranceClass
			(score_upper score_lower ScoreModel MatchCode class)
			;
*Creates: 	out.Tier1_InsuranceScore;

proc sql;
	create table out.Tier1_InsuranceScore as 
	select distinct d.CaseNumber, d.PolicyNumber, d.MatchCode, d.ScoreModel, d.InsScore
		, case when d.InsScore = . and d.MatchCode = 0 then 20
			   when d.InsScore = . and d.MatchCode = -1 then 21
			   when d.MatchCode = . then 21
			   else i.class 
		  end as insurance_class label="Insurance Class"
	from in.drc_info as d 
		left join Fac.TierInsuranceClass as i
		on i.score_upper>d.InsScore
		and i.score_lower<=d.InsScore
		and i.ScoreModel=d.ScoreModel
		and i.MatchCode=d.MatchCode
	;
quit;
