*OTW Rule 406 Deductibles CW (HO2);
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageDeductible CoverageA)
            Fac.OTWDeductibleFactor
            (Deductible AOI_Lower AOI_Upper Factor_Lower Factor_Upper Class)
			;
*Creates: 	out.OTW_R406_Deductibles;

proc sql;
	create table out.OTW_R406_Deductibles as
	select distinct p.CaseNumber, p.PolicyNumber
		  , c.Coverage, p.CoverageDeductible, df.Class, df.AOI_Lower, df.AOI_Upper, df.Factor_Lower, df.Factor_Upper
	      ,(c.Coverage - df.AOI_Lower)/(df.AOI_Upper - df.AOI_Lower) as D1
		  ,(df.AOI_Upper - c.Coverage)/(df.AOI_Upper - df.AOI_Lower) as D2
		  ,(calculated D1 + abs(calculated D1))/(2*calculated D1) as Ind
		  ,round((df.Factor_Upper*calculated D1 + df.Factor_Lower*calculated D2)*calculated Ind
		          + (df.Factor_Lower + df.Factor_Upper*(c.Coverage - df.AOI_Lower)/10000)*(1 - calculated Ind)
                , 0.001) as OTW_Deductible_Factor label="OTW Deductible"
	from in.Policy_Info as p
	inner join out.Coverage as c
	on p.CaseNumber = c.CaseNumber
	inner join Fac.OTWDeductibleFactor as df
	on input(p.CoverageDeductible,dollar8.) = df.Deductible
	and c.Coverage <= df.AOI_Upper
	and c.Coverage > df.AOI_Lower
	;
quit;
