*Tier 5 RCMV Class MA;
*Uses:    	in.RCMV_info
            (CaseNumber PolicyNumber RCMV)
            Fac.TierRCMVClass
            (Factor Class Ratio_upper Ratio_lower)
			;
*Creates: 	out.Tier5_RCMVClass;

proc sql;
    create table out.Tier5_RCMVClass as
	select distinct r.CaseNumber, r.PolicyNumber, t.Class as RCMV_Class label="RCMV Class", t.Factor as RCMV_Factor label="RCMV Factor"
	from in.RCMV_info as r
         inner join Fac.TierRCMVClass as t
         on round(r.RCMV,0.01) <= t.Ratio_upper
		 and round(r.RCMV,0.01) > t.Ratio_lower
	;
quit;
