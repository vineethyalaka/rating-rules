*HO6 OTW Rule 507.Coverage A Increase Limits CW;
*Uses:    	in.Policy_Info
            (CaseNumber PolicyNumber CoverageA)
            Fac.OTWPropertyCovACondoFactor
            (Factor)
			;
*Creates: 	out.OTW_R507_Incr_CovA;

proc sql;
	create table out.OTW_R507_Incr_CovA as
	select distinct p.CaseNumber, p.PolicyNumber, OTW_Territory_Rate, OTW_Prot_Constr
		 , case when p.CoverageA > 1000 then (p.CoverageA - 1000)
			else 0 end as IncrCovA
		 , round(calculated IncrCovA/1000*a.Factor*OTW_Territory_Rate*OTW_Prot_Constr,1) as Coverage_A
	from Fac.OTWPropertyCovACondoFactor as a
		 ,in.Policy_Info as p
		 inner join out.Base_Premium as b
		 on p.CaseNumber = b.CaseNumber
	;
quit;
