** RULE 406. DEDUCTIBLES **;

**=====================================================================================**
History: 2015 03 18 JT  Initial draft
		 2015 05 11 SY  Added Hurricane peril indicator
**=====================================================================================**;


data ded_EQ_table;
 set fac.'301#a location id$'n;
if _n_=1 then delete;
rename f1=LocationID
f2=Deductible_Group
f3=EQ_Zone;
keep f1 f2 f3;
run;

data LocationID;
 set out.TERRITORY;
keep CaseNumber PolicyNumber LocationID;
run;

proc sql;
create table ded_group as
select l.casenumber, l.policynumber, Deductible_Group,l.locationid
from ded_EQ_table d
inner join locationID l
on input(d.LocationID,8.) = l.LocationID
;
quit;


%macro Ded();
	data deductible_fac;
	set fac.'406# Deductibles$'n;
	format f1 f2 12.2;
	if f1 < 1 then f1 = f1*100;
	if f1 = . then delete;
	cova_L = lag(f2); if cova_L > f2 or cova_L = . then cova_L = 0; rename f2 = cova_H; 
	Fire_n = Fire*1; Theft_n = Theft*1; waternw_n = 'Water Non-Weather'n*1; 
	othernw_n = 'All Other Non-Weather'n*1; Lightning_n = Lightning*1; waterw_n = 'Water Weather'n*1; 
	Wind_n = Wind*1; Hail_n = Hail*1; Hurricane_n = Hurricane*1;
	array peril (9) Fire_n Theft_n waternw_n othernw_n Lightning_n waterw_n Wind_n Hail_n Hurricane_n;
	array fac_l (9) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l;
	array fac_h (9) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h;
	do i = 1 to 9; 
		fac_l(i) = lag(peril(i));
		fac_h(i) = peril(i);
		if cova_L = 0 then fac_l(i) = fac_h(i);
	end;
	drop i;
	run;
	proc sql noprint;
		select max(cova_H)
		into: maxdedcova
		from deductible_fac
		;
	quit;
	%put &maxdedcova;
/*	data deductible;*/
/*	set in.policy_info (keep=casenumber policynumber coveragea CoverageDeductible WindHailDeductible HurricaneDeductible);*/
/*	AOPDeductible = input(CoverageDeductible,dollar8.);*/
/*	WHDeductible =  input(WindHailDeductible,8.);*/
/*	HurrDeductible =  input(HurricaneDeductible,8.);*/
/*	if AOPDeductible = 250 then AOPDeductible = 500;*/
/*	if 10 < WHDeductible < 500 then WHDeductible = AOPDeductible;*/
/*	if HurrDeductible = . or 10 < HurrDeductible < 500 then HurrDeductible = WHDeductible;*/
/*	run;*/

	data deductible_0;
	set in.policy_info (keep=casenumber policynumber coveragea CoverageDeductible WindHailDeductible HurricaneDeductible);
    run;

	proc sql;
	create table deductible_1 as
	select *
	from deductible_0 d
	inner join ded_group dg
	on d.casenumber = dg.casenumber
	;
	quit;


	data deductible_2;
	set deductible_1;
	AOPDeductible = input(CoverageDeductible,dollar8.);
	WHDeductible =  input(WindHailDeductible,8.);
	HurrDeductible =  input(HurricaneDeductible,8.);
	if AOPDeductible = 250 then AOPDeductible = 500;
/*	if 10 < WHDeductible < 500 then WHDeductible = AOPDeductible;*/
/*	if HurrDeductible = . or 10 < HurrDeductible < 500 then HurrDeductible = WHDeductible;*/
	run;
	
	data deductible;
	 set deductible_2;
	if WHDeductible le 10 then WHDeductible_amt=coverageA*WHDeductible/100;
	else WHDeductible_amt=WHDeductible;

		if Deductible_Group = 1 then do;
	       if WHDeductible_amt ge (0.1*coverageA) and WHDeductible in (10) then do;
	       	    check=1;
		   end;
		   else do;
                check=0;
				Put "ERROR: Windstorm/Hail Min Deductible fail.";
		   end;
		end;
		else if Deductible_Group = 2  then do;
	       if WHDeductible_amt ge (0.05*coverageA) and WHDeductible in (5,10)then do;
	       	    check=1;
		   end;
		   else do;
                check=0;
				Put "ERROR: Windstorm/Hail Min Deductible fail.";
		   end;
		end;
		else if Deductible_Group = 3 then do;
	       if WHDeductible_amt ge max(0.02*coverageA,5000) and 
              WHDeductible in (2,3,4,5,10,5000) then do;
	       	    check=1;
		   end;
		   else do;
                check=0;
				Put "ERROR: Windstorm/Hail Min Deductible fail.";
		   end;
		end;
		else if Deductible_Group = 4 then do;
	       if WHDeductible_amt ge max(0.01*coverageA,2500) and 
              WHDeductible in (1,2,3,4,5,10,2500,5000) then do;
	       	    check=1;
		   end;
		   else do;
                check=0;
				Put "ERROR: Windstorm/Hail Min Deductible fail.";
		   end;
		end;
	run;

	proc sql;
		create table deductible1 as
		select p.*, f1.cova_L, f1.cova_H
			, f1.fire_l, f1.fire_h
			, f1.theft_l, f1.theft_h
			, f1.waternw_l, f1.waternw_h
			, f1.othernw_l, f1.othernw_h
			, f1.lightning_l, f1.lightning_h
			, f1.waterw_l, f1.waterw_h
			, f2.wind_l, f2.wind_h
			, f2.hail_l, f2.hail_h

			, Case when &Hurricane = 1 then f3.hurr_l else 0 end as hurr_l
			, Case when &Hurricane = 1 then f3.hurr_h else 0 end as hurr_h	
	
		from deductible p 
			left join deductible_fac f1 on (p.Coveragea <= f1.cova_H or f1.cova_H = &maxdedcova) 
				and p.Coveragea > f1.cova_L and p.AOPDeductible = f1.F1
			left join deductible_fac f2 on (p.Coveragea <= f2.cova_H or f2.cova_H = &maxdedcova) 
				and p.Coveragea > f2.cova_L and p.WHDeductible = f2.F1
			left join deductible_fac f3 on (p.Coveragea <= f3.cova_H or f3.cova_H = &maxdedcova) 
				and p.Coveragea > f3.cova_L and p.HurrDeductible = f3.F1
		;
	quit;
	data out.ded_fac; set deductible1;
	array fac_l (9) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l;
	array fac_h (9) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h;
	array fac   (9) fire_ded theft_ded waternw_ded othernw_ded lightning_ded waterw_ded wind_ded hail_ded hurr_ded;
	do i = 1 to 9;
		if coveragea > &maxdedcova or coveragea = cova_H then fac(i) = fac_h(i);
		else fac(i) = round((fac_h(i) - fac_l(i))*(coveragea - cova_L)/(cova_H - cova_L) + fac_l(i),.0001);
	end;
	drop i;
	run;

%if &Process = 0 %then %do;
	title 'RULE 406. DEDUCTIBLES';
	proc print data=out.ded_fac;
	where fire_ded*theft_ded*waternw_ded*othernw_ded*lightning_ded*waterw_ded*wind_ded*hail_ded*hurr_ded = .;
	run;
	proc sort data=out.ded_fac; by AOPDeductible; run;
	proc gplot data=out.ded_fac;
		by AOPDeductible;
		plot (fire_ded theft_ded waternw_ded othernw_ded lightning_ded waterw_ded)*coveragea 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sort data=out.ded_fac; by WHDeductible; run;
	proc gplot data=out.ded_fac;
		by WHDeductible;
		plot (wind_ded hail_ded)*coveragea 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
/*	proc sort data=out.ded_fac; by HurrDeductible; run;*/
/*	proc gplot data=out.ded_fac;*/
/*		by HurrDeductible;*/
/*		plot hurr_ded*coveragea */
/*			/ overlay legend vaxis=axis1;*/
/*	run;*/
/*	quit; */
%end;

%mend();
%ded();