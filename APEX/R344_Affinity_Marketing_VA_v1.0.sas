** RULE 344. AFFINITY MARKETING **;
*account numbers for partners are placeholders;
/*The General Account Code: 1910, 1911*/
/*added policyaccountnumbers 1132 and 1920 for Elephant and Horace Mann TM 1/3/16*/
%macro aff();
	data affinity; 
	set in.policy_info (keep=casenumber policynumber policyaccountnumber autopolicynumber);
	if policyaccountnumber in (1020:1026, 1051, 1130, 1131, 1132, 1550:1553, 1610, 1910, 1911, 1920
							 , 2000:2199, 2300:2699, 3300:3899, 4000:4499
							 , 7010, 7030, 8000:8999, 11000, 11510:11520
							 , 14000:14500)
		then partner = 1;
	else partner = 0;
	if autopolicynumber = '' then auto = 0;
	else auto = 1;
	run;


%if %sysfunc(exist(fac.'344# Partner Discounts$'n)) %then %do;

	data affinity_fac;
	set fac.'344# Partner Discounts$'n;
	if f1 = '' then delete;
	run;

%end;
%else %do;
	data affinity_fac;
	set fac.'344# Affinity Marketing$'n;
	if f1 = '' then delete;
	run;
%end;

	proc sql;
		create table out.affinity_fac as
		select p.*,  auto*partner*'Affinity Discount'n as affinity
		from affinity p, affinity_fac f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 344. AFFINITY MARKETING';
	proc freq data=out.affinity_fac;
	tables partner*auto*affinity / list missing;
	run;
%end;
%mend;
%aff();