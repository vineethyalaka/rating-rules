%macro aff();
	data affinity; 
	set in.policy_info (keep=casenumber policynumber policyaccountnumber autopolicynumber);
	if policyaccountnumber in (1020:1024
	                        , 1026
							, 1051
							, 1920:1921
							, 3929
							, 4000:4499
							, 7010
							, 7030
							, 13200:13299
							, 13800:13899
							, 14000:14500
							, 14900:14999
							, 60500:60599)
		then partner = 1;
	else partner = 0;
	if autopolicynumber = '' then auto = 0;
	else auto = 1;
	run;


%if %sysfunc(exist(fac.'344# Partner Discounts$'n)) %then %do;

	data affinity_fac;
	set fac.'344# Partner Discounts$'n;
	if f1 = '' then delete;
	run;

%end;
%else %do;
	data affinity_fac;
	set fac.'344# Affinity Marketing$'n;
	if f1 = '' then delete;
	run;
%end;

/*Format now, all discounts are 10%. Once WF and AMFam account numbers are given, must change rating engine.*/
	proc sql;
		create table out.affinity_fac as
		select p.*, a.partnerflag, 
case 
        when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		/*UWAA*/
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		/*Amfam*/
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51100 and a.PolicyAccountNumber<=51109 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51200 and a.PolicyAccountNumber<=51299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=14700 and a.PolicyAccountNumber<=14799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=60200 and a.PolicyAccountNumber<=60299 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

	
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.10 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51500 and a.PolicyAccountNumber<=51599 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51400 and a.PolicyAccountNumber<=51499 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51300 and a.PolicyAccountNumber<=51399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=51900 and a.PolicyAccountNumber<=51999 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=51700 and a.PolicyAccountNumber<=51799 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.05 /*Auto Partner*/
		when a.PolicyAccountNumber>=52300 and a.PolicyAccountNumber<=52399 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0 /*Affiliate Auto Discounts*/

		when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
		when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0 /*Non-Auto Partner*/
		when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
		when a.PolicyAccountNumber>=15100 and a.PolicyAccountNumber<=15199 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/
		else auto*partner*0.1
/*'Affinity Discount'n*/ end as affinity
		from affinity p inner join in.policy_info a on p.casenumber=a.casenumber/*, affinity_fac f*/;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 344. AFFINITY MARKETING';
	proc freq data=out.affinity_fac;
	tables partner*auto*affinity / list missing;
	run;
%end;
%mend;
%aff();