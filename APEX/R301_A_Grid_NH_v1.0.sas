** RULE 301.A GRID **;

**=====================================================================================**
History: 2015 03 18 JT  Initial draft
		 2015 05 11 SY  Added Hurricane peril indicator and temporary location_id factor table
**=====================================================================================**;

%macro grid();

	data location_id;
	set fac.'301#A Location ID$'n;
	if vtype(F1)='C' then location_id = input(F1,7.);
		else location_id = F1;
	run;

	proc sql;
		create table out.grid_fac as
		select p.casenumber, p.policynumber, /*p.GridID,*/ p.LocationID
			, f.'Water Non-Weather'n as waternw_grid
			, f.'Water Weather'n as waterw_grid
			, f.Wind as wind_grid
			, f.hail as hail_grid
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_grid
			, f.'Liability - Bodily injury (Perso'n as liabipi_grid
			, f.'Liability - Bodily injury (Medic'n as liabimp_grid		
			, f.'Water Back-up'n as waterbu_grid
		from out.territory p left join location_id f 
			on p.LocationID = f.location_id
where p.locationId <>0; 
;
	quit;


%if &Process = 0 %then %do;
	title 'RULE 301.A GRID';
	proc sql;
		select distinct LocationID /*gridID*/
			, waternw_grid
			, waterw_grid
			, wind_grid
			, hail_grid
			, hurr_grid
			, liabipi_grid
			, liabimp_grid
			, waterbu_grid
		from out.grid_fac;
	quit;
%end;
%mend;
%grid();