/* 2017 09 20 KK - Based on R617_Dangerous_Dog_CW_v1.0 and R620_Domestic_Animal_CW_v1.0 from SecondHome*/

/*NOTE: Will need an animal liability buy back flag*/

** RULE 620. DOMESTIC ANIMAL **;
%macro dd();

	data r620fac;
		set fac.'620# Animal Liability$'n;
		if f1="" then delete;
		fac1='Liability - Bodily injury (Perso'n*1;
		fac2='Liability - Bodily injury (Medic'n*1;
		fac3='Liability - Property Damage to O'n*1;
		if f1 = 'Dangerous Dog' then Domestic_flag = 1;
		if f1 = 'All Other Animals' then Domestic_flag = 2;
	run;


	data DomesticAnimal; 
		set in.Policy_Info (keep=casenumber policynumber DogsInHouseholdCount AnimalExclusion DogBreed);
		if  AnimalExclusion = 1 then Domestic_flag = 0;
		else if upcase(DogBreed) not in ("","GERMAN SHEPHERD","NONE OF THE ABOVE") then Domestic_flag = 1;				
		else Domestic_flag = 2; /*combined 617 + 620 factors, so dangerous dog is both and all other is only the 620 factor*/
	run;

	proc sql;
	    create table out.lia_DomesticAnimal_fac as
		select p.*
			, case when f.fac1 = . then 0 else f.fac1 end  as liabipi_DomesticAnimal
			, case when f.fac2 = . then 0 else f.fac2 end  as liabimp_DomesticAnimal
			, case when f.fac3 = . then 0 else f.fac3 end  as liapd_DomesticAnimal
		from DomesticAnimal p
		left join r620fac f
			on p.Domestic_flag = f.Domestic_flag;
	quit;

%mend;
%dd();
