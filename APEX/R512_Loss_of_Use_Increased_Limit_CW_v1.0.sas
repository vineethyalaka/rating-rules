** RULE 512. LOSS OF USE � INCREASED LIMIT **;
%macro lou();
	data r512;
	set fac.'512# Loss of Use Inc Lim$'n;
	if f1 = "" then delete;
	rename 'Coverage Limit Increment (i#e# r'n = Increment;
	run;
	proc sql;
		create table out.loss_of_use_rate as
		select p.*
			, round(p.Incr_CovD*f.Rate/Increment,1.0) as loss_of_use_rate
			, p.Incr_CovD*f.Rate/Increment as loss_of_use_rate_nr
		from coverage_d p, r512 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 512. LOSS OF USE � INCREASED LIMIT';
	proc freq data=out.loss_of_use_rate;
	tables Incr_CovD*loss_of_use_rate / list missing;
	run;
%end;
%mend;
%lou();