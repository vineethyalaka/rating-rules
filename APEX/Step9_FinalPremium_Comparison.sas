
proc sql;
create table premium_comparison as 
select p1.casenumber,
       p1.policynumber,
       p1.premiuminforce as PremiumInForce,
       s.final_premium as SAS_Premium,
	   (p1.premiuminforce-s.final_premium) as Premium_diff
from in.policy_info p1
inner join out.STEP8_FINAL_PREMIUM s
        on p1.casenumber = s.casenumber

;