** RULE 360. CLAIMS FREE DISCOUNT **;

/*Version control*/
/*1.0 - 09 11 2017 JK*/

%macro claim();
%if &Existing = 1 %then %do;

	proc sql;
		create table claim_free_buckets as
		select distinct l.casenumber, l.policynumber, p.PolicyTermYears, p.PolicyEffectiveDate, l.lossdate, 
			l.LossCode, l.LossAmount
			/* treat cat and non-cat the same */
			, case when l.LossCode > 1000 then l.LossCode-1000 else l.LossCode end as code
			, case when calculated code not in(5,8,11,12,24,41) then 1 else 0 end as naogclaim
			/* bucket loss codes into perils */
			, case when calculated code in(7,17,33) 	   		and calculated naogclaim = 1 then 1 else 0 end as fireclaim
			, case when calculated code in(24)      	   		and calculated naogclaim = 1 then 1 else 0 end as windclaim
			, case when calculated code in(18,19,20,53,54) 		and calculated naogclaim = 1 then 1 else 0 end as theftclaim
			, case when (calculated code in(12) or lightning =1) and calculated naogclaim = 1 then 1 else 0 end as lightningclaim
			, case when calculated code in(29) 			   		and calculated naogclaim = 1 then 1 else 0 end as liabilityclaim /**/
			, case when calculated code in(9,59)           		and calculated naogclaim = 1 then 1 else 0 end as waterclaim
			, case when calculated code in(23)            		and calculated naogclaim = 1 then 1 else 0 end as backupclaim
			, case when calculated code in(4,5,6,21,22,25,26,27,32,37,52,58,60,61) 
																and calculated naogclaim = 1 then 1 else 0 end as otherclaim

			, case when calculated liabilityclaim=0 			and calculated naogclaim = 1 then 1 else 0 end as claimexliability
			, case when calculated waterclaim=0  				and calculated naogclaim = 1 then 1 else 0 end as claimexwater
			, case when calculated backupclaim=0 	 	  		and calculated naogclaim = 1 then 1 else 0 end as claimexbackup
			

			, case when calculated naogclaim=1 and calculated fireclaim=0
													 	   then 1 else 0 end as naogexfire
			, case when calculated naogclaim=1 and calculated theftclaim=0 
													 	   then 1 else 0 end as naogextheft
			, case when calculated naogclaim=1 and calculated otherclaim=0 	 	   
														   then 1 else 0 end as naogexother


			, case when calculated naogclaim=1 and calculated lightningclaim=0
													 	   then 1 else 0 end as naogexlightning
			, case when calculated naogclaim=1 and calculated windclaim=0 
													 	   then 1 else 0 end as naogexwind




from in.loss_info_all l 
			inner join in.policy_info p on l.casenumber=p.casenumber
		where l.LossAmount >0
			and (p.PolicyTermYears=1 and &effst = 0 and (p.PolicyIssueDate - l.LossDate)/365 <3
			or  p.PolicyTermYears=1 and &effst ^= 0 and (p.PolicyEffectiveDate - l.LossDate)/365 <3
			or  p.PolicyTermYears^=1 and (p.PolicyEffectiveDate - l.LossDate - 60)/365 <3);
	quit;
	proc summary data=claim_free_buckets;
	class casenumber;
	var fireclaim windclaim theftclaim lightningclaim liabilityclaim waterclaim backupclaim otherclaim
		claimexliability claimexwater claimexbackup naogclaim naogexfire naogextheft naogexother;
	output out=claim_free_sums sum()=;
	run;

	proc sort data=in.policy_info out=policy_sort; by casenumber; run;
	proc sort data=claim_free_sums; by casenumber; run;
	data out.claim_free_buckets;
	merge policy_sort (in=a) claim_free_sums (in=b drop=_type_ _freq_);
	by casenumber;
	if a and not b then do;
		fireclaim=0; windclaim=0; theftclaim=0; lightningclaim=0; liabilityclaim=0; 
		waterclaim=0; backupclaim=0; otherclaim=0; claimexliability=0; claimexwater=0; 
		claimexbackup=0; naogclaim=0; naogexfire=0; naogextheft=0; naogexother=0; naogexlightning=0; naogexwind=0;
		end;
	if 	fireclaim > 5 		then fireclaim = 5; 
	if 	windclaim > 5 		then windclaim = 5; 
	if 	theftclaim > 5 		then theftclaim = 5; 
	if 	lightningclaim > 5 	then lightningclaim = 5; 
	if 	liabilityclaim > 5 	then liabilityclaim = 5; 
	if 	waterclaim > 5 		then waterclaim = 5; 
	if 	backupclaim > 5 	then backupclaim = 5; 
	if 	otherclaim > 5 		then otherclaim = 5; 
	if 	claimexliability>10 then claimexliability = 10; 
	if 	claimexwater > 10 	then claimexwater = 10; 
	if 	claimexbackup > 10 	then claimexbackup = 10; 
	if 	naogclaim > 10 		then naogclaim = 10; 
	if 	naogexfire > 10 	then naogexfire = 10; 
	if 	naogextheft > 10 	then naogextheft = 10; 
	if 	naogexother > 10 	then naogexother = 10; 
	if 	naogexlightning > 10 	then naogexlightning = 10; 
	if 	naogexwind > 10 	then naogexwind = 10; 



if not a then delete;
	run;



** RULE 360.A NAOG CLAIMS FREE XPERIL **;
	data priorclaimfreeNAOGxPerilfac;
	set fac.'360#A NAOG Claims Free xPeril$'n;
	naogclaimxPeril = f1;
	run;
proc sql;
create table work.priorclaimfree_NAOG_xPeril_fac1 as
		select p.casenumber, p.policynumber, 
		case when p.naogexfire = 0 then 'Yes' else 'No' end as naogexfirefree,
		case when p.naogextheft = 0 then "Yes" else "No" end as naogextheftfree, 
		case when p.naogexother = 0 then "Yes" else "No" end as naogexotherfree,
		case when p.naogexlightning = 0 then "Yes" else "No" end as naogexlightningfree, 
		case when p.naogexwind = 0 then "Yes" else "No" end as naogexwindfree, 
		case when p.naogexother = 0 then "Yes" else "No" end as naogexotherfree,
		case when p.claimexliability = 0 then "Yes" else "No" end as naogexliabfree,
		%if &Hurricane = 1 %then "Yes"; %else "No"; as naogfreexhurr
		from out.claim_free_buckets p;
		quit;

	proc sql;
		create table out.priorclaimfree_NAOG_xPeril_fac as
		select p.casenumber, p.policynumber, p.naogexfirefree, p.naogextheftfree, p.naogexotherfree,
		p.naogexlightningfree, p.naogexwindfree, p.naogexotherfree, p.naogexliabfree, p.naogfreexhurr

			, case when naogclaim <> 0 then 1 else f1.fire end as fire_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f2.theft end as theft_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f3.'All Other Non-Weather'n end as othernw_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f4.lightning end as lightning_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f5.wind end as wind_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f6.'Liability - Bodily injury (Perso'n end as naogclaimfreexliabpi
			, case when naogclaim <> 0 then 1 else f7.'Liability - Bodily injury (Medic'n end as naogclaimfreexliabmp
			, case when naogclaim <> 0 then 1 else f8.hurricane end as hurr_naogclaimfreexPeril

		from work.priorclaimfree_NAOG_xPeril_fac1 p left join priorclaimfreeNAOGxPerilfac f1
			on naogexfirefree=f1.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f2
			on naogextheftfree=f2.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f3
			on naogexotherfree=f3.naogclaimxPeril
            left join priorclaimfreeNAOGxPerilfac f4
			on naogexlightningfree=f4.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f5
			on naogexwindfree=f5.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f6
			on naogexotherfree=f6.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f7
			on naogexliabfree=f7.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f8
			on naogfreexhurr=f8.naogclaimxPeril;

	quit;



** RULE 360.B PERIL CLAIMS **;
data priorclaimfreePerilfac;
	set fac.'360#B Peril Claims Free$'n;
	Perilclaim = f1;
	run;

proc sql;
create table work.priorclaimfree_Peril_fac1 as
		select p.casenumber, p.policynumber, 
		case when p.fireclaim = 0 then "Yes" else "No" end as fireclaimfree, 
		case when p.theftclaim = 0 then "Yes" else "No" end as theftclaimfree,
        case when p.otherclaim = 0 then "Yes" else "No" end as otherclaimfree,
		case when p.liabilityclaim = 0 then "Yes" else "No" end as liabilityclaimfree
		from out.claim_free_buckets p;
		quit;

	proc sql;
		create table out.priorclaimfree_Peril_fac as
		select p.casenumber, p.policynumber, p.fireclaimfree, p.theftclaimfree, p.otherclaimfree, p.liabilityclaimfree
			, f1.fire as fire_claimfree		
			, f3.theft as theft_claimfree
			, f4.'All Other Non-Weather'n as othernw_claimfree
			, f2.'Liability - Property Damage to O'n as liapd_claimfree
		from work.priorclaimfree_Peril_fac1 p left join priorclaimfreePerilfac f1
			on fireclaimfree =f1.Perilclaim
			left join priorclaimfreePerilfac f2
			on liabilityclaimfree=f2.Perilclaim
			left join priorclaimfreePerilfac f3
			on theftclaimfree=f3.Perilclaim
			left join priorclaimfreePerilfac f4
			on otherclaimfree=f4.Perilclaim
		;
		quit;



%end;

%if &Existing = 0 %then %do;

	proc sql;
		create table claim_free_buckets as
		select distinct l.casenumber, l.policynumber, p.PolicyTermYears, p.PolicyEffectiveDate, l.lossdate, 
			l.LossCode, l.LossAmount
			/* treat cat and non-cat the same */
			, case when l.LossCode > 1000 then l.LossCode-1000 else l.LossCode end as code
			, case when calculated code not in(5,8,11,12,24,41) then 1 else 0 end as naogclaim
			/* bucket loss codes into perils */
			, case when calculated code in(7,17,33) 	   		and calculated naogclaim = 1 then 1 else 0 end as fireclaim
			, case when calculated code in(24)      	   		and calculated naogclaim = 1 then 1 else 0 end as windclaim
			, case when calculated code in(18,19,20,53,54) 		and calculated naogclaim = 1 then 1 else 0 end as theftclaim
			, case when (calculated code in(12) /*or lightning =1*/) and calculated naogclaim = 1 then 1 else 0 end as lightningclaim
			, case when calculated code in(29) 			   		and calculated naogclaim = 1 then 1 else 0 end as liabilityclaim /**/
			, case when calculated code in(9,59)           		and calculated naogclaim = 1 then 1 else 0 end as waterclaim
			, case when calculated code in(23)            		and calculated naogclaim = 1 then 1 else 0 end as backupclaim
			, case when calculated code in(4,5,6,21,22,25,26,27,32,37,52,58,60,61) 
																and calculated naogclaim = 1 then 1 else 0 end as otherclaim

			, case when calculated liabilityclaim=0 			and calculated naogclaim = 1 then 1 else 0 end as claimexliability
			, case when calculated waterclaim=0  				and calculated naogclaim = 1 then 1 else 0 end as claimexwater
			, case when calculated backupclaim=0 	 	  		and calculated naogclaim = 1 then 1 else 0 end as claimexbackup
			

			, case when calculated naogclaim=1 and calculated fireclaim=0
													 	   then 1 else 0 end as naogexfire
			, case when calculated naogclaim=1 and calculated theftclaim=0 
													 	   then 1 else 0 end as naogextheft
			, case when calculated naogclaim=1 and calculated otherclaim=0 	 	   
														   then 1 else 0 end as naogexother


			, case when calculated naogclaim=1 and calculated lightningclaim=0
													 	   then 1 else 0 end as naogexlightning
			, case when calculated naogclaim=1 and calculated windclaim=0 
													 	   then 1 else 0 end as naogexwind




from in.loss_info l 
			inner join in.policy_info p on l.casenumber=p.casenumber
		where l.LossAmount >0
			and (p.PolicyTermYears=1 and &effst = 0 and (p.PolicyIssueDate - l.LossDate)/365 <3
			or  p.PolicyTermYears=1 and &effst ^= 0 and (p.PolicyEffectiveDate - l.LossDate)/365 <3
			or  p.PolicyTermYears^=1 and (p.PolicyEffectiveDate - l.LossDate - 60)/365 <3);
	quit;
	proc summary data=claim_free_buckets;
	class casenumber;
	var fireclaim windclaim theftclaim lightningclaim liabilityclaim waterclaim backupclaim otherclaim
		claimexliability claimexwater claimexbackup naogclaim naogexfire naogextheft naogexother naogexlightning naogexwind;
	output out=claim_free_sums sum()=;
	run;

	proc sort data=in.policy_info out=policy_sort; by casenumber; run;
	proc sort data=claim_free_sums; by casenumber; run;
	data out.claim_free_buckets;
	merge policy_sort (in=a) claim_free_sums (in=b drop=_type_ _freq_);
	by casenumber;
	if a and not b then do;
		fireclaim=0; windclaim=0; theftclaim=0; lightningclaim=0; liabilityclaim=0; 
		waterclaim=0; backupclaim=0; otherclaim=0; claimexliability=0; claimexwater=0; 
		claimexbackup=0; naogclaim=0; naogexfire=0; naogextheft=0; naogexother=0; naogexlightning=0; naogexwind=0;
		end;
	if 	fireclaim > 5 		then fireclaim = 5; 
	if 	windclaim > 5 		then windclaim = 5; 
	if 	theftclaim > 5 		then theftclaim = 5; 
	if 	lightningclaim > 5 	then lightningclaim = 5; 
	if 	liabilityclaim > 5 	then liabilityclaim = 5; 
	if 	waterclaim > 5 		then waterclaim = 5; 
	if 	backupclaim > 5 	then backupclaim = 5; 
	if 	otherclaim > 5 		then otherclaim = 5; 
	if 	claimexliability>10 then claimexliability = 10; 
	if 	claimexwater > 10 	then claimexwater = 10; 
	if 	claimexbackup > 10 	then claimexbackup = 10; 
	if 	naogclaim > 10 		then naogclaim = 10; 
	if 	naogexfire > 10 	then naogexfire = 10; 
	if 	naogextheft > 10 	then naogextheft = 10; 
	if 	naogexother > 10 	then naogexother = 10; 
	if 	naogexlightning > 10 	then naogexlightning = 10; 
	if 	naogexwind > 10 	then naogexwind = 10; 



if not a then delete;
	run;



** RULE 360.A NAOG CLAIMS FREE XPERIL **;
	data priorclaimfreeNAOGxPerilfac;
	set fac.'360#A NAOG Claims Free xPeril$'n;
	naogclaimxPeril = f1;
	run;
proc sql;
create table work.priorclaimfree_NAOG_xPeril_fac1 as
		select p.casenumber, p.policynumber, naogclaim,
		case when p.naogexfire = 0 then 'Yes' else 'No' end as naogexfirefree,
		case when p.naogextheft = 0 then "Yes" else "No" end as naogextheftfree, 
		case when p.naogexother = 0 then "Yes" else "No" end as naogexotherfree,
		case when p.naogexlightning = 0 then "Yes" else "No" end as naogexlightningfree, 
		case when p.naogexwind = 0 then "Yes" else "No" end as naogexwindfree, 
		case when p.naogexother = 0 then "Yes" else "No" end as naogexotherfree,
		case when p.claimexliability = 0 then "Yes" else "No" end as naogexliabfree,
		%if &Hurricane = 1 %then "Yes"; %else "No"; as naogfreexhurr
		from out.claim_free_buckets p;
		quit;

	proc sql;
		create table out.priorclaimfree_NAOG_xPeril_fac as
		select p.casenumber, p.policynumber, p.naogexfirefree, p.naogextheftfree, p.naogexotherfree,
		p.naogexlightningfree, p.naogexwindfree, p.naogexotherfree, p.naogexliabfree, p.naogfreexhurr

			, case when naogclaim <> 0 then 1 else f1.fire end as fire_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f2.theft end as theft_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f3.'All Other Non-Weather'n end as othernw_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f4.lightning end as lightning_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f5.wind end as wind_naogclaimfreexPeril
			, case when naogclaim <> 0 then 1 else f6.'Liability - Bodily injury (Perso'n end as naogclaimfreexliabpi
			, case when naogclaim <> 0 then 1 else f7.'Liability - Bodily injury (Medic'n end as naogclaimfreexliabmp
			, case when naogclaim <> 0 then 1 else f8.hurricane end as hurr_naogclaimfreexPeril
		from work.priorclaimfree_NAOG_xPeril_fac1 p left join priorclaimfreeNAOGxPerilfac f1
			on naogexfirefree=f1.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f2
			on naogextheftfree=f2.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f3
			on naogexotherfree=f3.naogclaimxPeril
            left join priorclaimfreeNAOGxPerilfac f4
			on naogexlightningfree=f4.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f5
			on naogexwindfree=f5.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f6
			on naogexotherfree=f6.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f7
			on naogexliabfree=f7.naogclaimxPeril
			left join priorclaimfreeNAOGxPerilfac f8
			on naogfreexhurr=f8.naogclaimxPeril;

	quit;



** RULE 360.B PERIL CLAIMS **;
	data priorclaimfreePerilfac;
	set fac.'360#B Peril Claims Free$'n;
	Perilclaim = f1;
	run;

proc sql;
create table work.priorclaimfree_Peril_fac1 as
		select p.casenumber, p.policynumber, 
		case when p.fireclaim = 0 then "Yes" else "No" end as fireclaimfree, 
		case when p.theftclaim = 0 then "Yes" else "No" end as theftclaimfree,
        case when p.otherclaim = 0 then "Yes" else "No" end as otherclaimfree,
		case when p.liabilityclaim = 0 then "Yes" else "No" end as liabilityclaimfree
		from out.claim_free_buckets p;
		quit;

	proc sql;
		create table out.priorclaimfree_Peril_fac as
		select p.casenumber, p.policynumber, p.fireclaimfree, p.theftclaimfree, p.otherclaimfree, p.liabilityclaimfree
			, f1.fire as fire_claimfree		
			, f3.theft as theft_claimfree
			, f4.'All Other Non-Weather'n as othernw_claimfree
			, f2.'Liability - Property Damage to O'n as liapd_claimfree
		from work.priorclaimfree_Peril_fac1 p left join priorclaimfreePerilfac f1
			on fireclaimfree =f1.Perilclaim
			left join priorclaimfreePerilfac f2
			on liabilityclaimfree=f2.Perilclaim
			left join priorclaimfreePerilfac f3
			on theftclaimfree=f3.Perilclaim
			left join priorclaimfreePerilfac f4
			on otherclaimfree=f4.Perilclaim
		;
		quit;



%end;


%if &Process = 0 %then %do;
	title 'RULE 360.A NAOG CLAIMS FREE XPERIL';
	proc freq data=out.priorclaimfree_NAOG_xPeril_fac;
	tables naogexfirefree*fire_naogclaimfreexPeril
		   naogextheftfree*theft_naogclaimfreexPeril
		   naogexotherfree*othernw_naogclaimfreexPeril
		   naogexlightningfree*lightning_naogclaimfreexPeril
		   naogexwindfree*wind_naogclaimfreexPeril
		   naogexliabfree*naogclaimfreexliabpi
		   naogexliabfree*naogclaimfreexliabmp
		   naogfreexhurr*hurr_naogclaimfreexPeril/ list missing;
	run;
%end;


%if &Process = 0 %then %do;
	title 'RULE 360.B PERIL CLAIMS';
	proc freq data=out.priorclaim_Peril_fac;
	tables fireclaimfree*fire_claimfree
		theftclaimfree*theft_claimfree
		otherclaimfree*othernw_claimfree
		liabilityclaimfree*liapd_claimfree / list missing;
	run;
%end;



%mend;
%claim();