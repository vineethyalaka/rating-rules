** RULE 401. ADDITIONAL AMOUNT OF INSURANCE **;
%macro add();
	data r401;
	set fac.'401# Addl Amount of Insurance$'n;
	pct='Additional Amount of Insurance'n*100;
	fac=factor*1;
	run;
	proc sql;
		create table out.r401_add_amt_ins as
		select p.casenumber, p.policynumber, p.HO_0420_flag, p.HO_04_20_pct
			, case when p.HO_0420_FLAG = '1' /*and b.AdvInd='N'*/ then f.fac else 1 end as r401_HO_04_20_fac
		from in.Endorsement_Info p left join r401 f
			on p.HO_04_20_pct=f.pct;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 401. ADDITIONAL AMOUNT OF INSURANCE';
	proc freq data=out.r401_add_amt_ins;
	tables HO_04_20_pct*r401_HO_04_20_fac / list missing;
	run;
%end;
%mend;
%add();
