/*Step 3 Supplement*/
/* ITR 14558 - Vamsi - Added waternw_sec_res, lightning_sec_res, lightning_naogclaim, lightning_claim 
					   which were missing from old file and multipled factors from new rules */
%macro sup();
	proc sql;
		create table out.Step3Supplement as
		select casenumber
		, fire_base_rate * fire_census * fire_cova * fire_home_age * fire_num_res * fire_heat_sys * fire_ins_score * fire_rowhouse * fire_naogclaimxPeril * fire_claim * fire_ded * (fire_sp_personal_prop-1) as SpecialPersonalProp_Fire
		, Theft_base_rate * theft_county * Theft_census * theft_covcbase * theft_cust_age * Theft_ins_score * theft_length_res * theft_num_res * theft_num_units * theft_pool * theft_sqft * theft_naogclaimxPeril * theft_claim * theft_ded * (theft_sp_personal_prop-1) as SpecialPersonalProp_Theft
		, waternw_base_rate * waternw_grid * waternw_census * waternw_cova * waternw_fin_bsmnt * waternw_home_age * waternw_ins_score * waternw_length_res * waternw_num_res * waternw_bsmnt * waternw_pool * waternw_rcsqft * waternw_bathroom *waternw_rowhouse * waternw_sec_res * waternw_claimxPeril * waternw_claim * waternw_found_type_yr_built * waternw_num_stories * waternw_ded * (waternw_sp_personal_prop-1) as SpecialPersonalProp_WaterNW
		, othernw_base_rate * othernw_cova * othernw_cust_age * othernw_home_age * othernw_ins_score * othernw_pool * othernw_naogclaim * othernw_claim * othernw_ded * (othernw_sp_personal_prop-1) as SpecialPersonalProp_OtherNW
		, lightning_base_rate * lightning_census * lightning_cova * lightning_ins_score * lightning_pool * lightning_rcsqft * lightning_sec_res * lightning_naogclaim * lightning_claim * lightning_ded * (lightning_sp_personal_prop-1) as SpecialPersonalProp_Lightning
		, round(sum(calculated SpecialPersonalProp_Fire, calculated SpecialPersonalProp_Theft, calculated SpecialPersonalProp_WaterNW, calculated SpecialPersonalProp_OtherNW, calculated SpecialPersonalProp_Lightning),1.0) as SpecialPersonalProp
		from out.step2_base_premium
		;
	quit;

	proc sql;
		create table out.step3_subtotal_premium as
		select a.*
		, b.SpecialPersonalProp
		, round(a.fire_disc_base_premium
						+ a.theft_disc_base_premium
						+ a.waternw_disc_base_premium
						+ a.othernw_disc_base_premium
						+ a.lightning_disc_base_premium
						+ a.waterw_disc_base_premium
						+ a.wind_disc_base_premium
						+ a.hail_disc_base_premium
/*						+ hurr_disc_base_premium*/
						,.0001) as subtotal_premium
		
		from out.step2_base_premium as a
		inner join out.Step3Supplement as b
		on a.CaseNumber = b.CaseNumber
		;
	quit;

%if &Process = 0 %then %do;
	title 'STEP 3: SUBTOTAL PREMIUM (RULE 301.A.3)';
	proc print data=out.step3_subtotal_premium;
	where subtotal_premium = .;
	run;
	proc sql;
		select mean(fire_disc_base_premium) as fire_disc_base_prem_avg
			, mean(theft_disc_base_premium) as theft_disc_base_prem_avg
			, mean(waternw_disc_base_premium) as waternw_disc_base_prem_avg
			, mean(othernw_disc_base_premium) as othernw_disc_base_prem_avg
			, mean(lightning_disc_base_premium) as lightning_disc_base_prem_avg
			, mean(waterw_disc_base_premium) as waterw_disc_base_prem_avg
			, mean(wind_disc_base_premium) as wind_disc_base_prem_avg
			, mean(hail_disc_base_premium) as hail_disc_base_prem_avg
/*			, mean(hurr_disc_base_premium) as hurr_disc_base_prem_avg*/
			, mean(subtotal_premium) as subtotal_premium_avg
		from out.step3_subtotal_premium;
	quit;
%end;
%mend;
%sup();