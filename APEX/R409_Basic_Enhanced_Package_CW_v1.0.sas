** RULE 409. BASIC AND ENHANCED PACKAGE OPTIONAL ENDORSEMENTS **;
%macro pack();
	proc sql;
		create table r409 as
		select p.casenumber, p.policynumber, pd.Advfactor
		,  case when pd.Advfactor = 1 then 0.1
				when pd.Advfactor = 2 then 0.15
				else 0 end as Package_Fac
		from in.policy_info p
		left join in.production_info pd on p.casenumber = pd.casenumber
		;
		quit;

	proc sql;
		create table out.package_discount as
		select p.casenumber, p.policynumber, fa.AdvFactor, fa.Package_Fac
		, case when AdvFactor = 1 then 
					round((-(a.r401_HO_04_20_Prem
					+a.r403_HO_04_90_Prem
					+b.SpecialPersonalProp
					+c.incr_covc_base_premium
					+d.Jewelry
					+e.Refrigerated_Prop_rate
					+f.waterbu_base_premium))
					*fa.Package_Fac,1.0)
				when AdvFactor=2	then
					round((-(a.r401_HO_04_20_Prem
					+a.r403_HO_04_90_Prem
					+a.r404_Ordinance_or_Law_prem
					+b.SpecialPersonalProp
					+c.incr_covc_base_premium
					+d.Jewelry
					+e.Refrigerated_Prop_rate
					+f.waterbu_base_premium))
					*fa.Package_Fac,1.0)
				else 0 end as Package_Discount

		from in.policy_info p
		left join r409 fa 								on p.casenumber = fa.casenumber
		left join out.step4_Adjusted_Subtotal_premium a on p.casenumber = a.casenumber
		left join out.step3supplement b					on p.casenumber = b.casenumber
		left join out.incr_covc_base_premium c			on p.casenumber = c.casenumber
		left join out.USPP_rate d						on p.casenumber = d.casenumber
		left join out.Refrigerated_Property e			on p.casenumber = e.casenumber
		left join out.waterbu_base_premium f			on p.casenumber = f.casenumber

		;
	quit;
%mend;
%pack();