** RULE 338. Underwriting Tier**;
**=====================================================================================**
History: 2017 07 26 BF v1.0 Initial draft for APEX 2.0
		 2018 05 16 JL v1.1 Correcting logic
**=====================================================================================**;
%macro adj();

	data R339;
	set fac.'339# Rate Adjustment$'n;
	if F1='Rate Adjustment Factor' then delete;
	if F1=. then delete;
	Factor=input(F1,5.3);
	run;

	proc sql;
		create table out.rate_adjustment as
		select p.casenumber, p.policynumber, f.Factor as rate_adjustment
		from in.policy_info p, R339 f;
	quit;

%mend;
%adj();
