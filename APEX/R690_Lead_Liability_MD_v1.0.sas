** RULE 690. Lead Liability **;
%macro LLE();
	proc sql;
		create table out.lia_LeadLiabilityExcl_Credit as
		select t1.casenumber, t1.policynumber, t1.propertyunitsinbuilding, t2.LeadLiability_Coverage_Flag, t2.LeadLiability_Exclusion_Flag
			, case when t2.LeadLiability_Exclusion_Flag = 0 then 0 /*No exclusion, so no credit */
				   when t2.LeadLiability_Coverage_Flag = 1 then 0 /*Exclusion on policy but has the buyback, so no credit */
				   else f.Credit
			  end as lead_excl_Credit
		from in.policy_info t1 
		inner join in.endorsement_info t2
			on t1.casenumber = t2.casenumber
		left join fac.'690# Lead Liability$'n f
			on t1.propertyunitsinbuilding=f.'Number of Units in Building'n;
	quit;

%mend;
%LLE();