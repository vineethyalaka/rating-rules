/*2019/12/6 TM Rule 382 initial draft*/

/*Get PB xml data for each case from database*/
proc sql;
create table pb_xml_table as select p.policynumber, p.casenumber, pbxml.xml_data from in.policy_info p
left join &server..HS_POLICY_GEOSPATIAL_XMLDATA pbxml
on p.GEOCODE_POINTER = pbxml.XML_GeoCodePointer;
quit;

/*Use V3RatingRequestXMLData for test code purpose, because currently HS_POLICY_GEOSPATIAL_XMLDATA has no xml sample. Delete later*/
proc sql;
create table test_xml_table as select * from &server.."V3RatingRequestXMLData"n;
quit;

/*Parse xml to target values*/
data xml_parsed;
set test_xml_table;
pos_start = find(xmlkeyvalue, '<Fs1DepartmentType>')+length('<Fs1DepartmentType>');
pos_end = find (xmlkeyvalue, '</Fs1DepartmentType>');
FireDepartmentType = substr(xmlkeyvalue, pos_start, pos_end-pos_start); 
run;

/*Edit Fire department type to match with rate sheet*/
Proc SQL;
create table policy_fire_dpt_type as 
select *,
case when x.FireDepartmentType ='Volunteer' then 'Volunteer' else 'All Other' end as FireDepartmentType_edit
from xml_parsed x;
quit;

/*Modify Rate sheet*/
Data Fire_Dpt_fac;
set fac.'382# Fire Department Type$'n;
if fire=. then delete;
run;

/*Find factor for each case*/
Proc SQL;
create table Fire_Dpt_Type as
select *
from policy_fire_dpt_type p
inner join Fire_Dpt_fac f
on p.FireDepartmentType_edit = f.f1;



/*If calling store proceure to read values*/
/*%macro create_temp_table;*/
/*IF OBJECT_ID(N%str(%')server..##extracted_xml_data%str(%')) IS NOT NULL*/
/*	BEGIN*/
/*		DROP TABLE ##extracted_xml_data;*/
/*	END*/
/**/
/*CREATE TABLE server..##extracted_xml_data*/
/*(*/
/*					CaseNumber		  INT*/
/*				   ,PB_Key			  VARCHAR(20)*/
/*				   ,FS1DepartmentType VARCHAR(15)*/
/*				   ,FS1DriveDistance  DECIMAL(11, 5)*/
/*				   ,NearestWaterBody  DECIMAL(11, 5)*/
/*);*/
/*%mend create_temp_table;*/
/**/
/*%MACRO get_xml_values;*/
/**/
/*/*Create a temp table*/*/
/*proc sql;*/
/*&odbc_string;*/
/*&odbc_string2*/
/*(*/
/*    %create_temp_table;*/
/*/*	%myQuery_b;*/*/
/*/*	%myQuery;*/*/
/*  );     */
/**/
/*quit;*/
/**/
/**/
/*/*get casenumber*/*/;
/*proc sql;*/
/*create table test_xml_table as select * from &server.."V3RatingRequestXMLData"n;*/
/*select count(*) into: casecount from xml_table;*/
/*quit;*/
/**/
/*data xml_table_id;*/
/*set xml_table;*/
/*id = _n_;*/
/*run;*/
/**/
/*/*call stored procedure to add value of each case to temp table ##extracted_xml_data */*/
/*proc sql;*/
/*&odbc_string;*/
/*%do i=1 %to &casecount;*/
/*select casenumber into: casenumber from xml_table_id where id= &i;*/
/*execute (dbo.HSSP_RatingValidation_ConsumeXMLData @intCaseNumber = &casenumber) by odbc;*/
/*%end;*/
/**/
/*quit;*/
/*proc sql;*/
/*create table R382_Fire_Dpt as */
/*select * from &server.."##extracted_xml_data"n;*/
/**/
/*quit;*/
/**/
/*%mend get_xml_values;*/
/*%get_xml_values;*/;
