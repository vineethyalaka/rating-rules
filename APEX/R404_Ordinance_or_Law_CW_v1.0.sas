** RULE 404. ORDINANCE OR LAW **;
%macro or();
	data r404;
	set fac.'404# Ordinance or Law$'n;
	if factor = . then delete;
	run;
	proc sql;
	    create table out.r404_Ordinance_or_Law as
		select p.CaseNumber, p.PolicyNumber, p.ORD_LAW_FLAG
			, case when p.ORD_LAW_FLAG = '1' then f.Factor else 1 end as r404_Ordinance_or_Law_fac
		from in.Endorsement_Info p, r404 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 404. ORDINANCE OR LAW';
	proc freq data=out.r404_Ordinance_or_Law;
	tables ORD_LAW_FLAG*r404_Ordinance_or_Law_fac / list missing;
	run;
%end;
%mend;
%or();