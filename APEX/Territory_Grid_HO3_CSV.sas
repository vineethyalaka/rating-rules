proc sql;
create table out.territory as
select 
p.policynumber,
p.casenumber,
p2.GeoFipsCode as countyid,
p2.GeoCBGroupID2010 as census_block_group_id,
m.gridid,
m.locationid
from in.policy_info as p
inner join &server..DH0200BP as p1
on p.casenumber=p1.rating_case_number
inner join &server..PolicyGeoSpatialData as p2
on p1.GEOCODE_POINTER=p2.geocodepointer
inner join work.Grid_HO3_CSV as m
on p2.GeoGridId=m.N_ITindx;
quit;