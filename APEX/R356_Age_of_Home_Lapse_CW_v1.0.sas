** RULE 356. AGE OF HOME BY LAPSE **;
%macro agelapse();
	data exp_home_age_x_lapse_fac (keep=home_age lapse factor);
	set fac.'356# Age of Home by Lapse$'n;
	if f3=. then delete;
	home_age = f1; if f1 = . then home_age = 101;
	lapse=0; factor = 'Expense Fee'n; output;
	lapse=1; factor = f3; output;
	run;
	proc sql;
		create table out.exp_home_age_x_lapse_fac as
		select p1.casenumber, p1.policynumber, p1.PolicyIssueDate, p1.PolicyEffectiveDate, p1.PropertyYearBuilt, p1.home_age
			, p2.lapse, f.factor as exp_home_age_x_lapse_fac
		from home_age p1 
			inner join lapse p2	on p1.casenumber = p2.casenumber
			left join exp_home_age_x_lapse_fac f on f.home_age = p1.home_age and f.lapse = p2.lapse;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 356. AGE OF HOME BY LAPSE';
	proc print data=out.exp_home_age_x_lapse_fac;
	where exp_home_age_x_lapse_fac = .;
	run;
	proc sort data=out.exp_home_age_x_lapse_fac; by lapse; run;
	proc gplot data=out.exp_home_age_x_lapse_fac; 
		plot exp_home_age_x_lapse_fac*home_age 
			/ overlay legend vaxis=axis1;
	by lapse;
	run;
	quit;
%end;
%mend;
%agelapse();