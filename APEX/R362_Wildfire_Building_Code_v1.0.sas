** RULE 362. Wildfire Building Code **;
** 17DEC2019 Yu Fang wildfire building code **;

%macro building_code();
	proc sql;
		create table out.wildfire_building_code as
		select p.casenumber, p.policynumber, t.census_block_group_ID, p.PropertyYearBuilt
			, f.No , f.Yes /* 'No' means before 2008, 'Yes' means on or after 2008 */
			, case when p.PropertyYearBuilt < 2008 THEN f.No ELSE f.Yes END AS building_code
		from in.policy_info p left join out.territory t
			on p.casenumber = t.casenumber
			left join fac.'362# Wildfire Building Code$'n f 
			on t.census_block_group_ID = f.F1;
	quit;
%mend;
%building_code();