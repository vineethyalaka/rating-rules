/* 2017 08 08 KK - Removed Liability factors as in SecondHome*/

** RULE 320. PRESENCE OF POOL **;
%macro POP();
	proc sql;
		create table out.pool_fac as
		select p.casenumber, p.policynumber, p.poolpresent
			, input(f.theft, 8.3) as theft_pool
			, input(f.'Water Non-Weather'n, 8.3) as waternw_pool
			, input(f.'All Other Non-Weather'n, 8.3) as othernw_pool
			, input(f.lightning, 8.3) as lightning_pool
			/*, input(f.'Liability - Bodily injury (Perso'n, 8.3) as liabipi_pool*/
			/*, input(f.'Liability - Bodily injury (Medic'n, 8.3) as liabimp_pool*/
		from in.policy_info p left join fac.'320# Presence of Pool$'n f
			on p.poolpresent=substr(f1,1,1);
	quit;

%if &Process = 0 %then %do;
	title 'RULE 320. PRESENCE OF POOL';
	proc freq data=out.pool_fac;
	tables poolpresent
		*theft_pool
		*waternw_pool
		*othernw_pool
		*lightning_pool 
/*		*liabipi_pool*/
/*		*liabimp_pool*// list missing;
	run;
%end;
%mend;
%POP();
