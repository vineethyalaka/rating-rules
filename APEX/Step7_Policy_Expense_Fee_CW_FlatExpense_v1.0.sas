** POLICY EXPENSE FEE **
** Updated for MA to remove insurance score and set as a flat Expense Fee **;
%macro fee();
	proc sql;
		create table out.step7_policy_expense_fee as
		select casenumber, policynumber, expense_base_rate as policy_expense_fee
		from out.base_rate
	quit;

%if &Process = 0 %then %do;
	title 'STEP 7: POLICY EXPENSE FEE (RULE 301.A.7)';
	proc print data=out.step7_policy_expense_fee;
	where policy_expense_fee = .;
	run;
	proc sql;
		create table out.average_policy_expense_fee as
		select mean(policy_expense_fee) as policy_expense_fee_avg
		from out.step7_policy_expense_fee;
	quit;
%end;
%mend;
%fee();