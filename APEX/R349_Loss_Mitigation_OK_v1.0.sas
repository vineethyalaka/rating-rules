** RULE 349. Loss Mitigation Discounts **;
** For OK ONLY **
**=====================================================================================**
History: 2019 01 18 TM Initial Draft
**=====================================================================================*;
%macro LM();
data r349_loss_mitigation_fac;
	set fac.'349# Loss Mitigation Discount$'n;
	if f4 =. then delete;
	drop f1 f2;
	wind = input(f3,5.);
	hail =input(f4,5.);
    drop f3 f4;
run;

proc sql;
	create table out.loss_mitigation_fac as
	select p.CaseNUmber, p.PolicyNumber,
    case when p.construction_completed = 0 then 1  else f.wind end as wind_loss_mitigation,
	case when p.construction_completed = 0 then 1  else f.hail end as hail_loss_mitigation

	from in.production_info p, r349_loss_mitigation_fac f
	;
quit;


%mend;
%LM();