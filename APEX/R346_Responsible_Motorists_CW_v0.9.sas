

LIBNAME sqlsrvqa oledb init_string = 
"Provider=SQLOLEDB.1; Integrated Security=SSPI; Persist Security Info=False;
Initial Catalog=ISO_PROD; read_lock_type=nolock; update_lock_type=nolock; READ_ISOLATION_LEVEL = RU; Data Source=cambosuwsql01" 
schema=dbo;

%let server = sqlsrvqa;


proc sql noprint;;
select p.RatingVersion into :Rating_Version 
from in.POLICY_INFO p
;
quit;

/*proc sql;*/
/*create table work.test as*/
/*select a.policynumber ,a.casenumber, a.policytermyears, (-b.autolossdate+a.CLUE_REPORT_REQUEST_DATE)/(60*60*24) as diff  from in.policy_info as a*/
/*left join &server..HS_APRating_RequestAutoClaims as b*/
/*on a.casenumber=b.casenumber where a.casenumber=112857342;*/
/*quit;*/

  
proc sql;
create table work.Autoclaim_lookup as
select a.policynumber ,a.casenumber, a.policytermyears, yrdif(datepart(b.autolossdate), datepart(a.CLUE_REPORT_REQUEST_DATE), 'ACT/ACT') as year_difference, case when calculated year_difference <=3 then b.autolosscode else " " end as autolosscode, a.CLUE_REPORT_REQUEST_DATE,b.autolossdate , b.autolossamount from in.policy_info as a
left join &server..HS_APRating_RequestAutoClaims as b
on a.casenumber=b.casenumber;
quit;

proc sql;
create table work.Autoclaim_Losscode as
select autolosscode, peril from &server..HS_APRating_AutoLossCode_Peril
where version=&Rating_Version;
quit;

data peril_list;
input peril $ num;
cards;
P1 1
P2 2
P3 3
P4 4
P5 5
P6 6
P7 7
P8 8
P9 9
;

proc sql;
create table work.Autoclaim_lookup2 as
select a.*, b.autolosscode, c.peril, c.num from work.autoclaim_lookup as a 
left join work.autoclaim_losscode as b
on a.autolosscode=b.autolosscode
left join work.peril_list as c
on b.peril=c.num
order by a.policynumber;
quit;

/* important: summarize # of perils */
proc sql;
create table work.Autoclaim_Group as
select policynumber, casenumber, policytermyears, peril, count(num) as peril_count from work.Autoclaim_lookup2
group by policynumber, casenumber, peril
order by policynumber;
quit;

proc sort data=work.Autoclaim_Group;
by policynumber casenumber;
run;

data work.Autoclaim_Group_Rotated(drop=peril peril_count i);
set work.Autoclaim_Group;
by PolicyNumber casenumber;
array mark{7} P_1 P_2 P_3 P_4 P_5 P_6 P_8;
retain mark(0);
if peril=" " then do;
    do i=1 to 7;
       mark{i}=0;
    end;
end; /*there is only one row, output directly*/
else do;
    select (peril);
       when ('P1') P_1=peril_count;
       when ('P2') P_2=peril_count;
       when ('P3') P_3=peril_count;
       when ('P4') P_4=peril_count;
       when ('P5') P_5=peril_count;
       when ('P6') P_6=peril_count;
       when ('P8') P_8=peril_count;
       otherwise;
    end;
end;

if last.casenumber then do;
    output;
       do i=1 to 7;
           mark{i}=0;
       end;
    end;

run;

data r346fac;
set fac.'346# Responsible Motorist$'n;
if Fire=1 then F1=5;
else if Fire=. then delete;
drop F9 F10;
run;

proc sql;
create table out.res_motor_fac as
select 
a.*,
/* if it's marked, corresponding factor should be 1*/
case when P_1>0 then 1 else b.Fire end as fire_resmotor,
case when P_2>0 then 1 else b.Theft end as theft_resmotor,
case when P_3>0 then 1 else b.'Water Non-Weather'n end as waternw_resmotor,
case when P_4>0 then 1 else b.'All Other Non-Weather'n end as othernw_resmotor,
case when P_5>0 then 1 else b.Lightning end as lightning_resmotor,
case when P_6>0 then 1 else b.'Water Weather'n end as waterw_resmotor,
case when P_8>0 then 1 else b.Hail end as hail_resmotor
from Autoclaim_Group_Rotated as a
left join r346fac as b
on (case when a.policytermyears>5 then 5 else a.policytermyears end)=b.F1;
quit;
