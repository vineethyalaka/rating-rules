** RULE 348. FIRE TAX CREDIT **;
/* 2015-11-17 SH: Version 1 */

%macro firetax();

data out.firecredit_fac; 
set in.policy_info (keep=casenumber policynumber propertyzip5);
if propertyzip5 in (85377, 85268, 85269)
	then firecredit = .006;
else firecredit = 0;
run;

%if &Process = 0 %then %do;
	title 'RULE 344. AFFINITY MARKETING';
	proc freq data=out.firecredit_fac;
	tables firecredit / list missing;
	run;
%end;

%mend ;
%firetax();
