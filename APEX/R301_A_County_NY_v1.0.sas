** RULE 301.A COUNTY **;
%macro county();
	proc sql;
		create table out.county_fac as
		select p.casenumber, p.policynumber, p.countyiD
			, f.fire as fire_county
			, f.Theft as theft_county
			, f.'Water Non-Weather'n as wnw_county
			, f.lightning as lightning_county
			, f.'Water Weather'n as waterw_county
			, f.wind as wind_county
			, f.hail as hail_county
			, f.hurricane as hurr_county
			, f.'Liability - Bodily injury (Perso'n as liabipi_county
			, f.'Liability - Bodily injury (Medic'n as liabimp_county
			, f.'Liability - Property Damage to O'n as liapd_county
			, f.'Water Back-up'n as waterbu_county
		from out.territory p left join fac.'301#A County (Territory)$'n f 
			on p.countyiD = f.F2;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 301.A COUNTY';
	proc freq data=out.county_fac;
	tables countyid
		*theft_county / list missing;
	run;
%end;
%mend;
%county();