** RULE 387. Fire Department Drive Time**;
**=====================================================================================**
History: 2019 07 08 BGI v1.0 Initial draft for APEX 2.0
		 2020 02 06 JK  v1.1 Revised Rule number in code and adjusted code to accurately rate
**=====================================================================================**;
%macro PBDT();

	data R387;
	set fac.'387# Fire Department Drive Time$'n;
	Factor='Theft'n;
	if Theft=. then delete;
	FDDriveTimeBin=_n_-1;
run;

	proc sql;
		create table work.PB_Fire_Department_Drive_Time as select 
		p.casenumber, 
		p.policynumber, 
		p.DriveTime,
		Case When p.DriveTime > 0 and p.DriveTime<=999 then 1
/*			 When p.DriveTime>10 then 2*/
/*		 	 Else 3*/
			 End As DriveTime_Bin
		from in.policy_info as p 
	;quit;

proc sql;
		create table out.PB_Fire_Department_Drive_Time as select
			p.casenumber, 
			p.policynumber,
			p.DriveTime, 
			p.DriveTime_Bin,
			f.Factor as theft_DriveTime
	from work.PB_Fire_Department_Drive_Time as p left join R387 as f on p.DriveTime_Bin=f.FDDriveTimeBin
	;quit;


%mend;
%PBDT();
