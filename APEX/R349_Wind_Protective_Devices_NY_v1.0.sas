** RULE 349. WINDSTORM PROTECTIVE DEVICES **;
** New York only **;

%macro wind_devices();

	data r349;
	set fac.'349# Wind Protective Devices$'n;
	run;

	proc sql;
		create table out.wind_devices_fac as
		select p.CaseNumber, p.PolicyNumber, f.Wind as Wind_Prot_Devices, f.Hurricane as Hurr_Prot_Devices
		from in.Endorsement_Info as p left join r349 as f
			on p.Wind_Credit_Flag=f.F2;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 349. WINDSTORM PROTECTIVE DEVICES';
	proc freq data=out.wind_devices_fac;
	run;
%end;
%mend;
%wind_devices();

