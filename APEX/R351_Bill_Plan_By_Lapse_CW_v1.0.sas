** RULE 351. BILL PLAN BY LAPSE **;
%macro billlapse();
*Bill Plan;
	data bill_plan; 
	set in.policy_info (keep=casenumber policynumber BillPlan MortgageEBillFlag);
		if BillPlan in ('','00','03') /*or MortgageEBillFlag = 'Mortgage'*/ then BillPlanType = 'Full-pay or Mortgage';
		else if BillPlan in ('04','05') then BillPlanType = '4-pay';
		else if BillPlan in ('13','15') then BillPlanType = '10-pay';
	run;

	*Lapse;
	proc sort data=in.policy_info; by casenumber; run;
	proc sort data=in.production_info; by casenumber; run;
	data lapse;
	merge in.policy_info (keep=casenumber policynumber Lapse_of_Insurance end_code_3_NH end_code_3_PI)
		  in.production_info (keep=casenumber Lapse_Date_Used);
	by casenumber;
	period = end_code_3_NH - end_code_3_PI;
	if Lapse_Date_Used = 0 then lapse = 0;
	else if Lapse_Date_Used = 1 and period = . then lapse = 1;
	else if period <= 1 then lapse = 0;
	else lapse = 1;
	run;

	*Bill Plan by Lapse;
	data exp_bill_plan_x_lapse_fac (keep=bill_plan lapse factor);
	set fac.'351# Bill Plan by Lapse$'n;
	if f3=. then delete;
	lapse=0; factor = 'Expense Fee'n; output;
	lapse=1; factor = f3; output;
	rename f1 = bill_plan;
	run;
	proc sql;
		create table out.exp_bill_plan_x_lapse_fac as
		select p1.*, p2.lapse, f.factor as exp_bill_plan_x_lapse_fac
		from bill_plan p1 
			inner join lapse p2	on p1.casenumber = p2.casenumber
			left join exp_bill_plan_x_lapse_fac f on f.bill_plan = p1.BillPlanType and f.lapse = p2.lapse;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 351. BILL PLAN BY LAPSE';
	proc freq data=bill_plan;
	tables MortgageEBillFlag*BillPlan*BillPlanType / list missing;
	run;
	proc freq data=out.exp_bill_plan_x_lapse_fac;
	tables BillPlanType*lapse*exp_bill_plan_x_lapse_fac/ list missing;
	run;
%end;
%mend;
%billlapse();