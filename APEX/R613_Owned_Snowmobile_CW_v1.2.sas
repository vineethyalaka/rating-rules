/*** RULE 613. OWNED SNOWMOBILE **;*/

/*Version control*/
/*	Date		Editor	Version	Notes*/
/*	2015 07 29 	JT		1.1		Removed reference to "num_snowmobile" in factor cacluation. IT has confirmed that each snowmobile is rated individually. (ie. the premium for each snowmobile is rounded individually, then the total snowmobile premium is the sum of each individual unit.*/
/*								Version 1.1 of this rule is intended to work with Step6_Liability_Premium_CW_v1.3*/
/*	2015 10 06	JT		1.2		Set factor = 0 when no snowmobiles are present. This has no affect on premiums, as the factors are later multiplied by number of snowmobiles. This change was purely for error checking.*/

%macro snow();
data r613fac;
set fac.'613# Owned Snowmobile$'n;
if f1="" then delete;
fac1='Liability - Bodily injury (Perso'n*1;
fac2='Liability - Bodily injury (Medic'n*1;
fac3='Liability - Property Damage to O'n*1;
run;
data snowmobile; 
set in.endorsement_info (keep=casenumber policynumber HO_2464_Make_Model_01 - HO_2464_Make_Model_03 HO_2464_FLAG);
array model (3) HO_2464_Make_Model_01 - HO_2464_Make_Model_03;
num_snowmobile = 0;
if HO_2464_FLAG="1" then do i = 1 to 3;
	if model(i)="" then num_snowmobile=num_snowmobile;
	else num_snowmobile=num_snowmobile+1;
end;
drop i;
run;
proc sql;
    create table out.lia_snowmobile_fac as
	select p.*
		, case when p.num_snowmobile = 0 then 0 else f.fac1 end as liabipi_snowmobile
		, case when p.num_snowmobile = 0 then 0 else f.fac2 end as liabimp_snowmobile
		, case when p.num_snowmobile = 0 then 0 else f.fac3 end as liapd_snowmobile
	from r613fac f, snowmobile p;
quit;
%if &Process = 0 %then %do;
title 'RULE 613. OWNED SNOWMOBILE';
proc freq data=out.lia_snowmobile_fac;
tables num_snowmobile
	*liabipi_snowmobile
	*liabimp_snowmobile
	*liapd_snowmobile / list missing;
run;
%end;
%mend;
%snow();