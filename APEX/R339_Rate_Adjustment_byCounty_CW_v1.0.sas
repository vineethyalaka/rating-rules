** RULE 338. Underwriting Tier**;
**=====================================================================================**
History: 2017 07 26 BF v1.0 Initial draft for APEX 2.0
		 2018 05 16 JL v1.1 Correcting logic
**=====================================================================================**;
%macro adj();

	data R339;
	set fac.'339# Rate Adjustment$'n;
	if f3 = . then delete;
/*	Factor=input(F1,5.3);*/
	run;


	proc sql;
		create table out.rate_adjustment as
		select p.casenumber, p.policynumber, f3 as rate_adjustment
		from in.policy_info p
        inner join R339 f
        on p.countyID = f.f2;
/*p.county = substr(f.f1,1,length(f.f1)-7) and */
	quit;

%mend;
%adj();
