
** RULE 333. INSURANCE RISK SCORE **;
**=====================================================================================**
**************For on-leveling/impacts, need to include Model Type**************
History: 2016 12 08 JL  Initial draft based on Cw v1.1, NY APEX rule language uses tier in place of insurance score
**=====================================================================================**;
%macro insscore();
data test;
set fac.'333# Tier$'n;
run;

/*summarize loss information within three years*/
	proc sql;
		create table claim_buckets_within3 as
		select distinct l.casenumber, l.policynumber, p.PolicyTermYears, p.PolicyEffectiveDate, l.lossdate, 
			l.LossCode, l.LossAmount
			/* treat cat and non-cat the same */
			, case when l.LossCode > 1000 then l.LossCode-1000 else l.LossCode end as code
			/* bucket loss codes into perils */
			, case when calculated code in(7,17,33)        then 1 else 0 end as fireclaim
			, case when calculated code in(24)             then 1 else 0 end as windclaim
			, case when calculated code in(18,19,20,53,54) then 1 else 0 end as theftclaim
			, case when calculated code in(12) /*or lightning =1*/ then 1 else 0 end as lightningclaim /**/
			, case when calculated code in(29) 			   then 1 else 0 end as liabilityclaim /**/
			, case when calculated code in(9,59)           then 1 else 0 end as waterclaim
			, case when calculated code in(23)             then 1 else 0 end as backupclaim
			, case when calculated code in(4,5,6,21,22,25,26,27,32,37,52,58,60,61)
													 	   then 1 else 0 end as otherclaim			
			, case when calculated code not in(5,8,11,12,24,41) then 1 else 0 end as naogclaim

			
		from in.loss_info l 
			inner join in.policy_info p on l.casenumber=p.casenumber
		where l.LossAmount >0
			and (p.PolicyTermYears=1 and &effst = 0 and (p.PolicyIssueDate - l.LossDate)/365 <3
			or  p.PolicyTermYears=1 and &effst ^= 0 and (p.PolicyEffectiveDate - l.LossDate)/365 <3
			or  p.PolicyTermYears^=1 and (p.PolicyEffectiveDate - l.LossDate - 60)/365 <3);
	quit;

	proc summary data=claim_buckets_within3;
	class casenumber;
	var  fireclaim windclaim theftclaim lightningclaim liabilityclaim waterclaim backupclaim otherclaim naogclaim;
	output out=claim_sums sum=;
	run;

	data claim_sums;
	set claim_sums;
	total_claims = sum( of fireclaim -- naogclaim);
	run;


	/*Tier decrease table*/
/*	data Tier_Decrease_Margin;*/
/*	input Tier_LowerBound Tier_UpperBound Decreased_Margin;*/
/*	datalines;*/
/*	1 9 -1*/
/*	10 16 -2*/
/*	17 21 -3*/
/*	;*/
/*	run;*/
	
	proc sql;
	create table tier_decrease_margin
	(
	Tier_LowerBound int,
	Tier_UpperBound int,
	Decreased_Margin int
	);
	insert into tier_decrease_margin
	values(1,9,-1)
	values(10,16,-2)
	values(17,21,-3)
	;
	quit;


	data ins_tier_pre; *Brian Flood also used in rules 353, 354;
	set in.drc_info;
	length ins_tier $2.;
	if matchcode = 0 then ins_tier='20';
	else if matchcode < 0 then ins_tier='21';
	else if ScoreModel = 2 then do;
		if insscore >= 950 then ins_tier='1';
		else if 947 <= insscore <= 949 then ins_tier='2';
		else if 932 <= insscore <= 946 then ins_tier='3';
		else if 918 <= insscore <= 931 then ins_tier='4';
		else if 908 <= insscore <= 917 then ins_tier='5';
		else if 892 <= insscore <= 907 then ins_tier='6';
		else if 877 <= insscore <= 891 then ins_tier='7';
		else if 862 <= insscore <= 876 then ins_tier='8';
		else if 846 <= insscore <= 861 then ins_tier='9';
		else if 830 <= insscore <= 845 then ins_tier='10';
		else if 811 <= insscore <= 829 then ins_tier='11';
		else if 796 <= insscore <= 810 then ins_tier='12';
		else if 767 <= insscore <= 795 then ins_tier='13';
		else if 747 <= insscore <= 766 then ins_tier='14';
		else if 727 <= insscore <= 746 then ins_tier='15';
		else if 664 <= insscore <= 726 then ins_tier='16';
		else if 634 <= insscore <= 663 then ins_tier='17';
		else if 601 <= insscore <= 633 then ins_tier='18';
		else if 150 <= insscore <= 600 then ins_tier='19';
		else if   0 <= insscore <= 149 then ins_tier='20';
		else ins_tier='21';
	end;
	else if ScoreModel = 1 then do;
		if insscore >= 870 then ins_tier='1';
		else if 851 <= insscore <= 869 then ins_tier='2';
		else if 831 <= insscore <= 850 then ins_tier='3';
		else if 811 <= insscore <= 830 then ins_tier='4';
		else if 791 <= insscore <= 810 then ins_tier='5';
		else if 771 <= insscore <= 790 then ins_tier='6';
		else if 751 <= insscore <= 770 then ins_tier='7';
		else if 731 <= insscore <= 750 then ins_tier='8';
		else if 711 <= insscore <= 730 then ins_tier='9';
		else if 691 <= insscore <= 710 then ins_tier='10';
		else if 671 <= insscore <= 690 then ins_tier='11';
		else if 651 <= insscore <= 670 then ins_tier='12';
		else if 631 <= insscore <= 650 then ins_tier='13';
		else if 611 <= insscore <= 630 then ins_tier='14';
		else if 591 <= insscore <= 610 then ins_tier='15';
		else if 531 <= insscore <= 590 then ins_tier='16';
		else if 501 <= insscore <= 530 then ins_tier='17';
		else if 471 <= insscore <= 500 then ins_tier='18';
		else if 199 <= insscore <= 470 then ins_tier='19';
		else if   0 <= insscore <= 198 then ins_tier='20';
		else ins_tier='21';
	end;
	ins_tier_n=ins_tier*1;
	run;

/*re-calculate if there is adjustment*/
proc sql;
create table ins_tier as
select 
a.casenumber,
a.policynumber,
a.policytermyears,
a.insscore,
a.matchcode,
a.ScoreModel,
case when c.total_claims = . then 0 else c.total_claims end as total_claims,
case when /*mod(a.policytermyears-1,3)=0 and*/ a.policytermyears >=4 and a.ins_tier_n not in (1,21) then (case when calculated total_claims eq 0  then a.ins_tier_n + b.Decreased_Margin else a.ins_tier_n end)
									 else a.ins_tier_n
end as ins_tier_n,

left(put(calculated ins_tier_n, 2.)) as ins_tier

from ins_tier_pre as a inner join Tier_Decrease_Margin as b
on a.ins_tier_n>=b.Tier_LowerBound and a.ins_tier_n<=Tier_UpperBound
left join claim_sums as c 
on a.casenumber = c.casenumber
;
quit;
	proc sql;
		create table out.ins_score_fac as
		select p.casenumber, p.policynumber, p.insscore, p.policytermyears, p.ins_tier_n
			, f.fire as fire_ins_score
			, f.theft as theft_ins_score
			, f.'Water Non-Weather'n as waternw_ins_score
			, f.'All Other Non-Weather'n as othernw_ins_score
			, f.lightning as lightning_ins_score
			, f.'Water Weather'n as waterw_ins_score
			, f.wind as wind_ins_score
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_ins_score
			, f.'Liability - Bodily injury (Perso'n as liabipi_ins_score
			, f.'Liability - Bodily injury (Medic'n as liabimp_ins_score
			, f.'Liability - Property Damage to O'n as liapd_ins_score		
			, f.'Water Back-up'n as waterbu_ins_score
		from ins_tier p left join fac.'333# Tier$'n f
			on p.ins_tier=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 333. Tier';
	proc print data=out.ins_score_fac;
	where fire_ins_score*theft_ins_score*waternw_ins_score*othernw_ins_score*lightning_ins_score*waterw_ins_score
	*wind_ins_score*hurr_ins_score*liabipi_ins_score*liabimp_ins_score*liapd_ins_score*waterbu_ins_score = .;
	run;
	proc gplot data=out.ins_score_fac;
		plot (fire_ins_score theft_ins_score waternw_ins_score othernw_ins_score lightning_ins_score waterw_ins_score
	 		wind_ins_score hurr_ins_score liabipi_ins_score liabimp_ins_score liapd_ins_score waterbu_ins_score)*ins_tier_n 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.ins_score_fac;
		histogram ins_tier_n;
	run;
%end;
%mend;
%insscore();