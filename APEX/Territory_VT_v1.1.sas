
/*filename attr "\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\AL\v1301_11831\03_Data\Factors\AL_HO3_v3.csv";*/
filename attr "\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\VT\v_15026\03_Data\Factors\VT_HO3_v05_20191126_UW7_5k500mLocID.xlsx";
proc import dbms=xlsx
			file=attr
			out=Attr_table
			replace;
run;

		proc sql;
	     create table out.territory as
	     select distinct 
				p.policynumber
	           , p.CaseNumber
	           , p.policytermyears
	           , p.policyeffectivedate
	           , p.CountyID
	           , p.Census2010 as census_block_group_ID
	           , p.LocationID
			   , p3.DeductibleGroup as ded_terr
	     from in.policy_info p
		 left join Attr_table p3 on p.LocationID = p3.LocationID
;
	quit;
