** RULE 531. LIMITED SLAB AND FOUNDATION COVERAGE **;
*MM Updated to all for flag to be blank and thus able to run Legacy data through APEX*;
%macro slab();
	data r531;
	set fac.'531# Slab and Foundation$'n;
	if strip(f1) = '' then delete;
	run;
	proc sql;
		create table out.Slab_Foundation_Rate as
		select p.CaseNumber, p.PolicyNumber, p.HO_04_68_flag
			, CASE WHEN p.HO_04_68_flag IS NULL THEN 0 ELSE round(p.HO_04_68_flag*f.charge,1.0) END as Slab_Foundation_Rate
		from in.Endorsement_Info p, r531 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 531. LIMITED SLAB AND FOUNDATION COVERAGE';
	proc freq data=out.Slab_Foundation_Rate;
	tables HO_04_68_flag*Slab_Foundation_Rate / list missing;
	run;
%end;
%mend;
%slab();
