** RULE 515.D REFRIGERATED PROPERTY **;
%macro RP();
	data r515d;
	set fac.'515#D Refrigerated Property$'n;
	if strip(f1) = '' then delete;
	rename 'Rate Per Policy'n = Charge;
	run;

	proc sql;
		create table out.Refrigerated_Property as
		select e.casenumber, e.policynumber, e.HO_0498_FLAG
		, case when e.HO_0498_FLAG = "1" then round(f.charge,1.0) else 0 end as Refrigerated_Prop_Rate
		from in.Endorsement_Info e, r515d f;
	quit;


%if &Process = 0 %then %do;
	title 'RULE 515.D REFRIGERATED PROPERTY';
	proc freq data=out.Refrigerated_Property;
	tables HO_0498_FLAG*Refrigerated_Prop_Rate / list missing;
	run;
%end;
%mend;
%rp();