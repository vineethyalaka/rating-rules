** RULE 325. REPLACEMENT COST PER SQFT **;
/*2019/11/25 TM add P1*/
%macro RCFT();
	data rcsqft;
	set in.policy_info;
	if propertysquarefootage = 0 or propertyReplacementCost = 0 then rcsqft = 120;
	else rcsqft=round(propertyReplacementCost/propertysquarefootage,1);
	run;
	data rcsqft_fac;
	set fac.'325# Replacement Cost Sq Ft$'n;
	rcmin=f1+0;
	rcmax=f3+0;
	if rcmin=246 then rcmax=9999;
	if rcmin=. then delete;
	run;
	proc sql;
		create table out.rcsqft_fac as
		select p.casenumber, p.policynumber, p.orig_rpl_cc, p.propertysquarefootage, p.rcsqft
			, f.fire as fire_rcsqft
			, f.'Water Non-Weather'n as waternw_rcsqft
			, f.lightning as lightning_rcsqft
			, f.'Water Weather'n as waterw_rcsqft
			, f.wind as wind_rcsqft		
		from rcsqft p left join rcsqft_fac f
			on p.rcsqft>=f.rcmin
			and p.rcsqft<=f.rcmax;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 325. REPLACEMENT COST PER SQFT';
	proc print data=out.rcsqft_fac;
	where waternw_rcsqft*lightning_rcsqft*waterw_rcsqft*wind_rcsqft= .;
	run;
	proc gplot data=out.rcsqft_fac;
		plot (waternw_rcsqft lightning_rcsqft waterw_rcsqft wind_rcsqft)*rcsqft 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.rcsqft_fac;
		histogram rcsqft;
	run;
%end;
%mend;
%RCFT();