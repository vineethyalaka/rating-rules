
** RULE 334. DEROGATORY REASON CODE **;
**=====================================================================================**
		2018 10 26 KK  Initial draft - OR requires we keep old score factors the same
**=====================================================================================**;
%macro DRC();
	proc sql;
	create table drc as 
	select *,
	case when (score_12_1 not like '%D02%' or score_12_1 is null) and (score_13_1 not like '%D02%' or score_13_1 is null) and 
	           (score_14_1 not like '%D02%' or score_14_1 is null) and (score_15_1 not like '%D02%' or score_15_1 is null) and
	         ((case when score_12_1 like '%D%' then 1 else 0 end) + (case when score_13_1 like '%D%' then 1 else 0 end) +
	          (case when score_14_1 like '%D%' then 1 else 0 end) + (case when score_15_1 like '%D%' then 1 else 0 end)) >= 2
	     then 5
	     when (score_12_1 not like '%D02%' or score_12_1 is null) and (score_13_1 not like '%D02%' or score_13_1 is null) and 
	          (score_14_1 not like '%D02%' or score_14_1 is null) and (score_15_1 not like '%D02%' or score_15_1 is null) and
	         ((case when score_12_1 like '%D%' then 1 else 0 end) + (case when score_13_1 like '%D%' then 1 else 0 end) +
	          (case when score_14_1 like '%D%' then 1 else 0 end) + (case when score_15_1 like '%D%' then 1 else 0 end)) = 1
	     then 4
	     when (score_12_1 like '%D02%' or score_13_1 like '%D02%' or score_14_1 like '%D02%' or score_15_1 like '%D02%') and
	         ((case when score_12_1 like '%D%' then 1 else 0 end) + (case when score_13_1 like '%D%' then 1 else 0 end) +
	          (case when score_14_1 like '%D%' then 1 else 0 end) + (case when score_15_1 like '%D%' then 1 else 0 end)) >= 2
	     then 3
	     when (score_12_1 like '%D02%' or score_13_1 like '%D02%' or score_14_1 like '%D02%' or score_15_1 like '%D02%') and
	         ((case when score_12_1 like '%D%' then 1 else 0 end) + (case when score_13_1 like '%D%' then 1 else 0 end) +
	          (case when score_14_1 like '%D%' then 1 else 0 end) + (case when score_15_1 like '%D%' then 1 else 0 end)) = 1
	     then 2
	     else 1
	end as drc_class
	from in.drc_info;
	quit;
	proc sql;
		create table out.drc_fac as
		select p.casenumber, p.policynumber, case when p.ScoreModel = 3 then 1 else p.drc_class end as drc_class
			, f.theft as theft_drc
			, f.'Liability - Bodily injury (Perso'n as liabipi_drc
			, f.'Liability - Bodily injury (Medic'n as liabimp_drc
		from drc p 
		left join fac.'334# DRC$'n f
			on case when p.ScoreModel = 3 then 1 /* using 1.0 factor if not TUIRS */
					else p.drc_class 
				end=f.f1
			;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 334. DEROGATORY REASON CODE';
	proc freq data=out.drc_fac;
	tables drc_class
		*theft_drc
		*liabipi_drc
		*liabimp_drc / list missing;
	run;
%end;

%mend;
%DRC();