/*Rule 374 Roof Score*/
/*12/17/2019 TM Initial draft*/
/*2/3/2020 TM add the case when roofscore key = -1, assign factor 1.0 */
data roof_score_mapping;
set rfmap.'Rule 374$'n;
rename 'mapped score'n = mapped_score
'Model Score Low >='n = orig_score_l
'Model Score High <'n = orig_score_h
;
run;

data r374_fac;
set fac.'374# Roof Score$'n;
if f1=. then delete;
run;

/*map windscore in endorsement info table*/
proc sql;
create table wind_roof_score_mapped as 
select 
e.policynumber, e.casenumber, e.windscore, r.mapped_score as wind_mapped_score
from roof_score_mapping r
right join in.endorsement_info e
on e.windscore >= r.orig_score_l and e.windscore < r.orig_score_h ;

create table wind_roof_score_fac as 
select s.*, f.wind as wind_roof_score_fac from
wind_roof_score_mapped s
left join r374_fac f
on s.wind_mapped_score = f.f1;
quit;



/*map waterscore*/
proc sql;
create table water_roof_score_mapped as 
select 
e.policynumber, e.casenumber, e.waterscore, r.mapped_score as water_mapped_score
from roof_score_mapping r
right join in.endorsement_info e
on e.waterscore >= r.orig_score_l and e.waterscore < r.orig_score_h;

create table water_roof_score_fac as 
select s.*, f.'water weather'n as waterw_roof_score_fac from
water_roof_score_mapped s
left join r374_fac f
on s.water_mapped_score = f.f1;
quit;

/*map hailscore*/
proc sql;
create table hail_roof_score_mapped as 
select 
e.policynumber, e.casenumber, e.hailscore, r.mapped_score as hail_mapped_score
from roof_score_mapping r
right join in.endorsement_info e
on e.hailscore >= r.orig_score_l and e.hailscore < r.orig_score_h;

create table hail_roof_score_fac as 
select s.*, f.hail as hail_roof_score_fac from
hail_roof_score_mapped s
left join r374_fac f
on s.hail_mapped_score = f.f1;
quit;

/*join three factors*/
proc sql;
create table out.roof_score_factor as
select *
from work.water_roof_score_fac w
inner join work.wind_roof_score_fac wd
on w.casenumber = wd.casenumber
inner join work.hail_roof_score_fac h
on wd.casenumber = h.casenumber;
quit;

proc sql;
create table out.roof_score_factor as
select w.policynumber, w.casenumber,
case when waterscore = . then 1 else waterw_roof_score_fac end as waterw_roof_score_fac,
case when windscore = . then 1 else wind_roof_score_fac end as wind_roof_score_fac,
case when hailscore = . then 1 else hail_roof_score_fac end as hail_roof_score_fac,
*
from work.water_roof_score_fac w
inner join work.wind_roof_score_fac wd
on w.casenumber = wd.casenumber
inner join work.hail_roof_score_fac h
on wd.casenumber = h.casenumber;
quit;