	data work.RateAdjustment;
	set fac.'339# Rate Adjustment$'n;
    Rate_Adjustment_Factor = input(f1,8.);
	if Rate_Adjustment_Factor = . then delete;
	run;

	*temporarily change the factor in work.RateAdjustment table for testing;
	data out.RateAdjustment;
	set RateAdjustment;
	run;

