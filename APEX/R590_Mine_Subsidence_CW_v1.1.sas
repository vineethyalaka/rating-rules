** RULE 590. MINE SUBSIDENCE INSURANCE **;

**=====================================================================================**
History: 2015 04 13 JT  1.0 Initial draft
		 2016 07 13 SY  1.1 Add Macro		
**=====================================================================================**;

%macro ms();

data r590;
set fac.'590# Mine Subsidence$'n;
format f1 f3 12.2;
if f1 = . and f3 = . then delete;
run;
proc sql;
	create table out.Mine_Sub_Rate as
	select p.CaseNumber, p.PolicyNumber, e.HO2388_FLAG, p.CoverageA
		, case when e.HO2388_FLAG = '1' then f4 else 0 end as Mine_Sub_Rate
	from in.policy_info p
		inner join in.Endorsement_Info e on p.casenumber = e.casenumber
		left join r590 f on p.CoverageA >= f.f1 and (p.CoverageA <= f.f3 or f.f3 = .);
quit;

%if &Process = 0 %then %do;
title 'RULE 590. MINE SUBSIDENCE INSURANCE';
proc print data=out.Mine_Sub_Rate;
where Mine_Sub_Rate = .;
run;
proc sort data=out.Mine_Sub_Rate; by HO2388_FLAG; run;
proc gplot data=out.Mine_Sub_Rate; by HO2388_FLAG;
	plot Mine_Sub_Rate*CoverageA 
		/ overlay legend vaxis=axis2;
run;
quit;
%end;

%mend;

%ms();
