** RULE 330. NUMBER OF RESIDENTS **;
%macro NOR();
	data numres;
	set in.policy_info;
	if residents_totalcount >= 6 then numres=6; else numres=residents_totalcount;
	run;
	data numresfac;
	set fac.'330# Number of Residents$'n;
	if f1=. and fire ne . then f1=6;
	run;
	proc sql;
		create table out.num_residents_fac as
		select p.casenumber, p.policynumber, p.residents_totalcount
			, f.fire as fire_num_res
			, f.theft as theft_num_res
			, f.'Water Non-Weather'n as waternw_num_res
			, f.'Liability - Property Damage to O'n as liapd_num_res
			, f.'Water Back-up'n as waterbu_num_res
		from numres p left join numresfac f
			on p.numres=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 330. NUMBER OF RESIDENTS';
	proc freq data=out.num_residents_fac;
	tables residents_totalcount
		*fire_num_res
		*theft_num_res
		*waternw_num_res
		*liapd_num_res	
		*waterbu_num_res / list missing;
	run;
%end;
%mend;
%NOR();
