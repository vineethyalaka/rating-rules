** RULE 406. DEDUCTIBLES **;

**=====================================================================================**
History: 2015 03 18 JT  Initial draft
		 2015 05 11 SY  Added Hurricane peril indicator
		 2017 01 25 BF  Hail Peril uses AOP deductible
**=====================================================================================**;

proc sql;
create table work.deductible_pre as
select
p.casenumber, 
p.policynumber, 
p.coveragea,
t.deductiblegroup as deductible_group,
t.territory, 
CoverageDeductible, 
WindHailDeductible, 
HurricaneDeductible
from in.policy_info as p 
inner join out.territory as t 
on p.casenumber=t.casenumber
/*inner join work.ded_eq as de*/
/*on t.territory=de.territory*/
;
quit;


	data deductible_fac;
	set fac.'406# Deductibles$'n;
	format f1 f2 12.2;
	if f1 < 1 then f1 = f1*100;
	if f1 = . then delete;
	cova_L = lag(f2); if cova_L > f2 or cova_L = . then cova_L = 0; rename f2 = cova_H; 
	Fire_n = Fire*1; Theft_n = Theft*1; waternw_n = 'Water Non-Weather'n*1; 
	othernw_n = 'All Other Non-Weather'n*1; Lightning_n = Lightning*1; waterw_n = 'Water Weather'n*1; 
	Wind_n = Wind*1; Hail_n = Hail*1; Hurricane_n = Hurricane*1;
	array peril (9) Fire_n Theft_n waternw_n othernw_n Lightning_n waterw_n Wind_n Hail_n Hurricane_n;
	array fac_l (9) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l;
	array fac_h (9) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h;
	do i = 1 to 9; 
		fac_l(i) = lag(peril(i));
		fac_h(i) = peril(i);
		if cova_L = 0 then fac_l(i) = fac_h(i);
	end;
	drop i;
	run;
	proc sql noprint;
		select max(cova_H)
		into: maxdedcova
		from deductible_fac
		;
	quit;
	%put &maxdedcova;




	data work.deductible;
	length Message $40; /*indicator for whether correct w/h deductible amt is used or not*/
	set work.deductible_pre;
	AOPDeductible=input(CoverageDeductible,dollar8.);
	WHDeductible=input(WindHailDeductible,dollar8.);
	HurrDeductible=input(HurricaneDeductible,dollar8.);

/*	if deductible_group = 1 then */
/*		do;*/
/*	   	if WHDeductible ne AOPDeductible then Message="WindHail Deductible Incorrect!";*/
/*		if  HurrDeductible not in (5,10) then Message="Hurricane Deductible Incorrect!";*/
/*		end;*/
/*	else if deductible_group = 2 then*/
/*		do;*/
/*		if WHDeductible ne AOPDeductible then Message="WindHail Deductible Incorrect!";*/
/*		if HurrDeductible not in (2,3,4,5,10) then Message="Hurricane Deductible Incorrect!";*/
/*		end;*/
/*	 else */
/*	 	do;*/
/*		if HurrDeductible ne AOPDeductible then Message="Hurricane Deductible Incorrect!";*/
/*		if WHDeductible <= 10 then */
/*			do;*/
/*			if coveragea *  WHDeductible < AOPDeductible then Message="WindHail Deductible Incorrect!";*/
/*			end;*/
/*		 else if WHDeductible >10 then*/
/*		 	do;*/
/*			if  WHDeductible < AOPDeductible then Message="WindHail Deductible Incorrect!";*/
/*			end;*/
/*		 end;*/

	
	if deductible_group = 1 then 
		do;
	   	WHDeductible = AOPDeductible;
		if  HurrDeductible not in (5,10) then Message="Hurricane Deductible Incorrect!";
		end;
	else if deductible_group = 2 then
		do;
		WHDeductible = AOPDeductible;
		if HurrDeductible not in (2,3,4,5,10) then Message="Hurricane Deductible Incorrect!";
		end;
	 else 
	 	do;
		HurrDeductible = WHDeductible;
		if WHDeductible <= 10 then 
			do;
			if coveragea *  WHDeductible < AOPDeductible then Message="WindHail Deductible Incorrect!";
			end;
		 else if WHDeductible >10 then
		 	do;
			if  WHDeductible < AOPDeductible then Message="WindHail Deductible Incorrect!";
			end;
		 end;

	 run;

	proc sql;
		create table deductible1 as
		select t.GridGroup,f1.f3,p.*, f1.cova_L, f1.cova_H
			, f1.fire_l, f1.fire_h
			, f1.theft_l, f1.theft_h
			, f1.waternw_l, f1.waternw_h
			, f1.othernw_l, f1.othernw_h
			, f1.lightning_l, f1.lightning_h
			, f1.waterw_l, f1.waterw_h
			, f2.wind_l, f2.wind_h
			, f1.hail_l, f1.hail_h /*refer to the rule 406, should use F1 factor*/

			, Case when &Hurricane = 1 then f3.hurr_l else 0 end as hurr_l
			, Case when &Hurricane = 1 then f3.hurr_h else 0 end as hurr_h	
	
		from deductible p 
		left join out.territory t on t.casenumber = p.casenumber 
			left join deductible_fac f1 on (p.Coveragea <= f1.cova_H or f1.cova_H = &maxdedcova) 
				and p.Coveragea > f1.cova_L and p.AOPDeductible = f1.F1 and t.GridGroup = f1.f3
			left join deductible_fac f2 on (p.Coveragea <= f2.cova_H or f2.cova_H = &maxdedcova) 
				and p.Coveragea > f2.cova_L and p.WHDeductible = f2.F1 and t.GridGroup = f2.f3
			left join deductible_fac f3 on (p.Coveragea <= f3.cova_H or f3.cova_H = &maxdedcova) 
				and p.Coveragea > f3.cova_L and p.HurrDeductible = f3.F1 and t.GridGroup = f3.f3
		;
	quit;
	data out.ded_fac; set deductible1;
	array fac_l (9) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l;
	array fac_h (9) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h;
	array fac   (9) fire_ded theft_ded waternw_ded othernw_ded lightning_ded waterw_ded wind_ded hail_ded hurr_ded;
	do i = 1 to 9;
		if coveragea > &maxdedcova or coveragea = cova_H then fac(i) = fac_h(i);
		else fac(i) = round((fac_h(i) - fac_l(i))*(coveragea - cova_L)/(cova_H - cova_L) + fac_l(i),.0001);
	end;
	drop i;
	run;




