
%include "\\cambosvnxcifs01\ITRatingTeam\Server_name\Server_Libname.sas";

proc sql outobs = 1;;
select p.RatingVersion into :Rating_Version 
from in.POLICY_INFO p
;
quit;

proc sql;
create table IT_affinity as 
select *
from &server..HS_APRating_AffinityMarketing
where version = &Rating_Version 
;
quit;

proc sql;
   insert into IT_affinity
      set AffinityMarketingID = 0000,
          Version=&Rating_Version,
          MinMarketGroup='50000',
          MaxMarketGroup='50099',
          Description='Mindi Affinity',
          AffinityDiscount=0;
run;
proc sql;
   insert into IT_affinity
      set AffinityMarketingID = 0000,
          Version=&Rating_Version,
          MinMarketGroup='51000',
          MaxMarketGroup='51099',
          Description='UWAA Affinity',
          AffinityDiscount=0;
run;


proc sql;
	create table work.affinity_orig as
	select a.*,
           b.* 
	from in.policy_info as a
	inner join work.IT_affinity as b
	on a.PolicyAccountNumber <= input(b.MaxMarketGroup,11.)
	and a.PolicyAccountNumber >= input(b.MinMarketGroup,11.)
	;
quit;

data affinity_orig_0;
 set affinity_orig;
	if autopolicynumber = '' then auto = 0;
	else auto = 1;

    if PolicyAccountNumber in (51000:51099) then partner = 0; /*All the polices hava records are partners since inner join by find a shared account code*/
	else partner = 1;
run;

proc sql;
		create table work.affinity_fac_0 as
		select p.*,  
        case
			when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
			when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
			when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
			when a.PolicyAccountNumber>=50000 and a.PolicyAccountNumber<=50099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/
 
			when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="0" then 0
			when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is null and substr(a.partnerflag,1,1)="1" then 0.05 /*Non-Auto Partner*/
			when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="1" then 0.10 /*Auto Partner*/
			when a.PolicyAccountNumber>=51000 and a.PolicyAccountNumber<=51099 and a.AutoPolicyNumber is not null and substr(a.partnerflag,1,1)="0" then 0.10 /*Affiliate Auto Discounts*/
			else auto*partner*0.1
        end as affinity
		from affinity_orig_0 p inner join in.policy_info a on p.casenumber=a.casenumber;
quit;


proc sql;
create table affinity_fac_1 as 
select p.casenumber,
       p.policynumber,
	   a.PolicyAccountNumber,	
       a.AutoPolicyNumber, 
	   a.auto,
	   substr(a.partnerflag,1,1)as partnerflag,
	   a.affinity 
from in.policy_info as p 
left join work.affinity_fac_0 as a
       on p.casenumber = a.casenumber;
quit;


data out.AFFINITY_FAC;
 set work.affinity_fac_1;
     if affinity = '' then affinity=0;
	 if auto = '' then auto =0;
run;

