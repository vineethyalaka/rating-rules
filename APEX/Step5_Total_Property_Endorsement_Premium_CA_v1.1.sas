	** TOTAL PROPERTY ENDORSEMENTS PREMIUM **;

**=====================================================================================**
History: 2015 03 18 JT  1.0 Initial draft
		 2015 07 10 SY  1.1 Include all property endorsements in one code
		 2017 06 08 KK  CA Version - Added Rule 527 and removed EQ	
**=====================================================================================**;


**********************   Create dummy table for non-exist endorsements   ****************;

%macro dummy_S5(table);
%if %sysfunc(exist(out.&table)) = 0 %then %do;
	proc sql;
	create table out.&table as select
	casenumber, 0 as &table
	from out.step4_Adjusted_subtotal_premium
	;quit;
%end;
%if %sysfunc(exist(out.Refrigerated_Property)) = 0 %then %do;
	proc sql;
	create table out.Refrigerated_Property as select
	casenumber, 0 as Refrigerated_Prop_Rate
	from out.step4_Adjusted_subtotal_premium
	;quit;
%end;
%if %sysfunc(exist(out.Home_Business_Property)) = 0 %then %do;
	proc sql;
	create table out.Home_Business_Property as select
	casenumber, 0 as Home_Business_Prop_Rate
	from out.step4_Adjusted_subtotal_premium
	;quit;
%end;
%mend;
%dummy_S5(SPP_base_premium);
%dummy_S5(Other_Struc_base_premium);
%dummy_S5(incr_covc_base_premium);
%dummy_S5(Business_Prop_rate);
/*%dummy_S5(EQ_Loss_Assess_rate);*/
/*%dummy_S5(Earthquake_rate);*/
%dummy_S5(Loss_Assess_rate);
%dummy_S5(loss_of_use_rate);
%dummy_S5(Decr_CovC_rate);
%dummy_S5(USPP_rate);
%dummy_S5(Computer_Rate);
%dummy_S5(ID_Theft_Rate);
%dummy_S5(Mine_Sub_Rate);
%dummy_S5(Limited_Fungi_Rate);
%dummy_S5(Package_Discount);
%dummy_S5(Workers_Comp);



%macro endorse();
proc sql;
		create table out.step5_Property_Premium as
		select *
			, case when f16.package_discount = 0 then 0 else f15.Refrigerated_Prop_Rate end as Refrigerated_Prem
			, f1.waterbu_base_premium
			, f2.SPP_base_premium
			, f3.Other_Struc_base_premium
			, f4.incr_covc_base_premium
			, f5.Business_Prop_rate
/*			+ f6.EQ_Loss_Assess_rate*/
/*			+ f7.Earthquake_rate*/
			, f8.Loss_Assess_rate
			, f9.loss_of_use_rate
			, f10.Decr_CovC_rate
			, f11.USPP_rate
			, f12.Computer_Rate
			, f13.ID_Theft_Rate
/*			+ f14.Mine_Sub_Rate*/
			, f18.Limited_Fungi_Rate
			, calculated Refrigerated_prem
			, f16.Package_Discount
			, f17.Home_Business_Prop_Rate
			, f19.Workers_Comp

			, (f1.waterbu_base_premium
			+ f2.SPP_base_premium
			+ f3.Other_Struc_base_premium
			+ f4.incr_covc_base_premium
			+ f5.Business_Prop_rate
/*			+ f6.EQ_Loss_Assess_rate*/
/*			+ f7.Earthquake_rate*/
			+ f8.Loss_Assess_rate
			+ f9.loss_of_use_rate
			- f10.Decr_CovC_rate
			+ f11.USPP_rate
			+ f12.Computer_Rate
			+ f13.ID_Theft_Rate
/*			+ f14.Mine_Sub_Rate*/
			+ f18.Limited_Fungi_Rate
			+ calculated Refrigerated_prem
			+ f16.Package_Discount
			+ f17.Home_Business_Prop_Rate
			+ f19.Workers_Comp)  AS Property_Premium
		from out.waterbu_base_premium f1
			inner join out.SPP_base_premium			 f2 on f1.casenumber=f2.casenumber
			inner join out.Other_Struc_base_premium	 f3 on f1.casenumber=f3.casenumber
			inner join out.incr_covc_base_premium	 f4 on f1.casenumber=f4.casenumber
			inner join out.Business_Prop_rate		 f5 on f1.casenumber=f5.casenumber
/*			inner join out.EQ_Loss_Assess_rate		 f6 on f1.casenumber=f6.casenumber*/
/*			inner join out.Earthquake_rate			 f7 on f1.casenumber=f7.casenumber*/
			inner join out.Loss_Assess_rate			 f8 on f1.casenumber=f8.casenumber
			inner join out.loss_of_use_rate			 f9 on f1.casenumber=f9.casenumber
			inner join out.Decr_CovC_rate			f10 on f1.casenumber=f10.casenumber
			inner join out.USPP_rate				f11 on f1.casenumber=f11.casenumber
			inner join out.Computer_Rate			f12 on f1.casenumber=f12.casenumber
			inner join out.ID_Theft_Rate			f13 on f1.casenumber=f13.casenumber
/*			inner join out.Mine_Sub_Rate			f14 on f1.casenumber=f14.casenumber*/
			inner join out.Refrigerated_Property	f15 on f1.casenumber=f15.casenumber
			inner join out.package_discount			f16 on f1.casenumber=f16.casenumber
			inner join out.Home_Business_Property   f17 on f1.casenumber=f17.casenumber
			inner join out.Limited_Fungi_Rate		f18 on f1.casenumber=f18.casenumber
			inner join out.Workers_Comp				f19 on f1.casenumber=f19.casenumber
;
	quit;

%if &Process = 0 %then %do;
	title 'STEP 5: TOTAL PROPERTY ENDORSEMENTS PREMIUM (RULE 301.A.5)';
	proc sql;
		select mean(waterbu_base_premium) as waterbu_base_prem_avg
			, mean(SPP_base_premium) as SPP_base_prem_avg
			, mean(Other_Struc_base_premium) as Other_Struc_base_prem_avg
			, mean(incr_covc_base_premium) as incr_covc_base_prem_avg
			, mean(Business_Prop_rate) as Business_Prop_rate_avg
/*			, mean(EQ_Loss_Assess_rate) as EQ_Loss_Assess_rate_avg*/
/*			, mean(Earthquake_rate) as Earthquake_rate_avg*/
			, mean(Loss_Assess_rate) as Loss_Assess_rate_avg
			, mean(loss_of_use_rate) as loss_of_use_rate_avg
			, mean(Decr_CovC_rate) as Decr_CovC_rate_avg
			, mean(USPP_rate) as USPP_rate_avg
			, mean(Computer_Rate) as Computer_Rate_avg
			, mean(ID_Theft_Rate) as ID_Theft_Rate_avg
			, mean(Mine_Sub_Rate) as Mine_Sub_Rate_avg
			, mean(Limited_Fungi_Rate) as Limited_Fungi_Rate_avg			
			, mean(Workers_Comp) as Workers_Comp_avg
			, mean(Property_Premium) as Property_prem_avg
		from out.step5_Property_Premium;
	quit;
%end;
%mend;
%endorse();
