** RULE 347 OFF PREMISES THEFT EXCLUSION **;

%macro theftexclusion();
	proc sql;
		create table work.TheftExclusion as
		select p.CaseNumber, p.PolicyNumber,  p.County, e.TheftExclusion = 1
		, case when p.County in('Bronx','Kings','Nassau','New York',
			'Putnam','Queens','Richmond','Rockland','Suffolk',
			'Westchester') and e.TheftExclusion = 1 then 'Yes' 
			else 'No' end as TheftExclusion
		from out.territory as p left join in.endorsement_info as e on p.CaseNumber = e.CaseNumber
			;
	quit;

	proc sql;
		create table out.TheftExclusion_fac as
		select p.CaseNumber, p.PolicyNumber, p.County, p.TheftExclusion, f.Theft as theft_exclusion
		from work.TheftExclusion as p left join fac.'347# Theft Exclusion$'n f on p.TheftExclusion = f.F1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 347 THEFT EXCLUSION';
	proc freq data=out.theftexclusion_fac;
	run;;
%end;
%mend;
%theftexclusion();