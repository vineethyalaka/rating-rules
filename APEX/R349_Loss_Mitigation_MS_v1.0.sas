/* R349 Loss Mitigation MS v1.0 P9 Hurricane only*/

proc sql;
create table loss_mitigation_info as
select p1.casenumber, p1.policynumber, p1.construction_completed
,p2.ConstructionType
from in.production_info p1
inner join  CONSTRUCTION as p2
	on p1.casenumber =p2.casenumber
	;
quit;


data Loss_Mitigation_table;
  set fac.'349# Loss Mitigation Discount$'n;
if _n_=1 then delete;
rename  f1= Mitigation_Level	
f2=Construction_Type
Hurricane=hurr_loss_mitigation;
drop f4-f11;
run;


PROC format;
 invalue ecode
	'FFSLS'=200
	'IRC'=100
	'Retrofit Gold - Option 1'=300
	'Retrofit Gold - Option 2'=400
	'Retrofit Silver - Option 1'=500
	'Retrofit Silver - Option 2'=600
	'Retrofit Bronze - Option 1'=700
	'Retrofit Bronze - Option 2'=800;
Run;

data Loss_Mitigation_table_ec;
 set Loss_Mitigation_table;
 Mitigation_Level_en=input(Mitigation_Level,ecode.);
Run;

proc sql;
	create table out.mitigation_fac as
	select *
	, case when p.construction_completed = 0 then 1 else f1.hurr_loss_mitigation end as loss_mitigation_disc
		from loss_mitigation_info p
		left join Loss_Mitigation_table_ec f1 
             on (p.construction_completed = f1.Mitigation_Level_en)
			 and (left(trim(upper(p.ConstructionType))) = left(trim(upper(f1.Construction_Type))))
	;
quit;
