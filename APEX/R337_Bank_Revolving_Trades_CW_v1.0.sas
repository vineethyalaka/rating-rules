** RULE 363. Revolving Trades**;
**=====================================================================================**
History: 2017 07 21 BF  Initial draft
**=====================================================================================**;
%macro RT();
	data revolving_trades;
	set in.drc_info;
	
	if    score_12_1 = 'G30' 
		 	or score_13_1 = 'G30'
		 	or score_14_1 = 'G30'
			or score_15_1 = 'G30'
	then trades = 'G30';

	else if    score_12_1 = 'G31' 
		 	or score_13_1 = 'G31'
		 	or score_14_1 = 'G31'
			or score_15_1 = 'G31'
	then trades = 'G31';

	else if    score_12_1 = 'G32' 
		 	or score_13_1 = 'G32'
		 	or score_14_1 = 'G32'
			or score_15_1 = 'G32'
	then trades = 'G32';

	else if    score_12_1 = 'G33' 
		 	or score_13_1 = 'G33'
		 	or score_14_1 = 'G33'
			or score_15_1 = 'G33'
	then trades = 'G33';

	else if    score_12_1 = 'G34' 
		 	or score_13_1 = 'G34'
		 	or score_14_1 = 'G34'
			or score_15_1 = 'G34'
	then trades = 'G34';

	else if    score_12_1 = 'G35' 
		 	or score_13_1 = 'G35'
		 	or score_14_1 = 'G35'
			or score_15_1 = 'G35'
	then trades = 'G35';

	else if    score_12_1 = 'G36' 
		 	or score_13_1 = 'G36'
		 	or score_14_1 = 'G36'
			or score_15_1 = 'G36'
	then trades = 'G36';

	else trades = 'Non';
run;
			
	
	data R337;
	set fac.'337# Bank Revolving Trades$'n;
	if f1 = '' and hail <> '' then tradehail = 'Non';
	else tradehail = put(f1,4. -L);
	run;

	proc sql;
		create table out.Revolving_Trades_Fac as
		select p.casenumber, p.policynumber, f.hail as hail_trades
		from revolving_trades p left join R337 f
			on p.trades=f.tradehail;
	quit;

%mend;
%RT();