** RULE 205. MINIMUM PREMIUM **;
%macro minprem();
	proc sql;
	create table min_premium as
	select *
	from sashelp.vcolumn
	where libname='FAC' and memname="'205# Minimum Premium$'";
	quit;
	data out.min_premium;
	set min_premium;
	if substr(name,1,1)='$' then min_premium=input(name,dollar10.);
	else delete;
	keep min_premium;
	run;

%if &Process = 0 %then %do;
	title 'RULE 205. MINIMUM PREMIUM';
	proc print data=out.min_premium;
	run;
%end;
%mend;
%minprem();