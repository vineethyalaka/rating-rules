/*** RULE 613. OWNED SNOWMOBILE **;*/
%macro snow();
data r613fac;
set fac.'613# Owned Snowmobile$'n;
if f1="" then delete;
fac1='Liability - Bodily injury (Perso'n*1;
fac2='Liability - Bodily injury (Medic'n*1;
fac3='Liability - Property Damage to O'n*1;
run;
data snowmobile; 
set in.endorsement_info (keep=casenumber policynumber HO_2464_Make_Model_01 - HO_2464_Make_Model_03 HO_2464_FLAG);
array model (3) HO_2464_Make_Model_01 - HO_2464_Make_Model_03;
num_snowmobile = 0;
if HO_2464_FLAG="1" then do i = 1 to 3;
	if model(i)="" then num_snowmobile=num_snowmobile;
	else num_snowmobile=num_snowmobile+1;
end;
drop i;
run;
proc sql;
    create table out.lia_snowmobile_fac as
	select p.*
		, f.fac1 * num_snowmobile as liabipi_snowmobile
		, f.fac2 * num_snowmobile as liabimp_snowmobile
		, f.fac3 * num_snowmobile as liapd_snowmobile
	from r613fac f, snowmobile p;
quit;
%if &Process = 0 %then %do;
title 'RULE 613. OWNED SNOWMOBILE';
proc freq data=out.lia_snowmobile_fac;
tables num_snowmobile
	*liabipi_snowmobile
	*liabimp_snowmobile
	*liapd_snowmobile / list missing;
run;
%end;
%mend;
%snow();