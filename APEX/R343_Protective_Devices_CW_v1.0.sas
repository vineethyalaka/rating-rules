%macro PD();
	data protective_dev_fac;
	set fac.'343# Protective Devices$'n;
	if fire=. then delete;
	run;
	data protective_dev;
	set in.policy_info (keep= casenumber policynumber ProtDevBurglarAlarm ProtDevFireAlarm ProtDevSprinklers TXSprinkler);
	length Sprinkler $50. BurglarAlarm $50. FireAlarm $50.;
	if 		ProtDevSprinklers = 'Y' or TXSprinkler = 1	then Sprinkler = 'Sprinkler';
	if 		ProtDevBurglarAlarm = 'Central' then BurglarAlarm = 'Central Burglary Alarm';
	else if ProtDevBurglarAlarm = 'Direct' 	then BurglarAlarm = 'Direct Line to Police Station';
	if 		ProtDevFireAlarm = 'Central' 	then FireAlarm = 'Central Fire Alarm';
	else if ProtDevFireAlarm = 'Direct'	 	then FireAlarm = 'Direct Line to Fire Station';
	run;
	proc sql;
		create table out.protective_dev_fac as
		select p.*
			, case when ProtDevSprinklers = 'N' and TXSprinkler = 0 then 1 else f1.fire end as fire_sprinklers
			, case when FireAlarm = '' then 1 else f2.fire end as fire_alarm
			, calculated fire_sprinklers * calculated fire_alarm as fire_prot_dev
			, case when BurglarAlarm = '' then 1 else f3.theft end as theft_prot_dev
		from protective_dev p
			left join protective_dev_fac f1 on p.Sprinkler = f1.F1
			left join protective_dev_fac f2 on p.FireAlarm = f2.F1
			left join protective_dev_fac f3 on p.BurglarAlarm = f3.F1
		;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 343. PROTECTIVE DEVICES';
	proc freq data=out.protective_dev_fac;
	tables  ProtDevSprinklers*ProtDevFireAlarm*fire_sprinklers*fire_alarm*fire_prot_dev
			ProtDevBurglarAlarm*theft_prot_dev / list missing;
	run;
%end;
%mend;
%PD();