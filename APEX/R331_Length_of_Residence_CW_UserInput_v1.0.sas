
/*5/13/2019 	WZ ITR 14266 Length of Residence for CA, MA, MD, MT and WA rely on customer information, do not divide by 100*/

%macro LOR();
	data lengthresfac;
	set fac.'331# Length of Residence$'n;
	lengthmin = f1+0;
	lengthmax = f3+0;
	if f1=0 and f3=. then lengthmax=0;
	if f1="Unavailable" and f3=. then do; lengthmin=0; lengthmax=0;end;
	if f1=15.01 and f3=. then lengthmax=9999;
	run;

%if &Existing = 1 %then %do;
	data r331;
	set in.production_info;
/*	if lengthofresidence=0 then lengthofresidence=5;*/
	run;
%end;

%if &Existing = 0 %then %do;
	data r331 ;
	set in.production_info;
	format lengthofresidence 10.2;
	lengthofresidence1 = round(lengthofresidence,1.0);
/*	lengthofresidence1 = lengthofresidence1/100;*/ /*do not divide by 100*/
/*	if lengthofresidence1=0 then lengthofresidence1=5;*/
	drop lengthofresidence;
	rename lengthofresidence1=lengthofresidence;
	run;
%end;


	proc sql;
		create table out.length_res_fac as
		select p.casenumber, p.policynumber, p.lengthofresidence label='length of residence', p.lengthofresidenceind 
			, f.theft as theft_length_res
			, f.'Water Non-Weather'n as waternw_length_res
			, f.'Expense Fee'n as expense_length_res
		from r331 p left join lengthresfac f
			on p.lengthofresidence >= f.lengthmin
			and p.lengthofresidence <= f.lengthmax;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 331. LENGTH OF RESIDENCE';
	proc print data=out.length_res_fac;
	where theft_length_res*waternw_length_res*expense_length_res = .;
	run;
	proc gplot data=out.length_res_fac;
		plot (theft_length_res waternw_length_res expense_length_res)*lengthofresidence 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.length_res_fac;
		histogram lengthofresidence;
	run;
%end;
%mend;
%LOR();