/*12/20/2018 WZ 	Change all column format to number with Excel Macro*/

** RULE 320. PRESENCE OF POOL **;
%macro POP();
	proc sql;
		create table out.pool_fac as
		select p.casenumber, p.policynumber, p.poolpresent
			, f.theft as theft_pool
			, f.'Water Non-Weather'n as waternw_pool
			, f.'All Other Non-Weather'n as othernw_pool
			, f.lightning as lightning_pool
			, f.'Liability - Bodily injury (Perso'n as liabipi_pool
			, f.'Liability - Bodily injury (Medic'n as liabimp_pool
		from in.policy_info p left join fac.'320# Presence of Pool$'n f
			on p.poolpresent=substr(f1,1,1);
	quit;

%if &Process = 0 %then %do;
	title 'RULE 320. PRESENCE OF POOL';
	proc freq data=out.pool_fac;
	tables poolpresent
		*theft_pool
		*waternw_pool
		*othernw_pool
		*lightning_pool 
		*liabipi_pool
		*liabimp_pool/ list missing;
	run;
%end;
%mend();
%POP();
