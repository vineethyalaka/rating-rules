** RULE 329. NUMBER OF STORIES **;
/*2019/11/25 TM Expand perils*/
%macro NOS();
	
	data numstories;
	set in.policy_info;
	if propertystories < 2 then numstories='Less than 2'; else numstories='2 or More';
	run;

	data numstoriesfac;
	set fac.'329# Number of Stories$'n;
	run;


	proc sql;
		create table out.num_stories_fac as
		select p.casenumber, p.policynumber, p.propertystories
			
		, f.fire as fire_num_stories
			, f.theft as theft_num_stories
			, f.'Water Non-Weather'n as waternw_num_stories
			, f.'All Other Non-Weather'n as othernw_num_stories
			, f.lightning as lightning_num_stories
			, f.'Water Weather'n as waterw_num_stories
			, f.Wind as Wind_num_stories
			, f.Hail as Hail_num_stories
			, f.Hurricane as Hurr_num_stories
			, f.'Liability - Bodily injury (Perso'n as liabipi_num_stories
			, f.'Liability - Bodily Injury (Medic'n as liabimp_num_stories
			, f.'Liability - Property Damage to O'n as liapd_num_stories
			, f.'Water Back-up'n as waterbu_num_stories

		from numstories p left join numstoriesfac f
			on p.numstories=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 329. NUMBER OF STORIES';
	proc freq data=out.num_stories_fac;
	tables PropertyStories
		*waternw_num_stories / list missing;
	run;
%end;

%mend;
%NOS();