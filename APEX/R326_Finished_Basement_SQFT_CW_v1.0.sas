
** RULE 326. FINISHED BASEMENT SQFT **;
%macro FBSQFT();
	data finishbasefac;
	set fac.'326# Finished Basement Sq Ft$'n;
	if f1=0 and f3=. then basemax=0;
	else if f1>0 and f3=. then basemax=99999;
	else basemax=f3;
	run;
	proc sql;
		create table out.finished_bsmnt_fac as
		select p.casenumber, p.policynumber, p.propertyfinishedbasementsqft
			, f.'Water Non-Weather'n as waternw_fin_bsmnt
			, f.'Water Back-up'n as waterbu_fin_bsmnt
		from in.policy_info p left join finishbasefac f
			on p.propertyfinishedbasementsqft>=f.f1
			and p.propertyfinishedbasementsqft<=f.basemax;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 326. FINISHED BASEMENT SQFT';
	proc print data=out.finished_bsmnt_fac;
	where waternw_fin_bsmnt*waterbu_fin_bsmnt = .;
	run;
	proc gplot data=out.finished_bsmnt_fac;
		plot (waternw_fin_bsmnt waterbu_fin_bsmnt)*propertyfinishedbasementsqft 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
%end;
%mend;
%FBSQFT();