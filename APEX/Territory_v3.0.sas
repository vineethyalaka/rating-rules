proc sql;
select casenumber into : casenumber separated by ',' from in.policy_info;
quit;

proc sql;
create table out.territory as
select 
policynumber,
casenumber,
put(FIPSID,z5.) as countyid,
CensusBlockID as census_block_group_id,
m.GridID,
m.locationid
from &server..v3ratingrequest as a
inner join work.Grid_HO3_CSV as m
on a.gridid=m.gridid
where casenumber in (&casenumber);
quit;
