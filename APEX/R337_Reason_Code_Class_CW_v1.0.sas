/*2018 12 03	WZ	Create CW version that apply Rule 337 for score model type 3, for other score model, apply factor 1*/
%macro ReasonCode();
data R337;
set fac.'337# Reason Code Class$'n;
if F1=. then delete;
reason_class = put(f1, 4. -L);
run;

proc sql;
     create table reason_code_class as
	 select i.*, i.SCORE_12_1||","||i.SCORE_13_1||","||i.SCORE_14_1||","||i.SCORE_15_1 as score, i.ScoreModel,
		 case 
		 	  when i.ScoreModel ne 3 then 'N'	/*if not TRC ScoreModel 3, assign 'N' and factor 1 later*/
		      when index(calculated score,'G30') ne 0 then '1'
			  when index(calculated score,'G31') ne 0 then '1'
			  when index(calculated score,'G32') ne 0 then '2'
			  when index(calculated score,'G33') ne 0 then '3'
			  when index(calculated score,'G34') ne 0 then '4'
			  when index(calculated score,'G35') ne 0 then '4'
              when index(calculated score,'G36') ne 0 then '4'
			  else '1' 
		end as r_class
	from in.drc_info i;



	/*No factor rate for r_class ='N' in r337 rate sheet, so assign 1 to this class.*/
    insert into R337
	set hail = 1,
		reason_class ='N';
    create table out.Reason_Code_Fac as
	select p.casenumber, p.policynumber, f.hail as hail_reason
	from reason_code_class p left join R337 f
	on p.r_class = f.reason_class;
quit;


%mend;
%ReasonCode();

