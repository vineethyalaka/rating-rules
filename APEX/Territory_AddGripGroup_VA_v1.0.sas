/*proc sql;*/
/*select casenumber into : casenumber separated by ',' from in.policy_info;*/
/*quit;*/




proc import datafile="\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\VA\v1563_14151_\03_Data\Factors\VA_HO3_v05_20190205_UW2_5k500mLocIDs.csv"
	out=territory_ho3
	dbms=csv
	replace;
run;
/*proc import datafile="\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\VA\vpropose_14151\03_Data\Factors\VA_HO4_v05_20190205_UW7.csv"*/
/*	out=territory_ho4*/
/*	dbms=csv*/
/*	replace;*/
/*run;*/
/*proc import datafile="\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\VA\vpropose_14151\03_Data\Factors\VA_HO6_v05_20190205_UW7.csv"*/
/*	out=territory_ho6*/
/*	dbms=csv*/
/*	replace;*/
/*run;*/

/*proc sql;*/
/*create table out.territory as */
/*select 	p.CaseNumber,*/
/*		p.PolicyNumber,*/
/*		p.PolicyTermYears,*/
/*        p.PolicyEffectiveDate,*/
/*		p.CountyID,*/
/*		p.County,*/
/*		p.Census2010 as census_block_group_ID,*/
/*		m.locationid, */
/*		case when p.PolicyFormNumber = 3 then t1.GridGroup*/
/*			 when p.PolicyFormNumber = 4 then t2.GridGroup*/
/*		     when p.PolicyFormNumber = 6 then t3.GridGroup*/
/*		end as GridGroup*/
/*from	in.policy_info p*/
/*inner join &server..v3ratingresult as m on a.casenumber=m.casenumber*/
/*left join territory_ho3 t1 on p.Grid_ITindex = t1.GridID and p.PolicyFormNumber = t1.FormCode*/
/*left join territory_ho4 t2 on p.Grid_ITindex = t2.GridID and p.PolicyFormNumber = t2.FormCode*/
/*left join territory_ho6 t3 on p.Grid_ITindex = t3.GridID and p.PolicyFormNumber = t3.FormCode*/
/**/
/*;*/
/*quit;*/

proc sql;
create table out.territory as
select 
a.policynumber,
a.casenumber,
put(a.FIPSID,z5.) as countyid,
a.CensusBlockID as census_block_group_id,
a.GridID,
m.locationid,
t.GridGroup
/*case when a.FormNumber = 3 then t1.GridGroup*/
/*	 when a.FormNumber = 4 then t2.GridGroup*/
/*	 when a.FormNumber = 6 then t3.GridGroup*/
/*end as GridGroup*/
from &server..v3ratingrequest as a
inner join &server..v3ratingresult as m on a.casenumber=m.casenumber
left join territory_ho3 t on a.GridID = t.GridID 
/*and a.FormNumber = t.FormCode*/
/*left join territory_ho4 t2 on a.GridID = t2.GridID and a.FormNumber = t2.FormCode*/
/*left join territory_ho6 t3 on a.GridID = t3.GridID and a.FormNumber = t3.FormCode*/
/*where a.casenumber in (&casenumber);*/
where a.casenumber in (select casenumber from in.policy_info);
quit;
