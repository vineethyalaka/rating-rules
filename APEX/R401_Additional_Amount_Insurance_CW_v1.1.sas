** RULE 401. ADDITIONAL AMOUNT OF INSURANCE **;
** 2019DEC10 Seperate One Factor to Specific Factors **;
 
%macro add();
	data r401;
	set fac.'401# Addl Amount of Insurance$'n;
	pct='Additional Amount of Insurance'n*100;
	
	run;
	proc sql;
		create 	table out.r401_add_amt_ins as
		select 	p.casenumber, p.policynumber, p.HO_04_20_pct
			  	, d.fire_disc_base_premium as fire_disc_base_premium				, f.Fire as Fire_Additional_Factor
				, d.waternw_disc_base_premium as waternw_disc_base_premium			, f.'Water Non-Weather'n as Waternw_Additional_Factor
				, d.othernw_disc_base_premium as othernw_disc_base_premium			, f.'All Other Non-Weather'n as Othernw_Additional_Factor
				, d.lightning_disc_base_premium as lightning_disc_base_premium		, f.Lightning as Lightning_Additional_Factor
				, d.waterw_disc_base_premium as waterw_disc_base_premium			, f.'Water Weather'n as Waterw_Additional_Factor
				, d.wind_disc_base_premium as wind_disc_base_premium				, f.Wind as Wind_Additional_Factor
				, d.wildfire_disc_base_premium as wildfire_disc_base_premium		, f.Wildfire as Wildfire_Additional_Factor

				, case 
				when p.HO_04_20_pct = 0 then 0 

				else round( fire_disc_base_premium * (Fire_Additional_Factor - 1)
					 + waternw_disc_base_premium * (Waternw_Additional_Factor - 1)
					 + othernw_disc_base_premium * (Othernw_Additional_Factor - 1)
					 + lightning_disc_base_premium * (Lightning_Additional_Factor - 1)
					 + waterw_disc_base_premium * (Waterw_Additional_Factor - 1)
					 + wind_disc_base_premium * (Wind_Additional_Factor - 1)
					 + wildfire_disc_base_premium * (Wildfire_Additional_Factor - 1), .0001) 
				 end as r401_Additional_Amount_Insurance

		  from 	in.Endorsement_Info p 
				left join r401 as f
				on p.HO_04_20_pct=f.pct 

				left join out.step2_base_premium as d
				on p.CaseNumber = d.CaseNumber;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 401. ADDITIONAL AMOUNT OF INSURANCE';
	proc freq data=out.r401_add_amt_ins;
	tables HO_04_20_pct*r401_HO_04_20_fac / list missing;
	run;
%end;
%mend;
%add();
