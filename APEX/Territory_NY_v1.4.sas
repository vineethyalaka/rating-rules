/*Do not change the path, Hard code path for territory and CBG table*/
/*%let Terr_Def_FilePath = %str(\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\NY\v_14866\Documentation\NY Owners Territory Definition HNY II 1.0 (Clean).xlsx);*/
/*%include "\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\NY\v_14866\02_Programs\Territory_Def_Transformation_v3.sas";*/

/*Wanwan: Using Attribute table only instead */

proc import datafile="&loc.\03_Data\Factors\Terr_CBG_final.csv"
	out=TerrCBG
	dbms=csv
	replace;
run;
proc import datafile="\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\NY\v_14866\03_Data\Factors\NY_HO3_V5a_20191108.csv"
	out=territory
	dbms=csv
	replace;
run;

proc sql;
create table out.territory as 
select 	p.CaseNumber,
		p.PolicyNumber,
		p.PolicyTermYears,
        p.PolicyEffectiveDate,
		p.CountyID,
		p.County,
		t.*,
		c.CBG_Modifier as Census_Block_Group_Modifier,
		t.locationid as territory
from	in.policy_info p 
left join territory t on p.Grid_ITindex = t.GridID
left join TerrCBG c on INPUT(p.CBG, 12.) = c.CBG and t.locationid = c.proposedterr /*Di: added t.locationid = c.territory becauase cbg depends on both territory and census block group id, e.g.360594065014
*/
;
quit;


; 