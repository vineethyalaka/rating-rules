** RULE 515.A INCREASED COVERAGE C ***;
** Added wildfire and seperated Coverage into 515 rule **;

%macro incc();
	data r515a;
	set fac.'515#A Increased Cov C$'n;
	if strip(f1) = '' then delete;
	rename 'Coverage Limit Increment (i#e# r'n = Increment;
	run;
	proc sql;
		create table out.Incr_CovC_rate as
		select p.*
			, p.Incr_CovC*f.Rate/Increment as Incr_CovC_rate
		from coverage_c p, r515a f;
	quit;

	*Increased Coverage C Base Premium*;
	proc sql;
		create table out.incr_covc_base_premium as
		select f1.*, f2.theft_disc_base_premium, f3.theft_covcbase, f4.theft_covc, f5.r403_HO_04_90_fac
			, f.CoverageA, f.CoverageB, f3.base_covC, f.coverageD, f2.wildfire_disc_base_premium 
			, case when f1.Incr_CovC <= 0 then 0 else
					(f1.Incr_CovC_rate
					+ ( f2.theft_disc_base_premium
					/ f3.theft_covcbase
					* f4.theft_covc)
					- f2.theft_disc_base_premium 
					+ ( f2.wildfire_disc_base_premium 
					* (f.coverageC - f3.base_covC) 
					/ (f.CoverageA + f.CoverageB + f3.base_covC + f.coverageD)
					)) * f5.r403_HO_04_90_fac 
			end as incr_covc_base_premium_nr
			, round(calculated incr_covc_base_premium_nr, 1.0) as incr_covc_base_premium

		from out.Incr_CovC_rate f1
			inner join in.policy_info			f  on f1.casenumber=f.casenumber
			inner join out.step2_base_premium	f2 on f1.casenumber=f2.casenumber
			inner join out.coverage_c_base_fac	f3 on f1.casenumber=f3.casenumber
			inner join out.coverage_c_fac		f4 on f1.casenumber=f4.casenumber
			inner join out.r403_CovC_Repl_Cost	f5 on f1.casenumber=f5.casenumber;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 515.A INCREASED COVERAGE C';
	proc print data=out.incr_covc_base_premium;
	where incr_covc_base_premium = .;
	run;
	proc gplot data=out.incr_covc_base_premium;
		plot incr_covc_base_premium*Incr_CovC 
			/ overlay legend vaxis=axis2;
	run;
	quit; 
	proc sql;
		select mean(incr_covc_base_premium) as incr_covc_base_prem_avg
		from out.incr_covc_base_premium;
	quit;
%end;
%mend;
%incc();