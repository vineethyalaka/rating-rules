** RULE 350. BASE EXPENSE FACTOR **;
%macro exp();
	data out.base_exp_fac;
	set fac.'350# Base Expense Factor$'n;
	if strip(F1) = '' then delete;
	expense_base_fac = 'Expense Fee'n*1;
	drop f3-f8;
	run;

%if &Process = 0 %then %do;
	title 'RULE 350. BASE EXPENSE FACTOR';
	proc print data=out.base_exp_fac;
	run;
%end;
%mend;
%exp();
