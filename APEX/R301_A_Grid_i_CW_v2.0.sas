** RULE 301.A GRID **;

**=====================================================================================**
History: 2017 10 12 JK- Based off version 1.1, updating to use new tab name and GridID for APEX 2.0
**=====================================================================================**;

%macro grid();

	data GridID;
	set fac.'301#A Location ID_i$'n;
	if vtype(F1)='C' then GridID = input(F1,7.);
		else GridID = F1;
	run;

	proc sql;
		create table out.grid_fac as
		select p.casenumber, p.policynumber, p.GridID
			, f.'Water Non-Weather'n as waternw_grid
			, f.'Water Weather'n as waterw_grid
			, f.Wind as wind_grid
			, f.hail as hail_grid
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_grid
			, f.'Liability - Bodily injury (Perso'n as liabipi_grid
			, f.'Liability - Bodily injury (Medic'n as liabimp_grid		
			, f.'Water Back-up'n as waterbu_grid
		from out.territory p left join GridID f 
			on p.GridID = f.GridID; 
;
	quit;


%if &Process = 0 %then %do;
	title 'RULE 301.A GRID';
	proc sql;
		select distinct gridID
			, waternw_grid
			, waterw_grid
			, wind_grid
			, hail_grid
			, hurr_grid
			, liabipi_grid
			, liabimp_grid
			, waterbu_grid
		from out.grid_fac;
	quit;
%end;
%mend;
%grid();