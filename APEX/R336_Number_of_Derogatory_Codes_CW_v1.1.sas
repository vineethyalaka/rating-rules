** RULE 362. NUMBER	OF DEROGATORY CODES**;
**=====================================================================================**
History: 2017 07 20 BF  Initial draft
		 2017 08 21 BF	Changed so it only counts DRC that start with D
**=====================================================================================**;

/*If number of drc codes becomes a field, we can pull it in with this code*/
/*Until then use the bottom code to drc_fac*/

/*%macro NDRC();*/
/*	data Number_of_derogatory_codes;*/
/*	set in.drc_info;*/
/*	if propertystatecode='2' then drcfac = '2+';*/
/*	else if propertystatecode='' then drcfac = '0';*/
/*	else if propertystatecode = '1' then drcfac = '1';*/
/*	else drcfac = '2+';*/
/*	run;*/
/*	*/
/*	data R362;*/
/*	set fac.'362# Number of Derogatory Codes$'n;*/
/*	run;*/
/**/
/*	proc sql;*/
/*		create table out.Num_Der_Code as*/
/*		select p.casenumber, p.policynumber, f.lightning as lightning_NDRC*/
/*		from Number_of_derogatory_codes p left join fac.'362# Number of Derogatory Codes$'n f*/
/*			on p.drcfac=f.f1;*/
/*	quit;*/
/**/
/*%mend;*/
/*%NDRC();*/






%macro NDRC();
	data Number_of_derogatory_codes;
	set in.drc_info;
	drc_fac = 0;
	if substr(score_12_1,1,1) = 'D' then drc_fac = drc_fac +1;
	if substr(score_13_1,1,1) = 'D' then drc_fac = drc_fac +1;
	if substr(score_14_1,1,1) = 'D' then drc_fac = drc_fac +1;
	if substr(score_15_1,1,1) = 'D' then drc_fac = drc_fac +1;
	run;
	
	data R336;
	set fac.'336# Number of Derogatory Codes$'n;
	run;

	proc sql;
		create table out.Num_Der_Code as
		select p.casenumber, p.policynumber, f.lightning as lightning_NDRC
		from Number_of_derogatory_codes p left join fac.'336# Number of Derogatory Codes$'n f
			on p.drc_fac=f.f1;
	quit;

%mend;
%NDRC();