/* ----------------------------------------
Code exported from SAS Enterprise Guide
DATE: Friday, September 23, 2016     TIME: 11:57:52 AM
PROJECT: R346_Responsible_Motorits_MD_v1_0_sas
PROJECT PATH: C:\Users\di.wu\Desktop\R346_Responsible_Motorits_MD_v1_0_sas.egp
---------------------------------------- */

/* ---------------------------------- */
/* MACRO: enterpriseguide             */
/* PURPOSE: define a macro variable   */
/*   that contains the file system    */
/*   path of the WORK library on the  */
/*   server.  Note that different     */
/*   logic is needed depending on the */
/*   server type.                     */
/* ---------------------------------- */
%macro enterpriseguide;
%global sasworklocation;
%local tempdsn unique_dsn path;

%if &sysscp=OS %then %do; /* MVS Server */
	%if %sysfunc(getoption(filesystem))=MVS %then %do;
        /* By default, physical file name will be considered a classic MVS data set. */
	    /* Construct dsn that will be unique for each concurrent session under a particular account: */
		filename egtemp '&egtemp' disp=(new,delete); /* create a temporary data set */
 		%let tempdsn=%sysfunc(pathname(egtemp)); /* get dsn */
		filename egtemp clear; /* get rid of data set - we only wanted its name */
		%let unique_dsn=".EGTEMP.%substr(&tempdsn, 1, 16).PDSE"; 
		filename egtmpdir &unique_dsn
			disp=(new,delete,delete) space=(cyl,(5,5,50))
			dsorg=po dsntype=library recfm=vb
			lrecl=8000 blksize=8004 ;
		options fileext=ignore ;
	%end; 
 	%else %do; 
        /* 
		By default, physical file name will be considered an HFS 
		(hierarchical file system) file. 
		*/
		%if "%sysfunc(getoption(filetempdir))"="" %then %do;
			filename egtmpdir '/tmp';
		%end;
		%else %do;
			filename egtmpdir "%sysfunc(getoption(filetempdir))";
		%end;
	%end; 
	%let path=%sysfunc(pathname(egtmpdir));
    %let sasworklocation=%sysfunc(quote(&path));  
%end; /* MVS Server */
%else %do;
	%let sasworklocation = "%sysfunc(getoption(work))/";
%end;
%if &sysscp=VMS_AXP %then %do; /* Alpha VMS server */
	%let sasworklocation = "%sysfunc(getoption(work))";                         
%end;
%if &sysscp=CMS %then %do; 
	%let path = %sysfunc(getoption(work));                         
	%let sasworklocation = "%substr(&path, %index(&path,%str( )))";
%end;
%mend enterpriseguide;

%enterpriseguide


/* Conditionally delete set of tables or views, if they exists          */
/* If the member does not exist, then no action is performed   */
%macro _eg_conditional_dropds /parmbuff;
	
   	%local num;
   	%local stepneeded;
   	%local stepstarted;
   	%local dsname;
	%local name;

   	%let num=1;
	/* flags to determine whether a PROC SQL step is needed */
	/* or even started yet                                  */
	%let stepneeded=0;
	%let stepstarted=0;
   	%let dsname= %qscan(&syspbuff,&num,',()');
	%do %while(&dsname ne);	
		%let name = %sysfunc(left(&dsname));
		%if %qsysfunc(exist(&name)) %then %do;
			%let stepneeded=1;
			%if (&stepstarted eq 0) %then %do;
				proc sql;
				%let stepstarted=1;

			%end;
				drop table &name;
		%end;

		%if %sysfunc(exist(&name,view)) %then %do;
			%let stepneeded=1;
			%if (&stepstarted eq 0) %then %do;
				proc sql;
				%let stepstarted=1;
			%end;
				drop view &name;
		%end;
		%let num=%eval(&num+1);
      	%let dsname=%qscan(&syspbuff,&num,',()');
	%end;
	%if &stepstarted %then %do;
		quit;
	%end;
%mend _eg_conditional_dropds;

/* save the current settings of XPIXELS and YPIXELS */
/* so that they can be restored later               */
%macro _sas_pushchartsize(new_xsize, new_ysize);
	%global _savedxpixels _savedypixels;
	options nonotes;
	proc sql noprint;
	select setting into :_savedxpixels
	from sashelp.vgopt
	where optname eq "XPIXELS";
	select setting into :_savedypixels
	from sashelp.vgopt
	where optname eq "YPIXELS";
	quit;
	options notes;
	GOPTIONS XPIXELS=&new_xsize YPIXELS=&new_ysize;
%mend;

/* restore the previous values for XPIXELS and YPIXELS */
%macro _sas_popchartsize;
	%if %symexist(_savedxpixels) %then %do;
		GOPTIONS XPIXELS=&_savedxpixels YPIXELS=&_savedypixels;
		%symdel _savedxpixels / nowarn;
		%symdel _savedypixels / nowarn;
	%end;
%mend;

ODS PROCTITLE;
OPTIONS DEV=ACTIVEX;
GOPTIONS XPIXELS=0 YPIXELS=0;
FILENAME EGSRX TEMP;
ODS tagsets.sasreport13(ID=EGSRX) FILE=EGSRX
    STYLE=HtmlBlue
    STYLESHEET=(URL="file:///C:/Program%20Files/SASHome/SASEnterpriseGuide/6.1/Styles/HtmlBlue.css")
    NOGTITLE
    NOGFOOTNOTE
    GPATH=&sasworklocation
    ENCODING=UTF8
    options(rolap="on")
;

/*   START OF NODE: Program1   */
%LET _CLIENTTASKLABEL='Program1';
%LET _CLIENTPROJECTPATH='C:\Users\di.wu\Desktop\R346_Responsible_Motorits_MD_v1_0_sas.egp';
%LET _CLIENTPROJECTNAME='R346_Responsible_Motorits_MD_v1_0_sas.egp';
%LET _SASPROGRAMFILE=;

GOPTIONS ACCESSIBLE;
*Rule 346 Responsible Motorist;




proc sql outobs = 1;;
select p.RatingVersion into :Rating_Version 
from in.POLICY_INFO p
;
quit;

/*proc sql;*/
/*create table work.Autoclaim_lookup as*/
/*select a.policynumber ,a.casenumber, a.policytermyears, yrdif(datepart(b.autolossdate), datepart(datetime()), 'ACT/ACT') as year_difference, case when yrdif(datepart(b.autolossdate), datepart(datetime()), 'ACT/ACT') <=3 then b.autolosscode else " " end as autolosscode, b.autolossdate , b.autolossamount from in.policy_info as a*/
/*left join &server..HS_APRating_RequestAutoClaims as b*/
/*on a.casenumber=b.casenumber;*/
/*quit;*/
/*use original quote date*/
proc sql;
create table work.Autoclaim_lookup as
select a.policynumber ,a.casenumber, a.policytermyears, yrdif(datepart(b.autolossdate), datepart(a.CLUE_REPORT_REQUEST_DATE), 'ACT/ACT') as year_difference, case when calculated year_difference<=3 then b.autolosscode else " " end as autolosscode, a.CLUE_REPORT_REQUEST_DATE,b.autolossdate , b.autolossamount from in.policy_info as a
left join &server..HS_APRating_RequestAutoClaims as b
on a.casenumber=b.casenumber;
quit;

proc sql;
create table work.Autoclaim_Losscode as
select autolosscode, peril from &server..HS_APRating_AutoLossCode_Peril
where version=&Rating_Version;
quit;

data peril_list;
input peril $ num;
cards;
P1 1
P2 2
P3 3
P4 4
P5 5
P6 6
P7 7
P8 8
P9 9
;

proc sql;
create table work.Autoclaim_lookup2 as
select a.*, b.autolosscode, c.peril, c.num from work.autoclaim_lookup as a 
left join work.autoclaim_losscode as b
on a.autolosscode=b.autolosscode
left join work.peril_list as c
on b.peril=c.num
order by a.policynumber;
quit;

/* important: summarize # of perils */
proc sql;
create table work.Autoclaim_Group as
select policynumber, casenumber, policytermyears, peril, count(num) as peril_count from work.Autoclaim_lookup2
group by policynumber, casenumber, peril
order by policynumber;
quit;

proc sort data=work.Autoclaim_Group;
by policynumber casenumber;
run;

data work.Autoclaim_Group_Rotated(drop=peril peril_count i);
set work.Autoclaim_Group;
by PolicyNumber casenumber;
array mark{7} P_1 P_2 P_3 P_4 P_5 P_6 P_8;
retain mark(0);
if peril=" " then do;
	do i=1 to 7;
		mark{i}=0;
	end;
end; /*there is only one row, output directly*/
else do;
	select (peril);
		when ('P1') P_1=peril_count;
		when ('P2') P_2=peril_count;
		when ('P3') P_3=peril_count;
		when ('P4') P_4=peril_count;
		when ('P5') P_5=peril_count;
		when ('P6') P_6=peril_count;
		when ('P8') P_8=peril_count;
		otherwise;
	end;
end;

if last.casenumber then do;
	output;
		do i=1 to 7;
			mark{i}=0;
		end;
	end;

run;

data r346fac;
set fac.'346# Responsible Motorist$'n;
if Fire=1 then F1=5;
else if Fire=. then delete;
drop F9 F10;
run;

proc sql;
create table out.res_motor_fac as
select 
a.*,
/* if it's marked, corresponding factor should be 1*/
case when P_1>0 then 1 else b.Fire end as fire_resmotor,
case when P_2>0 then 1 else b.Theft end as theft_resmotor,
case when P_3>0 then 1 else b.'Water Non-Weather'n end as waternw_resmotor,
case when P_4>0 then 1 else b.'All Other Non-Weather'n end as othernw_resmotor,
case when P_5>0 then 1 else b.Lightning end as lightning_resmotor,
case when P_6>0 then 1 else b.'Water Weather'n end as waterw_resmotor,
case when P_8>0 then 1 else b.Hail end as hail_resmotor
from Autoclaim_Group_Rotated as a
left join r346fac as b
on (case when a.policytermyears>5 then 5 else a.policytermyears end)=b.F1;
quit;


GOPTIONS NOACCESSIBLE;
%LET _CLIENTTASKLABEL=;
%LET _CLIENTPROJECTPATH=;
%LET _CLIENTPROJECTNAME=;
%LET _SASPROGRAMFILE=;

;*';*";*/;quit;run;
ODS _ALL_ CLOSE;
