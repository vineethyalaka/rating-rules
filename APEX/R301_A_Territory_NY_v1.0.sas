** RULE 301.A CENSUS BLOCK **;
%macro CBG();
	proc sql;
		create table out.census_terr_fac as
		select p.casenumber, p.policynumber, p.LocationID
			, f.fire as fire_censusterr
			, f.theft as theft_censusterr	
			, f.'Water Non-Weather'n as waternw_censusterr
			, f.lightning as lightning_censusterr	
			, f.'Water Weather'n as waterw_censusterr	
			, f.Wind as wind_censusterr
			, f.Hail as hail_censusterr	
			, f.Hurricane as hurricane_censusterr
			, f.'Liability - Bodily injury (Perso'n  as liabipi_censusterr	
			, f.'Liability - Bodily injury (Medic'n as liabimp_censusterr
			, f.'Liability - Property Damage to O'n as liapd_censusterr
			, f.'Water Back-up'n as waterbu_censusterr			
		from out.territory p left join fac.'301#A Territory$'n f 
			on p.LocationID = f.F1;
	quit;

/*%if &Process = 0 %then %do;*/
/*	title 'RULE 301.A CENSUS BLOCK';*/
/*	proc sql;*/
/*		select distinct census_block_group_ID*/
/*			, fire_census*/
/*			, theft_census*/
/*			, waternw_census*/
/*			, lightning_census*/
/*			, wind_census*/
/*			, hail_census*/
/*			, liabipi_census*/
/*			, liabimp_census*/
/*			, liapd_census*/
/*			, waterbu_census*/
/*		from out.census_fac;*/
/*	quit;*/
/*%end;*/

%mend;
%CBG();