** RULE 514.C REDUCED COVERAGE B **;
*2016 11 02 TA Changed the variable PropertyFamiliesResident to PropertyUnitsInBuilding;

/*2019/06/11 TM, change ceil to round as Decr_covB_rate, edit from pricing rule v1.1*/

%macro RB();
	data r514c;
	set fac.'514#C Reduced Coverage B$'n;
	if strip(f1) = '' then delete;
	rename 'Coverage Limit Increment (i#e# c'n = Increment;
	run;
	proc sql;
		create table out.Decr_CovB_rate as
		select p.CaseNumber, 
			   p.PolicyNumber,
			   p.CoverageA,
			   p.CoverageB,
			   p.PropertyUnitsInBuilding,
			   case when p.PropertyUnitsInBuilding in (1,2) 
					then round(p.CoverageB - p.CoverageA*0.1,1) 
					else round(p.CoverageB - p.CoverageA*0.05,1) end as DecrCovB_calc,
				case when calculated DecrCovB_calc > 0 then 0 else calculated DecrCovB_calc end as DecrCovB,
			  round(calculated DecrCovB*f.credit/Increment,1.0) as Decr_CovB_rate
		from in.policy_info p, r514c f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 514.C REDUCED COVERAGE B';
	proc gplot data=out.Decr_CovB_rate;
		plot Decr_CovB_rate*DecrCovB
			/ overlay legend vaxis=axis2;
	run;
	quit; 
%end;
%mend;
%RB();


