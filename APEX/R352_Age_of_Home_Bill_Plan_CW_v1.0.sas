** RULE 352. AGE OF HOME BY BILL PLAN **;
%macro agebill();
	data exp_home_age_x_bill_plan_fac (keep=bill_plan home_age factor);
	length bill_plan $20.;
	set fac.'352# Age of Home by Bill Plan$'n;
	if f3=. then delete;
	home_age = f1*1; if f1 = '101+' then home_age = 101;
	bill_plan='10-pay'; factor = 'Expense Fee'n; output;
	bill_plan='4-pay'; factor = f3; output;
	bill_plan='Full-pay or Mortgage'; factor = f4; output;
	run;
	proc sql;
		create table out.exp_home_age_x_bill_plan_fac as
		select p1.*, p2.home_age, f.factor as exp_home_age_x_bill_plan_fac
		from bill_plan p1 
			inner join home_age p2 on p1.casenumber = p2.casenumber
			left join exp_home_age_x_bill_plan_fac f on f.bill_plan = p1.BillPlanType 
				and f.home_age = p2.home_age;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 352. AGE OF HOME BY BILL PLAN';
	proc print data=out.exp_home_age_x_bill_plan_fac;
	where exp_home_age_x_bill_plan_fac = .;
	run;
	proc sort data=out.exp_home_age_x_bill_plan_fac; by BillPlanType; run;
	proc gplot data=out.exp_home_age_x_bill_plan_fac; by BillPlanType;
		plot exp_home_age_x_bill_plan_fac*home_age 
			/ overlay legend vaxis=axis1;
	run;
	quit;
%end;
%mend;
%agebill();