/*2019/02/12 TM initial draft ITR 13759*/
/* Vendor type is hard coded to '1', due to horison doesnot store vendertype info in table */
/* code need to be modified once horizon has a field to pass vendertype*/

/*2019/08/14 Wanwan ITR 14458 activate the PB Fire Risk */
/*vendor type which is defined by effective date, 
for non-effective states, police renewal date before 9/1 using Corelogic model, after 9/1 using Pitney Bows score model  
for effetive states, the date depends on specific state*/

proc sql;
	create table out.OTW_Wildfire_Factor as
	select p.CaseNumber, p.PolicyNumber, p.county, p.GEOWFScore,
	case when p.GEOWFScore LT 24 or p.GEOWFScore = . then f.low_wildfire_factor /* if wildfire score <24 or null, use low factor*/
	     else f.high_wildfire_factor end as OTW_Wildfire_Factor
	from input3.policy_info p
	left join fac.otwwildfirefactor f
	on p.county = f.county
	;
quit;

