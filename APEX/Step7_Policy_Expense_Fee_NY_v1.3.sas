** POLICY EXPENSE FEE **;
/*BF 10 24 2017 Making fixed expense fee after year 1*/
/*BF 10 25 2017 Capping variable expense fee at base rate for year 1*/
/*BF 11 17 2017 Undoing previous change. Capping at $300 for years 1 and setting fee to $200 for years 2+*/

%macro fee();
	proc sql;
		create table out.step7_policy_expense_fee as
	
		select 
			 f2.casenumber, f2.policynumber, f2.expense_base_rate, f1.expense_base_fac
			, f3.lengthofresidence, f3.expense_length_res
			, f4.new_home_flag, f4.new_home_purch, f4.policytermyears, f4.term, expense_new_home_purch
			, f5.policyissuedate, f5.firstterm, f5.DOB1, f5.cust_age_nb, expense_customer_age_nb
			, f6.BillPlan, f6.MortgageEBillFlag, f6.BillPlanType, f6.lapse, exp_bill_plan_x_lapse_fac
			, f7.home_age, exp_home_age_x_bill_plan_fac
			, f8.matchcode, f8.ScoreModel, f8.insscore, f8.ins_tier


            , exp_tier_x_bill_plan_fac
			, exp_tier_x_lapse_fac
            , exp_home_age_x_tier_fac
            , exp_home_age_x_lapse_fac

			, round(expense_base_rate
				/ (expense_base_fac
				+ expense_length_res
				+ expense_new_home_purch
				+ expense_customer_age_nb
				+ exp_bill_plan_x_lapse_fac
				+ exp_home_age_x_bill_plan_fac

				+ exp_tier_x_bill_plan_fac
				+ exp_tier_x_lapse_fac
				+ exp_home_age_x_tier_fac
				+ exp_home_age_x_lapse_fac),1.0) as policy_expense_fee_before

			, case when f12.policytermyears = 1 then
				case when round(expense_base_rate
				/ (expense_base_fac
				+ expense_length_res
				+ expense_new_home_purch
				+ expense_customer_age_nb
				+ exp_bill_plan_x_lapse_fac
				+ exp_home_age_x_bill_plan_fac


				+ exp_tier_x_bill_plan_fac
				+ exp_tier_x_lapse_fac
				+ exp_home_age_x_tier_fac
				+ exp_home_age_x_lapse_fac),1.0) > 300
					then 300
					else
					round(expense_base_rate
					/ (expense_base_fac
					+ expense_length_res
					+ expense_new_home_purch
					+ expense_customer_age_nb
					+ exp_bill_plan_x_lapse_fac
					+ exp_home_age_x_bill_plan_fac
					+ exp_tier_x_bill_plan_fac
					+ exp_tier_x_lapse_fac
					+ exp_home_age_x_tier_fac
					+ exp_home_age_x_lapse_fac),1.0)
					end
			else 200
			end as policy_expense_fee


			from out.base_exp_fac f1, out.base_rate f2
			inner join out.length_res_fac 	  			 f3 on f2.casenumber=f3.casenumber
			inner join out.new_home_purch_fac 			 f4 on f2.casenumber=f4.casenumber
			inner join out.customer_age_nb_fac			 f5 on f2.casenumber=f5.casenumber
			inner join out.exp_bill_plan_x_lapse_fac     f6 on f2.casenumber=f6.casenumber
			inner join out.exp_home_age_x_bill_plan_fac  f7 on f2.casenumber=f7.casenumber
			inner join out.exp_tier_x_bill_plan_fac 	f8 on f2.casenumber=f8.casenumber
			inner join out.exp_tier_x_lapse_fac	 		f9 on f2.casenumber=f9.casenumber
			inner join out.exp_home_age_x_tier_fac 		f10 on f2.casenumber=f10.casenumber
			inner join out.exp_home_age_x_lapse_fac		f11 on f2.casenumber=f11.casenumber
			inner join in.policy_info					f12 on f2.casenumber=f12.casenumber;

		quit;

%if &Process = 0 %then %do;
	title 'STEP 7: POLICY EXPENSE FEE (RULE 301.A.7)';
	proc print data=out.step7_policy_expense_fee;
	where policy_expense_fee = .;
	run;
	proc sql;
		create table out.average_policy_expense_fee as
		select mean(policy_expense_fee) as policy_expense_fee_avg
		from out.step7_policy_expense_fee;
	quit;
%end;
%mend;
%fee();