/*If casenumber are too many, SAS cannot handel in one variable. Use this version to avoid creating a variable.*/

/*proc sql;*/
/*select casenumber into : casenumber separated by ',' from in.policy_info;*/
/*quit;*/

proc sql;
create table out.territory as
select 
a.policynumber,
a.casenumber,
put(a.FIPSID,z5.) as countyid,
a.CensusBlockID as census_block_group_id,
a.GridID,
m.locationid
from &server..v3ratingrequest as a
inner join &server..v3ratingresult as m
on a.casenumber=m.casenumber
/*where a.casenumber in (&casenumber);*/
where a.casenumber in (select casenumber from in.policy_info);
quit;
