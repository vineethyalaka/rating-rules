** RULE 349. Loss Mitigation Discounts **;
**=====================================================================================**
History: 2015 10 15 ES Initial Draft
		 2015 11 04 SY SC Specific Codes
**=====================================================================================**;
%macro LM();
	data LM_Factor;
	   	set fac.'349# Loss Mitigation Discounts$'n;
		if wind = . then delete;
		retain f3;
	   	if not missing(f1) then f3=f1;
	   	Mitigation = trim(f3)||'_'||trim(f2);
	   	drop f1 f2 f3;
	run;

	data LM_Factor;
	   retain mitigation wind hail hurricane;
	   set LM_Factor;
	run;

	proc sql;
	create table loss_mitigation_info as
	select casenumber
		, policynumber
		, construction_completed
		, ml_70_number_of_families_01
		, Question_code_20
		, comp_engineered_status
		, comp_roof_equipment
		, comp_window_door_protection
		, comp_roof_wall_anchor
		, case when construction_completed = 100 then 'Roof Covering_Reinforced Concrete Roof Deck' 
				else 'Roof Covering_All Other' end as LM_RoofCovering
		, case when ml_70_number_of_families_01 = 1 then 'Wall-to-Foundation Restraint_Wall-to-Foundation Restraint' 
				else 'Wall-to-Foundation Restraint_No Wall-to-Foundation Restraint' end as LM_Wall
		, case when Question_code_20 = 1 then 'Roof Shape_Hip' 
				else 'Roof Shape_Other' end as LM_RoofShape
		, case when comp_engineered_status = 1 then 'Roof-to-Wall Connection_Clips'
				when comp_engineered_status = 2 then 'Roof-to-Wall Connection_Single Wraps'
				when comp_engineered_status = 3 then 'Roof-to-Wall Connection_Double Wraps'
				else 'Roof-to-Wall Connection_Toe Nails' end as LM_RoofToWall
		, case when comp_roof_equipment = 1 then 'Shutters_Hurricane Protection'
				when comp_roof_equipment = 2 then 'Shutters_Intermediate'
				else 'Shutters_None' end as LM_Shutters
		, case when comp_window_door_protection = 1 then 'Roof Attachment_2.5" nail / 12" in field of plywood'
				when comp_window_door_protection = 2 then 'Roof Attachment_2.5" nail / 6" in field of plywood'
				else 'Roof Attachment_2" nail / 12" in field of plywood' end as LM_RoofAttach
		, case when comp_roof_wall_anchor = 1 then 'Secondary Water Resistance_SWR'
				else 'Secondary Water Resistance_None' end as LM_SecondWaterResis
	from in.production_info;
	quit;

	proc sql; 
		create table out.loss_mitigation_fac as 
		select p.*
		, f1.wind		as LM_RoofCovering_wind
		, f1.hail 		as LM_RoofCovering_hail
		, f1.hurricane 	as LM_RoofCovering_hurr
		, f2.wind 		as LM_Wall_wind
		, f2.hail 		as LM_Wall_hail
		, f2.hurricane 	as LM_Wall_hurr
		, f3.wind 		as LM_RoofShape_wind
		, f3.hail 		as LM_RoofShape_hail
		, f3.hurricane 	as LM_RoofShape_hurr
		, f4.wind 		as LM_RoofToWall_wind
		, f4.hail 		as LM_RoofToWall_hail
		, f4.hurricane 	as LM_RoofToWall_hurr
		, f5.wind 		as LM_Shutters_wind
		, f5.hail 		as LM_Shutters_hail
		, f5.hurricane 	as LM_Shutters_hurr
		, f6.wind 		as LM_RoofAttach_wind
		, f6.hail 		as LM_RoofAttach_hail
		, f6.hurricane 	as LM_RoofAttach_hurr
		, f7.wind 		as LM_SecondWaterResis_wind
		, f7.hail 		as LM_SecondWaterResis_hail
		, f7.hurricane 	as LM_SecondWaterResis_hurr
		, round (LM_RoofCovering_wind*LM_Wall_wind*LM_RoofShape_wind*LM_RoofToWall_wind*LM_Shutters_wind*LM_RoofAttach_wind*LM_SecondWaterResis_wind,0.0001) as wind_loss_mitigation
		, round (LM_RoofCovering_hail*LM_Wall_hail*LM_RoofShape_hail*LM_RoofToWall_hail*LM_Shutters_hail*LM_RoofAttach_hail*LM_SecondWaterResis_hail,0.0001) as hail_loss_mitigation
		, round (LM_RoofCovering_hurr*LM_Wall_hurr*LM_RoofShape_hurr*LM_RoofToWall_hurr*LM_Shutters_hurr*LM_RoofAttach_hurr*LM_SecondWaterResis_hurr,0.0001) as hurr_loss_mitigation
		from loss_mitigation_info p
		left join LM_Factor f1 on p.LM_RoofCovering 	= f1.Mitigation
		left join LM_Factor f2 on p.LM_Wall 			= f2.Mitigation
		left join LM_Factor f3 on p.LM_RoofShape 		= f3.Mitigation
		left join LM_Factor f4 on p.LM_RoofToWall 		= f4.Mitigation
		left join LM_Factor f5 on p.LM_Shutters 		= f5.Mitigation
		left join LM_Factor f6 on p.LM_RoofAttach 		= f6.Mitigation
		left join LM_Factor f7 on p.LM_SecondWaterResis	= f7.Mitigation
	;quit;


%if &Process = 0 %then %do;
	title 'RULE 349. LOSS MITIGATION DISCOUNTS';
	proc freq data=out.loss_mitigation_fac;
	tables  wind_loss_mitigation 
			hail_loss_mitigation 
			hurr_loss_mitigation / list missing;
	run;
%end;
%mend;
%LM();





