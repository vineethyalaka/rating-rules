%macro discounted_base(casenumber);
proc sql noprint;
	create table step4_step6 as 
	select s4.*,s6.*
	from out.STEP4_ADJUSTED_SUBTOTAL_PREMIUM s4
	join out.STEP6_LIABILITY_PREMIUM s6
	on s4.casenumber = s6.casenumber
	where s4.casenumber = &casenumber;
quit;

proc contents data=step4_step6 noprint out=_contents_ (keep=name);
run;

proc sql noprint;
	select strip(name), cats(strip(name),'=',tranwrd(strip(tranwrd(strip(name),"_disc_base_premium","")),"_base_premium",""))
	into :STEP4_6_col_list separated by ' ', :STEP4_6_col_list_replaced separated by ' '
	from _contents_
	where index(upcase(name), 'DISC') > 0 or index(upcase(name), 'LIABIPI_BASE_PREMIUM') > 0
	or index(upcase(name), 'LIABIMP_BASE_PREMIUM') > 0 or index(upcase(name), 'LIAPD_BASE_PREMIUM') > 0;
quit;

data Steps;
	set step4_step6 (keep=&STEP4_6_col_list);
	rename &STEP4_6_col_list_replaced.;
run;

proc transpose data=Steps out=Peril_Results;
run;
%mend;