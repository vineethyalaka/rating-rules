** RULE 301.A GRID_II **;

**=====================================================================================**
History: 2017 10 12 JK Based off R301_A_Grid_TX_v1.0
**=====================================================================================**;

%macro grid();
	data hurr_location_fac  (keep=bucket GridID factor);
	set fac.'301#A Location ID_ii$'n;
	if f1=. then delete;
	rename f1 = GridID;
	bucket = 'Prior1995_Group1'; factor = Hurricane; output;
	bucket = 'Prior1995_Group2'; factor = f3; output;
	bucket = '1995-2003_Group1'; factor = f4; output;
	bucket = '1995-2003_Group2'; factor = f5; output;
	bucket = 'Since2003_Group1'; factor = f6; output;
	bucket = 'Since2003_Group2'; factor = f7; output;
	run;

	data hurr_loc;
	set in.policy_info;
	rename Grid_ITindex = GridID;
	if propertyrooftypecode='Wood shake' then propertyrooftypecode='Wood shingles/shakes';
	if propertyrooftypecode='Wood shingles' then propertyrooftypecode='Wood shingles/shakes';
	if lowcase(propertyrooftypecode) in (' ','flat','gambrel','hip','mansard','plexiglass','truss-joist') then propertyrooftypecode='All Other';
/*	if propertyyearroofinstalled <1995 then propertyyearroofbucket='Prior1995';*/
/*	else if propertyyearroofinstalled >=1995 and propertyyearroofinstalled <=2003 then propertyyearroofbucket='1995-2003';*/
/*	else propertyyearroofbucket='Since2003';*/
	if PropertyYearBuilt <1995 then propertyyearroofbucket='Prior1995';
	else if PropertyYearBuilt >=1995 and PropertyYearBuilt <=2003 then propertyyearroofbucket='1995-2003';
	else propertyyearroofbucket='Since2003';

run;


	proc sql;
		create table hurr_loc_2 as 
		select *,
		case when propertyrooftypecode in ("Clay tile", "Concrete tile", "Reinforced Concrete", "Slate", "Spanish tile", "Tin/Membrane") then "Group2" else "Group1" end as Roof_group
		from hurr_loc;
	quit;

	proc sql;
		create table hurr_loc_3 as 
		select *,
		propertyyearroofbucket||"_"||roof_group as hurr_loc_type
		from hurr_loc_2
	quit;

	proc sql;
		create table out.hurr_loc_fac as
		select distinct b.casenumber, b.policynumber, b.GridID, b.propertyyearroofinstalled, b.PropertyYearBuilt,
		b.propertyrooftypecode, b.propertyyearroofbucket, b.roof_group, b.hurr_loc_type, f.factor as hurr_loc_factor
		from hurr_location_fac f 
		inner join hurr_loc_3 b on b.GridID = f.GridID
		and b.hurr_loc_type = f.bucket;
	quit;


%if &Process = 0 %then %do;
	title 'RULE 301.A Location ID_ii';
	proc freq data=out.building_type_fac;
	tables
		*propertyrooftypecode/ list missing;
	quit;
%end;
%mend;
%grid();