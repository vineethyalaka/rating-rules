** RULE 318. TOTAL ROOMS WITH BATH **;
%macro bath();
	data bathrooms;
	set in.policy_info;
	bathrooms=propertynumberoffullbaths+propertynumberofhalfbaths;
	if bathrooms>6 then bathrooms=6;
	run;
	data bathroom_fac;
	set fac.'318# Total Rooms with Bath$'n;
	if f1=. and 'Water Non-Weather'n not in(.,1) then f1=6;
	run;
	proc sql;
		create table out.bathroom_fac as
		select p.casenumber, p.policynumber, p.propertynumberoffullbaths, p.propertynumberofhalfbaths, p.bathrooms
			, f.'Water Non-Weather'n as waternw_bathroom
		from bathrooms p left join bathroom_fac f
			on p.bathrooms=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 318. TOTAL ROOMS WITH BATH';
	proc freq data=out.bathroom_fac;
	tables bathrooms
		*waternw_bathroom / list missing;
	run;
%end;
%mend;
%bath();