** RULE 301.A CENSUS BLOCK **;
*Changed By Di: CBG_Tier is not used in calculation of census_tier_terr nor could be found in census_tier_fac; 
%macro CBG();
	proc sql;
		create table out.census_tier_fac as
		select p.casenumber, p.policynumber, p.Census_Block_Group_Modifier
			, f.f2 as fire_censustier
			, f.f2 as theft_censustier	
			, f.f2 as waternw_censustier	
			, f.f2 as lightning_censustier
			, f.f2 as waterw_censustier	
			, f.f2 as wind_censustier	
			, f.f2 as hail_censustier
			, f.f2 as hurricane_censustier	
			, f.f2 as liabipi_censustier	
			, f.f2 as liabimp_censustier	
			, f.f2 as liapd_censustier	
			, f.f2 as waterbu_censustier	
		from out.territory p left join fac.'301#A Census Block Group Mod$'n f 
			on p.Census_Block_Group_Modifier = f.F1;
	quit;


	proc sql;
		create table out.census_tier_terr as
		select p.casenumber, p.policynumber/*, p.cbg_tier, p1.territory*/
		, p.fire_censustier * p1.fire_censusterr as fire_terr
		, p.theft_censustier * p1.theft_censusterr as theft_terr
		, p.waternw_censustier * p1.waternw_censusterr as waternw_terr
		, p.lightning_censustier * p1.lightning_censusterr as lightning_terr
		, p.waterw_censustier * p1.waterw_censusterr as waterw_terr
		, p.wind_censustier * p1.wind_censusterr as wind_terr
		, p.hail_censustier * p1.hail_censusterr as hail_terr
		, p.hurricane_censustier * p1.hurricane_censusterr as hurricane_terr
		, p.liabipi_censustier * p1.liabipi_censusterr as liabipi_terr
		, p.liabimp_censustier * p1.liabimp_censusterr as liabimp_terr
		, p.liapd_censustier * p1.liapd_censusterr as liapd_terr
		, p.waterbu_censustier * p1.waterbu_censusterr as waterbu_terr
		from out.census_tier_fac p 
		inner join out.census_terr_fac p1 on p.casenumber = p1.casenumber;
		quit;

/*%if &Process = 0 %then %do;*/
/*	title 'RULE 301.A CENSUS BLOCK';*/
/*	proc sql;*/
/*		select distinct census_block_group_ID*/
/*			, fire_census*/
/*			, theft_census*/
/*			, waternw_census*/
/*			, lightning_census*/
/*			, wind_census*/
/*			, hail_census*/
/*			, liabipi_census*/
/*			, liabimp_census*/
/*			, liapd_census*/
/*			, waterbu_census*/
/*		from out.census_fac;*/
/*	quit;*/
/*%end;*/

%mend;
%CBG();