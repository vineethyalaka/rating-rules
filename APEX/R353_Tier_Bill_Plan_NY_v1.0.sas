** RULE 353. TIER BY BILL PLAN **;
%macro tierbill();
	data exp_tier_x_bill_plan_fac (keep=bill_plan ins_tier factor);
	length bill_plan $20.;
	set fac.'353# Tier by Bill Plan$'n;
	if f3=. then delete;
	bill_plan='10-pay'; factor = 'Expense Fee'n; output;
	bill_plan='4-pay'; factor = f3; output;
	bill_plan='Full-pay or Mortgage'; factor = f4; output;
	rename f1 = ins_tier;
	run;
	proc sql;
		create table out.exp_tier_x_bill_plan_fac as
		select p1.*, p2.matchcode, p2.ScoreModel, p2.insscore, p2.ins_tier, p2.ins_tier_n, f.factor as exp_tier_x_bill_plan_fac
		from bill_plan p1 
			inner join ins_tier p2 on p1.casenumber = p2.casenumber
			left join exp_tier_x_bill_plan_fac f on f.bill_plan = p1.BillPlanType and f.ins_tier = p2.ins_tier;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 353. TIER BY BILL PLAN';
	proc freq data=out.exp_tier_x_bill_plan_fac;
	tables BillPlanType*ins_tier_n*exp_tier_x_bill_plan_fac / list missing;
	run;
%end;
%mend;
%tierbill();