**Rule 527 Workers Compensation CA **;


data r527_factor;
	set fac.'527# Workers Comp$'n;
	if strip(f1) = '' then delete;
	run;

proc sql;
    create table out.Workers_Comp as
	select p.CaseNumber, p.PolicyNumber, w.Factor, p.DomesticEmployeesCount
		 , case when p.DomesticEmployeesCount > 2 then input(w.Factor, dollar2.)*(p.DomesticEmployeesCount-2) else 0 end as Workers_Comp
	from in.Policy_Info as p
	, r527_factor as w
	;
quit;
