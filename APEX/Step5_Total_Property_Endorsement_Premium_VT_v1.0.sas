proc sql;
		create table out.step5_Property_Premium as
		select *
			, case when f16.package_discount = 0 then 0 else f15.Refrigerated_Prop_Rate end as Refrigerated_Prem
			, (f1.waterbu_base_premium
			+ f2.SPP_base_premium
			+ f3.Other_Struc_base_premium
			+ f4.incr_covc_base_premium
			+ f5.Business_Prop_rate
			+ f6.EQ_Loss_Assess_rate
			+ f7.Earthquake_rate
			+ f8.Loss_Assess_rate
			+ f9.loss_of_use_rate
			- f10.Decr_CovC_rate
			+ f11.USPP_rate
			+ f12.Computer_Rate
			+ f13.ID_Theft_Rate
	/*		+ f14.Mine_Sub_Rate*/
			+ calculated Refrigerated_prem
			+ f16.Package_Discount
			+ f17.Home_Business_Prop_Rate)  AS Property_Premium
		from out.waterbu_base_premium f1
			inner join out.SPP_base_premium			 f2 on f1.casenumber=f2.casenumber
			inner join out.Other_Struc_base_premium	 f3 on f1.casenumber=f3.casenumber
			inner join out.incr_covc_base_premium	 f4 on f1.casenumber=f4.casenumber
			inner join out.Business_Prop_rate		 f5 on f1.casenumber=f5.casenumber
			inner join out.EQ_Loss_Assess_rate		 f6 on f1.casenumber=f6.casenumber
			inner join out.Earthquake_rate			 f7 on f1.casenumber=f7.casenumber
			inner join out.Loss_Assess_rate			 f8 on f1.casenumber=f8.casenumber
			inner join out.loss_of_use_rate			 f9 on f1.casenumber=f9.casenumber
			inner join out.Decr_CovC_rate			f10 on f1.casenumber=f10.casenumber
			inner join out.USPP_rate				f11 on f1.casenumber=f11.casenumber
			inner join out.Computer_Rate			f12 on f1.casenumber=f12.casenumber
			inner join out.ID_Theft_Rate			f13 on f1.casenumber=f13.casenumber
	/*		inner join out.Mine_Sub_Rate			f14 on f1.casenumber=f14.casenumber*/
			inner join out.Refrigerated_Property	f15 on f1.casenumber=f15.casenumber
			inner join out.package_discount			f16 on f1.casenumber=f16.casenumber
			inner join out.Home_Business_Property   f17 on f1.casenumber=f17.casenumber
	;
	quit;