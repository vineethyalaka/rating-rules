﻿** RULE 310. TOWNHOUSE OR ROWHOUSE **;

**=====================================================================================**
History: 2019 11 14 Yu Fang added P19 wild-fire in Base Rates
**=====================================================================================**;

%macro town();
	data rowhouse;
		set in.policy_info;
		if propertyunitsinbuilding >=2 then num_units='2+';
		else num_units='1';
	run;

	proc sql;
		create table out.rowhouse_fac as
		select p.CaseNumber, p.PolicyNumber, p.PropertyTypeofDwelling, p.propertyunitsinbuilding, p.num_units
			 , case when p.PropertyTypeofDwelling in ('Rowhouse','Townhouse') then f.fire else 1 end as fire_rowhouse
			 , case when p.PropertyTypeofDwelling in ('Rowhouse','Townhouse') then f.'Water Non-Weather'n else 1 end as waternw_rowhouse
			 , case when p.PropertyTypeofDwelling in ('Rowhouse','Townhouse') then f.'Wildfire'n else 1 end as wildfire_rowhouse
		from rowhouse p left join fac.'310# Townhouse or Rowhouse$'n f
			on p.num_units=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 310. TOWNHOUSE OR ROWHOUSE';
	proc freq data=out.rowhouse_fac;
	tables PropertyTypeofDwelling*propertyunitsinbuilding
		*fire_rowhouse
		*waternw_rowhouse / list missing;
	run;
%end;
%mend;
%town();