** RULE 323. SQUARE FOOTAGE **;
%macro SF();
	data sqft; set in.policy_info;
	if propertysquarefootage = 0 then propertysquarefootage = 1850;
	run;
	data sqft_fac;
	set fac.'323# Square Footage$'n;
	if f3=. and theft ne . then f3=99999;
	if f1=. and f3=. then delete;
	run;
	proc sql;
		create table out.sqft_fac as
		select p.casenumber, p.policynumber, p.propertysquarefootage
			, f.theft as theft_sqft
		from sqft p left join sqft_fac f
			on p.propertysquarefootage>=f.f1
			and p.propertysquarefootage<=f.f3;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 323. SQUARE FOOTAGE';
	proc print data=out.sqft_fac;
	where theft_sqft= .;
	run;
	proc gplot data=out.sqft_fac;
		plot theft_sqft*propertysquarefootage 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.sqft_fac;
		histogram propertysquarefootage;
	run;
%end;
%mend;
%SF();