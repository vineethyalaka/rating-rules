** RULE 301.A CENSUS BLOCK **;

**=====================================================================================**
History: 2015 07 21 SY  Initial draft: No liability perils
**=====================================================================================**;

%macro CBG();
	proc sql;
		create table out.census_fac as
		select p.casenumber, p.policynumber, p.census_block_group_ID
			, f.Fire as fire_census
			, f.Theft as theft_census
			, f.'Water Non-Weather'n as waternw_census
			, f.Lightning as lightning_census
			, f.Wind as wind_census
			, f.hail as hail_census
/*			, f.'Liability - Bodily injury (Perso'n as liabipi_census*/
/*			, f.'Liability - Bodily injury (Medic'n as liabimp_census*/
/*			, f.'Liability - Property Damage to O'n as liapd_census*/
			, f.'Water Back-up'n as waterbu_census
		from out.territory p left join fac.'301#A Census Block ID$'n f 
			on p.census_block_group_ID = f.F1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 301.A CENSUS BLOCK';
	proc sql;
		select distinct census_block_group_ID
			, fire_census
			, theft_census
			, waternw_census
			, lightning_census
			, wind_census
			, hail_census
/*			, liabipi_census*/
/*			, liabimp_census*/
/*			, liapd_census*/
			, waterbu_census
		from out.census_fac;
	quit;
%end;

%mend;
%CBG();