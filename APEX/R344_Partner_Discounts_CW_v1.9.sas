** RULE 344. PARTNER DISCOUNTS **;
*account numbers for partners are placeholders;

**Version History**;
*2016 10 03 1.1 SH Removing the General;
*2017 03 10 1.6 BF Added rename clause in affinty_fac for VT rates manual;
*2017 07 18 1.7 JL Added Elephant account code 1132, AmFam group, addl Liberty and MetLife;
*2017 XX XX 1.8 XX Unknown update;		
*2017 08 30 1.9 JL Removing expired partnerships - APEX;

%macro aff();
	data affinity; 
	set in.policy_info (keep=casenumber policynumber policyaccountnumber autopolicynumber /*Partner_Customer*/);
	if policyaccountnumber in (1020:1026,1051,1130:1132,4000:4499,11510:11520,13220:13242,13800:13830,14000:14500)

/*							,1550:1553,1610,2000:2199,2300:2699,3300:3899,7010,7030,8000:8999,11000*/ /*Expired Partnerships*/

		then partner = 1;
	else if policyaccountnumber in (50000:50099,     /*Wells Fargo*/
									51000:51099		/*AmFam group*/)
		then partner = 3;
	else partner = 0;
	if autopolicynumber = '' then auto = 0;
	else auto = 1;
	run;


%if %sysfunc(exist(fac.'344# Partner Discounts$'n)) %then %do;

	data affinity_fac;
	set fac.'344# Partner Discounts$'n;
	if f1 = '' then delete;
	Discount_Indicator = input(substr(f1,1,1),1.);
	rename 'Partner Discounts'n = Affinity_Discount;
	rename 'Partner Discount'n = Affinity_Discount;
	run;

%end;
%else %do;
	data affinity_fac;
	set fac.'344# Affinity Marketing$'n;
	if f1 = '' then delete;
	Discount_Indicator = input(substr(f1,1,1),1.);
	rename 'Partner Discount'n = Affinity_Discount;
	rename 'Affinity Discount'n = Affinity_Discount;
	run;
%end;


	proc sql;
		create table affinity_type as
		select p.*,  
		case when p.auto = 1 and p.partner = 1 then 1 
/*		when p.partner = 2 then 2 */
		when p.partner = 3 and p.auto = 1 then 3
		when p.partner = 3 and p.auto = 0 /*and p.Partner_Customer = '1'*/ then 2
		else 0 end as Discount_Indicator
		from affinity p;
	quit;

proc sql;
	create table out.affinity_fac as
	select a.*,
	case when b.Affinity_Discount = . then 0 else b.Affinity_Discount end as Affinity
	from affinity_type a
	left join affinity_fac b on a.Discount_Indicator = b.Discount_Indicator
;
quit;




%if &Process = 0 %then %do;
	title 'RULE 344. PARTNER DISCOUNTS';
	proc freq data=out.affinity_fac;
	tables partner*auto*affinity / list missing;
	run;
%end;
%mend;
%aff();