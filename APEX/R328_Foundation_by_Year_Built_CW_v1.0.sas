/*2018/12/03	Wen Zhang	create CW version rule*/
** RULE 328. Foundation by Year Built **;
%macro FBYB();
	%if %sysfunc(exist(fac.'328# Foundation by Year$'n)) %then %do;	
		data foundation_year_fac;
			set fac.'328# Foundation by Year$'n;
		run;
	%end;
	%if %sysfunc(exist(fac.'328# Foundation by Year Built$'n)) %then %do;
		data foundation_year_fac;+
			set fac.'328# Foundation by Year Built$'n;
		run;
	%end;


	data foundation_type;
	set in.policy_info;

	if PropertyFoundationType = 1 then Foundation = 'Closed Basement'; 
		else if PropertyFoundationType = 2 then Foundation = 'Walk-in'; 
		else if PropertyFoundationType = 3 then Foundation = 'Slab'; 
		else if PropertyFoundationType = 4 then Foundation = 'Stilts';
		else if PropertyFoundationType = 5 then Foundation = 'Crawlspace'; 
		else Foundation = 'Other'; 
	if PropertyFoundationType = 3 and PropertyYearBuilt > 2000 then YearBuilt = '2001 or Later';
		else if PropertyFoundationType = 3 and PropertyYearBuilt < 2001 then YearBuilt = 'Before 2001'; 
		else YearBuilt = 'Any';
	run;

	proc sql;
		create table out.foundation_type_year_fac as
		select p1.casenumber, p1.policynumber, p1.Foundation, p1.YearBuilt
			, f.'Water Non-Weather'n as waternw_found_type_yr_built
		from foundation_type p1 
			left join foundation_year_fac f on p1.Foundation=f.f1 and p1.YearBuilt=f.f2;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 328. FOUNDATION BY YEAR BUILT';
	proc freq data=out.foundation_type_year_fac;
	tables Foundation*YearBuilt
		*waternw_found_type_yr_built / list missing;
	run;
%end;

%mend;
%FBYB();