*Rule 346 Responsible Motorist;
proc sql outobs = 1;;
select p.RatingVersion into :Rating_Version 
from in.POLICY_INFO p
;
quit;

proc sql;
create table work.Autoclaim_lookup as
select a.policynumber ,a.casenumber, a.policytermyears, b.autolosscode, b.autolossamount from in.policy_info as a
left join &server..HS_APRating_RequestAutoClaims as b
on a.casenumber=b.casenumber;
quit;

proc sql;
create table work.Autoclaim_Losscode as
select autolosscode, peril from &server..HS_APRating_AutoLossCode_Peril
where version=&Rating_Version;
quit;

data peril_list;
input peril $ num;
cards;
P1 1
P2 2
P3 3
P4 4
P5 5
P6 6
P7 7
P8 8
P9 9
;

proc sql;
create table work.Autoclaim_lookup2 as
select a.*, b.autolosscode, c.peril, c.num from work.autoclaim_lookup as a 
left join work.autoclaim_losscode as b
on a.autolosscode=b.autolosscode
left join work.peril_list as c
on b.peril=c.num
order by a.policynumber;
quit;

/* important: summarize # of perils */
proc sql;
create table work.Autoclaim_Group as
select policynumber, casenumber, policytermyears, peril, count(num) as peril_count from work.Autoclaim_lookup2
group by policynumber, casenumber, peril
order by policynumber;
quit;

proc sort data=work.Autoclaim_Group;
by policynumber casenumber;
run;

data work.Autoclaim_Group_Rotated(drop=peril peril_count i);
set work.Autoclaim_Group;
by PolicyNumber casenumber;
array mark{7} P_1 P_2 P_3 P_4 P_5 P_6 P_8;
retain mark(0);
if peril=" " then do;
	do i=1 to 7;
		mark{i}=0;
	end;
end; /*there is only one row, output directly*/
else do;
	select (peril);
		when ('P1') P_1=peril_count;
		when ('P2') P_2=peril_count;
		when ('P3') P_3=peril_count;
		when ('P4') P_4=peril_count;
		when ('P5') P_5=peril_count;
		when ('P6') P_6=peril_count;
		when ('P8') P_8=peril_count;
		otherwise;
	end;
end;

if last.casenumber then do;
	output;
		do i=1 to 7;
			mark{i}=0;
		end;
	end;

run;

data r346fac;
set fac.'346# Responsible Motorist$'n;
if Fire=1 then F1=5;
else if Fire=. then delete;
drop F9 F10;
run;

proc sql;
create table out.res_motor_fac as
select 
a.*,
/* if it's marked, corresponding factor should be 1*/
case when P_1>0 then 1 else round(b.Fire,0.001) end as fire_resmotor,
case when P_2>0 then 1 else round(b.Theft,0.001) end as theft_resmotor,
case when P_3>0 then 1 else round(b.'Water Non-Weather'n,0.001) end as waternw_resmotor,
case when P_4>0 then 1 else round(b.'All Other Non-Weather'n,0.001) end as othernw_resmotor,
case when P_5>0 then 1 else round(b.Lightning,0.001) end as lightning_resmotor,
case when P_6>0 then 1 else round(b.'Water Weather'n,0.001) end as waterw_resmotor,
case when P_8>0 then 1 else round(b.Hail,0.001) end as hail_resmotor
from Autoclaim_Group_Rotated as a
left join r346fac as b
on (case when a.policytermyears>5 then 5 else a.policytermyears end)=b.F1;
quit;
