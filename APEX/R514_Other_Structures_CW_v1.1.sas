/*	Version		Editor	Edit*/
/*	1.1			JT		Rounded to 4 decimal places before rounding to nearest dollar in endorsement premium*/



** RULE 514. OTHER STRUCTURES **;
%macro other();
*Rate*;
	data r514_rate;
	set fac.'514# Other Structures Rate$'n;
	if strip(f1) = '' then delete;
	rename 'Coverage Limit Increment (i#e# r'n = Increment;
	run;
	proc sql;
		create table out.Other_Structure_rate as
		select p.CaseNumber, p.PolicyNumber, p.HO_0448_Flag, p.HO_0448_addl_limit_01
			, case when p.HO_0448_Flag="1" then p.HO_0448_addl_limit_01*f.Rate/Increment else 0 end as Other_Structures_rate_nr
		from in.Endorsement_Info p, r514_rate f	;
	quit;

	*Hail Factor*;
	data r514_fac;
	set fac.'514# Other Structures Factor$'n;
	if strip(f1) = '' then delete;
	run;
	proc sql;
		create table out.Other_Structure_fac as
		select p.CaseNumber, p.PolicyNumber, p.HO_0448_Flag
			, case when p.HO_0448_Flag="1" then f.factor else 0 end as Other_Structures_fac
		from in.Endorsement_Info p, r514_fac f;
	quit;

	*Incr Limit Other Structure Base Premium*;
	proc sql;
		create table out.Other_Struc_base_premium as
		select f1.*, f2.hail_disc_base_premium, f3.*
			, round(round(f1.Other_Structures_rate_nr
			+(f2.hail_disc_base_premium
			* f3.Other_Structures_fac),0.0001),1.0)
			as Other_Struc_base_premium
			,f1.Other_Structures_rate_nr
			+(f2.hail_disc_base_premium
			* f3.Other_Structures_fac)
			as Other_Struc_base_premium_nr
		from out.Other_Structure_rate f1
			inner join out.step2_base_premium	f2 on f1.casenumber=f2.casenumber
			inner join out.Other_Structure_fac	f3 on f1.casenumber=f3.casenumber;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 514. OTHER STRUCTURES';
	proc print data=out.Other_Structure_rate;
	where Other_Structures_rate_nr = .;
	run;
	proc gplot data=out.Other_Structure_rate;
		plot Other_Structures_rate_nr*HO_0448_addl_limit_01 
			/ overlay legend vaxis=axis2;
		where HO_0448_Flag='1';
	run;
	quit; 
	proc freq data=out.Other_Structure_fac;
	tables HO_0448_Flag*Other_Structures_fac / list missing;
	run;
	proc sql;
		select mean(Other_Struc_base_premium) as Other_Struc_base_prem_avg
		from out.Other_Struc_base_premium
		where HO_0448_Flag='1';
	quit;
%end;
%mend;
%other();