/*********** STEP 3: SUBTOTAL PREMIUM (RULE 301.A.3) *******************************************************/

**=====================================================================================**

**=====================================================================================**;


proc sql;
	create table out.step2_base_premium as
	select *
	 	, fire_base_rate
		* fire_census
/*		* fire_county */
		* fire_cova
		* fire_home_age
/*		* fire_num_res*/
		* fire_heat_sys
		* fire_uw
		* fire_rowhouse
		* fire_sec_res
/*		* fire_seasonal_secondary_fac*/
		* fire_naogclaimxPeril
		* fire_claim
		* fire_ded
		* fire_sp_personal_prop
		as fire_base_premium

		, round(calculated fire_base_premium
		* fire_new_home
		* fire_new_home_purch
		* fire_prot_dev, .0001)
		as fire_disc_base_premium

		, theft_base_rate
		* theft_county
		* theft_census
		* theft_covcbase
		* theft_cust_age
/*		* theft_drc*/
		* theft_uw
		* theft_length_res
/*		* theft_num_res*/
		* theft_num_units
		* theft_pool
		* theft_sqft
		* theft_sec_res
/*		* theft_seasonal_secondary_fac*/
		* theft_naogclaimxPeril
		* theft_claim
		* theft_ded
		* theft_sp_personal_prop
		as theft_base_premium
	
	
		, round(calculated theft_base_premium
		* theft_prot_dev, .0001)
		as theft_disc_base_premium


		, waternw_base_rate
		* waternw_grid
		* waternw_census
		* waternw_cova	
		* waternw_fin_bsmnt
		* waternw_home_age
		* waternw_uw
		* waternw_length_res
/*		* waternw_num_res*/
		* waternw_bsmnt
		* waternw_pool
		* waternw_rcsqft
		* waternw_rowhouse
		* waternw_bathroom
		* waternw_sec_res
/*		* waternw_seasonal_secondary_fac*/
		* waternw_claimxPeril
		* waternw_claim
		* waternw_ded
		* waternw_sp_personal_prop
		as waternw_base_premium
	
		, round(calculated waternw_base_premium
		* waternw_new_home,.0001)
		as waternw_disc_base_premium


		, othernw_base_rate
		* othernw_cova
		* othernw_cust_age
		* othernw_home_age
		* othernw_uw
		* othernw_pool
		* othernw_sec_res
/*		* othernw_seasonal_secondary_fac*/
		* othernw_naogclaim
		* othernw_claim
		* othernw_ded
		* othernw_sp_personal_prop
		as othernw_base_premium
	
		, round(calculated othernw_base_premium
		* othernw_new_home,.0001)
		as othernw_disc_base_premium


		, lightning_base_rate
		* lightning_census
		* lightning_cova
		* lightning_uw
		* lightning_pool
		* lightning_rcsqft
		* lightning_sec_res
		* lightning_ded
		* lightning_sp_personal_prop
/*		* lightning_seasonal_secondary_fac*/
		* lightning_naogclaim
		* lightning_claim
		as lightning_base_premium
	
		, round(calculated lightning_base_premium
		* lightning_new_home_purch,.0001)
		as lightning_disc_base_premium


		, waterw_base_rate
		* waterw_grid
		* waterw_cova		
		* waterw_home_age
		* waterw_uw
		* waterw_rcsqft
		* waterw_roof_age
		* waterw_sec_res
/*		* waterw_seasonal_secondary_fac*/
		* waterw_claimxPeril
		* waterw_claim
		* waterw_ded
		as waterw_base_premium
	
		, round(calculated waterw_base_premium
		* waterw_new_roof
		* waterw_new_home,.0001)
		as waterw_disc_base_premium


		, wind_base_rate
		* wind_grid
		* wind_census
		* wind_const_class
		* wind_cova
		* wind_home_age
		* wind_uw
		* wind_fence
		* wind_rcsqft
		* wind_ACV_roof
		* wind_roof_age
		* wind_roof_type
		* wind_sqft_story
		* wind_sec_res
/*		* wind_seasonal_secondary_fac*/
		* wind_naogclaim
		* wind_claim
		* wind_ded
		as wind_base_premium
	
	
		, round(calculated wind_base_premium
		* wind_new_home
		* wind_new_roof
		* wind_new_home_purch,.0001)
		as wind_disc_base_premium
	
		, hail_base_rate
		* hail_grid
		* hail_census
		* hail_cova
		* hail_home_age
		* hail_ACV_roof
		* hail_roof_type
		* hail_roof_age_type
		* hail_sqft_story
		* hail_sec_res
/*		* hail_seasonal_secondary_fac*/
		* hail_ded
		as hail_base_premium
	
		, round(calculated hail_base_premium
		* hail_new_home
		* hail_new_roof,.0001)
		as hail_disc_base_premium


		, hurr_base_rate
		* hurr_grid
		* hurr_cova
		* hurr_ded
		* hurr_uw
		* hurr_home_age
		* hurr_roof_age
		* hurr_ACV_roof
		* hurr_sec_res
/*		* hurr_seasonal_secondary_fac*/
		* hurr_claim
		* hurr_naogclaim
		as hurr_base_premium
	
		, round(calculated hurr_base_premium
		* hurr_new_home
		* hurr_new_roof,.0001)
		as hurr_disc_base_premium

	from out.base_rate f1
		inner join out.grid_fac 			 	   f2 on f1.casenumber=f2.casenumber
		inner join out.census_fac   		 	   f3 on f1.casenumber=f3.casenumber
		inner join out.county_fac 				   f4 on f1.casenumber=f4.casenumber
		inner join out.ACV_Roof_fac 		 	   f5 on f1.casenumber=f5.casenumber
		inner join out.construction_class_fac	   f6 on f1.casenumber=f6.casenumber
		inner join out.coverage_a_fac 		 	   f7 on f1.casenumber=f7.casenumber
		inner join out.coverage_c_base_fac	 	   f8 on f1.casenumber=f8.casenumber
		inner join out.customer_age_fac 	 	   f9 on f1.casenumber=f9.casenumber
		inner join out.ded_fac	 				  f10 on f1.casenumber=f10.casenumber
/*		inner join out.drc_fac 					  f12 on f1.casenumber=f12.casenumber*/
		inner join out.finished_bsmnt_fac 		  f13 on f1.casenumber=f13.casenumber
		inner join out.heat_system_fac 			  f14 on f1.casenumber=f14.casenumber
		inner join out.home_age_fac 			  f15 on f1.casenumber=f15.casenumber
		inner join out.uw_tier_fac 			  	  f16 on f1.casenumber=f16.casenumber /*changed from ins_score_fac*/
		inner join out.length_res_fac 			  f17 on f1.casenumber=f17.casenumber
		inner join out.num_dogs_fac 			  f18 on f1.casenumber=f18.casenumber
/*		inner join out.num_residents_fac 		  f19 on f1.casenumber=f19.casenumber*/
		inner join out.num_units_fac 			  f20 on f1.casenumber=f20.casenumber
		inner join out.basement_fac				  f21 on f1.casenumber=f21.casenumber
		inner join out.fence_fac 				  f22 on f1.casenumber=f22.casenumber
		inner join out.pool_fac					  f23 on f1.casenumber=f23.casenumber
		inner join out.rcsqft_fac				  f24 on f1.casenumber=f24.casenumber
		inner join out.roof_age_fac				  f25 on f1.casenumber=f25.casenumber
		inner join out.roof_type_fac			  f26 on f1.casenumber=f26.casenumber
		inner join out.roof_age_type_fac		  f27 on f1.casenumber=f27.casenumber
		inner join out.sqft_fac				 	  f28 on f1.casenumber=f28.casenumber
		inner join out.sqft_story_fac		 	  f29 on f1.casenumber=f29.casenumber
		inner join out.special_personal_prop_fac  f30 on f1.casenumber=f30.casenumber
		inner join out.bathroom_fac			 	  f31 on f1.casenumber=f31.casenumber
		inner join out.priorclaim_NAOG_fac		  f32 on f1.casenumber=f32.casenumber
		inner join out.priorclaim_NAOG_xPeril_fac f33 on f1.casenumber=f33.casenumber
		inner join out.priorclaim_xPeril_fac	  f34 on f1.casenumber=f34.casenumber
		inner join out.priorclaim_Peril_fac		  f35 on f1.casenumber=f35.casenumber
		inner join out.priorclaim_xWater_fac	  f36 on f1.casenumber=f36.casenumber
		inner join out.priorclaim_Water_fac		  f37 on f1.casenumber=f37.casenumber
		inner join out.new_home_fac				  f39 on f1.casenumber=f39.casenumber
		inner join out.new_home_purch_fac 		  f40 on f1.casenumber=f40.casenumber
		inner join out.new_roof_fac				  f41 on f1.casenumber=f41.casenumber
		inner join out.protective_dev_fac		  f42 on f1.casenumber=f42.casenumber
		inner join out.rowhouse_fac				  f43 on f1.casenumber=f43.casenumber
		inner join out.Seasonal_Secondary_Fac 	  f44 on f1.casenumber=f44.casenumber
;
quit;