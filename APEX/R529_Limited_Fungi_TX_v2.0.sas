** RULE 529. LIMITED FUNGI OR MICROBES COVERAGE **;

**=====================================================================================**
History: 2017 09 11 JK  Initial draft with revised fungi structure for APEX 2.0
						NOTE: Needs adjustment to pull in Cov A % (defualt to 25% currently, waiting on IT creation of new column)
**=====================================================================================**;
%macro fungi();
	proc sql;
		create table out.Limited_Fungi_Rate as
		select 
			  a.CaseNumber
			, a.PolicyNumber
			, a.county
			, f.Code
			, b.Factor
			, c.adjusted_subtotal_premium
			, case when LimitedFungiFlag = 1 then(b.Factor*c.adjusted_subtotal_premium) else 0 end as Limited_Fungi_Rate
			, LimitedFungiFlag
			, Property_Amount
			, Limited_Fungi 
		from in.Policy_Info as a		
		left join in.PRODUCTION_INFO pd
		on	a.casenumber = pd.casenumber 
		left join Fungi.'County Definitions$'n as f
		on  upcase(a.county) = upcase(f.county)
		left join fac.'529# Limited Fungi$'n as b
		on f.code = b.territory and b.'Percentage of Policy Limit'n = pd.Property_Amount/100
		left join out.step4_adjusted_subtotal_premium as c
		on a.CaseNumber = c.CaseNumber
	;quit;

%mend;
%fungi();
