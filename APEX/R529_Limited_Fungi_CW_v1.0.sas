** RULE 530. IDENTITY FRAUD EXPENSE COVERAGE **;

**=====================================================================================**
History: 2015 05 28 SY  Initial draft
**=====================================================================================**;

%macro fungi();
	data r529;
	set fac.'529# Limited Fungi$'n;
	if charge = . then delete;
	retain CoverageType;
	if Coverage ^= "" then CoverageType = Coverage;
	run;
	proc sql;
		create table out.Limited_Fungi_Rate as
		select p.CaseNumber, p.PolicyNumber, p.Property_Amount, p.Liability_Amount
			, case when f1.charge = . then 0 else f1.charge end as Limited_Fungi_Property_Rate
			, case when f2.charge = . then 0 else f2.charge end as Limited_Fungi_Liability_Rate
			, calculated Limited_Fungi_Property_Rate + calculated Limited_Fungi_Liability_Rate as Limited_Fungi_Rate
		from in.Production_Info p
			left join r529 f1 on p.Property_Amount = f1.'Increased Limits'n and f1.coveragetype = "Section I - Property"
			left join r529 f2 on p.Liability_Amount = f2.'Increased Limits'n and f2.coveragetype = "Section II - Liability"
		;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 529. LIMITED FUNGI';
	proc freq data=out.Limited_Fungi_Rate;
	tables Property_Amount*Liability_Amount*Limited_Fungi_Rate / list missing;
	run;
%end;

%mend;
%fungi();
