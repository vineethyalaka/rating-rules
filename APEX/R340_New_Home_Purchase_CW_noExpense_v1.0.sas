** RULE 340. NEW HOME PURCHASE **;
/*		2015 12 30 	QS Updated to remove Expense Fee for MA*/

%macro NHP();
	data new_home_purch_term;
	set in.policy_info;
	length new_home_purch $3. term $2.;
	if new_home_flag=1 then new_home_purch='Yes'; else new_home_purch='No';
	if policytermyears>5 then term='6+'; else term=compress(policytermyears);
	run;
	proc sql;
		create table out.new_home_purch_fac as
		select p.casenumber, p.policynumber, p.new_home_flag, p.new_home_purch, p.policytermyears, p.term
			, f.fire as fire_new_home_purch
			, f.lightning as lightning_new_home_purch
			, f.wind as wind_new_home_purch
/*			, f.'Expense Fee'n as expense_new_home_purch*/
		from new_home_purch_term p left join fac.'340# New Home Purchase$'n f
			on p.new_home_purch=f.f1
			and p.term=f.f2;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 340. NEW HOME PURCHASE';
	proc freq data=out.new_home_purch_fac;
	tables new_home_flag*policytermyears
		*fire_new_home_purch
		*lightning_new_home_purch
		*wind_new_home_purch	
/*		*expense_new_home_purch*/ / list missing;
	run;
%end;
%mend;
%NHP();
