** RULE 515.C SPECIAL LIMITS **;
%macro sl();
	data r515c;
	set fac.'515#C SPECIAL LIMITS$'n;
	if strip('Type of Property'n) = '' then delete;
	rename 'Coverage Limit Increment (i#e# r'n = Increment;
	run;
	proc sql;
		create table r515c_USPP as
		select e.CaseNumber, e.PolicyNumber, pd.HH0015, e.HO_0465_flag, e.HO_0466_flag
			, e.HO_04_65_01_AMT, e.HO_04_65_02_AMT, e.HO_04_65_03_AMT, e.HO_04_65_04_AMT, e.HO_04_65_05_AMT, e.HO_04_65_06_AMT, e.HO_04_65_07_AMT
			, case when HH0015 = '1' and HO_0466_flag = "1" or e.HO_0465_flag = "1" then 1 else 0 end as r515c_flag
		from in.Endorsement_Info as e 
			inner join in.Production_Info as pd on e.CaseNumber = pd.CaseNumber;
	quit;
	proc sql;
		create table out.USPP_rate as
		select p.*
			, round(HO_04_65_01_AMT*f1.rate/f1.Increment,1.0) as Money
			, round(HO_04_65_02_AMT*f2.rate/f2.Increment,1.0) as Securities
			, round(HO_04_65_03_AMT*f3.rate/f3.Increment,1.0) as Jewelry
			, round(r515c_flag*HO_04_65_04_AMT*f4.rate/f4.Increment, 1.0) as Firearms
/*			, round(HO_04_65_05_AMT*f5.rate/f5.Increment,1.0) as Silverware*/
			,round((HO_04_65_06_AMT + HO_04_65_07_AMT)*f6.rate/f6.Increment,1.0) as Apparatus
			, calculated Money + calculated Securities + calculated Jewelry + calculated Firearms /*+ calculated Silverware*/
				+ calculated Apparatus as USPP_rate
		from r515c_USPP p, r515c f1, r515c f2, r515c f3, r515c f4, /*r515c f5,*/ r515c f6
			where 	strip(f1.'Type of Property'n) = 'Money'
			and 	strip(f2.'Type of Property'n) = 'Securities'
			and 	strip(f3.'Type of Property'n) = 'Jewelry, Watches, and Furs'
			and 	strip(f4.'Type of Property'n) = 'Firearms'
/*			and 	strip(f5.'Type of Property'n) = 'Silverware, Goldware, and Pewterware'*/
			and 	strip(f6.'Type of Property'n) = 'Electronic Apparatus';
	quit;

%if &Process = 0 %then %do;
	title 'RULE 515.C SPECIAL LIMITS';
	proc freq data=out.USPP_rate;
	tables r515c_flag*HO_04_65_01_AMT*Money*USPP_rate
			r515c_flag*HO_04_65_02_AMT*Securities*USPP_rate
			r515c_flag*HO_04_65_03_AMT*Jewelry*USPP_rate
			r515c_flag*HO_04_65_04_AMT*Firearms*USPP_rate
/*			r515c_flag*HO_04_65_05_AMT*Silverware*USPP_rate*/
			r515c_flag*HO_04_65_06_AMT*HO_04_65_07_AMT*Apparatus*USPP_rate
	/ list missing;
	run;
%end;
%mend;
%sl();
