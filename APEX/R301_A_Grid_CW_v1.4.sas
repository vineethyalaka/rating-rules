** RULE 301.A GRID **;
%macro grid();
	proc sql;
		create table out.grid_fac as
		select p.casenumber, p.policynumber
/*,p.GridID*/
            , p.LocationID
			, f.'Water Non-Weather'n as waternw_grid
			, f.'Water Weather'n as waterw_grid
			, f.Wind as wind_grid
			, f.hail as hail_grid
/*			, f.Hurricane as hurr_grid*/
			, f.'Liability - Bodily injury (Perso'n as liabipi_grid
			, f.'Liability - Bodily injury (Medic'n as liabimp_grid		
			, f.'Water Back-up'n as waterbu_grid
		from out.territory p left join fac.'301#A Location ID$'n f 
/*SAS often detects F1 as a character or string. If it does, then use the input function*/
/*			on p.LocationID = input(f.F1,7.)*/
/*			on p.LocationID = input(f.F1,8.)*/
			on p.LocationID = f.F1
;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 301.A GRID';
	proc sql;
		select distinct 
/*			  gridID,*/
			  waternw_grid
			, waterw_grid
			, wind_grid
			, hail_grid
/*			, hurr_grid*/
			, liabipi_grid
			, liabimp_grid
			, waterbu_grid
		from out.grid_fac;
	quit;
%end;
%mend;
%grid();
