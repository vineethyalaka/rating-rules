** RULE 355. AGE OF HOME BY INSURANCE SCORE **;
/*ITR 13402 11/7/2018 Change 21 ins_tier to 57 ins_tier according to rate sheet
                      Change max age from hard code to macro variable.
                      Add a temp table home_age_adj_r355 to deal the case when home age > max age by TM*/
%macro agescore();
	data r355_1;
	set fac.'355#1 Age of Home by Ins Score$'n;
	if f3=. or f1 = 'Age of Home' then delete;
	home_age = input(f1, 4.);
	run;

	data r355_2;
	set fac.'355#2 Age of Home by Ins Score$'n;
	if f3=. or f1 = 'Age of Home' then delete;
	home_age = input(f1, 4.);
	run;

    proc sql noprint;
		 select max(home_age)+1
		 		into :max_age_score
	     from r355_1;

		 update r355_1
		        set home_age = &max_age_score
				where home_age =. and f3 ne .;
	quit;

    proc sql noprint;
		 select max(home_age)+1
		 		into :max_age_score
	     from r355_2;

		 update r355_2
		        set home_age = &max_age_score
				where home_age =. and f3 ne .;
	quit;

/*21 buckets in TUIRS*/
	data exp_home_age_x_ins_score_fac_1 (keep=home_age ins_tier factor);
	set r355_1(rename=('Expense Fee'n=f2));
	array f (21) f2 - f22;
	do i = 1 to 21;
		ins_tier = i; factor = f(i);
		output;
	end;
	run;

/*57 buckets in TRC*/
	data exp_home_age_x_ins_score_fac_2 (keep=home_age ins_tier factor);
	set r355_2(rename=('Expense Fee'n=f2));
	array f (57) f2 - f58;
	do i = 1 to 57;
		ins_tier = i; factor = f(i);
		output;
	end;
	run;

/*max_age_score in rate sheet 1 and 2 must be same */
	proc sql;
		create table home_age_adj_r355 as
		select * from home_age;
		update home_age_adj_r355
		set home_age = &max_age_score
		where home_age > &max_age_score;

		create table out.exp_home_age_x_ins_score_fac as
		select p1.casenumber, p1.policynumber, p1.PolicyIssueDate, p1.PolicyEffectiveDate, p1.PropertyYearBuilt, p1.home_age
			, p2.matchcode, p2.ScoreModel, p2.insscore, p2.ins_tier, coalesce(fa.factor,fb.factor) as exp_home_age_x_ins_score_fac
		from home_age p1 
			inner join ins_tier p2 on p1.casenumber = p2.casenumber
			inner join home_age_adj_r355 p3 on p3.casenumber = p1.casenumber
			left join exp_home_age_x_ins_score_fac_1 fa /* These are the TUIRS (APEX) factors */
				on p2.ScoreModel = 2 and 
					fa.home_age = p3.home_age and fa.ins_tier = p2.ins_tier_n
			left join exp_home_age_x_ins_score_fac_2 fb /* These are the TRC (Mosaic) factors */
				on p2.ScoreModel = 3 and
					fb.home_age = p3.home_age and fb.ins_tier = p2.ins_tier_n;
	quit;


%if &Process = 0 %then %do;
	title 'RULE 355. AGE OF HOME BY INSURANCE SCORE';
	proc print data=out.exp_home_age_x_ins_score_fac;
	where exp_home_age_x_ins_score_fac = .;
	run;
	proc sort data=out.exp_home_age_x_ins_score_fac; by ins_tier; run;
	proc gplot data=out.exp_home_age_x_ins_score_fac; by ins_tier;
		plot exp_home_age_x_ins_score_fac*home_age 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
%end;
%mend;
%agescore();