** RULE 357. CUSTOMER AGE AT NB **;
**=====================================================================================**
History: 2017 05 25 JK  1.2 - Adjusted age calculation to use 365.25
Xinu: A discussion with pricing addressed the conclusion that sas need changed the age calculation from 365.25 to 365 to be consist with IT. 
2019/02/25
Wen: - removed the case when it is an effective state, IT uses the same formula regardless of whether it's an effective date state or not and is not going to fix it
     - adjusted the age calculation back to using 365 to be consistent with IT
**=====================================================================================**;

%macro canb();
	proc sort data = in.hf_info; by casenumber; run;
	data cust_age_nb;
	merge out.customer_age_fac (keep=casenumber policynumber policytermyears policyissuedate policyeffectivedate DOB1 DOB2) 
		  in.hf_info (keep=casenumber firstterm); 
	by casenumber;
	length cust_age_nb $9.;
	if firstterm = . then firstterm = policyeffectivedate;
	if &effst=0 then cust_age_nb=max(floor((policyissuedate - dob1)/365),floor((policyissuedate - dob2)/365));
	else cust_age_nb=max(floor((policyissuedate - dob1)/365),floor((policyissuedate - dob2)/365));
	if cust_age_nb>=110 then cust_age_nb='110+';
	else if cust_age_nb<18 then cust_age_nb=18;
	run;
	proc sql;
		create table out.customer_age_nb_fac as
		select distinct p.*
			, f.'Expense Fee'n as expense_customer_age_nb
		from cust_age_nb p left join fac.'357# Customer Age at NB$'n f
			on strip(p.cust_age_nb)=strip(f.f1)
			order by casenumber;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 357. CUSTOMER AGE AT NB';
	proc print data=out.customer_age_nb_fac;
	where expense_customer_age_nb = .;
	run;
	proc gplot data=out.customer_age_nb_fac;
		plot expense_customer_age_nb*cust_age_nb 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
%end;
%mend;
%canb();