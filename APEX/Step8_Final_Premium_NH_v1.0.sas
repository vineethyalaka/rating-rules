
	proc sql;
		create table out.workerscomp as
		select 
			p.casenumber,
			(3) as workerscomp
		from in.policy_info p;
	quit;
/*%rating_APEX('Step8_Final_Premium_NH',&rversion.);*/
	libname Fac2 excel path="\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\NH\v1339_12104\03_Data\Factors\Rate Adjustment sample rate.xlsm"; *make sure workbook is closed before assigning;
	data r339fac;
	set fac2.'339# Rate Adjustment$'n (firstobs=2 obs=2);
	format f1 4.;
	keep f1;
	call symput('quick_rate',f1);
	run;

    %let quk_rte= round(round((p1.adjusted_subtotal_premium
			+ p2.Property_Premium
			+ p3.Total_Liability_Premium
			+ p4.workerscomp
			+ p5.policy_expense_fee),1)*1.,1)
			as final_calc_prem;

	proc sql;
		create table out.step8_final_premium as
		select *
			, &quk_rte.

			, round(calculated final_calc_prem*p7.affinity,1) as Affnity_Discount
			, round(calculated final_calc_prem*p6.policy_conversion_fac,1) as Policy_Conversion_Disc
			, (calculated final_calc_prem - calculated Affnity_Discount - calculated Policy_Conversion_Disc) as discounted_final_calc_prem

			, case when calculated discounted_final_calc_prem <= p8.min_premium then p8.min_premium
				else calculated discounted_final_calc_prem end 
			as final_premium
		from r339fac f, out.step4_Adjusted_subtotal_premium p1
			inner join out.step5_Property_Premium	p2 on p1.casenumber = p2.casenumber
			inner join out.step6_Liability_Premium	p3 on p1.casenumber = p3.casenumber
			inner join out.workerscomp              p4 on p1.casenumber = p4.casenumber
			inner join out.step7_policy_expense_fee	p5 on p1.casenumber = p5.casenumber
			inner join out.policy_conversion_fac	p6 on p1.casenumber = p6.casenumber
			inner join out.affinity_fac				p7 on p1.casenumber = p7.casenumber
			, out.min_premium p8;
	quit;