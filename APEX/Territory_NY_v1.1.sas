/*version 1507*/
%let Terr_Def_FilePath = %str(\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\NY\v1507_13787\Documentation\NY Owners Territory Definition HNY II 1.0 (Clean).xlsx);

%include "\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\NY\v1507_13787\02_Programs\Territory_Def_Transformation_v3.sas";



proc import datafile="\\cambosvnxcifs01\ITRatingTeam\NewBusiness_CWS\APEX\NY\v1507_13787\03_Data\Factors\NY_HO3_V5a_20180212.csv"
	out=territory
	dbms=csv
	replace;
run;

proc sql;
create table out.territory as 
select 	p.CaseNumber,
		p.PolicyNumber,
		p.PolicyTermYears,
        p.PolicyEffectiveDate,
		p.CountyID,
		p.County,
		c.Census_Block_Group_Modifier,
		t.*,
		t.locationid as territory
from	in.policy_info p 
left join territory t on p.Grid_ITindex = t.GridID
left join CBG c on p.CBG = c.Census_Block_Group and t.locationid = c.territory /*Di: added t.locationid = c.territory becauase cbg depends on both territory and census block group id, e.g.360594065014
*/
;
quit;
