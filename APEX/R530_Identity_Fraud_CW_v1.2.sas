** RULE 530. IDENTITY FRAUD EXPENSE COVERAGE **;
%macro id();
	data r530;
	set fac.'530# Identity Fraud Coverage$'n;
	if strip(f1) = '' then delete;
	run;
	proc sql;
		create table out.ID_Theft_Rate as
		select p.CaseNumber, p.PolicyNumber, p.IDTheft
			, round(p.IDTheft*f.F2,1.0) as ID_Theft_Rate
		from in.Endorsement_Info p, r530 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 530. IDENTITY FRAUD EXPENSE COVERAGE';
	proc freq data=out.ID_Theft_Rate;
	tables IDTheft*ID_Theft_Rate / list missing;
	run;
%end;
%mend;
%id();
