** RULE 315. ROOF TYPE **;
%macro RT();
	data roof_type;
	set in.policy_info;
	if propertyrooftypecode='Wood shake' then propertyrooftypecode='Wood shingles/shakes';
	if propertyrooftypecode='Wood shingles' then propertyrooftypecode='Wood shingles/shakes';
	if lowcase(propertyrooftypecode) in (' ','flat','gambrel','hip','mansard','plexiglass','truss-joist') then propertyrooftypecode='All Other';
	run;
	proc sql;
		create table out.roof_type_fac as
		select p.casenumber, p.policynumber, p.propertyrooftypecode
			, f.wind as wind_roof_type
			, f.hail as hail_roof_type
		from roof_type p left join fac.'315# Roof Type$'n f
			on lowcase(p.propertyrooftypecode)=lowcase(f.f1);
	quit;

%if &Process = 0 %then %do;
	title 'RULE 315. ROOF TYPE';
	proc freq data=out.roof_type_fac;
	tables propertyrooftypecode
		*wind_roof_type
		*hail_roof_type / list missing;
	run;
%end;
%mend;
%RT();