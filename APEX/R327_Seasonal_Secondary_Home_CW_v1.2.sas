﻿** RULE 327. SEASONAL OR SECONDARY RESIDENCES **;

**=====================================================================================**
Note: The flag is called "Secondary_Home_Flag" but the flag is for both seasonal and secondary homes
History: 2015 03 18 JT  Initial draft
		 2015 05 11 SY  Added Hurricane peril indicator
		 2019 11 19 Yu Fang added P19 Wildfire
**=====================================================================================**;

%macro sshome();
	data sec_residence;
	set in.policy_info;
	if Secondary_Home_flag = 1 then sec_residence='Y';
	else sec_residence='N';
	run;

	proc sql;
		create table out.Seasonal_Secondary_Fac as
		select p.CaseNumber, p.PolicyNumber
			, p.Secondary_Home_flag
			, p.sec_residence
			, f.fire as fire_sec_res
			, f.Theft as theft_sec_res
			, f.'Water Non-Weather'n as waternw_sec_res
			, f.'All Other Non-Weather'n as othernw_sec_res
			, f.Lightning as lightning_sec_res
			, f.'Water Weather'n as waterw_sec_res
			, f.Wind as wind_sec_res
			, f.Hail as hail_sec_res

			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_sec_res
			
			,f.wildfire as wildfire_sec_res
		from sec_residence p left join fac.'327# Seasonal or Secondary$'n f
		on p.sec_residence=substr(f1,1,1);
	quit;

%if &Process = 0 %then %do;
	title 'RULE 327. SEASONAL OR SECONDARY HOME';
	proc freq data=out.Seasonal_Secondary_Fac;
	tables sec_residence
			*fire_sec_res
			*theft_sec_res
			*waternw_sec_res*othernw_sec_res
			*lightning_sec_res
			*waterw_sec_res
			*wind_sec_res
			*hail_sec_res					
			*hurr_sec_res/list missing;
	run;
%end;

%mend;
%SShome();