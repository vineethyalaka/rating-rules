
** RULE 321. PRESENCE OF FENCE **;
	%macro fence();
	%if &Existing = 1 %then %do;
		proc sql;
			create table out.fence_fac as
			select p.casenumber, p.policynumber, p.poolhasfourfootfence
				, f.wind as wind_fence
			from in.policy_info p 
		left join fac.'321# Presence of Fence$'n f
			on p.poolhasfourfootfence = substr(f.f1,1,1);
		quit;

	
		%if &Process = 0 %then %do;
			title 'RULE 321. PRESENCE OF FENCE';
			proc freq data=out.fence_fac;
			tables poolhasfourfootfence
				*wind_fence / list missing;
			run;
		%end;
	%end;

	%if &Existing = 0 %then %do;

	data FenceInd;
		keep casenumber policynumber presenceoffence fenceind;
		set in.policy_info;
		if presenceoffence = 1 then FenceInd = 'Y';
		else FenceInd='N';
	run;


	proc sql;
		create table out.fence_fac as
		select p.casenumber, p.policynumber, i.fenceind
		, f.wind as wind_fence
		from in.policy_info p 
		left join FenceInd i
			on p.casenumber = i.casenumber
		left join fac.'321# Presence of Fence$'n f
			on i.fenceind =substr(f.f1,1,1);
	quit;

	
		%if &Process = 0 %then %do;
			title 'RULE 321. PRESENCE OF FENCE';
			proc freq data=out.fence_fac;
			tables fenceind
				*wind_fence / list missing;
			run;
		%end;
	%end;

	%mend;
	%fence();