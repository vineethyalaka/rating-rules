** RULE 333.A UNDERWRITING TIER (Insurance Score x DRC) **;
**************For on-leveling/impacts, need to include Model Type**************;

/*PA-Specific: put in check to make sure ph wasn't moved to a worse tier*/

%macro insscore();

data ins_tier; *also used in rules 353, 354;
set in.drc_info;
length ins_tier $2.;
if matchcode = 0 then ins_tier='20';
else if matchcode < 0 then ins_tier='21';
else if ScoreModel = 2 then do;
	if insscore >= 950 then ins_tier='1';
	else if 947 <= insscore <= 949 then ins_tier='2';
	else if 932 <= insscore <= 946 then ins_tier='3';
	else if 918 <= insscore <= 931 then ins_tier='4';
	else if 908 <= insscore <= 917 then ins_tier='5';
	else if 892 <= insscore <= 907 then ins_tier='6';
	else if 877 <= insscore <= 891 then ins_tier='7';
	else if 862 <= insscore <= 876 then ins_tier='8';
	else if 846 <= insscore <= 861 then ins_tier='9';
	else if 830 <= insscore <= 845 then ins_tier='10';
	else if 811 <= insscore <= 829 then ins_tier='11';
	else if 796 <= insscore <= 810 then ins_tier='12';
	else if 767 <= insscore <= 795 then ins_tier='13';
	else if 747 <= insscore <= 766 then ins_tier='14';
	else if 727 <= insscore <= 746 then ins_tier='15';
	else if 664 <= insscore <= 726 then ins_tier='16';
	else if 634 <= insscore <= 663 then ins_tier='17';
	else if 601 <= insscore <= 633 then ins_tier='18';
	else if 150 <= insscore <= 600 then ins_tier='19';
	else if   0 <= insscore <= 149 then ins_tier='20';
	else ins_tier='21';
end;
else if ScoreModel = 1 then do;
	if insscore >= 870 then ins_tier='1';
	else if 851 <= insscore <= 869 then ins_tier='2';
	else if 831 <= insscore <= 850 then ins_tier='3';
	else if 811 <= insscore <= 830 then ins_tier='4';
	else if 791 <= insscore <= 810 then ins_tier='5';
	else if 771 <= insscore <= 790 then ins_tier='6';
	else if 751 <= insscore <= 770 then ins_tier='7';
	else if 731 <= insscore <= 750 then ins_tier='8';
	else if 711 <= insscore <= 730 then ins_tier='9';
	else if 691 <= insscore <= 710 then ins_tier='10';
	else if 671 <= insscore <= 690 then ins_tier='11';
	else if 651 <= insscore <= 670 then ins_tier='12';
	else if 631 <= insscore <= 650 then ins_tier='13';
	else if 611 <= insscore <= 630 then ins_tier='14';
	else if 591 <= insscore <= 610 then ins_tier='15';
	else if 531 <= insscore <= 590 then ins_tier='16';
	else if 501 <= insscore <= 530 then ins_tier='17';
	else if 471 <= insscore <= 500 then ins_tier='18';
	else if 199 <= insscore <= 470 then ins_tier='19';
	else if   0 <= insscore <= 198 then ins_tier='20';
	else ins_tier='21';
end;
ins_tier_n=ins_tier*1;
run;

proc sql;
create table drc as 
select *,
case when (score_12_1 not like '%D02%' or score_12_1 is null) and (score_13_1 not like '%D02%' or score_13_1 is null) and 
           (score_14_1 not like '%D02%' or score_14_1 is null) and (score_15_1 not like '%D02%' or score_15_1 is null) and
         ((case when score_12_1 like '%D%' then 1 else 0 end) + (case when score_13_1 like '%D%' then 1 else 0 end) +
          (case when score_14_1 like '%D%' then 1 else 0 end) + (case when score_15_1 like '%D%' then 1 else 0 end)) >= 2
     then 5
     when (score_12_1 not like '%D02%' or score_12_1 is null) and (score_13_1 not like '%D02%' or score_13_1 is null) and 
          (score_14_1 not like '%D02%' or score_14_1 is null) and (score_15_1 not like '%D02%' or score_15_1 is null) and
         ((case when score_12_1 like '%D%' then 1 else 0 end) + (case when score_13_1 like '%D%' then 1 else 0 end) +
          (case when score_14_1 like '%D%' then 1 else 0 end) + (case when score_15_1 like '%D%' then 1 else 0 end)) = 1
     then 4
     when (score_12_1 like '%D02%' or score_13_1 like '%D02%' or score_14_1 like '%D02%' or score_15_1 like '%D02%') and
         ((case when score_12_1 like '%D%' then 1 else 0 end) + (case when score_13_1 like '%D%' then 1 else 0 end) +
          (case when score_14_1 like '%D%' then 1 else 0 end) + (case when score_15_1 like '%D%' then 1 else 0 end)) >= 2
     then 3
     when (score_12_1 like '%D02%' or score_13_1 like '%D02%' or score_14_1 like '%D02%' or score_15_1 like '%D02%') and
         ((case when score_12_1 like '%D%' then 1 else 0 end) + (case when score_13_1 like '%D%' then 1 else 0 end) +
          (case when score_14_1 like '%D%' then 1 else 0 end) + (case when score_15_1 like '%D%' then 1 else 0 end)) = 1
     then 2
     else 1
end as drc_class
from in.drc_info;
quit;
*creates UW tier out of insurance and DRC tiers;
*UW tier 1 = ins tier 1 x DRC 1, UW tier 2 = ins tier 1 x DRC 2... UW Tier 6 = ins tier 2 x DRC 1... UW Tier 105 = ins tier 21 x DRC 5;
proc sql;
	create table uw_tier as
	select p.casenumber, p.policynumber, p.insscore, p.scoremodel, p.matchcode, p.ins_tier, p.ins_tier_n, d.drc_class
		, p.ins_tier_n*5 - (5 - d.drc_class) as uw_tier_n
		, strip(put(calculated uw_tier_n, 8.)) as uw_tier
	from ins_tier as p
	left join drc as d
	on p.casenumber = d.casenumber;
quit;


/*proc sql;*/
/*	create table out.drc_fac as*/
/*	select r.casenumber, r.policynumber, p.drc_class*/
/*		, f.theft as theft_drc*/
/*		, f.'Liability - Bodily injury (Perso'n as liabipi_drc*/
/*		, f.'Liability - Bodily injury (Medic'n as liabimp_drc*/
/*	from in.policy_info as r*/
/*		left join drc p */
/*			on r.casenumber = p.casenumber*/
/*		left join fac.'334# Underwriting Tier II$'n f */
/*			on p.drc_class=f.f1;*/
/*quit;*/

proc sql;
	create table out.uw_tier_fac as 
		select r.casenumber, r.policynumber, p.insscore, p.scoremodel, p.matchcode, p.ins_tier, p.ins_tier_n, p.drc_class, p.uw_tier_n, p.uw_tier
		, f.fire as fire_uw
		, f.theft as theft_uw
		, f.'Water Non-Weather'n as waternw_uw
		, f.'All Other Non-Weather'n as othernw_uw
		, f.lightning as lightning_uw
		, f.'Water Weather'n as waterw_uw
		, f.wind as wind_uw
		, f.hurricane as hurr_uw
		, f.'Liability - Bodily injury (Perso'n as liabipi_uw
		, f.'Liability - Bodily injury (Medic'n as liabimp_uw
		, f.'Liability - Property Damage to O'n as liapd_uw
		, f.'Water Back-up'n as waterbu_uw
	from in.policy_info as r
		left join uw_tier p
			on r.casenumber = p.casenumber
		left join fac.'333#A Underwriting Tier$'n f 
			on p.uw_tier=f.f1;
quit;

%if &Process = 0 %then %do;
title 'RULE 333. UNDERWRITING TIER';
proc freq data=out.uw_tier_fac;
tables uw_tier
	*fire_uw
	*theft_uw
	*waternw_uw 
	*othernw_uw
	*lightning_uw
	*waterw_uw
	*wind_uw 
	*hurr_uw
	*liabipi_uw
	*liabimp_uw
	*liapd_uw / list missing;
run;
%end;

/**/
/*proc sql;*/
/*	create table out.ins_score_fac as*/
/*	select p.casenumber, p.policynumber, p.insscore, p.ins_tier, p.ins_tier_n*/
/*		, f.fire as fire_ins_score*/
/*		, f.theft as theft_ins_score*/
/*		, f.'Water Non-Weather'n as waternw_ins_score*/
/*		, f.'All Other Non-Weather'n as othernw_ins_score*/
/*		, f.lightning as lightning_ins_score*/
/*		, f.'Water Weather'n as waterw_ins_score*/
/*		, f.wind as wind_ins_score*/
/*		, f.hurricane as hurr_ins_score*/
/*		, f.'Liability - Bodily injury (Perso'n as liabipi_ins_score*/
/*		, f.'Liability - Bodily injury (Medic'n as liabimp_ins_score*/
/*		, f.'Liability - Property Damage to O'n as liapd_ins_score		*/
/*		, f.'Water Back-up'n as waterbu_ins_score*/
/*	from ins_tier p left join fac.'333# Underwriting Tier I$'n f	*/
/*		on p.ins_tier=f.f1;*/
/*quit;*/
/*title 'RULE 333. INSURANCE RISK SCORE';*/
/*proc print data=out.ins_score_fac;*/
/*where fire_ins_score*theft_ins_score*waternw_ins_score*othernw_ins_score*lightning_ins_score*waterw_ins_score*/
/**wind_ins_score*hurr_ins_score*liabipi_ins_score*liabimp_ins_score*liapd_ins_score*waterbu_ins_score = .;*/
/*run;*/
/*proc gplot data=out.ins_score_fac;*/
/*	plot (fire_ins_score theft_ins_score waternw_ins_score othernw_ins_score lightning_ins_score waterw_ins_score*/
/* 		wind_ins_score hurr_ins_score liabipi_ins_score liabimp_ins_score liapd_ins_score waterbu_ins_score)*ins_tier_n */
/*		/ overlay legend vaxis=axis1;*/
/*run;*/
/*quit; */
/*proc sgplot data=out.ins_score_fac;*/
/*	histogram ins_tier_n;*/
/*run;*/
%mend;
%insscore();