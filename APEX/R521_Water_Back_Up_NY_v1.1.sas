/*Version History*/
/*Version - Creator - Notes*/
/*v1.0 - JT - Initial Version*/
/*v1.1 - JT - Change rate page name from "Water Backup Limit" to "Water Back-Up Limit" to be consistent with ISO rules*/
/*2019 11 26 TM add new rule factor*/



** RULE 521. WATER BACK UP **;
%macro wbu();
*Increased Limit Factor*;
	data waterbulim;
	set fac.'521# Water Back-Up Inc Lim$'n end=eof;
	if 'Water Back-Up Limit'n = . then delete;
	output;
	if eof = 1 then do;
		'Water Back-Up Limit'n = 0; 'Water Back-Up'n = 0;
		output;
	end;
	run;
	proc sql;
		create table waterbu as
		select distinct p.casenumber, p.policynumber, p.WaterSump, e.HO_04_95_IND
			, case when e.HO_04_95_IND="Y" and p.WaterSump=0 then 5000 
				 when e.HO_04_95_IND="N" and p.WaterSump^=0 then 0 
				else p.WaterSump end as WaterSump_Limit
		from in.Production_Info p inner join in.Endorsement_Info e
		on p.casenumber = e.casenumber;
	quit;
	proc sql;
		create table out.waterbulim_fac as
		select distinct p.*
			, f.'Water Back-Up'n as waterbu_lim
		from waterbu p left join waterbulim f
			on p.WaterSump_Limit=f.'Water Back-Up Limit'n;
	quit;

	*Base Premium*;
	proc sql;
		create table out.waterbu_base_premium as
		select *
			, round(waterbu_base_rate
/*			* waterbu_grid*/
/*			* waterbu_census*/
			* waterbu_terr
			* waterbu_lim
			* waterbu_bsmnt
			* waterbu_found_type_yr_built
			* waterbu_num_stories
			* waterbu_num_res
			* waterbu_fin_bsmnt
			* waterbu_ins_score
			* waterbu_claimxPeril
			* waterbu_claim
			* r403_HO_04_90_fac,1.0)
			as waterbu_base_premium
		from out.base_rate f1
/*			inner join out.grid_fac				  f2 on f1.casenumber=f2.casenumber*/
/*			inner join out.census_fac			  f3 on f1.casenumber=f3.casenumber*/
			inner join out.census_tier_terr       f2 on f1.casenumber=f2.casenumber
			inner join out.waterbulim_fac		  f4 on f1.casenumber=f4.casenumber
			inner join out.basement_fac			  f5 on f1.casenumber=f5.casenumber
			inner join out.num_residents_fac	  f6 on f1.casenumber=f6.casenumber
			inner join out.finished_bsmnt_fac	  f7 on f1.casenumber=f7.casenumber
			inner join out.ins_score_fac		  f8 on f1.casenumber=f8.casenumber
			inner join out.priorclaim_xWater_fac  f9 on f1.casenumber=f9.casenumber
			inner join out.priorclaim_Water_fac	 f10 on f1.casenumber=f10.casenumber
			inner join out.r403_CovC_Repl_Cost	 f11 on f1.casenumber=f11.casenumber
			inner join out.foundation_type_year_fac f12 on f1.casenumber=f12.casenumber
			inner join out.num_stories_fac       f13 on f1.casenumber=f13.casenumber
;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 521. WATER BACK UP';
	proc freq data=out.waterbulim_fac;
	tables WaterSump_Limit*waterbu_lim / list missing;
	run;
	proc print data=out.waterbu_base_premium;
	where waterbu_base_premium = .;
	run;
	proc sql;
		select mean(waterbu_base_premium) as waterbu_base_prem_avg
		from out.waterbu_base_premium
		where waterbu_lim>0; 
	quit;
%end;
%mend;
%wbu();
