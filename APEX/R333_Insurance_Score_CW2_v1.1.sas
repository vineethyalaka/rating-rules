
** RULE 333. INSURANCE RISK SCORE **;
**=====================================================================================**
**************For on-leveling/impacts, need to include Model Type**************
History: 2015 03 17 JT  Initial draft
		 2015 05 26 SY  Added Hurricane peril indicator
		 2017 07 24	BF	Changing to truerisk insurance model
		 2017 08 10 JK  APEX 2.0 adjustment to include correct number of tiers
		 2019 05 30 TM  Change matchcode = 0 to 57 no score, matchcode = -1 to 56 no hit
**=====================================================================================**;
%macro insscore();
	data ins_tier; *also used in rules 353, 354;
	set in.drc_info;
	length ins_tier $2.;
	if matchcode = 0 then ins_tier='57';
	else if matchcode < 0 then ins_tier='56';
	else do;
	if insscore >= 850 then ins_tier='1';
	else if 840 <= insscore <= 849 then ins_tier = '2';
	else if 830 <= insscore <= 839 then ins_tier = '3';
	else if 820 <= insscore <= 829 then ins_tier = '4';
	else if 810 <= insscore <= 819 then ins_tier = '5';
	else if 800 <= insscore <= 809 then ins_tier = '6';
	else if 790 <= insscore <= 799 then ins_tier = '7';
	else if 780 <= insscore <= 789 then ins_tier = '8';
	else if 770 <= insscore <= 779 then ins_tier = '9';
	else if 760 <= insscore <= 769 then ins_tier = '10';
	else if 750 <= insscore <= 759 then ins_tier = '11';
	else if 740 <= insscore <= 749 then ins_tier = '12';
	else if 730 <= insscore <= 739 then ins_tier = '13';
	else if 720 <= insscore <= 729 then ins_tier = '14';
	else if 710 <= insscore <= 719 then ins_tier = '15';
	else if 700 <= insscore <= 709 then ins_tier = '16';
	else if 690 <= insscore <= 699 then ins_tier = '17';
	else if 680 <= insscore <= 689 then ins_tier = '18';
	else if 670 <= insscore <= 679 then ins_tier = '19';
	else if 660 <= insscore <= 669 then ins_tier = '20';
	else if 650 <= insscore <= 659 then ins_tier = '21';
	else if 640 <= insscore <= 649 then ins_tier = '22';
	else if 630 <= insscore <= 639 then ins_tier = '23';
	else if 620 <= insscore <= 629 then ins_tier = '24';
	else if 610 <= insscore <= 619 then ins_tier = '25';
	else if 600 <= insscore <= 609 then ins_tier = '26';
	else if 590 <= insscore <= 599 then ins_tier = '27';
	else if 580 <= insscore <= 589 then ins_tier = '28';
	else if 570 <= insscore <= 579 then ins_tier = '29';
	else if 560 <= insscore <= 569 then ins_tier = '30';
	else if 550 <= insscore <= 559 then ins_tier = '31';
	else if 540 <= insscore <= 549 then ins_tier = '32';
	else if 530 <= insscore <= 539 then ins_tier = '33';
	else if 520 <= insscore <= 529 then ins_tier = '34';
	else if 510 <= insscore <= 519 then ins_tier = '35';
	else if 500 <= insscore <= 509 then ins_tier = '36';
	else if 490 <= insscore <= 499 then ins_tier = '37';
	else if 480 <= insscore <= 489 then ins_tier = '38';
	else if 470 <= insscore <= 479 then ins_tier = '39';
	else if 460 <= insscore <= 469 then ins_tier = '40';
	else if 450 <= insscore <= 459 then ins_tier = '41';
	else if 440 <= insscore <= 449 then ins_tier = '42';
	else if 430 <= insscore <= 439 then ins_tier = '43';
	else if 420 <= insscore <= 429 then ins_tier = '44';
	else if 410 <= insscore <= 419 then ins_tier = '45';
	else if 400 <= insscore <= 409 then ins_tier = '46';
	else if 390 <= insscore <= 399 then ins_tier = '47';
	else if 380 <= insscore <= 389 then ins_tier = '48';
	else if 370 <= insscore <= 379 then ins_tier = '49';
	else if 360 <= insscore <= 369 then ins_tier = '50';
	else if 350 <= insscore <= 359 then ins_tier = '51';
	else if 340 <= insscore <= 349 then ins_tier = '52';
	else if 330 <= insscore <= 339 then ins_tier = '53';
	else if 320 <= insscore <= 329 then ins_tier = '54';
	else ins_tier = '55';
	end;
	ins_tier_n=ins_tier*1;
	run;
	proc sql;
		create table out.ins_score_fac as
		select p.casenumber, p.policynumber, p.insscore, p.ins_tier, p.ins_tier_n, p.matchcode
			, f.fire as fire_ins_score
			, f.theft as theft_ins_score
			, f.'Water Non-Weather'n as waternw_ins_score
			, f.'All Other Non-Weather'n as othernw_ins_score
			, f.lightning as lightning_ins_score
			, f.'Water Weather'n as waterw_ins_score
			, f.wind as wind_ins_score
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_ins_score
			, f.'Liability - Bodily injury (Perso'n as liabipi_ins_score
			, f.'Liability - Bodily injury (Medic'n as liabimp_ins_score
			, f.'Liability - Property Damage to O'n as liapd_ins_score		
			, f.'Water Back-up'n as waterbu_ins_score
		from ins_tier p left join fac.'333# Insurance Risk Score$'n f
			on p.ins_tier=f.f3;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 333. INSURANCE RISK SCORE';
	proc print data=out.ins_score_fac;
	where fire_ins_score*theft_ins_score*waternw_ins_score*othernw_ins_score*lightning_ins_score*waterw_ins_score
	*wind_ins_score*hurr_ins_score*liabipi_ins_score*liabimp_ins_score*liapd_ins_score*waterbu_ins_score = .;
	run;
	proc gplot data=out.ins_score_fac;
		plot (fire_ins_score theft_ins_score waternw_ins_score othernw_ins_score lightning_ins_score waterw_ins_score
	 		wind_ins_score hurr_ins_score liabipi_ins_score liabimp_ins_score liapd_ins_score waterbu_ins_score)*ins_tier_n 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.ins_score_fac;
		histogram ins_tier_n;
	run;
%end;
%mend;
%insscore();