** FINAL PREMIUM **;

/*Modified from CW code*/
/*10 19 2017 BF Updated to include rule 361 multiproduct discount. Also deleting conversion rule*/
/* 11/18/19 TM updated to RS version, exclude min_prem*/

%macro final();
	proc sql;
		create table out.step8_uncapped_calc_premium as
		select *
			, round((p1.adjusted_subtotal_premium
			+ p2.Property_Premium
			+ p3.Total_Liability_Premium
			+ p4.policy_expense_fee),1)
			as final_calc_prem

			, round(calculated final_calc_prem*p6.affinity,1) as Affnity_Discount
			, (calculated final_calc_prem - calculated Affnity_Discount) as uncapped_calc_premium

/*			, case when calculated discounted_final_calc_prem <= p7.min_premium then p7.min_premium*/
/*				else calculated discounted_final_calc_prem end */
/*			as final_premium*/
		from out.step4_Adjusted_subtotal_premium p1
			inner join out.step5_Property_Premium	p2 on p1.casenumber = p2.casenumber
			inner join out.step6_Liability_Premium	p3 on p1.casenumber = p3.casenumber
			inner join out.step7_policy_expense_fee	p4 on p1.casenumber = p4.casenumber
			inner join out.affinity_fac				p6 on p1.casenumber = p6.casenumber;
/*			, out.min_premium p7;*/
	quit;

%if &Process = 0 %then %do;
	title 'STEP 8: FINAL PREMIUM (RULE 301.A.8)';
	proc print data=out.step8_uncapped_calc_premium;
	where final_calc_prem = .;
	run;
	proc sql;
		create table out.average_final_premium as
		select mean(adjusted_subtotal_premium) as adjusted_subtotal_premium_avg
			, mean(policy_conversion_fac) as policy_conversion_fac_avg
			, mean(affinity) as affinity_avg
			, mean(final_premium) as final_premium_avg
		from out.step8_final_premium;
	quit;
	proc sgplot data=out.step8_final_premium;
		histogram final_premium;
	run;

%end;
%mend;
%final();
