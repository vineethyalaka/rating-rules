** RULE 352. AGE OF HOME BY BILL PLAN **;
/* 11/7/2018 ITR 13402  Change max age from hard code to macro variable.
                        Add a temp table home_age_adj_r352 to deal the case when home age >max age. by TM*/

/* 12/22/2018 ITR 13706 Change when f1 is read as string*/
%macro agebill();
    data r352;
	set fac.'352# Age of Home by Bill Plan$'n;
	home_age = f1*1;
	run;

    proc sql noprint;
		 select max(home_age)+1
		 		into :max_age_bill
	     from r352;

		 update r352
		        set home_age = &max_age_bill
				where home_age=. and f3 ne .;
	quit;

	data exp_home_age_x_bill_plan_fac (keep=bill_plan home_age factor);
	length bill_plan $20.;
	set r352;
	if f3=. then delete;
	bill_plan='10-pay'; factor = 'Expense Fee'n; output;
	bill_plan='4-pay'; factor = f3; output;
	bill_plan='Full-pay or Mortgage'; factor = f4; output;
	run;

	proc sql;
/* create a temp table to change home age >101 to 101, so that home age can be used to join 352 table*/
	    create table home_age_adj_r352 as
		select * from home_age;
		update home_age_adj_r352
		set home_age = &max_age_bill
		where home_age > &max_age_bill;

		create table out.exp_home_age_x_bill_plan_fac as
		select p1.*, p2.home_age, f.factor as exp_home_age_x_bill_plan_fac
		from bill_plan p1 
			inner join home_age p2 on p1.casenumber = p2.casenumber
			inner join home_age_adj_r352 P3 on p1.casenumber = p3.casenumber
			left join exp_home_age_x_bill_plan_fac f on f.bill_plan = p1.BillPlanType 
				and f.home_age = p3.home_age;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 352. AGE OF HOME BY BILL PLAN';
	proc print data=out.exp_home_age_x_bill_plan_fac;
	where exp_home_age_x_bill_plan_fac = .;
	run;
	proc sort data=out.exp_home_age_x_bill_plan_fac; by BillPlanType; run;
	proc gplot data=out.exp_home_age_x_bill_plan_fac; by BillPlanType;
		plot exp_home_age_x_bill_plan_fac*home_age 
			/ overlay legend vaxis=axis1;
	run;
	quit;
%end;
%mend;
%agebill();