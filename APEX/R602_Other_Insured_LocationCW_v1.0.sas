
** RULE 602. OTHER INSURED LOCATION **;
/*data r602fac;*/
/*set fac.'602# Other Insured Location$'n;*/
/*if f1="" then delete;*/
/*fac1='Liability - Bodily injury (Perso'n*1;*/
/*fac2='Liability - Bodily injury (Medic'n*1;*/
/*fac3='Liability - Property Damage to O'n*1;*/
/*run;*/
/*proc sql;*/
/*	create table liaOtherInsLoc as */
/*	select distinct p.casenumber, p.policynumber, count(*) as secondary_residence_count*/
/*	from in.Additional_Residences_Info p*/
/*	where p.add_dwelling_type = 2*/
/*	group by p.casenumber*/
/*	;*/
/*quit;*/
/*proc sql;*/
/*    create table out.lia_OtherInsLoc_fac as*/
/*	select p.casenumber, p.policynumber*/
/*		, case when a.secondary_residence_count = . then 0 else a.secondary_residence_count end as secondary_residence_count*/
/*		, f.fac1 * calculated secondary_residence_count as liabipi_OtherInsLoc*/
/*		, f.fac2 * calculated secondary_residence_count as liabimp_OtherInsLoc*/
/*		, f.fac3 * calculated secondary_residence_count as liapd_OtherInsLoc*/
/*	from r602fac f, in.policy_info p left join liaOtherInsLoc a*/
/*		on p.casenumber = a.casenumber*/
/*		;*/
/*quit;*/
/*title 'RULE 602. OTHER INSURED LOCATION';*/
/*proc freq data=out.lia_OtherInsLoc_fac;*/
/*tables secondary_residence_count*/
/*	*liabipi_OtherInsLoc*/
/*	*liabimp_OtherInsLoc*/
/*	*liapd_OtherInsLoc / list missing;*/
/*run;*/