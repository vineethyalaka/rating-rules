** RULE 347 OFF PREMISES THEFT EXCLUSION **;
%macro theftexclusion();
	proc sql;
		create table work.TheftExclusion as
		select p.CaseNumber, p.PolicyNumber, f.F1 as CountyName
		, case when f.F1 in('Bronx County','Kings County','Nassau County','New York County',
			'Putnam County','Queens County','Richmond County','Rockland County','Suffolk County',
			'Westchester County') and e.TheftExclusion = 1 then 'Yes' 
			else 'No' end as TheftExclusion
		from out.territory as p left join in.endorsement_info as e on p.CaseNumber = e.CaseNumber
			left join fac.'301#A County (Territory)$'n as f on p.CountyID = f.F2;
	quit;

	proc sql;
		create table out.TheftExclusion_fac as
		select p.CaseNumber, p.PolicyNumber, p.CountyName, p.TheftExclusion, f.Theft as theft_exclusion
		from work.TheftExclusion as p left join fac.'347# Theft Exclusion$'n f on p.TheftExclusion = f.F1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 347 THEFT EXCLUSION';
	proc freq data=out.theftexclusion_fac;
	run;;
%end;
%mend;
%theftexclusion();