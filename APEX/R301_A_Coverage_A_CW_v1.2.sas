
** RULE 301.A COVERAGE A **;

**=====================================================================================**
History: 2019 11 14 Yu Fang added P19 wild-fire in Base Rates
**=====================================================================================**;


%macro cova();

	proc sql;
	    create table Coveragea as
		select p1.CaseNumber, p1.PolicyNumber, p1.CoverageA, p2.HO_0456_FLAG, p2.HO_04_56_pct
			, case when p2.HO_0456_FLAG='0' then p1.CoverageA else round(p1.CoverageA,1000) end as CoverageA_adj
		from in.policy_info p1
		inner join in.endorsement_info p2 on p1.CaseNumber = p2.CaseNumber;
	/*	left join r402c f on p2.HO_04_56_pct = f.pct;*/
	quit;
	data coveragea_aoi;
	set fac.'301#A Coverage A$'n;
	if f1 = . and f2 = . then delete;
	aoi_a_L = lag(f1); rename f1 = aoi_a_H;
	array peril (9) Fire 'Water Non-Weather'n 'All Other Non-Weather'n Lightning 'Water Weather'n Wind Hail Hurricane 'Wildfire'n;
	array fac_l (9) fire_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l wildfire_l;
	array fac_h (9) fire_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h wildfire_h;
	do i = 1 to 9;
		fac_l(i) = lag(peril(i));
		fac_h(i) = peril(i);
	end;
	if aoi_a_L = . then delete;
	drop i;
	run;
	proc sql;
		create table coverage_a as
		select p.casenumber, p.policynumber, p.coveragea, p.HO_0456_FLAG, p.HO_04_56_pct, p.CoverageA_adj, f.aoi_a_L, f.aoi_a_H
			, f.fire_l, f.fire_h
			, f.waternw_l, f.waternw_h
			, f.othernw_l, f.othernw_h
			, f.lightning_l, f.lightning_h
			, f.waterw_l, f.waterw_h
			, f.wind_l, f.wind_h
			, f.hail_l, f.hail_h

			, Case when &Hurricane = 1 then f.hurr_l else 0 end as hurr_l
			, Case when &Hurricane = 1 then f.hurr_h else 0 end as hurr_h

			, wildfire_l, wildfire_h
		from Coveragea p 
			left join coveragea_aoi f on (p.CoverageA_adj <= f.aoi_a_H or f.aoi_a_H = .) and p.CoverageA_adj > f.aoi_a_L
		;
	quit;
	data out.coverage_a_fac; set coverage_a;
	array fac_l (9) fire_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l wildfire_l;
	array fac_h (9) fire_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h wildfire_h;
	array fac   (9) fire_cova waternw_cova othernw_cova lightning_cova waterw_cova wind_cova hail_cova hurr_cova wildfire_cova;
	do i = 1 to 9;
		if aoi_a_H = . then fac(i) = round(fac_h(i)*(CoverageA_adj - aoi_a_L)/100000 + fac_l(i),.0001);
		else fac(i) = round((fac_h(i) - fac_l(i))*(CoverageA_adj - aoi_a_L)/(aoi_a_H - aoi_a_L) + fac_l(i),.0001);
	end;
	drop i;
	run;

%if &Process = 0 %then %do;
	title 'RULE 301.A COVERAGE A';
	proc print data=out.coverage_a_fac;
	where fire_cova*waternw_cova*othernw_cova*lightning_cova*waterw_cova*wind_cova*hail_cova*hurr_cova = .;
	run;
	proc gplot data=out.coverage_a_fac;
		plot (fire_cova waternw_cova othernw_cova lightning_cova waterw_cova wind_cova hail_cova hurr_cova)*CoverageA_adj 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
%end;

%mend;
%cova();