
** RULE 333. INSURANCE RISK SCORE **;
**=====================================================================================**
**************For on-leveling/impacts, need to include Model Type**************
History: 2016 12 08 JL  Initial draft based on Cw v1.1, NY APEX rule language uses tier in place of insurance score
**=====================================================================================**;
%macro insscore();
data test;
set fac.'333# Tier$'n;
run;


	data ins_tier; *also used in rules 353, 354;
	set in.drc_info;
	length ins_tier $2.;
	if matchcode = 0 then ins_tier='20';
	else if matchcode < 0 then ins_tier='21';
	else if ScoreModel = 2 then do;
		if insscore >= 950 then ins_tier='1';
		else if 947 <= insscore <= 949 then ins_tier='2';
		else if 932 <= insscore <= 946 then ins_tier='3';
		else if 918 <= insscore <= 931 then ins_tier='4';
		else if 908 <= insscore <= 917 then ins_tier='5';
		else if 892 <= insscore <= 907 then ins_tier='6';
		else if 877 <= insscore <= 891 then ins_tier='7';
		else if 862 <= insscore <= 876 then ins_tier='8';
		else if 846 <= insscore <= 861 then ins_tier='9';
		else if 830 <= insscore <= 845 then ins_tier='10';
		else if 811 <= insscore <= 829 then ins_tier='11';
		else if 796 <= insscore <= 810 then ins_tier='12';
		else if 767 <= insscore <= 795 then ins_tier='13';
		else if 747 <= insscore <= 766 then ins_tier='14';
		else if 727 <= insscore <= 746 then ins_tier='15';
		else if 664 <= insscore <= 726 then ins_tier='16';
		else if 634 <= insscore <= 663 then ins_tier='17';
		else if 601 <= insscore <= 633 then ins_tier='18';
		else if 150 <= insscore <= 600 then ins_tier='19';
		else if   0 <= insscore <= 149 then ins_tier='20';
		else ins_tier='21';
	end;
	else if ScoreModel = 1 then do;
		if insscore >= 870 then ins_tier='1';
		else if 851 <= insscore <= 869 then ins_tier='2';
		else if 831 <= insscore <= 850 then ins_tier='3';
		else if 811 <= insscore <= 830 then ins_tier='4';
		else if 791 <= insscore <= 810 then ins_tier='5';
		else if 771 <= insscore <= 790 then ins_tier='6';
		else if 751 <= insscore <= 770 then ins_tier='7';
		else if 731 <= insscore <= 750 then ins_tier='8';
		else if 711 <= insscore <= 730 then ins_tier='9';
		else if 691 <= insscore <= 710 then ins_tier='10';
		else if 671 <= insscore <= 690 then ins_tier='11';
		else if 651 <= insscore <= 670 then ins_tier='12';
		else if 631 <= insscore <= 650 then ins_tier='13';
		else if 611 <= insscore <= 630 then ins_tier='14';
		else if 591 <= insscore <= 610 then ins_tier='15';
		else if 531 <= insscore <= 590 then ins_tier='16';
		else if 501 <= insscore <= 530 then ins_tier='17';
		else if 471 <= insscore <= 500 then ins_tier='18';
		else if 199 <= insscore <= 470 then ins_tier='19';
		else if   0 <= insscore <= 198 then ins_tier='20';
		else ins_tier='21';
	end;
	ins_tier_n=ins_tier*1;
	run;
	proc sql;
		create table out.ins_score_fac as
		select p.casenumber, p.policynumber, p.insscore, p.ins_tier, p.ins_tier_n
			, f.fire as fire_ins_score
			, f.theft as theft_ins_score
			, f.'Water Non-Weather'n as waternw_ins_score
			, f.'All Other Non-Weather'n as othernw_ins_score
			, f.lightning as lightning_ins_score
			, f.'Water Weather'n as waterw_ins_score
			, f.wind as wind_ins_score
			, %if &Hurricane = 1 %then f.Hurricane; %else 0; as hurr_ins_score
			, f.'Liability - Bodily injury (Perso'n as liabipi_ins_score
			, f.'Liability - Bodily injury (Medic'n as liabimp_ins_score
			, f.'Liability - Property Damage to O'n as liapd_ins_score		
			, f.'Water Back-up'n as waterbu_ins_score
		from ins_tier p left join fac.'333# Tier$'n f
			on p.ins_tier=f.f1;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 333. Tier';
	proc print data=out.ins_score_fac;
	where fire_ins_score*theft_ins_score*waternw_ins_score*othernw_ins_score*lightning_ins_score*waterw_ins_score
	*wind_ins_score*hurr_ins_score*liabipi_ins_score*liabimp_ins_score*liapd_ins_score*waterbu_ins_score = .;
	run;
	proc gplot data=out.ins_score_fac;
		plot (fire_ins_score theft_ins_score waternw_ins_score othernw_ins_score lightning_ins_score waterw_ins_score
	 		wind_ins_score hurr_ins_score liabipi_ins_score liabimp_ins_score liapd_ins_score waterbu_ins_score)*ins_tier_n 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
	proc sgplot data=out.ins_score_fac;
		histogram ins_tier_n;
	run;
%end;
%mend;
%insscore();