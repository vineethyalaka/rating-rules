** RULE 355. AGE OF HOME BY INSURANCE SCORE **;
/*ITR 13402 11/7/2018 Change 21 ins_tier to 57 ins_tier according to rate sheet
                      Change max age from hard code to macro variable.
                      Add a temp table home_age_adj_r355 to deal the case when home age > max age by TM*/
/*2019/01/07 MOve v1.1 from SH merged folder to APEX */
%macro agescore();
	data r355;
	set fac.'355# Age of Home by Ins Score$'n;
	if f3=. or f1 = 'Age of Home' then delete;
	home_age = input(f1, 4.);
	run;

    proc sql;
		 select max(home_age)+1
		 		into :max_age_score
	     from r355;

		 update r355
		        set home_age = &max_age_score
				where home_age =. and f3 ne .;
	quit;


	data exp_home_age_x_ins_score_fac (keep=home_age ins_tier factor);
	set r355(rename=('Expense Fee'n=f2));
	array f (57) f2 - f58;
	do i = 1 to 57;
		ins_tier = strip(put(i,2.)); factor = f(i);
		output;
	end;
	run;

	proc sql;
		create table home_age_adj_r355 as
		select * from home_age;
		update home_age_adj_r355
		set home_age = &max_age_score
		where home_age > &max_age_score;

		create table out.exp_home_age_x_ins_score_fac as
		select p1.casenumber, p1.policynumber, p1.PolicyIssueDate, p1.PolicyEffectiveDate, p1.PropertyYearBuilt, p1.home_age
			, p2.matchcode, p2.ScoreModel, p2.insscore, p2.ins_tier, f.factor as exp_home_age_x_ins_score_fac
		from home_age p1 
			inner join ins_tier p2 on p1.casenumber = p2.casenumber
			inner join home_age_adj_r355 p3 on p3.casenumber = p1.casenumber
			left join exp_home_age_x_ins_score_fac f on f.home_age = p3.home_age and f.ins_tier = p2.ins_tier;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 355. AGE OF HOME BY INSURANCE SCORE';
	proc print data=out.exp_home_age_x_ins_score_fac;
	where exp_home_age_x_ins_score_fac = .;
	run;
	proc sort data=out.exp_home_age_x_ins_score_fac; by ins_tier; run;
	proc gplot data=out.exp_home_age_x_ins_score_fac; by ins_tier;
		plot exp_home_age_x_ins_score_fac*home_age 
			/ overlay legend vaxis=axis1;
	run;
	quit; 
%end;
%mend;
%agescore();