** RULE 516. SCHEDULED PERSONAL PROPERTY **;
%macro spp();
*Rate*;
	data r516_rate;
	set fac.'516# Scheduled PP Rate$'n;
	if strip('Type of Property'n) = '' then delete;
	rename 'Coverage Limit Increment (i#e# r'n = Increment;
	run;
	data SPP_rate (keep=CaseNumber PolicyNumber HO_0461_flag SPP_item SPP_limit);
	set in.Endorsement_Info (keep=CaseNumber PolicyNumber HO_0461_flag HO_0461_01 - HO_0461_11);
	array limit (11) HO_0461_01 - HO_0461_11;
	array item (11) $20. ("Jewelry" "Furs" "Cameras" "Musical Instruments" "Silverware"
						  "Golfers' Equipment" "Fine Arts" "Stamp Collections" "Coin Collections" "Bicycles" "Miscellaneous");
	if HO_0461_flag = '1' then do i=1 to 11;
		SPP_item = item(i);
		SPP_limit = limit(i);
		if limit(i) > 0 then output;
	end;
	run;
	proc sql;
		create table out.SPP_item_rate as
		select p.*
			, f.rate
			, f.Increment
			, rate*SPP_limit/Increment as item_rate
		from SPP_rate p
			left join r516_rate f on strip(upcase(p.SPP_item)) = strip(upcase(f.'Type of Property'n));
	quit;
	
	*Limits for Theft Peril*;
	data r516_amount_theft;
	set fac.'516# Scheduled PP Amount$'n;
	if 'Scheduled Personal Property Amou'n = . and 'Theft Factor'n = . then delete;
	SPP_L = lag('Scheduled Personal Property Amou'n); fac_L = lag('Theft Factor'n);
	rename 'Scheduled Personal Property Amou'n = SPP_H 'Theft Factor'n = fac_H;
	if SPP_L = . then delete;
	run;
	proc sql;
		create table out.SPP_Amt_Theft_fac as
		select p.CaseNumber, p.PolicyNumber, p.SPP_item, p.SPP_limit, f.SPP_L, f.SPP_H, f.fac_L, f.fac_H
			, case when p.SPP_limit = 0 then 1
				when f.SPP_H = . then round(f.fac_H*(p.SPP_limit - f.SPP_L)/100 + f.fac_L,.0001)
				else round((f.fac_H - f.fac_L)*(p.SPP_limit - f.SPP_L)/(f.SPP_H - f.SPP_L) + f.fac_L,.0001)
				end as SPP_Amt_Theft_fac
		from out.SPP_item_rate p 
			left join r516_amount_theft f on (p.SPP_limit <= f.SPP_H or f.SPP_H = .) and p.SPP_limit > f.SPP_L;
	quit;

	*SPP Factor*;
	data out.r516_fac;
	set fac.'516# Scheduled PP Factor$'n;
	if f1='Theft' then theftfac=f2;
	if f1='All Other Non-Weather' then othernwfac=f2;
	retain theftfac;
	if othernwfac=. then delete;
	keep theftfac othernwfac;
	run;

	*SPP Item Base Premium*;
	proc sql;
		create table out.SPP_item_base_premium as
		select f1.*, f2.theft_disc_base_premium, f2.othernw_disc_base_premium
			, f3.SPP_Amt_Theft_fac, f4.theftfac, f4.othernwfac, f6.r403_HO_04_90_fac
			,round(
				round(
						(f1.item_rate
						+max((f2.theft_disc_base_premium
						* f3.SPP_Amt_Theft_fac
						* f4.theftfac)
						- f2.theft_disc_base_premium,0)
						+(f2.othernw_disc_base_premium
						* f4.othernwfac))
						* f6.r403_HO_04_90_fac
				, 0.0001)
			,1.0)
			as SPP_base_premium

		from out.r516_fac f4, out.SPP_item_rate f1
			inner join out.step2_base_premium  f2 on f1.casenumber=f2.casenumber
			inner join out.SPP_Amt_Theft_fac   f3 on f1.casenumber=f3.casenumber and f1.SPP_item=f3.SPP_item
	/*		inner join out.r516_fac			   f4 on f1.casenumber=f4.casenumber*/
			inner join out.r403_CovC_Repl_Cost f6 on f1.casenumber=f6.casenumber;
	quit;


	proc sql;
		create table out.SPP_base_premium as
		select f1.casenumber
		, case when sum(f2.SPP_Base_Premium)=. then 0 else sum(f2.SPP_Base_Premium) end as SPP_Base_Premium
		from in.endorsement_info f1
		left join out.SPP_item_base_premium f2
		on f1.Casenumber = f2.CaseNumber
		group by f1.casenumber
		;
	quit;


%if &Process = 0 %then %do;
	title 'RULE 516. SCHEDULED PERSONAL PROPERTY';
	proc freq data=out.SPP_base_premium;
	tables SPP_Base_Premium / list missing;
	run;
	proc sql;
		select mean(SPP_base_premium) as SPP_base_prem_avg
		from out.SPP_base_premium;
	quit;
%end;
%mend;
%spp();