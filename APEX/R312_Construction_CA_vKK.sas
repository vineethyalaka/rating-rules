** RULE 312. CONSTRUCTION **;
**=====================================================================================**
History: 2015 03 17 JT  Initial draft
		 2016 08 24 SY  Revised construction definitions to include Masonry Veneer
		 2017 07 26 KK  Revised first proc sql statement to use f1 instead of f2 since CA doesn't have EQ
**=====================================================================================**;

%macro construct();
	data construction_0; *also used in rule 505 earthquake;
	set in.policy_info;
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Aluminium','Aluminum');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'EIFS/Synthetic','EIFS/ Synthetic');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Hardy Plank','HardiPlank');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Logs','Log');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Masonry non-combustible','Non-combustible');
	propertyconstructionclass=tranwrd(propertyconstructionclass,'Wood shakes','Wooden Shakes');
	if strip(propertyconstructionclass) = "" then propertyconstructionclass = "All Other";
	run;

	proc sql;
		create table construction as
		select p.*, f.f1 as ConstructionType
		from construction_0 p left join fac.'312# Construction$'n f
			on compress(upcase(p.propertyconstructionclass))=compress(upcase(f.f1));
	quit;

	proc sql;
		create table out.construction_class_fac as
		select distinct p.casenumber, p.policynumber, p.propertyconstructionclass
			, f.wind as wind_const_class
		from construction p left join fac.'312# Construction$'n f
			on compress(upcase(p.propertyconstructionclass))=compress(upcase(f.f1));
	quit;

%if &Process = 0 %then %do;
	title 'RULE 312. CONSTRUCTION';
	proc freq data=out.construction_class_fac;
	tables 	propertyconstructionclass
		*wind_const_class/ list missing;
	run;
%end;

%mend;
%construct();