** RULE 301.A zipcode **;
/*Add P4 All Other Non-Weather, P6 Water Weather*/
/*Add case when zipcode not in rate sheet, assign 1.0 as factors*/
/*2/20/2020 TM Move from SH folder and remove SH perils*/
%macro zipcode();
	proc sql;
		create table zipcode_fac as
		select p.casenumber, p.policynumber, p.PropertyZip5
            , f.Fire as fire_zipcode
			, f.Theft as theft_zipcode
			, f.'Water Non-Weather'n as waternw_zipcode
			, f.'All Other Non-Weather'n as othernw_zipcode  
			, f.Lightning as lightning_zipcode
			, f.'Water Weather'n as waterw_zipcode
			, f.Wind as wind_zipcode
			, f.hail as hail_zipcode
/*			, f.'Cov C - Fire'n as Cov_C_Fire_zipcode*/
/*			, f.'Cov C - EC'n as Cov_C_EC_zipcode*/
/*			, f.'Property Damage due to Burglary'n as PD_Burglary_zipcode*/
			, f.'Liability - Bodily injury (Perso'n as liabipi_zipcode
			, f.'Liability - Bodily injury (Medic'n as liabimp_zipcode
			, f.'Liability - Property Damage to O'n as liapd_zipcode
			, f.'Water Back-up'n as waterbu_zipcode
/*			, f.'Limited Theft Coverage'n as Limited_Theft_zipcode*/
/*			, f.'Expense Fee'n as Expense_zipcode*/
		from in.policy_info p left join fac.'301#A Zip Code$'n f 
			on p.PropertyZip5 = f.F1;
	quit;

/*fill all missing value with 1*/
	Data out.zipcode_fac;
	set work.zipcode_fac;
	array a(*) _numeric_;
	do i =1 to dim(a);
	if a(i) = . then a(i)=1;
	end; 
	drop i;
	run;

;
%mend;
%zipcode();
