/*
1. ITR 14866 NY Mosaic - Wanwan - Initial file

*/

data r382;
	set fac.'382# Fire Department Type$'n;
run;

proc sql;
	create table out.r382_fac as

		select * , r.fire as fire_dep_type from (
        select *,
		case when FS1DepartmentType ^= 'Volunteer' then 'All Other' else FS1DepartmentType end as FS1DepartmentType_updated
		from in.policy_info)  as temp 
		left join r382 r
		on r.f1 =  temp.FS1DepartmentType_updated;
quit;

