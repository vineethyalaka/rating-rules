** RULE 519. SPECIAL COMPUTER COVERAGE **;
%macro sc();
	data r542;
	set fac.'542# IBHS Fortified Roof$'n;
	if strip(f1) = '' then delete;
	run;
	proc sql;
		create table FortifiedRoof_Rate as
		select p.CaseNumber, p.PolicyNumber, p.HA_0638_FLAG
			, case when p.HA_0638_FLAG= 1 and PolicyFormNumber = 3 then round(f.charge,1.0) else 0 end as FortifiedRoof_Rate
		from in.Endorsement_Info p, r542 f;
	quit;

%if &Process = 0 %then %do;
	title 'RULE 519. SPECIAL COMPUTER COVERAGE';
	proc freq data=out.FortifiedRoof_Rate;
	tables HO_0414_FLAG*FortifiedRoof_Rate / list missing;
	run;
%end;
%mend;
%sc();
