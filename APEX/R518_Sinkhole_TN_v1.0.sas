** RULE 518 Sink Hole Coverage **;
**=====================================================================================**
History: 2015 10 27 SY  Initial draft
**=====================================================================================**;
%macro sk();
	data r518;
			set fac.'518# Sinkhole Coverage$'n;
			if f1 = "" then delete;
			rename 'Coverage Limit Increment (i#e# r'n = Increment;
			run;

		proc sql;
			create table out.Sink_Hole_Rate as
			select 
				  p.CaseNumber
				, p.PolicyNumber
				, p.SinkHole_Flag
				, case 
					when p.SinkHole_Flag=100 
					then f.Rate
					else 0 end as shf
				 , round(p.CoverageA*(calculated shf)/Increment,1) as Sink_Hole_Rate
			from 
				  in.policy_info as p, r518 as f
			;
		quit;

%if &Process = 0 %then %do;
	title 'RULE 518 SINK HOLE';
	proc freq data=out.Sink_Hole_Rate;
	tables Sink_Hole_Rate / list missing;
	run;
%end;
%mend;
%sk();
