/* R349 Loss Mitigation MS v1.0 P7 Wind & P9 Hurricane */
/*Mitigation level code is 0-8*/
proc sql;
create table loss_mitigation_info as
select p1.casenumber, p1.policynumber, p1.construction_completed
,p2.constructiontype
from in.production_info p1
inner join  construction as p2
	on p1.casenumber = p2.casenumber
	;
quit;


data Loss_Mitigation_table;
  set fac.'349# Loss Mitigation Discount$'n;
if _n_= 1 then delete;
rename  f1= mitigation_level	
f2 = construction_type
'Wind'n = wind_loss_mitigation
'Hurricane'n = hurr_loss_mitigation;
drop f5-f12;
run;


PROC format;
 invalue ecode
	'FFSLS'=200
	'IRC'=100
	'Retrofit Gold - Option 1' = 300
	'Retrofit Gold - Option 2' = 400
	'Retrofit Silver - Option 1' = 500
	'Retrofit Silver - Option 2' = 600
	'Retrofit Bronze - Option 1' = 700
	'Retrofit Bronze - Option 2' = 800;
Run;

data loss_mitigation_table_ec;
 set loss_mitigation_table;
 mitigation_level_en=input(mitigation_level,ecode.);
Run;

proc sql;
	create table out.mitigation_fac as
	select *
	, case when p.construction_completed = 0 then 1 else f1.hurr_loss_mitigation end as hurr_loss_mitigation_disc,
	case when p.construction_completed = 0 then 1 else f1.wind_loss_mitigation end as wind_loss_mitigation_disc
		from loss_mitigation_info p
		left join loss_mitigation_table_ec f1 
             on (p.construction_completed = f1.mitigation_level_en)
			 and (left(trim(upper(p.ConstructionType))) = left(trim(upper(f1.Construction_Type))))
	;
quit;
