** RULE 406. DEDUCTIBLES **;

**=====================================================================================**
History: 2017 10 25 JK  Initial draft- Based off TX vBroll. Uses GridID
**=====================================================================================**;


%macro Ded();
/*Create table with Min Deductible By Grid*/
	proc sql;
		create table out.grid_ded as
		select 
			p.casenumber
			, p.policynumber
			, p.GridID
			, f.'Deductible Territory'n as ded_terr
		from out.territory p 
 		left join GridID f on p.GridID = f.GridID;
	quit;



data deductible_fac;
	set fac.'406# Deductibles$'n;
	format f1 f2 12.2;
	if f1 < 1 then f1 = f1*100;
	if f1 = . then delete;
	cova_L = lag(f2); if cova_L > f2 or cova_L = . then cova_L = 0; rename f2 = cova_H; 
	Fire_n = Fire*1; Theft_n = Theft*1; waternw_n = 'Water Non-Weather'n*1; 
	othernw_n = 'All Other Non-Weather'n*1; Lightning_n = Lightning*1; waterw_n = 'Water Weather'n*1; 
	Wind_n = Wind*1; Hail_n = Hail*1; Hurricane_n = Hurricane*1;
	array peril (9) Fire_n Theft_n waternw_n othernw_n Lightning_n waterw_n Wind_n Hail_n Hurricane_n;
	array fac_l (9) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l;
	array fac_h (9) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h;
	do i = 1 to 9; 
		fac_l(i) = lag(peril(i));
		fac_h(i) = peril(i);
		if cova_L = 0 then fac_l(i) = fac_h(i);
	end;
	drop i;
	run;
	proc sql noprint;
		select max(cova_H)
		into: maxdedcova
		from deductible_fac
		;
	quit;
	%put &maxdedcova;

/*Add Distance to Shore Condition*/  /*No policies with DTS < 2500 feet*/
			proc sql;
				create table policy_info_terr as
				select 					
					  p.casenumber 
					, p.policynumber 
					, p.coveragea 
					, p.CoverageDeductible 
					, p.WindHailDeductible 
					, p.HurricaneDeductible
					, p.distancetoshore
					, g.ded_terr
					, case	when p.policyaccountnumber in (14300,14310) and p.policytermyears = 1
							then "N"
							else "Y"
					  end length=1 as EnforceMinDeductible

				from in.policy_info as p
				inner join out.grid_ded as g
					on p.casenumber = g.casenumber
			;quit;


/*TX code applies min ded by ded_terr*/

	data deductible;
	set policy_info_terr;
	AOPDeductible = input(CoverageDeductible,dollar8.);
	WHDeductible =  input(WindHailDeductible,8.);
	HurrDeductible =  input(HurricaneDeductible,8.);

	%if &Broll_Ded = 1 %then %do;
	if AOPDeductible = 250 and EnforceMinDeductible = "Y" then AOPDeductible = 500;
	if 10 < WHDeductible < 500 then WHDeductible = AOPDeductible;
	if HurrDeductible = . then HurrDeductible = WHDeductible;

	Wind_Dollar = WHDeductible; if WHDeductible <= 10 then Wind_Dollar = WHDeductible*CoverageA/100;
	Hurr_Dollar = HurrDeductible; if HurrDeductible <= 10 then Hurr_Dollar = HurrDeductible*CoverageA/100;

	Min_1_2500 = max(.01*coveragea,2500);
	Min_2_5000 = max(.02*coveragea,5000);

	if ded_terr in (1,3) 
		and  Wind_Dollar 	< Min_2_5000
		and  Min_2_5000 	<= 5000
		and  EnforceMinDeductible =  "Y"
		then whdeductible 	= 5000;
	if ded_terr in (1,3) 
		and  Wind_Dollar 	< Min_2_5000
		and  Min_2_5000		> 5000
		and  EnforceMinDeductible =  "Y"
		then whdeductible 	= 2;

	if ded_terr in (2,4) 
		and  Wind_Dollar 	< Min_1_2500
		and  Min_1_2500		<= 2500	
		and  EnforceMinDeductible =  "Y"
		then whdeductible 	= 2500;
	if ded_terr in (2,4) 
		and  Wind_Dollar 	< Min_1_2500
		and  Min_1_2500		>2500
		and  EnforceMinDeductible =  "Y"
		then whdeductible 	= 1;

	if ded_terr in (1,2) 	
		and  hurrdeductible not in (5,10)
		and  EnforceMinDeductible =  "Y"
		then hurrdeductible = 5;

	if ded_terr in (3) 
		and  Hurr_Dollar 	< Min_2_5000
		and  Min_2_5000 	<= 5000
		and  EnforceMinDeductible =  "Y"
		then hurrdeductible	= 5000;
	if ded_terr in (3) 
		and  Hurr_Dollar 	< Min_2_5000
		and  Min_2_5000		> 5000
		and  EnforceMinDeductible =  "Y"
		then hurrdeductible	= 2;

	if ded_terr in (4) 
		and  Hurr_Dollar 	< Min_1_2500
		and  Min_1_2500		<= 2500
		and  EnforceMinDeductible =  "Y"
		then hurrdeductible	= 2500;
	if ded_terr in (4) 
		and  Hurr_Dollar 	< Min_1_2500
		and  Min_1_2500		>2500
		and  EnforceMinDeductible =  "Y"
		then hurrdeductible	= 1;
	%end;%else %do;
	if AOPDeductible = 250 then AOPDeductible = 500;
	if 10 < WHDeductible < 500 then WHDeductible = AOPDeductible;
	if HurrDeductible = . then HurrDeductible = WHDeductible;

	Wind_Dollar = WHDeductible; if WHDeductible <= 10 then Wind_Dollar = WHDeductible*CoverageA/100;
	Hurr_Dollar = HurrDeductible; if HurrDeductible <= 10 then Hurr_Dollar = HurrDeductible*CoverageA/100;

	Min_1_2500 = max(.01*coveragea,2500);
	Min_2_5000 = max(.02*coveragea,5000);

	if ded_terr in (1,3) 
		and  Wind_Dollar 	< Min_2_5000
		and  Min_2_5000 	<= 5000
		then whdeductible 	= 5000;
	if ded_terr in (1,3) 
		and  Wind_Dollar 	< Min_2_5000
		and  Min_2_5000		> 5000
		then whdeductible 	= 2;

	if ded_terr in (2,4) 
		and  Wind_Dollar 	< Min_1_2500
		and  Min_1_2500		<= 2500	
		then whdeductible 	= 2500;
	if ded_terr in (2,4) 
		and  Wind_Dollar 	< Min_1_2500
		and  Min_1_2500		>2500
		then whdeductible 	= 1;

	if ded_terr in (1,2) 	
		and  hurrdeductible not in (5,10)
		then hurrdeductible = 5;

	if ded_terr in (3) 
		and  Hurr_Dollar 	< Min_2_5000
		and  Min_2_5000 	<= 5000
		then hurrdeductible	= 5000;
	if ded_terr in (3) 
		and  Hurr_Dollar 	< Min_2_5000
		and  Min_2_5000		> 5000
		then hurrdeductible	= 2;

	if ded_terr in (4) 
		and  Hurr_Dollar 	< Min_1_2500
		and  Min_1_2500		<= 2500	
		then hurrdeductible	= 2500;
	if ded_terr in (4) 
		and  Hurr_Dollar 	< Min_1_2500
		and  Min_1_2500		>2500
		then hurrdeductible	= 1;
	%end;

	;run;




	proc sql;
		create table deductible1 as
		select p.*, f1.cova_L, f1.cova_H
			, f1.fire_l, f1.fire_h
			, f1.theft_l, f1.theft_h
			, f1.waternw_l, f1.waternw_h
			, f1.othernw_l, f1.othernw_h
			, f1.lightning_l, f1.lightning_h
			, f1.waterw_l, f1.waterw_h
			, f2.wind_l, f2.wind_h
			, f2.hail_l, f2.hail_h
			, f3.hurr_l, f3.hurr_h
	
		from deductible p 
			left join deductible_fac f1 on (p.Coveragea <= f1.cova_H or f1.cova_H = &maxdedcova) 
				and p.Coveragea > f1.cova_L and p.AOPDeductible = f1.F1
			left join deductible_fac f2 on (p.Coveragea <= f2.cova_H or f2.cova_H = &maxdedcova) 
				and p.Coveragea > f2.cova_L and p.WHDeductible = f2.F1
			left join deductible_fac f3 on (p.Coveragea <= f3.cova_H or f3.cova_H = &maxdedcova) 
				and p.Coveragea > f3.cova_L and p.HurrDeductible = f3.F1
		;
	quit;

	data out.ded_fac; set deductible1;
	array fac_l (9) fire_l theft_l waternw_l othernw_l lightning_l waterw_l wind_l hail_l hurr_l;
	array fac_h (9) fire_h theft_h waternw_h othernw_h lightning_h waterw_h wind_h hail_h hurr_h;
	array fac   (9) fire_ded theft_ded waternw_ded othernw_ded lightning_ded waterw_ded wind_ded hail_ded hurr_ded;
	do i = 1 to 9;
		%if &Broll_Ded = 1 %then %do;
			if EnforceMinDeductible = "N" then do;
				if fac_h(i) = . then fac_h(i) = 1;
				if fac_l(i) = . then fac_l(i) = 1;	
			end;
		%end;
		if coveragea > &maxdedcova or coveragea = cova_H then fac(i) = fac_h(i);
		else fac(i) = round((fac_h(i) - fac_l(i))*(coveragea - cova_L)/(cova_H - cova_L) + fac_l(i),.0001);
	end;
	drop i;
	run;

%if &Process = 0 %then %do;
	title 'RULE 406. DEDUCTIBLES';
			proc sort data=out.ded_fac; by AOPDeductible; run;
			proc gplot data=out.ded_fac;
				by AOPDeductible;
				plot (fire_ded theft_ded waternw_ded othernw_ded lightning_ded waterw_ded)*coveragea 
					/ overlay legend vaxis=axis1;
			run;
			quit; 
			proc sort data=out.ded_fac; by WHDeductible; run;
			proc gplot data=out.ded_fac;
				by WHDeductible;
				plot (wind_ded hail_ded)*coveragea 
					/ overlay legend vaxis=axis1;
			run;
			quit; 
			proc sort data=out.ded_fac; by HurrDeductible; run;
			proc gplot data=out.ded_fac;
				by HurrDeductible;
				plot hurr_ded*coveragea 
					/ overlay legend vaxis=axis1;
			run;
			quit; 
%end;

%mend;
%ded();