/*Step 3 Supplement*/

**=====================================================================================**
History: 2015 04 20 JT  1.0 Initial draft
		 2015 05 27 SY  1.1 Added Hurricane peril back and revise the calculations for special personal property
**=====================================================================================**;

%macro sup();
proc sql;
	create table work.Pricing_PerilLog as
	select casenumber
	, fire_base_rate as BaseRate_Fire
	, fire_base_rate * fire_census as CensusBlock_Fire
	, calculated CensusBlock_Fire * fire_cova as CoverageA_Fire
	, calculated CoverageA_Fire * fire_home_age as AgeofHome_Fire
/*	, calculated AgeofHome_Fire * fire_num_res as NumberofResidents_Fire*/
	, calculated AgeofHome_Fire * fire_heat_sys as HeatingSystem_Fire
	, calculated HeatingSystem_Fire * fire_ins_score as UWTier_Fire
	, calculated UWTier_Fire * fire_rowhouse as TownhouseorRowhouse_Fire
	, calculated TownhouseorRowhouse_Fire * fire_sec_res as SecondaryRes_Fire
	, calculated SecondaryRes_Fire * fire_naogclaimxPeril as NAOGClaimsxPeril_Fire
	, calculated NAOGClaimsxPeril_Fire * fire_claim as PerilClaims_Fire
	, calculated PerilClaims_Fire * fire_ded as Deductibles_Fire
	, calculated Deductibles_Fire * fire_sp_personal_prop as SpecialPersonalProp_Fire
	, fire_base_premium as PerilBasePremium_Fire
	, fire_base_premium * fire_new_home as NewHomeConstruction_Fire
	, calculated NewHomeConstruction_Fire * fire_new_home_purch as NewHomePurchase_Fire
	, calculated NewHomePurchase_Fire * fire_prot_dev as ProtectiveDevices_Fire
	, fire_disc_base_premium as DiscPerilPrem_Fire

	, Theft_base_rate as BaseRate_Theft
	, Theft_base_rate * theft_county as County_Theft
	, calculated County_Theft * Theft_census as CensusBlock_Theft
	, calculated CensusBlock_Theft * theft_covcbase as CoverageC_Theft
	, calculated CoverageC_Theft * theft_cust_age as CustomerAge_Theft
/*	, calculated CustomerAge_Theft * theft_drc as DRC_Theft*/
	, calculated CustomerAge_Theft * theft_ins_score as UWTier_Theft
	, calculated UWTier_Theft * theft_length_res as LengthofResidence_Theft
/*	, calculated LengthofResidence_Theft * theft_num_res as NumberofResidents_Theft*/
	, calculated LengthofResidence_Theft * theft_num_units as MultiFamily_Theft
	, calculated MultiFamily_Theft * theft_pool as PresenceofPool_Theft
	, calculated PresenceofPool_Theft * theft_sqft as SquareFootage_Theft
	, calculated SquareFootage_Theft * theft_sec_res as SecondaryRes_Theft
	, calculated SecondaryRes_Theft * theft_naogclaimxPeril as NAOGClaimsxPeril_Theft
	, calculated NAOGClaimsxPeril_Theft * theft_claim as PerilClaims_Theft
	, calculated PerilClaims_Theft * theft_ded as Deductibles_Theft
	, calculated Deductibles_Theft * theft_sp_personal_prop as SpecialPersonalProp_Theft
	, theft_base_premium as PerilBasePremium_Theft
	, theft_base_premium * theft_prot_dev as ProtectiveDevices_Theft
	, Theft_disc_base_premium as DiscPerilPrem_Theft

	, waternw_base_rate as BaseRate_WaterNW
	, waternw_base_rate * waternw_grid as Grid_WaterNW
	, Calculated Grid_WaterNW * waternw_census as CensusBlock_WaterNW
	, calculated CensusBlock_WaterNW * waternw_cova as CoverageA_WaterNW
	, calculated CoverageA_WaterNW * waternw_fin_bsmnt as FinishedBasementSqFt_WaterNW
	, calculated FinishedBasementSqFt_WaterNW * waternw_home_age as AgeofHome_WaterNW
	, calculated AgeofHome_WaterNW * waternw_ins_score as UWTier_WaterNW
	, calculated UWTier_WaterNW * waternw_length_res as LengthofResidence_WaterNW
/*	, calculated LengthofResidence_WaterNW * waternw_num_res as NumberofResidents_WaterNW*/
	, calculated LengthofResidence_WaterNW * waternw_bsmnt as PresenceofBasement_WaterNW
	, calculated PresenceofBasement_WaterNW * waternw_pool as PresenceofPool_WaterNW
	, calculated PresenceofPool_WaterNW * waternw_rcsqft as ReplacementCostSqFt_WaterNW
	, calculated ReplacementCostSqFt_WaterNW * waternw_bathroom as TotalRoomswithBath_WaterNW
	, calculated TotalRoomswithBath_WaterNW * waternw_sec_res as SecondaryRes_WaterNW
	, calculated SecondaryRes_WaterNW * waternw_rowhouse  as TownhouseorRowhouse_WaterNW
	, calculated TownhouseorRowhouse_WaterNW * waternw_claimxPeril as ClaimsxWater_WaterNW
	, calculated ClaimsxWater_WaterNW * waternw_claim as WaterClaims_WaterNW
	, calculated WaterClaims_WaterNW * waternw_ded as Deductibles_WaterNW
	, calculated Deductibles_WaterNW * waternw_sp_personal_prop as SpecialPersonalProp_WaterNW
	, waternw_base_premium as PerilBasePremium_WaterNW
	, waternw_base_premium * waternw_new_home as NewHomeConstruction_WaterNW
	, waternw_disc_base_premium as DiscPerilPrem_WaterNW
	
	, othernw_base_rate as BaseRate_OtherNW
	, othernw_base_rate * othernw_cova as CoverageA_OtherNW
	, calculated CoverageA_OtherNW * othernw_cust_age as CustomerAge_OtherNW
	, calculated CustomerAge_OtherNW * othernw_home_age as AgeofHome_OtherNW
	, calculated AgeofHome_OtherNW * othernw_ins_score as UWTier_OtherNW
	, calculated UWTier_OtherNW * othernw_pool as PresenceofPool_OtherNW
	, calculated PresenceofPool_OtherNW * othernw_sec_res as SecondaryRes_OtherNW
	, calculated SecondaryRes_OtherNW * othernw_naogclaim as NAOGClaimsxPeril_OtherNW
	, calculated NAOGClaimsxPeril_OtherNW * othernw_claim as PerilClaims_OtherNW
	, calculated PerilClaims_OtherNW * othernw_ded as Deductibles_OtherNW
	, calculated Deductibles_OtherNW * othernw_sp_personal_prop as SpecialPersonalProp_OtherNW
	, othernw_base_premium as PerilBasePremium_OtherNW
	, othernw_base_premium * othernw_new_home as NewHomeConstruction_OtherNW
	, othernw_disc_base_premium as DiscPerilPrem_OtherNW

	, lightning_base_rate as BaseRate_Lightning
	, lightning_base_rate * lightning_census as CensusBlock_Lightning
	, calculated CensusBlock_Lightning * lightning_cova as CoverageA_Lightning
	, calculated CoverageA_Lightning * lightning_ins_score as UWTier_Lightning
	, calculated UWTier_Lightning * lightning_pool as PresenceofPool_Lightning
	, calculated PresenceofPool_Lightning * lightning_rcsqft as ReplacementCostSqFt_Lightning
	, calculated ReplacementCostSqFt_Lightning * lightning_sec_res as SecondaryRes_Lightning
	, calculated SecondaryRes_Lightning * lightning_ded as Deductibles_Lightning
	, calculated Deductibles_Lightning * lightning_sp_personal_prop as SpecialPersonalProp_Lightning
	, calculated SpecialPersonalProp_Lightning * lightning_naogclaim as NAOGClaims_Lightning
	, calculated NAOGClaims_Lightning * lightning_claim as PerilClaims_Lightning
	, lightning_base_premium as PerilBasePremium_Lightning
	, lightning_base_premium * lightning_new_home_purch as NewHomePurchase_Lightning
	, lightning_disc_base_premium as DiscPerilPrem_Lightning

	, waterw_base_rate as BaseRate_WaterW
	, waterw_base_rate * waterw_grid as Grid_WaterW
	, calculated Grid_WaterW * waterw_cova as CoverageA_WaterW
	, calculated CoverageA_WaterW * waterw_home_age as AgeofHome_WaterW
	, calculated AgeofHome_WaterW * waterw_ins_score as UWTier_WaterW
	, calculated UWTier_WaterW * waterw_rcsqft as ReplacementCostSqFt_WaterW
	, calculated ReplacementCostSqFt_WaterW * waterw_roof_age as RoofAge_WaterW
	, calculated RoofAge_WaterW * waterw_claimxPeril as ClaimsxWater_WaterW
	, calculated ClaimsxWater_WaterW * waterw_sec_res as SecondaryRes_WaterW
	, calculated SecondaryRes_WaterW * waterw_claim as WaterClaims_WaterW
	, calculated WaterClaims_WaterW * waterw_ded as Deductibles_WaterW
	, waterw_base_premium as PerilBasePremium_WaterW
	, waterw_base_premium * waterw_new_roof as NewRoofDiscount_WaterW
	, calculated NewRoofDiscount_WaterW * waterw_new_home as NewHomeConstruction_WaterW
	, waterw_disc_base_premium as DiscPerilPrem_WaterW

	, wind_base_rate as BaseRate_Wind
	, wind_base_rate * wind_grid as Grid_Wind
	, calculated Grid_Wind * wind_census as CensusBlock_Wind
	, calculated CensusBlock_Wind * wind_const_class as Construction_Wind
	, calculated Construction_Wind * wind_cova as CoverageA_Wind
	, calculated CoverageA_Wind * wind_home_age as AgeofHome_Wind
	, calculated AgeofHome_Wind * wind_ins_score as UWTier_Wind
	, calculated UWTier_Wind * wind_fence as PresenceofFence_Wind
	, calculated PresenceofFence_Wind * wind_rcsqft as ReplacementCostSqFt_Wind
/*	, calculated ReplacementCostSqFt_Wind * wind_ACV_roof as ACV_Wind*/
	, calculated ReplacementCostSqFt_Wind * wind_roof_age as RoofAge_Wind
	, calculated RoofAge_Wind * wind_roof_type as RoofType_Wind
	, calculated RoofType_Wind * wind_sqft_story as SqFtperStory_Wind
	, calculated SqFtperStory_Wind * wind_sec_res as SecondaryRes_Wind
	, calculated SecondaryRes_Wind * wind_naogclaim as NAOGClaims_Wind
	, calculated NAOGClaims_Wind * wind_claim as PerilClaims_Wind
	, calculated PerilClaims_Wind * wind_ded as WHDeductibles_Wind
	, wind_base_premium as PerilBasePremium_Wind
	, wind_base_premium * wind_new_home as NewHomeConstruction_Wind
	, calculated NewHomeConstruction_Wind * wind_new_roof as NewRoofDiscount_Wind
	, calculated NewRoofDiscount_Wind * wind_new_home_purch as NewHomePurchase_Wind
	, wind_disc_base_premium as DiscPerilPrem_Wind

	, hail_base_rate as BaseRate_Hail
	, hail_base_rate * hail_grid as Grid_Hail
	, calculated Grid_Hail * hail_census as CensusBlock_Hail
	, calculated CensusBlock_Hail * hail_cova as CoverageA_Hail
	, calculated CoverageA_Hail * hail_home_age as AgeofHome_Hail
/*	, calculated AgeofHome_Hail * hail_ACV_roof as ACV_Hail*/
	, calculated AgeofHome_Hail * hail_roof_type as RoofType_Hail
	, calculated RoofType_Hail * hail_roof_age_type as RoofAgebyRoofType_Hail
	, calculated RoofAgebyRoofType_Hail * hail_sqft_story as SqFtperStory_Hail
	, calculated SqFtperStory_Hail * hail_sec_res as SecondaryRes_Hail
	, calculated SecondaryRes_Hail * hail_ded as WHDeductibles_Hail
	, hail_base_premium as PerilBasePremium_Hail
	, hail_base_premium * hail_new_home as NewHomeConstruction_Hail
	, calculated NewHomeConstruction_Hail * hail_new_roof as NewRoofDiscount_Hail
	, hail_disc_base_premium as DiscPerilPrem_Hail

	, hurr_base_rate as BaseRate_Hurr
	, hurr_base_rate * hurr_grid as Grid_Hurr
	, calculated Grid_Hurr * hurr_cova as CoverageA_Hurr
	, calculated CoverageA_Hurr * hurr_ded as HurrDeductibles_Hurr
	, calculated HurrDeductibles_Hurr * hurr_ins_score as UWTier_Hurr
	, calculated UWTier_Hurr * hurr_home_age as AgeofHome_Hurr
	, calculated AgeofHome_Hurr * hurr_roof_age as RoofAge_Hurr
/*	, calculated RoofAge_Hurr * hurr_ACV_roof as ACV_Hurr*/
	, calculated RoofAge_Hurr * hurr_sec_res as SecondaryRes_Hurr
	, calculated SecondaryRes_Hurr * hurr_naogclaim as NAOGClaims_Hurr
	, calculated NAOGClaims_Hurr * hurr_claim as PerilClaims_Hurr
	, hurr_base_premium as PerilBasePremium_Hurr
	, hurr_base_premium * hurr_new_home as NewHomeConstruction_Hurr
	, calculated NewHomeConstruction_Hurr * hurr_new_roof as NewRoofDiscount_Hurr
	, hurr_disc_base_premium as DiscPerilPrem_Hurr	

	from out.step2_base_premium
	;
quit;

proc transpose data= work.Pricing_PerilLog out=work.Pricing_PerilLogT; by casenumber; run;
data work.Pricing_PerilLogT (drop=_Label_); set work.Pricing_PerilLogT(rename=(col1=RunningTotal _Name_=RuleName)); run;
data work.Pricing_PremsInitial; set work.Pricing_PerilLogT; rulename = substr(rulename,1,index(rulename,"_")-1); run;

data work.Pricing_Prems; set work.Pricing_PremsInitial;
by casenumber ; retain RunningTotal_prior;
if first.casenumber then discount = 0; *Initializes discount of 0;
else discount = RunningTotal - RunningTotal_prior;
output;
RunningTotal_prior = RunningTotal;
run;

*add ACV?;
proc sql;
	create table work.Step3Supplement as
	select casenumber
	, rulename
	, round(sum(discount),1.0) as Discount
	from work.Pricing_Prems
	where rulename in ('Deductibles','WHDeductibles','SpecialPersonalProp','AgeofHome','NewHomeConstruction',
	'ProtectiveDevices','NewHomePurchase','NewRoofDiscount','RoofAge', 'ACV') /*May need to add more*/
	group by casenumber, rulename
	order by casenumber;
	;
quit;

/*out.Step3Supplement contains Dollar Amounts of Discounts/Surchages associated with factors applied to Peril Prems & Disc Peril Prems*/
proc transpose data=work.Step3Supplement out=out.Step3Supplement (drop=_Name_); by casenumber; ID rulename; Var Discount; run;


proc sql;
	create table out.step3_subtotal_premium as
	select a.*
	, b.SpecialPersonalProp
	, round(a.fire_disc_base_premium
					+ a.theft_disc_base_premium
					+ a.waternw_disc_base_premium
					+ a.othernw_disc_base_premium
					+ a.lightning_disc_base_premium
					+ a.waterw_disc_base_premium
					+ a.wind_disc_base_premium
					+ a.hail_disc_base_premium
					+ a.hurr_disc_base_premium
					,.0001) as subtotal_premium
	
	from out.step2_base_premium as a
	inner join out.Step3Supplement as b
	on a.CaseNumber = b.CaseNumber
	;
quit;

%if &Process = 0 %then %do;
title 'STEP 3: SUBTOTAL PREMIUM (RULE 301.A.3)';
proc print data=out.step3_subtotal_premium;
where subtotal_premium = .;
run;
proc sql;
	select mean(fire_disc_base_premium) as fire_disc_base_prem_avg
		, mean(theft_disc_base_premium) as theft_disc_base_prem_avg
		, mean(waternw_disc_base_premium) as waternw_disc_base_prem_avg
		, mean(othernw_disc_base_premium) as othernw_disc_base_prem_avg
		, mean(lightning_disc_base_premium) as lightning_disc_base_prem_avg
		, mean(waterw_disc_base_premium) as waterw_disc_base_prem_avg
		, mean(wind_disc_base_premium) as wind_disc_base_prem_avg
		, mean(hail_disc_base_premium) as hail_disc_base_prem_avg
		, mean(hurr_disc_base_premium) as hurr_disc_base_prem_avg
		, mean(subtotal_premium) as subtotal_premium_avg
	from out.step3_subtotal_premium;
quit;
%end;
%mend;
%sup();